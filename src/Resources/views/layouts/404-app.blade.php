<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1, user-scalable=0 minimal-ui">
    <meta name="robots" content="index, follow, noodp, noydir" />
    <title>{{ __js('frontend', 'not_found', '404 Sayfa Bulunamadı.') }}</title>
    <meta name="description" content="{{ @$item->seo['description'] }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ @$item->seo['facebook']['title'] }}" />
    <meta property="og:description" content="{{ @$item->seo['facebook']['content'] }}" />
    <meta property="og:url" content="{{ @$item->seo['canonical'] }}" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="{{ @$item->seo['twitter']['title'] }}" />
    <meta name="twitter:description" content="{{ @$item->seo['twitter']['title'] }}" />
    <link rel="canonical" href="{{ @$item->seo['canonical'] }}" />
    <meta name="author" content="This site managed by JsPress">
    <link rel="shortcut icon" href="{{ setting('favicon') ? the_thumbnail_url(setting('favicon')) : asset(config('jspress.panel.logo.favicon')) }}" />
    @stack("styles")
</head>
<body class=@stack('bodyClasses')>
@yield('header')

@yield('content')

@yield('footer')

@stack('scripts')
</body>
</html>
