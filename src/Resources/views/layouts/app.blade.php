@php
    $cookie = get_cookie();
@endphp
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1, user-scalable=0 minimal-ui">
    @if(config('app.env') === 'local' || config('app.env') === 'development')
        <meta name="robots" content="noindex">
    @endif
    <title>{{ $item->seo['title'] }}</title>
    <meta name="description" content="{{ $item->seo['description'] }}" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ @$item->seo['facebook']['title'] }}" />
    <meta property="og:description" content="{{ @$item->seo['facebook']['description'] }}" />
    <meta property="og:url" content="{{ @$item->seo['canonical'] }}" />
    @if(isset($item->seo['facebook']['image']) && !is_null($item->seo['facebook']['image']))
        <meta property="og:image" content="{{the_thumbnail_url($item->seo['facebook']['image'])}}">
    @endif
    <meta name="twitter:card" content="summary_large_image" />
    <meta property="twitter:domain" content="{{ url('/') }}">
    <meta property="twitter:url" content="{{ @$item->seo['canonical'] }}">
    <meta name="twitter:title" content="{{ @$item->seo['twitter']['title'] }}" />
    <meta name="twitter:description" content="{{ @$item->seo['twitter']['description'] }}" />
    @if(isset($item->seo['twitter']['image']) && !is_null($item->seo['twitter']['image']))
        <meta name="twitter:image" content="{{the_thumbnail_url($item->seo['twitter']['image'])}}">
    @endif
    <link rel="canonical" href="{{ @$item->seo['canonical'] }}" />
    @isset($item->other_languages)
        @foreach($item->other_languages as $lang)
            <link rel="alternate" hreflang="{{$lang['locale']}}" href="{{$lang['url']}}" />
        @endforeach
    @endif
    <meta name="author" content="This site managed by JsPress">
    <link rel="shortcut icon" href="{{ setting('favicon') ? the_thumbnail_url(setting('favicon')) : asset(config('jspress.panel.logo.favicon')) }}" />
    @stack("styles")
    @if(isset($item->popups) && count($item->popups) > 0)
        <link rel="stylesheet" href="{{ asset('jspress/assets/css/popup.css') }}" />
    @endif
    @if(count($cookie) > 0)
        <link rel="stylesheet" href="{{ asset('jspress/assets/css/cookieconsent.css') }}" />
    @endif
</head>
<body class=@stack('bodyClasses')>
@yield('header')

@yield('content')

@yield('footer')

@stack('scripts')
@if(count($cookie) > 0)
<script src="{{ asset('jspress/assets/js/cookieconsent.umd.js') }}"></script>
<script>
    let cookie_data = @json($cookie);
    document.documentElement.classList.add(cookie_data.theme);
    CookieConsent.run(cookie_data);
</script>
@endif
<script src="{{ asset('jspress/assets/js/site.js') }}"></script>
@if(isset($item->popups) && count($item->popups) > 0)
    <script src="{{ asset('jspress/assets/js/popup.js') }}"></script>
    @foreach($item->popups as $popup)
        <style>{!! $popup['custom_css'] !!}</style>
    @endforeach
    <script>

        const popups = @json($item->popups);
        $(document).ready(function(){
            var popupsInstances = {};
            for (var p in popups) {
                (function (popup) {
                    if(popup.design.font_data != null){
                        SiteFunc.callGoogleFonts(popup.design.font_data);
                    }

                    var is_scroll_active = true;
                    popupsInstances[popup.id] = new JsPopup({
                        unique_id: popup.id,
                        title: popup.title,
                        content: popup.content,
                        type: popup.type,
                        transition: 'fade',
                        place: popup.place,
                        display_title: popup.is_display_name,
                        is_content_image: popup.is_content_image,
                        design: popup.design,
                        display_type: popup.conditions,
                        timeout: popup.condition_time,
                        time_type: popup.time_type,
                        display_time: popup.display_time,
                        impressions: popup.impressions,
                        impression_count: popup.impression_count,
                        condition: popup.conditions,
                        button_type: popup.button_type,
                        button_text: popup.button_text,
                        screen_timer_text: '{{ __js('popup', 'timer_text', 'saniye sonra sona erecek.') }}',
                        onOpen: function ($popupElement) {},
                        onClose: function ($popupElement) {}
                    });
                    if (popupsInstances[popup.id].is_active) {
                        if (popup.conditions === 0) {
                            setTimeout(function () {
                                popupsInstances[popup.id].open();
                            }, popup.condition_time * 1000);
                        } else {
                            window.addEventListener('scroll', function () {
                                const scroolY = window.scrollY;
                                if (scroolY >= parseInt(popup.condition_scroll) && is_scroll_active) {
                                    popupsInstances[popup.id].open();
                                    is_scroll_active = false;
                                }
                            });
                        }
                    }
                })(popups[p]);
            }
        });

    </script>
@endif
</body>
</html>
