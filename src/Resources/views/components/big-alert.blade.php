<div class="alert alert-dismissible {{$alertClass}} d-flex flex-center flex-column py-10 px-10 px-lg-20 mb-10">
    <span class="svg-icon svg-icon-5tx {{$iconClass}} mb-5">
        {!! $iconSvg !!}
    </span>
    <div class="text-center">
        <h1 class="fw-bolder mb-5">{{$title}}</h1>
        <div class="separator separator-dashed border-danger opacity-25 mb-5"></div>
        <div class="mb-9 text-dark">{{$message}}</div>
        @if(!is_null($button))
        <div class="d-flex flex-center flex-wrap">
            <a href="{{$button['url']}}" class="btn {{$buttonClass}} m-2">{{$button['text']}}</a>
        </div>
        @endif
    </div>
</div>
