<div id="{{$id}}">
    <div class="form-group p-2">
        @if(!is_null($label))
            <label class="{{$required ? 'required':''}} form-label">{{ $label }}</label>
        @endif
        <input type="text" name="{{$name}}" class="form-control jspress_datepicker mb-2 {{$classes}}" value="{{$value}}" {{$required == 1 ? 'required':''}}>
    </div>
</div>
