<div id="{{$id}}">
    <div class="form-group p-2">
        @if(!is_null($label))
            <label class="form-label">{{ $label }}</label>
        @endif
        <div class="d-flex flex-wrap">
            @foreach($options as $option)
                <label class="form-check form-check-sm form-check-custom form-check-solid me-2">
                    <input class="form-check-input" type="radio" value="{{$option['value']}}" name="{{$name}}" {{ $required ? 'required' : '' }} {{ $isChecked($option['value']) ? 'checked' : '' }}>
                    <span class="form-check-label">{{$option['label']}}</span>
                </label>
            @endforeach
        </div>
    </div>
</div>
