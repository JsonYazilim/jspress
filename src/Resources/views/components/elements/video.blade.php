<div class="col-12">
    <label class="form-label">{{$label}}</label>
    <div class="upload-ui pt-10 pb-10 min-h-250px border border-4 border-dashed border-active-danger text-center">
        <div class="d-flex flex-center min-h-250px uploadVideoFiles">
            <a href="javascript:void(0);" class="btn btn-flex btn-primary px-6 {{ !is_null($value) ? 'd-none':'' }}" onclick="document.getElementById('video_file_{{$id}}').click();">
                <span class="svg-icon svg-icon-2x">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM16 13.5L12.5 13V10C12.5 9.4 12.6 9.5 12 9.5C11.4 9.5 11.5 9.4 11.5 10L11 13L8 13.5C7.4 13.5 7 13.4 7 14C7 14.6 7.4 14.5 8 14.5H11V18C11 18.6 11.4 19 12 19C12.6 19 12.5 18.6 12.5 18V14.5L16 14C16.6 14 17 14.6 17 14C17 13.4 16.6 13.5 16 13.5Z" fill="black"></path>
                        <rect x="11" y="19" width="10" height="2" rx="1" transform="rotate(-90 11 19)" fill="black"></rect>
                        <rect x="7" y="13" width="10" height="2" rx="1" fill="black"></rect>
                        <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black"></path>
                    </svg>
                </span>
                <span class="d-flex flex-column align-items-start ms-2">
                    <span class="fs-3 fw-bolder">Video Seç</span>
                    <span class="fs-7">{{__('JsPress::backend.max_upload_file_size')}} {{ convert_max_file_size_to_string() }}</span>
                </span>
            </a>
            <div class="w-100 loadingVideoAnimation d-none">
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 200" width="150"><circle fill="none" stroke-opacity="1" stroke="#212E48" stroke-width=".5" cx="100" cy="100" r="0"><animate attributeName="r" calcMode="spline" dur="2" values="1;80" keyTimes="0;1" keySplines="0 .2 .5 1" repeatCount="indefinite"></animate><animate attributeName="stroke-width" calcMode="spline" dur="2" values="0;25" keyTimes="0;1" keySplines="0 .2 .5 1" repeatCount="indefinite"></animate><animate attributeName="stroke-opacity" calcMode="spline" dur="2" values="1;0" keyTimes="0;1" keySplines="0 .2 .5 1" repeatCount="indefinite"></animate></circle></svg>
                <h3 class="text-center pt-1">Video Yükleniyor...</h3>
            </div>
            @if(!is_null($value))
                @php
                    $video = get_file($value);
                @endphp
                <div class="w-100 mh-250px uploadedVideoField">
                    <video controls>
                        <source src="{{ $video['original_url']  }}" type="{{$video['mime']}}">
                    </video>
                    <div class="w-100 d-flex flex-center mt-1">
                        <button class="btn btn-light-danger me-2 mt-1" onclick="JsPressFunc.deleteVideoFile('video_file_{{$id}}')">Videoyu Kaldır</button>
                    </div>
                </div>
            @endif
            <input id="video_file_{{$id}}" type="file" accept="video/mp4,video/x-m4v,video/*" style="display:none;">
            <input type="hidden" name="{{$name}}" value="{{$value}}">
        </div>
    </div>
</div>
@push('script')
    <script>
        var file_el = 'video_file_{{$id}}';

        document.getElementById(file_el).addEventListener('change', function(){
            let $this = this;
            JsPressFunc.videFileUpload($this);
        });
    </script>
@endpush
