<div id="{{$id}}">
    <div class="form-group p-2">
        @if(!is_null($label))
            <label class="form-label">{{ $label }}</label>
        @endif
        <label class="form-check form-switch form-check-custom form-check-solid">
            <input class="form-check-input" name="{{$name}}" type="checkbox" value="1" {{ $isChecked() || ( is_null($selected) && $isopen) ? 'checked' : '' }}/>
            @if(!is_null($message))
                <span class="form-check-label fw-bold text-muted">{{$message}}</span>
            @endif
        </label>
    </div>
</div>
