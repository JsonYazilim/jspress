<div id="{{$id}}">
    <div class="form-group p-2">
        @if(!is_null($label))
            <label class="required form-label">{{ $label }}</label>
        @endif
        <textarea name="{{$name}}" class="form-control mb-2 {{$classes}}" placeholder="{{ $placeholder }}" {{$required == 1 ? 'required':''}}>{{$value}}</textarea>
    </div>
</div>
