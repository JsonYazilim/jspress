<div class="dropzone-image d-flex justify-content-center align-items-center w-100 h-200px position-relative border border-1 border-dashed rounded-2 cursor-pointer mb-3">
    <div class="dropzone-image-wrapper w-100 h-200px position-absolute z-index-1 {{ is_null($image) ? 'd-none':'' }}">
        <div class="w-100 h-200px d-flex justify-content-center align-items-center border border-1 border-dashed rounded-2 bg-body">
            <img src="{{ !is_null($image) ? the_thumbnail_url($image, 'medium') : ''  }}" class="w-100 ps-2 pe-2 pb-2 pt-2 rounded-2" style="object-fit: cover; max-height:100%"/>
        </div>
        <button type="button" class="btn btn-icon btn-danger btn-active-light-danger btn-hover-rise position-absolute z-index-2" style="bottom:10px; right:10px;" onclick="JsPressFunc.removeDropzoneImage(this, '{{$name}}')">
            <span class="svg-icon svg-icon-muted">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                <path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="black" />
                <path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="black" />
                <path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="black" />
              </svg>
            </span>
        </button>
    </div>
    <button type="button" class="btn btn-light-primary" onclick="return JsPressFunc.fileUploadButtonAction('{{$name}}','image', 1, 'dropzone_image', '{{JsPressPanel::panelLocale()}}')">{{__('JsPress::backend.upload_image')}}</button>
    <input type="hidden" name="{{$name}}" value="{{ !is_null($image) ? $image:'' }}"/>
</div>
@if(!is_null($message))
<div class="text-muted fs-7">{{$message}}</div>
@endif
