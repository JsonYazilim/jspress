<div class="w-{{ $width }}">
    <div class="me-2">
        <label class="form-label">{{$label}}</label>
        <div class="d-flex align-items-center flex-wrap">
            @foreach($options as $option)
                <label class="cursor-pointer form-check form-check-inline form-check-solid me-5 mb-2">
                    <input class="form-check-input" name="{{$name}}" type="checkbox" value="{{$option['value']}}">
                    <span class="fw-bold ps-2 fs-6">{{$option['label']}}</span>
                </label>
            @endforeach
        </div>
    </div>
</div>
