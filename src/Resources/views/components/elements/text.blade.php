<div id="{{$id}}">
    <div class="form-group p-2">
        @if(!is_null($label))
        <label class="{{$required ? 'required':''}} form-label">{{ $label }}</label>
        @endif
        <input type="{{$type}}" name="{{$name}}" class="form-control mb-2 {{$classes}}" placeholder="{{ $placeholder }}" value="{{$value}}" {{$required == 1 ? 'required':''}}>
    </div>
</div>
