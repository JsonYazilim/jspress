<div id="{{$id}}" class="repeaterFields" style="width: 100%;">
    <table class="table align-middle table-row-dashed fs-6 gy-5">
        <thead>
        <th>Sıra</th>
        @foreach($elements as $key => $element)
            <th>{{ __js('post_fields','post_field_'.$key.'_'.$element['key'].'_title', $element['label']) }}</th>
        @endforeach
        <th></th>
        </thead>
        <tbody class="{{$id}}" data-repeater-list="{{$name}}">
        <tr class="draggable" data-repeater-item>
            <td>
                <i class="draggable-handle fas fa-arrows-alt fs-2"></i>
            </td>
            @foreach($elements as $key => $element)
                <td style="vertical-align: bottom;">
                    @if($element['type'] === 'text' || $element['type'] === 'number' || $element['type'] === 'email')
                        <x-jspress-text
                            :width="null"
                            :name="$element['key']"
                            :label="null"
                            :type="$element['type']"
                            :placeholder="$element['placeholder']"
                            :required="$element['is_required']"
                            :classes="$element['classes'].' form-control-sm'"
                            :id="$element['ids']"
                            :value="null"
                        >
                        </x-jspress-text>
                    @endif
                    @if($element['type'] === 'textarea')
                        <x-jspress-textarea
                            :width="null"
                            :name="$element['key']"
                            :label="null"
                            :type="$element['type']"
                            :placeholder="$element['placeholder']"
                            :required="$element['is_required']"
                            :classes="$element['classes'].' form-control-sm'"
                            :id="$element['ids']"
                            :value="null"
                        >
                        </x-jspress-textarea>
                    @endif
                    @if($element['type'] === 'selectbox')
                        <x-jspress-select-box
                            :width="null"
                            :name="$element['key']"
                            :label="null"
                            :required="$element['is_required']"
                            :classes="$element['classes'].' form-control-sm'"
                            :multiple="$element['is_multiple']"
                            :id="$element['ids']"
                            :options="repeater_selectbox_parse($element)"
                            :selected="null"
                        >
                        </x-jspress-select-box>
                    @endif
                    @if($element['type'] === 'checkbox')
                        <x-jspress-check-box-field
                            :width="null"
                            :name="$element['key']"
                            :label="null"
                            :required="$element['is_required']"
                            :classes="$element['classes'].' form-control-sm'"
                            :id="$element['ids']"
                            :options="repeater_select_parse($element['options'])"
                            :selected="null"
                        >
                        </x-jspress-check-box-field>
                    @endif
                    @if($element['type'] === 'radio')
                        <x-jspress-radio
                            :width="null"
                            :name="$element['key']"
                            :label="null"
                            :required="$element['is_required']"
                            :classes="$element['classes'].' form-control-sm'"
                            :id="$element['ids']"
                            :options="repeater_select_parse($element['options'])"
                            :selected="null"
                        >
                        </x-jspress-radio>
                    @endif
                    @if($element['type'] === 'switch')
                        <x-jspress-switch-button
                            :width="null"
                            :name="$element['key']"
                            :label="null"
                            :required="$element['is_required']"
                            :classes="$element['classes'].' form-control-sm'"
                            :id="$element['ids']"
                            :isopen="$element['is_open']"
                            :message="$element['message']"
                            :selected="null"
                        >
                        </x-jspress-switch-button>
                    @endif
                    @if($element['type'] === 'image')
                        <x-jspress-file-upload-button
                            :id="$element['key']"
                            type="image"
                            :maxfile="1"
                            action="repeater_file_upload"
                            :name="$element['key']"
                            :image="null"
                            width="75"
                        ></x-jspress-file-upload-button>
                    @endif
                    @if($element['type'] === 'post_object')
                        <x-jspress-post-objects
                            :width="null"
                            name="extras[{{$element['key']}}]"
                            :label="null"
                            :required="$element['is_required']"
                            :classes="$element['classes'].' form-control-sm'"
                            :id="$element['ids']"
                            :options="get_posts_by_post_types($element['post_objects'])"
                            :selected="null"
                        >
                        </x-jspress-post-objects>
                    @endif
                    @if($element['type'] === 'editor')
                        <x-jspress-editor
                            :width="null"
                            name="extras[{{$element['key']}}]"
                            :label="null"
                            :required="$element['is_required']"
                            :classes="$element['classes'].' form-control-sm'"
                            :id="$element['ids']"
                            :value="null"
                        >
                        </x-jspress-editor>
                    @endif
                </td>
            @endforeach
            <td>
                <a href="javascript:void(0);" data-repeater-delete class="btn btn-sm btn-light-danger">
                    <i class="la la-trash-o"></i>
                </a>
            </td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="{{count($elements)}}">
                <a href="javascript:void(0);" data-repeater-create class="btn btn-sm btn-light-primary">
                    <i class="la la-plus"></i>{{__('JsPress::backend.add')}}
                </a>
            </td>
        </tr>
        </tfoot>
    </table>
</div>
@push('script')
    <script>
        var validatorField = {
            validators: {
                notEmpty: {
                    message: 'The field is required',
                },
            },
        };
        var tinymceoptions = {
            selector: '[data-control="tinymce"]',
            plugins: 'image media code imagetools pagebreak anchor fullscreen link lists print table',
            language: window._lang ?? 'tr',
            init_instance_callback : function(editor) {
                var id = editor.id;
                var value = document.getElementById(id).closest('.tinmy_editor_container').querySelector('.tinymce_inner').value;
                editor.setContent(value);
            },
            setup: function(ed) {
                var element = this.id;
                ed.on('keyup change', function(e) {
                    document.getElementById(element).closest('.tinmy_editor_container').querySelector('.tinymce_inner').value = ed.getContent();
                });

            }
        };
        var options = {
            initEmpty: false,
            show: function () {
                $(this).slideDown();
                $(this).find('[data-control="select2"]').select2();
                if (KTApp.isDarkMode()) {
                    options["skin"] = "oxide-dark";
                    options["content_css"] = "dark";
                }
                tinymce.init(tinymceoptions);
            },
            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);

            },
            ready:function(setIndexes){
                var containers = document.querySelectorAll(".{{$id}}");
                var swappable = new Sortable.default(containers, {
                    draggable: ".{{$id}} .draggable",
                    handle: ".{{$id}} .draggable .draggable-handle",
                    mirror: {
                        appendTo: "body",
                        constrainDimensions: true
                    },

                });
                swappable.on('sortable:stop', function(){
                    setTimeout(function(){
                        setIndexes();
                    },400);
                });
                $(this).find('[data-control="select2"]').select2();

                setTimeout(function (){
                    getMediaItems();
                },300);

            }
        };
        var repeater = $('#{{ $id }}').repeater(options);
        @if( !is_null($values) )
        repeater.setList(@json($values));
        @endif
        function getMediaItems(){
            let items = document.querySelectorAll('.repeaterFields .file_upload_wrapper');
            items.forEach((item) => {
                var input = item.getElementsByTagName('input')[0];
                if(input.value !== null && input.value !== ""){
                    JsPressFunc.loadMediaDetailViaFetch(input.value).then((data) => {
                        item.querySelector('.single_file_image').innerHTML = '<img src="'+data.original_url+'" style="height:60px;"/>'
                    });
                }
            });
        }
        function setEditorItems(){
            let items = document.querySelectorAll('.repeaterFields .tinmy_editor_container');
            items.forEach((item) => {
                var value = item.querySelector('.tinymce_inner').value;
                var id = item.querySelector('[data-control="tinymce"]').getAttribute('id');
                tinymce.get(id).setContent(value);
            });
        }
    </script>
@endpush
