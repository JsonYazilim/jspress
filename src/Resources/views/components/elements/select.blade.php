<div class="w-{{ $width }}">
    <div class="me-2 is_required">
        <label class="form-label">{{$label}}</label>
        <select class="form-select form-control" name="{{$name}}" aria-label="Select example" {{ $required ? 'required' : '' }}>
            <option value="">Seçiniz</option>
            @foreach($options as $option)
                <option value="{{ $option['key'] }}" {{ $isSelected($option['key']) ? 'selected' : '' }}>{{ $option['value'] }}</option>
            @endforeach
        </select>
    </div>
</div>

