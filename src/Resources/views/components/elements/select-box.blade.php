<div id="{{$id}}">
    <div class="form-group p-2 w-100">
        @if(!is_null($label))
            <label class="form-label">{{ $label }}</label>
        @endif
        <select name="{{$name}}" class="form-select mb-2 {{$classes}}" {{ $multiple ? 'multiple':'' }} data-control="{{$multiple ? 'select2':''}}" data-allow-clear="{{ $multiple ? 'true':'' }}" {{$required ? 'required':''}}>
            <option value="" {{$multiple ? 'disabled':''}}>{{ __('JsPress::backend.choose') }}</option>
            @foreach($options as $option)
                <option value="{{$option['value']}}" {{ $isSelected($option['value']) ? 'selected' : '' }}>{{$option['label']}}</option>
            @endforeach
        </select>
    </div>
</div>
