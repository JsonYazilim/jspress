<div id="{{ $id }}">
    <div class="form-group">
        <div data-repeater-list="{{$name}}">
            <div class="mb-3" data-repeater-item>
                <div class="form-group d-flex flex-wrap">
                    @foreach($elements as $type => $element)
                        @if($type == 'select')
                            <x-jspress-select
                                :width="$element['width']"
                                :name="$element['name']"
                                :label="$element['label']"
                                selected=""
                                :options="$element['options']"
                                :required="$element['required']"
                                :repeater="$element['is_repeater']"
                            ></x-jspress-select>
                        @endif
                        @if($type == 'checkbox')
                            <x-jspress-checkbox :width="$element['width']" :name="$element['name']" :label="$element['label']" :options="$element['options']" selected=""></x-jspress-checkbox>
                        @endif
                    @endforeach
                </div>
                <div class="d-flex">
                    <div class="w-25">
                        <a href="javascript:void(0);" data-repeater-delete class="btn btn-sm btn-light-danger">
                            <i class="la la-trash-o"></i>{{__('JsPress::backend.delete')}}
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="form-group mt-5">
        <a href="javascript:void(0);" data-repeater-create class="btn btn-light-primary">
            <i class="la la-plus"></i>{{__('JsPress::backend.add')}}
        </a>
    </div>
</div>
@push('script')
    <script>
        var validatorField = {
            validators: {
                notEmpty: {
                    message: 'The field is required',
                },
            },
        };
        var options = {
            initEmpty: false,
            show: function () {
                $(this).slideDown();
            },
            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        };
        var values = @json($values);
        var repeater = $('#{{ $id }}').repeater(options);
        if( values.length > 0 ){
            repeater.setList(values);
        }
    </script>
@endpush
