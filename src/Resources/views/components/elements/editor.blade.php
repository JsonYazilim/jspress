<div id="{{$id}}">
    <div class="form-group tinmy_editor_container p-2">
        @if(!is_null($label))
            <label class="{{$required ? 'required':''}} form-label">{{ $label }}</label>
        @endif
        <textarea data-control="tinymce" data-name="{{$name}}" class="form-control tox-target mb-2 {{$classes}}" {{$required == 1 ? 'required':''}}>{!! $value !!}</textarea>
        <textarea name="{{$name}}" class="tinymce_inner" style="display:none;">{!! $value !!}</textarea>
    </div>
</div>
@push('script')
    <script>
        var options = {
            selector: '[data-control="tinymce"]',
            plugins: 'image media code imagetools pagebreak anchor fullscreen link lists print table',
            language: window._lang ?? 'tr',
            init_instance_callback : function(editor) {
                var id = editor.id;
                var value = document.getElementById(id).closest('.tinmy_editor_container').querySelector('.tinymce_inner').value;
                editor.setContent(value);
            },
            setup: function(ed) {
                var element = this.id;
                ed.on('keyup change', function(e) {
                    document.getElementById(element).closest('.tinmy_editor_container').querySelector('.tinymce_inner').value = ed.getContent();
                });
            }
        };

        if (KTApp.isDarkMode()) {
            options["skin"] = "oxide-dark";
            options["content_css"] = "dark";
        }

        tinymce.init(options);
    </script>
@endpush
