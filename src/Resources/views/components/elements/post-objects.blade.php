<div id="{{$id}}">
    <div class="form-group p-2 w-100">
        @if(!is_null($label))
            <label class="form-label">{{ $label }}</label>
        @endif
        <select name="{{$name}}" class="form-select mb-2 {{$classes}}" {{ $multiple == 1 ? 'multiple' :'' }} data-control="select2" {{$required ? 'required':''}}>
            <option></option>
            @foreach($options as $option)
                <option value="{{$option['value']}}" {{ $isSelected($option['value']) ? 'selected' : '' }}>{{$option['label']}}</option>
            @endforeach
        </select>
    </div>
</div>
