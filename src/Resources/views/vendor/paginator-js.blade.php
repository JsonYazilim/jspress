@if ($paginator->hasPages())
    <nav class="d-flex justify-items-center justify-content-between">
        <div class="d-none flex-sm-fill d-sm-flex align-items-sm-center justify-content-center">
            <div>
                <ul id="paginator" class="pagination">
                    <li class="page-item">
                        <a class="page-link prev_element" href="javascript:void(0);" data-page="1" rel="prev">&lsaquo;</a>
                    </li>
                    @foreach ($elements as $element)
                        @if (is_string($element))
                            <li class="page-item disabled" aria-disabled="true">
                                <span class="page-link">{{ $element }}</span>
                            </li>
                        @endif
                        @if (is_array($element))
                            @foreach ($element as $page => $url)
                                @if ($page == $paginator->currentPage())
                                    <li class="page-item active" aria-current="page">
                                        <a class="page-link" href="javascript:void(0);" data-page="{{ $page }}">{{ $page }}</a>
                                    </li>
                                @else
                                    <li class="page-item">
                                        <a class="page-link" href="javascript:void(0);" data-page="{{ $page }}">{{ $page }}</a>
                                    </li>
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                    <li class="page-item">
                        <a class="page-link next_element" href="javascript:void(0);" data-page="2" rel="next" aria-label="@lang('pagination.next')">&rsaquo;</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
@endif
