@extends('JsPress::admin.layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{__('JsPress::backend.pages.translations.title')}}</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-fluid">
                @if(\Session::has('success'))
                    @include('JsPress::admin.layouts.alerts.success', ['message'=>session('success')])
                @endif
                <div class="card mb-5 mb-xl-10">
                    <div class="card-header  border-0 pt-6">
                        <div class="card-title">
                            <form action="">
                                <div class="row">
                                    <div class="col-lg-3 my-2">
                                        <div class="d-flex align-items-center">
                                            <span class="svg-icon svg-icon-1 position-absolute ms-6">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                    <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black"></rect>
                                                    <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black"></path>
                                                </svg>
                                            </span>
                                            <input type="text" name="q" class="form-control form-control-solid ps-14" placeholder="{{__('JsPress::backend.query_key')}}" value="{{request()->get('q')}}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 my-2">
                                        <div class="d-flex align-items-center">
                                            <span class="svg-icon svg-icon-1 position-absolute ms-6">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                    <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black"></rect>
                                                    <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black"></path>
                                                </svg>
                                            </span>
                                            <input type="text" name="group_key" class="form-control form-control-solid ps-14" placeholder="{{__('JsPress::backend.pages.translations.translation_key')}}" value="{{request()->get('group_key')}}">
                                        </div>
                                    </div>
                                    <div class="col-lg-3 my-2">
                                        <select class="form-select form-select-solid fw-bolder" name="group" data-kt-select2="true" data-placeholder="{{ __('JsPress::backend.pages.translations.table.select_translation_group') }}" data-allow-clear="true" data-kt-user-table-filter="role" data-hide-search="true">
                                            <option></option>
                                            @foreach($translation_groups as $group)
                                                <option value="{{$group->group}}" {{request()->has('group') && request()->get('group') == $group->group ? 'selected':''}}>{{strtoupper($group->group)}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-3 my-2">
                                        <div class="d-flex">
                                            <button type="submit" class="btn btn-light-primary pl-5 me-4">{{__('JsPress::backend.search')}}</button>
                                            <a href="{{ request()->url() }}" class="btn btn-light-danger pl-5">{{__('JsPress::backend.clear')}}</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card-toolbar">
                            <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                <button type="button" class="btn btn-light-success me-3" data-bs-toggle="modal" data-bs-target="#import_translation_modal">
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.3" x="12.75" y="4.25" width="12" height="2" rx="1" transform="rotate(90 12.75 4.25)" fill="black" />
                                            <path d="M12.0573 6.11875L13.5203 7.87435C13.9121 8.34457 14.6232 8.37683 15.056 7.94401C15.4457 7.5543 15.4641 6.92836 15.0979 6.51643L12.4974 3.59084C12.0996 3.14332 11.4004 3.14332 11.0026 3.59084L8.40206 6.51643C8.0359 6.92836 8.0543 7.5543 8.44401 7.94401C8.87683 8.37683 9.58785 8.34458 9.9797 7.87435L11.4427 6.11875C11.6026 5.92684 11.8974 5.92684 12.0573 6.11875Z" fill="black" />
                                            <path d="M18.75 8.25H17.75C17.1977 8.25 16.75 8.69772 16.75 9.25C16.75 9.80228 17.1977 10.25 17.75 10.25C18.3023 10.25 18.75 10.6977 18.75 11.25V18.25C18.75 18.8023 18.3023 19.25 17.75 19.25H5.75C5.19772 19.25 4.75 18.8023 4.75 18.25V11.25C4.75 10.6977 5.19771 10.25 5.75 10.25C6.30229 10.25 6.75 9.80228 6.75 9.25C6.75 8.69772 6.30229 8.25 5.75 8.25H4.75C3.64543 8.25 2.75 9.14543 2.75 10.25V19.25C2.75 20.3546 3.64543 21.25 4.75 21.25H18.75C19.8546 21.25 20.75 20.3546 20.75 19.25V10.25C20.75 9.14543 19.8546 8.25 18.75 8.25Z" fill="#C4C4C4" />
                                        </svg>
                                    </span>{{__('JsPress::backend.import')}}
                                </button>
                                <a href="{{Route('admin.translation.export', ['locale'=>$language->locale])}}" class="btn btn-light-primary me-3">
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.3" x="12.75" y="4.25" width="12" height="2" rx="1" transform="rotate(90 12.75 4.25)" fill="black" />
                                            <path d="M12.0573 6.11875L13.5203 7.87435C13.9121 8.34457 14.6232 8.37683 15.056 7.94401C15.4457 7.5543 15.4641 6.92836 15.0979 6.51643L12.4974 3.59084C12.0996 3.14332 11.4004 3.14332 11.0026 3.59084L8.40206 6.51643C8.0359 6.92836 8.0543 7.5543 8.44401 7.94401C8.87683 8.37683 9.58785 8.34458 9.9797 7.87435L11.4427 6.11875C11.6026 5.92684 11.8974 5.92684 12.0573 6.11875Z" fill="black" />
                                            <path d="M18.75 8.25H17.75C17.1977 8.25 16.75 8.69772 16.75 9.25C16.75 9.80228 17.1977 10.25 17.75 10.25C18.3023 10.25 18.75 10.6977 18.75 11.25V18.25C18.75 18.8023 18.3023 19.25 17.75 19.25H5.75C5.19772 19.25 4.75 18.8023 4.75 18.25V11.25C4.75 10.6977 5.19771 10.25 5.75 10.25C6.30229 10.25 6.75 9.80228 6.75 9.25C6.75 8.69772 6.30229 8.25 5.75 8.25H4.75C3.64543 8.25 2.75 9.14543 2.75 10.25V19.25C2.75 20.3546 3.64543 21.25 4.75 21.25H18.75C19.8546 21.25 20.75 20.3546 20.75 19.25V10.25C20.75 9.14543 19.8546 8.25 18.75 8.25Z" fill="#C4C4C4" />
                                        </svg>
                                    </span>{{__('JsPress::backend.export')}}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body py-4">
                        <div class="table-responsive">
                            <table class="table align-middle table-row-bordered table-row-solid gy-4 gs-9" id="translation_table">
                                <thead>
                                    <tr class="fw-bolder text-muted">
                                        <th class="mw-80px">{{__('JsPress::backend.pages.translations.table.translation_group_key')}}</th>
                                        <th class="mw-100px">{{__('JsPress::backend.pages.translations.table.translation_key')}}</th>
                                        <th class="mw-150px">{{__('JsPress::backend.pages.translations.table.default_translation')}}</th>
                                        <th class="min-w-200px">{{__('JsPress::backend.pages.translations.table.translation')}}</th>
                                    </tr>
                                </thead>
                                <tbody class="fw-bolder text-gray-600">
                                    @foreach($translations as $translation)
                                    <tr class="odd" data-id="{{ $translation->id }}">
                                        <td>{{$translation->group}}</td>
                                        <td>{{$translation->key}}</td>
                                        <td class="mw-150px fw-light">{{$translation->default_value}}</td>
                                        <td class="translate_td">
                                            <textarea class="form-control form-control-solid mb-2 translateString" data-id="{{$translation->id}}" data-locale="{{$language->locale}}" data-group="{{ $translation->group }}" data-key="{{ $translation->key }}" data-kt-autosize="true">{{$translation->value}}</textarea>
                                            <div class="h-15px message"></div>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        {!! $translations->withQueryString()->links() !!}
                    </div>
                </div>
            </div>
            @include('JsPress::admin.modals.translation-import', ['locale' => $language->locale])
        </div>
    </div>
@endsection
@push('script')
    <script>

        $('[data-control="select2"]').on('change', function(){
            let val = $(this).val();
            window.location.href = window.location.href+'?group='+val;
        });

        $('.translateString').on('change', function(){
            let data = {
                id: $(this).attr('data-id'),
                locale: $(this).attr('data-locale'),
                group: $(this).attr('data-group'),
                key: $(this).attr('data-key'),
                value:$(this).val()
            };
            $('#translation_table tbody tr[data-id="'+data.id+'"] td.translate_td div.message small').remove();
            $.ajax({
                url:'{{Route('admin.translation.store')}}',
                method:'POST',
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:{
                    data:data
                },
                dataType:'json',
                success:function(res){
                    if (res.status === 1) {
                        $('#translation_table tbody tr[data-id="'+data.id+'"] td.translate_td div.message').append('<small class="text-success">'+res.msg+'</small>');
                    } else {
                        Swal.fire({
                            text: res.msg,
                            icon: "error",
                            buttonsStyling: !1,
                            confirmButtonText: "{!! __('JsPress::backend.confirmButton') !!}",
                            customClass: {confirmButton: "btn btn-primary"}
                        }).then(
                            function (t) {
                            }
                        );
                    }
                }
            })
        });
    </script>
@endpush


