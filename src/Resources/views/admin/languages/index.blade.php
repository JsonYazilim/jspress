@extends('JsPress::admin.layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{__('JsPress::backend.pages.languages.title')}}</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                <div class="card card-flush">
                    <div class="card-header pt-7">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label fw-bolder text-dark">{{__('JsPress::backend.pages.languages.list.title')}}</span>
                            <span class="text-gray-400 mt-1 fw-bold fs-6">{{__('JsPress::backend.pages.languages.list.description')}}</span>
                        </h3>
                    </div>
                    <div class="card-body pt-7 py-7">
                        <table class="table table-striped align-middle table-row-dashed fs-6 gy-5" id="language_table">
                            <thead>
                            <tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
                                <th class="min-w-100px">ID</th>
                                <th class="min-w-250px">{{__('JsPress::backend.pages.languages.table.language')}}</th>
                                <th class="min-w-125px">{{__('JsPress::backend.pages.languages.table.code')}}</th>
                                <th class="min-w-125px">{{__('JsPress::backend.pages.languages.table.status')}}</th>
                                <th class="text-end min-w-100px">{{__('JsPress::backend.actions')}}</th>
                            </tr>
                            </thead>
                            <tbody class="text-gray-600 fw-bold">
                            @foreach( $languages as $language )
                                <tr>
                                    <td>
                                        <span class="m--font-bold m--font-danger">#{!! $language->id !!}</span>
                                    </td>
                                    <td>
                                        <img src="{{asset('jspress/assets/media/flags/svg/'.$language->locale.'.svg')}}" width="23" />
                                        <span class="m--font-bold m--font-danger">{!! $language->english_name !!}</span>
                                        @if( $language->is_default == 1 )
                                            <span class="badge badge-light-success">{{__('JsPress::backend.default')}}</span>
                                        @endif
                                    </td>
                                    <td>
                                        <span class="m--font-bold m--font-danger">{!! $language->locale !!}</span>
                                    </td>
                                    <td>
                                        @if( $language->status == 1 )
                                            <span class="badge badge-light-success">{{ __('JsPress::backend.active') }}</span>
                                        @else
                                            <span class="badge badge-light-danger">{{ __('JsPress::backend.deactive') }}</span>
                                        @endif
                                    </td>
                                    <td class="text-end">
                                        @if($language->is_default != 1 && $language->status == 1)
                                            <button class="btn btn-icon btn-bg-light btn-active-color-dark btn-sm me-1 setDefaultLanguage" data-id="{!! $language->id !!}" data-bs-toggle="tooltip" data-bs-placement="top" title="{{ __('JsPress::backend.pages.languages.table.set_as_default') }}">
                                                <span class="svg-icon svg-icon-3 svg-icon-dark">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path d="M17.1 15.8C16.6 15.7 16 16 15.9 16.5C15.7 17.4 14.9 18 14 18H6C4.9 18 4 17.1 4 16V8C4 6.9 4.9 6 6 6H14C15.1 6 16 6.9 16 8V9.4H18V8C18 5.8 16.2 4 14 4H6C3.8 4 2 5.8 2 8V16C2 18.2 3.8 20 6 20H14C15.8 20 17.4 18.8 17.9 17.1C17.9 16.5 17.6 16 17.1 15.8Z" fill="black"/>
                                                        <path opacity="0.3" d="M11.9 9.39999H21.9L17.6 13.7C17.2 14.1 16.6 14.1 16.2 13.7L11.9 9.39999Z" fill="black"/>
                                                    </svg>
                                                </span>
                                            </button>
                                        @endif
                                        @if($language->status != 1)
                                            <button class="btn btn-icon btn-bg-light btn-active-color-success btn-sm me-1 toggleLanguage" data-id="{!! $language->id !!}" data-status="{!! $language->status !!}" data-bs-toggle="tooltip" data-bs-placement="top" title="{{ __('JsPress::backend.pages.languages.table.active_langauge') }}">
                                                    <span class="svg-icon svg-icon-3 svg-icon-success">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-power" viewBox="0 0 16 16">
                                                          <path d="M7.5 1v7h1V1h-1z"/>
                                                          <path d="M3 8.812a4.999 4.999 0 0 1 2.578-4.375l-.485-.874A6 6 0 1 0 11 3.616l-.501.865A5 5 0 1 1 3 8.812z"/>
                                                        </svg>
                                                    </span>
                                            </button>
                                        @else
                                            <button class="btn btn-icon btn-bg-light btn-active-color-danger btn-sm me-1 toggleLanguage" data-id="{!! $language->id !!}" data-status="{!! $language->status !!}" data-bs-toggle="tooltip" data-bs-placement="top" title="{{ __('JsPress::backend.pages.languages.table.de_active_language') }}">
                                                    <span class="svg-icon svg-icon-3 svg-icon-danger">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-power" viewBox="0 0 16 16">
                                                          <path d="M7.5 1v7h1V1h-1z"/>
                                                          <path d="M3 8.812a4.999 4.999 0 0 1 2.578-4.375l-.485-.874A6 6 0 1 0 11 3.616l-.501.865A5 5 0 1 1 3 8.812z"/>
                                                        </svg>
                                                    </span>
                                            </button>
                                            <a href="{!! Route('admin.translation.index', ['locale'=>$language->locale]) !!}" class="btn btn-icon btn-bg-light btn-active-color-success btn-sm me-1" data-bs-toggle="tooltip" data-bs-placement="top" title="{{ __('JsPress::backend.pages.languages.table.translation_management') }}">
                                                <span class="svg-icon svg-icon-3 svg-icon-success">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-translate" viewBox="0 0 16 16">
                                                      <path d="M4.545 6.714 4.11 8H3l1.862-5h1.284L8 8H6.833l-.435-1.286H4.545zm1.634-.736L5.5 3.956h-.049l-.679 2.022H6.18z"/>
                                                      <path d="M0 2a2 2 0 0 1 2-2h7a2 2 0 0 1 2 2v3h3a2 2 0 0 1 2 2v7a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2v-3H2a2 2 0 0 1-2-2V2zm2-1a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H2zm7.138 9.995c.193.301.402.583.63.846-.748.575-1.673 1.001-2.768 1.292.178.217.451.635.555.867 1.125-.359 2.08-.844 2.886-1.494.777.665 1.739 1.165 2.93 1.472.133-.254.414-.673.629-.89-1.125-.253-2.057-.694-2.82-1.284.681-.747 1.222-1.651 1.621-2.757H14V8h-3v1.047h.765c-.318.844-.74 1.546-1.272 2.13a6.066 6.066 0 0 1-.415-.492 1.988 1.988 0 0 1-.94.31z"/>
                                                    </svg>
                                                </span>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        window._activate_language = "{{__('JsPress::backend.pages.languages.table.activate_language_warning')}}";
        window._de_activate_language = "{{__('JsPress::backend.pages.languages.table.de_activate_language_warning')}}";
        $("#language_table").DataTable({
            order: [[3, 'asc']],
            searching: true,
            language:{
                url:"{!! asset('jspress/assets/js/languages/datatable/'.app()->getLocale().'.json') !!}"
            }
        });
        $('.setDefaultLanguage').on('click', function(){
            let id = $(this).attr('data-id');
            Swal.fire({
                text: "{{__('JsPress::backend.pages.languages.table.verification_default_language')}}",
                icon: "warning",
                showCancelButton: true,
                buttonsStyling: true,
                confirmButtonText: "{!! __('JsPress::backend.yes_i_confirm') !!}",
                cancelButtonText: "{!! __('JsPress::backend.no_return') !!}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-active-light"
                },
            }).then(
                function (t) {
                    if( t.isConfirmed ){
                        $.ajax({
                            url:'{{Route('admin.language.default')}}',
                            type:'POST',
                            headers:{
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data:{
                                'language_id':id
                            },
                            dataType:'json',
                            success:function(res){
                                successTemplate(res);
                            }
                        });
                    }
                }
            );
        });

        $('.toggleLanguage').on('click', function(){
            let id = $(this).attr('data-id'), status = parseInt($(this).attr('data-status'));
            Swal.fire({
                text: status === 1 ? window._de_activate_language:window._activate_language,
                icon: "warning",
                showCancelButton: true,
                buttonsStyling: true,
                confirmButtonText: "{!! __('JsPress::backend.yes_i_confirm') !!}",
                cancelButtonText: "{!! __('JsPress::backend.no_return') !!}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-active-light"
                },
            }).then(
                function (t) {
                    if( t.isConfirmed ){
                        $.ajax({
                            url:'{{Route('admin.language.toggle')}}',
                            type:'POST',
                            headers:{
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data:{
                                'language_id':id,
                                'status':status
                            },
                            dataType:'json',
                            success:function(res){
                                successTemplate(res);
                            }
                        });
                    }
                }
            );
        });

        const successTemplate = function (res) {
            if (res.status === 1) {
                Swal.fire({
                    text: res.msg,
                    icon: "success",
                    buttonsStyling: !1,
                    confirmButtonText: "{!! __('JsPress::backend.confirmButton') !!}",
                    customClass: {confirmButton: "btn btn-primary"}
                }).then(
                    function (t) {
                        location.reload();
                    }
                );
            } else {
                Swal.fire({
                    text: res.msg,
                    icon: "error",
                    buttonsStyling: !1,
                    confirmButtonText: "{!! __('JsPress::backend.confirmButton') !!}",
                    customClass: {confirmButton: "btn btn-primary"}
                }).then(
                    function (t) {
                    }
                );
            }
        };
    </script>
@endpush


