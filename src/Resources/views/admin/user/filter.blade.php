<div class="menu menu-sub menu-sub-dropdown w-300px w-md-325px" data-kt-menu="true">
    <div class="px-7 py-5">
        <div class="fs-5 text-dark fw-bolder">{!! __('JsPress::backend.pages.users.filter_options') !!}</div>
    </div>
    <div class="separator border-gray-200"></div>
    <div class="px-7 py-5" data-kt-user-table-filter="form">
        <form action="{!! Route('admin.user.list') !!}">
            <div class="mb-10">
                <label class="form-label fs-6 fw-bold">{!! __('JsPress::backend.status') !!}:</label>
                <select class="form-select form-select-solid fw-bolder" name="status" data-kt-select2="true" data-placeholder="{!! __('JsPress::backend.pages.users.select_option') !!}" data-allow-clear="true" data-hide-search="true">
                    <option></option>
                    <option value="1" {!! \Request::get('status') == 1 ? 'selected':'' !!}>{!! __('JsPress::backend.verified') !!}</option>
                    <option value="2" {!! \Request::get('status') == 2 ? 'selected':'' !!}>{!! __('JsPress::backend.non_verified') !!}</option>
                </select>
            </div>
            <div class="d-flex justify-content-end">
                <button type="button" onClick="location.href='{!! Route('admin.user.list') !!}'" class="btn btn-light btn-active-light-primary fw-bold me-2 px-6">{!! __('JsPress::backend.pages.users.reset') !!}</button>
                <button type="submit" class="btn btn-primary fw-bold px-6" data-kt-menu-dismiss="true" data-kt-user-table-filter="filter">{!! __('JsPress::backend.pages.users.apply') !!}</button>
            </div>
        </form>
    </div>
</div>
