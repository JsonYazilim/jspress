<div class="modal fade" id="kt_modal_add_user" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content">
            <div class="modal-header" id="kt_modal_add_user_header">
                <h2 class="fw-bolder">{!! __('JsPress::backend.pages.users.add.add_user') !!}</h2>
                <div class="btn btn-icon btn-sm btn-active-icon-primary" data-kt-users-modal-action="close">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
                <form id="kt_modal_add_user_form" class="form" autocomplete="off" autofocus="off" enctype="multipart/form-data">
                    <div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_user_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_user_header" data-kt-scroll-wrappers="#kt_modal_add_user_scroll" data-kt-scroll-offset="300px">
                        <div class="fv-row mb-7">
                            <label class="d-block fw-bold fs-6 mb-5">{!! __('JsPress::backend.pages.users.add.profile_image') !!}</label>
                            <div class="image-input image-input-outline image-input-empty" data-kt-image-input="true" style="background-image: url('{!! asset('jspress/assets/media/avatars/blank.png') !!}')">
                                <div class="image-input-wrapper w-125px h-125px" style="background-image: none;"></div>
                                <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="{!! __('JsPress::backend.pages.users.add.upload_image') !!}">
                                    <i class="bi bi-pencil-fill fs-7"></i>
                                    <input type="file" name="avatar" class="form-control" accept=".png, .jpg, .jpeg, .gif" />
                                </label>
                                <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="{!! __('JsPress::backend.pages.users.add.remove_image') !!}">
                                    <i class="bi bi-x fs-2"></i>
                                </span>
                            </div>
                            <div class="form-text">{!! __('JsPress::backend.pages.users.add.allowed_mimes') !!}</div>
                        </div>
                        <div class="fv-row row mb-7">
                            <div class="col-lg-6">
                                <label class="required fw-bold fs-6 mb-2">{!! __('JsPress::backend.pages.users.first_name') !!}</label>
                                <input type="text" name="first_name" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="{!! __('JsPress::backend.pages.users.first_name') !!}"/>
                            </div>
                            <div class="col-lg-6">
                                <label class="required fw-bold fs-6 mb-2">{!! __('JsPress::backend.pages.users.last_name') !!}</label>
                                <input type="text" name="last_name" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="{!! __('JsPress::backend.pages.users.last_name') !!}"/>
                            </div>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="required fw-bold fs-6 mb-2">{!! __('JsPress::backend.pages.users.email') !!}</label>
                            <input type="email" name="email" class="form-control form-control-solid mb-3 mb-lg-0" placeholder="example@domain.com" />
                        </div>
                        <div class="fv-row row mb-7">
                            <div class="col-lg-6">
                                <label class="required fw-bold fs-6 mb-2">{!! __('JsPress::backend.pages.users.password') !!}</label>
                                <input type="password" name="password" class="form-control form-control-solid mb-3 mb-lg-0"/>
                            </div>
                            <div class="col-lg-6">
                                <label class="required fw-bold fs-6 mb-2">{!! __('JsPress::backend.pages.users.confirm_password') !!}</label>
                                <input type="password" name="confirmPassword" class="form-control form-control-solid mb-3 mb-lg-0"/>
                            </div>
                        </div>
                    </div>
                    <div class="text-center pt-15">
                        <button type="reset" class="btn btn-light me-3" data-kt-users-modal-action="cancel">{!! __('JsPress::backend.form.discard') !!}</button>
                        <button type="submit" class="btn btn-primary" data-kt-users-modal-action="submit">
                            <span class="indicator-label">{!! __('JsPress::backend.form.submit') !!}</span>
                            <span class="indicator-progress">{!! __('JsPress::backend.form.please_wait') !!}
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@push('script')
    <script>
        "use strict";
        var KTUsersAddUser = (function () {
            const t = document.getElementById("kt_modal_add_user"),
                e = t.querySelector("#kt_modal_add_user_form"),
                n = new bootstrap.Modal(t);
            return {
                init: function () {
                    (() => {
                        var o = FormValidation.formValidation(e, {
                            fields: {
                                avatar: {
                                    validators: {
                                        file: {
                                            extension: 'jpeg,jpg,png',
                                            type: 'image/jpeg,image/png',
                                            maxSize: 2097152, // 2048 * 1024
                                            message: 'The selected file is not valid',
                                        },
                                    },
                                },
                                first_name: {
                                    validators: {
                                        notEmpty: {
                                            message: "{!! __('JsPress::backend.form.validation.users.required.first_name') !!}"
                                        }
                                    }
                                },
                                last_name: {
                                    validators: {
                                        notEmpty: {
                                            message: "{!! __('JsPress::backend.form.validation.users.required.last_name') !!}"
                                        }
                                    }
                                },
                                email: {
                                    validators: {
                                        notEmpty: {
                                            message: "{!! __('JsPress::backend.form.validation.users.required.email') !!}"
                                        },
                                        emailAddress: {
                                            message: '{!! __('JsPress::backend.form.validation.users.email') !!}',
                                        },
                                        remote: {
                                            message: '{!! __('JsPress::backend.form.validation.users.confirm.exist') !!}',
                                            method: 'POST',
                                            url: '{!! Route('admin.user.checkEmail') !!}',
                                            headers:{
                                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                            }
                                        }
                                    }
                                },
                                password: {
                                    validators: {
                                        notEmpty: {
                                            message: "{!! __('JsPress::backend.form.validation.users.required.password') !!}"
                                        }
                                    }
                                },
                                confirmPassword: {
                                    validators: {
                                        identical: {
                                            compare: function () {
                                                return t.querySelector('[name="password"]').value;
                                            },
                                            message: '{!! __('JsPress::backend.form.validation.users.confirm.password') !!}',
                                        },
                                    },
                                }
                            },
                            plugins: { trigger: new FormValidation.plugins.Trigger(), bootstrap: new FormValidation.plugins.Bootstrap5({ rowSelector: ".fv-row", eleInvalidClass: "", eleValidClass: "" }) },
                        });
                        const i = t.querySelector('[data-kt-users-modal-action="submit"]');
                        i.addEventListener("click", (t) => {
                            t.preventDefault(),
                            o &&
                            o.validate().then(function (t) {
                                if( t == "Valid" ){
                                    i.setAttribute("data-kt-indicator", "on");
                                    i.disabled = !0;
                                    var formData = new FormData($('#kt_modal_add_user_form')[0]);
                                    $.ajax({
                                        url: '{!! Route('admin.user.store') !!}',
                                        type:'POST',
                                        data:formData,
                                        processData: false,
                                        contentType: false,
                                        headers:{
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        success:function(res){
                                            if(res.status == 1){
                                                i.disabled = !1;
                                                i.setAttribute("data-kt-indicator", "off");
                                                Swal.fire({ text: res.msg, icon: "success", buttonsStyling: !1, confirmButtonText: "{!! __('JsPress::backend.pages.users.add.confirmButton') !!}", customClass: { confirmButton: "btn btn-primary" } }).then(
                                                    function (t) {
                                                        t.isConfirmed && n.hide();
                                                        location.reload();
                                                    }
                                                );
                                            }else{
                                                i.disabled = false;
                                                i.setAttribute("data-kt-indicator", "off");
                                                Swal.fire({ text: res.msg, icon: "error", buttonsStyling: !1, confirmButtonText: "{!! __('JsPress::backend.pages.users.add.confirmButton') !!}", customClass: { confirmButton: "btn btn-primary" } }).then(
                                                    function (t) {

                                                    }
                                                );
                                            }
                                        },
                                        error:function(request, status, error){
                                            Swal.fire({
                                                text: "{!! __('JsPress::backend.pages.users.add.error') !!}",
                                                icon: "error",
                                                buttonsStyling: !1,
                                                confirmButtonText: "{!! __('JsPress::backend.pages.users.add.confirmButton') !!}",
                                                customClass: { confirmButton: "btn btn-primary" },
                                            }).then(function(){
                                                i.setAttribute("data-kt-indicator", "off");
                                                i.disabled = !1;
                                            });
                                        }
                                    });
                                }else{
                                    Swal.fire({
                                        text: "{!! __('JsPress::backend.pages.users.add.error') !!}",
                                        icon: "error",
                                        buttonsStyling: !1,
                                        confirmButtonText: "{!! __('JsPress::backend.pages.users.add.confirmButton') !!}",
                                        customClass: { confirmButton: "btn btn-primary" },
                                    });
                                }
                            });
                        }),
                            t.querySelector('[data-kt-users-modal-action="cancel"]').addEventListener("click", (t) => {
                                t.preventDefault(),
                                    Swal.fire({
                                        text: "{!! __('JsPress::backend.pages.users.add.cancel') !!}",
                                        icon: "warning",
                                        showCancelButton: !0,
                                        buttonsStyling: !1,
                                        confirmButtonText: "{!! __('JsPress::backend.pages.users.add.cancel_yes') !!}",
                                        cancelButtonText: "{!! __('JsPress::backend.pages.users.add.cancel_no') !!}",
                                        customClass: { confirmButton: "btn btn-primary", cancelButton: "btn btn-active-light" },
                                    }).then(function (t) {
                                        t.value
                                            ? (e.reset(), n.hide())
                                            : "cancel" === t.dismiss &&
                                            Swal.fire({ text: "{!! __('JsPress::backend.pages.users.add.cancelled') !!}", icon: "error", buttonsStyling: !1, confirmButtonText: "{!! __('JsPress::backend.pages.users.add.confirmButton') !!}", customClass: { confirmButton: "btn btn-primary" } });
                                    });
                            }),
                            t.querySelector('[data-kt-users-modal-action="close"]').addEventListener("click", (t) => {
                                t.preventDefault(),
                                    Swal.fire({
                                        text: "{!! __('JsPress::backend.pages.users.add.cancel') !!}",
                                        icon: "warning",
                                        showCancelButton: !0,
                                        buttonsStyling: !1,
                                        confirmButtonText: "{!! __('JsPress::backend.pages.users.add.cancel_yes') !!}",
                                        cancelButtonText: "{!! __('JsPress::backend.pages.users.add.cancel_no') !!}",
                                        customClass: { confirmButton: "btn btn-primary", cancelButton: "btn btn-active-light" },
                                    }).then(function (t) {
                                        t.value
                                            ? (e.reset(), n.hide())
                                            : "cancel" === t.dismiss &&
                                            Swal.fire({ text: "{!! __('JsPress::backend.pages.users.add.cancelled') !!}.", icon: "error", buttonsStyling: !1, confirmButtonText: "{!! __('JsPress::backend.pages.users.add.confirmButton') !!}", customClass: { confirmButton: "btn btn-primary" } });
                                    });
                            });
                    })();
                },
            };
        })();
        KTUtil.onDOMContentLoaded(function () {
            KTUsersAddUser.init();
        });
    </script>
@endpush
