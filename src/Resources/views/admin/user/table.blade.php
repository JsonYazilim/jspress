<div class="table-responsive">
    <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_table_users">
        <thead>
        <tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
            <th class="w-10px pe-2">
                <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                    <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_table_users .form-check-input" value="1" />
                </div>
            </th>
            <th class="min-w-125px">{!! __('JsPress::backend.pages.users.table.user') !!}</th>
            <th class="min-w-125px">{!! __('JsPress::backend.pages.users.table.verification') !!}</th>
            <th class="min-w-125px">{!! __('JsPress::backend.pages.users.table.status') !!}</th>
            <th class="min-w-125px">{!! __('JsPress::backend.pages.users.table.registered_date') !!}</th>
            <th class="text-end min-w-100px">{!! __('JsPress::backend.actions') !!}</th>
        </tr>
        </thead>
        <tbody class="text-gray-600 fw-bold">
        @if( $users->isNotEmpty() )
            @foreach( $users as $user )
                <tr class="user-row" data-id="{!! $user->id !!}">
                    <td>
                        <div class="form-check form-check-sm form-check-custom form-check-solid">
                            <input class="form-check-input" name="user_id" type="checkbox" value="{!! $user->id !!}" />
                        </div>
                    </td>
                    <td class="d-flex align-items-center">
                        <div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
                            <a href="#">
                                <div class="symbol-label">
                                    @if( $user->meta->avatar != NULL )
                                        <img src="{!! asset('uploads/avatar/'.$user->meta->avatar) !!}" alt="{!! strip_tags($user->name) !!}" class="w-100" />
                                    @else
                                        <img src="{!! asset('jspress/assets/media/avatars/blank.png') !!}" alt="{!! strip_tags($user->name) !!}" class="w-100" />
                                    @endif
                                </div>
                            </a>
                        </div>
                        <div class="d-flex flex-column">
                            <a href="#" class="text-gray-800 text-hover-primary mb-1">{!! $user->name !!}</a>
                            <span>{!! $user->email !!}</span>
                        </div>
                    </td>
                    <td class="verify">
                        @if( $user->email_verified_at == NULL )
                            <div class="badge badge-light-danger">{!! __('JsPress::backend.non_verified') !!}</div>
                        @else
                            <div class="badge badge-light-success">{!! __('JsPress::backend.verified') !!}</div>
                        @endif
                    </td>
                    <td>
                        @if( $user->status == 1 )
                            <span class="text-success">{!! __('JsPress::backend.active') !!}</span>
                        @else
                            <span class="text-danger">{!! __('JsPress::backend.deactive') !!}</span>
                        @endif
                    </td>
                    <td>{!! \Carbon\Carbon::parse($user->created_at)->isoFormat('DD MMMM YYYY, ddd') !!}</td>
                    <td class="text-end">
                        <a href="javascript:void(0);" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{!! __('JsPress::backend.actions') !!}
                            <span class="svg-icon svg-icon-5 m-0">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
                            </svg>
                        </span>
                        </a>
                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
                            @if( $user->email_verified_at == NULL )
                                <div class="menu-item px-3">
                                    <a href="javascript:void(0);" class="menu-link px-3 verifyUser" data-id="{!! $user->id !!}">{!! __('JsPress::backend.verify') !!}</a>
                                </div>
                            @endif
                            <div class="menu-item px-3">
                                <a href="{!! Route('admin.user.edit', ['user'=>$user->id]) !!}" class="menu-link px-3">{!! __('JsPress::backend.edit') !!}</a>
                            </div>
                            <div class="menu-item px-3">
                                <a href="javascript:void(0);" class="menu-link px-3 deleteUser" data-id="{!! $user->id !!}">{!! __('JsPress::backend.delete') !!}</a>
                            </div>
                        </div>
                    </td>
                </tr>
            @endforeach
        @else
            <tr class="odd"><td valign="top" colspan="7" class="text-center">{!! __('JsPress::backend.no_result_found') !!}</td></tr>
        @endif
        </tbody>
    </table>
</div>
@push('script')
    <script>
        $('.verifyUser').on('click', function(){
            var id = $(this).attr('data-id');
            console.log('triggered');
            Swal.fire({
                text: "{!! __('JsPress::backend.pages.users.table.verify_user_text') !!}",
                icon: "warning",
                showCancelButton: true,
                buttonsStyling: true,
                confirmButtonText: "{!! __('JsPress::backend.yes_i_confirm') !!}",
                cancelButtonText: "{!! __('JsPress::backend.no_return') !!}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-active-light"
                },
            }).then(
                function (t) {
                    if( t.isConfirmed ){
                        $.ajax({
                            url:'{!! Route('admin.user.verify') !!}',
                            type:'POST',
                            headers:{
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data:{user_id:id},
                            dataType:'json',
                            success:function(res){
                                $('table > tbody > tr.user-row[data-id="'+id+'"] td.verify').html("").append('<div class="badge badge-light-success">{!! __('backend.verified') !!}</div>');
                                $('table > tbody > tr.user-row[data-id="'+id+'"] .verifyUser').closest('.menu-item').remove();
                                toastr.options = {
                                    "closeButton": false,
                                    "debug": false,
                                    "newestOnTop": false,
                                    "progressBar": false,
                                    "positionClass": "toastr-top-right",
                                    "preventDuplicates": false,
                                    "onclick": null,
                                    "showDuration": "300",
                                    "hideDuration": "1000",
                                    "timeOut": "5000",
                                    "extendedTimeOut": "1000",
                                    "showEasing": "swing",
                                    "hideEasing": "linear",
                                    "showMethod": "fadeIn",
                                    "hideMethod": "fadeOut"
                                };
                                toastr.success("{!! __('JsPress::backend.pages.users.table.verify_user_success') !!}");
                            }
                        });
                    }
                }
            );
        });

        $('.deleteUser').on('click', function(){
            let id = $(this).attr('data-id');
            Swal.fire({
                text: "{{__('JsPress::backend.pages.users.delete.delete_confirm_title')}}",
                icon: "warning",
                showCancelButton: true,
                buttonsStyling: true,
                confirmButtonText: "{!! __('JsPress::backend.yes_i_confirm') !!}",
                cancelButtonText: "{!! __('JsPress::backend.no_return') !!}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-active-light"
                },
            }).then(
                function (t) {
                    if( t.isConfirmed ){
                        $.ajax({
                            url:'{!! Route('admin.user.destroy') !!}',
                            type:'POST',
                            headers:{
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data:{user_id:id},
                            dataType:'json',
                            success:function(res){
                                if(res.status == 1){
                                    $('#kt_table_users tbody tr[data-id="'+id+'"]').remove();
                                    toastr.options = {
                                        "closeButton": false,
                                        "debug": false,
                                        "newestOnTop": false,
                                        "progressBar": false,
                                        "positionClass": "toastr-top-right",
                                        "preventDuplicates": false,
                                        "onclick": null,
                                        "showDuration": "300",
                                        "hideDuration": "1000",
                                        "timeOut": "5000",
                                        "extendedTimeOut": "1000",
                                        "showEasing": "swing",
                                        "hideEasing": "linear",
                                        "showMethod": "fadeIn",
                                        "hideMethod": "fadeOut"
                                    };
                                    toastr.success(res.msg);
                                }else{
                                    toastr.options = {
                                        "closeButton": false,
                                        "debug": false,
                                        "newestOnTop": false,
                                        "progressBar": false,
                                        "positionClass": "toastr-top-right",
                                        "preventDuplicates": false,
                                        "onclick": null,
                                        "showDuration": "300",
                                        "hideDuration": "1000",
                                        "timeOut": "5000",
                                        "extendedTimeOut": "1000",
                                        "showEasing": "swing",
                                        "hideEasing": "linear",
                                        "showMethod": "fadeIn",
                                        "hideMethod": "fadeOut"
                                    };
                                    toastr.error(res.msg);
                                }
                            }
                        });
                    }
                }
            );
        });
    </script>
@endpush
