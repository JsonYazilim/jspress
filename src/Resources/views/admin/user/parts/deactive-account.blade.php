<div class="card pt-4 mb-6 mb-xl-9">
    <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_deactivate" aria-expanded="true" aria-controls="kt_account_deactivate">
        <div class="card-title m-0">
            <h3 class="fw-bolder m-0">{!! __('JsPress::backend.pages.users.deactivate_account') !!}</h3>
        </div>
    </div>
    <div id="kt_account_settings_deactivate" class="collapse show">
        <form id="kt_account_deactivate_form" class="form">
            @if( $user->status == 1 )
                <div class="card-body border-top p-9">
                    <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed mb-9 p-6">
                    <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"></rect>
                            <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black"></rect>
                            <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black"></rect>
                        </svg>
                    </span>
                        <div class="d-flex flex-stack flex-grow-1">
                            <div class="fw-bold">
                                <h4 class="text-gray-900 fw-bolder">{!! __('JsPress::backend.pages.users.deactivate_account_text') !!}</h4>
                                <div class="fs-6 text-gray-700">{!! __('JsPress::backend.pages.users.deactivate_account_sub_text') !!}</div>
                            </div>
                        </div>
                    </div>
                    <div class="form-check form-check-solid fv-row fv-plugins-icon-container">
                        <input name="deactivate" class="form-check-input" type="checkbox" value="" id="deactivate">
                        <label class="form-check-label fw-bold ps-2 fs-6" for="deactivate">{!! __('JsPress::backend.pages.users.deactivate_confirm') !!}</label>
                        <div class="fv-plugins-message-container invalid-feedback"></div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <input type="hidden" name="status" value="0"/>
                    <button id="kt_account_deactivate_account_submit" type="button" class="btn btn-danger fw-bold">{!! __('JsPress::backend.pages.users.deactivate_account') !!}</button>
                </div>
            @else
                <div class="card-body border-top p-9">
                    <div class="notice d-flex bg-light-warning rounded border-warning border border-dashed mb-9 p-6">
                    <span class="svg-icon svg-icon-2tx svg-icon-warning me-4">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"></rect>
                            <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black"></rect>
                            <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black"></rect>
                        </svg>
                    </span>
                        <div class="d-flex flex-stack flex-grow-1">
                            <div class="fw-bold">
                                <h4 class="text-gray-900 fw-bolder">{!! __('JsPress::backend.pages.users.activate_account_text') !!}</h4>
                                <div class="fs-6 text-gray-700">{!! __('JsPress::backend.pages.users.activate_account_sub_text') !!}</div>
                            </div>
                        </div>
                    </div>
                    <div class="form-check form-check-solid fv-row fv-plugins-icon-container">
                        <input name="deactivate" class="form-check-input" type="checkbox" value="" id="deactivate">
                        <label class="form-check-label fw-bold ps-2 fs-6" for="deactivate">{!! __('JsPress::backend.pages.users.activate_confirm') !!}</label>
                        <div class="fv-plugins-message-container invalid-feedback"></div>
                    </div>
                </div>
                <div class="card-footer d-flex justify-content-end py-6 px-9">
                    <input type="hidden" name="status" value="1"/>
                    <button id="kt_account_deactivate_account_submit" type="button" class="btn btn-success fw-bold">{!! __('JsPress::backend.pages.users.activate_account') !!}</button>
                </div>
            @endif
        </form>
    </div>
</div>
@push('script')
    <script>
        "use strict";
        var KTAccountSettingsDeactivateAccount = (function () {
            var t, n, e;
            return {
                init: function () {
                    (t = document.querySelector("#kt_account_deactivate_form")),
                        (e = document.querySelector("#kt_account_deactivate_account_submit")),
                        (n = FormValidation.formValidation(t, {
                            fields: { deactivate: { validators: { notEmpty: { message: "{!! $user->status == 1 ? __('JsPress::backend.pages.users.deactivate_confirm_warning'):__('backend.pages.users.activate_confirm_warning') !!}" } } } },
                            plugins: {
                                trigger: new FormValidation.plugins.Trigger(),
                                submitButton: new FormValidation.plugins.SubmitButton(),
                                bootstrap: new FormValidation.plugins.Bootstrap5({ rowSelector: ".fv-row", eleInvalidClass: "", eleValidClass: "" }),
                            },
                        })),
                        e.addEventListener("click", function (t) {
                            t.preventDefault(),
                                n.validate().then(function (t) {
                                    "Valid" == t
                                        ? swal
                                            .fire({
                                                text: "{!! $user->status == 1 ? __('JsPress::backend.pages.users.deactivate_confirm_sure'):__('JsPress::backend.pages.users.activate_confirm_sure') !!}",
                                                icon: "warning",
                                                buttonsStyling: !1,
                                                showDenyButton: !0,
                                                confirmButtonText: "{!! __('JsPress::backend.yes') !!}",
                                                denyButtonText: "{!! __('JsPress::backend.no') !!}",
                                                customClass: { confirmButton: "btn btn-light-primary", denyButton: "btn btn-danger" },
                                            })
                                            .then((t) => {
                                                if( t.isConfirmed ){
                                                    $.ajax({
                                                        url: '{!! Route('admin.user.updateUserStatus', ['user'=>$user->id]) !!}',
                                                        type:'POST',
                                                        data:{
                                                            status:$('input[name="status"]').val()
                                                        },
                                                        headers:{
                                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                                        },
                                                        success:function(res){
                                                            if( res.status == 1 ){
                                                                Swal.fire({ text: res.msg, icon: "success", buttonsStyling: !1, confirmButtonText: "{!! __('JsPress::backend.confirmButton') !!}", customClass: { confirmButton: "btn btn-primary" } }).then(
                                                                    function (t) {
                                                                        t.isConfirmed;
                                                                        location.reload();
                                                                    }
                                                                );
                                                            }else if(res.status == 2){
                                                                Swal.fire({ text: res.msg, icon: "error", buttonsStyling: !1, confirmButtonText: "{!! __('JsPress::backend.confirmButton') !!}", customClass: { confirmButton: "btn btn-primary" } }).then(
                                                                    function (t) {
                                                                    }
                                                                );
                                                            }else{
                                                                Swal.fire({ text: res.msg, icon: "success", buttonsStyling: !1, confirmButtonText: "{!! __('JsPress::backend.confirmButton') !!}", customClass: { confirmButton: "btn btn-primary" } }).then(
                                                                    function (t) {
                                                                        t.isConfirmed;
                                                                        location.reload();
                                                                    }
                                                                );
                                                            }
                                                        }
                                                    })
                                                }else{
                                                    t.isDenied && Swal.fire({ text: "{!! $user->status == 1 ? __('JsPress::backend.pages.users.user_deactivating_cancel'):__('JsPress::backend.pages.users.user_activating_cancel') !!}", icon: "info", confirmButtonText: "{!! __('backend.ok') !!}", buttonsStyling: !1, customClass: { confirmButton: "btn btn-light-primary" } });
                                                }
                                            })
                                        : swal.fire({
                                            text: "{!! __('JsPress::backend.swa_error') !!}",
                                            icon: "error",
                                            buttonsStyling: !1,
                                            confirmButtonText: "{!! __('JsPress::backend.confirmButton') !!}",
                                            customClass: { confirmButton: "btn btn-light-primary" },
                                        });
                                });
                        });
                },
            };
        })();
        KTUtil.onDOMContentLoaded(function () {
            KTAccountSettingsDeactivateAccount.init();
        });
    </script>
@endpush
