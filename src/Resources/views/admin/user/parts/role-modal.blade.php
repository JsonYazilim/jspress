<div class="modal fade" id="kt_modal_update_role" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="fw-bolder">{!! __('JsPress::backend.pages.users.update_user_role') !!}</h2>
                <button type="button" class="btn-close btn-close-white" aria-label="Close" data-bs-dismiss="modal"></button>
            </div>
            <div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
                <form id="kt_modal_update_role_form" class="form">
                    <div class="notice d-flex bg-light-primary rounded border-primary border border-dashed mb-9 p-6">
                        <span class="svg-icon svg-icon-2tx svg-icon-primary me-4">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black" />
                                <rect x="11" y="14" width="7" height="2" rx="1" transform="rotate(-90 11 14)" fill="black" />
                                <rect x="11" y="17" width="2" height="2" rx="1" transform="rotate(-90 11 17)" fill="black" />
                            </svg>
                        </span>
                        <div class="d-flex flex-stack flex-grow-1">
                            <div class="fw-bold">
                                <div class="fs-6 text-gray-700">{!! __('JsPress::backend.pages.users.role_modal_warning') !!}</div>
                            </div>
                        </div>
                    </div>
                    <div class="fv-row mb-7">
                        <div class="d-block">
                            <label class="fs-6 fw-bold form-label mb-5">
                                <span class="required">{!! __('JsPress::backend.pages.users.role') !!}</span>
                            </label>
                        </div>
                        @foreach( $roles as $role )
                            <div class="d-inline-flex">
                                <div class="form-check form-check-custom form-check-solid w-150px">
                                    <input class="form-check-input me-3" name="role" type="radio" value="{!! $role->id !!}" id="kt_modal_update_role_option_{!! $role->id !!}" {!! in_array($role->id, $user->rolePivot()->pluck('id')->toArray() ) ? 'checked':'' !!}/>
                                    <label class="form-check-label me-3" for="kt_modal_update_role_option_{!! $role->id !!}">
                                        <div class="fw-bolder text-gray-800">{!! $role->name !!}</div>
                                    </label>
                                </div>
                            </div>
                        @endforeach
                        <div class="d-inline-flex">
                            <div class="form-check form-check-custom form-check-solid w-100px">
                                <input class="form-check-input me-3" name="role" type="radio" value="default" id="kt_modal_update_role_option_default" {!! !$user->hasAnyRole($roles->pluck('name')->toArray()) ? 'checked':'' !!}/>
                                <label class="form-check-label" for="kt_modal_update_role_option_default">
                                    <div class="fw-bolder text-gray-800">{!! __('backend.pages.users.user') !!}</div>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="text-end">
                        <button type="submit" class="btn btn-primary" data-kt-users-modal-action="submit">
                            <span class="indicator-label">{!! __('JsPress::backend.pages.users.update_role') !!}</span>
                            <span class="indicator-progress">{!! __('JsPress::backend.form.please_wait') !!}
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                            </span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@push('script')
    <script>
        "use strict";
        var KTUsersUpdateRole = (function () {
            const t = document.getElementById("kt_modal_update_role"),
                e = t.querySelector("#kt_modal_update_role_form"),
                n = new bootstrap.Modal(t);
            return {
                init: function () {
                    (() => {
                        const o = t.querySelector('[data-kt-users-modal-action="submit"]');
                        o.addEventListener("click", function (t) {
                            t.preventDefault();
                            o.setAttribute("data-kt-indicator", "on");
                            o.disabled = !0;
                            var formData = new FormData($('#kt_modal_update_role_form')[0]);
                            $.ajax({
                                url: '{!! Route('admin.user.updateUserRole', ['user'=>$user->id]) !!}',
                                type:'POST',
                                data:formData,
                                processData: false,
                                contentType: false,
                                headers:{
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                },
                                success:function(res){
                                    o.disabled = !1;
                                    o.setAttribute("data-kt-indicator", "off");
                                    if(res.status == 1){
                                        Swal.fire({ text: "{!! __('JsPress::backend.pages.users.edit.update_role_success') !!}", icon: "success", buttonsStyling: !1, confirmButtonText: "{!! __('JsPress::backend.confirmButton') !!}", customClass: { confirmButton: "btn btn-primary" } }).then(
                                            function (t) {
                                                t.isConfirmed && n.hide();
                                                location.reload();
                                            }
                                        );
                                    }else{
                                        Swal.fire({ text: res.msg, icon: "error", buttonsStyling: !1, confirmButtonText: "{!! __('JsPress::backend.confirmButton') !!}", customClass: { confirmButton: "btn btn-primary" } }).then(
                                            function (t) {
                                            }
                                        );
                                    }

                                }
                            })
                        });
                    })();
                },
            };
        })();
        KTUtil.onDOMContentLoaded(function () {
            KTUsersUpdateRole.init();
        });

    </script>
@endpush
