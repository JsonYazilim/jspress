<div class="card mb-5 mb-xl-10">
    <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
        <div class="card-title m-0">
            <h3 class="fw-bolder m-0">{{__('JsPress::backend.pages.users.edit.profile_details')}}</h3>
        </div>
    </div>
    <div id="kt_account_settings_profile_details" class="collapse show">
        <form action="{!! route('admin.user.update', ['user'=>$user->id]) !!}" method="POST" class="form" enctype="multipart/form-data">
            @csrf
            <div class="card-body border-top p-9">
                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label fw-bold fs-6">{{__('JsPress::backend.pages.users.edit.profile_image')}}</label>
                    <div class="col-lg-8">
                        <div class="image-input image-input-outline" data-kt-image-input="true">
                            <div class="image-input-wrapper w-125px h-125px" style="background-image: url({!! !is_null($usermeta->avatar) ? asset('uploads/avatar/'.$usermeta->avatar) : asset('jspress/assets/media/avatars/blank.png') !!})"></div>
                            <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="{{__('JsPress::backend.pages.users.edit.upload_image')}}">
                                <i class="bi bi-pencil-fill fs-7"></i>
                                <input type="file" name="avatar" accept=".png, .jpg, .jpeg" />
                            </label>
                        </div>
                        <div class="form-text">{{__('JsPress::backend.pages.users.edit.allowed_image_types')}}</div>
                    </div>
                </div>
                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label required fw-bold fs-6">{{__('JsPress::backend.pages.users.edit.username')}}</label>
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-12 fv-row">
                                <input type="text" name="name" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0" placeholder="{{__('JsPress::backend.pages.users.edit.username')}}" value="{!! $user->name !!}" readonly/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label required fw-bold fs-6">{{__('JsPress::backend.pages.users.edit.email_address')}}</label>
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-12 fv-row">
                                <input type="text" name="email" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0" placeholder="{{__('JsPress::backend.pages.users.edit.email_address')}}" value="{!! $user->email !!}"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label required fw-bold fs-6">{{__('JsPress::backend.pages.users.edit.name_surname')}}</label>
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-6 fv-row">
                                <input type="text" name="first_name" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0" placeholder="{{__('JsPress::backend.pages.users.edit.first_name')}}" value="{!! $usermeta->first_name !!}" />
                            </div>
                            <div class="col-lg-6 fv-row">
                                <input type="text" name="last_name" class="form-control form-control-lg form-control-solid" placeholder="{{__('JsPress::backend.pages.users.edit.last_name')}}" value="{!! $usermeta->last_name !!}" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label required fw-bold fs-6">{{__('JsPress::backend.pages.users.edit.phone_information')}}</label>
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-6 fv-row">
                                <input type="text" name="phone_code" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0" placeholder="{{__('JsPress::backend.pages.users.edit.phone_code')}}" value="{!! $usermeta->phone_code !!}" />
                            </div>
                            <div class="col-lg-6 fv-row">
                                <input type="text" name="phone" class="form-control form-control-lg form-control-solid" placeholder="{{__('JsPress::backend.pages.users.edit.phone')}}" value="{!! $usermeta->phone !!}" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer d-flex justify-content-end py-6 px-9">
                <button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">{{__('JsPress::backend.pages.users.edit.update_profile')}}</button>
            </div>
        </form>
    </div>
</div>
