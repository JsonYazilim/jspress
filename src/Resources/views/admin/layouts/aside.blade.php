<div id="kt_aside" class="aside aside-dark aside-hoverable" data-kt-drawer="true" data-kt-drawer-name="aside" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="{default:'200px', '300px': '250px'}" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_aside_mobile_toggle">
    <div class="aside-logo flex-column-auto" id="kt_aside_logo">
        <a href="{!! Route('admin.dashboard') !!}">
            <img alt="Logo" src="{!! asset(config('jspress.panel.logos.aside_logo')) !!}" class="h-15px logo" />
        </a>
        <div id="kt_aside_toggle" class="btn btn-icon w-auto px-0 btn-active-color-primary aside-toggle" data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body" data-kt-toggle-name="aside-minimize">
            <span class="svg-icon svg-icon-1 rotate-180">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <path opacity="0.5" d="M14.2657 11.4343L18.45 7.25C18.8642 6.83579 18.8642 6.16421 18.45 5.75C18.0358 5.33579 17.3642 5.33579 16.95 5.75L11.4071 11.2929C11.0166 11.6834 11.0166 12.3166 11.4071 12.7071L16.95 18.25C17.3642 18.6642 18.0358 18.6642 18.45 18.25C18.8642 17.8358 18.8642 17.1642 18.45 16.75L14.2657 12.5657C13.9533 12.2533 13.9533 11.7467 14.2657 11.4343Z" fill="black" />
                    <path d="M8.2657 11.4343L12.45 7.25C12.8642 6.83579 12.8642 6.16421 12.45 5.75C12.0358 5.33579 11.3642 5.33579 10.95 5.75L5.40712 11.2929C5.01659 11.6834 5.01659 12.3166 5.40712 12.7071L10.95 18.25C11.3642 18.6642 12.0358 18.6642 12.45 18.25C12.8642 17.8358 12.8642 17.1642 12.45 16.75L8.2657 12.5657C7.95328 12.2533 7.95328 11.7467 8.2657 11.4343Z" fill="black" />
                </svg>
            </span>
        </div>
    </div>
    <div class="aside-menu flex-column-fluid">
        <div class="hover-scroll-overlay-y my-5 my-lg-5" id="kt_aside_menu_wrapper" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_aside_logo, #kt_aside_footer" data-kt-scroll-wrappers="#kt_aside_menu" data-kt-scroll-offset="0">
            <div class="menu menu-column menu-title-gray-800 menu-state-title-primary menu-state-icon-primary menu-state-bullet-primary menu-arrow-gray-500" id="#kt_aside_menu" data-kt-menu="true" data-kt-menu-expand="false">
                <div class="menu-item">
                    <a class="menu-link {!! Route::is('admin.dashboard') ? 'active':'' !!}" href="{!! Route('admin.dashboard') !!}" title="{!! __('JsPress::backend.menu.dashboard_title') !!}" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                        <span class="menu-icon">
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect x="2" y="2" width="9" height="9" rx="2" fill="black" />
                                    <rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="black" />
                                    <rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="black" />
                                    <rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="black" />
                                </svg>
                            </span>
                        </span>
                        <span class="menu-title">{!! __('JsPress::backend.menu.dashboard') !!}</span>
                    </a>
                </div>
                <div class="menu-item">
                    <div class="menu-content pt-8 pb-2">
                        <span class="menu-section text-muted text-uppercase fs-8 ls-1">{!! __('JsPress::backend.menu.applications') !!}</span>
                    </div>
                </div>

                @if(!config()->has('jspress.panel.hide_menu') || !in_array('user',config('jspress.panel.hide_menu') ) )
                    @if( auth()->user()->hasPermissionTo('view users') || Auth()->user()->hasRole('Super-Admin') )
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {!! Route::is('admin.user.list') || Route::is('admin.user.edit')  ? 'show':'' !!}">
                        <span class="menu-link">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" class="bi bi-person-gear" viewBox="0 0 16 16">
                                        <path opacity="0.5" d="M11 5a3 3 0 1 1-6 0 3 3 0 0 1 6 0ZM8 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4Zm.256 7a4.474 4.474 0 0 1-.229-1.004H3c.001-.246.154-.986.832-1.664C4.484 10.68 5.711 10 8 10c.26 0 .507.009.74.025.226-.341.496-.65.804-.918C9.077 9.038 8.564 9 8 9c-5 0-6 3-6 4s1 1 1 1h5.256Zm3.63-4.54c.18-.613 1.048-.613 1.229 0l.043.148a.64.64 0 0 0 .921.382l.136-.074c.561-.306 1.175.308.87.869l-.075.136a.64.64 0 0 0 .382.92l.149.045c.612.18.612 1.048 0 1.229l-.15.043a.64.64 0 0 0-.38.921l.074.136c.305.561-.309 1.175-.87.87l-.136-.075a.64.64 0 0 0-.92.382l-.045.149c-.18.612-1.048.612-1.229 0l-.043-.15a.64.64 0 0 0-.921-.38l-.136.074c-.561.305-1.175-.309-.87-.87l.075-.136a.64.64 0 0 0-.382-.92l-.148-.045c-.613-.18-.613-1.048 0-1.229l.148-.043a.64.64 0 0 0 .382-.921l-.074-.136c-.306-.561.308-1.175.869-.87l.136.075a.64.64 0 0 0 .92-.382l.045-.148ZM14 12.5a1.5 1.5 0 1 0-3 0 1.5 1.5 0 0 0 3 0Z" fill="black"/>
                                    </svg>
                                </span>
                            </span>
                            <span class="menu-title">{!! __('JsPress::backend.menu.user_management') !!}</span>
                            <span class="menu-arrow"></span>
                        </span>
                        <div class="menu-sub menu-sub-accordion">
                            <div class="menu-item menu-accordion mb-1">
                                <a href="{!! Route('admin.user.list') !!}" class="menu-link {!! Route::is('admin.user.list') ? 'active':'' !!}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">{!! __('JsPress::backend.menu.users') !!}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endif
                @endif
                @if( auth()->user()->hasPermissionTo('view admins') || Auth()->user()->hasRole('Super-Admin') )
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {!! Route::is('admin.admin.index') || Route::is('admin.admin.edit') || Route::is('admin.role.list')  ? 'show':'' !!}">
                    <span class="menu-link">
                        <span class="menu-icon">
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M20.5543 4.37824L12.1798 2.02473C12.0626 1.99176 11.9376 1.99176 11.8203 2.02473L3.44572 4.37824C3.18118 4.45258 3 4.6807 3 4.93945V13.569C3 14.6914 3.48509 15.8404 4.4417 16.984C5.17231 17.8575 6.18314 18.7345 7.446 19.5909C9.56752 21.0295 11.6566 21.912 11.7445 21.9488C11.8258 21.9829 11.9129 22 12.0001 22C12.0872 22 12.1744 21.983 12.2557 21.9488C12.3435 21.912 14.4326 21.0295 16.5541 19.5909C17.8169 18.7345 18.8277 17.8575 19.5584 16.984C20.515 15.8404 21 14.6914 21 13.569V4.93945C21 4.6807 20.8189 4.45258 20.5543 4.37824Z" fill="black" />
                                    <path d="M14.854 11.321C14.7568 11.2282 14.6388 11.1818 14.4998 11.1818H14.3333V10.2272C14.3333 9.61741 14.1041 9.09378 13.6458 8.65628C13.1875 8.21876 12.639 8 12 8C11.361 8 10.8124 8.21876 10.3541 8.65626C9.89574 9.09378 9.66663 9.61739 9.66663 10.2272V11.1818H9.49999C9.36115 11.1818 9.24306 11.2282 9.14583 11.321C9.0486 11.4138 9 11.5265 9 11.6591V14.5227C9 14.6553 9.04862 14.768 9.14583 14.8609C9.24306 14.9536 9.36115 15 9.49999 15H14.5C14.6389 15 14.7569 14.9536 14.8542 14.8609C14.9513 14.768 15 14.6553 15 14.5227V11.6591C15.0001 11.5265 14.9513 11.4138 14.854 11.321ZM13.3333 11.1818H10.6666V10.2272C10.6666 9.87594 10.7969 9.57597 11.0573 9.32743C11.3177 9.07886 11.6319 8.9546 12 8.9546C12.3681 8.9546 12.6823 9.07884 12.9427 9.32743C13.2031 9.57595 13.3333 9.87594 13.3333 10.2272V11.1818Z" fill="black" />
                                </svg>
                            </span>
                        </span>
                        <span class="menu-title">{!! __('JsPress::backend.menu.admins') !!}</span>
                        <span class="menu-arrow"></span>
                    </span>
                        <div class="menu-sub menu-sub-accordion">
                            <div class="menu-item menu-accordion mb-1">
                                <a href="{!! Route('admin.admin.index') !!}" class="menu-link {!! Route::is('admin.admin.index') ? 'active':'' !!}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                    <span class="menu-title">{!! __('JsPress::backend.menu.admins') !!}</span>
                                </a>
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <div class="menu-item menu-accordion mb-1">
                                <a href="{!! Route('admin.role.list') !!}" class="menu-link {!! Route::is('admin.role.list') ? 'active':'' !!}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                    <span class="menu-title">{!! __('JsPress::backend.menu.roles') !!}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
                @if( auth()->user()->hasPermissionTo('view media') || Auth()->user()->hasRole('Super-Admin') )
                <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {!! Route::is('admin.media.index') ? 'show':'' !!}">
                    <span class="menu-link">
                        <span class="menu-icon">
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M22 5V19C22 19.6 21.6 20 21 20H19.5L11.9 12.4C11.5 12 10.9 12 10.5 12.4L3 20C2.5 20 2 19.5 2 19V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5ZM7.5 7C6.7 7 6 7.7 6 8.5C6 9.3 6.7 10 7.5 10C8.3 10 9 9.3 9 8.5C9 7.7 8.3 7 7.5 7Z" fill="black"/>
                                    <path d="M19.1 10C18.7 9.60001 18.1 9.60001 17.7 10L10.7 17H2V19C2 19.6 2.4 20 3 20H21C21.6 20 22 19.6 22 19V12.9L19.1 10Z" fill="black"/>
                                </svg>
                            </span>
                        </span>
                        <span class="menu-title">{!! __('JsPress::backend.menu.media') !!}</span>
                        <span class="menu-arrow"></span>
                    </span>
                    <div class="menu-sub menu-sub-accordion">
                        <div class="menu-item menu-accordion mb-1">
                            <a href="{!! Route('admin.media.index') !!}" class="menu-link {!! Route::is('admin.media.index') ? 'active':'' !!}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                <span class="menu-title">{!! __('JsPress::backend.menu.media') !!}</span>
                            </a>
                        </div>
                    </div>
                </div>
                @endif
                @if(function_exists('panel_application_extra_menu'))
                    @foreach(panel_application_extra_menu() as $menu)
                        @if(isset($menu['has_children']) && $menu['has_children'])
                            <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {{ is_active_children_menu($menu) ? 'show':'' }}">
                                <span class="menu-link">
                                    <span class="menu-icon">
                                        {!! $menu['icon'] !!}
                                    </span>
                                    <span class="menu-title">{{ $menu['title'] }}</span>
                                        <span class="menu-arrow"></span>
                                    </span>
                                @foreach($menu['children'] as $children)
                                    <div class="menu-sub menu-sub-accordion">
                                        <div class="menu-item menu-accordion mb-1">
                                            <a href="{{ $children['url'] }}" class="menu-link {{ request()->fullUrl() == $children['url'] ? 'active':'' }}">
                                            <span class="menu-bullet">
                                                <span class="bullet bullet-dot"></span>
                                            </span>
                                                <span class="menu-title">{!! $children['title'] !!}</span>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <div class="menu-item">
                                <a class="menu-link {{ request()->fullUrl() == $menu['url'] ? 'active' : '' }}" href="{{ $menu['url'] }}" title="{{ $menu['title'] }}" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                    <span class="menu-icon">
                                        {!! $menu['icon'] !!}
                                    </span>
                                    <span class="menu-title">{{ $menu['title'] }}</span>
                                </a>
                            </div>
                        @endif
                    @endforeach
                @endif
                <div class="menu-item">
                    <div class="menu-content pt-8 pb-2">
                        <span class="menu-section text-muted text-uppercase fs-8 ls-1">{!! __('JsPress::backend.menu.contents') !!}</span>
                    </div>
                </div>
                @if( $jspress['post_panel_menu'] )
                    @foreach( $jspress['post_panel_menu'] as  $menu)
                        @if( auth()->user()->hasPostPermission('view', $menu->id) || Auth()->user()->hasRole('Super-Admin'))
                            @include('JsPress::admin.layouts.parts.post_menu', ['menu' => $menu])
                        @endif
                    @endforeach
                @endif
                @if(function_exists('panel_content_extra_menu'))
                    @foreach(panel_content_extra_menu() as $menu)
                        @if(isset($menu['has_children']) && $menu['has_children'])
                            <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {{ is_active_children_menu($menu) ? 'show':'' }}">
                                <span class="menu-link">
                                    <span class="menu-icon">
                                        {!! $menu['icon'] !!}
                                    </span>
                                    <span class="menu-title">{{ $menu['title'] }}</span>
                                        <span class="menu-arrow"></span>
                                    </span>
                                @foreach($menu['children'] as $children)
                                    <div class="menu-sub menu-sub-accordion">
                                        <div class="menu-item menu-accordion mb-1">
                                            <a href="{{ $children['url'] }}" class="menu-link {{ request()->fullUrl() == $children['url'] ? 'active':'' }}">
                                            <span class="menu-bullet">
                                                <span class="bullet bullet-dot"></span>
                                            </span>
                                                <span class="menu-title">{!! $children['title'] !!}</span>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <div class="menu-item">
                                <a class="menu-link {{ request()->fullUrl() == $menu['url'] ? 'active' : '' }}" href="{{ $menu['url'] }}" title="{{ $menu['title'] }}" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                    <span class="menu-icon">
                                        {!! $menu['icon'] !!}
                                    </span>
                                    <span class="menu-title">{{ $menu['title'] }}</span>
                                </a>
                            </div>
                        @endif
                    @endforeach
                @endif
                @if( auth()->user()->hasPermissionTo('view post_types') || Auth()->user()->hasRole('Super-Admin') )
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {!! Route::is('admin.post_type.index') || Route::is('admin.post_type.add') || Route::is('admin.post_type.edit')  ? 'show':'' !!}">
                        <span class="menu-link">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path opacity="0.3" d="M17 10H11C10.4 10 10 9.6 10 9V8C10 7.4 10.4 7 11 7H17C17.6 7 18 7.4 18 8V9C18 9.6 17.6 10 17 10ZM22 4V3C22 2.4 21.6 2 21 2H11C10.4 2 10 2.4 10 3V4C10 4.6 10.4 5 11 5H21C21.6 5 22 4.6 22 4ZM22 15V14C22 13.4 21.6 13 21 13H11C10.4 13 10 13.4 10 14V15C10 15.6 10.4 16 11 16H21C21.6 16 22 15.6 22 15ZM18 20V19C18 18.4 17.6 18 17 18H11C10.4 18 10 18.4 10 19V20C10 20.6 10.4 21 11 21H17C17.6 21 18 20.6 18 20Z" fill="black"/>
                                        <path d="M8 5C8 6.7 6.7 8 5 8C3.3 8 2 6.7 2 5C2 3.3 3.3 2 5 2C6.7 2 8 3.3 8 5ZM5 4C4.4 4 4 4.4 4 5C4 5.6 4.4 6 5 6C5.6 6 6 5.6 6 5C6 4.4 5.6 4 5 4ZM8 16C8 17.7 6.7 19 5 19C3.3 19 2 17.7 2 16C2 14.3 3.3 13 5 13C6.7 13 8 14.3 8 16ZM5 15C4.4 15 4 15.4 4 16C4 16.6 4.4 17 5 17C5.6 17 6 16.6 6 16C6 15.4 5.6 15 5 15Z" fill="black"/>
                                    </svg>
                                </span>
                            </span>
                            <span class="menu-title">{!! __('JsPress::backend.post_type.title') !!}</span>
                            <span class="menu-arrow"></span>
                        </span>
                        <div class="menu-sub menu-sub-accordion">
                            <div class="menu-item menu-accordion mb-1">
                                <a href="{!! Route('admin.post_type.index') !!}" class="menu-link {!! Route::is('admin.post_type.index') ? 'active':'' !!}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                    <span class="menu-title">{!! __('JsPress::backend.post_type.title') !!}</span>
                                </a>
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <div class="menu-item menu-accordion mb-1">
                                <a href="{!! Route('admin.post_type.add') !!}" class="menu-link {!! Route::is('admin.post_type.add') ? 'active':'' !!}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                    <span class="menu-title">{!! __('JsPress::backend.post_type.add') !!}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
                @if( auth()->user()->hasPermissionTo('view post_field') || Auth()->user()->hasRole('Super-Admin') )
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {!! Route::is('admin.post_field.index') || Route::is('admin.post_field.create') || Route::is('admin.post_field.edit')  ? 'show':'' !!}">
                        <span class="menu-link">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path opacity="0.3" d="M2 21V14C2 13.4 2.4 13 3 13H21C21.6 13 22 13.4 22 14V21C22 21.6 21.6 22 21 22H3C2.4 22 2 21.6 2 21Z" fill="black"/>
                                        <path d="M2 10V3C2 2.4 2.4 2 3 2H21C21.6 2 22 2.4 22 3V10C22 10.6 21.6 11 21 11H3C2.4 11 2 10.6 2 10Z" fill="black"/>
                                    </svg>
                                </span>
                            </span>
                            <span class="menu-title">{!! __('JsPress::backend.post_field.title') !!}</span>
                            <span class="menu-arrow"></span>
                        </span>
                        <div class="menu-sub menu-sub-accordion">
                            <div class="menu-item menu-accordion mb-1">
                                <a href="{!! Route('admin.post_field.index') !!}" class="menu-link {!! Route::is('admin.post_field.index') ? 'active':'' !!}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                    <span class="menu-title">{!! __('JsPress::backend.post_field.title') !!}</span>
                                </a>
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <div class="menu-item menu-accordion mb-1">
                                <a href="{!! Route('admin.post_field.create') !!}" class="menu-link {!! Route::is('admin.post_field.create') ? 'active':'' !!}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                    <span class="menu-title">{!! __('JsPress::backend.post_field.add_new') !!}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
                @if( auth()->user()->hasPermissionTo('view menu') || Auth()->user()->hasRole('Super-Admin') )
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {!! Route::is('admin.menu.index') || Route::is('admin.menu.create') || Route::is('admin.menu.edit')  ? 'show':'' !!}">
                        <span class="menu-link">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="black"/>
                                        <path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="black"/>
                                    </svg>
                                </span>
                            </span>
                            <span class="menu-title">{!! __('JsPress::backend.menus') !!}</span>
                            <span class="menu-arrow"></span>
                        </span>
                        <div class="menu-sub menu-sub-accordion">
                            <div class="menu-item menu-accordion mb-1">
                                <a href="{!! Route('admin.menu.index') !!}" class="menu-link {!! Route::is('admin.menu.index') ? 'active':'' !!}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                    <span class="menu-title">{!! __('JsPress::backend.menus') !!}</span>
                                </a>
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <div class="menu-item menu-accordion mb-1">
                                <a href="{!! Route('admin.menu.create') !!}" class="menu-link {!! Route::is('admin.menu.create') ? 'active':'' !!}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                    <span class="menu-title">{!! __('JsPress::backend.add_new_menu') !!}</span>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
                <div class="menu-item">
                    <div class="menu-content pt-8 pb-2">
                        <span class="menu-section text-muted text-uppercase fs-8 ls-1">{{ __('JsPress::backend.system_settings') }}</span>
                    </div>
                </div>
                @if( auth()->user()->hasPermissionTo('view languages') || Auth()->user()->hasRole('Super-Admin') )
                    <div class="menu-item">
                        <a class="menu-link {!! Route::is('admin.language.index') || Route::is('admin.translation.index') ? 'active':'' !!}" href="{!! Route('admin.language.index') !!}" title="{{__('JsPress::backend.aside_menu.language_management')}}" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                        <span class="menu-icon">
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" class="bi bi-translate" viewBox="0 0 16 16">
                                  <path opacity="0.4" d="M4.545 6.714 4.11 8H3l1.862-5h1.284L8 8H6.833l-.435-1.286H4.545zm1.634-.736L5.5 3.956h-.049l-.679 2.022H6.18z" fill="black"/>
                                  <path opacity="0.4" d="M0 2a2 2 0 0 1 2-2h7a2 2 0 0 1 2 2v3h3a2 2 0 0 1 2 2v7a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2v-3H2a2 2 0 0 1-2-2V2zm2-1a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H2zm7.138 9.995c.193.301.402.583.63.846-.748.575-1.673 1.001-2.768 1.292.178.217.451.635.555.867 1.125-.359 2.08-.844 2.886-1.494.777.665 1.739 1.165 2.93 1.472.133-.254.414-.673.629-.89-1.125-.253-2.057-.694-2.82-1.284.681-.747 1.222-1.651 1.621-2.757H14V8h-3v1.047h.765c-.318.844-.74 1.546-1.272 2.13a6.066 6.066 0 0 1-.415-.492 1.988 1.988 0 0 1-.94.31z" fill="black"/>
                                </svg>
                            </span>
                        </span>
                            <span class="menu-title">{{__('JsPress::backend.aside_menu.language_management')}}</span>
                        </a>
                    </div>
                @endif
                @if( auth()->user()->hasPermissionTo('view settings') || Auth()->user()->hasRole('Super-Admin') )
                <div class="menu-item">
                    <a class="menu-link {!! Route::is('admin.setting.index') ? 'active':'' !!}" href="{!! Route('admin.setting.index') !!}" title="Site Ayarları" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                        <span class="menu-icon">
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect x="2" y="2" width="9" height="9" rx="2" fill="black" />
                                    <rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="black" />
                                    <rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="black" />
                                    <rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="black" />
                                </svg>
                            </span>
                        </span>
                        <span class="menu-title">{{ __('JsPress::backend.website_settings') }}</span>
                    </a>
                </div>
                @endif
                @if(auth()->user()->hasAdvancedSettingsPermission())
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {!! Route::is('admin.system.index') || Route::is('admin.redirect.index') || Route::is('admin.popup.index') || Route::is('admin.popup.create') || Route::is('admin.popup.edit') || Route::is('admin.cookie.index')  ? 'show':'' !!}">
                    <span class="menu-link">
                        <span class="menu-icon">
                            <span class="svg-icon svg-icon-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24px" height="24px" viewBox="0 0 24 24">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                     <rect x="5" y="5" width="5" height="5" rx="1" fill="#000000"/>
                                        <rect x="14" y="5" width="5" height="5" rx="1" fill="#000000" opacity="0.3"/>
                                        <rect x="5" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"/>
                                        <rect x="14" y="14" width="5" height="5" rx="1" fill="#000000" opacity="0.3"/>
                                    </g>
                                </svg>
                            </span>
                        </span>
                        <span class="menu-title">{{ __('JsPress::backend.advanced_settings') }}</span>
                        <span class="menu-arrow"></span>
                    </span>
                    @if( auth()->user()->hasPermissionTo('view system_settings') || Auth()->user()->hasRole('Super-Admin') )
                        <div class="menu-sub menu-sub-accordion">
                            <div class="menu-item menu-accordion mb-1">
                                <a class="menu-link {!! Route::is('admin.system.index') ? 'active':'' !!}" href="{!! Route('admin.system.index') !!}" title="{{ __('JsPress::backend.system_settings') }}" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                    <span class="menu-icon">
                                        <span class="svg-icon svg-icon-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <path opacity="0.3" d="M22.1 11.5V12.6C22.1 13.2 21.7 13.6 21.2 13.7L19.9 13.9C19.7 14.7 19.4 15.5 18.9 16.2L19.7 17.2999C20 17.6999 20 18.3999 19.6 18.7999L18.8 19.6C18.4 20 17.8 20 17.3 19.7L16.2 18.9C15.5 19.3 14.7 19.7 13.9 19.9L13.7 21.2C13.6 21.7 13.1 22.1 12.6 22.1H11.5C10.9 22.1 10.5 21.7 10.4 21.2L10.2 19.9C9.4 19.7 8.6 19.4 7.9 18.9L6.8 19.7C6.4 20 5.7 20 5.3 19.6L4.5 18.7999C4.1 18.3999 4.1 17.7999 4.4 17.2999L5.2 16.2C4.8 15.5 4.4 14.7 4.2 13.9L2.9 13.7C2.4 13.6 2 13.1 2 12.6V11.5C2 10.9 2.4 10.5 2.9 10.4L4.2 10.2C4.4 9.39995 4.7 8.60002 5.2 7.90002L4.4 6.79993C4.1 6.39993 4.1 5.69993 4.5 5.29993L5.3 4.5C5.7 4.1 6.3 4.10002 6.8 4.40002L7.9 5.19995C8.6 4.79995 9.4 4.39995 10.2 4.19995L10.4 2.90002C10.5 2.40002 11 2 11.5 2H12.6C13.2 2 13.6 2.40002 13.7 2.90002L13.9 4.19995C14.7 4.39995 15.5 4.69995 16.2 5.19995L17.3 4.40002C17.7 4.10002 18.4 4.1 18.8 4.5L19.6 5.29993C20 5.69993 20 6.29993 19.7 6.79993L18.9 7.90002C19.3 8.60002 19.7 9.39995 19.9 10.2L21.2 10.4C21.7 10.5 22.1 11 22.1 11.5ZM12.1 8.59998C10.2 8.59998 8.6 10.2 8.6 12.1C8.6 14 10.2 15.6 12.1 15.6C14 15.6 15.6 14 15.6 12.1C15.6 10.2 14 8.59998 12.1 8.59998Z" fill="black"/>
                                                <path d="M17.1 12.1C17.1 14.9 14.9 17.1 12.1 17.1C9.30001 17.1 7.10001 14.9 7.10001 12.1C7.10001 9.29998 9.30001 7.09998 12.1 7.09998C14.9 7.09998 17.1 9.29998 17.1 12.1ZM12.1 10.1C11 10.1 10.1 11 10.1 12.1C10.1 13.2 11 14.1 12.1 14.1C13.2 14.1 14.1 13.2 14.1 12.1C14.1 11 13.2 10.1 12.1 10.1Z" fill="black"/>
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="menu-title">{{ __('JsPress::backend.system_settings') }}</span>
                                </a>
                            </div>
                        </div>
                    @endif
                    @if(!config()->has('jspress.panel.hide_menu') || !in_array('popup',config('jspress.panel.hide_menu') ) )
                        @if( auth()->user()->hasPermissionTo('view popup') || Auth()->user()->hasRole('Super-Admin') )
                            <div class="menu-sub menu-sub-accordion">
                                <div class="menu-item menu-accordion mb-1">
                                    <a class="menu-link {!! Route::is('admin.popup.index') || Route::is('admin.popup.create') || Route::is('admin.popup.edit') ? 'active':'' !!}" href="{!! Route('admin.popup.index') !!}" title="{{ __('JsPress::backend.popup_title') }}" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                        <span class="menu-icon">
                                            <span class="svg-icon svg-icon-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                    <path opacity="0.5" d="M18 2H9C7.34315 2 6 3.34315 6 5H8C8 4.44772 8.44772 4 9 4H18C18.5523 4 19 4.44772 19 5V16C19 16.5523 18.5523 17 18 17V19C19.6569 19 21 17.6569 21 16V5C21 3.34315 19.6569 2 18 2Z" fill="black"/>
                                                    <path fill-rule="evenodd" clip-rule="evenodd" d="M14.7857 7.125H6.21429C5.62255 7.125 5.14286 7.6007 5.14286 8.1875V18.8125C5.14286 19.3993 5.62255 19.875 6.21429 19.875H14.7857C15.3774 19.875 15.8571 19.3993 15.8571 18.8125V8.1875C15.8571 7.6007 15.3774 7.125 14.7857 7.125ZM6.21429 5C4.43908 5 3 6.42709 3 8.1875V18.8125C3 20.5729 4.43909 22 6.21429 22H14.7857C16.5609 22 18 20.5729 18 18.8125V8.1875C18 6.42709 16.5609 5 14.7857 5H6.21429Z" fill="black"/>
                                                </svg>
                                            </span>
                                        </span>
                                        <span class="menu-title">{{ __('JsPress::backend.popup_title') }}</span>
                                    </a>
                                </div>
                            </div>
                        @endif
                    @endif
                    @if(!config()->has('jspress.panel.hide_menu') || !in_array('cookie',config('jspress.panel.hide_menu') ) )
                        @if( auth()->user()->hasPermissionTo('view cookie') || Auth()->user()->hasRole('Super-Admin') )
                            <div class="menu-sub menu-sub-accordion">
                                <div class="menu-item menu-accordion mb-1">
                                    <a class="menu-link {!! Route::is('admin.cookie.index') ? 'active':'' !!}" href="{!! Route('admin.cookie.index') !!}" title="{{ __('JsPress::backend.cookie_consent.title') }}" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                    <span class="menu-icon">
                                        <span class="svg-icon svg-icon-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <path opacity="0.3" d="M18 22C19.7 22 21 20.7 21 19C21 18.5 20.9 18.1 20.7 17.7L15.3 6.30005C15.1 5.90005 15 5.5 15 5C15 3.3 16.3 2 18 2H6C4.3 2 3 3.3 3 5C3 5.5 3.1 5.90005 3.3 6.30005L8.7 17.7C8.9 18.1 9 18.5 9 19C9 20.7 7.7 22 6 22H18Z" fill="black"/>
                                                <path d="M18 2C19.7 2 21 3.3 21 5H9C9 3.3 7.7 2 6 2H18Z" fill="black"/>
                                                <path d="M9 19C9 20.7 7.7 22 6 22C4.3 22 3 20.7 3 19H9Z" fill="black"/>
                                            </svg>
                                        </span>
                                    </span>
                                        <span class="menu-title">{{ __('JsPress::backend.cookie_consent.title') }}</span>
                                    </a>
                                </div>
                            </div>
                        @endif
                    @endif
                    @if(!config()->has('jspress.panel.hide_menu') || !in_array('redirects',config('jspress.panel.hide_menu') ) )
                        @if( auth()->user()->hasPermissionTo('view redirects') || Auth()->user()->hasRole('Super-Admin') )
                            <div class="menu-sub menu-sub-accordion">
                                <div class="menu-item menu-accordion mb-1">
                                    <a class="menu-link {!! Route::is('admin.redirect.index') ? 'active':'' !!}" href="{!! Route('admin.redirect.index') !!}" title="{{ __('JsPress::backend.redirects') }}" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                    <span class="menu-icon">
                                        <span class="svg-icon svg-icon-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="23" height="24" viewBox="0 0 23 24" fill="none">
                                                <path d="M21 13V13.5C21 16 19 18 16.5 18H5.6V16H16.5C17.9 16 19 14.9 19 13.5V13C19 12.4 19.4 12 20 12C20.6 12 21 12.4 21 13ZM18.4 6H7.5C5 6 3 8 3 10.5V11C3 11.6 3.4 12 4 12C4.6 12 5 11.6 5 11V10.5C5 9.1 6.1 8 7.5 8H18.4V6Z" fill="black"/>
                                                <path opacity="0.3" d="M21.7 6.29999C22.1 6.69999 22.1 7.30001 21.7 7.70001L18.4 11V3L21.7 6.29999ZM2.3 16.3C1.9 16.7 1.9 17.3 2.3 17.7L5.6 21V13L2.3 16.3Z" fill="black"/>
                                            </svg>
                                        </span>
                                    </span>
                                        <span class="menu-title">{{ __('JsPress::backend.redirects') }}</span>
                                    </a>
                                </div>
                            </div>
                        @endif
                    @endif
                    @if(!config()->has('jspress.panel.hide_menu') || !in_array('error',config('jspress.panel.hide_menu') ) )
                        @if( auth()->user()->hasPermissionTo('view error') || Auth()->user()->hasRole('Super-Admin') )
                            <div class="menu-sub menu-sub-accordion">
                                <div class="menu-item menu-accordion mb-1">
                                    <a class="menu-link" href="{!! url('js-admin/log-viewer') !!}" target="_blank" title="{{ __('JsPress::backend.error_logs') }}" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                    <span class="menu-icon">
                                        <span class="svg-icon svg-icon-2">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                                <rect x="7" y="15.3137" width="12" height="2" rx="1" transform="rotate(-45 7 15.3137)" fill="black"/>
                                                <rect x="8.41422" y="7" width="12" height="2" rx="1" transform="rotate(45 8.41422 7)" fill="black"/>
                                            </svg>
                                        </span>
                                    </span>
                                        <span class="menu-title">{{ __('JsPress::backend.error_logs') }}</span>
                                    </a>
                                </div>
                            </div>
                        @endif
                    @endif
                </div>
                @endif
                @if( Auth()->user()->hasRole('Super-Admin') )
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {!! Route::is('admin.vcs.index')  ? 'show':'' !!}">
                        <span class="menu-link">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <path opacity="0.3" d="M11 6.5C11 9 9 11 6.5 11C4 11 2 9 2 6.5C2 4 4 2 6.5 2C9 2 11 4 11 6.5ZM17.5 2C15 2 13 4 13 6.5C13 9 15 11 17.5 11C20 11 22 9 22 6.5C22 4 20 2 17.5 2ZM6.5 13C4 13 2 15 2 17.5C2 20 4 22 6.5 22C9 22 11 20 11 17.5C11 15 9 13 6.5 13ZM17.5 13C15 13 13 15 13 17.5C13 20 15 22 17.5 22C20 22 22 20 22 17.5C22 15 20 13 17.5 13Z" fill="black"/>
                                        <path d="M17.5 16C17.5 16 17.4 16 17.5 16L16.7 15.3C16.1 14.7 15.7 13.9 15.6 13.1C15.5 12.4 15.5 11.6 15.6 10.8C15.7 9.99999 16.1 9.19998 16.7 8.59998L17.4 7.90002H17.5C18.3 7.90002 19 7.20002 19 6.40002C19 5.60002 18.3 4.90002 17.5 4.90002C16.7 4.90002 16 5.60002 16 6.40002V6.5L15.3 7.20001C14.7 7.80001 13.9 8.19999 13.1 8.29999C12.4 8.39999 11.6 8.39999 10.8 8.29999C9.99999 8.19999 9.20001 7.80001 8.60001 7.20001L7.89999 6.5V6.40002C7.89999 5.60002 7.19999 4.90002 6.39999 4.90002C5.59999 4.90002 4.89999 5.60002 4.89999 6.40002C4.89999 7.20002 5.59999 7.90002 6.39999 7.90002H6.5L7.20001 8.59998C7.80001 9.19998 8.19999 9.99999 8.29999 10.8C8.39999 11.5 8.39999 12.3 8.29999 13.1C8.19999 13.9 7.80001 14.7 7.20001 15.3L6.5 16H6.39999C5.59999 16 4.89999 16.7 4.89999 17.5C4.89999 18.3 5.59999 19 6.39999 19C7.19999 19 7.89999 18.3 7.89999 17.5V17.4L8.60001 16.7C9.20001 16.1 9.99999 15.7 10.8 15.6C11.5 15.5 12.3 15.5 13.1 15.6C13.9 15.7 14.7 16.1 15.3 16.7L16 17.4V17.5C16 18.3 16.7 19 17.5 19C18.3 19 19 18.3 19 17.5C19 16.7 18.3 16 17.5 16Z" fill="black"/>
                                    </svg>
                                </span>
                            </span>
                            <span class="menu-title">{!! __('JsPress::backend.version_control_system') !!}</span>
                            <span class="menu-arrow"></span>
                        </span>
                        <div class="menu-sub menu-sub-accordion">
                            <div class="menu-item menu-accordion mb-1">
                                <a href="{!! Route('admin.vcs.index') !!}" class="menu-link {!! Route::is('admin.vcs.index') ? 'active':'' !!}">
                                <span class="menu-bullet">
                                    <span class="bullet bullet-dot"></span>
                                </span>
                                    <span class="menu-title">Commits</span>
                                </a>
                            </div>
                        </div>
                    </div>
                @endif
                @if(function_exists('panel_system_extra_menu'))
                    @foreach(panel_system_extra_menu() as $menu)
                        @if(isset($menu['has_children']) && $menu['has_children'])
                            <div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {{ is_active_children_menu($menu) ? 'show':'' }}">
                                <span class="menu-link">
                                    <span class="menu-icon">
                                        {!! $menu['icon'] !!}
                                    </span>
                                    <span class="menu-title">{{ $menu['title'] }}</span>
                                        <span class="menu-arrow"></span>
                                    </span>
                                @foreach($menu['children'] as $children)
                                    <div class="menu-sub menu-sub-accordion">
                                        <div class="menu-item menu-accordion mb-1">
                                            <a href="{{ $children['url'] }}" class="menu-link {{ request()->fullUrl() == $children['url'] ? 'active':'' }}">
                                            <span class="menu-bullet">
                                                <span class="bullet bullet-dot"></span>
                                            </span>
                                                <span class="menu-title">{!! $children['title'] !!}</span>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @else
                            <div class="menu-item">
                                <a class="menu-link {{ request()->fullUrl() == $menu['url'] ? 'active' : '' }}" href="{{ $menu['url'] }}" title="{{ $menu['title'] }}" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss="click" data-bs-placement="right">
                                    <span class="menu-icon">
                                        {!! $menu['icon'] !!}
                                    </span>
                                    <span class="menu-title">{{ $menu['title'] }}</span>
                                </a>
                            </div>
                        @endif
                    @endforeach
                @endif
            </div>
        </div>
    </div>
</div>
