<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" style="direction: {{ auth()->user()->direction() }}" dir="{{ $jspress['dir'] }}" direction="{{ $jspress['dir'] }}" data-bs-theme="{{$jspress['theme']}}">
<head>
    <title>{{config('jspress.panel.title')}} - {{ @$jspressPanel->meta['title'] }}</title>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" href="{!! asset(config('jspress.panel.logos.favicon')) !!}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    @if( $jspress['dir'] === 'rtl' )
        <link href="{!! asset('jspress/assets/plugins/custom/datatables/datatables.bundle.rtl.css') !!}" rel="stylesheet" type="text/css" />
    @else
        <link href="{!! asset('jspress/assets/plugins/custom/datatables/datatables.bundle.css') !!}" rel="stylesheet" type="text/css" />
    @endif
    <link href="{!! asset('jspress/assets/plugins/custom/vis-timeline/vis-timeline.bundle.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! asset('jspress/assets/plugins/custom/cropper/cropper.bundle.css') !!}" rel="stylesheet" type="text/css" />
    @if( $jspress['theme'] === 'rtl' )
        <link href="{!! asset('jspress/assets/plugins/global/plugins.bundle.rtl.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('jspress/assets/css/style.bundle.rtl.css') !!}" rel="stylesheet" type="text/css" />
    @else
        @if( $jspress['theme'] == 'dark' )
            <link href="{!! asset('jspress/assets/plugins/global/plugins.dark.bundle.css') !!}" rel="stylesheet" type="text/css" />
            <link href="{!! asset('jspress/assets/css/style.dark.bundle.css') !!}" rel="stylesheet" type="text/css" />
        @else
            <link href="{!! asset('jspress/assets/plugins/global/plugins.bundle.css') !!}" rel="stylesheet" type="text/css" />
            <link href="{!! asset('jspress/assets/css/style.bundle.css') !!}" rel="stylesheet" type="text/css" />
        @endif
    @endif
    <link href="{!! asset('jspress/assets/css/style.css?v=1.12') !!}" rel="stylesheet" type="text/css" />
    @stack('style')
    <style>
        @keyframes blinker {
            50% {
                opacity: 0;
            }
        }
    </style>
</head>

<body id="kt_body" class="header-fixed header-tablet-and-mobile-fixed toolbar-enabled toolbar-fixed aside-enabled aside-fixed {!! $jspress['theme'] == 'dark' ? 'dark_theme':'light_theme' !!}" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
<div class="d-flex flex-column flex-root">
    <div class="page d-flex flex-row flex-column-fluid">
        @include('JsPress::admin.layouts.aside')
        <div class="wrapper d-flex flex-column flex-row-fluid" id="kt_wrapper">
            @include('JsPress::admin.layouts.header')
            @yield('content')
            @include('JsPress::admin.layouts.footer')
        </div>
    </div>
</div>

<div id="kt_scrolltop" class="scrolltop" data-kt-scrolltop="true">
    <span class="svg-icon">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
            <rect opacity="0.5" x="13" y="6" width="13" height="2" rx="1" transform="rotate(90 13 6)" fill="black" />
            <path d="M12.5657 8.56569L16.75 12.75C17.1642 13.1642 17.8358 13.1642 18.25 12.75C18.6642 12.3358 18.6642 11.6642 18.25 11.25L12.7071 5.70711C12.3166 5.31658 11.6834 5.31658 11.2929 5.70711L5.75 11.25C5.33579 11.6642 5.33579 12.3358 5.75 12.75C6.16421 13.1642 6.83579 13.1642 7.25 12.75L11.4343 8.56569C11.7467 8.25327 12.2533 8.25327 12.5657 8.56569Z" fill="black" />
        </svg>
    </span>
</div>
<script>
    window._uploadUrl = '{!! route('admin.fileUpload') !!}';
    window.theme = '{{$jspress['theme']}}';
    window.lang = '{{ $jspress['panel_locale'] }}';
    @if(Auth()->user()->hasAnyRole(['Super-Admin', 'Admin']))
        window._is_SuperAdmin = true;
    @else
        window._is_SuperAdmin = false;
    @endif
</script>
<script>
    const trans = {
        original_url:'{{ __('JsPress::backend.file_href') }}',
        file_name:'{{ __('JsPress::backend.filename') }}',
        upload_name:'{{ __('JsPress::backend.file_upload_name') }}',
        mime:'{{ __('JsPress::backend.file_mime_type') }}',
        type:'{{ __('JsPress::backend.file_type') }}',
        width:'{{ __('JsPress::backend.width') }}',
        height:'{{ __('JsPress::backend.height') }}',
        extension:'Extension',
        optimized_url:'Webp',
        path:'Path',
        uploaded_by:'{{ __('JsPress::backend.uploaded_by') }}',
        size:'{{ __('JsPress::backend.file_size') }}',
        tag:'{{ __('JsPress::backend.alt_tag') }}',
        title: '{{ __('JsPress::backend.title_tag') }}',
        caption: '{{ __('JsPress::backend.caption') }}',
        save:'{{ __('JsPress::backend.save') }}',
        delete_image:'{{ __('JsPress::backend.delete_media_file') }}',
        delete_image_warning:'{{ __('JsPress::backend.delete_image_warning') }}',
        confirm:"{!! __('JsPress::backend.yes_i_confirm') !!}",
        no_return:"{!! __('JsPress::backend.no_return') !!}",
        update_message:"{!! __('JsPress::backend.update_message') !!}",
        confirm_button:"{!! __('JsPress::backend.confirmButton') !!}",
        key_is_not_valid:"{{__('JsPress::backend.post_type.key_is_not_valid')}}",
        is_required_message:"{{__('JsPress::backend.this_field_is_required')}}",
        ok:"{{__('JsPress::backend.ok')}}",
        datatable:@json(__('JsPress::backend.datatable')),
        max_upload_size_error: '{{__('JsPress::backend.max_upload_size_error')}}',
        file_size_error:'{{__('JsPress::backend.file_type_error')}}',
        title_is_here:'{{__('JsPress::backend.title_is_here')}}',
        seo_content_is_here:'{{__('JsPress::backend.seo_content_is_here')}}',
        delete_translation_relation:'{{__('JsPress::backend.delete_translation_relation')}}',
        delete_post_warning:'{{__('JsPress::backend.delete_post_warning')}}',
        restore_post_warning:'{{__('JsPress::backend.restore_post_warning')}}',
        force_delete_post_warning:'{{__('JsPress::backend.force_delete_post_warning')}}',
        force_delete_category_warning:'{{__('JsPress::backend.force_delete_category_warning')}}',
        menu_url:'{{__('JsPress::backend.menu_url')}}',
        type_menu_title:'{{__('JsPress::backend.type_menu_title')}}',
        type_a_title:'{{__('JsPress::backend.type_a_title')}}',
        open_new_tab:'{{__('JsPress::backend.open_new_tab')}}',
        remove:'{{__('JsPress::backend.remove')}}',
        no_permission:'{{__('JsPress::backend.permission_error')}}',
        days:@json(collect(__('JsPress::backend.calendar.days_short'))->flatten(1)),
        months:@json(collect(__('JsPress::backend.calendar.month_names'))->flatten(1)),
        apply:'{{__('JsPress::backend.apply')}}',
        cancel:'{{__('JsPress::backend.cancel')}}',
        default:'{{__('JsPress::backend.default')}}'
    };
</script>
<script src="{!! asset('jspress/assets/plugins/global/plugins.bundle.js') !!}"></script>

<script src="{!! asset('jspress/assets/js/scripts.bundle.js') !!}"></script>
<script src="{!! asset('jspress/assets/plugins/custom/datatables/datatables.bundle.js') !!}"></script>
<script src="{!! asset('jspress/assets/plugins/custom/cropper/cropper.bundle.js') !!}"></script>
<script src="{{  asset('jspress/assets/plugins/custom/formrepeater/formrepeater.bundle.js') }}"></script>
<script src="{{ asset('jspress/assets/plugins/custom/draggable/draggable.bundle.js') }}"></script>
<script src="{{ asset('jspress/assets/plugins/custom/tinymce/tinymce.bundle.js') }}"></script>
<script src="{!! asset('jspress/assets/js/app.js?v=1.120') !!}"></script>
<script>
    $(".jspress_datepicker").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        autoApply: true,
        autoUpdateInput: false,
        minYear: 2023,
        maxYear: {!! \Carbon\Carbon::now()->addYear(10)->format('Y') !!},
        locale: {
            format: "YYYY-MM-DD"
        }
    });
    $('.jspress_datepicker').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('YYYY-MM-DD'));
    });
    $(document).ready(function(){
        let galleryContainer = document.querySelectorAll('.gallery_file_images.draggable-zone');

        if(galleryContainer.length > 0){
            galleryContainer.forEach((draggable_item) => {
                var swappable = new Sortable.default(draggable_item, {
                    draggable: ".draggable",
                    handle: ".draggable .draggable-handle",
                    mirror: {
                        appendTo: ".file_upload_wrapper",
                        constrainDimensions: true
                    },
                });
                swappable.on('sortable:stop', function(){
                    setTimeout(function(){
                        var gallery_ids = [];
                        draggable_item.querySelectorAll('.draggable').forEach((item) => {
                            var id = item.getAttribute('data-id');
                            gallery_ids.push(parseInt(id));
                        });
                        draggable_item.closest('.file_upload_wrapper ').querySelector('input').value = JSON.stringify(gallery_ids);
                    },300);
                });

                draggable_item.querySelectorAll('.draggable').forEach((item) => {
                    var deleteIconDiv = item.querySelector('.deleteGalleryImage');
                    deleteIconDiv.addEventListener('click', function(e){
                        e.preventDefault();
                        var divContainer = e.target.closest('.draggable');
                        var id = parseInt(divContainer.getAttribute('data-id'));
                        var input = e.target.closest('.file_upload_wrapper').querySelector('input');
                        var values = JSON.parse(input.value);
                        values = values.filter((input_item)=>{
                            return parseInt(input_item) !== id;
                        });
                        if(values.length === 0){
                            input.value = "";
                        }else{
                            input.value = JSON.stringify(values);
                        }
                        divContainer.remove();
                    });
                });
            });
        }
    });
</script>
@stack('script')
@stack('component_script')
</body>
</html>
