<ul class="breadcrumb breadcrumb-separatorless fw-bold fs-7 my-1">
    <li class="breadcrumb-item text-muted">
        <a href="{!! Route('admin.dashboard') !!}" class="text-muted text-hover-primary">{!! __('JsPress::backend.dashboard') !!}</a>
    </li>
    <li class="breadcrumb-item">
        <span class="bullet bg-gray-300 w-5px h-2px"></span>
    </li>
    @php $i = 0; @endphp
    @foreach( $breadcrumbs as  $key => $breadcrumb )
        @php $i++; @endphp
        <li class="breadcrumb-item text-muted">
            <a href="{!! $key !!}" class="text-muted text-hover-primary">{!! $breadcrumb !!}</a>
        </li>
        @if( $i != count($breadcrumbs) )
        <li class="breadcrumb-item">
            <span class="bullet bg-gray-300 w-5px h-2px"></span>
        </li>
        @endif
    @endforeach
</ul>
