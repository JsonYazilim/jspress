<div class="d-flex align-items-center ms-1 ms-lg-3" id="kt_header_user_menu_toggle">
    <div class="cursor-pointer symbol symbol-30px symbol-md-40px" data-kt-menu-trigger="click"
         data-kt-menu-attach="parent" data-kt-menu-placement="bottom-end">
        <img
            src="{!! $jspress['admin']['avatar'] != NULL ? asset($jspress['admin']['avatar']) : asset('jspress/assets/media/avatars/blank.png') !!}"
            alt="user"/>
    </div>
    <div
        class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-800 menu-state-bg menu-state-primary fw-bold py-4 fs-6 w-300px"
        data-kt-menu="true">
        <div class="menu-item px-3">
            <div class="menu-content d-flex align-items-center px-3">
                <div class="symbol symbol-50px me-5">
                    <img alt="Logo"
                         src="{!! $jspress['admin']['avatar'] != NULL ? asset($jspress['admin']['avatar']) : asset('jspress/assets/media/avatars/blank.png') !!}"/>
                </div>
                <div class="d-flex flex-column">
                    <div class="fw-bolder d-flex align-items-center fs-5">{{$jspress['admin']['name']}}
                        <span
                            class="badge badge-light-success fw-bolder fs-9 px-2 py-1 ms-2">{{Auth()->user()->roleName()}}</span>
                    </div>
                    <a href="#"
                       class="fw-bold text-muted text-hover-primary fs-7">{!! $jspress['admin']['email'] !!}</a>
                </div>
            </div>
        </div>
        <div class="separator my-2"></div>
        <div class="menu-item px-5">
            <a href="{{url('/')}}" class="menu-link px-5"
               target="_blank">{!! __('JsPress::backend.profile_bar.go_to_website') !!}</a>
        </div>
        <div class="menu-item px-5">
            <a href="{!! Route('admin.profile.index') !!}"
               class="menu-link px-5">{!! __('JsPress::backend.profile_bar.my_profile') !!}</a>
        </div>
        <div class="separator my-2"></div>
        @if(website_languages()->count() > 1)
            <div class="menu-item px-5" data-kt-menu-trigger="hover" data-kt-menu-placement="left-start">
                <a href="#" class="menu-link px-5">
                    <span class="menu-title position-relative">{{__('JsPress::backend.panel_language')}}
                    <span
                        class="fs-8 rounded bg-light px-3 py-2 position-absolute translate-middle-y top-50 end-0">{{ $jspress['panel_language'] }}
                    <img class="w-15px h-15px rounded-1 ms-2" src="{{ asset('jspress/assets/media/flags/svg/'.$jspress['panel_locale'].'.svg') }}" alt=""/></span></span>
                </a>
                <div class="menu-sub menu-sub-dropdown w-175px py-4">
                    <div class="menu-item px-3">
                        @foreach(website_languages() as $language)
                        <a href="javascript:void(0);" onclick="JsPressFunc.updateLanguage('{{$language->locale}}')" class="menu-link d-flex px-5 {{ $jspress['panel_locale'] == $language->locale ? 'active':'' }}">
                            <span class="symbol symbol-20px me-4">
                                <img class="rounded-1" src="{{ asset('jspress/assets/media/flags/svg/'.$language->locale.'.svg') }}" alt=""/>
                            </span>{{ General::getLanguageNameByLocale($language->locale) }}
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        <div class="menu-item px-5" data-kt-menu-trigger="hover" data-kt-menu-placement="left-start">
            <a href="#" class="menu-link px-5">
                <span class="menu-title position-relative">{{__('JsPress::backend.content_language')}}
                <span class="fs-8 rounded bg-light px-3 py-2 position-absolute translate-middle-y top-50 end-0">{{ $jspress['content_language'] }}
                <img class="w-15px h-15px rounded-1 ms-2" src="{{ asset('jspress/assets/media/flags/svg/'.$jspress['content_locale'].'.svg') }}" alt=""/></span></span>
            </a>
            <div class="menu-sub menu-sub-dropdown w-175px py-4">
                <div class="menu-item px-3">
                    @foreach(website_languages() as $language)
                        @php
                            $route = create_language_redirect_url($language->locale);
                        @endphp
                        <a href="{{$route}}" class="menu-link d-flex px-5 {{ $jspress['content_locale'] == $language->locale ? 'active':'' }}">
                            <span class="symbol symbol-20px me-4">
                                <img class="rounded-1" src="{{ asset('jspress/assets/media/flags/svg/'.$language->locale.'.svg') }}" alt=""/>
                            </span>{{ General::getLanguageNameByLocale($language->locale) }}
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="menu-item px-5">
            <a href="{!! Route('admin.logout') !!}" class="menu-link px-5">{!! __('JsPress::backend.logout') !!}</a>
        </div>
    </div>
</div>
