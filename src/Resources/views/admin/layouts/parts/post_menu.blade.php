<div data-kt-menu-trigger="click" class="menu-item menu-accordion mb-1 {!! $menu->key == request()->get('post-type') ? 'show':'' !!}">
    <span class="menu-link">
        <span class="menu-icon">
            {!! $menu->icon !!}
        </span>
        <span class="menu-title">{!! __js('post_type', JsPressPanel::postTypeLangKey($menu, 'list'), $menu->name) !!} {!!  $menu->status == 0 ? '<span class="badge badge-light-danger ms-2 fs-9">'.__('JsPress::backend.deactive').'</span>':'' !!}</span>
        <span class="menu-arrow"></span>
    </span>
    <div class="menu-sub menu-sub-accordion">
        <div class="menu-item menu-accordion mb-1">
            <a href="{{ route('admin.post.index', ['post-type'=>$menu->key]) }}" class="menu-link {!! $menu->key == request()->get('post-type') && Route::is('admin.post.index') ? 'active':'' !!}">
                <span class="menu-icon">
                    {!! $menu->icon !!}
                </span>
                <span class="menu-title">{!! __js('post_type', JsPressPanel::postTypeLangKey($menu, 'list'), $menu->name) !!}</span>
            </a>
        </div>
    </div>
    @if( auth()->user()->hasPostPermission('add', $menu->id) || Auth()->user()->hasRole('Super-Admin'))
    <div class="menu-sub menu-sub-accordion">
        <div class="menu-item menu-accordion mb-1">
            <a href="{{ Route('admin.post.add', ['post-type' => $menu->key]) }}" class="menu-link {!! $menu->key == request()->get('post-type') && Route::is('admin.post.add') ? 'active':'' !!}">
                <span class="menu-icon">
                    <i class="fas fa-plus fs-4"></i>
                </span>
                <span class="menu-title">{{ __('JsPress::backend.add_new') }}</span>
            </a>
        </div>
    </div>
    @endif
    @if( $menu->is_category == 1 && (auth()->user()->hasPostPermission('category', $menu->id) || Auth()->user()->hasRole('Super-Admin')) )
    <div class="menu-sub menu-sub-accordion">
        <div class="menu-item menu-accordion mb-1">
            <a href="{{Route('admin.category.index', ['post-type'=>$menu->key])}}" class="menu-link {!! $menu->key == request()->get('post-type') && Route::is('admin.category.index') ? 'active':'' !!}">
                <span class="menu-icon">
                    <i class="far fa-list-alt fs-4"></i>
                </span>
                <span class="menu-title">{{ __js('post_type', JsPressPanel::postTypeLangKey($menu, 'category'), $menu->category_detail->plural_name) }}</span>
            </a>
        </div>
    </div>
    @endif
    @if( auth()->user()->hasPostPermission('trash', $menu->id) || Auth()->user()->hasRole('Super-Admin'))
    <div class="menu-sub menu-sub-accordion">
        <div class="menu-item menu-accordion mb-1">
            <a href="{{ Route('admin.post.trash', ['post-type' => $menu->key]) }}" class="menu-link {!! $menu->key == request()->get('post-type') && Route::is('admin.post.trash') ? 'active':'' !!}">
                <span class="menu-icon">
                    <i class="far fa-trash-alt fs-4"></i>
                </span>
                <span class="menu-title">{{ __('JsPress::backend.trash') }}</span>
            </a>
        </div>
    </div>
    @endif
</div>
