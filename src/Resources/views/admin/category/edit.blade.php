@extends('JsPress::admin.layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Kategori Düzenle</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                @if(Session::has('success'))
                    <x-jspress-alert type="success" :title="__('JsPress::backend.success')" :message="session('success')"></x-jspress-alert>
                @endif
                <form id="edit_category" method="POST" action="{{Route('admin.category.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="card card-flush">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h3>Kategori Düzenle</h3>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="mb-10">
                                        <label class="form-label">Kategori Adı</label>
                                        <input type="text" name="title" class="form-control mb-2" placeholder="Kategori adı giriniz..." value="{{$current_category['title']}}" required>
                                    </div>
                                    @include('JsPress::admin.modules.post_fields.index', ['fields' => $post_fields['after_title'] ?? [], 'post'=>$current_category ])
                                    @if($category_type->is_category_url == 1)
                                        <div class="input-group mt-2">
                                            <span class="input-group-text">{{ url($category_type->slug) }}/</span>
                                            <input type="text" name="slug" class="form-control" value="{{@$current_category['slug']}}" autocomplete="off" required/>
                                        </div>
                                    @endif
                                    <div class="mb-10">
                                        <label class="required form-label">İçerik</label>
                                        <textarea id="category_editor" name="content" class="min-h-100px mb-2">{!! $current_category['content'] !!}</textarea>
                                    </div>
                                    @include('JsPress::admin.modules.post_fields.index', ['fields' => $post_fields['after_editor'] ?? [], 'post'=>$current_category ])
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card card-flush">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h3>Kategori Ayarları</h3>
                                    </div>
                                </div>
                                <div class="card-body">
                                    @if(count($category_translations) > 0)
                                        <div class="mb-10">
                                            <div class="d-flex align-items-center w-100">
                                                <h3 class="fs-6 flex-grow-1">Kategori Çevirileri</h3>
                                                <button type="button" onclick="JsPressFunc.removeCategoryTranslationRelation({{$current_category['id']}}, {{$current_category['transaction_id']}}, '{{$category_type->key}}')" class="btn btn-icon"><i class="bi bi-x-circle fs-2x text-danger"></i></button>
                                            </div>
                                            @foreach($category_translations as $translation)
                                                <div class="d-flex align-items-center mb-2 p-2 border-2 border-dashed">
                                                    <div class="flex-grow-1">
                                                        <a href="javascript:void(0);" class="text-gray-800 text-hover-primary fw-bolder fs-6">{{ $translation['title'] }}</a>
                                                        <span class="text-muted fw-bold d-block">{{display_language($translation['locale'])->display_language_name}}</span>
                                                    </div>
                                                    <img src="{{asset('/jspress/assets/media/flags/svg/'.$translation['locale'].'.svg')}}" width="25"/>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endif
                                    @if(count($related_categories) > 0 && count($category_translations) < 1)
                                        <div class="mb-10">
                                            <label class="form-label">Kategori Dil İlişkisi</label>
                                            <select name="transaction_id" class="form-select" data-control="select2" data-allow-clear="true" data-placeholder="{{__('JsPress::backend.choose')}}">
                                                <option value="">{{__('JsPress::backend.choose')}}</option>
                                                @foreach($related_categories as $category)
                                                    <option value="{{$category['id']}}">{{$category['title']}} ({{display_language($category['locale'])->display_language_name}})</option>
                                                @endforeach
                                            </select>
                                            <div class="d-flex mt-2">
                                                    <span class="d-flex justify-content-center align-items-center me-2">
                                                        <i class="fas fa-info-circle text-warning fs-2x"></i>
                                                    </span>
                                                <span class="mt-2 text-warning fs-8 text-sm">Oluşturulan kategoriyi farklı bir dilde herhangi bir kategoriye bağlamak için yukarıda ki seçeneklerden ilişkilendirmek istediğiniz kategoriyi seçiniz. Boş bırakmanız dahilinde ilgili kategori farklı bir dilde herhangi bir kategori ile ilişkilendirilmeyecektir.</span>
                                            </div>
                                        </div>
                                    @endif
                                    <input type="hidden" name="current_transaction_id" value="{{$current_category['id']}}"/>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="mb-10">
                                                <label class="form-label">Sıralama</label>
                                                <input type="number" name="order" class="form-control mb-2" placeholder="Sıra giriniz..." value="{{$current_category['order']}}">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="mb-10">
                                                <label class="form-label">Durum</label>
                                                <select class="form-select mb-2" name="status" data-control="select2" data-hide-search="true" data-placeholder="Durum seçiniz">
                                                    <option value="1" {{$current_category['status'] == 1 ? 'selected':''}}>Yayında</option>
                                                    <option value="2" {{$current_category['status'] == 2 ? 'selected':''}}>Taslak</option>
                                                    <option value="3" {{$current_category['status'] == 3 ? 'selected':''}}>Zamanlanmış</option>
                                                    <option value="0" {{$current_category['status'] == 0 ? 'selected':''}}>Pasif</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-10">
                                        <label class="form-label">Ebeveny Kategori</label>
                                        <select name="category_id" class="form-select" data-allow-clear="true" data-control="select2" data-placeholder="{{__('JsPress::backend.choose')}}">
                                            <option value="">{{__('JsPress::backend.choose')}}</option>
                                            @foreach($categories as $category)
                                                <option value="{{$category['id']}}" {{ $current_category['category_id'] == $category['id'] ? 'selected':'' }}>{{$category['title']}}</option>
                                                @if(count($category['children']) > 0)
                                                    @include('JsPress::admin.category.parts.children', ['categories' => $category['children'], 'count' => 0, 'type' => 'relation'])
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="mb-10">
                                        <label class="required form-label">Öne Çıkan Görsel</label>
                                        <x-jspress-file-upload-button id="category_featured_image" type="image" :maxfile="1" action="single_file_upload" name="extras[featured_image]" :image="@$current_category['extras']['featured_image']" ></x-jspress-file-upload-button>
                                    </div>
                                    <div class="mb-10">
                                        <input type="hidden" name="post-type" value="{{request()->get('post-type')}}"/>
                                        <input type="hidden" name="id" value="{{$current_category['id']}}"/>
                                        <button type="submit" id="kt_ecommerce_add_product_submit" class="btn btn-primary">
                                            <span class="indicator-label">Güncelle</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                            @include('JsPress::admin.modules.post_fields.index', ['fields' => $post_fields['after_sidebar'] ?? [], 'post'=>$current_category ])
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="{{ asset('jspress/assets/plugins/custom/tinymce/tinymce.bundle.js') }}"></script>
    <script src="{!! asset('jspress/assets/js/custom/validation/jquery.validate.min.js') !!}"></script>
    @if( \File::exists(public_path('jspress/assets/js/custom/validation/localization/messages_'.JsPressPanel::panelLocale().'.js')) )
        <script src="{!! asset('jspress/assets/js/custom/validation/localization/messages_'.JsPressPanel::panelLocale().'.js') !!}"></script>
    @endif
    <script>
        var options = {
            selector: "textarea",
            height: 250,
            plugins: 'image media code imagetools pagebreak anchor fullscreen link lists print table',
            language: window._lang ?? 'tr'
        };
        @if(auth()->guard('admin')->user()->theme_color() === 'dark')
            options["skin"] = "oxide-dark";
        options["content_css"] = "dark";
        @endif
        tinymce.init(options);
        var form = $('#create_category');
        form.validate({
            ignore: []
        });

    </script>
@endpush
