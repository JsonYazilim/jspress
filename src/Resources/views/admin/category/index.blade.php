@extends('JsPress::admin.layouts.app')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{__('JsPress::backend.categories')}}</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
                @if(website_languages()->count() > 0)
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <span class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{__('JsPress::backend.content_language')}}:</span>
                    <div class="d-flex">
                        <img src="{{asset('/jspress/assets/media/flags/svg/'.$current_language.'.svg')}}" class="me-2" width="20"  alt="{{$current_language}}"/>
                        <span>{{display_language($current_language)->display_language_name}}</span>
                    </div>
                </div>
                @endif
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-fluid">
                @if(Session::has('success'))
                    <x-jspress-alert type="success" :title="__('JsPress::backend.success')" :message="session('success')"></x-jspress-alert>
                @endif
                @if(Session::has('error'))
                    <x-jspress-alert type="error" :title="__('JsPress::backend.an_error_occurred')" :message="session('error')"></x-jspress-alert>
                @endif
                <div class="row">
                    <div class="col-lg-6">
                        @include('JsPress::admin.category.add')
                    </div>
                    <div class="col-lg-6">
                        <div class="card card-flush">
                            <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                                <div class="card-title">
                                    <div class="d-flex align-items-center position-relative my-1">
                                        <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                                                <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                                            </svg>
                                        </span>
                                        <input type="text" data-kt-filter="search" class="form-control form-control-solid w-250px ps-14" placeholder="Hızlı Arama" />
                                    </div>
                                </div>
                            </div>
                            <div class="card-body pt-0">
                                <div class="table-responsive">
                                    @include('JsPress::admin.category.parts.table')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="{{ asset('jspress/assets/plugins/custom/tinymce/tinymce.bundle.js') }}"></script>
    <script src="{!! asset('jspress/assets/js/custom/validation/jquery.validate.min.js') !!}"></script>
    <script>
        var options = {
            selector: "textarea",
            height: 250,
            plugins: 'image media code imagetools pagebreak anchor fullscreen link lists print table',
            language: window._lang ?? 'tr'
        };
        @if(auth()->guard('admin')->user()->theme_color() === 'dark')
            options["skin"] = "oxide-dark";
            options["content_css"] = "dark";
        @endif
        tinymce.init(options);
        var form = $('#create_category');
        form.validate({
            ignore: []
        });
    </script>
@endpush


