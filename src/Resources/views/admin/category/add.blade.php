<div class="card card-flush">
    <div class="card-header">
        <div class="card-title">
            <h3>{{ __js('post_type', 'post_type_category_add_'.$category_type->key, __('JsPress::backend.add_new_category', ['category'=>$category_type->singular_name])) }}</h3>
        </div>
    </div>
    <div class="card-body">
        <form id="create_category" method="POST" action="{{Route('admin.category.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="mb-10">
                <label class="form-label">Kategori Adı</label>
                <input type="text" name="title" class="form-control mb-2" placeholder="Kategori adı giriniz..." value="" required>
            </div>
            @include('JsPress::admin.modules.post_fields.index', ['fields' => $post_fields['after_title'] ?? [] ])
            @if(count($related_categories))
                <div class="mb-10">
                    <label class="form-label">Kategori Dil İlişkisi</label>
                    <select name="transaction_id" class="form-select" data-control="select2" data-allow-clear="true" data-placeholder="{{__('JsPress::backend.choose')}}">
                        <option value="">{{__('JsPress::backend.choose')}}</option>
                        @foreach($related_categories as $category)
                            <option value="{{$category['id']}}">{{$category['title']}} ({{display_language($category['locale'])->display_language_name}})</option>
                        @endforeach
                    </select>
                    <div class="d-flex">
                        <span class="d-flex justify-content-center align-items-center me-2">
                            <i class="fas fa-info-circle text-warning fs-2x"></i>
                        </span>
                        <span class="mt-2 text-warning text-sm">Oluşturulan kategoriyi farklı bir dilde herhangi bir kategoriye bağlamak için yukarıda ki seçeneklerden ilişkilendirmek istediğiniz kategoriyi seçiniz. Boş bırakmanız dahilinde ilgili kategori farklı bir dilde herhangi bir kategori ile ilişkilendirilmeyecektir.</span>
                    </div>
                </div>
            @endif
            <div class="mb-10">
                <label class="required form-label">İçerik</label>
                <textarea id="category_editor" name="content" class="min-h-100px mb-2"></textarea>
            </div>
            @include('JsPress::admin.modules.post_fields.index', ['fields' => $post_fields['after_editor'] ?? [] ])
            <div class="row">
                <div class="col-lg-6">
                    <div class="mb-10">
                        <label class="form-label">Sıralama</label>
                        <input type="number" name="order" class="form-control mb-2" placeholder="Sıra giriniz..." value="0">
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="mb-10">
                        <label class="form-label">Durum</label>
                        <select class="form-select mb-2" name="status" data-control="select2" data-hide-search="true" data-placeholder="Durum seçiniz">
                            <option></option>
                            <option value="1" selected="selected">Yayında</option>
                            <option value="2">Taslak</option>
                            <option value="3">Zamanlanmış</option>
                            <option value="0">Pasif</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="mb-10">
                <label class="form-label">Ebeveny Kategori</label>
                <select name="category_id" class="form-select" data-allow-clear="true" data-control="select2" data-placeholder="{{__('JsPress::backend.choose')}}">
                    <option value="">{{__('JsPress::backend.choose')}}</option>
                    @foreach($categories as $category)
                        <option value="{{$category['id']}}">{{$category['title']}}</option>
                        @if(count($category['children']) > 0)
                            @include('JsPress::admin.category.parts.children', ['categories' => $category['children'], 'count' => 0, 'type' => 'relation'])
                        @endif
                    @endforeach
                </select>
            </div>
            @include('JsPress::admin.modules.post_fields.index', ['fields' => $post_fields['after_sidebar'] ?? [] ])
            <div class="mb-10">
                <label class="required form-label">Öne Çıkan Görsel</label>
                <x-jspress-file-upload-button id="category_featured_image" type="image" :maxfile="1" action="single_file_upload" name="extras[featured_image]" :image="null" ></x-jspress-file-upload-button>
            </div>
            <div class="mb-10">
                <input type="hidden" name="post-type" value="{{request()->get('post-type')}}"/>
                <button type="submit" id="kt_ecommerce_add_product_submit" class="btn btn-primary">
                    <span class="indicator-label">Yayınla</span>
                </button>
            </div>
        </form>
    </div>
</div>
