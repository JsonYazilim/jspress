<table class="table align-middle table-row-dashed fs-6 gy-5" id="category_datatable">
    <thead>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
        <th class="w-10px pe-2">
            <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_ecommerce_category_table .form-check-input" value="1" />
            </div>
        </th>
        <th class="min-w-100px">{{__js('post_type', 'post_type_category_table_'.$category_type->key, $category_type->singular_name)}}</th>
        <th class="min-w-90px">{{__('JsPress::backend.status')}}</th>
        <th class="text-end min-w-70px">{{__('JsPress::backend.actions')}}</th>
    </tr>
    </thead>
    <tbody class="fw-bold text-gray-600">
        @foreach($categories as $category)
            <tr>
                <td>
                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                        <input class="form-check-input" type="checkbox" value="{{$category['id']}}" />
                    </div>
                </td>
                <td>
                    <div class="d-flex">
                        <div class="ms-5">
                            <a href="#" class="text-gray-800 text-hover-primary fs-7 fw-bolder mb-1" data-kt-ecommerce-category-filter="category_name">{{$category['title']}}</a>
                        </div>
                    </div>
                </td>
                <td>
                    {!! status_badge($category['status']) !!}
                </td>
                <td class="text-end">
                    <a href="javascript:void(0);" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{{__('JsPress::backend.actions')}}
                        <span class="svg-icon svg-icon-5 m-0">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
                            </svg>
                        </span>
                    </a>
                    <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
                        <div class="menu-item px-3">
                            <a href="{{Route('admin.category.edit', ['id'=>$category['id'], 'post-type'=>$category_type->key])}}" class="menu-link px-3">{{__('JsPress::backend.edit')}}</a>
                        </div>
                        <div class="menu-item px-3">
                            <a href="javascript:void(0);" class="menu-link px-3" onclick="JsPressFunc.deleteCategory({{$category['id']}}, '{{$category_type->key}}', '{{$current_language}}')">{{__('JsPress::backend.delete')}}</a>
                        </div>
                    </div>
                </td>
            </tr>
            @if(count($category['children']) > 0)
                @include('JsPress::admin.category.parts.children', ['categories' => $category['children'], 'count' => 0, 'type' => 'table', 'current_language' => $current_language])
            @endif
        @endforeach
    </tbody>
</table>
@push('script')
    @if( \File::exists(public_path('jspress/assets/js/custom/validation/localization/messages_'.JsPressPanel::panelLocale().'.js')) )
        <script src="{!! asset('jspress/assets/js/custom/validation/localization/messages_'.JsPressPanel::panelLocale().'.js') !!}"></script>
    @endif
    <script>
        var CategoryDataTable = function () {
            var table;
            var datatable;
            var initDatatable = function () {
                datatable = $(table).DataTable({
                    pageLength: 10,
                    ordering: false,
                    responsive: true,
                    language:trans.datatable
                });
            }
            var handleSearchDatatable = () => {
                const filterSearch = document.querySelector('[data-kt-filter="search"]');
                filterSearch.addEventListener('keyup', function (e) {
                    datatable.search(e.target.value).draw();
                });
            }
            return {
                init: function () {
                    table = document.querySelector('#category_table');
                    if ( !table ) {
                        return;
                    }
                    initDatatable();
                    handleSearchDatatable();
                }
            };
        }();
        KTUtil.onDOMContentLoaded(function () {
            CategoryDataTable.init();
        });
    </script>
@endpush
