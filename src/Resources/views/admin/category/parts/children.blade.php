@php $count++; @endphp
@foreach( $categories as $category )
    @if($type == 'table')
        <tr>
            <td>
                <div class="form-check form-check-sm form-check-custom form-check-solid">
                    <input class="form-check-input" type="checkbox" value="{{$category['id']}}" />
                </div>
            </td>
            <td>
                <div class="d-flex">
                    <div class="ms-5">
                        <a href="#" class="text-gray-800 text-hover-primary fs-7 fw-bolder mb-1" data-kt-ecommerce-category-filter="category_name">@for( $i=0; $i<$count; $i++ )<i class="bi bi-arrow-return-right "></i> @endfor {{$category['title']}}</a>
                    </div>
                </div>
            </td>
            <td>
                {!! status_badge($category['status']) !!}
            </td>
            <td class="text-end">
                <a href="javascript:void(0);" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">İşlemler
                    <span class="svg-icon svg-icon-5 m-0">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
                            </svg>
                        </span>
                </a>
                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
                    <div class="menu-item px-3">
                        <a href="{{Route('admin.category.edit', ['id'=>$category['id'], 'post-type'=>$category_type->key])}}" class="menu-link px-3">{{ __('JsPress::backend.edit') }}</a>
                    </div>
                    <div class="menu-item px-3">
                        <a href="javascript:void(0);" class="menu-link px-3" onclick="JsPressFunc.deleteCategory({{$category['id']}}, '{{$category_type->key}}', '{{$current_language}}')">{{__('JsPress::backend.delete')}}</a>
                    </div>
                </div>
            </td>
        </tr>
    @else
        <option value="{!! $category['id'] !!}" {{ @$current_category['category_id'] == $category['id'] ? 'selected':'' }}>@for( $i=0; $i<$count; $i++ )- @endfor {!! $category['title'] !!}</option>
    @endif
    @if(count($category['children']) > 0)
        @include('JsPress::admin.category.parts.children', ['categories' => $category['children'], 'count' => $count, 'type' => $type])
    @endif
@endforeach
