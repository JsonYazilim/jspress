@extends('JsPress::admin.layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Kontrol Paneli</h1>
                </div>

            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                @if(auth()->user()->hasRole('Super-Admin'))
                <div class="row gx-5 gx-xl-10">
                    <div class="col-xxl-12 mb-2 mb-xl-5">
                        <h3 class="fs-4 fw-bold text-info">{{ __('JsPress::backend.hello_admin', ['admin' => auth()->user()->name]) }},</h3>
                        <p class="fs-6">{{ __('JsPress::backend.dashboard_welcome_text') }}</p>
                        <p class="fs-6">{{ __('JsPress::backend.dashboard_welcome_text_two') }}</p>
                    </div>
                    <div class="row gy-5 g-xl-8 mb-10">
                        <div class="col-xl-4">
                            <div class="card card-xl-stretch">
                                <div class="card-header border-0 bg-danger pt-5 pb-20">
                                    <h3 class="card-title fw-bolder text-white">{{ __('JsPress::backend.start_navigaion') }}</h3>
                                    <p>{{ __('JsPress::backend.start_navigaion_text') }}</p>
                                </div>
                                <div class="card-body p-0 pb-5">
                                    <div class="card-p mt-n20 position-relative">
                                        <div class="row g-0">
                                            <div class="col bg-light-warning px-6 py-8 rounded-2 me-7 mb-7">
                                                <span class="svg-icon svg-icon-3x svg-icon-warning d-block my-2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z" fill="black"/>
                                                        <rect x="7" y="17" width="6" height="2" rx="1" fill="black"/>
                                                        <rect x="7" y="12" width="10" height="2" rx="1" fill="black"/>
                                                        <rect x="7" y="7" width="6" height="2" rx="1" fill="black"/>
                                                        <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black"/>
                                                    </svg>
                                                </span>
                                                <a href="{{Route('admin.post.index', ['post-type'=>'page'])}}" class="text-warning fw-bold fs-6">{{ __('JsPress::backend.pages_title') }}</a>
                                            </div>
                                            <div class="col bg-light-primary px-6 py-8 rounded-2 mb-7">
                                                <span class="svg-icon svg-icon-3x svg-icon-primary d-block my-2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path d="M21 7H3C2.4 7 2 6.6 2 6V4C2 3.4 2.4 3 3 3H21C21.6 3 22 3.4 22 4V6C22 6.6 21.6 7 21 7Z" fill="black"></path>
                                                        <path opacity="0.3" d="M21 14H3C2.4 14 2 13.6 2 13V11C2 10.4 2.4 10 3 10H21C21.6 10 22 10.4 22 11V13C22 13.6 21.6 14 21 14ZM22 20V18C22 17.4 21.6 17 21 17H3C2.4 17 2 17.4 2 18V20C2 20.6 2.4 21 3 21H21C21.6 21 22 20.6 22 20Z" fill="black"></path>
                                                    </svg>
                                                </span>
                                                <a href="{{Route('admin.menu.index')}}" class="text-primary fw-bold fs-6">{{ __('JsPress::backend.menus') }}</a>
                                            </div>
                                        </div>
                                        <div class="row g-0">
                                            <div class="col bg-light-danger px-6 py-8 rounded-2 me-7">
                                                <span class="svg-icon svg-icon-3x svg-icon-danger d-block my-2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="none" class="bi bi-translate" viewBox="0 0 16 16">
                                                      <path opacity="0.4" d="M4.545 6.714 4.11 8H3l1.862-5h1.284L8 8H6.833l-.435-1.286H4.545zm1.634-.736L5.5 3.956h-.049l-.679 2.022H6.18z" fill="black"></path>
                                                      <path opacity="0.4" d="M0 2a2 2 0 0 1 2-2h7a2 2 0 0 1 2 2v3h3a2 2 0 0 1 2 2v7a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2v-3H2a2 2 0 0 1-2-2V2zm2-1a1 1 0 0 0-1 1v7a1 1 0 0 0 1 1h7a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H2zm7.138 9.995c.193.301.402.583.63.846-.748.575-1.673 1.001-2.768 1.292.178.217.451.635.555.867 1.125-.359 2.08-.844 2.886-1.494.777.665 1.739 1.165 2.93 1.472.133-.254.414-.673.629-.89-1.125-.253-2.057-.694-2.82-1.284.681-.747 1.222-1.651 1.621-2.757H14V8h-3v1.047h.765c-.318.844-.74 1.546-1.272 2.13a6.066 6.066 0 0 1-.415-.492 1.988 1.988 0 0 1-.94.31z" fill="black"></path>
                                                    </svg>
                                                </span>
                                                <a href="{{ Route('admin.language.index') }}" class="text-danger fw-bold fs-6 mt-2">{{ __('JsPress::backend.pages.languages.title') }}</a>
                                            </div>
                                            <div class="col bg-light-success px-6 py-8 rounded-2">
                                                <span class="svg-icon svg-icon-3x svg-icon-success d-block my-2">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path opacity="0.3" d="M22 5V19C22 19.6 21.6 20 21 20H19.5L11.9 12.4C11.5 12 10.9 12 10.5 12.4L3 20C2.5 20 2 19.5 2 19V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5ZM7.5 7C6.7 7 6 7.7 6 8.5C6 9.3 6.7 10 7.5 10C8.3 10 9 9.3 9 8.5C9 7.7 8.3 7 7.5 7Z" fill="black"></path>
                                                        <path d="M19.1 10C18.7 9.60001 18.1 9.60001 17.7 10L10.7 17H2V19C2 19.6 2.4 20 3 20H21C21.6 20 22 19.6 22 19V12.9L19.1 10Z" fill="black"></path>
                                                    </svg>
                                                </span>
                                                <a href="{{ Route('admin.media.index') }}" class="text-success fw-bold fs-6 mt-2">Medya</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-8">
                            <div class="card card-flush overflow-hidden h-md-100">
                                <div class="card-header py-5">
                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder text-dark">{{ __('JsPress::backend.page_views') }}</span>
                                        <span class="text-gray-400 mt-1 fw-bold fs-6">{{ __('JsPress::backend.page_views_text') }}</span>
                                    </h3>
                                    <div class="card-toolbar">
                                        <div class="btn btn-sm btn-light d-flex align-items-center px-4 jsdaterangepicker" data-type="page_view">
                                            <div class="text-gray-600 fw-bolder">Loading date range...</div>
                                            <span class="svg-icon svg-icon-1 ms-2 me-0">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <path opacity="0.3" d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z" fill="black" />
                                                <path d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z" fill="black" />
                                                <path d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z" fill="black" />
                                            </svg>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body d-flex justify-content-between flex-column pb-1 px-0 pt-0">
                                    <div class="px-9 mb-5">
                                        <div class="d-flex mb-2">
                                            <span class="fs-2hx fw-bolder text-gray-800 me-2 lh-1 ls-n2 totalPageView">0</span>
                                        </div>
                                        <span class="fs-6 fw-bold text-gray-400">{{ __('JsPress::backend.total_views') }}</span>
                                    </div>
                                    <div id="page_views_report" class="min-h-auto ps-4 pe-6"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xxl-12 mb-5 mb-xl-10">
                        <div class="card card-flush h-xl-100">
                            <div class="card-header py-7">
                                <div class="mb-0">
                                    <h3 class="card-title align-items-start flex-column">
                                        <span class="card-label fw-bolder text-gray-800">{{ __('JsPress::backend.depend_on_device_text') }}</span>
                                        <span class="text-gray-400 pt-1 fw-bold fs-6">{{ __('JsPress::backend.depend_on_device_text_two') }}</span>
                                    </h3>
                                </div>
                                <div class="card-toolbar">
                                    <div class="btn btn-sm btn-light d-flex align-items-center px-4 jsdaterangepicker" data-type="device">
                                        <div class="text-gray-600 fw-bolder">Loading date range...</div>
                                        <span class="svg-icon svg-icon-1 ms-2 me-0">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                <path opacity="0.3" d="M21 22H3C2.4 22 2 21.6 2 21V5C2 4.4 2.4 4 3 4H21C21.6 4 22 4.4 22 5V21C22 21.6 21.6 22 21 22Z" fill="black" />
                                                <path d="M6 6C5.4 6 5 5.6 5 5V3C5 2.4 5.4 2 6 2C6.6 2 7 2.4 7 3V5C7 5.6 6.6 6 6 6ZM11 5V3C11 2.4 10.6 2 10 2C9.4 2 9 2.4 9 3V5C9 5.6 9.4 6 10 6C10.6 6 11 5.6 11 5ZM15 5V3C15 2.4 14.6 2 14 2C13.4 2 13 2.4 13 3V5C13 5.6 13.4 6 14 6C14.6 6 15 5.6 15 5ZM19 5V3C19 2.4 18.6 2 18 2C17.4 2 17 2.4 17 3V5C17 5.6 17.4 6 18 6C18.6 6 19 5.6 19 5Z" fill="black" />
                                                <path d="M8.8 13.1C9.2 13.1 9.5 13 9.7 12.8C9.9 12.6 10.1 12.3 10.1 11.9C10.1 11.6 10 11.3 9.8 11.1C9.6 10.9 9.3 10.8 9 10.8C8.8 10.8 8.59999 10.8 8.39999 10.9C8.19999 11 8.1 11.1 8 11.2C7.9 11.3 7.8 11.4 7.7 11.6C7.6 11.8 7.5 11.9 7.5 12.1C7.5 12.2 7.4 12.2 7.3 12.3C7.2 12.4 7.09999 12.4 6.89999 12.4C6.69999 12.4 6.6 12.3 6.5 12.2C6.4 12.1 6.3 11.9 6.3 11.7C6.3 11.5 6.4 11.3 6.5 11.1C6.6 10.9 6.8 10.7 7 10.5C7.2 10.3 7.49999 10.1 7.89999 10C8.29999 9.90003 8.60001 9.80003 9.10001 9.80003C9.50001 9.80003 9.80001 9.90003 10.1 10C10.4 10.1 10.7 10.3 10.9 10.4C11.1 10.5 11.3 10.8 11.4 11.1C11.5 11.4 11.6 11.6 11.6 11.9C11.6 12.3 11.5 12.6 11.3 12.9C11.1 13.2 10.9 13.5 10.6 13.7C10.9 13.9 11.2 14.1 11.4 14.3C11.6 14.5 11.8 14.7 11.9 15C12 15.3 12.1 15.5 12.1 15.8C12.1 16.2 12 16.5 11.9 16.8C11.8 17.1 11.5 17.4 11.3 17.7C11.1 18 10.7 18.2 10.3 18.3C9.9 18.4 9.5 18.5 9 18.5C8.5 18.5 8.1 18.4 7.7 18.2C7.3 18 7 17.8 6.8 17.6C6.6 17.4 6.4 17.1 6.3 16.8C6.2 16.5 6.10001 16.3 6.10001 16.1C6.10001 15.9 6.2 15.7 6.3 15.6C6.4 15.5 6.6 15.4 6.8 15.4C6.9 15.4 7.00001 15.4 7.10001 15.5C7.20001 15.6 7.3 15.6 7.3 15.7C7.5 16.2 7.7 16.6 8 16.9C8.3 17.2 8.6 17.3 9 17.3C9.2 17.3 9.5 17.2 9.7 17.1C9.9 17 10.1 16.8 10.3 16.6C10.5 16.4 10.5 16.1 10.5 15.8C10.5 15.3 10.4 15 10.1 14.7C9.80001 14.4 9.50001 14.3 9.10001 14.3C9.00001 14.3 8.9 14.3 8.7 14.3C8.5 14.3 8.39999 14.3 8.39999 14.3C8.19999 14.3 7.99999 14.2 7.89999 14.1C7.79999 14 7.7 13.8 7.7 13.7C7.7 13.5 7.79999 13.4 7.89999 13.2C7.99999 13 8.2 13 8.5 13H8.8V13.1ZM15.3 17.5V12.2C14.3 13 13.6 13.3 13.3 13.3C13.1 13.3 13 13.2 12.9 13.1C12.8 13 12.7 12.8 12.7 12.6C12.7 12.4 12.8 12.3 12.9 12.2C13 12.1 13.2 12 13.6 11.8C14.1 11.6 14.5 11.3 14.7 11.1C14.9 10.9 15.2 10.6 15.5 10.3C15.8 10 15.9 9.80003 15.9 9.70003C15.9 9.60003 16.1 9.60004 16.3 9.60004C16.5 9.60004 16.7 9.70003 16.8 9.80003C16.9 9.90003 17 10.2 17 10.5V17.2C17 18 16.7 18.4 16.2 18.4C16 18.4 15.8 18.3 15.6 18.2C15.4 18.1 15.3 17.8 15.3 17.5Z" fill="black" />
                                            </svg>
                                        </span>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body pt-0 pb-1">
                                <div class="row">
                                    <div class="col-xxl-6">
                                        <div id="device_analystics" class="min-h-auto"></div>
                                    </div>
                                    <div class="col-xxl-6 text-end">
                                        <div id="donut_chart" class="min-h-auto" style="float:right;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        window.start_date = '{{\Carbon\Carbon::now()->subDay(7)->format('Y-m-d H:i:s')}}';
        window.end_date = '{{\Carbon\Carbon::now()->format('Y-m-d H:i:s')}}';
    </script>
    <script src="{{  asset('jspress/assets/js/analystics.js?v=1.1') }}"></script>
    <script>
        var start = moment().subtract(7, 'days');
        var end = moment();
        var today_text = '{{__('JsPress::backend.today')}}';
        var e = [].slice.call(document.querySelectorAll('.jsdaterangepicker')),
            t = moment().lang(window.lang).subtract(6, "days"),
            n = moment().lang(window.lang);
        e.map((function(e) {
            var i = e.querySelector("div"),
                r = e.getAttribute('data-type'),
                o = function(e, t) {
                    i && (i.innerHTML = e.lang('tr').format("D MMM YYYY") + " - " + t.lang('tr').format("D MMM YYYY"))
                };
            $(e).daterangepicker({
                startDate: t,
                endDate: n,
                maxDate:n,
                opens: 'left',
                locale:{
                    format:'YYYY-MM-DD HH:mm:ss',
                    daysOfWeek:trans.days,
                    monthNames:trans.months,
                    applyLabel: trans.apply,
                    cancelLabel: trans.cancel,
                    customRangeLabel:'{{__('JsPress::backend.date_range')}}'
                },
                ranges: {
                    "{{ __('JsPress::backend.today') }}": [moment(), moment()],
                    "{{ __('JsPress::backend.yesterday') }}": [moment().subtract(1, "days"), moment().subtract(1, "days")],
                    "{{ __('JsPress::backend.last_seven_days') }}": [moment().subtract(6, "days"), moment()],
                    "{{ __('JsPress::backend.last_thirteen_days') }}": [moment().subtract(29, "days"), moment()],
                    "{{ __('JsPress::backend.this_month') }}": [moment().startOf("month"), moment().endOf("month")],
                    "{{ __('JsPress::backend.last_month') }}": [moment().subtract(1, "month").startOf("month"), moment().subtract(1, "month").endOf("month")]
                }
            }, o), o(t, n);
            $(e).on('apply.daterangepicker', function(ev, picker) {
                var start_date = picker.startDate.format('YYYY-MM-DD HH:mm:ss'),
                    end_date =  picker.endDate.format('YYYY-MM-DD HH:mm:ss');
                if(r === 'device'){
                    AnalysticsFunc.updateDeviceAnalystics('device', start_date, end_date, true);
                }
                if(r === 'page_view'){
                    AnalysticsFunc.updatePageViewAnalystics('page_view', start_date, end_date, true);
                }
            });
        }));

    </script>
@endpush
