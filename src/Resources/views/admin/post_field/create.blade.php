@extends('JsPress::admin.layouts.app')
@push('style')
    <style>
        #kt_docs_repeater_basic [data-display="off"]{
            display:none !important;
        }
    </style>
@endpush
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{__('JsPress::backend.post_field.title')}}</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-fluid">
                @if(Session::has('success'))
                    <x-jspress-alert type="success" :title="__('JsPress::backend.success')" :message="session('success')"></x-jspress-alert>
                @endif
                @if(Session::has('error'))
                    <x-jspress-alert type="error" :title="__('JsPress::backend.an_error_occurred')" :message="session('error')"></x-jspress-alert>
                @endif
                <form id="popup_store_form" action="{{Route('admin.post_field.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="mb-5">
                                <h3>{{__('JsPress::backend.post_field.add_new')}}</h3>
                            </div>
                            <div class="mb-10">
                                <input type="text" name="title" class="form-control mb-2" placeholder="Özel alan başlığını giriniz..." value="{{@$post_field->title}}" required autocomplete="off"/>
                            </div>
                            <div class="card">
                                <div class="card-body p-0">
                                    <ul class="d-flex w-100 pb-4 border-bottom border-bottom-dashed list-unstyled p-5">
                                        <li class="w-25 fs-5 fw-bolder text-muted">{{ __('JsPress::backend.post_field.order') }}</li>
                                        <li class="w-25 fs-5 fw-bolder text-muted">{{ __('JsPress::backend.post_field.field_label') }}</li>
                                        <li class="w-25 fs-5 fw-bolder text-muted">{{ __('JsPress::backend.post_field.field_key') }}</li>
                                        <li class="w-25 fs-5 fw-bolder text-muted">{{ __('JsPress::backend.post_field.field_type') }}</li>
                                    </ul>
                                    <div id="add_new_post_fiel_text" class="d-flex p-5">
                                        <span class="fs-5">{!! __('JsPress::backend.post_field.no_field_found') !!}</span>
                                    </div>
                                    <div id="kt_docs_repeater_basic">
                                        <div class="draggable-zone parent" data-repeater-list="post_field[items]">
                                            <div class="fieldWraps draggable parent" data-repeater-item>
                                                <div class="fieldContainer w-100 border-bottom">
                                                    <div class="meta d-none">
                                                        <input type="hidden" name="order" class="orderField" value="1"/>
                                                    </div>
                                                    <ul class="d-flex w-100 pb-4 list-unstyled p-5">
                                                        <li class="w-25 draggable-handle parent">
                                                            <div class="w-25px h-25px border rounded-3 text-center orderFieldText" style="line-height:25px;">1</div>
                                                        </li>
                                                        <li class="w-25 cursor-pointer">
                                                            <div class="d-flex justify-content-start flex-column">
                                                                <span class="fw-bolder text-primary labelField">No Label</span>
                                                                <ul class="d-flex list-unstyled">
                                                                    <li data-repeater-delete><span class="fs-8 text-danger">{{ __('JsPress::backend.remove') }}</span></li>
                                                                </ul>
                                                            </div>

                                                        </li>
                                                        <li class="w-25 keyText">-</li>
                                                        <li class="w-25">{{ __('JsPress::backend.post_field.text') }}</li>
                                                    </ul>
                                                    <div class="fieldItems w-100" style="display: none;">
                                                        @include('JsPress::admin.post_field.repeater_fields', ['callback'=>true])
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="bg-gray-400 w-100">
                                            <div class="d-flex justify-content-end p-5">
                                                <button type="button" class="btn btn-primary btn-sm" data-repeater-create>{{ __('JsPress::backend.post_field.add_field') }}</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card mt-10">
                                <div class="card-body">
                                    <div class="mb-5">
                                        <label class="form-label">{{ __('JsPress::backend.post_field.rules') }}</label>
                                        <div class="mb-5">
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <select id="conditionSelect" class="form-select" name="condition[key]">
                                                        <option value="post_type" {{ isset($post_field->conditions) && $post_field->conditions['key'] == 'post_type' ? 'selected':'' }}>{{ __('JsPress::backend.post_field.post_type') }}</option>
                                                        <option value="category_type" {{ isset($post_field->conditions) && $post_field->conditions['key'] == 'category_type' ? 'selected':'' }}>{{ __('JsPress::backend.post_field.category_type') }}</option>
                                                        <option value="template" {{ isset($post_field->conditions) && $post_field->conditions['key'] == 'template' ? 'selected':'' }}>{{ __('JsPress::backend.post_field.page_template') }}</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-2">
                                                    <select class="form-select" name="condition[equal]">
                                                        <option value="is_equal" {{ isset($post_field->conditions) && $post_field->conditions['equal'] == 'is_equal' ? 'selected':'' }}>{{ __('JsPress::backend.equal_to') }}</option>
                                                        <option value="is_not_equal" {{ isset($post_field->conditions) && $post_field->conditions['equal'] == 'is_not_equal' ? 'selected':'' }}>{{ __('JsPress::backend.not_equal_to') }}</option>
                                                    </select>
                                                </div>
                                                <div class="col-lg-4">
                                                    <select id="conditionValues" class="form-select" name="condition[value]">
                                                        @foreach($rule_options['post_type'] as $type)
                                                            <option value="{{$type['value']}}">{{$type['label']}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="card card-flush py-4">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>{{ __('JsPress::backend.settings') }}</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    <div class="mb-5">
                                        <label class="form-label">{{ __('JsPress::backend.post_field.position') }}</label>
                                        <select class="form-select" name="position">
                                            <option value="after_title" {{ @$post_field->position == 'after_title'? 'selected':''}}>{{ __('JsPress::backend.post_field.after_title') }}</option>
                                            <option value="after_editor" {{@$post_field->position == 'after_editor'? 'selected':''}}>{{ __('JsPress::backend.post_field.after_editor') }}</option>
                                            <option value="after_seo" {{@$post_field->position == 'after_seo'? 'selected':''}}>{{ __('JsPress::backend.post_field.after_seo') }}</option>
                                            <option value="sidebar" {{@$post_field->position == 'sidebar'? 'selected':''}}>{{ __('JsPress::backend.post_field.sidebar') }}</option>
                                            <option value="post_bottom" {{@$post_field->position == 'post_bottom'? 'selected':''}}>{{ __('JsPress::backend.post_field.post_bottom') }}</option>
                                        </select>
                                    </div>
                                    <div class="mb-5">
                                        <label class="form-label">{{ __('JsPress::backend.appearance') }}</label>
                                        <select class="form-select" name="appearence">
                                            <option value="accordion" {{@$post_field->appearence == 'accordion'? 'selected':''}}>Accordion</option>
                                            <option value="card" {{ @$post_field->appearence == 'card'? 'selected':''}}>Card</option>
                                            <option value="none" {{ @$post_field->appearence == 'none'? 'selected':''}}>{{ __('JsPress::backend.empty') }}</option>
                                        </select>
                                    </div>
                                    <div id="accordionStatus" class="mb-5 {{ @$post_field->appearence != 'accordion' ? 'd-none':'' }}">
                                        <label class="form-label">{{ __('JsPress::backend.accordion_appearance_status') }}</label>
                                        <select class="form-select" name="is_accordion_open">
                                            <option value="0" {{@$post_field->is_accordion_open == 0 ? 'selected':''}}>{{ __('JsPress::backend.close_status') }}</option>
                                            <option value="1" {{ @$post_field->is_accordion_open == 1 ? 'selected':''}}>{{ __('JsPress::backend.open_status') }}</option>
                                        </select>
                                    </div>
                                    <div class="mb-5">
                                        <label class="form-label">{{ __('JsPress::backend.order') }}</label>
                                        <input type="number" class="form-control" name="order" value="{{ isset($post_field->order) ? $post_field->order : 0 }}"/>
                                    </div>
                                    <div class="mb-5">
                                        <label class="form-label">{{ __('JsPress::backend.description') }}</label>
                                        <input type="text" class="form-control" name="description" value="{{@$post_field->description}}"/>
                                    </div>
                                </div>
                                <div class="card-footer text-end pt-3">
                                    @isset($post_field->id)
                                        <input type="hidden" name="post_field_id" value="{{$post_field->id}}"/>
                                    @endif
                                    <button type="submit" id="post_button" class="btn {{ isset($post_field->id) ? 'btn-success':'btn-primary' }}">
                                        <span class="indicator-label">{{ isset($post_field->id) ? __('JsPress::backend.post_field.update') : __('JsPress::backend.post_field.create') }}</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        window._tables = @json($table_options);
        window._rules = @json($rule_options);
        window._initEmpty = true;
        window._action = '{{ isset($post_field->id) ? 'edit':'create' }}';
        window._fields = @json([]);
        window._is_show_active = false;
        @isset($post_field->id)
            window._fields = @json($post_field->data);
            window._conditions = @json($post_field->conditions);
        @endif

        $('#conditionSelect').on('change', function(){
            var val = $(this).val();
            changeCondition(val);
        });
        var changeCondition = function(val){
            $('#conditionValues').html("");
            for(var i = 0; i < window._rules[val].length; i++){
                $('#conditionValues').append('<option value="'+window._rules[val][i].value+'">'+window._rules[val][i].label+'</option>')
            }
            if(window._action === 'edit'){
                $('#conditionValues').val(window._conditions.value);
            }
        }
        $(document).on('change','.field_type_select', function(){
            var val = $(this).val();
            $(this).closest('.fieldItems').find('[data-condition="on"]').each(function(item){
                var fields = JSON.parse($(this).attr('data-fields'));
                if( fields.includes(val) ){
                    $(this).attr('data-display', 'on');
                }else{
                    $(this).attr('data-display', 'off');
                }
            });
            if(val === 'selectbox'){
                var selectType = $(this).closest('.fieldItems').find('.select_option_items').val();
                if(selectType === 'database'){
                    $('.select_database_field').attr('data-display', 'on');
                    $('.select_manual_field').attr('data-display', 'off');
                }else{
                    $('.select_database_field').attr('data-display', 'off');
                    $('.select_manual_field').attr('data-display', 'on');
                }
            }else{
                $('.select_database_field').attr('data-display', 'off');
                $('.select_manual_field').attr('data-display', 'off');
            }
        });

        $(document).on('change', 'select[name="appearence"]', function(){
            $('#accordionStatus').addClass('d-none');
            if($(this).val() === 'accordion'){
                $('#accordionStatus').removeClass('d-none');
            }
        });

        var repeaterFunc = function(){
            var $repeater = $('#kt_docs_repeater_basic').repeater({
                initEmpty: true,
                repeaters: [{
                    selector: '.inner-repeater',
                    initEmpty: false,
                    show: function () {
                        var index = $(this).closest('[data-repeater-item]').index();
                        showFunc(index, $(this));
                        $(this).find('[data-kt-repeater="select2"]').select2();
                    },

                    hide: function (deleteElement) {
                        hideFunc($(this), deleteElement);
                    },
                    ready:function(setIndexes){
                        $(this).find('[data-kt-repeater="select2"]').select2();
                    }
                }],
                show: function () {
                    let $this = $(this);
                    $('#add_new_post_fiel_text').addClass('d-none');
                    var index = $(this).closest('[data-repeater-item]').index();
                    showFunc(index, $(this), true);
                    setTimeout(function(){
                        var mainContainer = document.querySelectorAll('.fieldWraps')[index];
                        var containers = mainContainer.querySelectorAll('.draggable-zone-child');
                        var swappable = new Sortable.default(containers, {
                            draggable: ".draggable.child",
                            handle: ".draggable .draggable-handle.child",
                            mirror: {
                                appendTo: "body",
                                constrainDimensions: true
                            },
                        });
                        swappable.on('sortable:stop', function(){
                            setTimeout(function(){
                                reIndexOrderChild();
                            },300);
                        });
                        reIndexOrderChild();
                        var val = $this.find('.field_type_select').val();
                        if(val === 'selectbox'){
                            var option_type = $this.find('.select_option_items').val();
                            if(option_type === 'database'){
                                $this.find('.select_database_field').attr('data-display', 'on');
                                $this.find('.select_manual_field').attr('data-display', 'off');
                                setTableOptions($this.find('.select_options_table'));
                                $this.find('.select_options_column_value').val(window._fields[index].select_options_column_value);
                                $this.find('.select_options_column_label').val(window._fields[index].select_options_column_label);
                            }
                        }
                    },500);
                    $(this).find('[data-kt-repeater="select2"]').select2();
                },
                hide: function (deleteElement) {
                    hideFunc($(this), deleteElement);
                },
                ready: function(setIndexes){
                    var containers = document.querySelectorAll(".draggable-zone.parent");
                    var swappable = new Sortable.default(containers, {
                        draggable: ".draggable.parent",
                        handle: ".draggable.parent .draggable-handle.parent",
                        mirror: {
                            appendTo: "body",
                            constrainDimensions: true
                        },

                    });
                    $('.fieldItems').slideToggle();
                    swappable.on('sortable:stop', function(){
                        setTimeout(function(){
                            reIndexOrder();
                            setIndexes();
                            setTimeout(function(){
                                reIndexOrderChild();
                            },300);
                        },300);
                    });
                    $(this).find('[data-kt-repeater="select2"]').select2();
                }
            });
            @isset($post_field->id)
                $repeater.setList(window._fields);
            @endif
        }
        $(document).on('click', '.labelField', function(e){
            var container = $(this).closest('.fieldContainer').find('.fieldItems').first();
            container.slideToggle(250);
        });

        $(document).on('change', '.fieldLabel', function(){
            var val = $(this).val();
            var container = $(this).closest('.fieldContainer');
            var el = container.find('.keyLabel');
            container.find('.labelField').text(val);
            if( el.val() === "" ){
                el.val(slugify(val));
                container.find('.keyText').text(slugify(val));
            }
        });
        $(document).on('change', '.select_option_items', function(){
            var val = $(this).val();
            if(val === 'database'){
                $(this).closest('.fieldContainer').find('.select_database_field').attr('data-display', 'on');
                $(this).closest('.fieldContainer').find('.select_manual_field').attr('data-display', 'off');
            }
            if(val === 'manual'){
                $(this).closest('.fieldContainer').find('.select_database_field').attr('data-display', 'off');
                $(this).closest('.fieldContainer').find('.select_manual_field').attr('data-display', 'on');
            }
        });
        $(document).on('change', '.select_options_table', function(){
            setTableOptions($(this));
        });
        var setTableOptions = function(el){
            var table = el.val();
            var columns = window._tables[table].columns;
            el.closest('.fieldContainer').find('.select_options_column_value').html("");
            el.closest('.fieldContainer').find('.select_options_column_label').html("");
            for(var i = 0; i < columns.length; i++){
                el.closest('.fieldContainer').find('.select_options_column_value').append('<option value="'+columns[i]+'">'+columns[i]+'</option>>');
                el.closest('.fieldContainer').find('.select_options_column_label').append('<option value="'+columns[i]+'">'+columns[i]+'</option>>');
            }
        }
        $(document).ready(function(){
            repeaterFunc();
            if(window._action === 'edit'){
                setTimeout(function(){
                    $('.fieldItems').each(function(item){
                        var val = $(this).find('.field_type_select').val();
                        $(this).find('[data-condition="on"]').each(function(){
                            var fields = JSON.parse($(this).attr('data-fields'));
                            if( fields.includes(val) ){
                                $(this).attr('data-display', 'on');
                            }else{
                                $(this).attr('data-display', 'off');
                            }
                        });

                    });
                    $('.fieldContainer').each(function(item){
                        var val = $(this).find('.fieldLabel').val();
                        var key = $(this).find('.keyLabel').val();
                        $(this).find('.labelField').text(val);
                        $(this).find('.keyText').text(key);
                    });
                    $('.fieldWrapsChild').each(function(){
                        var fieldType = $(this).find('.field_type_select').val();
                        if(fieldType === 'selectbox'){
                            var selectType = $(this).find('.select_option_items').val();
                            if(selectType === 'database'){
                                $(this).find('.select_manual_field').attr('data-display', 'off');
                                $(this).find('.select_database_field').attr('data-display', 'on');
                                setTableOptions($(this).find('.select_options_table'));
                                var table = $(this).find('.select_options_table').val();
                                if(table != null && table !== ""){
                                    var index = $(this).index();
                                    var parentIndex = $(this).closest('.fieldWraps').index();
                                    $(this).find('.select_options_column_value').val(window._fields[parentIndex].items[index].select_options_column_value);
                                    $(this).find('.select_options_column_label').val(window._fields[parentIndex].items[index].select_options_column_label);
                                }
                            }else{
                                $(this).find('.select_manual_field').attr('data-display', 'on');
                                $(this).find('.select_database_field').attr('data-display', 'off');
                            }
                        }
                    });
                    var condition = window._conditions.key;
                    changeCondition(condition);
                },500);
            }
            window._is_show_active = true;
        });

        var showFunc = function(index, el, is_main = false){
            el.find('.orderFieldText').text(index + 1);
            el.find('.orderField').val(index + 1);
            el.attr('data-field', uid());
            var select_items = el.find('.select_option_items');
            el.slideDown();
            if(window._action === 'create' || (window._is_show_active && window._action === 'edit')){
                el.find('.field_type_select').val('text');
                if(select_items.val() === null){
                    select_items.val('manual');
                }
            }
            if(window._is_show_active){
                el.find('.fieldItems').slideToggle(100);
            }
            var type = el.find('.field_type_select').val();
            if(is_main && type !== 'repeater'){
                el.find('.fieldWrapsChild [data-repeater-delete]').click();
            }
        }
        var hideFunc = function(el, deleteElement){
            el.slideUp(deleteElement);
            setTimeout(function(){
                reIndexOrder();
                if( $('.fieldItems').length < 1 ){
                    $('#add_new_post_fiel_text').removeClass('d-none');
                }
            },500);
        }
        var hideFuncChild = function(el, deleteElement){
            el.slideUp(deleteElement);
            setTimeout(function(){
                reIndexOrderChild();
            },500);
        }

        var reIndexOrder = function(){
            $('.fieldWraps').each(function(index){
                $(this).find('.orderFieldText').text(index + 1);
                $(this).find('.orderField').val(index + 1);
            });
        }
        var reIndexOrderChild = function(){
            $('.fieldWraps').each(function(index){
                var i = 0;
                $(this).find('.fieldWrapsChild').each(function(index){
                    i++;
                    $(this).find('.orderFieldText').text(i);
                    $(this).find('.orderField').val(i);
                });
            });

        }
        var slugify = function(str) {
            return String(str)
                .normalize('NFKD')
                .replace('ı', 'i')
                .replace(/[\u0300-\u036f]/g, '')
                .trim()
                .toLowerCase()
                .replace(/[^a-z0-9 -]/g, '')
                .replace(/\s+/g, '-')
                .replace(/-+/g, '_');
        }
        var uid = function(){
            return Date.now().toString(36) + Math.random().toString(36).substr(2);
        }
    </script>
@endpush

