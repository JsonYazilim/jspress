<div class="d-flex w-100 border-bottom">
    <div class="w-25 bg-light p-5">
        <div class="d-flex justify-content-start flex-column">
            <a href="javascript:void(0);" class="text-dark fw-bolder text-hover-primary fs-6">{{ __('JsPress::backend.post_field.field_label') }}</a>
            <span class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.post_field.field_label_description') }}</span>
        </div>
    </div>
    <div class="w-75 p-5">
        <input type="text" name="label" class="form-control fieldLabel mb-2 mb-md-0" required/>
    </div>
</div>
<div class="d-flex w-100 border-bottom">
    <div class="w-25 bg-light p-5">
        <div class="d-flex justify-content-start flex-column">
            <a href="javascript:void(0);" class="text-dark fw-bolder text-hover-primary fs-6">{{ __('JsPress::backend.post_field.field_key') }}</a>
            <span class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.post_field.field_key_description') }}</span>
        </div>
    </div>
    <div class="w-75 p-5">
        <input type="text" name="key" class="form-control keyLabel mb-2 mb-md-0" required/>
    </div>
</div>
<div class="d-flex w-100 border-bottom">
    <div class="w-25 bg-light p-5">
        <div class="d-flex justify-content-start flex-column">
            <a href="javascript:void(0);" class="text-dark fw-bolder text-hover-primary fs-6">{{ __('JsPress::backend.post_field.field_type') }}</a>
            <span class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.post_field.field_type_description') }} </span>
        </div>
    </div>
    <div class="w-75 p-5">
        <select name="type" class="form-select field_type_select">
            <option value="text">{{ __('JsPress::backend.post_field.text') }}</option>
            <option value="number">{{ __('JsPress::backend.post_field.number') }}</option>
            <option value="email">{{ __('JsPress::backend.post_field.email') }}</option>
            <option value="textarea">{{ __('JsPress::backend.post_field.textarea') }}</option>
            <option value="editor">{{ __('JsPress::backend.post_field.editor') }}</option>
            <option value="selectbox">{{ __('JsPress::backend.post_field.selectbox') }}</option>
            <option value="checkbox">{{ __('JsPress::backend.post_field.checkbox') }}</option>
            <option value="radio">{{ __('JsPress::backend.post_field.radio') }}</option>
            <option value="switch">Switch</option>
            <option value="datepicker">Datepicker</option>
            <option value="post_object">{{ __('JsPress::backend.post_field.post_objects') }}</option>
            <option value="category_object">{{ __('JsPress::backend.category_object') }}</option>
            <option value="file">{{ __('JsPress::backend.file') }}</option>
            <option value="image">{{ __('JsPress::backend.post_field.image') }}</option>
            <option value="gallery">{{ __('JsPress::backend.post_field.gallery') }}</option>
            <option value="video">Video</option>
            @if($callback)
                <option value="repeater">{{ __('JsPress::backend.post_field.repeater') }}</option>
            @endif
        </select>
    </div>
</div>
<div class="d-flex w-100 border-bottom">
    <div class="w-25 bg-light p-5">
        <div class="d-flex justify-content-start flex-column">
            <a href="javascript:void(0);" class="text-dark fw-bolder text-hover-primary fs-6">{{ __('JsPress::backend.post_field.required_field') }}</a>
            <span class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.post_field.required_field_description') }}</span>
        </div>
    </div>
    <div class="w-75 p-5">
        <label class="form-check form-switch form-check-custom form-check-solid">
            <input class="form-check-input w-60px" name="is_required" type="checkbox" value="1"/>
        </label>
    </div>
</div>
<div class="d-flex w-100 border-bottom" data-condition="on" data-display="off" data-fields='["post_object"]'>
    <div class="w-25 bg-light p-5">
        <div class="d-flex justify-content-start flex-column">
            <a href="javascript:void(0);" class="text-dark fw-bolder text-hover-primary fs-6">{{ __('JsPress::backend.post_field.post_objects') }}</a>
            <span class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.post_field.post_objects_description') }}</span>
        </div>
    </div>
    <div class="w-75 p-5">
        <select class="form-select form-select-solid" name="post_objects" data-kt-repeater="select2" data-placeholder="{{__('JsPress::backend.choose')}}" data-allow-clear="true" multiple="multiple">
            <option></option>
            @foreach( $post_objects as $object )
                <option value="{{$object['value']}}">{{$object['label']}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="d-flex w-100 border-bottom" data-condition="on" data-display="off" data-fields='["category_object"]'>
    <div class="w-25 bg-light p-5">
        <div class="d-flex justify-content-start flex-column">
            <a href="javascript:void(0);" class="text-dark fw-bolder text-hover-primary fs-6">{{ __('JsPress::backend.category_object') }}</a>
            <span class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.category_object_description') }}</span>
        </div>
    </div>
    <div class="w-75 p-5">
        <select class="form-select form-select-solid" name="category_objects" data-kt-repeater="select2" data-placeholder="{{__('JsPress::backend.choose')}}" data-allow-clear="true" multiple="multiple">
            <option></option>
            @foreach( $category_objects as $object )
                <option value="{{$object['value']}}">{{$object['label']}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="d-flex w-100 border-bottom" data-condition="on" data-display="on" data-fields='["text","number","email","textarea"]'>
    <div class="w-25 bg-light p-5">
        <div class="d-flex justify-content-start flex-column">
            <a href="javascript:void(0);" class="text-dark fw-bolder text-hover-primary fs-6">{{ __('JsPress::backend.post_field.placeholder') }}</a>
            <span class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.post_field.placeholder_text') }}</span>
        </div>
    </div>
    <div class="w-75 p-5">
        <input type="text" name="placeholder" class="form-control mb-2 mb-md-0"/>
    </div>
</div>
<div class="d-flex w-100 border-bottom" data-condition="on" data-display="off" data-fields='["gallery", "file"]'>
    <div class="w-25 bg-light p-5">
        <div class="d-flex justify-content-start flex-column">
            <a href="javascript:void(0);" class="text-dark fw-bolder text-hover-primary fs-6">{{ __('JsPress::backend.max_file_upload') }}</a>
            <span class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.max_file_upload_text') }}</span>
        </div>
    </div>
    <div class="w-75 p-5">
        <input type="text" name="max_file" class="form-control mb-2 mb-md-0"/>
    </div>
</div>
@if($callback)
    <div class="d-flex w-100 border-bottom" data-condition="on" data-display="off" data-fields='["repeater"]'>
        <div class="w-25 bg-light p-5">
            <div class="d-flex justify-content-start flex-column">
                <a href="javascript:void(0);" class="text-dark fw-bolder text-hover-primary fs-6">{{ __('JsPress::backend.post_field.repeater_fields') }}</a>
                <span class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.post_field.repeater_fields_description') }}</span>
            </div>
        </div>
        <div class="w-75 p-5 repeaterBoxField">
            <ul class="d-flex w-100 pb-4 border-bottom border-bottom-dashed list-unstyled p-5">
                <li class="w-25 fs-5 fw-bolder text-muted">{{ __('JsPress::backend.post_field.order') }}</li>
                <li class="w-25 fs-5 fw-bolder text-muted">{{ __('JsPress::backend.post_field.field_label') }}</li>
                <li class="w-25 fs-5 fw-bolder text-muted">{{ __('JsPress::backend.post_field.field_key') }}</li>
                <li class="w-25 fs-5 fw-bolder text-muted">{{ __('JsPress::backend.post_field.field_type') }}</li>
            </ul>
            <div class="inner-repeater">
                @include('JsPress::admin.post_field.content', ['callback'=>false])
            </div>
        </div>
    </div>
@endif
<div class="d-flex w-100 border-bottom" data-condition="on" data-display="off" data-fields='["selectbox","checkbox","radio"]'>
    <div class="w-25 bg-light p-5">
        <div class="d-flex justify-content-start flex-column">
            <a href="javascript:void(0);" class="text-dark fw-bolder text-hover-primary fs-6">{{ __('JsPress::backend.select_option_type') }}</a>
            <p class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.select_option_type_description') }} </p>
        </div>
    </div>
    <div class="w-75 p-5">
        <select class="form-select select_option_items" name="option_type">
            <option value="manual">{{ __('JsPress::backend.manual') }}</option>
            <option value="database">{{ __('JsPress::backend.database') }}</option>
        </select>
    </div>
</div>
<div class="d-flex w-100 border-bottom select_database_field" data-display="off">
    <div class="w-25 bg-light p-5">
        <div class="d-flex justify-content-start flex-column">
            <a href="javascript:void(0);" class="text-dark fw-bolder text-hover-primary fs-6">{{ __('JsPress::backend.database_post_field_title') }}</a>
            <p class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.database_post_field_description') }} </p>
        </div>
    </div>
    <div class="w-75 p-5">
        <div class="row">
            <div class="col-lg-4">
                <label>Veritabanını Seçiniz</label>
                <select class="form-select select_options_table" name="select_options_table">
                    @foreach($table_options as $table)
                        <option value="{{$table['name']}}">{{$table['name']}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-lg-4">
                <label>Value Değerini Seçiniz</label>
                <select class="form-select select_options_column_value" name="select_options_column_value">

                </select>
            </div>
            <div class="col-lg-4">
                <label>Label Değerini Seçiniz</label>
                <select class="form-select select_options_column_label" name="select_options_column_label">

                </select>
            </div>
        </div>
    </div>
</div>
<div class="d-flex w-100 border-bottom select_manual_field" data-condition="on" data-display="off" data-fields='["selectbox", "checkbox", "radio"]'>
    <div class="w-25 bg-light p-5">
        <div class="d-flex justify-content-start flex-column">
            <a href="javascript:void(0);" class="text-dark fw-bolder text-hover-primary fs-6">{{ __('JsPress::backend.post_field.chooses') }}</a>
            <p class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.post_field.chooses_description_first_line') }} </p>
            <p class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.post_field.chooses_description_second_line') }}</p>
            <p class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.post_field.chooses_description_third_line') }}</p>
        </div>
    </div>
    <div class="w-75 p-5">
        <textarea name="options" rows="6" class="form-control w-100 mb-2 mb-md-0"></textarea>
    </div>
</div>
<div class="d-flex w-100 border-bottom" data-condition="on" data-display="off" data-fields='["selectbox","category_object", "post_object"]'>
    <div class="w-25 bg-light p-5">
        <div class="d-flex justify-content-start flex-column">
            <a href="javascript:void(0);" class="text-dark fw-bolder text-hover-primary fs-6">{{ __('JsPress::backend.post_field.multiple_chooses') }}</a>
            <span class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.post_field.multiple_chooses_description') }}</span>
        </div>
    </div>
    <div class="w-75 p-5">
        <label class="form-check form-switch form-check-custom form-check-solid">
            <input class="form-check-input w-60px" name="is_multiple" type="checkbox" value="1"/>
        </label>
    </div>
</div>
<div class="d-flex w-100 border-bottom" data-condition="on" data-display="off" data-fields='["switch"]'>
    <div class="w-25 bg-light p-5">
        <div class="d-flex justify-content-start flex-column">
            <a href="javascript:void(0);" class="text-dark fw-bolder text-hover-primary fs-6">{{ __('JsPress::backend.post_field.message') }}</a>
            <span class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.post_field.message_description') }}</span>
        </div>
    </div>
    <div class="w-75 p-5">
        <input type="text" name="message" class="form-control mb-2 mb-md-0"/>
    </div>
</div>
<div class="d-flex w-100 border-bottom" data-condition="on" data-display="off" data-fields='["switch"]'>
    <div class="w-25 bg-light p-5">
        <div class="d-flex justify-content-start flex-column">
            <a href="javascript:void(0);" class="text-dark fw-bolder text-hover-primary fs-6">{{ __('JsPress::backend.post_field.default_value') }}</a>
            <span class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.post_field.default_value_switch_description') }}</span>
        </div>
    </div>
    <div class="w-75 p-5">
        <label class="form-check form-switch form-check-custom form-check-solid">
            <input class="form-check-input w-60px" name="is_open" type="checkbox" value="1"/>
        </label>
    </div>
</div>
<div class="d-flex w-100 border-bottom">
    <div class="w-25 bg-light p-5">
        <div class="d-flex justify-content-start flex-column">
            <a href="javascript:void(0);" class="text-dark fw-bolder text-hover-primary fs-6">{{ __('JsPress::backend.post_field.style_properties') }}</a>
            <span class="text-muted fw-bold text-muted d-block fs-7">{{ __('JsPress::backend.post_field.style_properties_description') }}</span>
        </div>
    </div>
    <div class="w-75 p-5">
        <div class="row">
            <div class="col">
                <div class="input-group mb-5 ">
                    <span class="input-group-text">{{ __('JsPress::backend.post_field.width') }}</span>
                    <input type="text" name="width" class="form-control" />
                    <span class="input-group-text">%</span>
                </div>
            </div>
            <div class="col">
                <div class="input-group mb-5">
                    <span class="input-group-text">Class</span>
                    <input type="text" name="classes" class="form-control" />
                </div>
            </div>
            <div class="col">
                <div class="input-group mb-5">
                    <span class="input-group-text">ID</span>
                    <input type="text" name="ids" class="form-control" />
                </div>
            </div>
        </div>
    </div>
</div>
