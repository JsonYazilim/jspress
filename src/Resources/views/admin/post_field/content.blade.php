<div class="draggable-zone-child" data-repeater-list="items">
    <div class="fieldWrapsChild draggable child" data-repeater-item>
        <div class="fieldContainer w-100 border-bottom">
            <div class="meta d-none">
                <input type="hidden" name="order" class="orderField" value="1"/>
            </div>
            <ul class="d-flex w-100 pb-4 list-unstyled p-5">
                <li class="w-25 draggable-handle child">
                    <div class="w-25px h-25px border rounded-3 text-center orderFieldText" style="line-height:25px;">1</div>
                </li>
                <li class="w-25 cursor-pointer">
                    <div class="d-flex justify-content-start flex-column">
                        <span class="fw-bolder text-primary labelField">No Label</span>
                        <ul class="d-flex list-unstyled">
                            <li data-repeater-delete><span class="fs-8 text-danger">{{ __('JsPress::backend.remove') }}</span></li>
                        </ul>
                    </div>

                </li>
                <li class="w-25 keyText">-</li>
                <li class="w-25">{{ __('JsPress::backend.post_field.text') }}</li>
            </ul>
            <div class="fieldItems w-100" style="display: none;">
                @include('JsPress::admin.post_field.repeater_fields', ['callback'=>$callback])
            </div>
        </div>
    </div>
</div>
<div class="bg-gray-400 w-100">
    <div class="d-flex justify-content-end p-5">
        <button type="button" class="btn btn-primary btn-sm" data-repeater-create>{{ __('JsPress::backend.post_field.add_field') }}</button>
    </div>
</div>
