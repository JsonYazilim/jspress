@extends('JsPress::admin.layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Sistem Ayarları</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                <div class="row g-5 g-xl-8">
                    <div class="col-xl-6">
                        <div class="card card-xl-stretch mb-xl-8">
                            <div class="card-body d-flex align-items-center pt-3 pb-0">
                                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                                    <a href="javascript:void(0);" class="fw-bolder text-dark fs-4 mb-2 text-hover-primary">Cache Temizle</a>
                                    <span class="fw-bold text-muted fs-5">Uygulama içerisinde yaptığınız değişikliklerden sonra cacheleri temizlemeniz verinin güncel olmasında yararlı olacaktır.</span>
                                </div>
                                <button type="button" class="btn btn-bg-light btn-active-color-success min-w-100px systemButton" data-type="cache" data-bs-toggle="tooltip" data-bs-placement="top" title="Cache Temizle">
                                    <span class="indicator-label">
                                        <span class="svg-icon svg-icon-2x">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-power" viewBox="0 0 16 16">
                                                <path d="M7.5 1v7h1V1h-1z"/>
                                                <path d="M3 8.812a4.999 4.999 0 0 1 2.578-4.375l-.485-.874A6 6 0 1 0 11 3.616l-.501.865A5 5 0 1 1 3 8.812z"/>
                                            </svg>
                                        </span>
                                    </span>
                                    <span class="indicator-progress">
                                        <span class="spinner-border spinner-border-lg align-middle"></span>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="card card-xl-stretch mb-xl-8">
                            <div class="card-body d-flex align-items-center pt-3 pb-0">
                                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                                    <a href="javascript:void(0);" class="fw-bolder text-dark fs-4 mb-2 text-hover-primary">Config Temizle</a>
                                    <span class="fw-bold text-muted fs-5">Uygulama içerisinde config ayarlarında yaptığınız değişikliklerin algılanması için temizleyiniz.</span>
                                </div>
                                <button class="btn btn-bg-light btn-active-color-success min-w-100px systemButton" data-type="config" data-bs-toggle="tooltip" data-bs-placement="top" title="Config Temizle">
                                    <span class="svg-icon svg-icon-2x">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-power" viewBox="0 0 16 16">
                                            <path d="M7.5 1v7h1V1h-1z"/>
                                            <path d="M3 8.812a4.999 4.999 0 0 1 2.578-4.375l-.485-.874A6 6 0 1 0 11 3.616l-.501.865A5 5 0 1 1 3 8.812z"/>
                                        </svg>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="card card-xl-stretch mb-xl-8">
                            <div class="card-body d-flex align-items-center pt-3 pb-0">
                                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                                    <a href="javascript:void(0);" class="fw-bolder text-dark fs-4 mb-2 text-hover-primary">View Temizle</a>
                                    <span class="fw-bold text-muted fs-5">Görünüm blade dosyaları içerisinde yaptığınız değişikliklerin algılanması için temizleyiniz.</span>
                                </div>
                                <button class="btn btn-bg-light btn-active-color-success min-w-100px systemButton" data-type="view" data-bs-toggle="tooltip" data-bs-placement="top" title="View Temizle">
                                    <span class="svg-icon svg-icon-2x">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-power" viewBox="0 0 16 16">
                                            <path d="M7.5 1v7h1V1h-1z"/>
                                            <path d="M3 8.812a4.999 4.999 0 0 1 2.578-4.375l-.485-.874A6 6 0 1 0 11 3.616l-.501.865A5 5 0 1 1 3 8.812z"/>
                                        </svg>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="card card-xl-stretch mb-xl-8">
                            <div class="card-body d-flex align-items-center pt-3 pb-0">
                                <div class="d-flex flex-column flex-grow-1 py-2 py-lg-13 me-2">
                                    <a href="javascript:void(0);" class="fw-bolder text-dark fs-4 mb-2 text-hover-primary">Route Temizle</a>
                                    <span class="fw-bold text-muted fs-5">Route sınıfları içerisinde yaptığınız değişikliklerin algılanması için temizleyiniz.</span>
                                </div>
                                <button class="btn btn-bg-light btn-active-color-success min-w-100px systemButton" data-type="route" data-bs-toggle="tooltip" data-bs-placement="top" title="Route Temizle">
                                    <span class="svg-icon svg-icon-2x">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-power" viewBox="0 0 16 16">
                                            <path d="M7.5 1v7h1V1h-1z"/>
                                            <path d="M3 8.812a4.999 4.999 0 0 1 2.578-4.375l-.485-.874A6 6 0 1 0 11 3.616l-.501.865A5 5 0 1 1 3 8.812z"/>
                                        </svg>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $('.systemButton').on('click', function(){
            let el = $(this)[0];
            el.setAttribute("data-kt-indicator", "on");
            el.disabled = true;
            let type = $(this).attr('data-type');
            $.ajax({
                url:'{!! Route('admin.system.clear') !!}',
                type:'POST',
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:{type:type},
                dataType:'json',
                success:function(res){
                    if(res.status == 1){
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toastr-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        toastr.success(res.msg);
                    }else{
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toastr-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        toastr.error(res.msg);
                    }
                    el.removeAttribute("data-kt-indicator");
                    el.disabled = false;
                }
            });
        });
    </script>
@endpush
