<div class="col-md-6 col-lg-6 col-xl-6 col-xxl-3 mb-md-5 mb-xl-10">
    <div class="card card-flush h-md-50 mb-5 mb-xl-10">
        <div class="card-header pt-5">
            <div class="card-title d-flex flex-column">
                <div class="d-flex align-items-center">
                    <span class="fs-1 fw-bolder text-dark me-2 lh-1 ls-n2 todaySaleGrandTotal"></span>
                    <span class="badge fs-base classPlacement">
                        <span class="svg-icon svg-icon-5 svg-icon-white ms-n1 iconPlacement"></span>
                        <span class="percentageOfChanges"></span>%</span>
                </div>
                <span class="text-gray-400 pt-1 fw-bold fs-6">Bugün Toplam Satış</span>
            </div>
        </div>
        <div class="card-body d-flex align-items-end px-0 pb-0">
            <div id="kt_card_widget_6_chart" class="w-100" style="height: 80px"></div>
        </div>
    </div>
    <div class="card card-flush h-md-50 mb-5 mb-xl-10">
        <div class="card-header pt-5">
            <div class="card-title d-flex flex-column">
                <div class="d-flex align-items-center">
                    <span class="fs-1 fw-bolder text-dark me-2 lh-1 ls-n2 todaySaleCommissionTotal"></span>
                    <span class="badge fs-base classPlacement">
                        <span class="svg-icon svg-icon-5 svg-icon-white ms-n1 iconPlacement"></span>
                        <span class="percentageOfChangesCommission"></span>%</span>
                </div>
                <span class="text-gray-400 pt-1 fw-bold fs-6">Bugün Toplam Kazanç</span>
            </div>
        </div>
        <div class="card-body pt-2 pb-4 d-flex align-items-center">
            <div class="d-flex flex-column content-justify-center w-100">
                <div class="d-flex fs-6 fw-bold align-items-center">
                    <div class="bullet w-8px h-6px rounded-2 bg-danger me-3"></div>
                    <div class="text-gray-500 flex-grow-1 me-4">Tek Yön</div>
                    <div class="fw-boldest text-gray-700 text-xxl-end oneWayPriceText"></div>
                </div>
                <div class="d-flex fs-6 fw-bold align-items-center my-3">
                    <div class="bullet w-8px h-6px rounded-2 bg-primary me-3"></div>
                    <div class="text-gray-500 flex-grow-1 me-4">Gidiş - Dönüş</div>
                    <div class="fw-boldest text-gray-700 text-xxl-end returnPriceText"></div>
                </div>
                <div class="d-flex fs-6 fw-bold align-items-center">
                    <div class="bullet w-8px h-6px rounded-2 me-3" style="background-color: #E4E6EF"></div>
                    <div class="text-gray-500 flex-grow-1 me-4">Paket Uçuş</div>
                    <div class="fw-boldest text-gray-700 text-xxl-end packagePriceText"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-12 col-xl-12 col-xxl-9 mb-5 mb-xl-0">
    <div class="card card-flush overflow-hidden h-md-100">
        <div class="card-header py-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder text-dark">Son 6 Aylık Satış Raporu</span>
                <span class="text-gray-400 mt-1 fw-bold fs-6">Sistem içerisinde ki son 6 aylık satış rapor özetini inceleyebilirsiniz.</span>
            </h3>
        </div>
        <div class="card-body d-flex justify-content-between flex-column pb-1 px-0">
            <div class="px-9 mb-5 grandTotalBox" style="display:none;">
                <div class="d-flex mb-2">
                    <span class="fs-2hx fw-bolder text-gray-800 me-2 lh-1 ls-n2 grandTotalText"></span>
                    <span class="fs-2hx fw-bolder text-gray-800 me-2 lh-1 ls-n2 me-1">TL</span>
                </div>
                <span class="fs-6 fw-bold text-gray-400">Toplam rezervasyon satış tutarıdır.</span>
            </div>
            <div id="kt_charts_widget_3" class="min-h-auto ps-4 pe-6" style="height: 300px"></div>
        </div>
    </div>
</div>
@push('script')
    <script>
        window.weekly_chart = false;
        window.sale_charts = false;
        function renderSaleChart(data){
            var element = document.getElementById('kt_charts_widget_3');

            var height = parseInt(KTUtil.css(element, 'height'));
            var labelColor = KTUtil.getCssVariableValue('--bs-gray-500');
            var borderColor = KTUtil.getCssVariableValue('--bs-border-dashed-color');
            var baseColor = KTUtil.getCssVariableValue('--bs-success');
            var secondaryColor = KTUtil.getCssVariableValue('--bs-light-success');

            if (!element) {
                return;
            }
            var options = {
                series: [{ name: "Rezervasyon", data: data.sales }],
                chart: { fontFamily: "inherit", type: "area", height: height, toolbar: { show: !1 } },
                plotOptions: {},
                legend: { show: !1 },
                dataLabels: {
                    enabled: true
                },
                fill:{type:"gradient",gradient:{shadeIntensity:1,opacityFrom:.4,opacityTo:0,stops:[0,80,100]}},
                stroke: { curve: "smooth", show: !0, width: 3, colors: [baseColor] },
                xaxis: {
                    categories: data.months,
                    axisBorder: { show: !1 },
                    axisTicks: { show: !1 },
                    labels:{rotate:0,rotateAlways:!0,style:{colors:labelColor,fontSize:"12px"}},
                    crosshairs:{position:"front",stroke:{color:baseColor,width:1,dashArray:3}},
                    tooltip: { enabled: !0, formatter: void 0, offsetY: 0, style: { fontSize: "12px" } },
                },
                yaxis: { labels: { style: { colors: labelColor, fontSize: "12px" } } },
                states: { normal: { filter: { type: "none", value: 0 } }, hover: { filter: { type: "none", value: 0 } }, active: { allowMultipleDataPointsSelection: !1, filter: { type: "none", value: 0 } } },
                tooltip: {
                    style: { fontSize: "12px" },
                    y: {
                        formatter: function (e) {
                            return e + " satış";
                        },
                    },
                },
                colors: [secondaryColor],
                grid: { borderColor: borderColor, strokeDashArray: 4, yaxis: { lines: { show: !0 } } },
                markers: { strokeColor: baseColor, strokeWidth: 3 },
            };
            var chart = new ApexCharts(element, options);
            chart.render().then(() => window.sale_charts = chart);;
        }
        function renderWeeklySaleChart(data){
            var e = document.getElementById("kt_card_widget_6_chart");
            var a = parseInt(KTUtil.css(e, "height")),
                t = KTUtil.getCssVariableValue("--bs-gray-500"),
                l = KTUtil.getCssVariableValue("--bs-border-dashed-color"),
                o = KTUtil.getCssVariableValue("--bs-primary"),
                r = KTUtil.getCssVariableValue("--bs-gray-300");
            var options = {
                series: [{ name: "Toplam Satış", data: data.sales }],
                chart: { fontFamily: "inherit", type: "bar", height: a, toolbar: { show: !1 }, sparkline: { enabled: !0 } },
                plotOptions: { bar: { horizontal: !1, columnWidth: ["55%"], borderRadius: 6 } },
                legend: { show: !1 },
                dataLabels: {
                    enabled: false,
                    offsetY: -20,
                },
                stroke: { show: !0, width: 9, colors: ["transparent"] },
                xaxis: { categories: data.days, axisBorder: { show: !1 }, axisTicks: { show: !1, tickPlacement: "between" }, labels: { show: !1, style: { colors: t, fontSize: "12px" } }, crosshairs: { show: !1 } },
                yaxis: { labels: { show: !1, style: { colors: t, fontSize: "12px" } } },
                fill: { type: "solid" },
                states: { normal: { filter: { type: "none", value: 0 } }, hover: { filter: { type: "none", value: 0 } }, active: { allowMultipleDataPointsSelection: !1, filter: { type: "none", value: 0 } } },
                tooltip: {
                    style: { fontSize: "12px" },
                },
                colors: [o, r],
                grid: { padding: { left: 10, right: 10 }, borderColor: l, strokeDashArray: 4, yaxis: { lines: { show: !0 } } },
            };

            if (e) {
                let i = new ApexCharts(e, options);
                setTimeout(function () {
                    i.render().then(() => window.weekly_chart = i);
                }, 300);
            }
        }
        var loadReports = function(){
            $.ajax({
                url:'/',
                type:'POST',
                dataType:'json',
                success:function(res){
                    if(window._is_SuperAdmin){
                        $('.grandTotalText').text(res.total);
                        $('.grandTotalBox').show();
                    }else{
                        $('.grandTotalBox').hide();
                    }
                    $('.todaySaleGrandTotal').text(res.days.today+' TL');
                    $('.percentageOfChanges').text(res.days.percentage);
                    $('.classPlacement').addClass(res.days.template.class);
                    $('.iconPlacement').html(res.days.template.icon);
                    $('.todaySaleCommissionTotal').text(res.commissions.days.today+' TL');
                    $('.percentageOfChangesCommission').text(res.commissions.days.percentage);
                    $('.oneWayPriceText').text(res.flight_types.one_way+ ' TL');
                    $('.returnPriceText').text(res.flight_types.return+ ' TL');
                    $('.packagePriceText').text(res.flight_types.package+ ' TL');
                    renderSaleChart(res);
                    renderWeeklySaleChart(res.week);
                }
            });
        }
        loadReports();
        setInterval(function() {
            if (window.weekly_chart) {
                window.weekly_chart.destroy();
                window.weekly_chart = false;
            }
            if (window.sale_charts) {
                window.sale_charts.destroy();
                window.sale_charts = false;
            }
            loadReports();
        }, 60 * 1000);

    </script>
@endpush
