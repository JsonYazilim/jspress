<div class="col-lg-12 col-xl-12 col-xxl-12 mb-5 mb-xl-0">
    <div class="card card-flush overflow-hidden h-md-100">
        <div class="card-header py-5">
            <h3 class="card-title align-items-start flex-column">
                <span class="card-label fw-bolder text-dark">Gerçek Zamanlı Arama</span>
                <span class="text-gray-400 mt-1 fw-bold fs-6">Sistem içerisinde ki uçuş aramalarını gerçek zamanlı görüntüleyebilirsiniz.</span>
            </h3>
        </div>
    </div>
</div>
<div id="kt_docs_vistimeline_template"></div>
@push('script')
    <script>
        const data = `<table class="score">
            <tr>
                <td colspan="3" class="description">
                    @{{flight_type}}</br>@{{ip}}
            </td>
        </tr>
        <tr>
            <td>@{{ origin }}</td>
            <th>-</th>
                <td>@{{ destination }}</td>
            </tr>
            <tr>
                <td>
                    <img
                    src="https://flagpedia.net/data/flags/mini/@{{ origin_country }}.png"
                    width="31"
                    height="20"
                    alt="@{{ origin_country }}"
                    />
                </td>
            <th></th>
                <td>
                    <img
                    src="https://flagpedia.net/data/flags/mini/@{{ destination_country }}.png"
                    width="31"
                    height="20"
                    alt="@{{ destination_country }}"
                    />
                </td>
            </tr>
        </table>`;
        window._timeline = false;
        // Private functions
        var renderTimeLine = function (res) {
            if(!window._timeline){
                var template = Handlebars.compile(data);
                var container = document.getElementById("kt_docs_vistimeline_template");
                var items = new vis.DataSet(res);
                var options = {
                    template: template,
                    stack: false,
                    margin: { axis: 5 },
                    height: '250px',
                };

                var timeline = new vis.Timeline(container, items, options);
                window._timeline = timeline;
            }else{
                window._timeline.setItems({});
                window._timeline.itemsData.update(res);
            }


        }
        var loadSearchTimeline = function(){
            $.ajax({
                url:'/',
                type:'POST',
                dataType:'json',
                success:function(res){
                    renderTimeLine(res);
                }
            });
        }
        loadSearchTimeline();
        setInterval(function() {
            loadSearchTimeline();
        }, 60 * 1000);
    </script>
@endpush
