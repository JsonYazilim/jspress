@extends('JsPress::admin.layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{__('JsPress::backend.site_settings')}}</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                <div class="flex-lg-row-fluid ms-lg-15">
                    @if(\Session::has('success'))
                        @include('JsPress::admin.layouts.alerts.success', ['message'=>session('success')])
                    @endif
                    <ul class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-bold mb-8">

                        @foreach($tabs as $key => $tab)
                        <li class="nav-item">
                            <a class="nav-link text-active-primary pb-4 {{ $key == 0 ? 'active':'' }}" data-bs-toggle="tab" href="#{{$tab['id']}}">
                                <span>{!! $tab['icon'] !!}</span>
                                {{__js('setting', 'setting_'.$tab['id'], $tab['title'])}}</a>
                        </li>
                        @endforeach
                    </ul>
                    <form id="settings_form" class="form" action="{!! Route('admin.setting.store') !!}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="tab-content" id="myTabContent">
                            @foreach($tabs as $key => $tab)
                            <div class="tab-pane fade {{$key == 0 ? 'show active':''}}" id="{{$tab['id']}}" role="tabpanel">
                                <div class="card card-flush">
                                    <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                                        <div class="card-title">
                                            <h2>{{__js('setting', 'setting_'.$tab['id'], $tab['title'])}}</h2>
                                        </div>
                                    </div>
                                    <div class="card-body pt-0">
                                        <div class="d-flex flex-wrap">
                                            @foreach($tab['settings'] as $setting)
                                                @if($setting['type'] == 'image')
                                                    <div class="{{ isset($setting['width']) ? 'w-'.$setting['width'] : ''  }} mb-5">
                                                        <label class="fs-6 fw-bold form-label mt-3">
                                                            <span>{{__js('setting', 'setting_'.$tab['id'].'_'.$setting['name'], $setting['label'])}}</span>
                                                        </label>
                                                        <x-jspress-file-upload-button
                                                            :id="'setting_'.$tab['id'].'_'.$setting['name']"
                                                            type="image"
                                                            :maxfile="1"
                                                            action="single_file_upload"
                                                            name="settings[{{$setting['name']}}]"
                                                            :image="setting($setting['name'])"
                                                        >
                                                        </x-jspress-file-upload-button>
                                                    </div>
                                                @endif
                                                @if($setting['type'] == 'text')
                                                    <div class="{{ isset($setting['width']) ? 'w-'.$setting['width'] : ''  }} mb-5">
                                                        <label class="fs-6 fw-bold form-label mt-3">
                                                            <span>{{__js('setting', 'setting_'.$tab['id'].'_'.$setting['name'], $setting['label'])}}</span>
                                                        </label>
                                                        <x-jspress-text
                                                            width="100"
                                                            name="settings[{{$setting['name']}}]"
                                                            :label="null"
                                                            type="text"
                                                            placeholder=""
                                                            required="0"
                                                            classes=""
                                                            id=""
                                                            :value="setting($setting['name'])"
                                                        >
                                                        </x-jspress-text>
                                                    </div>
                                                @endif
                                                @if($setting['type'] == 'textarea')
                                                    <div class="{{ isset($setting['width']) ? 'w-'.$setting['width'] : ''  }} mb-5">
                                                        <label class="fs-6 fw-bold form-label mt-3">
                                                            <span>{{__js('setting', 'setting_'.$tab['id'].'_'.$setting['name'], $setting['label'])}}</span>
                                                        </label>
                                                        <x-jspress-textarea
                                                            :width="100"
                                                            name="settings[{{$setting['name']}}]"
                                                            :label="null"
                                                            type="text"
                                                            placeholder=""
                                                            required="0"
                                                            classes=""
                                                            id=""
                                                            :value="setting($setting['name'])"
                                                        >
                                                        </x-jspress-textarea>
                                                    </div>
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="mt-5">
                            <button type="submit" class="btn btn-success">
                                <span class="indicator-label">{{__('JsPress::backend.save')}}</span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection
