@if(count($fields) > 0)
    @foreach($fields as $key => $field)
        <div class="mb-10">
            @if($field['appearence'] === 'accordion')
                @include('JsPress::admin.modules.post_fields.accordion', ['field'=>$field])
            @endif
            @if($field['appearence'] === 'card')
                @include('JsPress::admin.modules.post_fields.card', ['field'=>$field])
            @endif
            @if($field['appearence'] === 'none')
                @include('JsPress::admin.modules.post_fields.fields', ['field'=>$field])
            @endif
        </div>
    @endforeach
@endif
