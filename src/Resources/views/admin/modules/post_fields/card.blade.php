<div class="card card-flush py-4">
    <div class="card-header">
        <div class="card-title">
            <h2>{{ __js('post_field', 'post_field_title_'.$field['key'], $field['title']) }}</h2>
        </div>
    </div>
    <div class="card-body pt-0">
        @include('JsPress::admin.modules.post_fields.fields')
    </div>
</div>
