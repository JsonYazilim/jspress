<div class="accordion" id="{{ $field['key'] }}">
    <div class="accordion-item">
        <h2 class="accordion-header" id="{{ $field['key'] }}_header">
            <button class="accordion-button fs-4 fw-bold {{ $field['is_accordion_open'] == 1 ? '':'collapsed' }}" type="button" data-bs-toggle="collapse" data-bs-target="#{{ $field['key'] }}_body" aria-expanded="false" aria-controls="{{ $field['key'] }}_body">
                {{ __js('post_field', 'post_field_title_'.$field['key'], $field['title']) }}
            </button>
        </h2>
        <div id="{{ $field['key'] }}_body" class="accordion-collapse collapse {{ $field['is_accordion_open'] == 1 ? 'show':'' }}" aria-labelledby="{{ $field['key'] }}_header" data-bs-parent="#{{ $field['key'] }}">
            <div class="accordion-body">
                @include('JsPress::admin.modules.post_fields.fields')
            </div>
        </div>
    </div>
</div>
