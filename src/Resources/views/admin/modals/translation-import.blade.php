<div class="modal fade" id="import_translation_modal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="fw-bolder">Çeviri İçe Aktar</h2>
                <div id="import_translation_modal_close" class="btn btn-icon btn-sm btn-active-icon-primary">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                             viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"/>
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"/>
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
                <form id="import_translation_form" class="form" action="{{ Route('admin.translation.import', ['locale'=> $locale]) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="fv-row mb-10">
                        <label class="fs-5 fw-bold form-label mb-5">Dosya Seçiniz:</label>
                        <input name="translation" type="file" class="form-control form-control-solid"/>
                    </div>
                    <div class="text-end">
                        <button type="submit" class="btn btn-primary"><span class="indicator-label">İçe Aktar</span></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
