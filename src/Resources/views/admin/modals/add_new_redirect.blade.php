<div class="modal fade" id="add_new_redirect" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content">
            <form class="form" action="{{route('admin.redirect.store')}}" id="add_new_redirect_form" method="POST">
                @csrf
                <div class="modal-header" id="add_new_redirect_header">
                    <h2 class="fw-bolder">{{ __('JsPress::backend.add_new_redirect') }}</h2>
                    <div id="add_new_redirect_close" data-bs-dismiss="modal" class="btn btn-icon btn-sm btn-active-icon-primary">
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                            </svg>
                        </span>
                    </div>
                </div>
                <div class="modal-body py-10 px-lg-17">
                    <div class="scroll-y me-n7 pe-7" id="add_new_redirect_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_customer_header" data-kt-scroll-wrappers="#kt_modal_add_customer_scroll" data-kt-scroll-offset="300px">
                        <div class="fv-row mb-7">
                            <label class="required fs-6 fw-bold mb-2">{{ __('JsPress::backend.redirected_url') }}</label>
                            <input type="text" class="form-control form-control-solid" name="old_url" />
                            <small class="pt-2 text-danger"> {{ __('JsPress::backend.old_url_input_desc') }} </small>
                        </div>
                        <div class="fv-row mb-7">
                            <label class="required fs-6 fw-bold mb-2">{{ __('JsPress::backend.redirect_url') }}</label>
                            <input type="text" class="form-control form-control-solid" name="new_url" />
                            <small class="pt-2 text-danger"> {{ __('JsPress::backend.new_url_input_desc') }} </small>
                        </div>
                        <div class="fv-row mb-15">
                            <label class="fs-6 fw-bold mb-2">Description</label>
                            <select name="status_code" aria-label="{{ __('JsPress::backend.choose') }}" data-control="select2" data-placeholder="{{ __('JsPress::backend.choose') }}" data-dropdown-parent="#add_new_redirect" class="form-select form-select-solid fw-bolder">
                                <option value="301">Permanent 301</option>
                                <option value="302">Normal 302</option>
                                <option value="307">Temporary 307</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer flex-center">
                    <button type="reset" id="add_new_redirect_cancel" data-bs-dismiss="modal" class="btn btn-light me-3">{{__('JsPress::backend.close')}}</button>
                    <button type="submit" id="add_new_redirect_submit" class="btn btn-primary">
                        <span class="indicator-label">{{__('JsPress::backend.add_new')}}</span>
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
