<div class="table-responsive">
    <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_table_users">
        <thead>
        <tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
            <th class="w-10px pe-2">
                <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                    <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_table_users .form-check-input" value="1" />
                </div>
            </th>
            <th class="min-w-125px">{!! __('JsPress::backend.post_type.post_type_table_name') !!}</th>
            <th class="min-w-60px">{!! __('JsPress::backend.post_type.key') !!}</th>
            <th class="min-w-60px">{!! __('JsPress::backend.post_type.is_category') !!}</th>
            <th class="min-w-60px">{!! __('JsPress::backend.post_type.is_terms') !!}</th>
            <th class="min-w-60px">{!! __('JsPress::backend.post_type.is_image') !!}</th>
            <th class="min-w-60px">{!! __('JsPress::backend.post_type.is_seo') !!}</th>
            <th class="min-w-60px">{!! __('JsPress::backend.post_type.is_url') !!}</th>
            <th class="min-w-60px">{!! __('JsPress::backend.post_type.is_menu') !!}</th>
            <th class="min-w-60px">{!! __('JsPress::backend.status') !!}</th>
            <th class="min-w-60px">{!! __('JsPress::backend.order') !!}</th>
            <th class="text-end min-w-100px">{!! __('JsPress::backend.actions') !!}</th>
        </tr>
        </thead>
        <tbody class="text-gray-600 fw-bold">
        @if( $post_types->isNotEmpty() )
            @foreach( $post_types as $post_type )
                <tr class="user-row" data-id="{!! $post_type->id !!}">
                    <td>
                        <div class="form-check form-check-sm form-check-custom form-check-solid">
                            <input class="form-check-input" name="user_id" type="checkbox" value="{!! $post_type->id !!}" />
                        </div>
                    </td>
                    <td>
                        {!! __js('post_type', JsPressPanel::postTypeLangKey($post_type, 'list'), $post_type->name) !!}
                    </td>
                    <td>
                        {!! $post_type->key !!}
                    </td>
                    <td>
                        @if($post_type->is_category == 1)
                            <x-jspress-true-icon></x-jspress-true-icon>
                        @else
                            <x-jspress-false-icon></x-jspress-false-icon>
                        @endif
                    </td>
                    <td>
                        @if($post_type->is_terms == 1)
                            <x-jspress-true-icon></x-jspress-true-icon>
                        @else
                            <x-jspress-false-icon></x-jspress-false-icon>
                        @endif
                    </td>
                    <td>
                        @if($post_type->is_image == 1)
                            <x-jspress-true-icon></x-jspress-true-icon>
                        @else
                            <x-jspress-false-icon></x-jspress-false-icon>
                        @endif
                    </td>
                    <td>
                        @if($post_type->is_seo == 1)
                            <x-jspress-true-icon></x-jspress-true-icon>
                        @else
                            <x-jspress-false-icon></x-jspress-false-icon>
                        @endif
                    </td>
                    <td>
                        @if($post_type->is_url == 1)
                            <x-jspress-true-icon></x-jspress-true-icon>
                        @else
                            <x-jspress-false-icon></x-jspress-false-icon>
                        @endif
                    </td>
                    <td>
                        @if($post_type->is_menu == 1)
                            <x-jspress-true-icon></x-jspress-true-icon>
                        @else
                            <x-jspress-false-icon></x-jspress-false-icon>
                        @endif
                    </td>
                    <td>
                        @if( $post_type->status == 1 )
                            <span class="badge badge-light-success">{{__('JsPress::backend.active')}}</span>
                        @else
                            <span class="badge badge-light-danger">{{__('JsPress::backend.deactive')}}</span>
                        @endif
                    </td>
                    <td>{{$post_type->order}}</td>
                    <td class="text-end">
                        <button class="btn btn-icon btn-bg-light btn-active-color-{{$post_type->status == 1 ? 'danger':'success'}} btn-sm me-1 toggleStatus" data-id="{!! $post_type->id !!}" data-status="{!! $post_type->status == 1 ? 0 : 1 !!}" data-bs-toggle="tooltip" data-bs-placement="top" title="{{ $post_type->status == 1 ? __('JsPress::backend.deactivate') : __('JsPress::backend.activate') }}">
                            <span class="svg-icon svg-icon-3 svg-icon-{{$post_type->status == 1 ? 'danger':'success'}}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-power" viewBox="0 0 16 16">
                                  <path d="M7.5 1v7h1V1h-1z"/>
                                  <path d="M3 8.812a4.999 4.999 0 0 1 2.578-4.375l-.485-.874A6 6 0 1 0 11 3.616l-.501.865A5 5 0 1 1 3 8.812z"/>
                                </svg>
                            </span>
                        </button>
                        <a href="{{route('admin.post_type.edit', ['post_type' => $post_type->id])}}" class="btn btn-icon btn-bg-light btn-active-color-warning btn-sm me-1" data-bs-toggle="tooltip" data-bs-placement="top" title="{{ __('JsPress::backend.edit') }}">
                            <span class="svg-icon svg-icon-3 svg-icon-warning">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z" fill="black"/>
                                    <path d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z" fill="black"/>
                                    <path d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z" fill="black"/>
                                </svg>
                            </span>
                        </a>
                        <button type="button" class="btn btn-icon btn-bg-light btn-active-color-danger btn-sm me-1 deletePostType" data-id="{{$post_type->id}}" data-bs-toggle="tooltip" data-bs-placement="top" title="{{ __('JsPress::backend.delete') }}">
                            <span class="svg-icon svg-icon-3 svg-icon-danger">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="black"/>
                                    <path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="black"/>
                                    <path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="black"/>
                                </svg>
                            </span>
                        </button>
                    </td>
                </tr>
            @endforeach
        @else
            <tr class="odd"><td valign="top" colspan="11" class="text-center">{!! __('JsPress::backend.no_result_found') !!}</td></tr>
        @endif
        </tbody>
    </table>
</div>
@push('script')
    <script>
        let toggleStatusElements = document.querySelectorAll('.toggleStatus');
        toggleStatusElements.forEach((el) => {
            el.addEventListener('click', (e) => {
                let id = e.currentTarget.getAttribute('data-id');
                let status = e.currentTarget.getAttribute('data-status');
                Swal.fire({
                    text: "{{__('JsPress::backend.post_type.confirm_status_update')}}",
                    icon: "warning",
                    showCancelButton: true,
                    buttonsStyling: true,
                    confirmButtonText: "{!! __('JsPress::backend.yes_i_confirm') !!}",
                    cancelButtonText: "{!! __('JsPress::backend.no_return') !!}",
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-active-light"
                    },
                }).then(
                    function (t) {
                        if( t.isConfirmed ){
                            let xhr = new XMLHttpRequest();
                            xhr.onreadystatechange = function() {
                                if (xhr.readyState !== 4) {
                                    return;
                                }
                                if (xhr.status === 200) {
                                    let res = JSON.parse(xhr.responseText);
                                    console.log(res);
                                    if(res.status === 1){
                                        JsPressFunc.__callSwalFireReLoad('{{ __('JsPress::backend.ok') }}', 'success', res.msg);
                                    }else{
                                        JsPressFunc.toastrError(res.msg);
                                    }
                                }else{
                                    JsPressFunc.toastrError('{{__('JsPress::backend.an_error_occurred')}}');
                                }
                            };
                            xhr.open("POST", '{{ route('admin.post_type.status') }}', true);
                            xhr.setRequestHeader('X-CSRF-TOKEN', document.head.querySelector('meta[name="csrf-token"]').content);
                            const data = new FormData();
                            data.append("id", id);
                            data.append("status", status);
                            xhr.send(data);
                        }
                    }
                );
            });
        });

        let deletePostTypeElements = document.querySelectorAll('.deletePostType');
        deletePostTypeElements.forEach((el) => {
            el.addEventListener('click', (e) => {
                let id = e.currentTarget.getAttribute('data-id');
                Swal.fire({
                    text: "{{__('JsPress::backend.post_type.confirm_delete_post_type')}}",
                    icon: "warning",
                    showCancelButton: true,
                    buttonsStyling: true,
                    confirmButtonText: "{!! __('JsPress::backend.yes_i_confirm') !!}",
                    cancelButtonText: "{!! __('JsPress::backend.no_return') !!}",
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-active-light"
                    },
                }).then(
                    function (t) {
                        if( t.isConfirmed ){
                            let xhr = new XMLHttpRequest();
                            xhr.onreadystatechange = function() {
                                if (xhr.readyState !== 4) {
                                    return;
                                }
                                if (xhr.status === 200) {
                                    let res = JSON.parse(xhr.responseText);
                                    console.log(res);
                                    if(res.status === 1){
                                        JsPressFunc.__callSwalFireReLoad('{{ __('JsPress::backend.ok') }}', 'success', res.msg);
                                    }else{
                                        JsPressFunc.toastrError(res.msg);
                                    }
                                }else{
                                    JsPressFunc.toastrError('{{__('JsPress::backend.an_error_occurred')}}');
                                }
                            };
                            xhr.open("POST", '{{ route('admin.post_type.destroy') }}', true);
                            xhr.setRequestHeader('X-CSRF-TOKEN', document.head.querySelector('meta[name="csrf-token"]').content);
                            const data = new FormData();
                            data.append("id", id);
                            xhr.send(data);
                        }
                    }
                );
            });
        })

    </script>
@endpush
