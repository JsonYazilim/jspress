@extends('JsPress::admin.layouts.app')
@push('script')
    <script>
        JsPressFunc.generateFormFields();
        JsPressFunc.autoCompleteSlugPostType();
    </script>
@endpush
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{!! __('JsPress::backend.post_type.edit') !!}</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                @if( \Session::has('success') )
                    @include('JsPress::admin.layouts.success-modal', ['message'=>session('success')])
                @endif
                <form id="post_type_create_form" class="form d-flex flex-column flex-lg-row" novalidate>
                    @csrf
                    <div class="d-flex flex-column flex-row-fluid gap-7 me-lg-10 gap-lg-5">
                        <div class="alert alert-dismissible bg-light-warning d-flex flex-column flex-sm-row p-5 mb-5">
                            <span class="svg-icon svg-icon-2hx svg-icon-warning me-4 mb-5 mb-sm-0">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <path opacity="0.3" d="M12 22C13.6569 22 15 20.6569 15 19C15 17.3431 13.6569 16 12 16C10.3431 16 9 17.3431 9 19C9 20.6569 10.3431 22 12 22Z" fill="black"></path>
                                    <path d="M19 15V18C19 18.6 18.6 19 18 19H6C5.4 19 5 18.6 5 18V15C6.1 15 7 14.1 7 13V10C7 7.6 8.7 5.6 11 5.1V3C11 2.4 11.4 2 12 2C12.6 2 13 2.4 13 3V5.1C15.3 5.6 17 7.6 17 10V13C17 14.1 17.9 15 19 15ZM11 10C11 9.4 11.4 9 12 9C12.6 9 13 8.6 13 8C13 7.4 12.6 7 12 7C10.3 7 9 8.3 9 10C9 10.6 9.4 11 10 11C10.6 11 11 10.6 11 10Z" fill="black"></path>
                                </svg>
                            </span>
                            <div class="d-flex flex-column pe-0 pe-sm-10">
                                <h4 class="fw-bold">{{ __('JsPress::backend.important_warning') }}</h4>
                                <span>{{__('JsPress::backend.post_type.alert_warning')}}</span>
                            </div>
                        </div>
                        <div class="card card-flush py-4">
                            <div class="card-header">
                                <div class="card-title">
                                    <h2>{{__('JsPress::backend.post_type.post_type_info')}}</h2>
                                </div>
                            </div>
                            <div class="card-body pt-0">
                                @foreach( $website_languages = website_languages() as $key => $language )
                                    @php
                                        $post_detail_locale = $postType->post_detail_locale($language->locale);
                                        $post_category_locale = $postType->post_category_locale($language->locale);
                                    @endphp
                                    <div class="post_type_container">
                                        <div class="row mb-5 postTypeContainer">
                                            <div class="col-lg-4">
                                                <div class="fv-row fv-plugins-icon-container">
                                                    <label class="required form-label">{{__('JsPress::backend.post_type.post_type_single_name')}} (<img src="{{ asset('jspress/assets/media/flags/svg/'.$language->locale.'.svg') }}" class="w-15px"/> {{ General::getLanguageNameByLocale($language->locale) }}) </label>
                                                    <input type="text" name="post_type[details][{{$language->locale}}][post][single_name]" class="form-control mb-2 single_name" placeholder="{{__('JsPress::backend.post_type.post_type_single_name_placeholder')}}" value="{{ @$post_detail_locale->singular_name }}" {{ @$post_detail_locale->status == 0 ? 'disabled':'required' }}>
                                                    <div class="text-muted fs-7">{{__('JsPress::backend.post_type.post_type_single_name_info')}}</div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="fv-row fv-plugins-icon-container">
                                                    <label class="required form-label">{{__('JsPress::backend.post_type.post_type_plural_name')}} (<img src="{{ asset('jspress/assets/media/flags/svg/'.$language->locale.'.svg') }}" class="w-15px"/> {{ General::getLanguageNameByLocale($language->locale) }}) </label>
                                                    <input type="text" name="post_type[details][{{$language->locale}}][post][plural_name]" class="form-control mb-2" placeholder="{{__('JsPress::backend.post_type.post_type_plural_name_placeholder')}}" value="{{ @$post_detail_locale->plural_name }}" {{ @$post_detail_locale->status == 0 ? 'disabled':'required' }}>
                                                    <div class="text-muted fs-7">{{__('JsPress::backend.post_type.post_type_plural_name_info')}}</div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="fv-row">
                                                    <label class="form-label">{{__('JsPress::backend.post_type.post_type_slug')}} (<img src="{{ asset('jspress/assets/media/flags/svg/'.$language->locale.'.svg') }}" class="w-15px"/> {{ General::getLanguageNameByLocale($language->locale) }}) </label>
                                                    <input type="text" name="post_type[details][{{$language->locale}}][post][slug]" class="form-control mb-2 slugify" placeholder="{{__('JsPress::backend.post_type.post_type_slug_placeholder')}}" data-type="slug" value="{{ @$post_detail_locale->slug }}" {{ @$post_detail_locale->status == 0 || $postType->is_segment_disable == 1 ? 'disabled':'' }}>
                                                    <div class="text-muted fs-7">{{__('JsPress::backend.post_type.post_type_slug_info')}}</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-5 dependOnCategory {{ $postType->is_category != 1 ? 'd-none':'' }}">
                                            <div class="col-lg-4">
                                                <div class="fv-row fv-plugins-icon-container">
                                                    <label class="required form-label">{{__('JsPress::backend.post_type.category_single_name')}} (<img src="{{ asset('jspress/assets/media/flags/svg/'.$language->locale.'.svg') }}" class="w-15px"/> {{ General::getLanguageNameByLocale($language->locale) }}) </label>
                                                    <input type="text" name="post_type[details][{{$language->locale}}][category][single_name]" class="form-control mb-2" placeholder="{{__('JsPress::backend.post_type.category_single_name_placeholder')}}" value="{{@$post_category_locale->singular_name}}" {{ @$post_category_locale->status == 0 || !$post_category_locale ? 'disabled':'' }}>
                                                    <div class="text-muted fs-7">{{__('JsPress::backend.post_type.category_single_name_info')}}</div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="fv-row fv-plugins-icon-container">
                                                    <label class="required form-label">{{__('JsPress::backend.post_type.category_plural_name')}} (<img src="{{ asset('jspress/assets/media/flags/svg/'.$language->locale.'.svg') }}" class="w-15px"/> {{ General::getLanguageNameByLocale($language->locale) }}) </label>
                                                    <input type="text" name="post_type[details][{{$language->locale}}][category][plural_name]" class="form-control mb-2" placeholder="{{__('JsPress::backend.post_type.category_plural_name_placeholder')}}" value="{{@$post_category_locale->plural_name}}" {{ @$post_category_locale->status == 0 || !$post_category_locale ? 'disabled':'' }}>
                                                    <div class="text-muted fs-7">{{__('JsPress::backend.post_type.category_plural_name_info')}}</div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="fv-row">
                                                    <label class="form-label">{{__('JsPress::backend.post_type.category_slug')}} (<img src="{{ asset('jspress/assets/media/flags/svg/'.$language->locale.'.svg') }}" class="w-15px"/> {{ General::getLanguageNameByLocale($language->locale) }}) </label>
                                                    <input type="text" name="post_type[details][{{$language->locale}}][category][slug]" class="form-control slugify mb-2" placeholder="{{__('JsPress::backend.post_type.category_slug_placeholder')}}" value="{{@$post_category_locale->slug}}" {{ @$post_category_locale->status == 0 || !$post_category_locale ? 'disabled':'' }}>
                                                    <div class="text-muted fs-7">{{__('JsPress::backend.post_type.category_slug_info')}}</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-5">
                                            <div class="col-lg-12">
                                                <label class="cursor-pointer form-check form-check-solid me-5 mb-2">
                                                    <input class="form-check-input disablePostType" data-language="{{$language->locale}}" name="post_type[details][{{$language->locale}}][disabled]" type="checkbox" value="1" {{ @$post_detail_locale->status == 0 ? 'checked':'' }}>
                                                    <span class="fw-bold ps-2 fs-6">Yazı tipini bu dil için devre dışı bırak</span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    @if($key != $website_languages->count() - 1)
                                        <div class="separator my-8"></div>
                                    @endif
                                @endforeach
                            </div>
                        </div>
                        <div class="card card-flush py-4">
                            <div class="card-header">
                                <div class="card-title">
                                    <h2>{{__('JsPress::backend.post_type.permissions')}}</h2>
                                </div>
                            </div>
                            <div class="card-body pb-2">
                                <x-jspress-repeater id="post_permission_repeater" name="permissions" :elements="$elements = $permission_data" :values="$permission_values"></x-jspress-repeater>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex flex-column gap-7 gap-lg-10 min-w-300px mw-300px w-300px mb-7">
                        <div class="card py-4">
                            <div class="card-header">
                                <div class="card-title">
                                    <h2>{{__('JsPress::backend.post_type.post_type_settings')}}</h2>
                                </div>
                                <div class="card-toolbar">
                                    <div class="rounded-circle bg-success w-15px h-15px"></div>
                                </div>
                            </div>
                            <div class="card-body pb-2">
                                <div class="fv-row fv-plugins-icon-container mb-5">
                                    <label class="required form-label">{{__('JsPress::backend.post_type.name')}}</label>
                                    <input type="text" name="post_type[name]" class="form-control mb-2" placeholder="{{__('JsPress::backend.post_type.name_placeholder')}}" required value="{{$postType->name}}">
                                </div>
                                <div class="fv-row fv-plugins-icon-container mb-5">
                                    <label class="required form-label">{{__('JsPress::backend.post_type.key_value')}}</label>
                                    <input type="text" name="post_type[key]" class="form-control mb-2" placeholder="{{__('JsPress::backend.post_type.key_value_placeholder')}}" required value="{{$postType->key}}">
                                    <div class="text-muted fs-7">{{__('JsPress::backend.post_type.key_value_info')}}</div>
                                </div>
                                <div class="fv-row fv-plugins-icon-container mb-5">
                                    <label class="form-label">{{__('JsPress::backend.post_type.parent_post_type')}}</label>
                                    <select class="form-select" name="parent_id">
                                        <option value="">{{__('JsPress::backend.post_type.no_parent')}}</option>
                                        @foreach( \JsPress\JsPress\Models\PostType::where('id', '!=', $postType->id)->orderBy('order')->get() as $post_type )
                                            <option value="{{$post_type->id}}" {{ $post_type->id == $postType->post_type_id ? 'selected':'' }}>{!! __js('post_type', JsPressPanel::postTypeLangKey($post_type, 'list'), $post_type->name) !!}</option>
                                        @endforeach
                                    </select>

                                </div>
                                <div class="fv-row fv-plugins-icon-container mb-5">
                                    <label class="required form-label">{{__('JsPress::backend.order')}}</label>
                                    <input type="text" name="post_type[order]" class="form-control mb-2" pattern="[0-9]+" placeholder="{{__('JsPress::backend.post_type.order_placeholder')}}" required value="{{$postType->order}}">
                                </div>
                                <div class="fv-row fv-plugins-icon-container mb-5">
                                    <label class="required form-label">{{__('JsPress::backend.icon')}}</label>
                                    <input type="text" name="post_type[icon]" class="form-control mb-2" placeholder="{{__('JsPress::backend.post_type.icon_info_placeholder')}}" value="{{$postType->icon}}">
                                    <div class="text-muted fs-7">{!! __('JsPress::backend.post_type.icon_info') !!}</div>
                                </div>
                                <div class="d-flex mb-2">
                                    <label class="col-form-label cursor-pointer fw-bold fs-6 w-75" for="is_category">{{__('JsPress::backend.post_type.activate_category')}}</label>
                                    <div class="d-flex align-items-center">
                                        <div class="form-check form-check-solid form-switch fv-row">
                                            <input type="checkbox" class="form-check-input w-45px h-30px" name="is_category" value="1" id="is_category" {{ $postType->is_category == 1 ? 'checked':'' }}/>
                                            <label class="form-check-label" for="is_category"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex mb-2">
                                    <label class="col-form-label cursor-pointer fw-bold fs-6 w-75" for="is_category_url">{{__('JsPress::backend.is_category_url')}}</label>
                                    <div class="d-flex align-items-center">
                                        <div class="form-check form-check-solid form-switch fv-row">
                                            <input type="checkbox" class="form-check-input w-45px h-30px" name="is_category_url" value="1" id="is_category_url"  {{ $postType->is_category_url == 1 ? 'checked':'' }}/>
                                            <label class="form-check-label" for="is_category_url"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex mb-2">
                                    <label class="col-form-label cursor-pointer fw-bold fs-6 w-75" for="is_editor">{{__('JsPress::backend.active_editor')}}</label>
                                    <div class="d-flex align-items-center">
                                        <div class="form-check form-check-solid form-switch fv-row">
                                            <input type="checkbox" class="form-check-input w-45px h-30px" name="is_editor" value="1" id="is_editor" {{ $postType->is_editor == 1 ? 'checked':'' }}/>
                                            <label class="form-check-label" for="is_editor"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex mb-2">
                                    <label class="col-form-label cursor-pointer fw-bold fs-6 w-75" for="is_terms">{{__('JsPress::backend.post_type.activate_terms')}}</label>
                                    <div class="d-flex align-items-center">
                                        <div class="form-check form-check-solid form-switch fv-row">
                                            <input type="checkbox" class="form-check-input w-45px h-30px" name="is_terms" value="1" id="is_terms" {{ $postType->is_terms == 1 ? 'checked':'' }}/>
                                            <label class="form-check-label" for="is_terms"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex mb-2">
                                    <label class="col-form-label cursor-pointer fw-bold fs-6 w-75" for="is_searchable">{{__('JsPress::backend.is_searchable')}}</label>
                                    <div class="d-flex align-items-center">
                                        <div class="form-check form-check-solid form-switch fv-row">
                                            <input type="checkbox" class="form-check-input w-45px h-30px" name="is_searchable" value="1" id="is_searchable" {{ $postType->is_searchable == 1 ? 'checked':'' }}/>
                                            <label class="form-check-label" for="is_editor"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex mb-2">
                                    <label class="col-form-label cursor-pointer fw-bold fs-6 w-75" for="is_image">{{__('JsPress::backend.post_type.activate_image')}}</label>
                                    <div class="d-flex align-items-center">
                                        <div class="form-check form-check-solid form-switch fv-row">
                                            <input type="checkbox" class="form-check-input w-45px h-30px" name="is_image" value="1" id="is_image" {{ $postType->is_image == 1 ? 'checked':'' }}/>
                                            <label class="form-check-label" for="is_image"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex mb-2">
                                    <label class="col-form-label cursor-pointer fw-bold fs-6 w-75" for="is_seo">{{__('JsPress::backend.post_type.activate_seo')}}</label>
                                    <div class="d-flex align-items-center">
                                        <div class="form-check form-check-solid form-switch fv-row">
                                            <input type="checkbox" class="form-check-input w-45px h-30px" name="is_seo" value="1" id="is_seo" {{ $postType->is_seo == 1 ? 'checked':'' }}/>
                                            <label class="form-check-label" for="is_seo"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex mb-2">
                                    <label class="col-form-label cursor-pointer fw-bold fs-6 w-75" for="is_menu">{{__('JsPress::backend.post_type.activate_menu')}}</label>
                                    <div class="d-flex align-items-center">
                                        <div class="form-check form-check-solid form-switch fv-row">
                                            <input type="checkbox" class="form-check-input w-45px h-30px" name="is_menu" value="1" id="is_menu" {{ $postType->is_menu == 1 ? 'checked':'' }}/>
                                            <label class="form-check-label" for="is_menu"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex mb-2">
                                    <label class="col-form-label cursor-pointer fw-bold fs-6 w-75" for="is_url">
                                        <span class="d-flex">
                                            <span class="me-2" data-bs-toggle="tooltip" data-bs-placement="top" title="{{__('JsPress::backend.post_type.activate_url_info')}}">
                                                <i class="bi bi-info-circle fs-2"></i>
                                            </span>
                                            <span>{{__('JsPress::backend.post_type.activate_url')}}</span>
                                        </span>
                                    </label>
                                    <div class="d-flex align-items-center">
                                        <div class="form-check form-check-solid form-switch fv-row">
                                            <input type="checkbox" class="form-check-input w-45px h-30px" name="is_url" value="1" id="is_url" {{ $postType->is_url == 1 ? 'checked':'' }}/>
                                            <label class="form-check-label" for="is_url"></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex mb-2 segmentDisable {{ $postType->is_url == 1 ?: 'd-none' }}">
                                    <label class="col-form-label cursor-pointer fw-bold fs-6 w-75" for="is_segment_disable">
                                        <span class="d-flex">
                                            <span class="me-2" data-bs-toggle="tooltip" data-bs-placement="top" title="{{__('JsPress::backend.post_type.segment_disable_info')}}">
                                                <i class="bi bi-info-circle fs-2"></i>
                                            </span>
                                            <span>{{__('JsPress::backend.post_type.disable_segment')}}</span>
                                        </span>
                                    </label>
                                    <div class="d-flex align-items-center">
                                        <div class="form-check form-check-solid form-switch fv-row">
                                            <input type="checkbox" class="form-check-input w-45px h-30px" name="is_segment_disable" value="1" id="is_segment_disable" {{ $postType->is_segment_disable == 1 ? 'checked':'' }}/>
                                            <label class="form-check-label" for="is_segment_disable"></label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer pb-2">
                                <input type="hidden" name="post_type_id" value="{{$postType->id}}"/>
                                <button id="submitPostTypeForm" type="submit" class="btn btn-success w-100">
                                    <span class="indicator-label">{{ __('JsPress::backend.post_type.update_post_type') }}</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
