@extends('JsPress::admin.layouts.app')
@push('style')
    <style>
        #uploadFileCaption{
            overflow: hidden;
            transition: height 300ms ease-in;
            height: 0;
        }
        #kt_content_container.dragContainer .dragOverBackdrop {
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            width: 100%;
            height: 100vh;
            overflow: hidden;
            background: #212e48;
            z-index: 999;
            opacity: 0.4;
            padding: 10px;
        }
        #kt_content_container.dragContainer .dragOverBackdrop .dragUploadWindow{
            width: 100%;
            height: 100%;
            border: 4px dashed #eee !important;
        }
        body.dark_theme .image_wrap{
            border-color: #eee;
        }
        .image_wrap{
            width: 9.09%;
            height: 150px;
            border-radius: 10px;
            background: rgb(255 255 255 / 2%);
            border: 1px dashed #878585;
            cursor: pointer;
            padding: 10px;
        }
        .loading-image {
            background-image: -webkit-linear-gradient(left, #2a2a41 0px, #2b2b40 40px, #2a2a41 80px);
            background-image: -o-linear-gradient(left, #2a2a41 0px, #2b2b40 40px, #2a2a41 80px);
            background-image: linear-gradient(90deg, #2a2a41 0px, #2b2b40 40px, #2a2a41 80px);
            background-size: 250px;
            -webkit-animation: shine-loading-image 2s infinite ease-out;
            animation: shine-loading-image 2s infinite ease-out;
        }
        .image_wrap img{
            width:100%;
            height: 100%;
            border-radius: 10px;
        }
        div#imageList.mediaTheme img {
            width: 100%;
        }
        @media(max-width: 767px){
            .image_wrap{
                width: 48%;
            }
        }
        @-webkit-keyframes shine-loading-image {
            0% {
                background-position: -32px;
            }
            40%, 100% {
                background-position: 208px;
            }
        }


        @keyframes shine-loading-image {
            0% {
                background-position: -32px;
            }
            40%, 100% {
                background-position: 208px;
            }
        }

    </style>
@endpush
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{__('JsPress::backend.media.list.title')}}</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
            </div>
        </div>
        <div class="d-flex flex-column-fluid" id="kt_media">
            <div id="kt_content_container" class="container-fluid">
                <div class="card card-flush">
                    <div class="card-header pt-5 pb-5">
                        <div class="card-title">
                            <div class="d-flex align-items-center position-relative my-1">
                                <span class="svg-icon svg-icon-1 position-absolute ms-6">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                                        <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                                    </svg>
                                </span>
                                <input id="mediaSearchInput" type="text" name="term" class="form-control form-control-solid w-250px ps-15" placeholder="{{ __('JsPress::backend.quick_search') }}" />
                                <button id="clearMediaFilter" class="btn btn-light-danger mx-2" onclick="JsPressFunc.clearMediaFilter()">{{__('JsPress::backend.clear')}}</button>
                            </div>
                        </div>
                        <div class="card-toolbar">
                            <div class="d-flex justify-content-end" data-kt-filemanager-table-toolbar="base">
                                <button id="toggleUpload" type="button" class="btn btn-primary">
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path opacity="0.3" d="M10 4H21C21.6 4 22 4.4 22 5V7H10V4Z" fill="black" />
                                            <path d="M10.4 3.60001L12 6H21C21.6 6 22 6.4 22 7V19C22 19.6 21.6 20 21 20H3C2.4 20 2 19.6 2 19V4C2 3.4 2.4 3 3 3H9.20001C9.70001 3 10.2 3.20001 10.4 3.60001ZM16 11.6L12.7 8.29999C12.3 7.89999 11.7 7.89999 11.3 8.29999L8 11.6H11V17C11 17.6 11.4 18 12 18C12.6 18 13 17.6 13 17V11.6H16Z" fill="black" />
                                            <path opacity="0.3" d="M11 11.6V17C11 17.6 11.4 18 12 18C12.6 18 13 17.6 13 17V11.6H11Z" fill="black" />
                                        </svg>
                                    </span>
                                    {{__('JsPress::backend.upload_file')}}
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="uploadFileCaption" class="row mt-10">
                    <div class="col-12 text-center">
                        <div class="upload-ui pt-10 pb-10 border border-4 border-dashed border-active-danger">
                            <h2 class="upload-instructions drop-instructions">{{__('JsPress::backend.drag_and_drop_files')}}</h2>
                            <p class="upload-instructions drop-instructions">{{ __('JsPress::backend.or') }}</p>
                            <div class="d-flex flex-center uploadFiles">
                                <a href="javascript:void(0);" class="btn btn-flex btn-primary px-6" onclick="document.getElementById('file').click();">
                                    <span class="svg-icon svg-icon-2x">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM16 13.5L12.5 13V10C12.5 9.4 12.6 9.5 12 9.5C11.4 9.5 11.5 9.4 11.5 10L11 13L8 13.5C7.4 13.5 7 13.4 7 14C7 14.6 7.4 14.5 8 14.5H11V18C11 18.6 11.4 19 12 19C12.6 19 12.5 18.6 12.5 18V14.5L16 14C16.6 14 17 14.6 17 14C17 13.4 16.6 13.5 16 13.5Z" fill="black"/>
                                            <rect x="11" y="19" width="10" height="2" rx="1" transform="rotate(-90 11 19)" fill="black"/>
                                            <rect x="7" y="13" width="10" height="2" rx="1" fill="black"/>
                                            <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black"/>
                                        </svg>
                                    </span>
                                    <span class="d-flex flex-column align-items-start ms-2">
                                        <span class="fs-3 fw-bolder">{{ __('JsPress::backend.choose_file') }}</span>
                                        <span class="fs-7">{{__('JsPress::backend.max_upload_file_size')}} {{ convert_max_file_size_to_string() }}</span>
                                    </span>
                                </a>
                                <input id="file" type="file" class="form-control mw-250px" style="display:none;" multiple/>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="imageList" class="d-flex align-items-center flex-wrap mt-10 mediaTheme"></div>
                <div class="row">
                    <div id="pagination" class="col-lg-12 mt-3">

                    </div>
                </div>
                <div class="dragOverBackdrop">
                    <div class="dragUploadWindow"></div>
                </div>
            </div>
        </div>
    </div>
    @include('JsPress::admin.media.modal')
@endsection
@push('script')
    <script>
        window._page = 1;
        const wrapper = document.getElementById('uploadFileCaption')
        const content = document.querySelector('.upload-ui')
        const button = document.getElementById('toggleUpload')
        const dropField = document.getElementById('kt_content_container')
        const dropzoneField = document.getElementById('uploadFileCaption')
        let open = false

        if (open) {
            wrapper.style.height = `${content.getBoundingClientRect().height}px`
        }

        function toggleOpen () {
            if (open) {
                wrapper.style.height = '0px'
                open = false
            } else {
                const height = content.getBoundingClientRect().height
                wrapper.style.height = `${height}px`;
                open = true
            }
        }
        button.addEventListener('click', toggleOpen);

        $('#file').on('change', function(e){
            e.preventDefault();
            let file = document.getElementById('file');
            let file_list= file.files;
            JsPressFunc.MediaFileUpload(file_list, 'media_library');
        });

        $(window).on('dragenter', function(e) {
            e.stopPropagation();
            if(open){
                dropField.classList.add('dragContainer');
            }
        }).on('dragover', function(e) {
            e.preventDefault();
            if(open){
                dropField.classList.add('dragContainer');
            }
        }).on('drop', function (e) {
            e.preventDefault();
            if(open){
                dropField.classList.remove('dragContainer');
                let file_list= e.originalEvent.dataTransfer.files;
                JsPressFunc.MediaFileUpload(file_list, 'media_library');
            }
        }).on('resize', function() {

        });

        $('#kt_content_container').on('dragleave', function(e) {
            e.preventDefault();
            if(open){
                dropField.classList.remove('dragContainer');
            }
        });

        $(document).ready(function(){
            JsPressFunc.getMediaFiles(window._page, 40);
            JsPressFunc.searchMediaFile();
        });
    </script>
@endpush
