@push('style')
    <style>
        #mediaDetailModal .modal-body{
            margin: 0 !important;
            padding: 0 !important;
        }
        #mediaDetailModal .modal-body .imageCropArea{
            padding: 10px !important;
        }
        .imageCropArea img{
            display: block;
            margin: 0 auto 16px;
            max-width: 100%;
            max-height: calc(100% - 100px);
        }
        @media(max-width: 767px){
            #mediaDetailModal .modal-dialog{
                padding: 0 !important;
            }
            #mediaDetailModal .modal-body div#imageDetailBox {
                flex-wrap: wrap;
                max-height: inherit !important;
            }
            #mediaDetailModal .modal-body .imageCropArea,#mediaDetailModal .modal-body .imageSidebarArea{
                width: 100% !important;
                min-height: inherit !important;
            }
            #mediaDetailModal .modal-footer .btn{
                font-size:12px !important;
            }
        }
    </style>
@endpush
<div class="modal fade" id="mediaDetailModal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-fullscreen p-9">
        <div class="modal-content">
            <div class="modal-header py-7 d-flex justify-content-between"></div>
            <div class="modal-body scroll-y m-5">
                <div id="imageDetailBox" class="d-flex mh-100 overflow-hidden">
                    <div class="w-50 imageCropArea text-center d-flex flex-column align-items-center justify-content-center"></div>
                    <div class="w-50 imageSidebarArea min-vh-100 bg-light">
                        <form id="mediaDetailForm" enctype="multipart/form-data">
                            <div id="MediaDetailList" class="pt-10 px-5"></div>
                            <div id="MediaDetailTab" class="pt-10 px-5 mt-10">
                                <ul class="nav nav-custom nav-tabs nav-line-tabs nav-line-tabs-2x border-0 fs-4 fw-bold mb-n2"></ul>
                                <div class="tab-content" id="MediaDetailTabContent"></div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer"></div>
        </div>
    </div>
</div>
