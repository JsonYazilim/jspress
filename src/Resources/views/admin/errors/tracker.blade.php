@extends('admin.layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Servis İzleyici</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-fluid">
                <div class="card mb-5 mb-xl-8">
                    <div class="card-header border-0 pt-5">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label fw-bolder fs-3 mb-1">Servis İzleyici</span>
                            <span class="text-muted mt-1 fw-bold fs-7">Websitesinde kullanıcıların bilet arama, rezervasyon gibi işlemleri anlık olarak takip edebilirsiniz.</span>
                        </h3>
                    </div>
                    <div class="card-body py-3">
                        <div class="table-responsive">
                            <table class="table align-middle gy-4">
                                <thead>
                                <tr class="fw-bolder text-muted bg-light">
                                    <th class="ps-4 rounded-start">#ID</th>
                                    <th class="mw-200px">Yolcu/Telefon</th>
                                    <th class="mw-200px">Kalkış/Varış</th>
                                    <th class="mw-150px">Uçuş Tipi</th>
                                    <th class="mw-150px">ShopFieldID</th>
                                    <th class="min-w-200px">Mevcut Servis</th>
                                    <th class="min-w-150px">Son İşlem Zamanı</th>
                                    <th class="text-end pr-2"></th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($trackers as $track)
                                    @php

                                        $origin = \App\Models\Airport::where('iata_code', $track->departure['segments'][0]['o_code'])->first();
                                        $destination = \App\Models\Airport::where('iata_code', $track->departure['segments'][0]['d_code'])->first();
                                        if($track->passenger_data){
                                             $passenger = collect($track->passenger_data)[0];
                                        }
                                    @endphp
                                    <tr>
                                        <td>
                                            <span class="text-dark fw-bolder text-hover-primary mb-1 fs-6">#{!! $track->id !!}</span>
                                        </td>
                                        <td class="d-flex align-items-center">
                                            <div class="d-flex flex-column">
                                                @if($track->passenger_data)
                                                <a href="javascript:void(0);" class="text-gray-800 text-hover-primary mb-1">{{ $passenger['first_name'].' '.$passenger['last_name'] }}</a>
                                                <span class="font-size-sm text-dark-50">{{ $track['contact_data']['phone_code'].' '.$track['contact_data']['phone'] }}</span>
                                                @endif
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <span class="fs-sm-7">{!! $origin->localized_city_name !!} - {!! $track->departure['segments'][0]['o_code'] !!}</span>
                                                <span class="fs-sm-7">{!! $destination->localized_city_name !!} - {!! $track->departure['segments'][0]['d_code'] !!}</span>
                                            </div>
                                        </td>
                                        <td>
                                            <span class="text-muted fw-bold text-muted d-block fs-7">
                                                @if( $track->type == 'OW' )
                                                    Tek Yön
                                                @endif
                                                @if( $track->type == 'RT' )
                                                    Gidiş - Dönüş
                                                @endif
                                                @if( $track->type == 'REC' )
                                                    Paket Uçuş
                                                @endif
                                            </span>
                                        </td>
                                        <td>
                                            <span class="text-muted fw-bold text-muted d-block fs-7">{!! $track->shop_id !!}</span>
                                        </td>
                                        <td>
                                            <span class="text-muted fw-bold text-muted d-block fs-7">
                                                @if( $track->method == 'update-passenger' )
                                                    <div class="badge badge-light-info">Yolcu Bilgileri</div>
                                                @endif
                                                @if( $track->method == 'makeprebooking' )
                                                    <div class="badge badge-light-success">Ön Rezervasyon</div>
                                                @endif
                                                @if( $track->method == 'payment' )
                                                    <div class="badge badge-light-warning">Ödeme</div>
                                                @endif
                                            </span>
                                        </td>
                                        <td>
                                            <span class="text-muted fw-bold text-muted d-block fs-7 mt-1">{!! \Carbon\Carbon::parse($track->updated_at)->isoFormat('DD MMMM YYYY H:mm') !!}</span>
                                        </td>
                                        @php
                                            $payments = \App\Models\Payments::where('transaction_id', $track->transaction_id)->orderBy('updated_at', 'DESC')->get();
                                        @endphp
                                        <td class="text-end">
                                            <div class="d-flex justify-content-end flex-shrink-0">
                                                @if($payments->isNotEmpty())
                                                    <button class="btn btn-icon btn-light btn-sm me-1 pulse pulse-danger paymentError" id="paymentData_{!! $track->id !!}_button" data-bs-toggle="tooltip" title="" data-bs-original-title="Ödeme hataları mevcut">
                                                        <span class="svg-icon svg-icon-3">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                <path d="M22 7H2V11H22V7Z" fill="black"/>
                                                                <path opacity="0.3" d="M21 19H3C2.4 19 2 18.6 2 18V6C2 5.4 2.4 5 3 5H21C21.6 5 22 5.4 22 6V18C22 18.6 21.6 19 21 19ZM14 14C14 13.4 13.6 13 13 13H5C4.4 13 4 13.4 4 14C4 14.6 4.4 15 5 15H13C13.6 15 14 14.6 14 14ZM16 15.5C16 16.3 16.7 17 17.5 17H18.5C19.3 17 20 16.3 20 15.5C20 14.7 19.3 14 18.5 14H17.5C16.7 14 16 14.7 16 15.5Z" fill="black"/>
                                                            </svg>
                                                        </span>
                                                        <span class="pulse-ring"></span>
                                                    </button>
                                                @endif
                                                <button class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1" id="viewData_{!! $track->id !!}_button" data-bs-toggle="tooltip" title="" data-bs-original-title="Veri Çıktısı">
                                                    <span class="svg-icon svg-icon-3">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                            <rect opacity="0.3" width="12" height="2" rx="1" transform="matrix(-1 0 0 1 15.5 11)" fill="black"/>
                                                            <path d="M13.6313 11.6927L11.8756 10.2297C11.4054 9.83785 11.3732 9.12683 11.806 8.69401C12.1957 8.3043 12.8216 8.28591 13.2336 8.65206L16.1592 11.2526C16.6067 11.6504 16.6067 12.3496 16.1592 12.7474L13.2336 15.3479C12.8216 15.7141 12.1957 15.6957 11.806 15.306C11.3732 14.8732 11.4054 14.1621 11.8756 13.7703L13.6313 12.3073C13.8232 12.1474 13.8232 11.8526 13.6313 11.6927Z" fill="black"/>
                                                            <path d="M8 5V6C8 6.55228 8.44772 7 9 7C9.55228 7 10 6.55228 10 6C10 5.44772 10.4477 5 11 5H18C18.5523 5 19 5.44772 19 6V18C19 18.5523 18.5523 19 18 19H11C10.4477 19 10 18.5523 10 18C10 17.4477 9.55228 17 9 17C8.44772 17 8 17.4477 8 18V19C8 20.1046 8.89543 21 10 21H19C20.1046 21 21 20.1046 21 19V5C21 3.89543 20.1046 3 19 3H10C8.89543 3 8 3.89543 8 5Z" fill="#C4C4C4"/>
                                                        </svg>
                                                    </span>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @if($payments->isNotEmpty())
                                        @include('admin.errors.payment-errors')
                                    @endif
                                    <div id="viewData_{!! $track->id !!}" data-kt-drawer="true" data-kt-drawer-activate="true" data-kt-drawer-toggle="#viewData_{!! $track->id !!}_button"  data-kt-drawer-close="#viewData_{!! $track->id !!}_close"  data-kt-drawer-width="500px">
                                        <div class="card w-100 rounded-0">
                                            <div class="card-header pe-5">
                                                <div class="card-title">
                                                    <div class="d-flex justify-content-center flex-column me-3">
                                                        <a href="#" class="fs-4 fw-bolder text-gray-900 text-hover-primary me-1 lh-1">Veri Çıktısı</a>
                                                    </div>
                                                </div>
                                                <div class="card-toolbar">
                                                    <div class="btn btn-sm btn-icon btn-active-light-primary" id="viewData_{!! $track->id !!}_close">
                                                        <span class="svg-icon svg-icon-2">
															<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"></rect>
																<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"></rect>
															</svg>
														</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body hover-scroll-overlay-y">
                                                @php
                                                    $trackerData = \App\DataSources\ShoppingDS::getShopping($track, false);
                                                    echo '<pre>'. json_encode(json_decode($trackerData), JSON_PRETTY_PRINT) . '</pre>';
                                                @endphp
                                            </div>

                                        </div>
                                    </div>
                                  @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        {!! $trackers->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script>


    </script>
@endpush
