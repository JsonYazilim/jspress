@extends('admin.layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">Servis Hata İzleme</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                <div class="card mb-5 mb-xl-8">
                    <div class="card-header border-0 pt-5">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label fw-bolder fs-3 mb-1">Servis Hata İzleme</span>
                            <span class="text-muted mt-1 fw-bold fs-7">Sağlayıcı methodlarından gelen hataları buradan takip edebilirsiniz.</span>
                        </h3>
                        <div class="card-toolbar">
                            <div class="d-flex justify-content-end">
                                <button class="btn btn-sm btn-danger deleteErrorAll">
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="black"></path>
                                            <path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="black"></path>
                                            <path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="black"></path>
                                        </svg>
                                    </span>
                                    Tüm Hataları Temizle
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="card-body py-3">
                        <div class="table-responsive">
                            <table class="table align-middle gs-0 gy-4">
                                <thead>
                                    <tr class="fw-bolder text-muted bg-light">
                                        <th class="ps-4 rounded-start">#ID</th>
                                        <th class="mw-150px">Method</th>
                                        <th class="mw-150px">Hata Adı</th>
                                        <th class="min-w-200px">Debug Mesajı</th>
                                        <th class="min-w-150px">Hata Zamanı</th>
                                        <th class="min-w-200px text-end rounded-end"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($errors as $error)
                                    <tr>
                                        <td>
                                            <span class="text-dark fw-bolder text-hover-primary mb-1 fs-6">#{!! $error->id !!}</span>
                                        </td>
                                        <td>
                                            <span class="text-muted fw-bold text-muted d-block fs-7">{!! $error->service !!}</span>
                                        </td>
                                        <td>
                                            <span class="text-muted fw-bold text-muted d-block fs-7">{!! $error->name !!}</span>
                                        </td>
                                        <td>
                                            <span class="text-muted fw-bold text-muted d-block fs-7">{!! $error->debug_message !!}</span>
                                        </td>
                                        <td>
                                            <span class="text-muted fw-bold text-muted d-block fs-7 mt-1">{!! \Carbon\Carbon::parse($error->created_at)->isoFormat('DD MMMM YYYY H:mm') !!}</span>
                                        </td>
                                        <td class="text-end">
                                            <div class="d-flex justify-content-end flex-shrink-0">
                                                <button class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1" id="viewRequest_{!! $error->id !!}_button" data-bs-toggle="tooltip" title="" data-bs-original-title="Request">
                                                    <span class="svg-icon svg-icon-3">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                            <rect opacity="0.3" width="12" height="2" rx="1" transform="matrix(-1 0 0 1 15.5 11)" fill="black"/>
                                                            <path d="M13.6313 11.6927L11.8756 10.2297C11.4054 9.83785 11.3732 9.12683 11.806 8.69401C12.1957 8.3043 12.8216 8.28591 13.2336 8.65206L16.1592 11.2526C16.6067 11.6504 16.6067 12.3496 16.1592 12.7474L13.2336 15.3479C12.8216 15.7141 12.1957 15.6957 11.806 15.306C11.3732 14.8732 11.4054 14.1621 11.8756 13.7703L13.6313 12.3073C13.8232 12.1474 13.8232 11.8526 13.6313 11.6927Z" fill="black"/>
                                                            <path d="M8 5V6C8 6.55228 8.44772 7 9 7C9.55228 7 10 6.55228 10 6C10 5.44772 10.4477 5 11 5H18C18.5523 5 19 5.44772 19 6V18C19 18.5523 18.5523 19 18 19H11C10.4477 19 10 18.5523 10 18C10 17.4477 9.55228 17 9 17C8.44772 17 8 17.4477 8 18V19C8 20.1046 8.89543 21 10 21H19C20.1046 21 21 20.1046 21 19V5C21 3.89543 20.1046 3 19 3H10C8.89543 3 8 3.89543 8 5Z" fill="#C4C4C4"/>
                                                        </svg>
                                                    </span>
                                                </button>
                                                <button class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm me-1" id="viewResponse_{!! $error->id !!}_button" data-bs-toggle="tooltip" title="" data-bs-original-title="Response">
                                                    <span class="svg-icon svg-icon-3">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                            <rect opacity="0.3" x="4" y="11" width="12" height="2" rx="1" fill="black"/>
                                                            <path d="M5.86875 11.6927L7.62435 10.2297C8.09457 9.83785 8.12683 9.12683 7.69401 8.69401C7.3043 8.3043 6.67836 8.28591 6.26643 8.65206L3.34084 11.2526C2.89332 11.6504 2.89332 12.3496 3.34084 12.7474L6.26643 15.3479C6.67836 15.7141 7.3043 15.6957 7.69401 15.306C8.12683 14.8732 8.09458 14.1621 7.62435 13.7703L5.86875 12.3073C5.67684 12.1474 5.67684 11.8526 5.86875 11.6927Z" fill="black"/>
                                                            <path d="M8 5V6C8 6.55228 8.44772 7 9 7C9.55228 7 10 6.55228 10 6C10 5.44772 10.4477 5 11 5H18C18.5523 5 19 5.44772 19 6V18C19 18.5523 18.5523 19 18 19H11C10.4477 19 10 18.5523 10 18C10 17.4477 9.55228 17 9 17C8.44772 17 8 17.4477 8 18V19C8 20.1046 8.89543 21 10 21H19C20.1046 21 21 20.1046 21 19V5C21 3.89543 20.1046 3 19 3H10C8.89543 3 8 3.89543 8 5Z" fill="#C4C4C4"/>
                                                        </svg>
                                                    </span>
                                                </button>
                                                <button class="btn btn-icon btn-bg-light btn-active-color-primary btn-sm deleteError" data-id="{!! $error->id !!}" data-bs-toggle="tooltip" title="" data-bs-original-title="Hatayı Sil">
                                                    <span class="svg-icon svg-icon-3">
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                            <path d="M5 9C5 8.44772 5.44772 8 6 8H18C18.5523 8 19 8.44772 19 9V18C19 19.6569 17.6569 21 16 21H8C6.34315 21 5 19.6569 5 18V9Z" fill="black"></path>
                                                            <path opacity="0.5" d="M5 5C5 4.44772 5.44772 4 6 4H18C18.5523 4 19 4.44772 19 5V5C19 5.55228 18.5523 6 18 6H6C5.44772 6 5 5.55228 5 5V5Z" fill="black"></path>
                                                            <path opacity="0.5" d="M9 4C9 3.44772 9.44772 3 10 3H14C14.5523 3 15 3.44772 15 4V4H9V4Z" fill="black"></path>
                                                        </svg>
                                                    </span>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <div id="viewRequest_{!! $error->id !!}" data-kt-drawer="true" data-kt-drawer-activate="true" data-kt-drawer-toggle="#viewRequest_{!! $error->id !!}_button"  data-kt-drawer-close="#viewRequest_{!! $error->id !!}_close"  data-kt-drawer-width="500px">
                                        <div class="card w-100 rounded-0">
                                            <div class="card-header pe-5">
                                                <div class="card-title">
                                                    <div class="d-flex justify-content-center flex-column me-3">
                                                        <a href="#" class="fs-4 fw-bolder text-gray-900 text-hover-primary me-1 lh-1">Request Çıktısı</a>
                                                    </div>
                                                </div>
                                                <div class="card-toolbar">
                                                    <div class="btn btn-sm btn-icon btn-active-light-primary" id="viewRequest_{!! $error->id !!}_close">
                                                        <span class="svg-icon svg-icon-2">
															<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"></rect>
																<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"></rect>
															</svg>
														</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body hover-scroll-overlay-y">
                                                @php echo '<pre>'. json_encode(json_decode($error->request), JSON_PRETTY_PRINT) . '</pre>'; @endphp
                                            </div>
                                        </div>
                                    </div>
                                    <div id="viewResponse_{!! $error->id !!}" data-kt-drawer="true" data-kt-drawer-activate="true" data-kt-drawer-toggle="#viewResponse_{!! $error->id !!}_button"  data-kt-drawer-close="#viewResponse_{!! $error->id !!}_close"  data-kt-drawer-width="500px">
                                        <div class="card w-100 rounded-0">
                                            <div class="card-header pe-5">
                                                <div class="card-title">
                                                    <div class="d-flex justify-content-center flex-column me-3">
                                                        <a href="#" class="fs-4 fw-bolder text-gray-900 text-hover-primary me-1 lh-1">Response Çıktısı</a>
                                                    </div>
                                                </div>
                                                <div class="card-toolbar">
                                                    <div class="btn btn-sm btn-icon btn-active-light-primary" id="viewResponse_{!! $error->id !!}_close">
                                                        <span class="svg-icon svg-icon-2">
															<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
																<rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"></rect>
																<rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"></rect>
															</svg>
														</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="card-body hover-scroll-overlay-y">
                                                @php echo '<pre>'. json_encode(json_decode($error->response), JSON_PRETTY_PRINT) . '</pre>'; @endphp
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        {!! $errors->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script>
        $('.deleteError').on('click', function(){
            let id = $(this).attr('data-id');
            Swal.fire({
                text: "Bu hata mesajı sistemden kalıcı olarak kaldırılacaktır. Onaylıyor musunuz?",
                icon: "warning",
                showCancelButton: true,
                buttonsStyling: true,
                confirmButtonText: "{!! __('backend.yes_i_confirm') !!}",
                cancelButtonText: "{!! __('backend.no_return') !!}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-active-light"
                },
            }).then(
                function (t) {
                    if( t.isConfirmed ){
                        $.ajax({
                            url:'{!! Route('admin.tracker.destroy') !!}',
                            type:'POST',
                            headers:{
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data:{error_id:id},
                            dataType:'json',
                            success:function(res){
                                if(res.status == 1){
                                    Swal.fire({ text: res.msg, icon: "success", buttonsStyling: !1, confirmButtonText: "Onayla", customClass: { confirmButton: "btn btn-primary" } }).then(
                                        function (t) {
                                            location.reload();
                                        }
                                    );
                                }else{
                                    Swal.fire({ text: res.msg, icon: "error", buttonsStyling: !1, confirmButtonText: "Onayla", customClass: { confirmButton: "btn btn-primary" } }).then(
                                        function (t) {
                                            location.reload();
                                        }
                                    );
                                }
                            }
                        });
                    }
                }
            );
        });
        $('.deleteErrorAll').on('click', function(){
            Swal.fire({
                text: "Tüm hata mesajları sistemden kalıcı olarak kaldırılacaktır. Onaylıyor musunuz?",
                icon: "warning",
                showCancelButton: true,
                buttonsStyling: true,
                confirmButtonText: "{!! __('backend.yes_i_confirm') !!}",
                cancelButtonText: "{!! __('backend.no_return') !!}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-active-light"
                },
            }).then(
                function (t) {
                    if( t.isConfirmed ){
                        $.ajax({
                            url:'{!! Route('admin.tracker.destroyAll') !!}',
                            type:'POST',
                            headers:{
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType:'json',
                            success:function(res){
                                if(res.status == 1){
                                    Swal.fire({ text: res.msg, icon: "success", buttonsStyling: !1, confirmButtonText: "Onayla", customClass: { confirmButton: "btn btn-primary" } }).then(
                                        function (t) {
                                            location.reload();
                                        }
                                    );
                                }else{
                                    Swal.fire({ text: res.msg, icon: "error", buttonsStyling: !1, confirmButtonText: "Onayla", customClass: { confirmButton: "btn btn-primary" } }).then(
                                        function (t) {
                                            location.reload();
                                        }
                                    );
                                }
                            }
                        });
                    }
                }
            );
        });
    </script>
@endpush
