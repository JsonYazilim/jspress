<div id="paymentData_{!! $track->id !!}" data-kt-drawer="true" data-kt-drawer-activate="true" data-kt-drawer-toggle="#paymentData_{!! $track->id !!}_button"  data-kt-drawer-close="#paymentData_{!! $track->id !!}_close"  data-kt-drawer-width="500px">
    <div class="card w-100 rounded-0">
        <div class="card-header pe-5">
            <div class="card-title">
                <div class="d-flex justify-content-center flex-column me-3">
                    <a href="#" class="fs-4 fw-bolder text-gray-900 text-hover-primary me-1 lh-1">Ödeme Deneme ve Hataları</a>
                </div>
            </div>
            <div class="card-toolbar">
                <div class="btn btn-sm btn-icon btn-active-light-primary" id="paymentData_{!! $track->id !!}_close">
                    <span class="svg-icon svg-icon-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black"></rect>
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black"></rect>
                        </svg>
                    </span>
                </div>
            </div>
        </div>
        <div class="card-body hover-scroll-overlay-y">
            @foreach($payments as $payment)
                <div class="rounded border-gray-300 border-1 border-gray-300 border-dashed px-7 py-3 mb-6">
                <div class="row mb-3">
                    <label class="col-lg-4 fw-bold text-muted">Kart İsim Soyisim</label>
                    <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-gray-800">{!! $payment->card_holder !!}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-lg-4 fw-bold text-muted">Son İşlem Zamanı</label>
                    <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-gray-800">{!! \Carbon\Carbon::parse($payment->updated_at)->isoFormat('DD MMM YYYY HH:mm') !!}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-lg-4 fw-bold text-muted">Kart No:</label>
                    <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-gray-800">{!! Auth()->user()->hasAnyRole(['Super-Admin','Admin'])  ? $payment->card_number:hideCardNumbers($payment->card_number) !!}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-lg-4 fw-bold text-muted">Son Kul. Tarihi:</label>
                    <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-gray-800">{!! \Carbon\Carbon::create()->month($payment->expire_month)->format('m') !!} / {!! $payment->expire_year !!}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-lg-4 fw-bold text-muted">Cvv:</label>
                    <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-gray-800">{!! $payment->cvv !!}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-lg-4 fw-bold text-muted">Kart Tipi:</label>
                    <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-gray-800">{!! $payment->card_type !!}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-lg-4 fw-bold text-muted">Payment ID:</label>
                    <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-gray-800">{!! $payment->payment_id !!}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-lg-4 fw-bold text-muted">Ödeme Methodu:</label>
                    <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-gray-800">{!! $payment->payment_type->type !!}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-lg-4 fw-bold text-muted">Taksit:</label>
                    <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-gray-800">{!! $payment->installment == 1 ? '<span class="badge badge-success">Evet</span>':'<span class="badge badge-danger">Hayır</span>' !!}</span>
                    </div>
                </div>
                @if($payment->installment == 1)
                <div class="row mb-3">
                    <label class="col-lg-4 fw-bold text-muted">Taksit Bilgileri:</label>
                    <div class="col-lg-8">
                        <span class="fw-bolder fs-6 text-gray-800">
                            <ul class="list-unstyled">
                                <li><span class="text-info">Taksit ID:</span> {!! $payment->installment_data['installmentId'] !!}</li>
                                <li><span class="text-info">Taksit Oran:</span> {!! $payment->installment_data['installmentRate'] !!}</li>
                                <li><span class="text-info">Taksit Sayısı:</span> {!! $payment->installment_data['installmentCount'] !!}</li>
                                <li><span class="text-info">Aylık Taksit:</span> {!! $payment->installment_data['installmentAmount'] !!}</li>
                                <li><span class="text-info">Toplam Tutarı:</span> {!! $payment->installment_data['installmentTotalPrice'] !!}</li>
                            </ul>
                        </span>
                    </div>
                </div>
                @endif
                @if($payment->statıs != 1)
                <div class="row mb-3">
                    <label class="col-lg-4 fw-bold text-muted">Hata:</label>
                    <div class="col-lg-8">
                        @if( !is_null($payment->payment_data) && $payment->status == 0 )
                            <span class="fw-bolder fs-6 text-gray-800">
                                <ul class="list-unstyled">
                                    <li><span class="text-info">Hata Nedeni:</span> Biletbank Servis</li>
                                    <li><span class="text-info">Hata Adı:</span> {!! $payment->payment_data['Name'] !!}</li>
                                    <li><span class="text-info">Hata Debug Mesajı:</span> {!! $payment->payment_data['DebugMessage'] !!}</li>
                                    <li><span class="text-info">Hata Mesajı:</span> {!! $payment->payment_data['ErrorMessage'] !!}</li>
                                </ul>
                            </span>
                        @elseif(!is_null($payment->payment_data) && ($payment->status == 2 || $payment->status == 3))
                            <span class="fw-bolder fs-6 text-gray-800">{{$payment->payment_data['ErrorMessage']}}</span>
                        @else
                            <span class="fw-bolder fs-6 text-gray-800">Kullanıcı 3D ekranında beklemede ya da çıkış yapmıştır.</span>
                        @endif

                    </div>
                </div>
                @endif
            </div>
            @endforeach
        </div>
    </div>
</div>
