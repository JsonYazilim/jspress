<div class="modal fade" id="kt_modal_add_role" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-1000px">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="fw-bolder">{{ __('JsPress::backend.add_new_role') }}</h2>
                <div class="btn btn-icon btn-sm btn-active-icon-primary" data-kt-roles-modal-action="close">
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                        </svg>
                    </span>
                </div>
            </div>
            <div class="modal-body scroll-y mx-lg-5 my-7">
                <form id="kt_modal_add_role_form" class="form">
                    <div class="d-flex flex-column scroll-y me-n7 pe-7" id="kt_modal_add_role_scroll" data-kt-scroll="true" data-kt-scroll-activate="{default: false, lg: true}" data-kt-scroll-max-height="auto" data-kt-scroll-dependencies="#kt_modal_add_role_header" data-kt-scroll-wrappers="#kt_modal_add_role_scroll" data-kt-scroll-offset="300px">
                        <div class="fv-row mb-10">
                            <label class="fs-5 fw-bolder form-label mb-2">
                                <span class="required">{{ __('JsPress::backend.role_name') }}</span>
                            </label>
                            <input class="form-control form-control-solid" placeholder="{{ __('JsPress::backend.type_role_name') }}" name="role_name" />
                        </div>
                        <div class="fv-row">
                            <label class="fs-5 fw-bolder form-label mb-2">{{__('JsPress::backend.role_permissions')}}</label>
                            <div class="table-responsive">
                                <table class="table align-middle table-row-dashed fs-6 gy-5">
                                    <tbody class="text-gray-600 fw-bold">
                                    <tr>
                                        <td class="text-gray-800 min-w-250px">{{__('JsPress::backend.super_admin_permission_set')}}
                                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="" data-bs-original-title="{{__('JsPress::backend.super_admin_permission_description')}}" aria-label="{{__('JsPress::backend.super_admin_permission_description')}}"></i></td>
                                        <td>
                                            <label class="form-check form-check-sm form-check-custom form-check-solid me-9">
                                                <input class="form-check-input" type="checkbox" value="" id="kt_roles_select_all">
                                                <span class="form-check-label" for="kt_roles_select_all">{{ __('JsPress::backend.choose_all') }}</span>
                                            </label>
                                        </td>
                                    </tr>
                                    @foreach( $permissions as $permission )
                                        <tr>
                                            <td class="text-gray-800">{!! __js('backend', \Str::slug($permission[0]->group_name , '-'), $permission[0]->group_name ) !!}</td>
                                            <td>
                                                @if(collect($permission)->count() > 5)
                                                    <div class="d-flex mb-4">
                                                        @foreach( $permission->take(4) as $permissionItem )
                                                            @include('JsPress::admin.roles.permission_row')
                                                        @endforeach
                                                    </div>
                                                    <div class="d-flex">
                                                        @foreach( $permission->skip(4) as $permissionItem )
                                                            @include('JsPress::admin.roles.permission_row')
                                                        @endforeach
                                                    </div>
                                                @else
                                                    <div class="d-flex">
                                                        @foreach( $permission as $permissionItem )
                                                            @include('JsPress::admin.roles.permission_row')
                                                        @endforeach
                                                    </div>
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!--end::Scroll-->
                    <!--begin::Actions-->
                    <div class="text-center pt-15">
                        <button type="reset" class="btn btn-light me-3" data-kt-roles-modal-action="cancel">{{__('JsPress::backend.close')}}</button>
                        <button type="submit" class="btn btn-primary" data-kt-roles-modal-action="submit">
                            <span class="indicator-label">{{__('JsPress::backend.add_new_role')}}</span>
                            <span class="indicator-progress">{{ __('JsPress::backend.form.please_wait') }}
                            <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@push('script')
    <script>
        $(document).on('click', '[data-kt-roles-modal-action="cancel"], [data-kt-roles-modal-action="close"]', function(){
            $('#kt_modal_add_role').modal('hide');
        });
        $('#kt_modal_add_role_form').on('submit', function(e){
            e.preventDefault();
            $('[data-kt-roles-modal-action="submit"]').attr('data-kt-indicator', 'on');
            let formData = new FormData($('#kt_modal_add_role_form')[0]);
            $.ajax({
                url: '{!! Route('admin.role.store') !!}',
                type:'POST',
                data:formData,
                processData: false,
                contentType: false,
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                complete:function(){
                    $('[data-kt-roles-modal-action="submit"]').attr('data-kt-indicator', 'off');
                },
                success:function(res){
                    if(res.status == 1){
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toastr-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        toastr.success(res.msg);
                        setTimeout(function(){
                            location.reload();
                        },4000);
                    }else{
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toastr-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        toastr.error(res.msg);
                    }
                },
                error:function(xhr, status, errorThrown) {
                    let response = JSON.parse(xhr.responseText);
                    for(var i in response.errors){
                        for( var e in response.errors[i]){
                            toastr.options = {
                                "closeButton": false,
                                "debug": false,
                                "newestOnTop": false,
                                "progressBar": false,
                                "positionClass": "toastr-top-right",
                                "preventDuplicates": false,
                                "onclick": null,
                                "showDuration": "300",
                                "hideDuration": "1000",
                                "timeOut": "5000",
                                "extendedTimeOut": "1000",
                                "showEasing": "swing",
                                "hideEasing": "linear",
                                "showMethod": "fadeIn",
                                "hideMethod": "fadeOut"
                            };
                            toastr.error(response.errors[i]);
                        }
                    }
                }
            })
            return false;

        });
    </script>
@endpush
