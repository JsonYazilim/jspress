@extends('JsPress::admin.layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{!! __('JsPress::backend.pages.roles.breadcrumb.list.title') !!}</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                <div class="row row-cols-1 row-cols-md-2 row-cols-xl-3 g-5 g-xl-9">
                    @foreach( $roles as $role )
                        @if($role->id != 1)
                            <div class="col-md-4">
                                <div class="card card-flush h-md-100">
                                    <div class="card-header">
                                        <div class="card-title">
                                            <h2>{!! $role->name !!}</h2>
                                        </div>
                                    </div>
                                    <div class="card-body pt-1">
                                        <div class="fw-bolder text-gray-600 mb-5">{{__('JsPress::backend.admin_has_role_count_text', ['count'=>count($role->users)])}}</div>
                                        <div class="d-flex flex-column text-gray-600">
                                            @if($role->permissions->isNotEmpty())
                                                @foreach($role->permissions->take(5) as $permission)
                                                    @php
                                                        $permission_detail = \JsPress\JsPress\Models\PermissionGroup::where('permission_id', $permission->id)->first();
                                                        $translation_key = 'permission_description_'.\Str::replace(' ', '', $permission->name);
                                                    @endphp
                                                    <div class="d-flex align-items-center py-2">
                                                        <span class="bullet bg-primary me-3"></span>{!! __js('backend', $translation_key, $permission_detail->description ) !!}
                                                    </div>
                                                @endforeach
                                                @if( $role->permissions->count() > 5 )
                                                    <div class="d-flex align-items-center py-2">
                                                        <span class="bullet bg-primary me-3"></span>{!! $role->permissions->count() - 5 !!} yetki daha...
                                                    </div>
                                                @endif
                                            @else
                                                <div class="d-flex align-items-center py-2">
                                                    <span class="bullet bg-primary me-3"></span>{{ __('JsPress::backend.this_role_has_not_permission') }}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="card-footer flex-wrap pt-0">
                                        <button type="button" class="btn btn-light btn-active-light-primary my-1 editRole" data-role="{!! $role->id !!}">{{__('JsPress::backend.edit_role')}}</button>
                                        <button class="btn btn-light btn-active-primary my-1 me-2 deleteRole" data-id="{!! $role->id !!}">{{ __('JsPress::backend.delete_role') }}</button>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                    <div class="ol-md-4">
                        <div class="card h-md-100">
                            <div class="card-body d-flex flex-center">
                                <button type="button" class="btn btn-clear d-flex flex-column flex-center" data-bs-toggle="modal" data-bs-target="#kt_modal_add_role">
                                    <img src="{!! asset('jspress/assets/media/illustrations/sketchy-1/4.png') !!}" alt="" class="mw-100 mh-150px mb-7">
                                    <div class="fw-bolder fs-3 text-gray-600 text-hover-primary">{{ __('JsPress::backend.add_new_role') }}</div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="kt_modal_update_role" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered mw-1000px">
            <div class="modal-content">

            </div>
        </div>
    </div>
    @include('JsPress::admin.roles.add-modal')
@endsection
@push('script')
    <script>
        $('.editRole').on('click', function(){
            $('#kt_modal_update_role .modal-content').html("");
            let role_id = $(this).attr('data-role');
            $.ajax({
                url: '{!! Route('admin.role.getRolePermissions') !!}',
                type:'POST',
                data:{
                    role_id:role_id
                },
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(res){
                    if(res.status == 1){
                        $('#kt_modal_update_role .modal-content').html(res.render);
                        $('#kt_modal_update_role').modal('show');
                        $('[data-bs-toggle="tooltip"]').tooltip();
                    }
                }
            })
        });
        $(document).on('click', '[data-kt-roles-modal-action="cancel"], [data-kt-roles-modal-action="close"]', function(){
            $('#kt_modal_update_role').modal('hide');
        });
        $(document).on('change', '#kt_roles_select_all', function(){
            if($(this).is(':checked')){
                $('.form-check-input').prop('checked', true);
            }else{
                $('.form-check-input').prop('checked', false);
            }
        });
        $(document).on('submit', '#kt_modal_update_role_form', function(e){
            e.preventDefault();
            $('button.updatePermissions').attr('data-kt-indicator', 'on');
            let formData = new FormData($('#kt_modal_update_role_form')[0]);
            $.ajax({
                url: '{!! Route('admin.role.update') !!}',
                type:'POST',
                data:formData,
                processData: false,
                contentType: false,
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success:function(res){
                    $('button.updatePermissions').attr('data-kt-indicator', 'off');
                    if(res.status == 1){
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toastr-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        toastr.success(res.msg);
                    }else{
                        toastr.options = {
                            "closeButton": false,
                            "debug": false,
                            "newestOnTop": false,
                            "progressBar": false,
                            "positionClass": "toastr-top-right",
                            "preventDuplicates": false,
                            "onclick": null,
                            "showDuration": "300",
                            "hideDuration": "1000",
                            "timeOut": "5000",
                            "extendedTimeOut": "1000",
                            "showEasing": "swing",
                            "hideEasing": "linear",
                            "showMethod": "fadeIn",
                            "hideMethod": "fadeOut"
                        };
                        toastr.error(res.msg);
                    }
                }
            })
            return false;
        });

        $('.deleteRole').on('click', function(){
            let id = $(this).attr('data-id');
            Swal.fire({
                text: "Bu rol sistemden kalıcı olarak kaldırılacaktır. Onaylıyor musunuz?",
                icon: "warning",
                showCancelButton: true,
                buttonsStyling: true,
                confirmButtonText: "{!! __('backend.yes_i_confirm') !!}",
                cancelButtonText: "{!! __('backend.no_return') !!}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-active-light"
                },
            }).then(
                function (t) {
                    if( t.isConfirmed ){
                        $.ajax({
                            url:'{!! Route('admin.role.destroy') !!}',
                            type:'POST',
                            headers:{
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data:{role_id:id},
                            dataType:'json',
                            success:function(res){
                                if(res.status == 1){
                                    Swal.fire({ text: res.msg, icon: "success", buttonsStyling: !1, confirmButtonText: "{!! __('backend.confirmButton') !!}", customClass: { confirmButton: "btn btn-primary" } }).then(
                                        function () {
                                            location.reload();
                                        }
                                    );
                                }
                            }
                        });
                    }
                }
            );
        });
    </script>
@endpush
