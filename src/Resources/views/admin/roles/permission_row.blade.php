@php
    $translation_key = 'permission_description_'.\Str::replace(' ', '', $permissionItem->permission);
@endphp
<label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20" title="{!! __js('backend', $translation_key, $permissionItem->description ) !!}" data-bs-toggle="tooltip" data-bs-placement="top">
    <input class="form-check-input" type="checkbox" value="1" name="permissions[{!! $permissionItem->permission !!}]" {!! $role->hasPermissionTo($permissionItem->permission) ? 'checked':'' !!}>
    <span class="form-check-label fs-7">{!! __js('backend', \Str::slug($permissionItem->title, '-'), $permissionItem->title) !!}</span>
</label>
