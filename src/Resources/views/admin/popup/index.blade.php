@extends('JsPress::admin.layouts.app')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{__('JsPress::backend.popup_title')}}</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
                <a href="{{ Route('admin.popup.create') }}" class="btn btn-sm btn-primary">{{__('JsPress::backend.add_new_popup')}}</a>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container">
                <div class="card">
                    <div class="card-header border-0 pt-6">
                        <div class="card-title">
                            <h3 class="card-title align-items-start flex-column">
                                <span class="card-label fw-bolder fs-3 mb-1">{{ __('JsPress::backend.popup.popup_list') }}</span>
                                <span class="text-muted mt-1 fw-bold fs-7">{{ __('JsPress::backend.popup.popup_list_description') }}</span>
                            </h3>
                        </div>
                        <div class="card-toolbar">
                            <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                <a href="{!! route('admin.popup.create') !!}" class="btn btn-primary">
                                    <span class="svg-icon svg-icon-2">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="black" />
                                            <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="black" />
                                        </svg>
                                    </span>
                                    {{ __('JsPress::backend.add_new_popup') }}
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card-body py-4">
                        <div class="table-responsive">
                            <table class="table align-middle table-row-dashed fs-6 gy-5" id="coupon_table">
                                <thead>
                                <tr class="text-start text-muted fw-bolder fs-7 text-uppercase gs-0">
                                    <th class="min-w-10px">ID</th>
                                    <th class="min-w-150px">{{ __('JsPress::backend.title') }}</th>
                                    <th class="min-w-100px">{{ __('JsPress::backend.status') }}</th>
                                    <th class="min-w-125px">{{ __('JsPress::backend.post_field.order') }}</th>
                                    <th class="min-w-125px">{{ __('JsPress::backend.created_at') }}</th>
                                    <th class="text-end min-w-100px">{{ __('JsPress::backend.actions') }}</th>
                                </tr>
                                </thead>
                                <tbody class="text-gray-600 fw-bold">
                                @if( $popups->isNotEmpty() )
                                    @foreach( $popups as $popup )
                                        <tr class="user-row" data-id="{!! $popup->id !!}">
                                            <td>
                                                <span class="m--font-bold m--font-danger">#{!! $popup->id !!}</span>
                                            </td>
                                            <td>
                                                <span class="m--font-bold m--font-danger">{!! $popup->title !!}</span>
                                            </td>
                                            <td>
                                                @if($popup->status == 1)
                                                    <span class="text-success">{{ __('JsPress::backend.active') }}</span>
                                                @else
                                                    <span class="text-danger">{{ __('JsPress::backend.deactive') }}</span>
                                                @endif
                                            </td>
                                            <td>
                                                {{$popup->order}}
                                            </td>
                                            <td>
                                                <span class="m--font-bold m--font-danger">{!! $popup->created_at !!}</span>
                                            </td>
                                            <td class="text-end">
                                                <a href="javascript:void(0);" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{!! __('JsPress::backend.actions') !!}
                                                    <span class="svg-icon svg-icon-5 m-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
                                                    </svg>
                                                </span>
                                                </a>
                                                <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
                                                    <div class="menu-item px-3 text-start">
                                                        <a href="{!! route('admin.popup.edit', ['popup' => $popup->id]) !!}" class="menu-link px-3">{!! __('JsPress::backend.edit') !!}</a>
                                                    </div>
                                                    <div class="menu-item px-3 text-start">
                                                        <a href="javascript:void(0);" class="menu-link px-3 deletePopup" data-href="{!! Route('admin.popup.delete', ['popup'=>$popup->id]) !!}">{!! __('JsPress::backend.delete') !!}</a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr class="odd"><td valign="top" colspan="10" class="text-center">{!! __('JsPress::backend.no_result_found') !!}</td></tr>
                                @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        {!! $popups->withQueryString()->links() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $('.deletePopup').on('click', function(){
            let url = $(this).attr('data-href');
            Swal.fire({
                text: '{{ __('JsPress::backend.popup.delete_warning') }}',
                icon: "warning",
                showCancelButton: true,
                buttonsStyling: true,
                confirmButtonText: "{!! __('JsPress::backend.yes_i_confirm') !!}",
                cancelButtonText: "{!! __('JsPress::backend.no_return') !!}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-active-light"
                },
            }).then(
                function (t) {
                    if( t.isConfirmed ){
                        $.ajax({
                            url:url,
                            type:'POST',
                            headers:{
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            dataType:'json',
                            success:function(res){
                                if(res.status === 1){
                                    Swal.fire({ text: res.msg, icon: "success", buttonsStyling: !1, confirmButtonText: "{!! __('JsPress::backend.confirmButton') !!}", customClass: { confirmButton: "btn btn-primary" } }).then(
                                        function (t) {
                                            location.reload();
                                        }
                                    );
                                }else{
                                    Swal.fire({ text: res.msg, icon: "error", buttonsStyling: !1, confirmButtonText: "{!! __('JsPress::backend.confirmButton') !!}", customClass: { confirmButton: "btn btn-primary" } }).then(
                                        function (t) {
                                        }
                                    );
                                }
                            }
                        });
                    }
                }
            );
        });
    </script>
@endpush
