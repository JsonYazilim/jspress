@extends('JsPress::admin.layouts.app')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{__('JsPress::backend.add_new_popup')}}</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="js_popup">
            <div id="kt_content_container" class="container-fluid">
                <div class="container-fluid p-0">
                    @include('JsPress::admin.popup.form')
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="{!! asset('jspress/libs/ckeditor/ckeditor.js') !!}"></script>
    <script src="{!! asset('jspress/assets/js/custom/validation/jquery.validate.min.js') !!}"></script>
    @if( \File::exists(public_path('jspress/assets/js/custom/validation/localization/messages_'.JsPressPanel::panelLocale().'.js')) )
        <script src="{!! asset('jspress/assets/js/custom/validation/localization/messages_'.JsPressPanel::panelLocale().'.js') !!}"></script>
    @endif
    <script src="{{asset('jspress/assets/js/custom/select2/languages/'.JsPressPanel::panelLocale().'.js')}}"></script>
    <script>
        var element_array = [];
        $('.pageSelect').select2({
            multiple: true,
            minimumInputLength: 3,
            minimumResultsForSearch: 15,
            allowClear:true,
            placeholder: '{{__('JsPress::backend.choose')}}',
            language: '{{JsPressPanel::panelLocale()}}',
            ajax: {
                url: '{{route('admin.popup.search_posts')}}',
                dataType: "json",
                type: "GET",
                data: function (params) {
                    return {
                        term: params.term,
                        languages:$('select[name="languages[]"]').val()
                    };
                },
                processResults: function (data) {
                    return {
                        results: $.map(data, function (item) {
                            return {
                                text: item.title,
                                id: parseInt(item.id),
                                type:item.type
                            }
                        })
                    };
                }
            }
        }).on('select2:selecting', function(e) {
            let data = e.params.args.data;
            element_array.push(data);
            $('input[name="page_urls"]').val(JSON.stringify(element_array));
        }).on('select2:unselecting', function(e) {
            let data = e.params.args.data;
            element_array = element_array.filter((item) => parseInt(item.id) !== parseInt(data.id));
            $('input[name="page_urls"]').val(JSON.stringify(element_array));
        });

        $('.googleFontSelect').select2({
            language: '{{JsPressPanel::panelLocale()}}',
            ajax: {
                url: 'https://www.googleapis.com/webfonts/v1/webfonts',
                type:'GET',
                data: function (params) {
                    return {
                        family: params.term,
                        key: 'AIzaSyABz2tRgVVtSBwADS_7RDvWfFeiK-gF9fA'
                    };
                },
                dataType: "json",
                processResults: function (data) {
                    data.items.unshift({
                        family:trans.default,
                        category:'local'
                    });
                    data = data.items.map(function (item) {
                        return {
                            text: item.family,
                            id: item.family,
                            font_data:JSON.stringify(item),
                            category:item.category
                        };
                    });
                    return {
                        results:data
                    };
                }
            }
        }).on('select2:selecting', function(e) {
            let data = e.params.args.data;
            if(data.category === 'local'){
                $('input[name="design[font_data]"]').val(null);
            }else{
                $('input[name="design[font_data]"]').val(data.font_data);
            }
        });

        $(document).ready(function(){
           JsPressFunc.__callJsEditor();
           JsPressFunc.storePopupForm();
        });
    </script>
@endpush
