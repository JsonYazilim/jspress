<form id="create_popup_form" action="" method="POST" class="form" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col-lg-7">
            <div class="card w-100">
                <div class="card-header">
                    <div class="card-title">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label fw-bolder fs-3 mb-1">{{ __('JsPress::backend.popup.popup_basic_settings') }}</span>
                            <span class="text-muted mt-1 fw-bold fs-7">{{ __('JsPress::backend.popup.popup_basic_settings_description') }}</span>
                        </h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="mb-10">
                        <label class="required form-label">{{__('JsPress::backend.title')}}</label>
                        <input type="text" name="title" class="form-control mb-2" placeholder="{{ __('JsPress::backend.type_title') }}" value="{{$popup->title ?? ''}}" autocomplete="off"/>
                    </div>
                    <div class="mb-5">
                        <label class="required form-label">{{__('JsPress::backend.popup.type')}}</label>
                        <div class="d-flex flex-wrap">
                            <div class="d-block mb-5">
                                <div class="form-check form-check-custom form-check-solid form-check-sm">
                                    <label class="form-check-label" for="popup_type_lightbox">
                                        <input class="form-check-input typePopup" type="radio" name="type" value="lightbox" id="popup_type_lightbox" {{ isset($popup->type) && $popup->type === 'lightbox' ? 'checked' : ($popup->type ?? 'checked') }}/>
                                        Lightbox ({{ __('JsPress::backend.popup.center') }})
                                        <img class="mt-5 d-block" src="{{asset('jspress/assets/media/popup/lightbox.png')}}" alt="">
                                    </label>
                                </div>
                            </div>
                            <div class="d-block mb-5">
                                <div class="form-check form-check-custom form-check-solid form-check-sm">
                                    <label class="form-check-label" for="popup_type_tooltip">
                                        <input class="form-check-input typePopup" type="radio" name="type" value="tooltip" id="popup_type_tooltip" {{ isset($popup->type) && $popup->type === 'tooltip' ? 'checked' : '' }}/>
                                        Tooltip ({{ __('JsPress::backend.popup.left_right_bottom') }})
                                        <img class="mt-5 d-block" src="{{asset('jspress/assets/media/popup/tooltip.png')}}" alt="">
                                    </label>
                                </div>
                            </div>
                            <div class="d-block mb-5">
                                <div class="form-check form-check-custom form-check-solid form-check-sm">
                                    <label class="form-check-label" for="popup_type_bar">
                                        <input class="form-check-input typePopup" type="radio" name="type" value="bar" id="popup_type_bar"  {{ isset($popup->type) && $popup->type === 'bar' ? 'checked' : '' }}>
                                        Bar ({{ __('JsPress::backend.popup.bottom_top_long') }})
                                        <img class="mt-5 d-block" src="{{asset('jspress/assets/media/popup/bar.png')}}" alt="">
                                    </label>
                                </div>
                            </div>
                            <div class="d-block mb-5">
                                <div class="form-check form-check-custom form-check-solid form-check-sm">
                                    <label class="form-check-label" for="popup_type_screen">
                                        <input class="form-check-input typePopup" type="radio" name="type" value="screen" id="popup_type_screen"  {{ isset($popup->type) && $popup->type === 'screen' ? 'checked' : '' }}/>
                                        Screen ({{ __('JsPress::backend.popup.fullscreen') }})
                                        <img class="mt-5 d-block" src="{{asset('jspress/assets/media/popup/perde.png')}}" alt="">
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dependsPopupType mb-10 {{ isset($popup->type) && $popup->type === 'tooltip' ? '':'d-none' }}" data-type="tooltip">
                        <label class="form-label">{{ __('JsPress::backend.popup.tooltip_place') }}</label>
                        <div class="d-flex">
                            <div class="form-check form-check-custom form-check-solid form-check-sm me-3">
                                <input class="form-check-input" type="radio" name="place" value="left" id="popup_place_tooltip_left"  {{ isset($popup->place) && $popup->place === 'left' ? 'checked' : '' }}/>
                                <label class="form-check-label" for="popup_place_tooltip_left">
                                    {{__('JsPress::backend.popup.left_bottom')}}
                                </label>
                            </div>
                            <div class="form-check form-check-custom form-check-solid form-check-sm">
                                <input class="form-check-input" type="radio" name="place" value="right" id="popup_place_tooltip_right" {{ isset($popup->place) && $popup->place === 'right' ? 'checked' : '' }}/>
                                <label class="form-check-label" for="popup_place_tooltip_right">
                                    {{__('JsPress::backend.popup.right_bottom')}}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="dependsPopupType mb-10 {{ isset($popup->type) && $popup->type === 'bar' ? '':'d-none' }}" data-type="bar">
                        <label class="form-label">Bar Yeri</label>
                        <div class="d-flex">
                            <div class="form-check form-check-custom form-check-solid form-check-sm me-3">
                                <input class="form-check-input" type="radio" name="place" value="top" id="popup_place_bar_top" {{ isset($popup->place) && $popup->place === 'top' ? 'checked' : '' }}/>
                                <label class="form-check-label" for="popup_place_bar_top">
                                    {{ __('JsPress::backend.popup.top') }}
                                </label>
                            </div>
                            <div class="form-check form-check-custom form-check-solid form-check-sm">
                                <input class="form-check-input" type="radio" name="place" value="bottom" id="popup_place_bar_bottom" {{ isset($popup->place) && $popup->place === 'bottom' ? 'checked' : '' }}/>
                                <label class="form-check-label" for="popup_place_bar_bottom">
                                    {{ __('JsPress::backend.popup.bottom') }}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="separator my-10"></div>
                    <div class="mb-10">
                        <label class="required form-label">{{ __('JsPress::backend.popup.is_dialog_title') }}</label>
                        <div class="d-flex">
                            <div class="form-check form-check-custom form-check-solid form-check-sm me-3">
                                <input class="form-check-input" type="radio" name="is_display_name" value="1" id="is_display_name_yes" {{ isset($popup->is_display_name) && $popup->is_display_name == 1 ? 'checked' : '' }}/>
                                <label class="form-check-label" for="is_display_name_yes">
                                    {{ __('JsPress::backend.yes') }}
                                </label>
                            </div>
                            <div class="form-check form-check-custom form-check-solid form-check-sm">
                                <input class="form-check-input" type="radio" name="is_display_name" value="0" id="is_display_name_no" {{ isset($popup->is_display_name) && $popup->is_display_name == 0 ? 'checked' : ($popup->is_display_name ?? 'checked') }}/>
                                <label class="form-check-label" for="is_display_name_no">
                                    {{ __('JsPress::backend.no') }}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="dependsPopupType {{ isset($popup->type) && $popup->type === 'tooltip' ? '':'d-none' }} mb-10" data-type="tooltip">
                        <label class="required form-label">{{ __('JsPress::backend.popup.is_tooltip_content_image') }}</label>
                        <div class="d-flex">
                            <div class="form-check form-check-custom form-check-solid form-check-sm me-3">
                                <input class="form-check-input" type="radio" name="is_content_image" value="1" id="is_content_image_yes" {{ isset($popup->is_content_image) && $popup->is_content_image == 1 ? 'checked' : '' }}/>
                                <label class="form-check-label" for="is_content_image_yes">
                                    {{ __('JsPress::backend.yes') }}
                                </label>
                            </div>
                            <div class="form-check form-check-custom form-check-solid form-check-sm">
                                <input class="form-check-input" type="radio" name="is_content_image" value="0" id="is_content_image_no" {{ isset($popup->is_content_image) && $popup->is_content_image == 0 ? 'checked' : ($popup->is_content_image ?? 'checked') }}/>
                                <label class="form-check-label" for="is_content_image_no">
                                    {{ __('JsPress::backend.no') }}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="dependsPopupType {{ isset($popup->type) && $popup->type === 'bar' ? '':'d-none' }} mb-10" data-type="bar">
                        <label class="required form-label">{{ __('JsPress::backend.popup.button_type') }}</label>
                        <div class="d-flex">
                            <div class="form-check form-check-custom form-check-solid form-check-sm me-3">
                                <input class="form-check-input" type="radio" name="button_type" value="1" id="button_type_close" {{ isset($popup->button_type) && $popup->button_type == 1 ? 'checked' : ($popup->button_type ?? 'checked') }}/>
                                <label class="form-check-label" for="button_type_close">
                                    {{ __('JsPress::backend.popup.close_button') }}
                                </label>
                            </div>
                            <div class="form-check form-check-custom form-check-solid form-check-sm">
                                <input class="form-check-input" type="radio" name="button_type" value="0" id="button_type_confirmation" {{ isset($popup->button_type) && $popup->button_type == 0 ? 'checked' : '' }}/>
                                <label class="form-check-label" for="button_type_confirmation">
                                    {{ __('JsPress::backend.popup.approved_button') }}
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="dependsPopupType dependsPopupTypeBar {{ isset($popup->type) &&  ( ( $popup->type === 'bar' && $popup->button_type === 0 ) || $popup->type === 'screen' ) ? '':'d-none' }} mb-10" data-type="screen">
                        <label class="required form-label">{{ __('JsPress::backend.popup.button_text') }}</label>
                        <input type="text" name="button_text" class="form-control mb-2" value="{{ $popup->button_text ?? __('JsPress::backend.popup.button_text_placeholder') }}" autocomplete="off"/>
                    </div>
                    <div class="mb-10">
                        <label class="form-label">{{__('JsPress::backend.content')}}</label>
                        <textarea name="content" class="js_editor form-control">{!! $popup->content ?? '' !!}</textarea>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="card card-flush py-4 mb-8">
                <div class="card-header">
                    <div class="card-title">
                        <h2>{{ __('JsPress::backend.popup.popup_advanced_settings') }}</h2>
                    </div>
                </div>
                <div class="card-body pt-0">
                    <div class="mb-5">
                        <label class="required form-label">{{__('JsPress::backend.status')}}</label>
                        <select class="form-select mb-2" name="status" data-control="select2" data-hide-search="true" data-placeholder="Durum seçiniz">
                            <option value="1" {{ isset($popup->status) && $popup->status == 1 ? 'selected':($popup->status ?? 'selected') }}>{{__('JsPress::backend.active')}}</option>
                            <option value="0" {{ isset($popup->status) && $popup->status == 0 ? 'selected':'' }}>{{__('JsPress::backend.deactive')}}</option>
                        </select>
                    </div>
                    <div class="mb-5">
                        <label class="required form-label">{{__('JsPress::backend.order')}}</label>
                        <input type="number" name="order" class="form-control mb-2" placeholder="Sıra numarası giriniz..." value="{{ $popup->order ?? '' }}" autocomplete="off"/>
                    </div>
                    <button type="submit" id="create_popup_btn" class="btn btn-primary">
                        @isset($popup->id)
                            <input type="hidden" name="id" value="{{$popup->id}}" />
                        @endisset
                        {{ isset($popup) ? __('JsPress::backend.update') : __('JsPress::backend.add_new') }}
                    </button>
                </div>
            </div>
            <div class="accordion w-100" id="popup_accordion">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="popup_accordion_header_1">
                        <button class="accordion-button fs-4 fw-bold collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#popup_accordion_body_1" aria-expanded="true" aria-controls="popup_accordion_body_1">
                            {{ __('JsPress::backend.popup.screen_place') }}
                        </button>
                    </h2>
                    <div id="popup_accordion_body_1" class="accordion-collapse collapse" aria-labelledby="popup_accordion_header_1" data-bs-parent="#popup_accordion">
                        <div class="accordion-body">
                            <div class="mb-5">
                                <label class="required form-label">{{ __('JsPress::backend.popup.device') }}</label>
                                <div class="d-flex flex-wrap">
                                    <div class="form-check form-check-custom form-check-solid me-5">
                                        <input class="form-check-input" name="devices[]" type="checkbox" value="desktop" id="device" {{ isset($popup->devices) && in_array('desktop', $popup->devices->pluck('device')->toArray()) ? 'checked' : ($popup->devices ?? 'checked') }}/>
                                        <label class="form-check-label" for="device">
                                            {{ __('JsPress::backend.desktop') }}
                                        </label>
                                    </div>
                                    <div class="form-check form-check-custom form-check-solid me-5">
                                        <input class="form-check-input" name="devices[]" type="checkbox" value="mobile" id="mobile" {{ isset($popup->devices) && in_array('mobile', $popup->devices->pluck('device')->toArray()) ? 'checked' : '' }}/>
                                        <label class="form-check-label" for="mobile">
                                            {{ __('JsPress::backend.mobile') }}
                                        </label>
                                    </div>
                                    <div class="form-check form-check-custom form-check-solid">
                                        <input class="form-check-input" name="devices[]" type="checkbox" value="tablet" id="tablet" {{ isset($popup->devices) && in_array('tablet', $popup->devices->pluck('device')->toArray()) ? 'checked' : '' }}/>
                                        <label class="form-check-label" for="tablet">
                                            {{ __('JsPress::backend.tablet') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="separator my-10"></div>
                            <div class="mb-10">
                                <label class="required form-label">{{ __('JsPress::backend.popup.view_count') }}</label>
                                <div class="d-flex flex-wrap">
                                    <div class="form-check form-check-custom form-check-solid me-5">
                                        <input class="form-check-input pageShowType" name="page_show_type" type="radio" value="0" id="page_show_type_all" {{ isset($popup->page_show_type) && $popup->page_show_type == 0 ? 'checked' : ($popup->page_show_type ?? 'checked') }}/>
                                        <label class="form-check-label" for="page_show_type_all">
                                            {{ __('JsPress::backend.popup.all_pages') }}
                                        </label>
                                    </div>
                                    <div class="form-check form-check-custom form-check-solid me-5">
                                        <input class="form-check-input pageShowType" name="page_show_type" type="radio" value="1" id="page_show_type_some" {{ isset($popup->page_show_type) && $popup->page_show_type == 1 ? 'checked' : '' }}/>
                                        <label class="form-check-label" for="page_show_type_some">
                                            {{ __('JsPress::backend.popup.specific_pages') }}
                                        </label>
                                    </div>
                                    <div class="form-check form-check-custom form-check-solid me-5">
                                        <input class="form-check-input pageShowType" name="page_show_type" type="radio" value="2" id="page_show_type_homepage" {{ isset($popup->page_show_type) && $popup->page_show_type == 2 ? 'checked' : '' }}/>
                                        <label class="form-check-label" for="page_show_type_homepage">
                                            {{ __('JsPress::backend.popup.homepage') }}
                                        </label>
                                    </div>
                                    <div class="form-check form-check-custom form-check-solid me-5">
                                        <input class="form-check-input pageShowType" name="page_show_type" type="radio" value="3" id="page_show_type_custom" {{ isset($popup->page_show_type) && $popup->page_show_type == 3 ? 'checked' : '' }}/>
                                        <label class="form-check-label" for="page_show_type_custom">
                                            {{ __('JsPress::backend.popup.custom_url') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="dependsPopupPageShow mb-10 {{ isset($popup->page_show_type) && $popup->page_show_type == 1 ? '':'d-none' }}" data-type="show_posts">
                                <label class="required form-label"> {{ __('JsPress::backend.popup.languages') }}</label>
                                <select class="form-select" name="languages[]" data-control="select2" data-placeholder="Dil Seçiniz" data-hide-search="true" multiple="multiple">
                                    <option></option>
                                    @foreach(website_languages() as $language)
                                        <option value="{{$language->locale}}" {{ isset($popup->locales) && in_array($language->locale, $popup->locales->pluck('locale')->toArray()) ? 'selected':'' }}>{{display_language($language->locale)->display_language_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="dependsPopupPageShow mb-10 {{ isset($popup->page_show_type) && $popup->page_show_type == 1 ? '':'d-none' }}" data-type="show_posts">
                                <label class="required form-label">{{ __('JsPress::backend.popup.pages_to_show') }}</label>
                                <select class="form-select pageSelect" name="element_ids[]" multiple="multiple">
                                    <option></option>
                                </select>
                                <input type="hidden" name="page_urls" value="{{ $popup_pages['value'] ?? ''}}"/>
                            </div>
                            <div class="dependsPopupPageShow mb-10 {{ isset($popup->page_show_type) && $popup->page_show_type == 3 ? '':'d-none' }}" data-type="custom_url">
                                <label class="required form-label">{{ __('JsPress::backend.popup.links') }}</label>
                                <textarea class="form-control" name="custom_url">{{ isset($popup->page_show_type) ? implode(PHP_EOL, $popup->urls->pluck('url')->toArray()) : '' }}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--- Limits --->
            <div class="accordion w-100 mt-5" id="popup_accordion_2">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="popup_accordion_2_header_1">
                        <button class="accordion-button fs-4 fw-bold collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#popup_accordion_2_body_1" aria-expanded="true" aria-controls="popup_accordion_2_body_1">
                            {{ __('JsPress::backend.popup.limits') }}
                        </button>
                    </h2>
                    <div id="popup_accordion_2_body_1" class="accordion-collapse collapse" aria-labelledby="popup_accordion_2_header_1" data-bs-parent="#popup_accordion_2">
                        <div class="accordion-body">
                            <div class="mb-5">
                                <label class="required form-label">{{ __('JsPress::backend.popup.same_person_view_count') }}</label>
                                <div class="d-flex flex-wrap">
                                    <div class="form-check form-check-custom form-check-solid me-5">
                                        <input class="form-check-input impressionInput" name="impressions" type="radio" value="0" id="impressions_zero" {{ isset($popup->impressions) && $popup->impressions == 0 ? 'checked':($popup->impressions ?? 'checked') }}/>
                                        <label class="form-check-label" for="impressions_zero">
                                            {{ __('JsPress::backend.popup.always') }}
                                        </label>
                                    </div>
                                    <div class="form-check form-check-custom form-check-solid me-5">
                                        <input class="form-check-input impressionInput" name="impressions" type="radio" value="1" id="impressions_one" {{ isset($popup->impressions) && $popup->impressions == 1 ? 'checked':'' }}/>
                                        <label class="form-check-label" for="impressions_one">
                                            {{ __('JsPress::backend.popup.same_device') }}
                                        </label>
                                    </div>
                                    <div class="form-check form-check-custom form-check-solid">
                                        <input class="form-check-input impressionInput" name="impressions" type="radio" value="2" id="impressions_two" {{ isset($popup->impressions) && $popup->impressions == 2 ? 'checked':'' }}/>
                                        <label class="form-check-label" for="impressions_two">
                                            {{ __('JsPress::backend.popup.same_session') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="dependsPopupImression mb-5 {{ isset($popup->impressions) && ($popup->impressions == 1 || $popup->impressions == 2) ? '':'d-none' }}" data-type="impression">
                                <div class="input-group input-group-solid mb-5">
                                    <span class="input-group-text"> {{ __('JsPress::backend.popup.same_sessiong_first') }}</span>
                                    <input type="text" name="impression_count" class="form-control" value="{{ $popup->impression_count ?? 3 }}">
                                    <span class="input-group-text">{{ __('JsPress::backend.popup.same_sessiong_last') }}.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--- Time --->
            <div class="accordion w-100 mt-5" id="popup_accordion_3">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="popup_accordion_3_header_1">
                        <button class="accordion-button fs-4 fw-bold collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#popup_accordion_3_body_1" aria-expanded="true" aria-controls="popup_accordion_3_body_1">
                            {{ __('JsPress::backend.popup.duration_settings') }}
                        </button>
                    </h2>
                    <div id="popup_accordion_3_body_1" class="accordion-collapse collapse" aria-labelledby="popup_accordion_3_header_1" data-bs-parent="#popup_accordion_3">
                        <div class="accordion-body">
                            <div class="mb-10">
                                <label class="required form-label">{{ __('JsPress::backend.popup.how_long_duration') }}</label>
                                <div class="d-flex flex-wrap">
                                    <div class="form-check form-check-custom form-check-solid me-5">
                                        <input class="form-check-input dateTypeInput" name="date_type" type="radio" value="0" id="date_type_zero" {{ isset($popup->date_type) && $popup->date_type == 0 ? 'checked':($popup->date_type ?? 'checked') }}/>
                                        <label class="form-check-label" for="date_type_zero">
                                            {{ __('JsPress::backend.popup.always') }}
                                        </label>
                                    </div>
                                    <div class="form-check form-check-custom form-check-solid me-5">
                                        <input class="form-check-input dateTypeInput" name="date_type" type="radio" value="1" id="date_type_one" {{ isset($popup->date_type) && $popup->date_type == 1 ? 'checked':'' }}/>
                                        <label class="form-check-label" for="date_type_one">
                                            {{ __('JsPress::backend.popup.specific_dates') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="dependsPopupDateType mb-10 {{ isset($popup->date_type) && $popup->date_type == 1 ? '':'d-none' }}" data-type="date_type">
                                <div class="d-flex flex-wrap">
                                    <div class="d-block me-2">
                                        <label class="required form-label">{{__('JsPress::backend.start_date')}}</label>
                                        <input class="jspress-datepicker form-control form-control-solid" name="start_date" data-timepicker="false" data-format="YYYY-MM-DD" placeholder="{{ __('JsPress::backend.select_a_date') }}" value="{{$popup->start_date ?? ''}}" readonly/>
                                    </div>
                                    <div class="d-block">
                                        <label class="required form-label">{{__('JsPress::backend.end_date')}}</label>
                                        <input class="jspress-datepicker form-control form-control-solid" name="end_date" data-timepicker="false" data-format="YYYY-MM-DD" placeholder="{{ __('JsPress::backend.select_a_date') }}" value="{{$popup->end_date ?? ''}}" readonly/>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-10">
                                <label class="required form-label">{{ __('JsPress::backend.popup.how_long_should_it_appear') }}</label>
                                <div class="d-flex flex-wrap">
                                    <div class="form-check form-check-custom form-check-solid me-5">
                                        <input class="form-check-input timeTypeInput" name="time_type" type="radio" value="0" id="time_type_zero" {{ isset($popup->time_type) && $popup->time_type == 0 ? 'checked':($popup->time_type ?? 'checked') }}/>
                                        <label class="form-check-label" for="time_type_zero">
                                            {{ __('JsPress::backend.popup.always_visible') }}
                                        </label>
                                    </div>
                                    <div class="form-check form-check-custom form-check-solid me-5">
                                        <input class="form-check-input timeTypeInput" name="time_type" type="radio" value="1" id="time_type_one" {{ isset($popup->time_type) && $popup->time_type == 1 ? 'checked':'' }}/>
                                        <label class="form-check-label" for="time_type_one">
                                            {{ __('JsPress::backend.popup.appear_for_a_specific_time') }}
                                        </label>
                                    </div>
                                    <div class="form-check form-check-custom form-check-solid me-5">
                                        <input class="form-check-input timeTypeInput" name="time_type" type="radio" value="2" id="time_type_two" {{ isset($popup->time_type) && $popup->time_type == 2 ? 'checked':'' }}/>
                                        <label class="form-check-label" for="time_type_two">
                                            {{ __('JsPress::backend.popup.appear_till_button_click') }}
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="dependsPopupTimeType mb-10 {{ isset($popup->time_type) && $popup->time_type == 1 ? '':'d-none' }}" data-type="time_type">
                                <label class="required form-label">{{ __('JsPress::backend.popup.duration_second') }}</label>
                                <input type="text" class="form-control" name="display_time" value="{{ $popup->display_time ?? 60 }}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Triggers -->
            <div class="accordion w-100 mt-5" id="popup_accordion_4">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="popup_accordion_4_header_1">
                        <button class="accordion-button fs-4 fw-bold collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#popup_accordion_4_body_1" aria-expanded="true" aria-controls="popup_accordion_4_body_1">
                            {{ __('JsPress::backend.popup.triggers') }}
                        </button>
                    </h2>
                    <div id="popup_accordion_4_body_1" class="accordion-collapse collapse" aria-labelledby="popup_accordion_4_header_1" data-bs-parent="#popup_accordion_4">
                        <div class="accordion-body">
                            <div class="mb-10">
                                <label class="form-label">{{ __('JsPress::backend.popup.in_which_conditions_to_show') }}</label>
                                <div class="d-flex flex-wrap">
                                    <div class="form-check form-check-custom form-check-solid me-5">
                                        <input class="form-check-input conditionInput" name="conditions" type="radio" value="0" id="conditions_zero" {{ isset($popup->conditions) && $popup->conditions == 0 ? 'checked':($popup->conditions ?? 'checked') }}/>
                                        <label class="form-check-label" for="conditions_zero">
                                            {{ __('JsPress::backend.popup.duration_second') }}
                                        </label>
                                    </div>
                                    <div class="form-check form-check-custom form-check-solid me-5">
                                        <input class="form-check-input conditionInput" name="conditions" type="radio" value="1" id="conditions_one" {{ isset($popup->conditions) && $popup->conditions == 1 ? 'checked':'' }}/>
                                        <label class="form-check-label" for="conditions_one">
                                            Scroll (Pixel)
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="dependsPopupConditions w-100 {{ isset($popup->conditions) && $popup->conditions == 0 ? '': ( isset($popup->conditions) ? 'd-none':'' ) }}" data-type="condition_time">
                                <label class="form-label">{{ __('JsPress::backend.popup.type_duration_in_time') }}</label>
                                <input type="text" class="form-control" name="condition_time" value="{{ $popup->condition_time ?? '1' }}"/>
                            </div>
                            <div class="dependsPopupConditions w-100 {{ isset($popup->conditions) && $popup->conditions == 1 ? '':'d-none' }}" data-type="condition_scroll">
                                <label class="form-label">{{ __('JsPress::backend.popup.type_scroll_height_in_pixel') }}</label>
                                <input type="text" class="form-control" name="condition_scroll" value="{{ $popup->condition_scroll ?? '100' }}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Design -->
            <div class="accordion w-100 mt-5" id="popup_accordion_5">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="popup_accordion_5_header_1">
                        <button class="accordion-button fs-4 fw-bold collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#popup_accordion_5_body_1" aria-expanded="true" aria-controls="popup_accordion_5_body_1">
                            {{ __('JsPress::backend.popup.design') }}
                        </button>
                    </h2>
                    <div id="popup_accordion_5_body_1" class="accordion-collapse collapse" aria-labelledby="popup_accordion_5_header_1" data-bs-parent="#popup_accordion_5">
                        <div class="accordion-body">
                            <div class="row">
                                <div class="col-lg-4 mb-10">
                                    <label class="form-label">Font</label>
                                    <select class="form-select googleFontSelect" name="design[font]">
                                        <option value="default">{{ __('JsPress::backend.default') }}</option>
                                    </select>
                                    <input type="hidden" name="design[font_data]" value="{{ isset($popup->design['font_data']) ? json_encode($popup->design['font_data']) : '' }}"/>
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && $popup->type == 'lightbox' ? '': (isset($popup->type) ? 'd-none':'') }}" data-types='["lightbox"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.border_width') }}</label>
                                    <input type="number" class="form-control" name="design[border_size]" min="0" value="{{ $popup->design['border_size'] ?? 5 }}">
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && $popup->type == 'lightbox' ? '': (isset($popup->type) ? 'd-none':'') }}" data-types='["lightbox"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.border_color') }}</label>
                                    <input type="color" class="form-control h-40px" name="design[border_color]" value="{{ $popup->design['border_color'] ?? '#000000' }}">
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && $popup->type == 'lightbox' ? '': (isset($popup->type) ? 'd-none':'') }}" data-types='["lightbox"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.padding_value') }}</label>
                                    <input type="number" class="form-control" name="design[padding]" min="0" value="{{$popup->design['padding'] ?? '15'}}">
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && ($popup->type == 'lightbox' || $popup->type == 'tooltip' || $popup->type == 'bar') ? '': (isset($popup->type) ? 'd-none':'') }}" data-types='["lightbox", "tooltip", "bar"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.button_background_color') }}</label>
                                    <input type="color" class="form-control h-40px" name="design[button_background_color]" value="{{ $popup->design['button_background_color'] ?? '#0b1f8f' }}">
                                </div>
                                <div class="popupDesign col-lg-4 mb-10" data-types='["lightbox", "tooltip", "bar", "screen"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.button_color') }}</label>
                                    <input type="color" class="form-control h-40px" name="design[button_color]" value="{{ $popup->design['button_color'] ?? '#ffffff' }}">
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && $popup->type == 'tooltip' ? '': 'd-none' }}" data-types='["tooltip"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.background_backdrop') }}</label>
                                    <select class="form-select mb-2" name="design[is_backdrop]">
                                        <option value="1" {{ isset($popup->design['background_backdrop']) && $popup->design['background_backdrop'] == '1' ? 'selected':'' }}>{{ __('JsPress::backend.active') }}</option>
                                        <option value="2" {{ isset($popup->design['background_backdrop']) && $popup->design['background_backdrop'] == '2' ? 'selected':'' }}>{{ __('JsPress::backend.deactive') }}</option>
                                    </select>
                                    <small class="text-warning">{{ __('JsPress::backend.popup.apply_transparent_while_backdrop_active') }}</small>
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && ($popup->type == 'tooltip' || $popup->type == 'bar') ? '': 'd-none' }}" data-types='["tooltip", "bar"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.dialog_background_color') }}</label>
                                    <input type="color" class="form-control h-40px" name="design[dialog_background_color]" value="{{ $popup->design['dialog_background_color'] ?? '#ffffff' }}">
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && ($popup->type == 'tooltip' || $popup->type == 'bar' || $popup->type == 'screen') ? '': 'd-none' }}" data-types='["tooltip", "bar", "screen"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.text_color') }}</label>
                                    <input type="color" class="form-control h-40px" name="design[color]" value="{{ $popup->design['color'] ?? '#000000' }}">
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && ($popup->type == 'lightbox' || $popup->type == 'tooltip' || $popup->type == 'bar') ? '': (isset($popup->type) ? 'd-none':'') }}" data-types='["lightbox", "tooltip", "bar"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.link_color') }}</label>
                                    <input type="color" class="form-control h-40px" name="design[link_color]" value="{{ $popup->design['link_color'] ?? '#0b1f8f' }}">
                                    <small class="text-warning">{{ __('JsPress::backend.popup.applying_to_links_in_content') }}</small>
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && $popup->type == 'screen' ? '': 'd-none' }}" data-types='["screen"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.title_font_size') }}</label>
                                    <input type="number" class="form-control" name="design[title_font_size]" min="0" value="{{ $popup->design['title_font_size'] ?? '20' }}">
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && $popup->type == 'screen' ? '': 'd-none' }}" data-types='["screen"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.title_font_color') }}</label>
                                    <input type="color" class="form-control h-40px" name="design[title_color]" value="{{ $popup->design['title_color'] ?? '#000000' }}">
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && $popup->type == 'screen' ? '': 'd-none' }}" data-types='["screen"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.title_animation') }}</label>
                                    <select class="form-select mb-2" name="design[title_animation]">
                                        <option value="fadeIn" {{ isset($popup->design['title_animation']) && $popup->design['title_animation'] == 'fadeIn' ? 'selected':($popup->design['title_animation'] ?? 'selected') }}>{{ __('JsPress::backend.popup.fade_in') }}</option>
                                        <option value="fadeInLeft" {{ isset($popup->design['title_animation']) && $popup->design['title_animation'] == 'fadeInLeft' ? 'selected':'' }}>{{ __('JsPress::backend.popup.left_to_right') }}</option>
                                        <option value="fadeInRight" {{ isset($popup->design['title_animation']) && $popup->design['title_animation'] == 'fadeInRight' ? 'selected':'' }}>{{ __('JsPress::backend.popup.right_to_left') }}</option>
                                        <option value="fadeInDown" {{ isset($popup->design['title_animation']) && $popup->design['title_animation'] == 'fadeInDown' ? 'selected':'' }}>{{ __('JsPress::backend.popup.bottom_to_top') }}</option>
                                        <option value="fadeInUp" {{ isset($popup->design['title_animation']) && $popup->design['title_animation'] == 'fadeInUp' ? 'selected':'' }}>{{ __('JsPress::backend.popup.top_to_bottom') }}</option>
                                    </select>
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && $popup->type == 'screen' ? '': 'd-none' }}" data-types='["screen"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.title_space') }}</label>
                                    <input type="number" class="form-control" name="design[title_space]" min="0" value="{{ $popup->design['title_space'] ?? '10' }}">
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && $popup->type == 'screen' ? '': 'd-none' }}" data-types='["screen"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.content_font_size') }}</label>
                                    <input type="number" class="form-control" name="design[content_font_size]" min="0" value="{{ $popup->design['content_font_size'] ?? '17' }}">
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && $popup->type == 'screen' ? '': 'd-none' }}" data-types='["screen"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.content_animation') }}</label>
                                    <select class="form-select mb-2" name="design[content_animation]">
                                        <option value="fadeIn" {{ isset($popup->design['content_animation']) && $popup->design['content_animation'] == 'fadeIn' ? 'selected':($popup->design['content_animation'] ?? 'selected') }}>{{ __('JsPress::backend.popup.fade_in') }}</option>
                                        <option value="fadeInLeft" {{ isset($popup->design['content_animation']) && $popup->design['content_animation'] == 'fadeInLeft' ? 'selected':'' }}>{{ __('JsPress::backend.popup.left_to_right') }}</option>
                                        <option value="fadeInRight" {{ isset($popup->design['content_animation']) && $popup->design['content_animation'] == 'fadeInRight' ? 'selected':'' }}>{{ __('JsPress::backend.popup.right_to_left') }}</option>
                                        <option value="fadeInDown" {{ isset($popup->design['content_animation']) && $popup->design['content_animation'] == 'fadeInDown' ? 'selected':'' }}>{{ __('JsPress::backend.popup.bottom_to_top') }}</option>
                                        <option value="fadeInUp" {{ isset($popup->design['content_animation']) && $popup->design['content_animation'] == 'fadeInUp' ? 'selected':'' }}>{{ __('JsPress::backend.popup.top_to_bottom') }}</option>
                                    </select>
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && $popup->type == 'screen' ? '': 'd-none' }}" data-types='["screen"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.next_button_font_size') }}</label>
                                    <input type="number" class="form-control" name="design[button_font_size]" min="0" value="{{ $popup->design['button_font_size'] ?? '12' }}">
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && $popup->type == 'screen' ? '': 'd-none' }}" data-types='["screen"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.next_button_position') }}</label>
                                    <select class="form-select mb-2" name="design[button_position]">
                                        <option value="right" {{ isset($popup->design['button_position']) && $popup->design['button_position'] == 'right' ? 'selected':($popup->design['button_position'] ?? 'selected') }}>{{ __('JsPress::backend.popup.right') }}</option>
                                        <option value="left" {{ isset($popup->design['button_position']) && $popup->design['button_position'] == 'left' ? 'selected':'' }}>{{ __('JsPress::backend.popup.left') }}</option>
                                    </select>
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && $popup->type == 'screen' ? '': 'd-none' }}" data-types='["screen"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.counter_font_size') }}</label>
                                    <input type="number" class="form-control" name="design[counter_font_size]" min="0" value="{{ $popup->design['counter_font_size'] ?? '12' }}">
                                </div>
                                <div class="popupDesign col-lg-4 mb-10 {{ isset($popup->type) && $popup->type == 'lightbox' ? '': (isset($popup->type) ? 'd-none':'') }}" data-types='["lightbox","tooltip"]'>
                                    <label class="form-label">{{ __('JsPress::backend.popup.border_radius') }}</label>
                                    <input type="number" class="form-control" name="design[border_radius]" min="0" value="{{ $popup->design['border_radius'] ?? '0' }}">
                                </div>
                                <div class="col-lg-12 mb-10">
                                    <label class="form-label">{{ __('JsPress::backend.popup.custom_style') }}</label>
                                    <textarea class="form-control" rows="6" name="custom_css">{{ $popup->custom_css ?? '' }}</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
