@extends('JsPress::admin.layouts.app')
@push('style')
    <style>
        label.error {
            display: block;
        }
        .cookineConsentContent.active {
            background-color: #1e1e2d;
        }
        .dependsOnLanguage{
            display:none;
        }
        .dependsOnLanguage.active{
            display:block;
        }
        .dependsOnLanguage.show{
            animation: fadeIn 0.3s;
        }
        @keyframes fadeIn {
            0% { opacity: 0; }
            100% { opacity: 1; }
        }
    </style>
@endpush
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{__('JsPress::backend.cookie_consent.title')}}</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="js_popup">
            <div id="kt_content_container" class="container-fluid">
                <div class="container-fluid p-0">
                    @if(Session::has('success'))
                        <x-jspress-alert type="success" :title="__('JsPress::backend.success')" :message="session('success')"></x-jspress-alert>
                    @endif
                    <form id="create_cookie_form" action="{{route('admin.cookie.store')}}" method="POST" class="form" enctype="multipart/form-data">
                        @csrf
                        <div class="row">
                            <div class="col-lg-8">
                                <div class="card w-100 mb-10">
                                    <div class="card-header">
                                        <h3 class="card-title align-items-start flex-column">
                                            <span class="card-label fw-bolder fs-3 mb-1">Çerez Ayarları</span>
                                            <span class="text-muted mt-1 fw-bold fs-7">Global çerez ayarlarını buradan yapabilirsiniz.</span>
                                        </h3>
                                    </div>
                                    <div class="card-body">
                                        <div class="row mb-8">
                                            <div class="col-xl-3">
                                                <div class="fs-6 fw-bold mt-2 mb-3">{{ __('JsPress::backend.cookie_consent.form.properties') }}</div>
                                            </div>
                                            <div class="col-xl-9">
                                                <div class="d-flex fw-bold h-100">
                                                    <div class="form-check form-check-custom form-check-solid me-9">
                                                        <input class="form-check-input" name="categories[]" type="checkbox" value="1" id="categories_necessary" checked disabled>
                                                        <label class="form-check-label ms-3">{{ __('JsPress::backend.cookie_consent.form.necessary_cookie') }}</label>
                                                    </div>
                                                    <div class="form-check form-check-custom form-check-solid me-9">
                                                        <input class="form-check-input" name="categories[]" type="checkbox" value="2" id="categories_functional" {{ !is_null($cookie) && in_array(2, $cookie->categories) ? 'checked': ( is_null($cookie) ? 'checked':'' ) }}>
                                                        <label class="form-check-label ms-3" for="categories_functional">{{ __('JsPress::backend.cookie_consent.form.functional_cookie') }}</label>
                                                    </div>
                                                    <div class="form-check form-check-custom form-check-solid me-9">
                                                        <input class="form-check-input" name="categories[]" type="checkbox" value="3" id="categories_analystic" {{ !is_null($cookie) && in_array(3, $cookie->categories) ? 'checked': ( is_null($cookie) ? 'checked':'' ) }}>
                                                        <label class="form-check-label ms-3" for="categories_analystic">{{ __('JsPress::backend.cookie_consent.form.analystic_cookie') }}</label>
                                                    </div>
                                                    <div class="form-check form-check-custom form-check-solid">
                                                        <input class="form-check-input" name="categories[]" type="checkbox" value="4" id="categories_marketing" {{ !is_null($cookie) && in_array(4, $cookie->categories) ? 'checked': ( is_null($cookie) ? 'checked':'' ) }}>
                                                        <label class="form-check-label ms-3" for="categories_marketing">{{ __('JsPress::backend.cookie_consent.form.marketing_cookie') }}</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row mb-8">
                                            <div class="col-xl-3">
                                                <div class="fs-6 fw-bold mt-2 mb-3">{{ __('JsPress::backend.cookie_consent.language_appearance') }}</div>
                                            </div>
                                            <div class="col-xl-9">
                                                <div class="d-flex flex-wrap fw-bold h-100">
                                                    @foreach(website_languages() as $language)
                                                        <div class="languageItem form-check form-check-custom form-check-solid me-9 border py-3 px-3 mb-3 rounded-3">
                                                            <input class="form-check-input" name="languages_data[]" type="checkbox" value="{{$language->locale}}" id="active_locale_{{$language->locale}}" {{ !is_null($cookie) && in_array($language->locale, $cookie->languages) ? 'checked': ( is_null($cookie) ? 'checked':'' ) }}>
                                                            <label for="active_locale_{{$language->locale}}" class="form-check-label d-flex ms-3">
                                                                <img src="{{asset('jspress/assets/media/flags/svg/'.$language->locale.'.svg')}}" class="me-2" width="25"/>
                                                                <span>{{ display_language($language->locale)->display_language_name }}</span>
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-flex w-100">
                                    @foreach( website_languages() as $key => $language )
                                        <div class="cookineConsentContent mw-200px py-3 px-5 cursor-pointer {{$key === 0 ? 'active':''}}" data-locale="{{$language->locale}}">
                                            <img src="{{asset('jspress/assets/media/flags/svg/'.$language->locale.'.svg')}}" class="me-2" width="25" alt="{{$language->locale}}"/>
                                            <span>{{ display_language($language->locale)->display_language_name }}</span>
                                        </div>
                                    @endforeach
                                </div>
                                @foreach( website_languages() as $key => $language )
                                    <div class="w-100 dependsOnLanguage {{ $key === 0 ? 'active show':'' }}" data-locale="{{$language->locale}}">
                                        <div class="card rounded-0 w-100 mb-10">
                                            <div class="card-header py-5">
                                                <div class="card-title">
                                                    <h3 class="card-title align-items-start flex-column">
                                                        <span class="card-label fw-bolder fs-3 mb-1">{{ __('JsPress::backend.cookie_consent.card_header_title') }}</span>
                                                        <span class="text-muted mt-1 fw-bold fs-7">{{ __('JsPress::backend.cookie_consent.card_header_description') }}</span>
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="mb-10">
                                                    <label class="required form-label">{{__('JsPress::backend.cookie_consent.cookie_consent_modal_title')}}
                                                        <span class="svg-icon svg-icon-muted svg-icon-2hx cursor-pointer" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="<img src='{{asset('jspress/assets/media/cookie/cookie_consent_modal_title.png')}}' height='300' />">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                                                <rect x="11" y="17" width="7" height="2" rx="1" transform="rotate(-90 11 17)" fill="black"/>
                                                                <rect x="11" y="9" width="2" height="2" rx="1" transform="rotate(-90 11 9)" fill="black"/>
                                                            </svg>
                                                        </span>
                                                    </label>
                                                    <input type="text" name="languages[{{$language->locale}}][consentModal][title]" class="form-control mb-2" placeholder="{{ __('JsPress::backend.type_title') }}" value="{{ $cookie->translations[$language->locale]['consentModal']['title'] ?? __('JsPress::backend.cookie_consent.cookie_consent_modal_title_value') }}" autocomplete="off" required/>
                                                    <small class="text-warning">{{ __('JsPress::backend.cookie_consent.form.title_description') }}</small>
                                                </div>
                                                <div class="mb-10">
                                                    <label class="required form-label">{{__('JsPress::backend.cookie_consent.cookie_consent_modal_content')}}
                                                        <span class="svg-icon svg-icon-muted svg-icon-2hx cursor-pointer" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="<img src='{{asset('jspress/assets/media/cookie/cookie_consent_modal_content.png')}}' height='300' />">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                                                <rect x="11" y="17" width="7" height="2" rx="1" transform="rotate(-90 11 17)" fill="black"/>
                                                                <rect x="11" y="9" width="2" height="2" rx="1" transform="rotate(-90 11 9)" fill="black"/>
                                                            </svg>
                                                        </span>
                                                    </label>
                                                    <textarea name="languages[{{$language->locale}}][consentModal][description]" class="form-control form-control-solid h-100px">{{ $cookie->translations[$language->locale]['consentModal']['description'] ??  __('JsPress::backend.cookie_consent.cookie_consent_modal_content_value')}}</textarea>
                                                    <small class="text-warning pt-3">{{ __('JsPress::backend.cookie_consent.form.content_description') }}</small>
                                                </div>
                                                <div class="mb-10">
                                                    <label class="required form-label">{{__('JsPress::backend.cookie_consent.preferences_modal_title')}}
                                                        <span class="svg-icon svg-icon-muted svg-icon-2hx cursor-pointer" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="<img src='{{asset('jspress/assets/media/cookie/cookie_pereferences_modal_title.png')}}' height='300' />">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                                                <rect x="11" y="17" width="7" height="2" rx="1" transform="rotate(-90 11 17)" fill="black"/>
                                                                <rect x="11" y="9" width="2" height="2" rx="1" transform="rotate(-90 11 9)" fill="black"/>
                                                            </svg>
                                                        </span>
                                                    </label>
                                                    <input type="text" name="languages[{{$language->locale}}][preferencesModal][title]" class="form-control mb-2" placeholder="{{ __('JsPress::backend.type_title') }}" value="{{ $cookie->translations[$language->locale]['preferencesModal']['title'] ??  __('JsPress::backend.cookie_consent.preferences_modal_title_value')}}" autocomplete="off" required/>
                                                    <small class="text-warning">{{ __('JsPress::backend.cookie_consent.preferences_modal_title_description') }}</small>
                                                </div>
                                                <div class="mb-10">
                                                    <label class="required form-label">{{__('JsPress::backend.cookie_consent.preferences_content_title')}}
                                                        <span class="svg-icon svg-icon-muted svg-icon-2hx cursor-pointer" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="<img src='{{asset('jspress/assets/media/cookie/cookie_pereferences_modal_content_title.png')}}' height='300' />">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                                                <rect x="11" y="17" width="7" height="2" rx="1" transform="rotate(-90 11 17)" fill="black"/>
                                                                <rect x="11" y="9" width="2" height="2" rx="1" transform="rotate(-90 11 9)" fill="black"/>
                                                            </svg>
                                                        </span>
                                                    </label>
                                                    <input type="text" name="languages[{{$language->locale}}][preferencesModal][sections][0][title]" class="form-control mb-2" placeholder="{{ $cookie->translations[$language->locale]['preferencesModal']['sections'][0]['title'] ?? __('JsPress::backend.type_title') }}" value="{{ __('JsPress::backend.cookie_consent.preferences_content_title_value') }}" autocomplete="off" required/>
                                                    <small class="text-warning">{{ __('JsPress::backend.cookie_consent.preferences_content_title_description') }}</small>
                                                </div>
                                                <div class="mb-10">
                                                    <label class="required form-label">{{__('JsPress::backend.cookie_consent.preferences_content')}}
                                                        <span class="svg-icon svg-icon-muted svg-icon-2hx cursor-pointer" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-html="true" title="<img src='{{asset('jspress/assets/media/cookie/cookie_pereferences_modal_content.png')}}' height='300' />">
                                                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="black"/>
                                                                <rect x="11" y="17" width="7" height="2" rx="1" transform="rotate(-90 11 17)" fill="black"/>
                                                                <rect x="11" y="9" width="2" height="2" rx="1" transform="rotate(-90 11 9)" fill="black"/>
                                                            </svg>
                                                        </span>
                                                    </label>
                                                    <textarea name="languages[{{$language->locale}}][preferencesModal][sections][0][description]" class="form-control form-control-solid h-100px">{{ $cookie->translations[$language->locale]['preferencesModal']['sections'][0]['description'] ?? __('JsPress::backend.cookie_consent.preferences_content_value')}}</textarea>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 mb-5">
                                                        <label class="required form-label">{{__('JsPress::backend.cookie_consent.accept_all_button')}}</label>
                                                        <input type="text" name="languages[{{$language->locale}}][consentModal][acceptAllBtn]" class="form-control mb-2" autocomplete="off" value="{{ $cookie->translations[$language->locale]['consentModal']['acceptAllBtn'] ?? __('JsPress::backend.cookie_consent.accept_all') }}" required/>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 mb-5">
                                                        <label class="required form-label">{{__('JsPress::backend.cookie_consent.reject_all_button')}}</label>
                                                        <input type="text" name="languages[{{$language->locale}}][consentModal][acceptNecessaryBtn]" class="form-control mb-2" autocomplete="off" value="{{ $cookie->translations[$language->locale]['consentModal']['acceptNecessaryBtn'] ?? __('JsPress::backend.cookie_consent.reject_all') }}" required/>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 mb-5">
                                                        <label class="required form-label">{{__('JsPress::backend.cookie_consent.show_preferences_button')}}</label>
                                                        <input type="text" name="languages[{{$language->locale}}][consentModal][showPreferencesBtn]" class="form-control mb-2" autocomplete="off" value="{{ $cookie->translations[$language->locale]['consentModal']['showPreferencesBtn'] ?? __('JsPress::backend.cookie_consent.show_preferences_button_text') }}" required/>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 mb-5">
                                                        <label class="required form-label">{{__('JsPress::backend.cookie_consent.save_preferences_button')}}</label>
                                                        <input type="text" name="languages[{{$language->locale}}][preferencesModal][savePreferencesBtn]" class="form-control mb-2" autocomplete="off" value="{{  $cokie->translations[$language->locale]['preferencesModal']['savePreferencesBtn'] ?? __('JsPress::backend.cookie_consent.save_preferences_button_text') }}" required/>
                                                    </div>
                                                    <div class="col-lg-12 mb-5">
                                                        <label class="form-label">Footer</label>
                                                        <input type="text" name="languages[{{$language->locale}}][consentModal][footer]" class="form-control mb-2" autocomplete="off" value="{{ $cookie->translations[$language->locale]['consentModal']['footer'] ?? __('JsPress::backend.cookie_consent.footer_message') }}" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card w-100 mb-10">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 class="card-title align-items-start flex-column">
                                                        <span class="card-label fw-bolder fs-3 mb-1">{{ __('JsPress::backend.cookie_consent.necessary_options') }}</span>
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="mb-10">
                                                    <label class="required form-label">{{__('JsPress::backend.cookie_consent.necessary_title')}}</label>
                                                    <input type="text" name="languages[{{$language->locale}}][preferencesModal][sections][1][title]" class="form-control mb-2" placeholder="{{ __('JsPress::backend.type_title') }}" value="{{ $cookie->translations[$language->locale]['preferencesModal']['sections'][1]['title'] ?? __('JsPress::backend.cookie_consent.necessary_title_value')}}" autocomplete="off" required/>
                                                </div>
                                                <div class="mb-10">
                                                    <label class="required form-label">{{__('JsPress::backend.cookie_consent.necessary_content')}}</label>
                                                    <textarea name="languages[{{$language->locale}}][preferencesModal][sections][1][description]" class="form-control form-control-solid h-100px">{{$cookie->translations[$language->locale]['preferencesModal']['sections'][1]['description'] ?? __('JsPress::backend.cookie_consent.necessary_content_value')}}</textarea>
                                                </div>
                                                <input type="hidden" name="languages[{{$language->locale}}][preferencesModal][sections][1][linkedCategory]" value="necessary"/>
                                            </div>
                                        </div>
                                        <div class="card w-100 mb-10 dependsOnCategory d-none" data-categories="2">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 class="card-title align-items-start flex-column">
                                                        <span class="card-label fw-bolder fs-3 mb-1">{{ __('JsPress::backend.cookie_consent.functional_options') }}</span>
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="mb-10">
                                                    <label class="required form-label">{{__('JsPress::backend.cookie_consent.functional_title')}}</label>
                                                    <input type="text" name="languages[{{$language->locale}}][preferencesModal][sections][2][title]" class="form-control mb-2" placeholder="{{ __('JsPress::backend.type_title') }}" value="{{$cookie->translations[$language->locale]['preferencesModal']['sections'][2]['title'] ?? __('JsPress::backend.cookie_consent.functional_title_value')}}" autocomplete="off"/>
                                                </div>
                                                <div class="mb-10">
                                                    <label class="required form-label">{{__('JsPress::backend.cookie_consent.functional_content')}}</label>
                                                    <textarea name="languages[{{$language->locale}}][preferencesModal][sections][2][description]" class="form-control form-control-solid h-100px">{{$cookie->translations[$language->locale]['preferencesModal']['sections'][2]['description'] ?? __('JsPress::backend.cookie_consent.functional_content_value')}}</textarea>
                                                </div>
                                                <input type="hidden" name="languages[{{$language->locale}}][preferencesModal][sections][2][linkedCategory]" value="functionality"/>
                                            </div>
                                        </div>
                                        <div class="card w-100 mb-10 dependsOnCategory d-none" data-categories="3">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 class="card-title align-items-start flex-column">
                                                        <span class="card-label fw-bolder fs-3 mb-1">{{ __('JsPress::backend.cookie_consent.analystic_options') }}</span>
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="mb-10">
                                                    <label class="required form-label">{{__('JsPress::backend.cookie_consent.analystic_title')}}</label>
                                                    <input type="text" name="languages[{{$language->locale}}][preferencesModal][sections][3][title]" class="form-control mb-2" placeholder="{{ __('JsPress::backend.type_title') }}" value="{{ $cookie->translations[$language->locale]['preferencesModal']['sections'][3]['title'] ?? __('JsPress::backend.cookie_consent.analystic_title_value')}}" autocomplete="off"/>
                                                </div>
                                                <div class="mb-10">
                                                    <label class="required form-label">{{__('JsPress::backend.cookie_consent.analystic_content')}}</label>
                                                    <textarea name="languages[{{$language->locale}}][preferencesModal][sections][3][description]" class="form-control form-control-solid h-100px">{{ $cookie->translations[$language->locale]['preferencesModal']['sections'][3]['description'] ?? __('JsPress::backend.cookie_consent.analystic_content_value')}}</textarea>
                                                </div>
                                                <input type="hidden" name="languages[{{$language->locale}}][preferencesModal][sections][3][linkedCategory]" value="analytics"/>
                                            </div>
                                        </div>
                                        <div class="card w-100 mb-10 dependsOnCategory d-none" data-categories="4">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 class="card-title align-items-start flex-column">
                                                        <span class="card-label fw-bolder fs-3 mb-1">{{ __('JsPress::backend.cookie_consent.marketing_options') }}</span>
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="mb-10">
                                                    <label class="required form-label">{{__('JsPress::backend.cookie_consent.marketing_title')}}</label>
                                                    <input type="text" name="languages[{{$language->locale}}][preferencesModal][sections][4][title]" class="form-control mb-2" placeholder="{{ __('JsPress::backend.type_title') }}" value="{{ $cookie->translations[$language->locale]['preferencesModal']['sections'][4]['title'] ?? __('JsPress::backend.cookie_consent.marketing_title_value')}}" autocomplete="off"/>
                                                </div>
                                                <div class="mb-10">
                                                    <label class="required form-label">{{__('JsPress::backend.cookie_consent.marketing_content')}}</label>
                                                    <textarea name="languages[{{$language->locale}}][preferencesModal][sections][4][description]" class="form-control form-control-solid h-100px">{{$cookie->translations[$language->locale]['preferencesModal']['sections'][4]['description'] ?? __('JsPress::backend.cookie_consent.marketing_content_value')}}</textarea>
                                                </div>
                                                <input type="hidden" name="languages[{{$language->locale}}][preferencesModal][sections][4][linkedCategory]" value="marketing"/>
                                            </div>
                                        </div>
                                        <div class="card w-100 mb-10">
                                            <div class="card-header">
                                                <div class="card-title">
                                                    <h3 class="card-title align-items-start flex-column">
                                                        <span class="card-label fw-bolder fs-3 mb-1">{{ __('JsPress::backend.cookie_consent.more_information_options') }}</span>
                                                    </h3>
                                                </div>
                                            </div>
                                            <div class="card-body">
                                                <div class="mb-10">
                                                    <label class="required form-label">{{__('JsPress::backend.cookie_consent.more_information_title')}}</label>
                                                    <input type="text" name="languages[{{$language->locale}}][preferencesModal][sections][5][title]" class="form-control mb-2" placeholder="{{ __('JsPress::backend.type_title') }}" value="{{ $cookie->translations[$language->locale]['preferencesModal']['sections'][5]['title'] ?? __('JsPress::backend.cookie_consent.more_information_title_value')}}" autocomplete="off"/>
                                                </div>
                                                <div class="mb-10">
                                                    <label class="required form-label">{{__('JsPress::backend.cookie_consent.more_information_content')}}</label>
                                                    <textarea name="languages[{{$language->locale}}][preferencesModal][sections][5][description]" class="form-control form-control-solid h-100px">{{$cookie->translations[$language->locale]['preferencesModal']['sections'][5]['description'] ?? __('JsPress::backend.cookie_consent.more_information_content_value')}}</textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="col-lg-4">
                                <div class="card w-100 mb-5">
                                    <div class="card-header py-5">
                                        <div class="card-title">
                                            <h3 class="card-title align-items-start flex-column">
                                                <span class="card-label fw-bolder fs-3 mb-1">{{ __('JsPress::backend.cookie_consent.preview_settings') }}</span>
                                                <span class="text-muted mt-1 fw-bold fs-7">{{ __('JsPress::backend.cookie_consent.preview_settings_description') }}</span>
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="mb-5">
                                            <label class="required form-label">{{ __('JsPress::backend.status') }}</label>
                                            <select class="form-select" name="status">
                                                <option value="1" {{ !is_null($cookie) && $cookie->status == 1 ? 'selected': (is_null($cookie) ? 'selected':'') }}>{{ __('JsPress::backend.active') }}</option>
                                                <option value="0" {{ !is_null($cookie) && $cookie->status == 0 ? 'selected':'' }}>{{ __('JsPress::backend.deactive') }}</option>
                                            </select>
                                        </div>
                                        <div class="mb-5">
                                            <label class="required form-label">{{ __('JsPress::backend.cookie_consent.disable_page_interaction') }}</label>
                                            <select class="form-select" name="disablePageInteraction">
                                                <option value="1" {{ !is_null($cookie) && $cookie->page_interaction == 0 ? 'selected':'' }}>{{ __('JsPress::backend.active') }}</option>
                                                <option value="0" {{ !is_null($cookie) && $cookie->page_interaction == 0 ? 'selected': (is_null($cookie) ? 'selected':'') }}>{{ __('JsPress::backend.deactive') }}</option>
                                            </select>
                                        </div>
                                        <div class="mb-5">
                                            <label class="required form-label">{{ __('JsPress::backend.cookie_consent.cookie_name') }}</label>
                                            <input type="text" name="cookie_name" class="form-control mb-2 valid" autocomplete="off" value="{{ $cookie->cookie_name ?? 'jspress_cookie' }}">
                                            <small class="text-danger">{{ __('JsPress::backend.cookie_consent.cookie_name_description') }}</small>
                                        </div>
                                        <div class="mb-5">
                                            <label class="required form-label">{{ __('JsPress::backend.cookie_consent.theme') }}</label>
                                            <select class="form-select" name="theme">
                                                <option value="default-light" {{ !is_null($cookie) && $cookie->theme == 'default-light' ? 'selected': (is_null($cookie) ? 'selected':'') }}>{{ __('JsPress::backend.cookie_consent.default_theme') }}</option>
                                                <option value="cc--darkmode" {{ !is_null($cookie) && $cookie->theme == 'cc--darkmode' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.dark_theme') }}</option>
                                                <option value="cc--dark-turquoise" {{ !is_null($cookie) && $cookie->theme == 'cc--dark-turquoise' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.dark_turquoise') }}</option>
                                                <option value="cc--light-funky" {{ !is_null($cookie) && $cookie->theme == 'cc--light-funky' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.light_funky') }}</option>
                                                <option value="cc--elegant-black" {{ !is_null($cookie) && $cookie->theme == 'cc--elegant-black' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.elegant_black') }}</option>
                                            </select>
                                        </div>
                                        <div class="w-100">
                                            <button type="submit" class="btn btn-primary">
                                                <span class="indicator-label">{{ __('JsPress::backend.update') }}</span>
                                                <span class="indicator-progress">{{__('JsPress::backend.please_wait')}}
                                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                                </span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div class="card w-100 mb-5">
                                    <div class="card-header py-5">
                                        <div class="card-title">
                                            <h3 class="card-title align-items-start flex-column">
                                                <span class="card-label fw-bolder fs-3 mb-1">{{ __('JsPress::backend.cookie_consent.cookie_consent_settings') }}</span>
                                                <span class="text-muted mt-1 fw-bold fs-7">{{ __('JsPress::backend.cookie_consent.preview_settings_description') }}</span>
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="mb-5">
                                            <label class="required form-label">{{ __('JsPress::backend.cookie_consent.cookie_consent_modal_preview') }}</label>
                                            <select class="form-select" name="consentModal[layout]">
                                                <option value="box" {{ !is_null($cookie) && $cookie->consentModal['layout'] == 'box' ? 'selected' : (is_null($cookie) ? 'selected' : '') }}>{{ __('JsPress::backend.cookie_consent.form.box') }}</option>
                                                <option value="box_inline" {{ !is_null($cookie) && $cookie->consentModal['layout'] == 'box_inline' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.form.box_inline') }}</option>
                                                <option value="box_wide" {{ !is_null($cookie) && $cookie->consentModal['layout'] == 'box_wide' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.form.box_wide') }}</option>
                                                <option value="cloud" {{ !is_null($cookie) && $cookie->consentModal['layout'] == 'cloud' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.form.cloud') }}</option>
                                                <option value="cloud_inline" {{ !is_null($cookie) && $cookie->consentModal['layout'] == 'cloud_inline' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.form.cloud_inline') }}</option>
                                                <option value="bar" {{ !is_null($cookie) && $cookie->consentModal['layout'] == 'bar' ? 'selected':'' }}>Bar</option>
                                                <option value="bar_inline" {{ !is_null($cookie) && $cookie->consentModal['layout'] == 'bar_inline' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.form.bar_inline') }}</option>
                                            </select>
                                        </div>
                                        <div class="mb-5">
                                            <label class="required form-label">{{ __('JsPress::backend.cookie_consent.cookie_consent_modal_position') }}</label>
                                            <select class="form-select" name="consentModal[position]">
                                                @if(!is_null($cookie) && ( $cookie->consentModal['layout'] == 'bar' || $cookie->consentModal['layout'] == 'bar_inline' ) )
                                                    <option value="top" {{ !is_null($cookie) && $cookie->consentModal['position'] == 'top' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.form.top') }}</option>
                                                    <option value="bottom" {{ !is_null($cookie) && $cookie->consentModal['position'] == 'bottom' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.form.bottom') }}</option>
                                                @else
                                                    <option value="top_left" {{ !is_null($cookie) && $cookie->consentModal['position'] == 'top_left' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.form.top_left') }}</option>
                                                    <option value="top_center" {{ !is_null($cookie) && $cookie->consentModal['position'] == 'top_center' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.form.top_center') }}</option>
                                                    <option value="top_right" {{ !is_null($cookie) && $cookie->consentModal['position'] == 'top_right' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.form.top_right') }}</option>
                                                    <option value="middle_left" {{ !is_null($cookie) && $cookie->consentModal['position'] == 'middle_left' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.form.middle_left') }}</option>
                                                    <option value="middle_center" {{ !is_null($cookie) && $cookie->consentModal['position'] == 'middle_center' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.form.middle_center') }}</option>
                                                    <option value="middle_right" {{ !is_null($cookie) && $cookie->consentModal['position'] == 'middle_right' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.form.middle_right') }}</option>
                                                    <option value="bottom_left" {{ !is_null($cookie) && $cookie->consentModal['position'] == 'bottom_left' ? 'selected' : (is_null($cookie) ? 'selected' : '') }}>{{ __('JsPress::backend.cookie_consent.form.bottom_left') }}</option>
                                                    <option value="bottom_center" {{ !is_null($cookie) && $cookie->consentModal['position'] == 'bottom_center' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.form.bottom_center') }}</option>
                                                    <option value="bottom_right" {{ !is_null($cookie) && $cookie->consentModal['position'] == 'bottom_right' ? 'selected':'' }}>{{ __('JsPress::backend.cookie_consent.form.bottom_right') }}</option>
                                                @endif
                                            </select>
                                        </div>
                                        <div class="mb-5">
                                            <div class="d-flex flex-stack w-100">
                                                <div class="me-5">
                                                    <label class="fs-6 fw-bold form-label">{{ __('JsPress::backend.cookie_consent.form.cookie_consent_fip_button_label') }}</label>
                                                    <div class="fs-7 fw-bold text-muted">{{ __('JsPress::backend.cookie_consent.form.cookie_consent_fip_button_description') }}</div>
                                                </div>
                                                <label class="form-check form-switch form-check-custom form-check-solid">
                                                    <input class="form-check-input" name="consentModal[flipButtons]" id="consentModal_flipButtons" type="checkbox" value="1" {{ !is_null($cookie) && $cookie->consentModal['flipButtons'] ? 'checked':'' }}/>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="mb-5">
                                            <div class="d-flex flex-stack w-100">
                                                <div class="me-5">
                                                    <label class="fs-6 fw-bold form-label">{{ __('JsPress::backend.cookie_consent.form.cookie_consent_equal_weight_label') }}</label>
                                                    <div class="fs-7 fw-bold text-muted">{{ __('JsPress::backend.cookie_consent.form.cookie_consent_equal_weight_description') }}</div>
                                                </div>
                                                <label class="form-check form-switch form-check-custom form-check-solid">
                                                    <input class="form-check-input" name="consentModal[equalWeightButtons]" id="consentModal_equalWeightButtons" type="checkbox" value="1" {{ !is_null($cookie) && $cookie->consentModal['equalWeightButtons'] ? 'checked':'' }}/>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card w-100">
                                    <div class="card-header py-5">
                                        <div class="card-title">
                                            <h3 class="card-title align-items-start flex-column">
                                                <span class="card-label fw-bolder fs-3 mb-1">{{ __('JsPress::backend.cookie_consent.cookie_preferences_settings') }}</span>
                                                <span class="text-muted mt-1 fw-bold fs-7">{{ __('JsPress::backend.cookie_consent.cookie_preferences_description') }}</span>
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="mb-5">
                                            <label class="required form-label">{{ __('JsPress::backend.cookie_consent.cookie_consent_preferences_preview') }}</label>
                                            <select class="form-select" name="preferencesModal[layout]">
                                                <option value="box" {{ !is_null($cookie) && $cookie->preferencesModal['layout'] == 'box' ? 'selected' : (is_null($cookie) ? 'selected' : '') }}>{{ __('JsPress::backend.cookie_consent.form.box') }}</option>
                                                <option value="bar" {{ !is_null($cookie) && $cookie->preferencesModal['layout'] == 'bar' ? 'selected' : '' }}>Bar</option>
                                                <option value="bar_wide" {{ !is_null($cookie) && $cookie->preferencesModal['layout'] == 'bar_wide' ? 'selected' : '' }}>{{ __('JsPress::backend.cookie_consent.form.bar_wide') }}</option>
                                            </select>
                                        </div>
                                        <div class="mb-5">
                                            <label class="required form-label">{{ __('JsPress::backend.cookie_consent.cookie_consent_preferences_position') }}</label>
                                            <select class="form-select" name="preferencesModal[position]">
                                                <option value="left" {{ !is_null($cookie) && $cookie->preferencesModal['position'] == 'left' ? 'selected' : '' }}>{{ __('JsPress::backend.cookie_consent.form.left') }}</option>
                                                <option value="right" {{ !is_null($cookie) && $cookie->preferencesModal['position'] == 'right' ? 'selected' : (is_null($cookie) ? 'selected' : '') }}>{{ __('JsPress::backend.cookie_consent.form.right') }}</option>
                                            </select>
                                        </div>
                                        <div class="mb-5">
                                            <div class="d-flex flex-stack w-100">
                                                <div class="me-5">
                                                    <label class="fs-6 fw-bold form-label">{{ __('JsPress::backend.cookie_consent.form.cookie_consent_fip_button_label') }}</label>
                                                    <div class="fs-7 fw-bold text-muted">{{ __('JsPress::backend.cookie_consent.form.cookie_consent_fip_button_description') }}</div>
                                                </div>
                                                <label class="form-check form-switch form-check-custom form-check-solid">
                                                    <input class="form-check-input" name="preferencesModal[flipButtons]" id="preferencesModal_flipButtons" type="checkbox" value="1" {{ !is_null($cookie) && $cookie->preferencesModal['flipButtons'] ? 'checked':'' }}/>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="mb-5">
                                            <div class="d-flex flex-stack w-100">
                                                <div class="me-5">
                                                    <label class="fs-6 fw-bold form-label">{{ __('JsPress::backend.cookie_consent.form.cookie_consent_equal_weight_label') }}</label>
                                                    <div class="fs-7 fw-bold text-muted">{{ __('JsPress::backend.cookie_consent.form.cookie_consent_equal_weight_description') }}</div>
                                                </div>
                                                <label class="form-check form-switch form-check-custom form-check-solid">
                                                    <input class="form-check-input" name="preferencesModal[equalWeightButtons]" id="preferencesModal_equalWeightButtons" type="checkbox" value="1" {{ !is_null($cookie) && $cookie->preferencesModal['equalWeightButtons'] ? 'checked':'' }}/>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script src="{!! asset('jspress/libs/ckeditor/ckeditor.js') !!}"></script>
    <script src="{!! asset('jspress/assets/js/custom/validation/jquery.validate.min.js') !!}"></script>
    @if( \File::exists(public_path('jspress/assets/js/custom/validation/localization/messages_'.JsPressPanel::panelLocale().'.js')) )
        <script src="{!! asset('jspress/assets/js/custom/validation/localization/messages_'.JsPressPanel::panelLocale().'.js') !!}"></script>
    @endif
    <script>
        $(function(){
            var form = $('#create_cookie_form');
            form.validate({
                ignore: []
            });
            document.querySelectorAll('textarea').forEach((item)=>{
                CKEDITOR.replace(item, {
                    height:300,
                    versionCheck: false,
                    skin: window.theme === 'dark' ? 'monodark':'moono-lisa',
                    language: window.lang,
                    filebrowserBrowseUrl: '/js-admin/file-manager',
                    filebrowserImageBrowseUrl: '/js-admin/file-manager?type=image&max_file=1&action=ckfinder',
                    filebrowserUploadUrl: '/js-admin/file-manager/upload',
                    entities: false,
                    entities_latin: false,
                    pasteFromWordPromptCleanup: true,
                    pasteFromWordRemoveFontStyles: true,
                    forcePasteAsPlainText: true,
                    ignoreEmptyParagraph: true,
                    clipboard_handleImages :false,
                    removePlugins:["exportpdf"],
                    toolbar: [
                        {
                            name: 'clipboard',
                            groups: ['clipboard', 'undo'],
                            items: ['PasteFromWord', '-', 'Undo', 'Redo']
                        },
                        {
                            name: 'editing',
                            groups: ['find', 'selection', 'spellchecker'],
                            items: ['Find', 'Replace']
                        },
                        //  { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
                        {
                            name: 'paragraph',
                            groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                            items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'textindent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
                        },
                        {name: 'insert', items: ['Image', 'Table', 'bol', 'SpecialChar', 'Iframe']},
                        {name: 'links', items: ['Link', 'Unlink']},
                        '/',
                        {
                            name: 'basicstyles',
                            groups: ['basicstyles', 'cleanup'],
                            items: ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript', '-', 'RemoveFormat']
                        },
                        {name: 'styles', items: ['Styles', 'Format', 'FontSize']},
                        {name: 'colors', items: ['TextColor', 'BGColor']},
                        {name: 'tools', items: ['Maximize', 'ShowBlocks']},
                        {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', 'kopyala']}

                    ]
                });
            });
            setTimeout(function(){
                setLanguageItems();
                setCategories();
            },200);
            onLoadDocument();

        });

        var onLoadDocument = function()
        {
            document.querySelectorAll('input[name="categories[]"]').forEach((item) => {
                item.addEventListener('change', function(){
                    setCategories();
                });
            });
            document.querySelectorAll('.cookineConsentContent').forEach((item) => {
                item.addEventListener('click', function(e){
                    let locale = item.getAttribute('data-locale');
                    let contentLocale = document.querySelector('.dependsOnLanguage.active').getAttribute('data-locale');
                    if( contentLocale === locale ){
                        item.classList.add('active');
                        return false;
                    }
                    document.querySelectorAll('.cookineConsentContent').forEach((el)=>{
                        el.classList.remove('active');
                    })
                    item.classList.add('active');
                    document.querySelectorAll('.dependsOnLanguage').forEach((item)=>{
                        item.classList.remove('active');
                        item.classList.remove('show');
                    });
                    let contentEl =  document.querySelector('.dependsOnLanguage[data-locale="'+locale+'"]');
                    contentEl.classList.add('active');
                    setTimeout(function(){
                        contentEl.classList.add('show');
                    }, 100);
                });

            });

            document.querySelectorAll('.languageItem').forEach((item) => {
                item.addEventListener('change', function(){
                    const checkedEl = document.querySelectorAll('.languageItem input:checked');
                    if(checkedEl.length < 1){
                        alert('At least one element');
                        item.querySelector('input').checked = true;
                        return false;
                    }
                    setLanguageItems();
                });
            });

            document.querySelector('select[name="consentModal[layout]"]').addEventListener('change', function(e){
                let posEl = document.querySelector('select[name="consentModal[position]"]');
                posEl.innerHTML = '';
                if(e.target.value === 'bar' || e.target.value === 'bar_inline'){
                    let options = [
                        {
                            value:'top',
                            text:'{{ __('JsPress::backend.cookie_consent.form.top') }}'
                        },
                        {
                            value:'bottom',
                            text:'{{ __('JsPress::backend.cookie_consent.form.bottom') }}'
                        }
                    ];

                    for(var o in options){
                        posEl.insertAdjacentHTML('beforeend', '<option value="'+options[o].value+'">'+options[o].text+'</option>')
                    }

                    posEl.value = 'bottom';

                }else{
                    let options = [
                        {
                            value:'top_left',
                            text:'{{ __('JsPress::backend.cookie_consent.form.top_left') }}'
                        },
                        {
                            value:'top_center',
                            text:'{{ __('JsPress::backend.cookie_consent.form.top_center') }}'
                        },
                        {
                            value:'top_right',
                            text:'{{ __('JsPress::backend.cookie_consent.form.top_right') }}'
                        },
                        {
                            value:'middle_left',
                            text:'{{ __('JsPress::backend.cookie_consent.form.middle_left') }}'
                        },
                        {
                            value:'middle_center',
                            text:'{{ __('JsPress::backend.cookie_consent.form.middle_center') }}'
                        },
                        {
                            value:'middle_right',
                            text:'{{ __('JsPress::backend.cookie_consent.form.middle_right') }}'
                        },
                        {
                            value:'bottom_left',
                            text:'{{ __('JsPress::backend.cookie_consent.form.bottom_left') }}'
                        },
                        {
                            value:'bottom_center',
                            text:'{{ __('JsPress::backend.cookie_consent.form.bottom_center') }}'
                        },
                        {
                            value:'bottom_center',
                            text:'{{ __('JsPress::backend.cookie_consent.form.bottom_center') }}'
                        }
                    ];
                    for(var o in options){
                        posEl.insertAdjacentHTML('beforeend', '<option value="'+options[o].value+'">'+options[o].text+'</option>')
                    }
                    posEl.value = 'bottom_left';
                }
            });
        }

        var setCategories = function(){
            var categories = [];
            document.querySelectorAll('input[name="categories[]"]').forEach((item) => {
                if(item.checked){
                    categories.push(item.value);
                }
            });
            toggleCategoriesContent(categories);
        }
        var toggleCategoriesContent = function(categories)
        {
            document.querySelectorAll('.dependsOnCategory').forEach((item)=>{
                item.classList.add('d-none');
            });
            for(var i in categories){
                document.querySelectorAll('.dependsOnCategory[data-categories="'+categories[i]+'"]').forEach((item)=>{
                    item.classList.remove('d-none');
                });
            }
        }
        var setLanguageItems = function()
        {
            document.querySelectorAll('.cookineConsentContent ').forEach((item) =>{
                item.classList.remove('active');
                item.classList.add('d-none');
            });
            document.querySelectorAll('.languageItem input').forEach((el) => {
                if(el.checked){
                    let value = el.value;
                    document.querySelector('.cookineConsentContent[data-locale="'+value+'"]').classList.remove('d-none');
                    Array.from(document.querySelectorAll('.cookineConsentContent:not(.d-none)')).find(item => {
                        item.click();
                        return true;
                    });
                }
            });
        }
    </script>
@endpush
