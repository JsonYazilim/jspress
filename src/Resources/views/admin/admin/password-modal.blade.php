<div class="modal fade" id="kt_modal_update_password" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered mw-650px">
        <div class="modal-content">
            <div class="modal-header">
                <h2 class="fw-bolder">{!! __('JsPress::backend.profile.update_password') !!}</h2>
                <button type="button" class="btn-close btn-close-white" aria-label="Close" data-bs-dismiss="modal"></button>
            </div>
            <div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
                <form id="kt_modal_update_password_form" class="form">
                    <div class="mb-10 fv-row" data-kt-password-meter="true">
                        <div class="mb-1">
                            <label class="form-label fw-bold fs-6 mb-2">{!! __('JsPress::backend.profile.new_password') !!}</label>
                            <div class="position-relative mb-3">
                                <input class="form-control form-control-lg form-control-solid" type="password" placeholder="" name="password" autocomplete="off" />
                                <span class="btn btn-sm btn-icon position-absolute translate-middle top-50 end-0 me-n2" data-kt-password-meter-control="visibility">
                                    <i class="bi bi-eye-slash fs-2"></i>
                                    <i class="bi bi-eye fs-2 d-none"></i>
                                </span>
                            </div>
                            <div class="d-flex align-items-center mb-3" data-kt-password-meter-control="highlight">
                                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px me-2"></div>
                                <div class="flex-grow-1 bg-secondary bg-active-success rounded h-5px"></div>
                            </div>
                        </div>
                        <div class="text-muted">{!! __('JsPress::backend.profile.password_checker') !!}</div>
                    </div>
                    <div class="fv-row mb-10">
                        <label class="form-label fw-bold fs-6 mb-2">{!! __('JsPress::backend.profile.confirm_new_password') !!}</label>
                        <input class="form-control form-control-lg form-control-solid" type="password" placeholder="" name="confirm_password" autocomplete="off" />
                    </div>
                    <div class="text-end">
                        <button type="submit" class="btn btn-primary" data-kt-users-modal-action="submit">
                            <span class="indicator-label">{!! __('JsPress::backend.profile.update_password') !!}</span>
                            <span class="indicator-progress">{!! __('JsPress::backend.form.please_wait') !!}
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                            </span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@push('script')
    <script>
        "use strict";
        var KTUsersUpdatePassword = (function () {
            const t = document.getElementById("kt_modal_update_password"),
                e = t.querySelector("#kt_modal_update_password_form"),
                n = new bootstrap.Modal(t);
            return {
                init: function () {
                    (() => {
                        var o = FormValidation.formValidation(e, {
                            fields: {
                                password: {
                                    validators: {
                                        notEmpty: {
                                            message: "{!! __('JsPress::backend.form.validation.users.required.password') !!}"
                                        },
                                        callback: {
                                            message: "{!! __('JsPress::backend.form.validation.users.valid.password') !!}",
                                            callback: function (t) {
                                                if (t.value.length > 0) return validatePassword();
                                            },
                                        },
                                    },
                                },
                                confirm_password: {
                                    validators: {
                                        notEmpty: {
                                            message: "{!! __('JsPress::backend.form.validation.users.required.confirm_password') !!}"
                                        },
                                        identical: {
                                            compare: function () {
                                                return e.querySelector('[name="password"]').value;
                                            },
                                            message: "{!! __('JsPress::backend.form.validation.users.confirm.password') !!}",
                                        },
                                    },
                                },
                            },
                            plugins: { trigger: new FormValidation.plugins.Trigger(), bootstrap: new FormValidation.plugins.Bootstrap5({ rowSelector: ".fv-row", eleInvalidClass: "", eleValidClass: "" }) },
                        });

                        const a = t.querySelector('[data-kt-users-modal-action="submit"]');
                        a.addEventListener("click", function (t) {
                            t.preventDefault();
                            o &&
                            o.validate().then(function (t) {
                                if( t === "Valid" ){
                                    a.setAttribute("data-kt-indicator", "on");
                                    a.disabled = !0;
                                    var formData = new FormData($('#kt_modal_update_password_form')[0]);
                                    formData.append('admin_id', '{!! $admin->id !!}');
                                    $.ajax({
                                        url: '{!! Route('admin.admin.update.password') !!}',
                                        type:'POST',
                                        data:formData,
                                        processData: false,
                                        contentType: false,
                                        headers:{
                                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                        },
                                        success:function(res){
                                            a.disabled = !1;
                                            a.setAttribute("data-kt-indicator", "off");
                                            if(res.status === 1){
                                                e.reset();
                                                Swal.fire({ text: "{!! __('JsPress::backend.pages.users.edit.update_password_success') !!}", icon: "success", buttonsStyling: !1, confirmButtonText: "{!! __('JsPress::backend.confirmButton') !!}", customClass: { confirmButton: "btn btn-primary" } }).then(
                                                    function (t) {
                                                        t.isConfirmed && n.hide();
                                                    }
                                                );
                                            }else{
                                                Swal.fire({ text: res.msg, icon: "error", buttonsStyling: !1, confirmButtonText: "{!! __('JsPress::backend.confirmButton') !!}", customClass: { confirmButton: "btn btn-primary" } }).then(
                                                    function (t) {

                                                    }
                                                );
                                            }
                                        }
                                    })
                                }
                            });
                        });
                    })();
                },
            };
        })();
        KTUtil.onDOMContentLoaded(function () {
            KTUsersUpdatePassword.init();
        });

    </script>
@endpush
