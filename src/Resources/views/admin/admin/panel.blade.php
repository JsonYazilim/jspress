<div class="card mb-5 mb-xl-10">
    <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
        <div class="card-title m-0">
            <h3 class="fw-bolder m-0">{{__('JsPress::backend.profile.panel_settings')}}</h3>
        </div>
    </div>
    <div id="kt_account_profile_settings" class="collapse show">
        <form id="panelForm" class="form" enctype="multipart/form-data">
            @csrf
            <div class="card-body border-top p-9">
                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label required fw-bold fs-6">{{__('JsPress::backend.profile.panel_language')}}</label>
                    <div class="col-lg-8">
                        <select name="panel_language" aria-label="{{ __('JsPress::backend.profile.choose_a_language') }}" data-control="select2" data-placeholder="{{ __('JsPress::backend.profile.choose_a_language') }}" class="form-select form-select-solid form-select-lg">
                            <option value="">{{ __('JsPress::backend.profile.choose_a_language') }}</option>
                            @foreach( website_languages() as $lang )
                                @php
                                    $display_language = display_language($lang->locale);
                                @endphp
                                <option value="{{$lang->locale}}" {{ $admin->panel_language == $lang->locale ? "selected":"" }}>{{$display_language->display_language_name}}</option>
                            @endforeach
                        </select>
                        <div class="form-text">{{__('JsPress::backend.profile.panel_language_description')}}</div>
                    </div>
                </div>
                <div class="row mb-2">
                    <label class="col-lg-4 col-form-label fw-bold fs-6">{{ __('JsPress::backend.profile.rtl_activation') }}</label>
                    <div class="col-lg-8 d-flex align-items-center">
                        <div class="form-check form-check-solid form-switch fv-row">
                            <input class="form-check-input w-45px h-30px" name="rtl" value="1" type="checkbox" id="rtl" {{ $admin->rtl == 1 ? "checked":"" }}/>
                            <label class="form-check-label" for="rtl"></label>
                        </div>
                    </div>
                </div>
                <div class="row mb-2">
                    <label class="col-lg-4 col-form-label fw-bold fs-6">{{ __('JsPress::backend.profile.panel_dark_theme') }}</label>
                    <div class="col-lg-8 d-flex align-items-center">
                        <div class="form-check form-check-solid form-switch fv-row">
                            <input class="form-check-input w-45px h-30px" name="theme" value="1" type="checkbox" id="theme" {{ $admin->theme == 1 ? "checked":"" }}/>
                            <label class="form-check-label" for="theme"></label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer d-flex justify-content-end py-6 px-9">
                <input type="hidden" name="admin_id" value="{{$admin->id}}"/>
                <button type="submit" class="btn btn-primary">{{__('JsPress::backend.update')}}</button>
            </div>
        </form>
    </div>
</div>
