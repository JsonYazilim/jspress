<div class="card mb-5 mb-xl-10">
    <div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
        <div class="card-title m-0">
            <h3 class="fw-bolder m-0">{{__('JsPress::backend.profile.profile_details')}}</h3>
        </div>
    </div>
    <div id="kt_account_settings_profile_details" class="collapse show">
        <form action="{!! route('admin.admin.update') !!}" method="POST" class="form" enctype="multipart/form-data" autofocus="off" autocomplete="off">
            @csrf
            <div class="card-body border-top p-9">
                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label fw-bold fs-6">{{__('JsPress::backend.profile.profile_image')}}</label>
                    <div class="col-lg-8">
                        <div class="image-input image-input-outline" data-kt-image-input="true" style="background-image: url('{!! asset('jspress/assets/media/svg/avatars/blank.svg') !!}')">
                            <div class="image-input-wrapper w-125px h-125px" style="background-image: url({!! !is_null($admin->avatar) ? asset($admin->avatar) : asset('jspress/assets/media/avatars/blank.png') !!})"></div>
                            <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="{{__('JsPress::backend.profile.upload_image')}}">
                                <i class="bi bi-pencil-fill fs-7"></i>
                                <input type="file" name="avatar" accept=".png, .jpg, .jpeg" />
                            </label>
                        </div>
                        <div class="form-text">{{__('JsPress::backend.profile.allowed_image_types')}}</div>
                    </div>
                </div>
                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label required fw-bold fs-6">{{__('JsPress::backend.profile.username')}}</label>
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-12 fv-row">
                                <input type="text" name="name" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0" placeholder="{{__('JsPress::backend.profile.username')}}" value="{!! $admin->name !!}"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mb-6">
                    <label class="col-lg-4 col-form-label required fw-bold fs-6">{{__('JsPress::backend.profile.email')}}</label>
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-lg-12 fv-row">
                                <input type="text" name="email" class="form-control form-control-lg form-control-solid mb-3 mb-lg-0" placeholder="{{__('JsPress::backend.profile.email')}}" value="{!! $admin->email !!}" readonly/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer d-flex justify-content-end py-6 px-9">
                @if($admin->id != auth()->guard('admin')->user()->id)
                    <input type="hidden" name="admin_id" value="{{$admin->id}}"/>
                @endif
                <button type="submit" class="btn btn-primary" id="kt_account_profile_details_submit">{{__('JsPress::backend.update')}}</button>
            </div>
        </form>
    </div>
</div>
