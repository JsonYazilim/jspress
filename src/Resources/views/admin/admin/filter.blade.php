<div class="menu menu-sub menu-sub-dropdown w-300px w-md-325px" data-kt-menu="true">
    <div class="px-7 py-5">
        <div class="fs-5 text-dark fw-bolder">{!! __('JsPress::backend.filter_options') !!}</div>
    </div>
    <div class="separator border-gray-200"></div>
    <div class="px-7 py-5" data-kt-user-table-filter="form">
        <form action="{!! Route('admin.admin.index') !!}">
            <div class="mb-10">
                <label class="form-label fs-6 fw-bold">{{__('JsPress::backend.admin_role')}}:</label>
                <select class="form-select form-select-solid fw-bolder" name="role" data-kt-select2="true" data-placeholder="{!! __('JsPress::backend.choose_role') !!}" data-allow-clear="true" data-kt-user-table-filter="role" data-hide-search="true">
                    <option value="">{!! __('backend.all') !!}</option>
                    @foreach( $roles as $role )
                        <option value="{!! $role->id !!}" {!! \Request::get('role') == $role->id ? 'selected':'' !!}>{!! $role->name !!}</option>
                    @endforeach
                </select>
            </div>
            <div class="d-flex justify-content-end">
                <button type="button" onClick="location.href='{!! Route('admin.admin.index') !!}'" class="btn btn-light btn-active-light-primary fw-bold me-2 px-6">{!! __('JsPress::backend.clear') !!}</button>
                <button type="submit" class="btn btn-primary fw-bold px-6" data-kt-menu-dismiss="true" data-kt-user-table-filter="filter">{!! __('JsPress::backend.apply') !!}</button>
            </div>
        </form>
    </div>
</div>
