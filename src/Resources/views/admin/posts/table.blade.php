<div class="table-responsive">
    <table class="table align-middle table-row-dashed fs-6 gy-5" data-type="{{$post_type->key}}">
        <thead>
        <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
            <th class="w-10px pe-2">
                <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                    <input name="id" class="form-check-input" type="checkbox" data-kt-check="true" value="1" />
                </div>
            </th>
            @foreach( $post_type->columns as $column )
                <th class="min-w-{{$column['width']}}px">{!! $column['name'] !!}</th>
            @endforeach
            <th class="text-end min-w-70px">İşlemler</th>
        </tr>
        </thead>
        <tbody>
        @foreach($post_data['posts'] as $post)
            <tr>
                <td>
                    <div class="form-check form-check-sm form-check-custom form-check-solid">
                        <input class="form-check-input" type="checkbox" value="{{$post['id']}}" />
                    </div>
                </td>
                @foreach($post_type->columns as $column)
                    @if($column['key'] == 'languages')
                        <td>
                            @foreach($post['languages'] as $language)
                                @if($language['type'] == 'add')
                                    <a href="{{$language['url']}}">
                                        <span class="btn btn-icon btn-sm btn-color-primary btn-active-light-primary border rounded-1 w-25px h-25px">
                                            <span class="svg-icon svg-icon-2">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                    <rect opacity="0.5" x="11" y="18" width="12" height="2" rx="1" transform="rotate(-90 11 18)" fill="black"/>
                                                    <rect x="6" y="11" width="12" height="2" rx="1" fill="black"/>
                                                </svg>
                                            </span>
                                        </span>
                                    </a>
                                @endif
                                @if($language['type'] == 'edit')
                                    <a href="{{$language['url']}}">
                                        <span class="btn btn-icon btn-sm btn-color-warning btn-active-light-warning border rounded-1 w-25px h-25px">
                                            <span class="svg-icon svg-icon-3">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                    <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z" fill="black"/>
                                                    <path d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z" fill="black"/>
                                                    <path d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z" fill="black"/>
                                                </svg>
                                            </span>
                                        </span>
                                    </a>
                                @endif
                                @if($language['type'] == 'need_parent')
                                    <a href="javascript:void(0);">
                                    <span class="btn btn-icon btn-sm btn-color-danger btn-active-light-danger border rounded-1 w-25px h-25px" data-bs-toggle="tooltip" data-bs-placement="top" title="Alt sayfaların çevirileri için öncelikle üst sayfanın diğer dillerini açmalısınız.">
                                        <span class="svg-icon svg-icon-3">
                                            <i class="fas fa-info-circle fs-4"></i>
                                        </span>
                                    </span>
                                    </a>
                                @endif
                            @endforeach
                        </td>
                    @else
                        <td>
                            @if( $column['key'] == 'title' )
                                <div class="d-flex justify-content-start flex-column">
                                    <a href="#" class="text-dark fw-bolder text-hover-primary fs-6 mb-2"> {!! $post[$column['key']] !!}</a>
                                    @if($post['post_id'] != 0)
                                        @php $parent_post = get_the_post($post['post_id']); @endphp
                                        <span class="text-muted fw-bold text-muted d-block fs-7"><span class="badge badge-primary">{{ __('JsPress::backend.parent') }}</span> - {!! $parent_post->title !!}</span>
                                     @endif
                                </div>
                            @else
                                {!! $post[$column['key']] !!}
                            @endif

                        </td>
                    @endif
                @endforeach
                <td class="text-end">
                    @include('JsPress::admin.posts.actions', [ 'id'=>$post['id'], 'post_type' => $post_type, 'table' => $table])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
