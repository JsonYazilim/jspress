@extends('JsPress::admin.layouts.app')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{!! __js('post_type', JsPressPanel::postTypeLangKey($post_type, 'breadcrumb'), __('JsPress::backend.post.title', ['name' => $post_type->name])) !!}</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-fluid">
                <x-jspress-big-alert type="error" :title="__('JsPress::backend.post_not_found_for_language_title')" :message="__('JsPress::backend.post_not_found_for_language_message', ['language' => $display_language->display_language_name])" :button="$button"></x-jspress-big-alert>
            </div>
        </div>
    </div>
@endsection
