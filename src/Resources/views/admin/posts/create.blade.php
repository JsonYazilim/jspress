@extends('JsPress::admin.layouts.app')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend"
                     data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}"
                     class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{ __('JsPress::backend.add_new') }}</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    @if(website_languages()->count() > 0)
                        <div class="d-flex align-items-center gap-2 gap-lg-3">
                            <span class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{__('JsPress::backend.content_language')}}:</span>
                            <div class="d-flex">
                                <img src="{{asset('/jspress/assets/media/flags/svg/'.JsPressPanel::contentLocale().'.svg')}}" class="me-2" width="20"  alt="{{JsPressPanel::contentLocale()}}"/>
                                <span>{{display_language(JsPressPanel::contentLocale())->display_language_name}}</span>
                            </div>
                        </div>
                    @endif
                    @isset($post['url'])
                        <a href="{{$post['url']}}" target="_blank" class="btn btn-sm btn-success">{{ __('JsPress::backend.view') }}</a>
                    @endif
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-fluid">
                <form id="create_post_form" action="{!! route('admin.post.store') !!}" method="POST" class="form" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-lg-9">
                            <div class="mb-10">
                                <label class="required form-label">{{__('JsPress::backend.title')}}</label>
                                <input type="text" name="title" class="form-control mb-2" placeholder="{{ __('JsPress::backend.type_title') }}" value="{{@$post['title']}}" required autocomplete="off"/>
                                <div class="text-muted fs-7">{{__('JsPress::backend.max_title_warning')}}</div>
                                @if(isset($post['id']))
                                <div class="input-group mt-2">
                                    <span class="input-group-text">{{$data['root_url']}}</span>
                                    <input type="text" name="slug" class="form-control" value="{{@$post['slug']}}" autocomplete="off" required/>
                                </div>
                                @endif
                            </div>
                            @include('JsPress::admin.modules.post_fields.index', ['fields' => isset($post_fields['after_title']) ? $post_fields['after_title'] : [] ])
                            @if($data['is_editor'] == 1)
                            <div class="mb-10">
                                <label class="form-label">{{__('JsPress::backend.content')}}</label>
                                <textarea name="content" class="js_editor form-control">{!! @$post['content'] !!}</textarea>
                                <div class="text-muted fs-7">{{ __('JsPress::backend.content_warning') }}</div>
                            </div>
                            @endif
                            @include('JsPress::admin.modules.post_fields.index', ['fields' => isset($post_fields['after_editor']) ? $post_fields['after_editor'] : [] ])
                            @if($data['is_seo'] == 1)
                                @include('JsPress::admin.posts.modules.seo')
                            @endif
                            @include('JsPress::admin.modules.post_fields.index', ['fields' => isset($post_fields['after_seo']) ? $post_fields['after_seo'] : [] ])
                        </div>
                        <div class="col-lg-3">
                            <div class="card card-flush py-4 mb-8">
                                <div class="card-header">
                                    <div class="card-title">
                                        <h2>{{__('JsPress::backend.post_settings')}}</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-0">
                                    @if( !is_null($post_translation) )
                                        <div class="mb-5">
                                            <x-jspress-alert type="info" :title="__('JsPress::backend.info')" :message="__('JsPress::backend.this_is_translation_of_post', ['post'=>$post_translation['title']])"></x-jspress-alert>
                                        </div>
                                    @endif
                                    <div class="mb-5">
                                        <label class="required form-label">{{__('JsPress::backend.status')}}</label>
                                        <select class="form-select mb-2" name="status" data-control="select2" data-hide-search="true" data-placeholder="Durum seçiniz">
                                            <option value="1" {{ @$post['status'] == 1 ? 'selected':'' }}>{{__('JsPress::backend.publish')}}</option>
                                            <option value="2" {{ @$post['status'] == 2 ? 'selected':'' }}>{{__('JsPress::backend.draft')}}</option>
                                            <option value="3" {{ @$post['status'] == 3 ? 'selected':'' }}>{{__('JsPress::backend.scheduled')}}</option>
                                            <option value="0" {{ @$post['status'] == "0" ? 'selected':'' }}>{{__('JsPress::backend.deactive')}}</option>
                                        </select>
                                        <div class="text-muted fs-7">{{__('JsPress::backend.select_post_status')}}</div>
                                        <div class="d-none mt-10">
                                            <label for="kt_ecommerce_add_product_status_datepicker" class="form-label">Select publishing date and time</label>
                                            <input class="form-control" id="kt_ecommerce_add_product_status_datepicker" placeholder="Pick date &amp; time" />
                                        </div>
                                    </div>
                                    @role('Super-Admin')
                                        <div class="mb-5">
                                            <label class="required form-label">{{__('JsPress::backend.author')}}</label>
                                            <select class="form-select mb-2" name="author" data-control="select2" data-hide-search="true" data-placeholder="Durum seçiniz">
                                                @foreach( \JsPress\JsPress\Models\Admin::all() as $admin )
                                                <option value="{{ $admin->id  }}" {{ (isset($post['id']) && $admin->id == $post['author']) || auth()->user()->id == $admin->id ? 'selected':''  }}> {{ $admin->name }} </option>
                                                @endforeach
                                            </select>
                                            <div class="text-muted fs-7">{{__('JsPress::backend.select_post_status')}}</div>
                                            <div class="d-none mt-10">
                                                <label for="kt_ecommerce_add_product_status_datepicker" class="form-label">Select publishing date and time</label>
                                                <input class="form-control" id="kt_ecommerce_add_product_status_datepicker" placeholder="Pick date &amp; time" />
                                            </div>
                                        </div>
                                    @endrole
                                    <div class="mb-5">
                                        <label class="form-label">{{ __('JsPress::backend.publish_date') }}</label>
                                        <input class="jspress-datepicker form-control form-control-solid" name="publish_date" data-format="YYYY-MM-DD HH:mm:ss" data-timepicker="true" placeholder="{{ __('JsPress::backend.select_a_date') }}" value="{{@$post['publish_date']}}" readonly/>
                                    </div>
                                    <div class="mb-5">
                                        <label class="required form-label">{{__('JsPress::backend.order')}}</label>
                                        <input type="number" name="order" class="form-control mb-2" placeholder="Sıra numarası giriniz..." value="{{ $post['order'] ?? 0 }}" required autocomplete="off"/>
                                    </div>
                                    @if($data['key'] === 'page')
                                        <div class="mb-5">
                                            <div class="form-check form-switch form-check-custom form-check-solid pulse pulse-success">
                                                <label class="form-check form-switch form-check-custom form-check-solid pulse pulse-success" for="is_homepage">
                                                    <span class="form-check-label text-gray-600 fs-4 me-2">Anasayfa olarak ayarla</span>
                                                    <input class="form-check-input h-20px w-50px text-end" name="is_homepage" value="1" type="checkbox" id="is_homepage" {{ @$post['is_homepage'] == 1 ? 'checked':'' }}/>
                                                </label>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="card-footer pt-0 pb-1" style="text-align:right">
                                    <input type="hidden" name="post-type" value="{{$data['key']}}" />
                                    @if(request()->input('lang'))
                                        <input type="hidden" name="language" value="{{ request()->input('lang') }}" />
                                    @else
                                        <input type="hidden" name="language" value="{{ JsPressPanel::contentLocale() }}" />
                                    @endif
                                    @if(request()->input('post_id') && $data['key'] != 'page')
                                        <input type="hidden" name="post_id" value="{{ request()->input('post_id') }}" />
                                    @endif
                                    @isset($post['id'])
                                        <input type="hidden" name="id" value="{{ $post['id'] }}" />
                                        @if(request()->input('trid'))
                                            <input type="hidden" name="transaction_id" value="{{ request()->input('trid') }}" />
                                        @else
                                            <input type="hidden" name="transaction_id" value="{{ $post['translation']['transaction_id'] }}" />
                                        @endif
                                        @if(request()->input('source_locale'))
                                            <input type="hidden" name="source_locale" value="{{ request()->input('source_locale') }}" />
                                        @else
                                            <input type="hidden" name="source_locale" value="{{ $post['translation']['source_locale'] }}" />
                                        @endif
                                    @else
                                        @if(request()->input('trid'))
                                            <input type="hidden" name="transaction_id" value="{{ request()->input('trid') }}" />
                                        @endif
                                        @if(request()->input('source_locale'))
                                            <input type="hidden" name="source_locale" value="{{ request()->input('source_locale') }}" />
                                        @endif
                                    @endif
                                    <button type="submit" id="post_button" class="btn {{ isset($post['id']) ? 'btn-success':'btn-primary' }}">
                                        <span class="indicator-label">{{ isset($post['id']) ? $data['key'] == 'page' ? __('JsPress::backend.page_update') : __('JsPress::backend.update') : ($data['key'] == 'page' ? __('JsPress::backend.create_page') : __('JsPress::backend.create_post')) }}</span>
                                        <span class="indicator-progress">{{__('JsPress::backend.please_wait')}}
                                        <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                                    </span>
                                    </button>
                                </div>
                            </div>
                            @include('JsPress::admin.modules.post_fields.index', ['fields' => isset($post_fields['sidebar']) ? $post_fields['sidebar'] : [] ])
                            @includeWhen($data['key'] == 'page', 'JsPress::admin.posts.modules.template')
                            @includeWhen($data['is_category'] == 1, 'JsPress::admin.posts.modules.categories')
                            @includeWhen($data['is_image'] == 1, 'JsPress::admin.posts.modules.image')
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            @include('JsPress::admin.modules.post_fields.index', ['fields' => isset($post_fields['post_bottom']) ? $post_fields['post_bottom'] : [] ])
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        window.post_data = @json($data);
    </script>
    <script src="{!! asset('jspress/libs/ckeditor/ckeditor.js') !!}"></script>
    <script src="{!! asset('jspress/assets/js/custom/validation/jquery.validate.min.js') !!}"></script>
    @if( \File::exists(public_path('jspress/assets/js/custom/validation/localization/messages_'.JsPressPanel::panelLocale().'.js')) )
        <script src="{!! asset('jspress/assets/js/custom/validation/localization/messages_'.JsPressPanel::panelLocale().'.js') !!}"></script>
    @endif
    <script>
        JsPressFunc.__callJsEditor();
        JsPressFunc.completePostSeo('{{$data['key']}}');
        var form = $('#create_post_form');
        form.validate({
            ignore: []
        });
        form.on('submit', function(e){
            e.preventDefault();
            if(!form.valid()){
                return;
            }

            var formData = new FormData(form[0]);

            if(CKEDITOR.instances.content){
                formData.append('content', CKEDITOR.instances.content.getData());
            }
            $.ajax({
                url:'{{Route('admin.post.store')}}',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:formData,
                processData: false,
                contentType: false,
                dataType:'json',
                beforeSend: function() {
                    $('#post_button').attr('data-kt-indicator', 'on').prop('disabled', true);
                },
                success:function(res){
                    if(res.status === 1){
                        Swal.fire({
                            text: res.msg,
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: '{{__('JsPress::backend.ok')}}',
                            customClass: {
                                confirmButton: "btn btn-light"
                            }
                        }).then(function(){
                            window.location.href = res.route;
                        });
                    }else{
                        Swal.fire({
                            text: res.msg,
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: '{{__('JsPress::backend.ok')}}',
                            customClass: {
                                confirmButton: "btn btn-light"
                            }
                        });
                    }
                },
                error:function(err, res){
                    Swal.fire({
                        text: res.msg,
                        icon: "error",
                        buttonsStyling: false,
                        confirmButtonText: '{{__('JsPress::backend.ok')}}',
                        customClass: {
                            confirmButton: "btn btn-light"
                        }
                    });
                },
                complete:function(){
                    $('#post_button').attr('data-kt-indicator', 'off').prop('disabled', false);
                }
            });
            return false;
        });
    </script>
@endpush


