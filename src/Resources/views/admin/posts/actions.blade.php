<a href="javascript:void(0);" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{!! __('JsPress::backend.actions') !!}
    <span class="svg-icon svg-icon-5 m-0">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
            <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
        </svg>
    </span>
</a>
<div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-200px py-4" data-kt-menu="true">
    @if(!is_null($post_type->children))
        @if($table == 'index')
            @foreach($post_type->children as $children)
            <div class="menu-item px-3 text-start">
                <a href="{{Route('admin.post.index', ['post_id' => $id, 'post-type'=>$children->key, 'lang'=>$post_type->locale])}}" class="menu-link px-3">{!! __js('post_type', JsPressPanel::postTypeLangKey($children, 'breadcrumb'), __('JsPress::backend.post.title', ['name' => $children->name])) !!}</a>
            </div>
            @endforeach
        @endif
    @endif
    @if($table == 'index')
        @if( $post_type->is_url == 1 )
        <div class="menu-item px-3 text-start">
            <a href="{{$post['url']}}" target="_blank" class="menu-link px-3">{!! __('JsPress::backend.view') !!}</a>
        </div>
        @endif
        <div class="menu-item px-3 text-start">
            <a href="{{Route('admin.post.edit', ['id' => $id, 'post-type'=>$post_type->key, 'lang'=>$post_type->locale, 'post_id' => request()->input('post_id')])}}" class="menu-link px-3">{!! __('JsPress::backend.edit') !!}</a>
        </div>
        <div class="menu-item px-3 text-start">
            <a href="javascript:void(0);" class="menu-link px-3" onclick="JsPressFunc.destroyPost({{$id}}, '{{ $post_type->key }}')">{!! __('JsPress::backend.delete') !!}</a>
        </div>
    @endif
    @if($table == 'trash')
        <div class="menu-item px-3 text-start">
            <a href="javascript:void(0);" class="menu-link px-3" onclick="JsPressFunc.restorePost({{$id}}, '{{ $post_type->key }}')">{!! __('JsPress::backend.restore') !!}</a>
        </div>
        <div class="menu-item px-3 text-start">
            <a href="javascript:void(0);" class="menu-link px-3" onclick="JsPressFunc.deletePost({{$id}}, '{{ $post_type->key }}', '{{ $post_type->locale }}')">{!! __('JsPress::backend.destroy') !!}</a>
        </div>
    @endif
</div>
