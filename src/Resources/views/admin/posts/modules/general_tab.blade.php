<div class="tab-pane fade" id="content_general_tab" role="tab-panel">
    <div class="d-flex flex-column gap-7 gap-lg-10">
        <div class="card card-flush py-4">
            <div class="card-header">
                <div class="card-title">
                    <h2>Genel Ayarlar</h2>
                </div>
            </div>
            <div class="card-body pt-0">
                <div class="row">
                    @foreach( $modules as $module )
                        <div class="col-sm-12 mb-5">
                            @include('admin.posts.fields.'.$module['type'], ['field'=>$module])
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

</div>
