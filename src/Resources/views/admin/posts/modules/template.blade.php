<div class="card card-flush py-4 mb-5">
    <div class="card-header">
        <div class="card-title">
            <h2>{{ __('JsPress::backend.page_template') }}</h2>
        </div>
    </div>
    <div class="card-body pt-0">
        <label for="kt_ecommerce_add_product_store_template" class="form-label">Sayfa için bir şablon seçiniz</label>
        <select class="form-select mb-2 cursor-pointer" name="view" id="kt_ecommerce_add_product_store_template">
            <option value="" {{isset($post['view']) && is_null($post['view']) ? 'selected':''}}>Varsayılan</option>
            @foreach(JsPressPanel::getTemplates() as $template)
                <option value="{{$template['value']}}" {{ isset($post['view']) && $post['view'] == $template['value'] ? 'selected':'' }}>{{$template['label']}}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="card card-flush py-4">
    <div class="card-header">
        <div class="card-title">
            <h2>{{ __('JsPress::backend.parent_page') }}</h2>
        </div>
    </div>
    <div class="card-body pt-0">
        <label for="kt_ecommerce_add_product_store_template" class="form-label">Sayfa için bir şablon seçiniz</label>
        <select class="form-select mb-2 cursor-pointer" name="post_id" id="kt_ecommerce_add_product_store_template">
            <option value="" {{isset($post['post_id']) && $post['post_id'] == "0" ? 'selected':''}}>Varsayılan</option>
            @foreach(JsPressPanel::getParentPages() as $page)
                <option value="{{$page['value']}}" {{ isset($post['post_id']) && $post['post_id'] == $page['value'] ? 'selected':'' }}>{{$page['label']}}</option>
            @endforeach
        </select>
    </div>
</div>
