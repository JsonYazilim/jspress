@foreach($categories as $category)
    @php
        $category_name = $parent_category_name.' > '. $category['title'];
    @endphp
    <option value="{{$category['id']}}" {{ request()->input('category') == $category['id'] ? 'selected':'' }}>{{ $category_name }}</option>
    @includeWhen(count($category['children']) > 0, 'JsPress::admin.posts.modules.search-category', ['categories'=>$category['children'], 'parent_category_name' => $category_name])
@endforeach
