<div class="card card-flush py-4">
    <div class="card-header">
        <div class="card-title">
            <h2>{{__('JsPress::backend.featured_image')}}</h2>
        </div>
    </div>
    <div class="card-body text-center pt-0">
        <x-jspress-image-upload name="extras[featured_image]" :image="@$post['extras']['featured_image'] != null ? $post['extras']['featured_image']:null" :message="__('JsPress::backend.featured_image_message')"></x-jspress-image-upload>
    </div>
</div>
