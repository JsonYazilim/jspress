@foreach($categories as $category)
    @php
        $category_name = $parent_category_name.' > '. $category['title'];
    @endphp
    <option value="{{$category['id']}}" {{ isset($post['categories']) && in_array($category['id'], $post['categories']) ? 'selected':'' }}>{{ $category_name }}</option>
    @includeWhen(count($category['children']) > 0, 'JsPress::admin.posts.modules.child-category', ['categories'=>$category['children'], 'parent_category_name' => $category_name])
@endforeach
