<div class="card card-flush py-4 mb-8">
    <div class="card-header">
        <div class="card-title">
            <h2>{{__('JsPress::backend.categories')}}</h2>
        </div>
    </div>
    <div class="card-body pt-0">
        <select class="form-select form-select-solid" name="categories[]" data-control="select2" data-allow-clear="true" multiple="multiple" data-placeholder="{{__('JsPress::backend.choose_category')}}">
            <option value="" disabled>{{__('JsPress::backend.choose')}}</option>
            @foreach(get_the_admin_categories(['post_type' => $data['key'], 'deep'=> true]) as $category)
                <option value="{{$category['id']}}" {{ isset($post['categories']) && in_array($category['id'], $post['categories']) ? 'selected':'' }}>{{$category['title']}}</option>
                @includeWhen(count($category['children']) > 0, 'JsPress::admin.posts.modules.child-category', ['categories'=>$category['children'], 'parent_category_name' => $category['title'] ])
            @endforeach
        </select>

        <div class="text-muted fs-7 mb-7">{{ __('JsPress::backend.select_category_for_post') }}</div>
        <a href="javascript:void(0);" class="btn btn-light-primary btn-sm mb-10">
            <span class="svg-icon svg-icon-2">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                    <rect opacity="0.5" x="11" y="18" width="12" height="2" rx="1" transform="rotate(-90 11 18)" fill="black" />
                    <rect x="6" y="11" width="12" height="2" rx="1" fill="black" />
                </svg>
            </span>
            {{__('JsPress::backend.create_new_category')}}
        </a>
    </div>
</div>
