@if(count($fields) > 0)
    @foreach($fields as $key => $field)
        <div class="mb-10">
            <div class="accordion" id="{{ $field['key'] }}">
                <div class="accordion-item">
                    <h2 class="accordion-header" id="{{ $field['key'] }}_header">
                        <button class="accordion-button fs-4 fw-bold collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#{{ $field['key'] }}_body" aria-expanded="false" aria-controls="{{ $field['key'] }}_body">
                            {{ __js('post_field', 'post_field_title_'.$field['key'], $field['title']) }}
                        </button>
                    </h2>
                    <div id="{{ $field['key'] }}_body" class="accordion-collapse collapse" aria-labelledby="{{ $field['key'] }}_header" data-bs-parent="#{{ $field['key'] }}">
                        <div class="accordion-body">
                            <div class="d-flex flex-wrap w-100">
                                @foreach($field['data'] as $data)
                                    <div class="{{ $data['type'] === 'repeater' || is_null($data['width']) ? 'w-100':'w-'.$data['width'] }}">
                                        @if( $data['type'] == 'repeater' )
                                            <h4 class="fs-5">{{__js('post_fields','post_field_'.$data['label'].'_title', $data['label'])}}</h4>
                                        @endif
                                        @if( $data['type'] === 'repeater' )
                                            <x-jspress-post-repeater
                                                :id="$data['key']"
                                                name="extras[{{$data['key']}}]"
                                                :elements="$data['items']"
                                                :values="isset($post['extras'][$data['key']]) ? $post['extras'][$data['key']] : []"
                                            ></x-jspress-post-repeater>
                                        @endif
                                        @if($data['type'] === 'text' || $data['type'] === 'number' || $data['type'] === 'email')
                                            <x-jspress-text
                                                :width="$data['width']"
                                                name="extras[{{$data['key']}}]"
                                                :label="__js('post_fields', 'post_field_label_'.$data['key'], $data['label'])"
                                                :type="$data['type']"
                                                :placeholder="$data['placeholder']"
                                                :required="$data['is_required']"
                                                :classes="$data['classes'].' form-control-sm'"
                                                :id="$data['ids']"
                                                :value="@$post['extras'][$data['key']]"
                                            >
                                            </x-jspress-text>
                                        @endif
                                        @if($data['type'] === 'textarea')
                                            <x-jspress-textarea
                                                :width="$data['width']"
                                                name="extras[{{$data['key']}}]"
                                                :label="__js('post_fields', 'post_field_label_'.$data['key'], $data['label'])"
                                                :type="$data['type']"
                                                :placeholder="$data['placeholder']"
                                                :required="$data['is_required']"
                                                :classes="$data['classes'].' form-control-sm'"
                                                :id="$data['ids']"
                                                :value="@$post['extras'][$data['key']]"
                                            >
                                            </x-jspress-textarea>
                                        @endif
                                        @if($data['type'] === 'selectbox')
                                            <x-jspress-select-box
                                                :width="$data['width']"
                                                name="extras[{{$data['key']}}]{{$data['is_multiple'] ? '[]':''}}"
                                                :label="__js('post_fields', 'post_field_label_'.$data['key'], $data['label'])"
                                                :required="$data['is_required']"
                                                :classes="$data['classes'].' form-control-sm'"
                                                :multiple="$data['is_multiple']"
                                                :id="$data['ids']"
                                                :options="repeater_select_parse($data['options'])"
                                                :selected="@$post['extras'][$data['key']]"
                                            >
                                            </x-jspress-select-box>
                                        @endif
                                        @if($data['type'] === 'checkbox')
                                            <x-jspress-check-box-field
                                                :width="$data['width']"
                                                name="extras[{{$data['key']}}][]"
                                                :label="__js('post_fields', 'post_field_label_'.$data['key'], $data['label'])"
                                                :required="$data['is_required']"
                                                :classes="$data['classes'].' form-control-sm'"
                                                :id="$data['ids']"
                                                :options="repeater_select_parse($data['options'])"
                                                :selected="@$post['extras'][$data['key']]"
                                            >
                                            </x-jspress-check-box-field>
                                        @endif
                                        @if($data['type'] === 'radio')
                                            <x-jspress-radio
                                                :width="$data['width']"
                                                name="extras[{{$data['key']}}]"
                                                :label="__js('post_fields', 'post_field_label_'.$data['key'], $data['label'])"
                                                :required="$data['is_required']"
                                                :classes="$data['classes'].' form-control-sm'"
                                                :id="$data['ids']"
                                                :options="repeater_select_parse($data['options'])"
                                                :selected="@$post['extras'][$data['key']]"
                                            >
                                            </x-jspress-radio>
                                        @endif
                                        @if($data['type'] === 'switch')
                                            <x-jspress-switch-button
                                                :width="$data['width']"
                                                name="extras[{{$data['key']}}]"
                                                :label="__js('post_fields', 'post_field_label_'.$data['key'], $data['label'])"
                                                :required="$data['is_required']"
                                                :classes="$data['classes'].' form-control-sm'"
                                                :id="$data['ids']"
                                                :isopen="$data['is_open']"
                                                :message="$data['message']"
                                                :selected="@$post['extras'][$data['key']]"
                                            >
                                            </x-jspress-switch-button>
                                        @endif
                                        @if($data['type'] === 'image')
                                            <div class="mb-5">
                                                <label class="form-label">{{__js('post_fields', 'post_field_label_'.$data['key'], $data['label'])}}</label>
                                                <x-jspress-file-upload-button
                                                    :id="$data['key']"
                                                    type="image"
                                                    :maxfile="1"
                                                    action="single_file_upload"
                                                    name="extras[{{$data['key']}}]"
                                                    :image="@$post['extras'][$data['key']]"
                                                    :width="null"
                                                ></x-jspress-file-upload-button>
                                            </div>
                                        @endif
                                        @if($data['type'] === 'post_object')
                                            <x-jspress-post-objects
                                                :width="$data['width']"
                                                name="extras[{{$data['key']}}][]"
                                                :label="__js('post_fields', 'post_field_label_'.$data['key'], $data['label'])"
                                                :required="$data['is_required']"
                                                :classes="$data['classes'].' form-control-sm'"
                                                :id="$data['ids']"
                                                :options="get_posts_by_post_types($data['post_objects'])"
                                                :selected="@$post['extras'][$data['key']]"
                                            >
                                            </x-jspress-post-objects>
                                        @endif
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endif
