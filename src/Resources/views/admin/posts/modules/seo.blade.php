<div class="card card-flush py-4 mb-10">
    <div class="card-header">
        <div class="card-title">
            <h2>{{__('JsPress::backend.seo_settings')}}</h2>
        </div>
    </div>
    <div class="card-body pt-0">
        <div class="mb-10">
            <ul class="nav nav-tabs nav-line-tabs nav-line-tabs-2x mb-5 fs-6">
                <li class="nav-item">
                    <a class="nav-link active" data-bs-toggle="tab" href="#seo">{{__('JsPress::backend.seo')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-bs-toggle="tab" href="#social">{{__('JsPress::backend.social')}}</a>
                </li>
            </ul>
        </div>
        <div class="tab-content mb-10" id="seoTab">
            <div class="tab-pane fade show active" id="seo" role="tabpanel">
                <div class="mb-10">
                    <div class="d-flex flex-column">
                        <div class="w-75 border border-gray-200 p-4">
                            <div class="w-100 d-flex mb-3">
                                <div class="google-image-logo">
                                    <img src="{{ setting('favicon') ? the_thumbnail_url(setting('favicon')) : asset(config('jspress.panel.logos.favicon')) }}" style="height:18px;width:18px"/>
                                </div>
                                <div class="google-seo-sub d-flex flex-column">
                                    <span class="fs-7">{{ucfirst(request()->getHost())}}</span>
                                    <span class="fs-8 google-seo-sub-url" data-root="{{$data['root_url']}}">{{$data['root_url']}}@isset($post['slug'])<span>{{$post['slug']}}</span>@endif</span>
                                </div>
                            </div>
                            <div class="w-100 d-flex flex-column">
                                @if(isset($post['seo']['title']) && !is_null($post['seo']['title']))
                                    <h2 class="google">{{$post['seo']['title']}}</h2>
                                @else
                                    <h2 class="google">{{__('JsPress::backend.title_is_here')}}</h2>
                                @endif
                                @if(isset($post['seo']['description']) && !is_null($post['seo']['description']))
                                    <p class="google-description">{{$post['seo']['description']}}</p>
                                @else
                                    <p class="google-description">{{__('JsPress::backend.seo_content_is_here')}}</p>
                                @endif

                            </div>
                        </div>
                    </div>
                </div>
                <div class="mb-10">
                    <label class="form-label">{{__('JsPress::backend.seo_meta_title')}}</label>
                    <input type="text" class="form-control mb-2" id="meta_title" name="seo[title]" value="{{@$post['seo']['title']}}" placeholder="Meta başlığı giriniz..." />
                    <div class="text-muted fs-7">{{ __('JsPress::backend.seo_meta_title_description') }}</div>
                    <progress max="60" value="0" aria-hidden="true" class="orange ProgressBar_Meta_Title"></progress>
                </div>
                <div class="mb-10">
                    <label class="form-label">{{__('JsPress::backend.meta_description')}}</label>
                    <textarea id="meta_description" name="seo[description]" class="form-control" rows="4">{{@$post['seo']['description']}}</textarea>
                    <div class="text-muted fs-7">{{ __('JsPress::backend.meta_description_description') }}</div>
                    <progress max="160" value="0" aria-hidden="true" class="orange ProgressBar_Meta_Desc"></progress>
                </div>
            </div>
            <div class="tab-pane fade" id="social" role="tabpanel">
                <div class="mb-10">
                    <label class="form-label">{{__('JsPress::backend.facebook_image')}}</label>
                    <x-jspress-file-upload-button id="facebookMetaImage" type="image" :maxfile="1" action="single_file_upload" name="seo[facebook][image]" :image="@$post['seo']['facebook']['image'] != null ? $post['seo']['facebook']['image']:null"></x-jspress-file-upload-button>
                </div>
                <div class="mb-10">
                    <label class="form-label">{{__('JsPress::backend.facebook_title')}}</label>
                    <input type="text" class="form-control mb-2" name="seo[facebook][title]" value="{{@$post['seo']['facebook']['title']}}" placeholder="Meta başlığı giriniz..." />
                </div>
                <div class="mb-10">
                    <label class="form-label">{{ __('JsPress::backend.facebook_description') }}</label>
                    <textarea name="seo[facebook][description]" class="form-control" rows="4">{{@$post['seo']['facebook']['description']}}</textarea>
                </div>
                <div class="separator separator-dotted border-gray-200 my-10"></div>
                <div class="mb-10">
                    <label class="form-label">{{__('JsPress::backend.twitter_image')}}</label>
                    <x-jspress-file-upload-button id="facebookMetaImage" type="image" :maxfile="1" action="single_file_upload" name="seo[twitter][image]" :image="@$post['seo']['twitter']['image'] != null ? $post['seo']['twitter']['image']:null"></x-jspress-file-upload-button>
                </div>
                <div class="mb-10">
                    <label class="form-label">{{ __('JsPress::backend.twitter_title') }}</label>
                    <input type="text" class="form-control mb-2" name="seo[twitter][title]" value="{{@$post['seo']['twitter']['title']}}" placeholder="Meta başlığı giriniz..." />
                </div>
                <div class="mb-10">
                    <label class="form-label">{{ __('JsPress::backend.twitter_description') }}</label>
                    <textarea name="seo[twitter][description]" class="form-control" rows="4">{{@$post['seo']['twitter']['description']}}</textarea>
                </div>
            </div>
        </div>
    </div>
</div>
@push('script')
    <script>
        let meta_title = document.getElementById('meta_title');
        let meta_description = document.getElementById('meta_description');
        let meta_title_progress = document.querySelector('.ProgressBar_Meta_Title');
        let meta_description_progress = document.querySelector('.ProgressBar_Meta_Desc');
        meta_title.addEventListener('keyup', function(e){
            var l = this.value.toString().length;
            meta_title_progress.value = l;
            if(l > 60){
                meta_title_progress.classList.remove('orange');
                meta_title_progress.classList.remove('green');
                meta_title_progress.classList.add('red');
            }else if( l >= 50){
                meta_title_progress.classList.remove('red');
                meta_title_progress.classList.remove('orange');
                meta_title_progress.classList.add('green');
            }else{
                meta_title_progress.classList.remove('red');
                meta_title_progress.classList.remove('green');
                meta_title_progress.classList.add('orange');
            }
        });
        meta_description.addEventListener('keyup', function(e){
            var l = this.value.toString().length;
            meta_description_progress.value = l;
            if(l > 160){
                meta_description_progress.classList.remove('orange');
                meta_description_progress.classList.remove('green');
                meta_description_progress.classList.add('red');
            }else if(l >= 140){
                meta_description_progress.classList.remove('red');
                meta_description_progress.classList.remove('orange');
                meta_description_progress.classList.add('green');
            }else{
                meta_description_progress.classList.remove('red');
                meta_description_progress.classList.remove('green');
                meta_description_progress.classList.add('orange');
            }
        });
    </script>
@endpush
