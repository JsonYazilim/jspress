<div class="form-check form-switch form-check-custom form-check-solid pulse pulse-success">
    <label class="form-check form-switch form-check-custom form-check-solid pulse pulse-success" for="{!! $field['name'] !!}">
        <input class="form-check-input h-20px w-30px" name="extras[{!! $field['name'] !!}]" type="hidden" value="0"/>
        <input class="form-check-input h-20px w-30px" name="extras[{!! $field['name'] !!}]" value="{!! $field['value'] !!}" type="checkbox" id="{!! $field['name'] !!}" {!! @$extras[$field['name']] ? 'checked':'' !!}/>
        <span class="pulse-ring ms-n1"></span>
        <span class="form-check-label text-gray-600 fs-5">{!! $field['label'] !!}</span>
    </label>
</div>
