<div class="mb-10">
    <label for="{!! $field['label'] !!}" class="required form-label">{!! $field['label'] !!}</label>
    <input type="text" name="extras[{!! $field['name'] !!}]" id="{!! $field['label'] !!}" class="form-control flatpicker" placeholder="{!! $field['description'] !!}" value='{!! @$extras[$field['name']] !!}'/>
</div>
@push('script')
    <script src="https://npmcdn.com/flatpickr/dist/l10n/tr.js"></script>
    <script>
        $(".flatpicker").flatpickr({
            enableTime: true,
            dateFormat: "Y-m-d H:i",
            locale: "tr"
        });
    </script>
@endpush
