<div class="mb-10">
    <label for="{!! $field['label'] !!}" class="required form-label">{!! $field['label'] !!}</label>
    <input type="text" name="extras[{!! $field['name'] !!}]" id="{!! $field['label'] !!}" class="form-control" placeholder="{!! $field['description'] !!}" value='{!! @$extras[$field['name']] !!}'/>
</div>
