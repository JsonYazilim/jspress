@extends('JsPress::admin.layouts.app')

@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{__('JsPress::backend.version_control_system')}}</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                @if(session('success'))
                    @include('JsPress::admin.layouts.success-modal', ['message'=>session('success')])
                @endif
                <div class="card mb-5 mb-xl-8">
                    <div class="card-header border-0 pt-5">
                        <h3 class="card-title align-items-start flex-column">
                            <span class="card-label fw-bolder fs-3 mb-1">Commits</span>
                            <span class="text-muted mt-1 fw-bold fs-7">{{__('JsPress::backend.vcs_table_description')}}</span>
                        </h3>
                    </div>
                    <div class="card-body py-3">
                        <div class="table-responsive">
                            <table class="table align-middle gs-0 gy-4">
                                <thead>
                                <tr class="fw-bolder text-muted bg-light">
                                    <th class="ps-4 min-w-300px rounded-start">{{ __('JsPress::backend.commit_owner') }}</th>
                                    <th class="min-w-125px">Commit</th>
                                    <th class="min-w-125px">{{ __('JsPress::backend.commit_message') }}</th>
                                    <th class="min-w-200px">{{ __('JsPress::backend.commit_date') }}</th>
                                    <th class="min-w-200px text-end rounded-end"></th>
                                </tr>
                                </thead>
                                <tbody>
                                    @foreach($commits as $commit)
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="symbol symbol-circle symbol-50px overflow-hidden me-3">
                                                    <a href="javascript:void(0);">
                                                        <div class="symbol-label">
                                                            <img src="{!! get_gravatar($commit['author']['email']) !!}" alt="{!! $commit['author']['name'] !!}" class="w-100">
                                                        </div>
                                                    </a>
                                                </div>
                                                <div class="d-flex flex-column">
                                                    <a href="javascript:void(0)" class="text-gray-800 text-hover-primary mb-1">{!! $commit['author']['name'] !!}</a>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <a target="_blank" href="{!! $remote.'/commits/'.$commit['commit'] !!}">{!! mb_substr($commit['commit'],0,8) !!}</a>
                                        </td>
                                        <td>
                                            {!! \Str::limit($commit['message'],30) !!}
                                        </td>
                                        <td>
                                            <span title="{!! $commit['date'] !!}">{!! $commit['date']->diffForHumans() !!}</span>
                                        </td>
                                        <td class="text-end">
                                            <a href="{!! route('admin.vcs.merge',['commitId'=>$commit['commit'],'branch'=>str_replace('origin/','',$getOrigin)]) !!}" class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4 me-2">Pull and Merge</a>
                                            <a href="{!! $remote.'/branches/compare/'.$commit['commit'].'..'.$lastCommit['commit'].'#diff' !!}" class="btn btn-bg-light btn-color-muted btn-active-color-primary btn-sm px-4">Compare</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
