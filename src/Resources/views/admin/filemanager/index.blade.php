<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" >
<head>
    <title>{!! __('JsPress::backend.meta.title') !!}</title>
    <meta charset="utf-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="shortcut icon" href="{!! asset('jspress/assets/media/logos/json.png') !!}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    @if( $jspress['theme'] === 'rtl' )
        <link href="{!! asset('jspress/assets/plugins/global/plugins.bundle.rtl.css') !!}" rel="stylesheet" type="text/css" />
        <link href="{!! asset('jspress/assets/css/style.bundle.rtl.css') !!}" rel="stylesheet" type="text/css" />
    @else
        @if( $jspress['theme'] == 'dark' )
            <link href="{!! asset('jspress/assets/plugins/global/plugins.dark.bundle.css') !!}" rel="stylesheet" type="text/css" />
            <link href="{!! asset('jspress/assets/css/style.dark.bundle.css') !!}" rel="stylesheet" type="text/css" />
        @else
            <link href="{!! asset('jspress/assets/plugins/global/plugins.bundle.css') !!}" rel="stylesheet" type="text/css" />
            <link href="{!! asset('jspress/assets/css/style.bundle.css') !!}" rel="stylesheet" type="text/css" />
        @endif
    @endif
    <link href="{!! asset('jspress/assets/css/filemanager.css') !!}" rel="stylesheet" type="text/css" />
</head>

<body id="kt_body" style="--kt-toolbar-height:55px;--kt-toolbar-height-tablet-and-mobile:55px">
<div id="content" class="container w-100">
    <div class="d-flex justify-content-center w-100 mt-10">
        <div class="card shadow-sm w-100 mb-30">
            <div class="card-body w-100">
                <ul class="nav nav-tabs nav-line-tabs mb-5 fs-4 justify-content-center">
                    <li class="nav-item">
                        <a class="nav-link active" data-bs-toggle="tab" href="#upload_file">{{__('JsPress::backend.upload_file')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="filesTabLink" data-bs-toggle="tab" data-bs-target="#files" href="#files">{{__('JsPress::backend.media_library')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-bs-toggle="tab" href="#upload_from_url">{{__('JsPress::backend.upload_from_url')}}</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade w-100 show active" id="upload_file" role="tabpanel">
                        <div class="row mt-10">
                            <div class="col-12 text-center">
                                <div class="d-flex  align-items-center justify-content-center flex-column upload-ui pt-10 pb-10 border border-4 border-dashed border-active-danger min-h-450px">
                                    <h2 class="upload-instructions drop-instructions">{{__('JsPress::backend.drag_and_drop_files')}}</h2>
                                    <p class="upload-instructions drop-instructions">{{ __('JsPress::backend.or') }}</p>
                                    <div class="d-flex flex-center">
                                        <a href="javascript:void(0);" class="btn btn-flex btn-primary px-6" onclick="document.getElementById('file').click();">
                                            <span class="svg-icon svg-icon-2x">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                    <path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM16 13.5L12.5 13V10C12.5 9.4 12.6 9.5 12 9.5C11.4 9.5 11.5 9.4 11.5 10L11 13L8 13.5C7.4 13.5 7 13.4 7 14C7 14.6 7.4 14.5 8 14.5H11V18C11 18.6 11.4 19 12 19C12.6 19 12.5 18.6 12.5 18V14.5L16 14C16.6 14 17 14.6 17 14C17 13.4 16.6 13.5 16 13.5Z" fill="black"/>
                                                    <rect x="11" y="19" width="10" height="2" rx="1" transform="rotate(-90 11 19)" fill="black"/>
                                                    <rect x="7" y="13" width="10" height="2" rx="1" fill="black"/>
                                                    <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="black"/>
                                                </svg>
                                            </span>
                                            <span class="d-flex flex-column align-items-start ms-2">
                                                <span class="fs-3 fw-bolder">{{ __('JsPress::backend.choose_file') }}</span>
                                                <span class="fs-7">{{__('JsPress::backend.max_upload_file_size')}} {{ convert_max_file_size_to_string() }}</span>
                                            </span>
                                        </a>
                                        @php
                                            $accept = '';
                                            if(request()->input('type') == 'image'){
                                                $accept = 'image/*';
                                            }
                                            if(request()->input('type') == 'application'){
                                                $accept = 'application/*';
                                            }
                                        @endphp
                                        <input id="file" type="file" class="form-control mw-250px" style="display:none;" accept="{{$accept}}" multiple/>
                                    </div>
                                    <div id="imageList" class="d-flex align-items-center flex-wrap mt-10"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade w-100" id="files" role="tabpanel">
                        <div id="searchFileManager">
                            <div class="row pb-3 mb-3 border-bottom">
                                <div class="col-lg-2 col-6 pb-2">
                                    <label class="fs-6 form-label text-dark">{{__('JsPress::backend.filename')}}</label>
                                    <input type="text" name="key" class="form-control form-control-solid form-control-sm" placeholder="{{__('JsPress::backend.type_filename')}}"/>
                                </div>
                                <div class="col-lg-2 col-6 pb-2">
                                    <label class="fs-6 form-label text-dark">{{__('JsPress::backend.file_type')}}</label>
                                    <select class="form-control form-select-solid form-control-sm" name="file_type">
                                        <option value="" selected>{{__('JsPress::backend.all')}}</option>
                                        <option value="image">{{__('JsPress::backend.image')}}</option>
                                        <option value="video">{{__('JsPress::backend.video')}}</option>
                                        <option value="audio">{{__('JsPress::backend.audio')}}</option>
                                        <option value="application">{{__('JsPress::backend.file')}}</option>
                                    </select>
                                </div>
                                <div class="col-lg-2 col-6 pb-2">
                                    <label class="fs-6 form-label text-dark">{{__('JsPress::backend.sort')}}</label>
                                    <select class="form-control form-select-solid form-control-sm" name="order">
                                        <option value="created_at-desc" selected>{{__('JsPress::backend.date_asc')}}</option>
                                        <option value="created_at-asc">{{__('JsPress::backend.date_desc')}}</option>
                                        <option value="file_name-asc">{{__('JsPress::backend.filename_asc')}}</option>
                                        <option value="file_name-desc">{{__('JsPress::backend.filename_desc')}}</option>
                                        <option value="size-asc">{{__('JsPress::backend.file_size_asc')}}</option>
                                        <option value="size-desc">{{__('JsPress::backend.file_size_desc')}}</option>
                                    </select>
                                </div>
                                <div class="col-lg-2 col-6 pb-2">
                                    <label class="fs-6 form-label text-dark">{{__('JsPress::backend.start_date')}}</label>
                                    <input class="form-control form-control-solid form-control-sm" name="start_date" placeholder="{{__('JsPress::backend.choose')}}" id="start_date" autocomplete="off" autofocus="off"/>
                                </div>
                                <div class="col-lg-2 col-6 pb-2">
                                    <label class="fs-6 form-label text-dark">{{__('JsPress::backend.end_date')}}</label>
                                    <input class="form-control form-control-solid form-control-sm" name="end_date" placeholder="{{__('JsPress::backend.choose')}}" id="end_date" autocomplete="off" autofocus="off"/>
                                </div>
                                <div class="col-lg-2 col-6">
                                    <button type="button" class="btn btn-sm btn-primary mt-8 me-2" onclick="JsPressFunc.searchFileManager()">{{__('JsPress::backend.search')}}</button>
                                    <button type="button" class="btn btn-sm btn-danger mt-8" onclick="JsPressFunc.resetFileManagerFilter()">{{__('JsPress::backend.clear')}}</button>
                                </div>
                            </div>
                        </div>
                        <div id="fileManagerContainer" class="row min-h-300px"></div>
                        <div id="pagination"></div>
                        <div id="fileManagerFooter" class="position-fixed bg-white w-100 pt-5 d-none">
                            <div class="container">
                                <div class="row">
                                    <div class="col-lg-9 col-md-8 col-sm-6 col-12">
                                        <div class="d-flex w-100 align-items-center pt-5 pb-5">
                                            <div class="selectedFileCount"><span id="fileCount">3</span> {{__('JsPress::backend.file_choosed')}}.</div>
                                            <div id="selectedFiles"></div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-4 col-sm-6 col-12 text-end" style="justify-content: flex-end;align-items: center;display: flex">
                                        <button class="btn btn-outline btn-outline-primary btn-active-primary" onclick="JsPressFunc.applySelectedFile()">{{__('JsPress::backend.add_file_to_page')}}</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade w-100" id="upload_from_url" role="tabpanel">
                        Sint sit mollit irure quis est nostrud cillum consequat Lorem esse do quis dolor esse fugiat sunt do.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('JsPress::admin.media.modal')
<script src="{!! asset('jspress/assets/plugins/global/plugins.bundle.js') !!}"></script>
<script src="{!! asset('jspress/assets/js/scripts.bundle.js') !!}"></script>
<script src="{!! asset('jspress/assets/js/app.js') !!}"></script>
<script>
    var dateTrans = @json(__('JsPress::backend.datepicker'));
    window._dayShortNames = [dateTrans.days.sun,dateTrans.days.mon,dateTrans.days.tue,dateTrans.days.wed,dateTrans.days.thu,dateTrans.days.fri,dateTrans.days.sat];
    window._monthNames = [dateTrans.months.jan,dateTrans.months.feb,dateTrans.months.march,dateTrans.months.april,dateTrans.months.may,dateTrans.months.june, dateTrans.months.july, dateTrans.months.agu, dateTrans.months.sep, dateTrans.months.oct, dateTrans.months.nov, dateTrans.months.dec];
    $("#start_date, #end_date").daterangepicker({
        singleDatePicker: true,
        showDropdowns: true,
        minYear: 2010,
        maxYear: parseInt(moment().format("YYYY"),10),
        autoUpdateInput: false,
        locale: {
            format: 'YYYY-MM-DD',
            "daysOfWeek": window._dayShortNames,
            "monthNames": window._monthNames,
        },
        autoApply:true
    }).on("apply.daterangepicker", function (e, picker) {
        picker.element.val(picker.startDate.format(picker.locale.format));
    });
    const trans = {
        original_url:'{{ __('JsPress::backend.file_href') }}',
        file_name:'{{ __('JsPress::backend.filename') }}',
        upload_name:'{{ __('JsPress::backend.file_upload_name') }}',
        mime:'{{ __('JsPress::backend.file_mime_type') }}',
        type:'{{ __('JsPress::backend.file_type') }}',
        width:'{{ __('JsPress::backend.width') }}',
        height:'{{ __('JsPress::backend.height') }}',
        size:'{{ __('JsPress::backend.file_size') }}',
        tag:'{{ __('JsPress::backend.alt_tag') }}',
        title: '{{ __('JsPress::backend.title_tag') }}',
        caption: '{{ __('JsPress::backend.caption') }}',
        save:'{{ __('JsPress::backend.save') }}',
        delete_image:'{{ __('JsPress::backend.delete_media_file') }}',
        delete_image_warning:'{{ __('JsPress::backend.delete_image_warning') }}',
        confirm:"{!! __('JsPress::backend.yes_i_confirm') !!}",
        no_return:"{!! __('JsPress::backend.no_return') !!}",
        update_message:"{!! __('JsPress::backend.update_message') !!}",
        confirm_button:"{!! __('JsPress::backend.confirmButton') !!}",
        key_is_not_valid:"{{__('JsPress::backend.post_type.key_is_not_valid')}}",
        is_required_message:"{{__('JsPress::backend.this_field_is_required')}}",
        ok:"{{__('JsPress::backend.ok')}}",
        datatable:@json(__('JsPress::backend.datatable')),
        max_upload_size_error: '{{__('JsPress::backend.max_upload_size_error')}}',
        file_size_error:'{{__('JsPress::backend.file_type_error')}}',
        not_found_result:'{{__('JsPress::backend.no_result_found')}}'
    };
    window._selected = [];
    window._data = @json(request()->input());
    window._type = '{{ request()->input('type') }}';
    window._action = '{{request()->input('action')}}';
    window._max_file = {{ request()->input('max_file') }};
    window._ckeditorFunc = '{{request()->input('CKEditorFuncNum')}}';
    window._source = '{{request()->input('source') ?? ""}}'
    window._max_file_error = '{{ __('JsPress::backend.max_file_error', ['count' => request()->input('max_file')]) }}';
    window._confirm_button = '{{ __('JsPress::backend.confirmButton') }}';
    JsPressFunc.__callFileManager(1, null, window._data);
    $('#file').on('change', function(e){
        e.preventDefault();
        let file = document.getElementById('file');
        let file_list= file.files;
        JsPressFunc.MediaFileUpload(file_list, 'media_library');
    });
    $(window).on('dragenter', function(e) {
        e.stopPropagation();
    }).on('dragover', function(e) {
        e.preventDefault();
    }).on('drop', function (e) {
        e.preventDefault();
        let file_list= e.originalEvent.dataTransfer.files;
        JsPressFunc.MediaFileUpload(file_list, 'media_library');
    }).on('resize', function() {

    });
</script>
</body>
</html>
