<!DOCTYPE html>
<html lang="tr">
<head>
    <title>{!! __('JsPress::backend.auth.meta.title') !!}</title>
    <meta name="robots" content="noindex">
    <link rel="shortcut icon" href="{!! asset('jspress/assets/media/logos/json.png') !!}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="{!! asset('jspress/assets/plugins/global/plugins.bundle.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! asset('jspress/assets/css/style.bundle.css') !!}" rel="stylesheet" type="text/css" />
</head>
<body id="kt_body" class="bg-body">
<div class="d-flex flex-column flex-root">
    <div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed" style="background-image: url({!! asset('admin/media/illustrations/sketchy-1/14.png') !!})">
        <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
            <a href="javascript:void(0);" class="mb-12">
                <img alt="Logo" src="{!! asset(config('jspress.panel.logos.logo')) !!}" class="h-40px" />
            </a>
            <div class="w-lg-500px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                <form class="form w-100" novalidate="novalidate" action="{{ route('admin.login.attempt') }}" method="POST">
                    @csrf
                    <div class="text-center mb-10">
                        <h1 class="text-dark mb-3">{!! config('jspress.panel.login_title') !!}</h1>
                        <div class="text-gray-400 fw-bold fs-4">
                            <p>{!! __('JsPress::backend.auth.page.text') !!}</p>
                        </div>
                    </div>
                    <div class="fv-row mb-10">
                        <label class="form-label fs-6 fw-bolder text-dark">{!! __('JsPress::backend.auth.page.email') !!}</label>
                        <input class="form-control form-control-lg form-control-solid @if($errors->login->first('email')) is-invalid @endif" id="email" type="email" name="email" value="{{ old('email') }}" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"  autocomplete="email" autofocus />
                        @if($errors->login->first('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->login->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="fv-row mb-10">
                        <div class="d-flex flex-stack mb-2">
                            <label class="form-label fw-bolder text-dark fs-6 mb-0">{!! __('JsPress::backend.auth.page.password') !!}</label>
                        </div>
                        <input class="form-control form-control-lg form-control-solid @if($errors->login->first('password')) is-invalid @endif" id="password" type="password" name="password" autocomplete="current-password" />
                        @if($errors->login->first('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->login->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="fv-row mb-10 fv-plugins-icon-container">
                        <div class="form-check form-check-custom form-check-solid form-check-inline">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" value="1" {{ old('remember') ? 'checked' : '' }}>
                            <label for="remember" class="form-check-label fw-bold text-gray-700 fs-6">{!! __('JsPress::backend.auth.page.remember_me') !!}</label>
                        </div>
                    </div>
                    <div class="text-center">
                        <button type="submit" id="kt_sign_in_submit" class="btn btn-lg btn-primary w-100 mb-5">
                            <span class="indicator-label">{!! __('JsPress::backend.auth.page.login') !!}</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{!! asset('jspress/assets/plugins/global/plugins.bundle.js') !!}"></script>
<script src="{!! asset('jspress/assets/js/scripts.bundle.js') !!}"></script>
</body>
</html>
