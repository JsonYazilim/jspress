<!DOCTYPE html>
<html lang="tr">
<head>
    <title>{!! __('JsPress::backend.auth.meta.title') !!}</title>
    <meta name="robots" content="noindex">
    <link rel="shortcut icon" href="{!! asset('jspress/assets/media/logos/json.png') !!}" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
    <link href="{!! asset('jspress/assets/plugins/global/plugins.bundle.css') !!}" rel="stylesheet" type="text/css" />
    <link href="{!! asset('jspress/assets/css/style.bundle.css') !!}" rel="stylesheet" type="text/css" />
</head>
<body id="kt_body" class="bg-body">
<div class="d-flex flex-column flex-root">
    <div class="d-flex flex-column flex-column-fluid bgi-position-y-bottom position-x-center bgi-no-repeat bgi-size-contain bgi-attachment-fixed" style="background-image: url({!! asset('admin/media/illustrations/sketchy-1/14.png') !!})">
        <div class="d-flex flex-center flex-column flex-column-fluid p-10 pb-lg-20">
            <a href="javascript:void(0);" class="mb-12">
                <img alt="Logo" src="{!! asset(config('jspress.panel.logos.logo')) !!}" class="h-40px" />
            </a>
            <div class="w-lg-600px bg-body rounded shadow-sm p-10 p-lg-15 mx-auto">
                <form id="verification_form" action="{{Route('admin.verify_account')}}"  method="POST" class="form w-100 fv-plugins-bootstrap5 fv-plugins-framework" novalidate="novalidate">
                    @csrf
                    <div class="text-center mb-10">
                        <h1 class="text-dark mb-3">{{ __('JsPress::backend.verify_your_login') }}</h1>
                        <div class="text-gray-400 fw-bold fs-4">{{__('JsPress::backend.enter_your_verification_code')}}</div>
                    </div>
                    <div class="mb-10 px-md-10">
                        <div class="fw-bolder text-start text-dark fs-6 mb-1 ms-1">{{__('JsPress::backend.your_x_digits_number', ['digit' => 6])}}</div>
                        <div class="d-flex flex-wrap flex-stack">
                            <input type="text" data-inputmask="'mask': '9', 'placeholder': ''" maxlength="1" class="form-control form-control-solid h-60px w-60px fs-2qx text-center border-primary border-hover mx-1 my-2 otp-input" />
                            <input type="text" data-inputmask="'mask': '9', 'placeholder': ''" maxlength="1" class="form-control form-control-solid h-60px w-60px fs-2qx text-center border-primary border-hover mx-1 my-2 otp-input" />
                            <input type="text" data-inputmask="'mask': '9', 'placeholder': ''" maxlength="1" class="form-control form-control-solid h-60px w-60px fs-2qx text-center border-primary border-hover mx-1 my-2 otp-input" />
                            <input type="text" data-inputmask="'mask': '9', 'placeholder': ''" maxlength="1" class="form-control form-control-solid h-60px w-60px fs-2qx text-center border-primary border-hover mx-1 my-2 otp-input" />
                            <input type="text" data-inputmask="'mask': '9', 'placeholder': ''" maxlength="1" class="form-control form-control-solid h-60px w-60px fs-2qx text-center border-primary border-hover mx-1 my-2 otp-input" />
                            <input type="text" data-inputmask="'mask': '9', 'placeholder': ''" maxlength="1" class="form-control form-control-solid h-60px w-60px fs-2qx text-center border-primary border-hover mx-1 my-2 otp-input" />
                        </div>
                        @error('verification_code')
                        <span class="text-danger fw-bold">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="d-flex flex-wrap justify-content-center pb-lg-0">
                        <input type="hidden" id="verification_input" name="verification_code" />
                        <button type="submit" class="btn btn-lg btn-primary fw-bolder me-4">{{__('JsPress::backend.verify')}}</button>
                    </div>
                </form>
                <div class="text-center fw-bold pt-3 fs-5">
                    <span class="text-muted me-1">{{ __('JsPress::backend.resend_verification_code') }}</span>
                    <a href="javascript:void(0);" id="resendVeriricationEl" onclick="resendVerificationCode()" class="link-primary fw-bolder fs-5 me-1">{{__('JsPress::backend.resend')}}</a>
                    <div id="resendLoading" class="spinner-border text-primary d-none" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{!! asset('jspress/assets/plugins/global/plugins.bundle.js') !!}"></script>
<script src="{!! asset('jspress/assets/js/scripts.bundle.js') !!}"></script>
<script>

    const resendVerificationCode = () => {
        document.getElementById('resendLoading').classList.remove('d-none');
        document.getElementById('resendVeriricationEl').classList.add('d-none');
        fetch("{{Route('admin.resend_verification_code')}}",
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'X-CSRF-TOKEN': $('input[name="_token"]').val()
                },
                method: "POST",
                body: JSON.stringify({})
            })
            .then(function(res){
                return res.json();
            })
            .catch(function(err){
                Swal.fire({
                    text: err.message,
                    icon: "error",
                    buttonsStyling: false,
                    confirmButtonText: '{{__('JsPress::backend.ok')}}',
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                });
            })
            .then((data) => {
                Swal.fire({
                    text: data.message,
                    icon: "success",
                    buttonsStyling: false,
                    confirmButtonText: '{{__('JsPress::backend.ok')}}',
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                });
                document.getElementById('resendLoading').classList.add('d-none');
                document.getElementById('resendVeriricationEl').classList.remove('d-none');
            })
    };

    document.addEventListener('DOMContentLoaded', function () {
        const inputs = document.querySelectorAll('.otp-input');
        const hiddenInput = document.getElementById('verification_input');
        const form = document.getElementById('verification_form');

        form.addEventListener('submit', (e) => {
            e.preventDefault(); // Normal submit işlemini durdur (opsiyonel)

            // Tüm input'lardaki değerleri birleştir
            let otpCode = '';
            inputs.forEach(input => {
                otpCode += input.value;
            });

            // Gizli input'a değerini ata
            hiddenInput.value = otpCode;

            // Formu submit et
            form.submit(); // Bu satır, manuel submit etmeyi sağlar
        });

        inputs.forEach((input, index) => {
            input.addEventListener('input', (e) => {
                const value = e.target.value;

                // Eğer kullanıcı birden fazla karakter yapıştırdıysa
                if (value.length > 1) {
                    handlePaste(e, inputs);
                    return;
                }

                // Sadece sayısal karakterlere izin ver
                if (!/[0-9]/.test(value)) {
                    e.target.value = '';
                    return;
                }

                // Bir sonraki input'a geç
                if (value !== '' && index < inputs.length - 1) {
                    inputs[index + 1].focus();
                }
            });

            input.addEventListener('keydown', (e) => {
                // Eğer backspace ve input dolu değilse bir önceki input'a dön
                if (e.key === 'Backspace') {
                    if (input.value === '' && index > 0) {
                        inputs[index - 1].focus();
                    } else {
                        input.value = '';  // Şu anki input'u sıfırla
                    }
                }
            });
        });

        // Yapıştırma işlemi
        const handlePaste = (e, inputs) => {
            const paste = e.clipboardData.getData('text');
            const pasteArray = paste.split('').slice(0, inputs.length);

            inputs.forEach((input, i) => {
                input.value = pasteArray[i] || ''; // Kopyalanan değeri yerleştir
            });

            setTimeout(function(){
                inputs[Math.min(pasteArray.length - 1, inputs.length - 1)].focus();
            },100);
        };

        // Yapıştırma olayını dinleme
        inputs[0].addEventListener('paste', (e) => {
            e.preventDefault();
            handlePaste(e, inputs);
        });
    });
</script>
</body>
</html>
