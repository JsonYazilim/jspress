<!doctype html>
<html dir="ltr" lang="tr">
<head>

    <style>
        .content{
            background: white;
            max-width: 760px;
            padding: 100px;
            margin: 0 auto;
            font-size: 16px;
            text-align: center;
        }
        .content strong{
            font-size: 55px;
            display: block;
            font-weight: 800;
            margin: 30px 0;
        }
        .content a,button{
            background: #3D3D3D;
            color:white;
            display: inline-block;
            text-align: center;
            text-decoration: none;
            font-size: 14px;
            font-weight: bold;
            padding: 15px 35px;
            border-radius: 30px;
        }
        .content small{
            display: block;
            font-size: 12px;
            max-width: 480px;
            margin: 30px auto 0;
        }
    </style>
</head>
<body>
<div class="content">
    <p>{{__('JsPress::backend.hello_admin', ['admin'=>$admin->name])}},</p>
    <p>{{__('JsPress::backend.two_factor_email_description')}}</p>
    <strong>{{$admin->verification_code}}</strong>
    <small>{{__('JsPress::backend.two_factor_email_description_last', ['minute' => 5])}}</small>
</div>
</body>
</html>
