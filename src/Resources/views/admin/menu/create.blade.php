@extends('JsPress::admin.layouts.app')
@push('style')
    <style>
        .dd-item,
        .dd-empty,
        .dd-placeholder { display: block; position: relative; margin: 0; padding: 0; min-height: 20px; font-size: 13px; line-height: 20px; }
        .dd-placeholder,
        .dd-empty { margin: 5px 0; padding: 0; min-height: 30px; background: #f5f8fa; border: 1px dashed #d3d3d3; box-sizing: border-box; -moz-box-sizing: border-box; }
        .dd-empty { border: 1px dashed #bbb; min-height: 100px;
            background:#000;
            background-size: 60px 60px;
            background-position: 0 0, 30px 30px;
        }
        .dd-dragel { position: absolute; pointer-events: none; z-index: 9999; }
        .dd-dragel > .dd-item .dd-handle { margin-top: 0; }
        .dd-dragel .dd-handle {
            -webkit-box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
            box-shadow: 2px 4px 6px 0 rgba(0,0,0,.1);
        }
        .dd-handle{
            box-shadow: 0 0 13px 0 rgba(0,0,0,.05);
            background: #fff;
            padding: 15px;
            border: 1px solid #ddd;
        }
        [data-action="collapse"], [data-action="expand"]{
            display:none !important;
        }
        .openDetails{
            cursor:pointer;
        }
        .menuDetail{
            background:#fff;
        }
        .menuDetail.active{
            border:1px solid #ddd;
        }
        .dd-item > .dd-list {
            margin-top: 0.5rem!important;
        }
        .dd-empty {
            display: none;
        }
        /**
         * Nestable Extras
         */
    </style>
@endpush
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{__('JsPress::backend.add_new_menu')}}</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    @if(website_languages()->count() > 0)
                        <div class="d-flex align-items-center gap-2 gap-lg-3">
                            <span class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{__('JsPress::backend.content_language')}}:</span>
                            <div class="d-flex">
                                <img src="{{asset('/jspress/assets/media/flags/svg/'.JsPressPanel::contentLocale().'.svg')}}" class="me-2" width="20"  alt="{{JsPressPanel::contentLocale()}}"/>
                                <span>{{display_language(JsPressPanel::contentLocale())->display_language_name}}</span>
                            </div>
                        </div>
                    @endif
                    <button class="btn btn-sm btn-success" form="menu_form">{{__('JsPress::backend.save_changes')}}</button>
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                <form id="menu_form" action="{{Route('admin.menu.store')}}" method="POST">
                    @csrf
                    <input type="hidden" name="language" value="{{ request()->input('lang') ?? JsPressPanel::contentLocale() }}" />
                    @isset($menu['id'])
                        <input type="hidden" name="id" value="{{ $menu['id'] }}" />
                        @if(request()->input('trid'))
                            <input type="hidden" name="transaction_id" value="{{ request()->input('trid') }}" />
                        @else
                            <input type="hidden" name="transaction_id" value="{{ $menu['translation']['transaction_id'] }}" />
                        @endif
                        @if(request()->input('source_locale'))
                            <input type="hidden" name="source_locale" value="{{ request()->input('source_locale') }}" />
                        @else
                            <input type="hidden" name="source_locale" value="{{ $menu['translation']['source_locale'] }}" />
                        @endif
                    @else
                        @if(request()->input('trid'))
                            <input type="hidden" name="transaction_id" value="{{ request()->input('trid') }}" />
                        @endif
                        @if(request()->input('source_locale'))
                            <input type="hidden" name="source_locale" value="{{ request()->input('source_locale') }}" />
                        @endif
                    @endif
                    <input type="hidden" name="menu_data" id="nestable-output" />
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="mb-5">
                                <label class="required form-label">{{ __('JsPress::backend.menu_title') }}</label>
                                <input type="text" name="title" class="form-control mb-2" placeholder="Bir başlık giriniz..." value="{{ @$menu['title'] }}" required="" autocomplete="off">
                                <div class="text-muted fs-7">{{ __('JsPress::backend.type_max_hundred_letters') }}</div>
                            </div>
                            <div class="mb-5">
                                <label class="required form-label">{{ __('JsPress::backend.menu_slug') }}</label>
                                <input type="text" name="slug" class="form-control mb-2" value="{{ @$menu['slug'] }}" required="" autocomplete="off">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="card card-flush">
                                <div class="card-header pt-3 border border-dashed" id="kt_chat_contacts_header">
                                    <div class="card-title">
                                        <h2>{{ __('JsPress::backend.add_menu_item') }}</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-5">
                                    @foreach($menu_options as $type => $menu_type)
                                        @foreach($menu_type as $key => $option_item)
                                            <div class="accordion" id="{{$type}}_{{$key}}_accordion">
                                                <div class="accordion-item rounded-0">
                                                    <h2 class="accordion-header" id="{{ $key }}_header">
                                                        <button class="accordion-button fs-6 fw-bold p-4 rounded-0 {{ $key != 'page' ? 'collapsed':'' }}" type="button" data-bs-toggle="collapse" data-bs-target="#{{ $type.'_'.$key }}_body" aria-expanded="false" aria-controls="{{ $type.'_'.$key }}_body">
                                                            {!! $option_item['name'] !!}
                                                        </button>
                                                    </h2>
                                                    <div id="{{ $type.'_'.$key }}_body" class="accordion-collapse collapse {{ $key == 'page' ? 'show':'' }}" aria-labelledby="{{ $type.'_'.$key }}_header" data-bs-parent="#{{$type.'_'.$key}}_accordion">
                                                        <div class="accordion-body pb-2">
                                                            <ul class="nav nav-tabs border-0" id="{{$type.'_'.$key}}_nav_tabs" role="tablist">
                                                                @foreach( $option_item['menu_items'] as $k => $items )
                                                                    <li class="nav-item" role="presentation">
                                                                        <a class="nav-link {{ $k == 'recently_added' ? 'active':'' }}" id="{{$type.'_'.$key}}_nav_tabs_{{$k}}" data-bs-toggle="tab" href="#{{$type.'_'.$key}}_{{$k}}" role="tab" aria-controls="{{$type.'_'.$key}}_{{$k}}" aria-selected="true" >{{ __('JsPress::backend.'.$k) }}</a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                            <div class="tab-content">
                                                                @foreach( $option_item['menu_items'] as $k => $items )
                                                                    <div class="tab-pane fade {{ $k == 'recently_added' ? 'show active':'' }}" id="{{$type.'_'.$key}}_{{$k}}" role="tabpanel" aria-labelledby="{{$type.'_'.$key}}_nav_tabs_{{$k}}">
                                                                        <div class="d-flex flex-column border border-1 pt-3 p-2" style="max-height: 150px; overflow: scroll;">
                                                                            @foreach($items as $item)
                                                                                <div class="form-check form-check-custom form-check-solid form-check-sm mb-2" data-key="{{$key}}">
                                                                                    <input name="menu_items[]" class="form-check-input" type="checkbox" value="{{$item['id']}}" id="menu_{{$type}}_{{$key}}_{{$k}}_{{$item['id']}}" data-name="{{$item['title']}}" data-type="{{$type}}"/>
                                                                                    <label class="form-check-label" for="menu_{{$type}}_{{$key}}_{{$k}}_{{$item['id']}}">
                                                                                        {{ $item['title'] }}
                                                                                    </label>
                                                                                </div>
                                                                            @endforeach
                                                                        </div>
                                                                        <div class="d-flex justify-content-end mt-2">
                                                                            <button type="button" class="btn btn-sm btn-primary fs-8" onclick="addToMenu('{{ $key }}')">Menüye ekle</button>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endforeach
                                    <div class="accordion mb-5" id="custom_menu">
                                        <div class="accordion-item accordion-item rounded-0">
                                            <h2 class="accordion-header" id="custom_menu_header">
                                                <button class="accordion-button fs-6 fw-bold p-4 rounded-0 collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#custom_menu_body" aria-expanded="false" aria-controls="custom_menu_body">
                                                    {{ __('JsPress::backend.add_custom_url') }}
                                                </button>
                                            </h2>
                                            <div id="custom_menu_body" class="accordion-collapse collapse " aria-labelledby="custom_menu_header" data-bs-parent="#custom_menu">
                                                <div class="accordion-body pb-2">
                                                    <div class="p-2">
                                                        <div class="mb-5">
                                                            <label class="required form-label">URL</label>
                                                            <input type="text" class="form-control mb-2 customLinkUrl" placeholder="https://" value="" autocomplete="off">
                                                        </div>
                                                        <div class="mb-5">
                                                            <label class="required form-label">{{ __('JsPress::backend.menu_title') }}</label>
                                                            <input type="text" class="form-control mb-2 customLinkTitle" placeholder="" value="" autocomplete="off">
                                                        </div>
                                                        <div class="d-flex justify-content-end mt-2">
                                                            <button type="button" class="btn btn-sm btn-primary fs-8" onclick="addToCustomMenu()">{{ __('JsPress::backend.add_to_menu') }}</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-8">
                            <div class="card card-flush ">
                                <div class="card-header pt-7" id="kt_chat_contacts_header">
                                    <div class="card-title">
                                        <h2>{{ __('JsPress::backend.menu_hiearchi') }}</h2>
                                    </div>
                                </div>
                                <div class="card-body pt-5">
                                    <div id="nestable" class="dd">
                                        <ol class="dd-list list-unstyled p-5 min-h-400px" style="background-color: #f5f8fa;">

                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        @isset($menu['id'])
            window._menu_items = @json($menu['data']);
        @else
            window._menu_items = null;
        @endif
    </script>
    <script src="{{asset('jspress/libs/jquery.nestable.js')}}"></script>
    <script src="{!! asset('jspress/assets/js/custom/validation/jquery.validate.min.js') !!}"></script>
    @if( \File::exists(public_path('jspress/assets/js/custom/validation/localization/messages_'.JsPressPanel::panelLocale().'.js')) )
        <script src="{!! asset('jspress/assets/js/custom/validation/localization/messages_'.JsPressPanel::panelLocale().'.js') !!}"></script>
    @endif
    <script>
        $(document).ready(function(){
            if(window._menu_items != null){
                appendMenuItems(window._menu_items);
                updateOutput($('#nestable').data('output', $('#nestable-output')));
            }
        });
        var form = $('#menu_form');
        form.validate({
            ignore: []
        });
        var updateOutput = function(e)
        {
            var list   = e.length ? e : $(e.target);
            var output = list.data('output');
            output.val(window.JSON.stringify(list.nestable('serialize')));
        };
        $('#nestable').nestable({
            group: 1,
            collapseBtnHTML:'<button class="btn btn-sm btn-icon btn-primary" data-action="collapse"><i class="fas fa-minus"></i></button>',
            expandBtnHTML:'<button class="btn btn-sm btn-icon btn-primary" data-action="expand"><i class="fas fa-plus"></i></button>'
        }).on('change', updateOutput);

        updateOutput($('#nestable').data('output', $('#nestable-output')));

        $(document).on('click', '.openDetails', function(){
            var menuDetail = $(this).closest('.dd-item').find('.menuDetail').first();
            menuDetail.slideToggle();
            if(menuDetail.hasClass('active')){
                menuDetail.removeClass('active');
            }else{
                menuDetail.addClass('active');
            }
            if(menuDetail.hasClass('active')){
                $(this).addClass('active');
            }else{
                $(this).removeClass('active');
            }
        });
        var addToMenu = function(key){
            var menuArray = [];
            $('[data-key="'+key+'"] input').each(function(){
                var id = parseInt($(this).val());
                var name = $(this).attr('data-name');
                var type = $(this).attr('data-type');
                if($(this).is(':checked')){
                    menuArray.push({
                        id : id,
                        title : name,
                        type:type
                    });
                }
                $(this).prop('checked', false);
            });
            if(menuArray.length > 0){
                for(var i = 0; i < menuArray.length; i++){
                    var $clone = menuTemplate(menuArray[i].id, menuArray[i].title, menuArray[i].type);
                    $('.dd-list').first().append($clone);
                }
                updateOutput($('#nestable').data('output', $('#nestable-output')));
            }

        }

        var addToCustomMenu = function(){
            var name = $('.customLinkTitle').val();
            var type = 'custom';
            var url = $('.customLinkUrl').val();
            var $clone = menuTemplate(0, name, type, '_self', url);
            $('.dd-list').first().append($clone);
            $('.customLinkTitle').val("");
            $('.customLinkUrl').val("");
            updateOutput($('#nestable').data('output', $('#nestable-output')));
        }

        var menuTemplate = function(id, title, type, target = '_self', url = null,){
            var checked = target === "_blank" ? "checked":"";
            var data_url = url === null ? "" : url;
            var html = '<li class="dd-item mb-2" data-id="'+id+'" data-title="'+title+'" data-url="'+data_url+'" data-type="'+type+'" data-target="'+target+'">';
            html += '<div class="dd-handle fw-bold cursor-move">';
            html += '<span class="text-gray-700 fs-5 menuTitle">'+title+'</span>';
            html += '</div>';
            html += '<div class="position-absolute start top-0 end-0 w-50px h-50px d-flex align-items-center justify-content-center rotate openDetails"><i class="fas fa-arrow-down fs-4 rotate-180"></i> </div>';
            html += ' <div class="menuDetail" style="display:none;">';
            html += '<div class="d-flex flex-column p-5">';
            html += '<div class="mb-5">';
            if( url != null ){
                html += '<div class="mb-5">';
                html += '<label class="form-label">'+trans.menu_url+'</label>';
                html += '<input type="text" class="form-control menuCustomUrl mb-2" placeholder="https://" value="'+url+'" autocomplete="off">';
                html += '</div>';
            }
            html += '<div class="mb-5">';
            html += '<label class="required form-label">'+trans.type_menu_title+'</label>';
            html += '<input type="text" name="title" class="form-control menuInputTitle mb-2" placeholder="'+trans.type_a_title+'" value="'+title+'" autocomplete="off">';
            html += '</div>';
            html += '<div class="mb-2">';
            html += '<div class="d-flex">';
            html += '<div class="w-75">';
            html += '<label class="form-check form-switch form-check-custom form-check-solid">';
            html += '<input class="form-check-input toggleTarget" type="checkbox" '+checked+' />';
            html += '<span class="form-check-label fw-bold text-muted">'+trans.open_new_tab+'</span>';
            html += '</label>';
            html += '</div>';
            html += '<div class="d-flex justify-content-end w-25"><button type="button" class="btn btn-danger btn-sm btn-sm removeMenuItem" data-id="'+id+'">'+trans.remove+'</button></div>';
            html += '</div></div></div></div></li>';
            return html;
        }

        var appendMenuItems = function(menu_items, children = null){
            if(children === null){
                var menuContainer = document.querySelector('.dd-list');
            }else{
                var menuContainer = children;
            }
            for(var i = 0; i < menu_items.length; i++){
                var template = menuTemplate(menu_items[i].id, menu_items[i].title, menu_items[i].type, menu_items[i].target, menu_items[i].url);
                menuContainer.insertAdjacentHTML('beforeend', template);
                if( menu_items[i].hasOwnProperty('children') ){
                    var createMenuContainer = '<ol class="dd-list"></ol>';
                    var menuContainerChildren = menuContainer.querySelectorAll('.dd-item');
                    menuContainerChildren = menuContainerChildren[menuContainerChildren.length - 1];
                    menuContainerChildren.insertAdjacentHTML('beforeend', createMenuContainer);
                    menuContainerChildren = menuContainerChildren.querySelector('.dd-list');
                    appendMenuItems(menu_items[i].children, menuContainerChildren);
                }
            }
        }

        $(document).on('change', '.toggleTarget', function(){
            if($(this).is(':checked')){
                $(this).closest('.dd-item').data('target', '_blank');
            }else{
                $(this).closest('.dd-item').data('target', '_self');
            }
            setTimeout(function(){
                updateOutput($('#nestable').data('output', $('#nestable-output')));
            },500);
        });
        $(document).on('change', '.menuInputTitle', function(){
            var val = $(this).val();
            $(this).closest('.dd-item').find('.menuTitle').text(val);
            $(this).closest('.dd-item').data('title', val);
            setTimeout(function(){
                updateOutput($('#nestable').data('output', $('#nestable-output')));
            },500);
        });
        $(document).on('change', '.menuCustomUrl', function(){
            var val = $(this).val();
            $(this).closest('.dd-item').data('url', val);
            setTimeout(function(){
                updateOutput($('#nestable').data('output', $('#nestable-output')));
            },500);
        });

        $(document).on('click', '.removeMenuItem', function(){
            var parent = $(this).closest('.dd-item');
            var childOfParent = parent.find('.dd-list');
            var parentOfParent = parent.closest('.dd-list');
            if(childOfParent.length > 0){
                var html = childOfParent.html();
                parent.fadeOut(400, function(){
                    $(this).remove();
                    parentOfParent.append(html);
                });
            }else{
                parent.fadeOut(400, function(){
                    $(this).remove();
                });
            }
            setTimeout(function(){
                updateOutput($('#nestable').data('output', $('#nestable-output')));
            },500);
        });

        form.on('submit', function(e){
            e.preventDefault();
            console.log(form.valid());
            if(!form.valid()){
                return;
            }
            $.ajax({
                url:'{{Route('admin.menu.store')}}',
                type:'POST',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data:{
                    id:$('input[name="id"]').val(),
                    title:$('input[name="title"]').val(),
                    slug:$('input[name="slug"]').val(),
                    data:JSON.parse($('input[name="menu_data"]').val()),
                    locale:$('input[name="language"]').val(),
                    source_locale:$('input[name="source_locale"]').val(),
                    transaction_id:$('input[name="transaction_id"]').val()
                },
                dataType:'json',
                success:function(res){
                    if(res.status === 1){
                        Swal.fire({
                            text: res.msg,
                            icon: "success",
                            buttonsStyling: false,
                            confirmButtonText: '{{__('JsPress::backend.ok')}}',
                            customClass: {
                                confirmButton: "btn btn-light"
                            }
                        }).then(function(){
                            window.location.href = res.route;
                        });
                    }
                }
            })
            return false;
        });
        $('input[name="title"]').on('change', function(){
            var slug = $('input[name="slug"]').val();
            if(slug == null || slug === ""){
                var slugify = JsPressFunc.slugify($(this).val());
                $('input[name="slug"]').val(slugify);
            }
        });
    </script>
@endpush
