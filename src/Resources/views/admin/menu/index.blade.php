@extends('JsPress::admin.layouts.app')
@section('content')
    <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
        <div class="toolbar" id="kt_toolbar">
            <div id="kt_toolbar_container" class="container-fluid d-flex flex-stack">
                <div data-kt-swapper="true" data-kt-swapper-mode="prepend" data-kt-swapper-parent="{default: '#kt_content_container', 'lg': '#kt_toolbar_container'}" class="page-title d-flex align-items-center flex-wrap me-3 mb-5 mb-lg-0">
                    <h1 class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{__('JsPress::backend.menus')}}</h1>
                    <span class="h-20px border-gray-300 border-start mx-4"></span>
                    @include('JsPress::admin.layouts.parts.breadcrumb', ['breadcrumbs' => $breadcrumbs])
                </div>
                @if(website_languages()->count() > 0)
                    <div class="d-flex align-items-center gap-2 gap-lg-3">
                        <span class="d-flex text-dark fw-bolder fs-3 align-items-center my-1">{{__('JsPress::backend.content_language')}}:</span>
                        <div class="d-flex">
                            <img src="{{asset('/jspress/assets/media/flags/svg/'.JsPressPanel::contentLocale().'.svg')}}" class="me-2" width="20"  alt="{{JsPressPanel::contentLocale()}}"/>
                            <span>{{display_language(JsPressPanel::contentLocale())->display_language_name}}</span>
                        </div>
                    </div>
                @endif
                <a href="{{ Route('admin.menu.create') }}" class="btn btn-sm btn-primary">{{__('JsPress::backend.add_new_menu')}}</a>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-fluid">
                <div class="card card-flush">
                    <div class="card-header align-items-center py-5 gap-2 gap-md-5">
                        <div class="card-title">
                            <div class="d-flex align-items-center position-relative my-1">
                                <span class="svg-icon svg-icon-1 position-absolute ms-4">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                                        <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                                    </svg>
                                </span>
                                <input type="text" data-kt-filter="search" class="form-control form-control-solid w-250px ps-14" placeholder="Hızlı Arama" />
                            </div>
                        </div>
                    </div>
                    <div class="card-body pt-0">
                        <div class="table-responsive">
                            <table class="table align-middle table-row-dashed fs-6 gy-5" id="category_datatable">
                                <thead>
                                    <tr class="text-start text-gray-400 fw-bolder fs-7 text-uppercase gs-0">
                                        <th class="w-10px pe-2">
                                            <div class="form-check form-check-sm form-check-custom form-check-solid me-3">
                                                <input class="form-check-input" type="checkbox" data-kt-check="true" data-kt-check-target="#kt_ecommerce_category_table .form-check-input" value="1" />
                                            </div>
                                        </th>
                                        <td class="min-w-100px">Menu Başlık</td>
                                        <td class="min-w-100px">Menu Slug</td>
                                        <th class="min-w-100px">
                                            @foreach(website_languages()->where('locale', '!=', JsPressPanel::contentLocale()) as $language)
                                                <img src="{{asset("/jspress/assets/media/flags/svg/".$language->locale.".svg")}}" width="20" height="20" class="me-2"/>
                                            @endforeach
                                        </th>
                                        <th class="text-end min-w-70px">{{__('JsPress::backend.actions')}}</th>
                                    </tr>
                                </thead>
                                <tbody class="fw-bold text-gray-600">
                                @foreach($menus as $menu)
                                    <tr>
                                        <td>
                                            <div class="form-check form-check-sm form-check-custom form-check-solid">
                                                <input class="form-check-input" name="menu_id[]" type="checkbox" value="{{$menu['id']}}" />
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex">
                                                <div class="ms-5">
                                                    <a href="#" class="text-gray-800 text-hover-primary fs-7 fw-bolder mb-1">{{$menu['title']}}</a>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            {{$menu['slug']}}
                                        </td>
                                        <td>
                                            @foreach($menu['translations'] as $locale => $menu_item)
                                                @if($menu_item['type'] == 'add')
                                                    <a href="{{$menu_item['url']}}">
                                                        <span class="btn btn-icon btn-sm btn-color-primary btn-active-light-primary border rounded-1 w-25px h-25px">
                                                            <span class="svg-icon svg-icon-2">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <rect opacity="0.5" x="11" y="18" width="12" height="2" rx="1" transform="rotate(-90 11 18)" fill="black"/>
                                                                    <rect x="6" y="11" width="12" height="2" rx="1" fill="black"/>
                                                                </svg>
                                                            </span>
                                                        </span>
                                                    </a>
                                                @endif
                                                @if($menu_item['type'] == 'edit')
                                                    <a href="{{$menu_item['url']}}">
                                                        <span class="btn btn-icon btn-sm btn-color-warning btn-active-light-warning border rounded-1 w-25px h-25px">
                                                            <span class="svg-icon svg-icon-3">
                                                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                                    <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M2 4.63158C2 3.1782 3.1782 2 4.63158 2H13.47C14.0155 2 14.278 2.66919 13.8778 3.04006L12.4556 4.35821C11.9009 4.87228 11.1726 5.15789 10.4163 5.15789H7.1579C6.05333 5.15789 5.15789 6.05333 5.15789 7.1579V16.8421C5.15789 17.9467 6.05333 18.8421 7.1579 18.8421H16.8421C17.9467 18.8421 18.8421 17.9467 18.8421 16.8421V13.7518C18.8421 12.927 19.1817 12.1387 19.7809 11.572L20.9878 10.4308C21.3703 10.0691 22 10.3403 22 10.8668V19.3684C22 20.8218 20.8218 22 19.3684 22H4.63158C3.1782 22 2 20.8218 2 19.3684V4.63158Z" fill="black"/>
                                                                    <path d="M10.9256 11.1882C10.5351 10.7977 10.5351 10.1645 10.9256 9.77397L18.0669 2.6327C18.8479 1.85165 20.1143 1.85165 20.8953 2.6327L21.3665 3.10391C22.1476 3.88496 22.1476 5.15129 21.3665 5.93234L14.2252 13.0736C13.8347 13.4641 13.2016 13.4641 12.811 13.0736L10.9256 11.1882Z" fill="black"/>
                                                                    <path d="M8.82343 12.0064L8.08852 14.3348C7.8655 15.0414 8.46151 15.7366 9.19388 15.6242L11.8974 15.2092C12.4642 15.1222 12.6916 14.4278 12.2861 14.0223L9.98595 11.7221C9.61452 11.3507 8.98154 11.5055 8.82343 12.0064Z" fill="black"/>
                                                                </svg>
                                                            </span>
                                                        </span>
                                                    </a>
                                                @endif
                                            @endforeach
                                        </td>
                                        <td class="text-end">
                                            <a href="javascript:void(0);" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">{{__('JsPress::backend.actions')}}
                                                <span class="svg-icon svg-icon-5 m-0">
                                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                                        <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="black" />
                                                    </svg>
                                                </span>
                                            </a>
                                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-125px py-4" data-kt-menu="true">
                                                <div class="menu-item px-3">
                                                    <a href="{{Route('admin.menu.edit', ['id'=>$menu['id']])}}" class="menu-link px-3">{{__('JsPress::backend.edit')}}</a>
                                                </div>
                                                <div class="menu-item px-3">
                                                    <a href="javascript:void(0);" class="menu-link px-3 deleteMenu" data-id="{{ $menu['id'] }}" data-lang="{{ JsPressPanel::contentLocale() }}">{{__('JsPress::backend.delete')}}</a>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('script')
    <script>
        $('.deleteMenu').on('click', function(){
            let id = $(this).attr('data-id');
            let lang = $(this).attr('data-lang');
            Swal.fire({
                text: "{{ __('JsPress::backend.delete_menu_warning') }}",
                icon: "warning",
                showCancelButton: true,
                buttonsStyling: true,
                confirmButtonText: "{!! __('JsPress::backend.yes_i_confirm') !!}",
                cancelButtonText: "{!! __('JsPress::backend.no_return') !!}",
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-active-light"
                },
            }).then(
                function (t) {
                    if( t.isConfirmed ){
                        $.ajax({
                            url:'{!! Route('admin.menu.destroy') !!}',
                            type:'POST',
                            headers:{
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data:{
                                id:id,
                                lang:lang
                            },
                            dataType:'json',
                            success:function(res){
                                if(res.status === 1){
                                    Swal.fire({ text: res.msg, icon: "success", buttonsStyling: !1, confirmButtonText: "{!! __('JsPress::backend.confirmButton') !!}", customClass: { confirmButton: "btn btn-primary" } }).then(
                                        function (t) {
                                            location.reload();
                                        }
                                    );
                                }else{
                                    Swal.fire({ text: res.msg, icon: "error", buttonsStyling: !1, confirmButtonText: "{!! __('JsPress::backend.confirmButton') !!}", customClass: { confirmButton: "btn btn-primary" } }).then(
                                        function (t) {
                                        }
                                    );
                                }
                            }
                        });
                    }
                }
            );
        });
    </script>
@endpush


