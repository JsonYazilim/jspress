<?php

namespace JsPress\JsPress;


use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;
use JsPress\JsPress\Http\Middlewares\RedirectRules;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const ADMIN = '/js-admin/dashboard';

    protected string $route_path = __DIR__.'/Routes';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    protected $namespace = 'JsPress\JsPress\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->routes(function () {
            Route::prefix('js-admin')
                ->middleware(['web', \Opcodes\LogViewer\Http\Middleware\AuthorizeLogViewer::class])
                ->name('admin.')
                ->namespace($this->namespace.'\Admin')
                ->group($this->route_path.'/panel.php');

            Route::middleware(['web'])
                ->namespace($this->namespace)
                ->group($this->route_path.'/web.php');
        });
    }

}
