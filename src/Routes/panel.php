<?php
/*
|--------------------------------------------------------------------------
| Panel Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Route;
use JsPress\JsPress\Http\Controllers\Admin\AdminController;
use JsPress\JsPress\Http\Controllers\Admin\Auth\LoginController;
use JsPress\JsPress\Http\Controllers\Admin\PostController;
use JsPress\JsPress\Http\Controllers\Admin\AdminServicesController;
use JsPress\JsPress\Http\Controllers\Admin\UserController;
use JsPress\JsPress\Http\Controllers\Admin\RoleController;
use JsPress\JsPress\Http\Controllers\Admin\CategoryController;
use JsPress\JsPress\Http\Controllers\Admin\ProfileController;
use JsPress\JsPress\Http\Controllers\Admin\SystemController;
use JsPress\JsPress\Http\Controllers\Admin\SettingsController;
use JsPress\JsPress\Http\Controllers\Admin\LanguageController;
use JsPress\JsPress\Http\Controllers\Admin\TranslationController;
use JsPress\JsPress\Http\Controllers\Admin\MediaController;
use JsPress\JsPress\Http\Controllers\Admin\PostTypeController;
use JsPress\JsPress\Http\Controllers\Admin\FileManagerController;
use JsPress\JsPress\Http\Controllers\Admin\PostFieldController;
use JsPress\JsPress\Http\Controllers\Admin\MenuController;
use JsPress\JsPress\Http\Controllers\Admin\WebsiteAnalystics;
use JsPress\JsPress\Http\Controllers\Admin\VCSController;
use JsPress\JsPress\Http\Controllers\Admin\RedirectController;
use JsPress\JsPress\Http\Controllers\Admin\PopupController;
use JsPress\JsPress\Http\Controllers\Admin\CookieConsentController;


Route::get('/', [AdminController::class, 'index'])->name('login')->middleware('admin');
Route::post('login', [LoginController::class, 'login'])->name('login.attempt');
Route::get('verify-login', [AdminController::class, 'verifiyTwoFactor'])->name('verify_login');
Route::post('verify-account', [LoginController::class, 'verifyTwoFactor'])->name('verify_account');
Route::post('resend-verification-code', [LoginController::class, 'reSendVerificationCode'])->name('resend_verification_code');
Route::group(
    [
        'middleware' => ['js-auth:admin', 'panel', '2fa']
    ],
    function()
    {

        Route::get('/dashboard', [AdminController::class, 'dashboard'])->name('dashboard');
        Route::post('/change-theme', [AdminController::class, 'changeTemplate']);
        Route::get('/logout', [AdminController::class, 'logout'])->name('logout');
        Route::post('/avatar-upload', [AdminServicesController::class, 'fileUpload'])->name('fileUpload');
        Route::post('/change-post-order', [AdminServicesController::class, 'changePostOrder'])->name('change_order');
        Route::post('/upload', [MediaController::class, 'store'])->name('upload');
        Route::post('/loadFiles', [MediaController::class, 'getJson']);
        Route::post('/getMediaDetail/{id}', [MediaController::class, 'getDetailJson']);
        Route::post('/saveMediaDetail/{id}', [MediaController::class, 'saveDetails']);
        Route::post('/delete-file/{id}', [MediaController::class, 'destroy']);

        Route::get('/dashboard', [AdminController::class, 'dashboard'])->name('dashboard');
        Route::post('/change-theme', [AdminController::class, 'changeTemplate']);
        Route::post('/change-language', [AdminController::class, 'changeLanguage']);
        Route::post('/change-content-language', [AdminController::class, 'changeContentLanguage']);
        Route::prefix('users')->name('user.')->group(function () {
            Route::get('/', [UserController::class, 'index'])->name('list');
            Route::post('/store', [UserController::class, 'store'])->name('store');
            Route::get('/edit/{user}', [UserController::class, 'edit'])->name('edit');
            Route::post('/update/{user}', [UserController::class, 'update'])->name('update');
            Route::post('/delete', [UserController::class, 'destroy'])->name('destroy');
            Route::post('/update-password/{user}', [UserController::class, 'updatePassword'])->name('updatePassword');
            Route::post('/update-role/{user}', [UserController::class, 'updateUserRole'])->name('updateUserRole');
            Route::post('/update-status/{user}', [UserController::class, 'updateUserStatus'])->name('updateUserStatus');
            Route::post('/checkEmail', [UserController::class, 'checkEmailExists'])->name('checkEmail');
            Route::post('/verify', [UserController::class, 'verify'])->name('verify');
        });

        Route::prefix('admin')->name('admin.')->group(function () {
            Route::get('/', [AdminController::class, 'list'])->name('index');
            Route::get('/edit/{admin}', [AdminController::class, 'edit'])->name('edit');
            Route::post('/update', [AdminController::class, 'update'])->name('update');
            Route::post('/email', [AdminController::class, 'checkEmailExists'])->name('email');
            Route::post('/store', [AdminController::class, 'store'])->name('store');
            Route::post('/delete', [AdminController::class, 'destroy'])->name('destroy');
            Route::post('/update/password', [AdminController::class, 'updatePassword'])->name('update.password');
            Route::post('/update/role}', [AdminController::class, 'updateRole'])->name('update.role');
            Route::post('/update-panel', [AdminController::class, 'updateAdminPanel'])->name('update.panel');
            Route::post('/toggle-two-factor', [AdminController::class, 'toggleTwoFactor'])->name('toggle_two_factor');
        });

        Route::prefix('roles')->name('role.')->group(function () {
            Route::get('/', [RoleController::class, 'index'])->name('list');
            Route::post('/getRolePermissions', [RoleController::class, 'getRolePermissions'])->name('getRolePermissions');
            Route::post('/store', [RoleController::class, 'store'])->name('store');
            Route::post('/update', [RoleController::class, 'update'])->name('update');
            Route::post('/destroy', [RoleController::class, 'destroy'])->name('destroy');
        });

        Route::prefix('languages')->name('language.')->group(function () {
            Route::get('/', [LanguageController::class, 'index'])->name('index');
            Route::post('/default', [LanguageController::class, 'setDefaultLanguage'])->name('default');
            Route::post('/toggle-language', [LanguageController::class, 'toggleLanguage'])->name('toggle');
        });

        Route::prefix('translations')->name('translation.')->group(function () {
            Route::get('/{locale}', [TranslationController::class, 'index'])->name('index');
            Route::post('/translate', [TranslationController::class, 'store'])->name('store');
            Route::get('/export/{locale}', [TranslationController::class, 'export'])->name('export');
            Route::post('/import/{locale}', [TranslationController::class, 'import'])->name('import');
        });

        Route::prefix('post-type')->name('post_type.')->group(function () {
            Route::get('/', [PostTypeController::class, 'index'])->name('index');
            Route::get('/add', [PostTypeController::class, 'create'])->name('add');
            Route::post('/validate', [PostTypeController::class, 'validateKey'])->name('validate');
            Route::post('/store', [PostTypeController::class, 'store'])->name('store');
            Route::get('/edit/{post_type}', [PostTypeController::class, 'edit'])->name('edit');
            Route::post('/change-status', [PostTypeController::class, 'toggleStatus'])->name('status');
            Route::post('/delete', [PostTypeController::class, 'destroy'])->name('destroy');
        });

        Route::prefix('posts')->name('post.')->group(function () {
            Route::get('/', [PostController::class, 'index'])->name('index');
            Route::post('/table', [PostController::class, 'table'])->name('table');
            Route::get('/add', [PostController::class, 'create'])->name('add');
            Route::post('/check-slug', [PostController::class, 'generateSlug']);
            Route::post('/store', [PostController::class, 'store'])->name('store');
            Route::get('/edit/{id}', [PostController::class, 'edit'])->name('edit');
            Route::post('/update', [PostController::class, 'update'])->name('update');
            Route::post('/destroy', [PostController::class, 'destroy'])->name('destroy');
            Route::get('/trash', [PostController::class, 'trash'])->name('trash');
            Route::post('/restore', [PostController::class, 'restore'])->name('restore');
            Route::post('/delete', [PostController::class, 'delete'])->name('delete');
        });

        Route::prefix('categories')->name('category.')->group(function () {
            Route::get('/', [CategoryController::class, 'index'])->name('index');
            Route::get('/create', [CategoryController::class, 'create'])->name('create');
            Route::post('/store', [CategoryController::class, 'store'])->name('store');
            Route::get('/edit/{id}', [CategoryController::class, 'edit'])->name('edit');
            Route::post('/update', [CategoryController::class, 'update'])->name('update');
            Route::post('/destroy', [CategoryController::class, 'destroy'])->name('destroy');
            Route::post('/delete-translation', [CategoryController::class, 'deleteTranslationRelation']);
        });

        Route::prefix('post-field')->name('post_field.')->group(function () {
            Route::get('/', [PostFieldController::class, 'index'])->name('index');
            Route::get('/create', [PostFieldController::class, 'create'])->name('create');
            Route::post('/store', [PostFieldController::class, 'store'])->name('store');
            Route::get('/edit/{id}', [PostFieldController::class, 'edit'])->name('edit');
            Route::get('/repeater_fields', [PostFieldController::class, 'repeaterFields'])->name('repeater_field');
            Route::post('/destroy', [PostFieldController::class, 'destroy'])->name('destroy');
        });

        Route::prefix('media')->name('media.')->group(function () {
            Route::get('/', [MediaController::class, 'index'])->name('index');
        });

        Route::prefix('file-manager')->name('filemanager.')->group(function () {
            Route::get('/', [FileManagerController::class, 'index'])->name('index');
        });

        Route::prefix('profile')->name('profile.')->group(function () {
            Route::get('/', [ProfileController::class, 'index'])->name('index');
        });

        Route::prefix('settings')->name('setting.')->group(function () {
            Route::get('/', [SettingsController::class, 'index'])->name('index');
            Route::post('/store', [SettingsController::class, 'store'])->name('store');
        });

        Route::prefix('system')->name('system.')->group(function () {
            Route::get('/', [SystemController::class, 'index'])->name('index');
            Route::post('/', [SystemController::class, 'clearSystem'])->name('clear');
        });

        Route::prefix('menu')->name('menu.')->group(function () {
            Route::get('/', [MenuController::class, 'index'])->name('index');
            Route::get('/create', [MenuController::class, 'create'])->name('create');
            Route::get('/edit/{id}', [MenuController::class, 'edit'])->name('edit');
            Route::post('/store', [MenuController::class, 'store'])->name('store');
            Route::post('/destroy', [MenuController::class, 'destroy'])->name('destroy');
        });

        Route::prefix('analystic')->name('analystic.')->group(function () {
            Route::post('/track', [WebsiteAnalystics::class, 'track'])->name('track');
        });

        Route::prefix('vcs')->name('vcs.')->group(function () {
            Route::get('/', [VCSController::class, 'index'])->name('index');
            Route::get('/merge/{commitId}/{branch?}', [VCSController::class, 'merge'])->name('merge');
        });

        Route::prefix('popup')->name('popup.')->group(function () {
            Route::get('/', [PopupController::class, 'index'])->name('index');
            Route::get('/create', [PopupController::class, 'create'])->name('create');
            Route::get('/edit/{popup}', [PopupController::class, 'edit'])->name('edit');
            Route::post('/store', [PopupController::class, 'store'])->name('store');
            Route::post('/delete/{popup}', [PopupController::class, 'delete'])->name('delete');
            Route::get('/search-posts', [PopupController::class, 'searchPosts'])->name('search_posts');
        });

        Route::prefix('redirect')->name('redirect.')->group(function () {
            Route::get('/', [RedirectController::class, 'index'])->name('index');
            Route::post('/store', [RedirectController::class, 'store'])->name('store');
            Route::post('/import', [RedirectController::class, 'import'])->name('import');
            Route::get('/export', [RedirectController::class, 'export'])->name('export');
            Route::get('/delete/{redirect}', [RedirectController::class, 'delete'])->name('delete');
        });

        Route::prefix('cookie')->name('cookie.')->group(function () {
            Route::get('/', [CookieConsentController::class, 'index'])->name('index');
            Route::post('/store', [CookieConsentController::class, 'store'])->name('store');
        });
    }
);
