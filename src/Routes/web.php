<?php

use Illuminate\Support\Facades\Route;
use JsPress\JsPress\Http\Controllers\Frontend\CategoryController;
use JsPress\JsPress\Http\Controllers\Frontend\HomepageController;
use JsPress\JsPress\Http\Controllers\Frontend\PostController;
use JsPress\JsPress\Http\Controllers\Frontend\SitemapController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

if(config('jspress.sitemap')){
    Route::get('sitemap.xml', [SitemapController::class, 'index'])->name('sitemap');
    Route::get('{locale}-{slug}/sitemap.xml', [SitemapController::class, 'sitemaps'])->name('sitemap.post_type');
}
Route::group([
    'prefix' => JsPressLocalization::setLocale(),
    'middleware' => ['localizationRedirect', 'localize']
],function ()  {
    $post_routes = JsPressPanel::getPostTypeRoutes();

    Route::get('/', [HomepageController::class, 'index'])->name('homepage');
    foreach($post_routes as $route){
        if($route['type'] == 'post'){
            Route::get($route['route'], [PostController::class, 'post'])->name($route['name']);
        }
        if($route['type'] == 'category'){
            Route::get($route['route'], [CategoryController::class, 'index'])->name($route['name']);
        }

    }
    Route::get('/{slug}', [PostController::class, 'post'])->name('post');
    Route::get('/{parent_slug}/{slug}', [PostController::class, 'subPost'])->name('subPost')->where('parent_slug', '^(?!.*js-admin).*$');
});

