<?php

namespace JsPress\JsPress\Traits;


use JsPress\JsPress\Models\PostTypePermission;
use Spatie\Permission\Models\Role;

trait AdminTraits{


    /**
     * @return string
     */
    public function direction(): string
    {
        return $this->rtl == 1 ? "rtl" : "ltr";
    }

    /**
     * @return string
     */
    public function theme_color(): string
    {
        return $this->theme == 1 ? "dark" : "light";
    }

    /**
     * @param string $permission
     * @param int $post_type_id
     * @return bool
     */
    public function hasPostPermission(string $permission, int $post_type_id): bool
    {
        $role = Role::where('name', $this->roleName())->first();
        $permission = PostTypePermission::where('role_id', $role->id)
            ->where('permission', $permission)
            ->where('post_type_id', $post_type_id)
            ->first();
        return (bool)$permission;
    }

    /**
     * @return bool
     */
    public function hasAdvancedSettingsPermission(): bool
    {
        return $this->hasRole('Super-Admin')
            ||
            (
                $this->hasPermissionTo('view system_settings') ||
                $this->hasPermissionTo('view redirects') ||
                $this->hasPermissionTo('view error') ||
                $this->hasPermissionTo('view cookie')
            );
    }

}
