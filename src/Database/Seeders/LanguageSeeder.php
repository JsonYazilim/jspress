<?php

namespace JsPress\JsPress\Database\Seeders;

use Illuminate\Database\Seeder;

class LanguageSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {


        \DB::table('languages')->delete();

        \DB::table('languages')->insert(array (
            0 =>
            array (
                'id' => 1,
                'locale' => 'en',
                'english_name' => 'English',
                'status' => 0,
                'default_locale' => 'en_US',
                'is_default' => 0
            ),
            1 =>
            array (
                'id' => 2,
                'locale' => 'es',
                'english_name' => 'Spanish',
                'status' => 0,
                'default_locale' => 'es_ES',
                'is_default' => 0
            ),
            2 =>
            array (
                'id' => 3,
                'locale' => 'de',
                'english_name' => 'German',
                'status' => 0,
                'default_locale' => 'de_DE',
                'is_default' => 0
            ),
            3 =>
            array (
                'id' => 4,
                'locale' => 'fr',
                'english_name' => 'French',
                'status' => 0,
                'default_locale' => 'fr_FR',
                'is_default' => 0
            ),
            4 =>
            array (
                'id' => 5,
                'locale' => 'ar',
                'english_name' => 'Arabic',
                'status' => 0,
                'default_locale' => 'ar',
                'is_default' => 0
            ),
            5 =>
            array (
                'id' => 6,
                'locale' => 'bs',
                'english_name' => 'Bosnian',
                'status' => 0,
                'default_locale' => 'bs_BA',
                'is_default' => 0
            ),
            6 =>
            array (
                'id' => 7,
                'locale' => 'bg',
                'english_name' => 'Bulgarian',
                'status' => 0,
                'default_locale' => 'bg_BG',
                'is_default' => 0
            ),
            7 =>
            array (
                'id' => 8,
                'locale' => 'ca',
                'english_name' => 'Catalan',
                'status' => 0,
                'default_locale' => 'ca',
                'is_default' => 0
            ),
            8 =>
            array (
                'id' => 9,
                'locale' => 'cs',
                'english_name' => 'Czech',
                'status' => 0,
                'default_locale' => 'cs_CZ',
                'is_default' => 0
            ),
            9 =>
            array (
                'id' => 10,
                'locale' => 'sk',
                'english_name' => 'Slovak',
                'status' => 0,
                'default_locale' => 'sk_SK',
                'is_default' => 0
            ),
            10 =>
            array (
                'id' => 11,
                'locale' => 'cy',
                'english_name' => 'Welsh',
                'status' => 0,
                'default_locale' => 'cy_GB',
                'is_default' => 0
            ),
            11 =>
            array (
                'id' => 12,
                'locale' => 'da',
                'english_name' => 'Danish',
                'status' => 0,
                'default_locale' => 'da_DK',
                'is_default' => 0
            ),
            12 =>
            array (
                'id' => 13,
                'locale' => 'el',
                'english_name' => 'Greek',
                'status' => 0,
                'default_locale' => 'el',
                'is_default' => 0
            ),
            13 =>
            array (
                'id' => 14,
                'locale' => 'eo',
                'english_name' => 'Esperanto',
                'status' => 0,
                'default_locale' => 'eo_UY',
                'is_default' => 0
            ),
            14 =>
            array (
                'id' => 15,
                'locale' => 'et',
                'english_name' => 'Estonian',
                'status' => 0,
                'default_locale' => 'et',
                'is_default' => 0
            ),
            15 =>
            array (
                'id' => 16,
                'locale' => 'eu',
                'english_name' => 'Basque',
                'status' => 0,
                'default_locale' => 'eu_ES',
                'is_default' => 0
            ),
            16 =>
            array (
                'id' => 17,
                'locale' => 'fa',
                'english_name' => 'Persian',
                'status' => 0,
                'default_locale' => 'fa_IR',
                'is_default' => 0
            ),
            17 =>
            array (
                'id' => 18,
                'locale' => 'fi',
                'english_name' => 'Finnish',
                'status' => 0,
                'default_locale' => 'fi',
                'is_default' => 0
            ),
            18 =>
            array (
                'id' => 19,
                'locale' => 'ga',
                'english_name' => 'Irish',
                'status' => 0,
                'default_locale' => 'ga_IE',
                'is_default' => 0
            ),
            19 =>
            array (
                'id' => 20,
                'locale' => 'he',
                'english_name' => 'Hebrew',
                'status' => 0,
                'default_locale' => 'he_IL',
                'is_default' => 0
            ),
            20 =>
            array (
                'id' => 21,
                'locale' => 'hi',
                'english_name' => 'Hindi',
                'status' => 0,
                'default_locale' => 'hi_IN',
                'is_default' => 0
            ),
            21 =>
            array (
                'id' => 22,
                'locale' => 'hr',
                'english_name' => 'Croatian',
                'status' => 0,
                'default_locale' => 'hr',
                'is_default' => 0
            ),
            22 =>
            array (
                'id' => 23,
                'locale' => 'hu',
                'english_name' => 'Hungarian',
                'status' => 0,
                'default_locale' => 'hu_HU',
                'is_default' => 0
            ),
            23 =>
            array (
                'id' => 24,
                'locale' => 'hy',
                'english_name' => 'Armenian',
                'status' => 0,
                'default_locale' => 'hy_AM',
                'is_default' => 0
            ),
            24 =>
            array (
                'id' => 25,
                'locale' => 'id',
                'english_name' => 'Indonesian',
                'status' => 0,
                'default_locale' => 'id_ID',
                'is_default' => 0
            ),
            25 =>
            array (
                'id' => 26,
                'locale' => 'is',
                'english_name' => 'Icelandic',
                'status' => 0,
                'default_locale' => 'is_IS',
                'is_default' => 0
            ),
            26 =>
            array (
                'id' => 27,
                'locale' => 'it',
                'english_name' => 'Italian',
                'status' => 0,
                'default_locale' => 'it_IT',
                'is_default' => 0
            ),
            27 =>
            array (
                'id' => 28,
                'locale' => 'ja',
                'english_name' => 'Japanese',
                'status' => 0,
                'default_locale' => 'ja',
                'is_default' => 0
            ),
            28 =>
            array (
                'id' => 29,
                'locale' => 'ko',
                'english_name' => 'Korean',
                'status' => 0,
                'default_locale' => 'ko_KR',
                'is_default' => 0
            ),
            29 =>
            array (
                'id' => 30,
                'locale' => 'ku',
                'english_name' => 'Kurdish',
                'status' => 0,
                'default_locale' => 'ckb',
                'is_default' => 0
            ),
            30 =>
            array (
                'id' => 31,
                'locale' => 'lv',
                'english_name' => 'Latvian',
                'status' => 0,
                'default_locale' => 'lv_LV',
                'is_default' => 0
            ),
            31 =>
            array (
                'id' => 32,
                'locale' => 'lt',
                'english_name' => 'Lithuanian',
                'status' => 0,
                'default_locale' => 'lt_LT',
                'is_default' => 0
            ),
            32 =>
            array (
                'id' => 33,
                'locale' => 'mk',
                'english_name' => 'Macedonian',
                'status' => 0,
                'default_locale' => 'mk_MK',
                'is_default' => 0
            ),
            33 =>
            array (
                'id' => 34,
                'locale' => 'mt',
                'english_name' => 'Maltese',
                'status' => 0,
                'default_locale' => 'mt_MT',
                'is_default' => 0
            ),
            34 =>
            array (
                'id' => 35,
                'locale' => 'mn',
                'english_name' => 'Mongolian',
                'status' => 0,
                'default_locale' => 'mn_MN',
                'is_default' => 0
            ),
            35 =>
            array (
                'id' => 36,
                'locale' => 'ne',
                'english_name' => 'Nepali',
                'status' => 0,
                'default_locale' => 'ne',
                'is_default' => 0
            ),
            36 =>
            array (
                'id' => 37,
                'locale' => 'nl',
                'english_name' => 'Dutch',
                'status' => 0,
                'default_locale' => 'nl_NL',
                'is_default' => 0
            ),
            37 =>
            array (
                'id' => 38,
                'locale' => 'no',
                'english_name' => 'Norwegian Bokmål',
                'status' => 0,
                'default_locale' => 'nb_NO',
                'is_default' => 0
            ),
            38 =>
            array (
                'id' => 39,
                'locale' => 'pa',
                'english_name' => 'Punjabi',
                'status' => 0,
                'default_locale' => 'pa_IN',
                'is_default' => 0
            ),
            39 =>
            array (
                'id' => 40,
                'locale' => 'pl',
                'english_name' => 'Polish',
                'status' => 0,
                'default_locale' => 'pl_PL',
                'is_default' => 0
            ),
            40 =>
            array (
                'id' => 41,
                'locale' => 'pt-pt',
                'english_name' => 'Portuguese, Portugal',
                'status' => 0,
                'default_locale' => 'pt_PT',
                'is_default' => 0
            ),
            41 =>
            array (
                'id' => 42,
                'locale' => 'pt-br',
                'english_name' => 'Portuguese, Brazil',
                'status' => 0,
                'default_locale' => 'pt_BR',
                'is_default' => 0
            ),
            42 =>
            array (
                'id' => 43,
                'locale' => 'qu',
                'english_name' => 'Quechua',
                'status' => 0,
                'default_locale' => 'quz_PE',
                'is_default' => 0
            ),
            43 =>
            array (
                'id' => 44,
                'locale' => 'ro',
                'english_name' => 'Romanian',
                'status' => 0,
                'default_locale' => 'ro_RO',
                'is_default' => 0
            ),
            44 =>
            array (
                'id' => 45,
                'locale' => 'ru',
                'english_name' => 'Russian',
                'status' => 0,
                'default_locale' => 'ru_RU',
                'is_default' => 0
            ),
            45 =>
            array (
                'id' => 46,
                'locale' => 'sl',
                'english_name' => 'Slovenian',
                'status' => 0,
                'default_locale' => 'sl_SI',
                'is_default' => 0
            ),
            46 =>
            array (
                'id' => 47,
                'locale' => 'so',
                'english_name' => 'Somali',
                'status' => 0,
                'default_locale' => 'so_SO',
                'is_default' => 0
            ),
            47 =>
            array (
                'id' => 48,
                'locale' => 'sq',
                'english_name' => 'Albanian',
                'status' => 0,
                'default_locale' => 'sq_AL',
                'is_default' => 0
            ),
            48 =>
            array (
                'id' => 49,
                'locale' => 'sr',
                'english_name' => 'Serbian',
                'status' => 0,
                'default_locale' => 'sr_RS',
                'is_default' => 0
            ),
            49 =>
            array (
                'id' => 50,
                'locale' => 'sv',
                'english_name' => 'Swedish',
                'status' => 0,
                'default_locale' => 'sv_SE',
                'is_default' => 0
            ),
            50 =>
            array (
                'id' => 51,
                'locale' => 'ta',
                'english_name' => 'Tamil',
                'status' => 0,
                'default_locale' => 'ta_IN',
                'is_default' => 0
            ),
            51 =>
            array (
                'id' => 52,
                'locale' => 'th',
                'english_name' => 'Thai',
                'status' => 0,
                'default_locale' => 'th',
                'is_default' => 0
            ),
            52 =>
            array (
                'id' => 53,
                'locale' => 'tr',
                'english_name' => 'Turkish',
                'status' => 1,
                'default_locale' => 'tr_TR',
                'is_default' => 1
            ),
            53 =>
            array (
                'id' => 54,
                'locale' => 'uk',
                'english_name' => 'Ukrainian',
                'status' => 0,
                'default_locale' => 'uk',
                'is_default' => 0
            ),
            54 =>
            array (
                'id' => 55,
                'locale' => 'ur',
                'english_name' => 'Urdu',
                'status' => 0,
                'default_locale' => 'ur',
                'is_default' => 0
            ),
            55 =>
            array (
                'id' => 56,
                'locale' => 'uz',
                'english_name' => 'Uzbek',
                'status' => 0,
                'default_locale' => 'uz_UZ',
                'is_default' => 0
            ),
            56 =>
            array (
                'id' => 57,
                'locale' => 'vi',
                'english_name' => 'Vietnamese',
                'status' => 0,
                'default_locale' => 'vi_VN',
                'is_default' => 0
            ),
            57 =>
            array (
                'id' => 58,
                'locale' => 'yi',
                'english_name' => 'Yiddish',
                'status' => 0,
                'default_locale' => '',
                'is_default' => 0
            ),
            58 =>
            array (
                'id' => 59,
                'locale' => 'zh-hans',
                'english_name' => 'Chinese (Simplified)',
                'status' => 0,
                'default_locale' => 'zh_CN',
                'is_default' => 0
            ),
            59 =>
            array (
                'id' => 60,
                'locale' => 'zu',
                'english_name' => 'Zulu',
                'status' => 0,
                'default_locale' => '',
                'is_default' => 0
            ),
            60 =>
            array (
                'id' => 61,
                'locale' => 'zh-hant',
                'english_name' => 'Chinese (Traditional)',
                'status' => 0,
                'default_locale' => 'zh_TW',
                'is_default' => 0
            ),
            61 =>
            array (
                'id' => 62,
                'locale' => 'ms',
                'english_name' => 'Malay',
                'status' => 0,
                'default_locale' => 'ms_MY',
                'is_default' => 0
            ),
            62 =>
            array (
                'id' => 63,
                'locale' => 'gl',
                'english_name' => 'Galician',
                'status' => 0,
                'default_locale' => 'gl_ES',
                'is_default' => 0
            ),
            63 =>
            array (
                'id' => 64,
                'locale' => 'bn',
                'english_name' => 'Bengali',
                'status' => 0,
                'default_locale' => 'bn_BD',
                'is_default' => 0
            ),
            64 =>
            array (
                'id' => 65,
                'locale' => 'az',
                'english_name' => 'Azerbaijani',
                'status' => 0,
                'default_locale' => 'az',
                'is_default' => 0
            ),
        ));


    }
}
