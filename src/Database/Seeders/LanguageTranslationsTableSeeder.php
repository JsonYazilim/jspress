<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: LanguageTranslationsTableSeeder.php
 * Author: Json Yazılım
 * Class: LanguageTranslationsTableSeeder.php
 * Current Username: Erdinc
 * Last Modified: 28.05.2023 15:25
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Database\Seeders;

use Illuminate\Database\Seeder;

class LanguageTranslationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run(): void
    {


        \DB::table('language_translations')->delete();

        \DB::table('language_translations')->insert(array (
            0 =>
            array (
                'id' => 1,
                'language_code' => 'en',
                'display_language_code' => 'en',
                'name' => 'English',
            ),
            1 =>
            array (
                'id' => 2,
                'language_code' => 'en',
                'display_language_code' => 'es',
                'name' => 'Inglés',
            ),
            2 =>
            array (
                'id' => 3,
                'language_code' => 'en',
                'display_language_code' => 'de',
                'name' => 'Englisch',
            ),
            3 =>
            array (
                'id' => 4,
                'language_code' => 'en',
                'display_language_code' => 'fr',
                'name' => 'Anglais',
            ),
            4 =>
            array (
                'id' => 5,
                'language_code' => 'en',
                'display_language_code' => 'ar',
                'name' => 'الإنجليزية',
            ),
            5 =>
            array (
                'id' => 6,
                'language_code' => 'en',
                'display_language_code' => 'bs',
                'name' => 'English',
            ),
            6 =>
            array (
                'id' => 7,
                'language_code' => 'en',
                'display_language_code' => 'bg',
                'name' => 'Английски',
            ),
            7 =>
            array (
                'id' => 8,
                'language_code' => 'en',
                'display_language_code' => 'ca',
                'name' => 'English',
            ),
            8 =>
            array (
                'id' => 9,
                'language_code' => 'en',
                'display_language_code' => 'cs',
                'name' => 'Angličtina',
            ),
            9 =>
            array (
                'id' => 10,
                'language_code' => 'en',
                'display_language_code' => 'sk',
                'name' => 'Angličtina',
            ),
            10 =>
            array (
                'id' => 11,
                'language_code' => 'en',
                'display_language_code' => 'cy',
                'name' => 'English',
            ),
            11 =>
            array (
                'id' => 12,
                'language_code' => 'en',
                'display_language_code' => 'da',
                'name' => 'English',
            ),
            12 =>
            array (
                'id' => 13,
                'language_code' => 'en',
                'display_language_code' => 'el',
                'name' => 'Αγγλικά',
            ),
            13 =>
            array (
                'id' => 14,
                'language_code' => 'en',
                'display_language_code' => 'eo',
                'name' => 'English',
            ),
            14 =>
            array (
                'id' => 15,
                'language_code' => 'en',
                'display_language_code' => 'et',
                'name' => 'English',
            ),
            15 =>
            array (
                'id' => 16,
                'language_code' => 'en',
                'display_language_code' => 'eu',
                'name' => 'English',
            ),
            16 =>
            array (
                'id' => 17,
                'language_code' => 'en',
                'display_language_code' => 'fa',
                'name' => 'English',
            ),
            17 =>
            array (
                'id' => 18,
                'language_code' => 'en',
                'display_language_code' => 'fi',
                'name' => 'Englanti',
            ),
            18 =>
            array (
                'id' => 19,
                'language_code' => 'en',
                'display_language_code' => 'ga',
                'name' => 'English',
            ),
            19 =>
            array (
                'id' => 20,
                'language_code' => 'en',
                'display_language_code' => 'he',
                'name' => 'אנגלית',
            ),
            20 =>
            array (
                'id' => 21,
                'language_code' => 'en',
                'display_language_code' => 'hi',
                'name' => 'English',
            ),
            21 =>
            array (
                'id' => 22,
                'language_code' => 'en',
                'display_language_code' => 'hr',
                'name' => 'Engleski',
            ),
            22 =>
            array (
                'id' => 23,
                'language_code' => 'en',
                'display_language_code' => 'hu',
                'name' => 'Angol',
            ),
            23 =>
            array (
                'id' => 24,
                'language_code' => 'en',
                'display_language_code' => 'hy',
                'name' => 'English',
            ),
            24 =>
            array (
                'id' => 25,
                'language_code' => 'en',
                'display_language_code' => 'id',
                'name' => 'English',
            ),
            25 =>
            array (
                'id' => 26,
                'language_code' => 'en',
                'display_language_code' => 'is',
                'name' => 'English',
            ),
            26 =>
            array (
                'id' => 27,
                'language_code' => 'en',
                'display_language_code' => 'it',
                'name' => 'Inglese',
            ),
            27 =>
            array (
                'id' => 28,
                'language_code' => 'en',
                'display_language_code' => 'ja',
                'name' => '英語',
            ),
            28 =>
            array (
                'id' => 29,
                'language_code' => 'en',
                'display_language_code' => 'ko',
                'name' => '영어',
            ),
            29 =>
            array (
                'id' => 30,
                'language_code' => 'en',
                'display_language_code' => 'ku',
                'name' => 'English',
            ),
            30 =>
            array (
                'id' => 31,
                'language_code' => 'en',
                'display_language_code' => 'lv',
                'name' => 'English',
            ),
            31 =>
            array (
                'id' => 32,
                'language_code' => 'en',
                'display_language_code' => 'lt',
                'name' => 'English',
            ),
            32 =>
            array (
                'id' => 33,
                'language_code' => 'en',
                'display_language_code' => 'mk',
                'name' => 'English',
            ),
            33 =>
            array (
                'id' => 34,
                'language_code' => 'en',
                'display_language_code' => 'mt',
                'name' => 'English',
            ),
            34 =>
            array (
                'id' => 35,
                'language_code' => 'en',
                'display_language_code' => 'mn',
                'name' => 'English',
            ),
            35 =>
            array (
                'id' => 36,
                'language_code' => 'en',
                'display_language_code' => 'ne',
                'name' => 'English',
            ),
            36 =>
            array (
                'id' => 37,
                'language_code' => 'en',
                'display_language_code' => 'nl',
                'name' => 'Engels',
            ),
            37 =>
            array (
                'id' => 38,
                'language_code' => 'en',
                'display_language_code' => 'no',
                'name' => 'Engelsk',
            ),
            38 =>
            array (
                'id' => 39,
                'language_code' => 'en',
                'display_language_code' => 'pa',
                'name' => 'English',
            ),
            39 =>
            array (
                'id' => 40,
                'language_code' => 'en',
                'display_language_code' => 'pl',
                'name' => 'Angielski',
            ),
            40 =>
            array (
                'id' => 41,
                'language_code' => 'en',
                'display_language_code' => 'pt-pt',
                'name' => 'Inglês',
            ),
            41 =>
            array (
                'id' => 42,
                'language_code' => 'en',
                'display_language_code' => 'pt-br',
                'name' => 'Inglês',
            ),
            42 =>
            array (
                'id' => 43,
                'language_code' => 'en',
                'display_language_code' => 'qu',
                'name' => 'English',
            ),
            43 =>
            array (
                'id' => 44,
                'language_code' => 'en',
                'display_language_code' => 'ro',
                'name' => 'Engleză',
            ),
            44 =>
            array (
                'id' => 45,
                'language_code' => 'en',
                'display_language_code' => 'ru',
                'name' => 'Английский',
            ),
            45 =>
            array (
                'id' => 46,
                'language_code' => 'en',
                'display_language_code' => 'sl',
                'name' => 'Angleščina',
            ),
            46 =>
            array (
                'id' => 47,
                'language_code' => 'en',
                'display_language_code' => 'so',
                'name' => 'English',
            ),
            47 =>
            array (
                'id' => 48,
                'language_code' => 'en',
                'display_language_code' => 'sq',
                'name' => 'English',
            ),
            48 =>
            array (
                'id' => 49,
                'language_code' => 'en',
                'display_language_code' => 'sr',
                'name' => 'енглески',
            ),
            49 =>
            array (
                'id' => 50,
                'language_code' => 'en',
                'display_language_code' => 'sv',
                'name' => 'Engelska',
            ),
            50 =>
            array (
                'id' => 51,
                'language_code' => 'en',
                'display_language_code' => 'ta',
                'name' => 'English',
            ),
            51 =>
            array (
                'id' => 52,
                'language_code' => 'en',
                'display_language_code' => 'th',
                'name' => 'อังกฤษ',
            ),
            52 =>
            array (
                'id' => 53,
                'language_code' => 'en',
                'display_language_code' => 'tr',
                'name' => 'İngilizce',
            ),
            53 =>
            array (
                'id' => 54,
                'language_code' => 'en',
                'display_language_code' => 'uk',
                'name' => 'English',
            ),
            54 =>
            array (
                'id' => 55,
                'language_code' => 'en',
                'display_language_code' => 'ur',
                'name' => 'English',
            ),
            55 =>
            array (
                'id' => 56,
                'language_code' => 'en',
                'display_language_code' => 'uz',
                'name' => 'English',
            ),
            56 =>
            array (
                'id' => 57,
                'language_code' => 'en',
                'display_language_code' => 'vi',
                'name' => 'English',
            ),
            57 =>
            array (
                'id' => 58,
                'language_code' => 'en',
                'display_language_code' => 'yi',
                'name' => 'English',
            ),
            58 =>
            array (
                'id' => 59,
                'language_code' => 'en',
                'display_language_code' => 'zh-hans',
                'name' => '英语',
            ),
            59 =>
            array (
                'id' => 60,
                'language_code' => 'en',
                'display_language_code' => 'zu',
                'name' => 'English',
            ),
            60 =>
            array (
                'id' => 61,
                'language_code' => 'en',
                'display_language_code' => 'zh-hant',
                'name' => '英語',
            ),
            61 =>
            array (
                'id' => 62,
                'language_code' => 'en',
                'display_language_code' => 'ms',
                'name' => 'English',
            ),
            62 =>
            array (
                'id' => 63,
                'language_code' => 'en',
                'display_language_code' => 'gl',
                'name' => 'English',
            ),
            63 =>
            array (
                'id' => 64,
                'language_code' => 'en',
                'display_language_code' => 'bn',
                'name' => 'English',
            ),
            64 =>
            array (
                'id' => 65,
                'language_code' => 'en',
                'display_language_code' => 'az',
                'name' => 'Ingilis',
            ),
            65 =>
            array (
                'id' => 66,
                'language_code' => 'es',
                'display_language_code' => 'en',
                'name' => 'Spanish',
            ),
            66 =>
            array (
                'id' => 67,
                'language_code' => 'es',
                'display_language_code' => 'es',
                'name' => 'Español',
            ),
            67 =>
            array (
                'id' => 68,
                'language_code' => 'es',
                'display_language_code' => 'de',
                'name' => 'Spanisch',
            ),
            68 =>
            array (
                'id' => 69,
                'language_code' => 'es',
                'display_language_code' => 'fr',
                'name' => 'Espagnol',
            ),
            69 =>
            array (
                'id' => 70,
                'language_code' => 'es',
                'display_language_code' => 'ar',
                'name' => 'الأسبانية',
            ),
            70 =>
            array (
                'id' => 71,
                'language_code' => 'es',
                'display_language_code' => 'bs',
                'name' => 'Spanish',
            ),
            71 =>
            array (
                'id' => 72,
                'language_code' => 'es',
                'display_language_code' => 'bg',
                'name' => 'Испански',
            ),
            72 =>
            array (
                'id' => 73,
                'language_code' => 'es',
                'display_language_code' => 'ca',
                'name' => 'Spanish',
            ),
            73 =>
            array (
                'id' => 74,
                'language_code' => 'es',
                'display_language_code' => 'cs',
                'name' => 'Španělský',
            ),
            74 =>
            array (
                'id' => 75,
                'language_code' => 'es',
                'display_language_code' => 'sk',
                'name' => 'Španielčina',
            ),
            75 =>
            array (
                'id' => 76,
                'language_code' => 'es',
                'display_language_code' => 'cy',
                'name' => 'Spanish',
            ),
            76 =>
            array (
                'id' => 77,
                'language_code' => 'es',
                'display_language_code' => 'da',
                'name' => 'Spanish',
            ),
            77 =>
            array (
                'id' => 78,
                'language_code' => 'es',
                'display_language_code' => 'el',
                'name' => 'Ισπανικά',
            ),
            78 =>
            array (
                'id' => 79,
                'language_code' => 'es',
                'display_language_code' => 'eo',
                'name' => 'Spanish',
            ),
            79 =>
            array (
                'id' => 80,
                'language_code' => 'es',
                'display_language_code' => 'et',
                'name' => 'Spanish',
            ),
            80 =>
            array (
                'id' => 81,
                'language_code' => 'es',
                'display_language_code' => 'eu',
                'name' => 'Spanish',
            ),
            81 =>
            array (
                'id' => 82,
                'language_code' => 'es',
                'display_language_code' => 'fa',
                'name' => 'Spanish',
            ),
            82 =>
            array (
                'id' => 83,
                'language_code' => 'es',
                'display_language_code' => 'fi',
                'name' => 'Espanja',
            ),
            83 =>
            array (
                'id' => 84,
                'language_code' => 'es',
                'display_language_code' => 'ga',
                'name' => 'Spanish',
            ),
            84 =>
            array (
                'id' => 85,
                'language_code' => 'es',
                'display_language_code' => 'he',
                'name' => 'ספרדית',
            ),
            85 =>
            array (
                'id' => 86,
                'language_code' => 'es',
                'display_language_code' => 'hi',
                'name' => 'Spanish',
            ),
            86 =>
            array (
                'id' => 87,
                'language_code' => 'es',
                'display_language_code' => 'hr',
                'name' => 'španjolski',
            ),
            87 =>
            array (
                'id' => 88,
                'language_code' => 'es',
                'display_language_code' => 'hu',
                'name' => 'Spanyol',
            ),
            88 =>
            array (
                'id' => 89,
                'language_code' => 'es',
                'display_language_code' => 'hy',
                'name' => 'Spanish',
            ),
            89 =>
            array (
                'id' => 90,
                'language_code' => 'es',
                'display_language_code' => 'id',
                'name' => 'Spanish',
            ),
            90 =>
            array (
                'id' => 91,
                'language_code' => 'es',
                'display_language_code' => 'is',
                'name' => 'Spanish',
            ),
            91 =>
            array (
                'id' => 92,
                'language_code' => 'es',
                'display_language_code' => 'it',
                'name' => 'Spagnolo',
            ),
            92 =>
            array (
                'id' => 93,
                'language_code' => 'es',
                'display_language_code' => 'ja',
                'name' => 'スペイン語',
            ),
            93 =>
            array (
                'id' => 94,
                'language_code' => 'es',
                'display_language_code' => 'ko',
                'name' => '스페인어',
            ),
            94 =>
            array (
                'id' => 95,
                'language_code' => 'es',
                'display_language_code' => 'ku',
                'name' => 'Spanish',
            ),
            95 =>
            array (
                'id' => 96,
                'language_code' => 'es',
                'display_language_code' => 'lv',
                'name' => 'Spanish',
            ),
            96 =>
            array (
                'id' => 97,
                'language_code' => 'es',
                'display_language_code' => 'lt',
                'name' => 'Spanish',
            ),
            97 =>
            array (
                'id' => 98,
                'language_code' => 'es',
                'display_language_code' => 'mk',
                'name' => 'Spanish',
            ),
            98 =>
            array (
                'id' => 99,
                'language_code' => 'es',
                'display_language_code' => 'mt',
                'name' => 'Spanish',
            ),
            99 =>
            array (
                'id' => 100,
                'language_code' => 'es',
                'display_language_code' => 'mn',
                'name' => 'Spanish',
            ),
            100 =>
            array (
                'id' => 101,
                'language_code' => 'es',
                'display_language_code' => 'ne',
                'name' => 'Spanish',
            ),
            101 =>
            array (
                'id' => 102,
                'language_code' => 'es',
                'display_language_code' => 'nl',
                'name' => 'Spaans',
            ),
            102 =>
            array (
                'id' => 103,
                'language_code' => 'es',
                'display_language_code' => 'no',
                'name' => 'Spansk',
            ),
            103 =>
            array (
                'id' => 104,
                'language_code' => 'es',
                'display_language_code' => 'pa',
                'name' => 'Spanish',
            ),
            104 =>
            array (
                'id' => 105,
                'language_code' => 'es',
                'display_language_code' => 'pl',
                'name' => 'Hiszpański',
            ),
            105 =>
            array (
                'id' => 106,
                'language_code' => 'es',
                'display_language_code' => 'pt-pt',
                'name' => 'Espanhol',
            ),
            106 =>
            array (
                'id' => 107,
                'language_code' => 'es',
                'display_language_code' => 'pt-br',
                'name' => 'Espanhol',
            ),
            107 =>
            array (
                'id' => 108,
                'language_code' => 'es',
                'display_language_code' => 'qu',
                'name' => 'Spanish',
            ),
            108 =>
            array (
                'id' => 109,
                'language_code' => 'es',
                'display_language_code' => 'ro',
                'name' => 'Spaniolă',
            ),
            109 =>
            array (
                'id' => 110,
                'language_code' => 'es',
                'display_language_code' => 'ru',
                'name' => 'Испанский',
            ),
            110 =>
            array (
                'id' => 111,
                'language_code' => 'es',
                'display_language_code' => 'sl',
                'name' => 'Španščina',
            ),
            111 =>
            array (
                'id' => 112,
                'language_code' => 'es',
                'display_language_code' => 'so',
                'name' => 'Spanish',
            ),
            112 =>
            array (
                'id' => 113,
                'language_code' => 'es',
                'display_language_code' => 'sq',
                'name' => 'Spanish',
            ),
            113 =>
            array (
                'id' => 114,
                'language_code' => 'es',
                'display_language_code' => 'sr',
                'name' => 'шпански',
            ),
            114 =>
            array (
                'id' => 115,
                'language_code' => 'es',
                'display_language_code' => 'sv',
                'name' => 'Spanska',
            ),
            115 =>
            array (
                'id' => 116,
                'language_code' => 'es',
                'display_language_code' => 'ta',
                'name' => 'Spanish',
            ),
            116 =>
            array (
                'id' => 117,
                'language_code' => 'es',
                'display_language_code' => 'th',
                'name' => 'สเปน',
            ),
            117 =>
            array (
                'id' => 118,
                'language_code' => 'es',
                'display_language_code' => 'tr',
                'name' => 'İspanyolca',
            ),
            118 =>
            array (
                'id' => 119,
                'language_code' => 'es',
                'display_language_code' => 'uk',
                'name' => 'Spanish',
            ),
            119 =>
            array (
                'id' => 120,
                'language_code' => 'es',
                'display_language_code' => 'ur',
                'name' => 'Spanish',
            ),
            120 =>
            array (
                'id' => 121,
                'language_code' => 'es',
                'display_language_code' => 'uz',
                'name' => 'Spanish',
            ),
            121 =>
            array (
                'id' => 122,
                'language_code' => 'es',
                'display_language_code' => 'vi',
                'name' => 'Spanish',
            ),
            122 =>
            array (
                'id' => 123,
                'language_code' => 'es',
                'display_language_code' => 'yi',
                'name' => 'Spanish',
            ),
            123 =>
            array (
                'id' => 124,
                'language_code' => 'es',
                'display_language_code' => 'zh-hans',
                'name' => '西班牙语',
            ),
            124 =>
            array (
                'id' => 125,
                'language_code' => 'es',
                'display_language_code' => 'zu',
                'name' => 'Spanish',
            ),
            125 =>
            array (
                'id' => 126,
                'language_code' => 'es',
                'display_language_code' => 'zh-hant',
                'name' => '西班牙語',
            ),
            126 =>
            array (
                'id' => 127,
                'language_code' => 'es',
                'display_language_code' => 'ms',
                'name' => 'Spanish',
            ),
            127 =>
            array (
                'id' => 128,
                'language_code' => 'es',
                'display_language_code' => 'gl',
                'name' => 'Spanish',
            ),
            128 =>
            array (
                'id' => 129,
                'language_code' => 'es',
                'display_language_code' => 'bn',
                'name' => 'Spanish',
            ),
            129 =>
            array (
                'id' => 130,
                'language_code' => 'es',
                'display_language_code' => 'az',
                'name' => 'Spanish',
            ),
            130 =>
            array (
                'id' => 131,
                'language_code' => 'de',
                'display_language_code' => 'en',
                'name' => 'German',
            ),
            131 =>
            array (
                'id' => 132,
                'language_code' => 'de',
                'display_language_code' => 'es',
                'name' => 'Alemán',
            ),
            132 =>
            array (
                'id' => 133,
                'language_code' => 'de',
                'display_language_code' => 'de',
                'name' => 'Deutsch',
            ),
            133 =>
            array (
                'id' => 134,
                'language_code' => 'de',
                'display_language_code' => 'fr',
                'name' => 'Allemand',
            ),
            134 =>
            array (
                'id' => 135,
                'language_code' => 'de',
                'display_language_code' => 'ar',
                'name' => 'الألمانية',
            ),
            135 =>
            array (
                'id' => 136,
                'language_code' => 'de',
                'display_language_code' => 'bs',
                'name' => 'German',
            ),
            136 =>
            array (
                'id' => 137,
                'language_code' => 'de',
                'display_language_code' => 'bg',
                'name' => 'Немски',
            ),
            137 =>
            array (
                'id' => 138,
                'language_code' => 'de',
                'display_language_code' => 'ca',
                'name' => 'German',
            ),
            138 =>
            array (
                'id' => 139,
                'language_code' => 'de',
                'display_language_code' => 'cs',
                'name' => 'Němec',
            ),
            139 =>
            array (
                'id' => 140,
                'language_code' => 'de',
                'display_language_code' => 'sk',
                'name' => 'Nemčina',
            ),
            140 =>
            array (
                'id' => 141,
                'language_code' => 'de',
                'display_language_code' => 'cy',
                'name' => 'German',
            ),
            141 =>
            array (
                'id' => 142,
                'language_code' => 'de',
                'display_language_code' => 'da',
                'name' => 'German',
            ),
            142 =>
            array (
                'id' => 143,
                'language_code' => 'de',
                'display_language_code' => 'el',
                'name' => 'Γερμανικά',
            ),
            143 =>
            array (
                'id' => 144,
                'language_code' => 'de',
                'display_language_code' => 'eo',
                'name' => 'German',
            ),
            144 =>
            array (
                'id' => 145,
                'language_code' => 'de',
                'display_language_code' => 'et',
                'name' => 'German',
            ),
            145 =>
            array (
                'id' => 146,
                'language_code' => 'de',
                'display_language_code' => 'eu',
                'name' => 'German',
            ),
            146 =>
            array (
                'id' => 147,
                'language_code' => 'de',
                'display_language_code' => 'fa',
                'name' => 'German',
            ),
            147 =>
            array (
                'id' => 148,
                'language_code' => 'de',
                'display_language_code' => 'fi',
                'name' => 'Saksa',
            ),
            148 =>
            array (
                'id' => 149,
                'language_code' => 'de',
                'display_language_code' => 'ga',
                'name' => 'German',
            ),
            149 =>
            array (
                'id' => 150,
                'language_code' => 'de',
                'display_language_code' => 'he',
                'name' => 'גרמנית',
            ),
            150 =>
            array (
                'id' => 151,
                'language_code' => 'de',
                'display_language_code' => 'hi',
                'name' => 'German',
            ),
            151 =>
            array (
                'id' => 152,
                'language_code' => 'de',
                'display_language_code' => 'hr',
                'name' => 'Njemački',
            ),
            152 =>
            array (
                'id' => 153,
                'language_code' => 'de',
                'display_language_code' => 'hu',
                'name' => 'Német',
            ),
            153 =>
            array (
                'id' => 154,
                'language_code' => 'de',
                'display_language_code' => 'hy',
                'name' => 'German',
            ),
            154 =>
            array (
                'id' => 155,
                'language_code' => 'de',
                'display_language_code' => 'id',
                'name' => 'German',
            ),
            155 =>
            array (
                'id' => 156,
                'language_code' => 'de',
                'display_language_code' => 'is',
                'name' => 'German',
            ),
            156 =>
            array (
                'id' => 157,
                'language_code' => 'de',
                'display_language_code' => 'it',
                'name' => 'Tedesco',
            ),
            157 =>
            array (
                'id' => 158,
                'language_code' => 'de',
                'display_language_code' => 'ja',
                'name' => 'ドイツ語',
            ),
            158 =>
            array (
                'id' => 159,
                'language_code' => 'de',
                'display_language_code' => 'ko',
                'name' => '독어',
            ),
            159 =>
            array (
                'id' => 160,
                'language_code' => 'de',
                'display_language_code' => 'ku',
                'name' => 'German',
            ),
            160 =>
            array (
                'id' => 161,
                'language_code' => 'de',
                'display_language_code' => 'lv',
                'name' => 'German',
            ),
            161 =>
            array (
                'id' => 162,
                'language_code' => 'de',
                'display_language_code' => 'lt',
                'name' => 'German',
            ),
            162 =>
            array (
                'id' => 163,
                'language_code' => 'de',
                'display_language_code' => 'mk',
                'name' => 'German',
            ),
            163 =>
            array (
                'id' => 164,
                'language_code' => 'de',
                'display_language_code' => 'mt',
                'name' => 'German',
            ),
            164 =>
            array (
                'id' => 165,
                'language_code' => 'de',
                'display_language_code' => 'mn',
                'name' => 'German',
            ),
            165 =>
            array (
                'id' => 166,
                'language_code' => 'de',
                'display_language_code' => 'ne',
                'name' => 'German',
            ),
            166 =>
            array (
                'id' => 167,
                'language_code' => 'de',
                'display_language_code' => 'nl',
                'name' => 'Duits',
            ),
            167 =>
            array (
                'id' => 168,
                'language_code' => 'de',
                'display_language_code' => 'no',
                'name' => 'Tysk',
            ),
            168 =>
            array (
                'id' => 169,
                'language_code' => 'de',
                'display_language_code' => 'pa',
                'name' => 'German',
            ),
            169 =>
            array (
                'id' => 170,
                'language_code' => 'de',
                'display_language_code' => 'pl',
                'name' => 'Niemiecki',
            ),
            170 =>
            array (
                'id' => 171,
                'language_code' => 'de',
                'display_language_code' => 'pt-pt',
                'name' => 'Alemão',
            ),
            171 =>
            array (
                'id' => 172,
                'language_code' => 'de',
                'display_language_code' => 'pt-br',
                'name' => 'Alemão',
            ),
            172 =>
            array (
                'id' => 173,
                'language_code' => 'de',
                'display_language_code' => 'qu',
                'name' => 'German',
            ),
            173 =>
            array (
                'id' => 174,
                'language_code' => 'de',
                'display_language_code' => 'ro',
                'name' => 'Germană',
            ),
            174 =>
            array (
                'id' => 175,
                'language_code' => 'de',
                'display_language_code' => 'ru',
                'name' => 'Немецкий',
            ),
            175 =>
            array (
                'id' => 176,
                'language_code' => 'de',
                'display_language_code' => 'sl',
                'name' => 'Nemščina',
            ),
            176 =>
            array (
                'id' => 177,
                'language_code' => 'de',
                'display_language_code' => 'so',
                'name' => 'German',
            ),
            177 =>
            array (
                'id' => 178,
                'language_code' => 'de',
                'display_language_code' => 'sq',
                'name' => 'German',
            ),
            178 =>
            array (
                'id' => 179,
                'language_code' => 'de',
                'display_language_code' => 'sr',
                'name' => 'немачки',
            ),
            179 =>
            array (
                'id' => 180,
                'language_code' => 'de',
                'display_language_code' => 'sv',
                'name' => 'Tyska',
            ),
            180 =>
            array (
                'id' => 181,
                'language_code' => 'de',
                'display_language_code' => 'ta',
                'name' => 'German',
            ),
            181 =>
            array (
                'id' => 182,
                'language_code' => 'de',
                'display_language_code' => 'th',
                'name' => 'เยอรมัน',
            ),
            182 =>
            array (
                'id' => 183,
                'language_code' => 'de',
                'display_language_code' => 'tr',
                'name' => 'Almanca',
            ),
            183 =>
            array (
                'id' => 184,
                'language_code' => 'de',
                'display_language_code' => 'uk',
                'name' => 'German',
            ),
            184 =>
            array (
                'id' => 185,
                'language_code' => 'de',
                'display_language_code' => 'ur',
                'name' => 'German',
            ),
            185 =>
            array (
                'id' => 186,
                'language_code' => 'de',
                'display_language_code' => 'uz',
                'name' => 'German',
            ),
            186 =>
            array (
                'id' => 187,
                'language_code' => 'de',
                'display_language_code' => 'vi',
                'name' => 'German',
            ),
            187 =>
            array (
                'id' => 188,
                'language_code' => 'de',
                'display_language_code' => 'yi',
                'name' => 'German',
            ),
            188 =>
            array (
                'id' => 189,
                'language_code' => 'de',
                'display_language_code' => 'zh-hans',
                'name' => '德语',
            ),
            189 =>
            array (
                'id' => 190,
                'language_code' => 'de',
                'display_language_code' => 'zu',
                'name' => 'German',
            ),
            190 =>
            array (
                'id' => 191,
                'language_code' => 'de',
                'display_language_code' => 'zh-hant',
                'name' => '德語',
            ),
            191 =>
            array (
                'id' => 192,
                'language_code' => 'de',
                'display_language_code' => 'ms',
                'name' => 'German',
            ),
            192 =>
            array (
                'id' => 193,
                'language_code' => 'de',
                'display_language_code' => 'gl',
                'name' => 'German',
            ),
            193 =>
            array (
                'id' => 194,
                'language_code' => 'de',
                'display_language_code' => 'bn',
                'name' => 'German',
            ),
            194 =>
            array (
                'id' => 195,
                'language_code' => 'de',
                'display_language_code' => 'az',
                'name' => 'German',
            ),
            195 =>
            array (
                'id' => 196,
                'language_code' => 'fr',
                'display_language_code' => 'en',
                'name' => 'French',
            ),
            196 =>
            array (
                'id' => 197,
                'language_code' => 'fr',
                'display_language_code' => 'es',
                'name' => 'Francés',
            ),
            197 =>
            array (
                'id' => 198,
                'language_code' => 'fr',
                'display_language_code' => 'de',
                'name' => 'Französisch',
            ),
            198 =>
            array (
                'id' => 199,
                'language_code' => 'fr',
                'display_language_code' => 'fr',
                'name' => 'Français',
            ),
            199 =>
            array (
                'id' => 200,
                'language_code' => 'fr',
                'display_language_code' => 'ar',
                'name' => 'الفرنسية',
            ),
            200 =>
            array (
                'id' => 201,
                'language_code' => 'fr',
                'display_language_code' => 'bs',
                'name' => 'French',
            ),
            201 =>
            array (
                'id' => 202,
                'language_code' => 'fr',
                'display_language_code' => 'bg',
                'name' => 'Френски',
            ),
            202 =>
            array (
                'id' => 203,
                'language_code' => 'fr',
                'display_language_code' => 'ca',
                'name' => 'French',
            ),
            203 =>
            array (
                'id' => 204,
                'language_code' => 'fr',
                'display_language_code' => 'cs',
                'name' => 'Francouzština',
            ),
            204 =>
            array (
                'id' => 205,
                'language_code' => 'fr',
                'display_language_code' => 'sk',
                'name' => 'Francúzština',
            ),
            205 =>
            array (
                'id' => 206,
                'language_code' => 'fr',
                'display_language_code' => 'cy',
                'name' => 'French',
            ),
            206 =>
            array (
                'id' => 207,
                'language_code' => 'fr',
                'display_language_code' => 'da',
                'name' => 'French',
            ),
            207 =>
            array (
                'id' => 208,
                'language_code' => 'fr',
                'display_language_code' => 'el',
                'name' => 'Γαλλικά',
            ),
            208 =>
            array (
                'id' => 209,
                'language_code' => 'fr',
                'display_language_code' => 'eo',
                'name' => 'French',
            ),
            209 =>
            array (
                'id' => 210,
                'language_code' => 'fr',
                'display_language_code' => 'et',
                'name' => 'French',
            ),
            210 =>
            array (
                'id' => 211,
                'language_code' => 'fr',
                'display_language_code' => 'eu',
                'name' => 'French',
            ),
            211 =>
            array (
                'id' => 212,
                'language_code' => 'fr',
                'display_language_code' => 'fa',
                'name' => 'French',
            ),
            212 =>
            array (
                'id' => 213,
                'language_code' => 'fr',
                'display_language_code' => 'fi',
                'name' => 'Ranska',
            ),
            213 =>
            array (
                'id' => 214,
                'language_code' => 'fr',
                'display_language_code' => 'ga',
                'name' => 'French',
            ),
            214 =>
            array (
                'id' => 215,
                'language_code' => 'fr',
                'display_language_code' => 'he',
                'name' => 'צרפתית',
            ),
            215 =>
            array (
                'id' => 216,
                'language_code' => 'fr',
                'display_language_code' => 'hi',
                'name' => 'French',
            ),
            216 =>
            array (
                'id' => 217,
                'language_code' => 'fr',
                'display_language_code' => 'hr',
                'name' => 'Francuski',
            ),
            217 =>
            array (
                'id' => 218,
                'language_code' => 'fr',
                'display_language_code' => 'hu',
                'name' => 'Francia',
            ),
            218 =>
            array (
                'id' => 219,
                'language_code' => 'fr',
                'display_language_code' => 'hy',
                'name' => 'French',
            ),
            219 =>
            array (
                'id' => 220,
                'language_code' => 'fr',
                'display_language_code' => 'id',
                'name' => 'French',
            ),
            220 =>
            array (
                'id' => 221,
                'language_code' => 'fr',
                'display_language_code' => 'is',
                'name' => 'French',
            ),
            221 =>
            array (
                'id' => 222,
                'language_code' => 'fr',
                'display_language_code' => 'it',
                'name' => 'Francese',
            ),
            222 =>
            array (
                'id' => 223,
                'language_code' => 'fr',
                'display_language_code' => 'ja',
                'name' => 'フランス語',
            ),
            223 =>
            array (
                'id' => 224,
                'language_code' => 'fr',
                'display_language_code' => 'ko',
                'name' => '불어',
            ),
            224 =>
            array (
                'id' => 225,
                'language_code' => 'fr',
                'display_language_code' => 'ku',
                'name' => 'French',
            ),
            225 =>
            array (
                'id' => 226,
                'language_code' => 'fr',
                'display_language_code' => 'lv',
                'name' => 'French',
            ),
            226 =>
            array (
                'id' => 227,
                'language_code' => 'fr',
                'display_language_code' => 'lt',
                'name' => 'French',
            ),
            227 =>
            array (
                'id' => 228,
                'language_code' => 'fr',
                'display_language_code' => 'mk',
                'name' => 'French',
            ),
            228 =>
            array (
                'id' => 229,
                'language_code' => 'fr',
                'display_language_code' => 'mt',
                'name' => 'French',
            ),
            229 =>
            array (
                'id' => 230,
                'language_code' => 'fr',
                'display_language_code' => 'mn',
                'name' => 'French',
            ),
            230 =>
            array (
                'id' => 231,
                'language_code' => 'fr',
                'display_language_code' => 'ne',
                'name' => 'French',
            ),
            231 =>
            array (
                'id' => 232,
                'language_code' => 'fr',
                'display_language_code' => 'nl',
                'name' => 'Frans',
            ),
            232 =>
            array (
                'id' => 233,
                'language_code' => 'fr',
                'display_language_code' => 'no',
                'name' => 'Fransk',
            ),
            233 =>
            array (
                'id' => 234,
                'language_code' => 'fr',
                'display_language_code' => 'pa',
                'name' => 'French',
            ),
            234 =>
            array (
                'id' => 235,
                'language_code' => 'fr',
                'display_language_code' => 'pl',
                'name' => 'Francuski',
            ),
            235 =>
            array (
                'id' => 236,
                'language_code' => 'fr',
                'display_language_code' => 'pt-pt',
                'name' => 'Francês',
            ),
            236 =>
            array (
                'id' => 237,
                'language_code' => 'fr',
                'display_language_code' => 'pt-br',
                'name' => 'Francês',
            ),
            237 =>
            array (
                'id' => 238,
                'language_code' => 'fr',
                'display_language_code' => 'qu',
                'name' => 'French',
            ),
            238 =>
            array (
                'id' => 239,
                'language_code' => 'fr',
                'display_language_code' => 'ro',
                'name' => 'Franceză',
            ),
            239 =>
            array (
                'id' => 240,
                'language_code' => 'fr',
                'display_language_code' => 'ru',
                'name' => 'Французский',
            ),
            240 =>
            array (
                'id' => 241,
                'language_code' => 'fr',
                'display_language_code' => 'sl',
                'name' => 'Francoščina',
            ),
            241 =>
            array (
                'id' => 242,
                'language_code' => 'fr',
                'display_language_code' => 'so',
                'name' => 'French',
            ),
            242 =>
            array (
                'id' => 243,
                'language_code' => 'fr',
                'display_language_code' => 'sq',
                'name' => 'French',
            ),
            243 =>
            array (
                'id' => 244,
                'language_code' => 'fr',
                'display_language_code' => 'sr',
                'name' => 'француски',
            ),
            244 =>
            array (
                'id' => 245,
                'language_code' => 'fr',
                'display_language_code' => 'sv',
                'name' => 'Franska',
            ),
            245 =>
            array (
                'id' => 246,
                'language_code' => 'fr',
                'display_language_code' => 'ta',
                'name' => 'French',
            ),
            246 =>
            array (
                'id' => 247,
                'language_code' => 'fr',
                'display_language_code' => 'th',
                'name' => 'ฝรั่งเศส',
            ),
            247 =>
            array (
                'id' => 248,
                'language_code' => 'fr',
                'display_language_code' => 'tr',
                'name' => 'Fransızca',
            ),
            248 =>
            array (
                'id' => 249,
                'language_code' => 'fr',
                'display_language_code' => 'uk',
                'name' => 'French',
            ),
            249 =>
            array (
                'id' => 250,
                'language_code' => 'fr',
                'display_language_code' => 'ur',
                'name' => 'French',
            ),
            250 =>
            array (
                'id' => 251,
                'language_code' => 'fr',
                'display_language_code' => 'uz',
                'name' => 'French',
            ),
            251 =>
            array (
                'id' => 252,
                'language_code' => 'fr',
                'display_language_code' => 'vi',
                'name' => 'French',
            ),
            252 =>
            array (
                'id' => 253,
                'language_code' => 'fr',
                'display_language_code' => 'yi',
                'name' => 'French',
            ),
            253 =>
            array (
                'id' => 254,
                'language_code' => 'fr',
                'display_language_code' => 'zh-hans',
                'name' => '法语',
            ),
            254 =>
            array (
                'id' => 255,
                'language_code' => 'fr',
                'display_language_code' => 'zu',
                'name' => 'French',
            ),
            255 =>
            array (
                'id' => 256,
                'language_code' => 'fr',
                'display_language_code' => 'zh-hant',
                'name' => '法語',
            ),
            256 =>
            array (
                'id' => 257,
                'language_code' => 'fr',
                'display_language_code' => 'ms',
                'name' => 'French',
            ),
            257 =>
            array (
                'id' => 258,
                'language_code' => 'fr',
                'display_language_code' => 'gl',
                'name' => 'French',
            ),
            258 =>
            array (
                'id' => 259,
                'language_code' => 'fr',
                'display_language_code' => 'bn',
                'name' => 'French',
            ),
            259 =>
            array (
                'id' => 260,
                'language_code' => 'fr',
                'display_language_code' => 'az',
                'name' => 'French',
            ),
            260 =>
            array (
                'id' => 261,
                'language_code' => 'ar',
                'display_language_code' => 'en',
                'name' => 'Arabic',
            ),
            261 =>
            array (
                'id' => 262,
                'language_code' => 'ar',
                'display_language_code' => 'es',
                'name' => 'Árabe',
            ),
            262 =>
            array (
                'id' => 263,
                'language_code' => 'ar',
                'display_language_code' => 'de',
                'name' => 'Arabisch',
            ),
            263 =>
            array (
                'id' => 264,
                'language_code' => 'ar',
                'display_language_code' => 'fr',
                'name' => 'Arabe',
            ),
            264 =>
            array (
                'id' => 265,
                'language_code' => 'ar',
                'display_language_code' => 'ar',
                'name' => 'العربية',
            ),
            265 =>
            array (
                'id' => 266,
                'language_code' => 'ar',
                'display_language_code' => 'bs',
                'name' => 'Arabic',
            ),
            266 =>
            array (
                'id' => 267,
                'language_code' => 'ar',
                'display_language_code' => 'bg',
                'name' => 'Арабски',
            ),
            267 =>
            array (
                'id' => 268,
                'language_code' => 'ar',
                'display_language_code' => 'ca',
                'name' => 'Arabic',
            ),
            268 =>
            array (
                'id' => 269,
                'language_code' => 'ar',
                'display_language_code' => 'cs',
                'name' => 'Arabština',
            ),
            269 =>
            array (
                'id' => 270,
                'language_code' => 'ar',
                'display_language_code' => 'sk',
                'name' => 'Arabčina',
            ),
            270 =>
            array (
                'id' => 271,
                'language_code' => 'ar',
                'display_language_code' => 'cy',
                'name' => 'Arabic',
            ),
            271 =>
            array (
                'id' => 272,
                'language_code' => 'ar',
                'display_language_code' => 'da',
                'name' => 'Arabic',
            ),
            272 =>
            array (
                'id' => 273,
                'language_code' => 'ar',
                'display_language_code' => 'el',
                'name' => 'Αραβικά',
            ),
            273 =>
            array (
                'id' => 274,
                'language_code' => 'ar',
                'display_language_code' => 'eo',
                'name' => 'Arabic',
            ),
            274 =>
            array (
                'id' => 275,
                'language_code' => 'ar',
                'display_language_code' => 'et',
                'name' => 'Arabic',
            ),
            275 =>
            array (
                'id' => 276,
                'language_code' => 'ar',
                'display_language_code' => 'eu',
                'name' => 'Arabic',
            ),
            276 =>
            array (
                'id' => 277,
                'language_code' => 'ar',
                'display_language_code' => 'fa',
                'name' => 'Arabic',
            ),
            277 =>
            array (
                'id' => 278,
                'language_code' => 'ar',
                'display_language_code' => 'fi',
                'name' => 'Arabia',
            ),
            278 =>
            array (
                'id' => 279,
                'language_code' => 'ar',
                'display_language_code' => 'ga',
                'name' => 'Arabic',
            ),
            279 =>
            array (
                'id' => 280,
                'language_code' => 'ar',
                'display_language_code' => 'he',
                'name' => 'ערבית',
            ),
            280 =>
            array (
                'id' => 281,
                'language_code' => 'ar',
                'display_language_code' => 'hi',
                'name' => 'Arabic',
            ),
            281 =>
            array (
                'id' => 282,
                'language_code' => 'ar',
                'display_language_code' => 'hr',
                'name' => 'Arapski',
            ),
            282 =>
            array (
                'id' => 283,
                'language_code' => 'ar',
                'display_language_code' => 'hu',
                'name' => 'Arab',
            ),
            283 =>
            array (
                'id' => 284,
                'language_code' => 'ar',
                'display_language_code' => 'hy',
                'name' => 'Arabic',
            ),
            284 =>
            array (
                'id' => 285,
                'language_code' => 'ar',
                'display_language_code' => 'id',
                'name' => 'Arabic',
            ),
            285 =>
            array (
                'id' => 286,
                'language_code' => 'ar',
                'display_language_code' => 'is',
                'name' => 'Arabic',
            ),
            286 =>
            array (
                'id' => 287,
                'language_code' => 'ar',
                'display_language_code' => 'it',
                'name' => 'Arabo',
            ),
            287 =>
            array (
                'id' => 288,
                'language_code' => 'ar',
                'display_language_code' => 'ja',
                'name' => 'アラビア語',
            ),
            288 =>
            array (
                'id' => 289,
                'language_code' => 'ar',
                'display_language_code' => 'ko',
                'name' => '아랍어',
            ),
            289 =>
            array (
                'id' => 290,
                'language_code' => 'ar',
                'display_language_code' => 'ku',
                'name' => 'Arabic',
            ),
            290 =>
            array (
                'id' => 291,
                'language_code' => 'ar',
                'display_language_code' => 'lv',
                'name' => 'Arabic',
            ),
            291 =>
            array (
                'id' => 292,
                'language_code' => 'ar',
                'display_language_code' => 'lt',
                'name' => 'Arabic',
            ),
            292 =>
            array (
                'id' => 293,
                'language_code' => 'ar',
                'display_language_code' => 'mk',
                'name' => 'Arabic',
            ),
            293 =>
            array (
                'id' => 294,
                'language_code' => 'ar',
                'display_language_code' => 'mt',
                'name' => 'Arabic',
            ),
            294 =>
            array (
                'id' => 295,
                'language_code' => 'ar',
                'display_language_code' => 'mn',
                'name' => 'Arabic',
            ),
            295 =>
            array (
                'id' => 296,
                'language_code' => 'ar',
                'display_language_code' => 'ne',
                'name' => 'Arabic',
            ),
            296 =>
            array (
                'id' => 297,
                'language_code' => 'ar',
                'display_language_code' => 'nl',
                'name' => 'Arabisch',
            ),
            297 =>
            array (
                'id' => 298,
                'language_code' => 'ar',
                'display_language_code' => 'no',
                'name' => 'Arabisk',
            ),
            298 =>
            array (
                'id' => 299,
                'language_code' => 'ar',
                'display_language_code' => 'pa',
                'name' => 'Arabic',
            ),
            299 =>
            array (
                'id' => 300,
                'language_code' => 'ar',
                'display_language_code' => 'pl',
                'name' => 'Arabski',
            ),
            300 =>
            array (
                'id' => 301,
                'language_code' => 'ar',
                'display_language_code' => 'pt-pt',
                'name' => 'Árabe',
            ),
            301 =>
            array (
                'id' => 302,
                'language_code' => 'ar',
                'display_language_code' => 'pt-br',
                'name' => 'Árabe',
            ),
            302 =>
            array (
                'id' => 303,
                'language_code' => 'ar',
                'display_language_code' => 'qu',
                'name' => 'Arabic',
            ),
            303 =>
            array (
                'id' => 304,
                'language_code' => 'ar',
                'display_language_code' => 'ro',
                'name' => 'Arabică',
            ),
            304 =>
            array (
                'id' => 305,
                'language_code' => 'ar',
                'display_language_code' => 'ru',
                'name' => 'Арабский',
            ),
            305 =>
            array (
                'id' => 306,
                'language_code' => 'ar',
                'display_language_code' => 'sl',
                'name' => 'Arabščina',
            ),
            306 =>
            array (
                'id' => 307,
                'language_code' => 'ar',
                'display_language_code' => 'so',
                'name' => 'Arabic',
            ),
            307 =>
            array (
                'id' => 308,
                'language_code' => 'ar',
                'display_language_code' => 'sq',
                'name' => 'Arabic',
            ),
            308 =>
            array (
                'id' => 309,
                'language_code' => 'ar',
                'display_language_code' => 'sr',
                'name' => 'арапски',
            ),
            309 =>
            array (
                'id' => 310,
                'language_code' => 'ar',
                'display_language_code' => 'sv',
                'name' => 'Arabiska',
            ),
            310 =>
            array (
                'id' => 311,
                'language_code' => 'ar',
                'display_language_code' => 'ta',
                'name' => 'Arabic',
            ),
            311 =>
            array (
                'id' => 312,
                'language_code' => 'ar',
                'display_language_code' => 'th',
                'name' => 'อารบิก',
            ),
            312 =>
            array (
                'id' => 313,
                'language_code' => 'ar',
                'display_language_code' => 'tr',
                'name' => 'Arapça',
            ),
            313 =>
            array (
                'id' => 314,
                'language_code' => 'ar',
                'display_language_code' => 'uk',
                'name' => 'Arabic',
            ),
            314 =>
            array (
                'id' => 315,
                'language_code' => 'ar',
                'display_language_code' => 'ur',
                'name' => 'Arabic',
            ),
            315 =>
            array (
                'id' => 316,
                'language_code' => 'ar',
                'display_language_code' => 'uz',
                'name' => 'Arabic',
            ),
            316 =>
            array (
                'id' => 317,
                'language_code' => 'ar',
                'display_language_code' => 'vi',
                'name' => 'Arabic',
            ),
            317 =>
            array (
                'id' => 318,
                'language_code' => 'ar',
                'display_language_code' => 'yi',
                'name' => 'Arabic',
            ),
            318 =>
            array (
                'id' => 319,
                'language_code' => 'ar',
                'display_language_code' => 'zh-hans',
                'name' => '阿拉伯语',
            ),
            319 =>
            array (
                'id' => 320,
                'language_code' => 'ar',
                'display_language_code' => 'zu',
                'name' => 'Arabic',
            ),
            320 =>
            array (
                'id' => 321,
                'language_code' => 'ar',
                'display_language_code' => 'zh-hant',
                'name' => '阿拉伯語',
            ),
            321 =>
            array (
                'id' => 322,
                'language_code' => 'ar',
                'display_language_code' => 'ms',
                'name' => 'Arabic',
            ),
            322 =>
            array (
                'id' => 323,
                'language_code' => 'ar',
                'display_language_code' => 'gl',
                'name' => 'Arabic',
            ),
            323 =>
            array (
                'id' => 324,
                'language_code' => 'ar',
                'display_language_code' => 'bn',
                'name' => 'Arabic',
            ),
            324 =>
            array (
                'id' => 325,
                'language_code' => 'ar',
                'display_language_code' => 'az',
                'name' => 'Arabic',
            ),
            325 =>
            array (
                'id' => 326,
                'language_code' => 'bs',
                'display_language_code' => 'en',
                'name' => 'Bosnian',
            ),
            326 =>
            array (
                'id' => 327,
                'language_code' => 'bs',
                'display_language_code' => 'es',
                'name' => 'Bosnio',
            ),
            327 =>
            array (
                'id' => 328,
                'language_code' => 'bs',
                'display_language_code' => 'de',
                'name' => 'Bosnisch',
            ),
            328 =>
            array (
                'id' => 329,
                'language_code' => 'bs',
                'display_language_code' => 'fr',
                'name' => 'Bosnien',
            ),
            329 =>
            array (
                'id' => 330,
                'language_code' => 'bs',
                'display_language_code' => 'ar',
                'name' => 'البوسنية',
            ),
            330 =>
            array (
                'id' => 331,
                'language_code' => 'bs',
                'display_language_code' => 'bs',
                'name' => 'Bosnian',
            ),
            331 =>
            array (
                'id' => 332,
                'language_code' => 'bs',
                'display_language_code' => 'bg',
                'name' => 'Босненски',
            ),
            332 =>
            array (
                'id' => 333,
                'language_code' => 'bs',
                'display_language_code' => 'ca',
                'name' => 'Bosnian',
            ),
            333 =>
            array (
                'id' => 334,
                'language_code' => 'bs',
                'display_language_code' => 'cs',
                'name' => 'Bosenština',
            ),
            334 =>
            array (
                'id' => 335,
                'language_code' => 'bs',
                'display_language_code' => 'sk',
                'name' => 'Bosniačtina',
            ),
            335 =>
            array (
                'id' => 336,
                'language_code' => 'bs',
                'display_language_code' => 'cy',
                'name' => 'Bosnian',
            ),
            336 =>
            array (
                'id' => 337,
                'language_code' => 'bs',
                'display_language_code' => 'da',
                'name' => 'Bosnian',
            ),
            337 =>
            array (
                'id' => 338,
                'language_code' => 'bs',
                'display_language_code' => 'el',
                'name' => 'Βοσνιακά',
            ),
            338 =>
            array (
                'id' => 339,
                'language_code' => 'bs',
                'display_language_code' => 'eo',
                'name' => 'Bosnian',
            ),
            339 =>
            array (
                'id' => 340,
                'language_code' => 'bs',
                'display_language_code' => 'et',
                'name' => 'Bosnian',
            ),
            340 =>
            array (
                'id' => 341,
                'language_code' => 'bs',
                'display_language_code' => 'eu',
                'name' => 'Bosnian',
            ),
            341 =>
            array (
                'id' => 342,
                'language_code' => 'bs',
                'display_language_code' => 'fa',
                'name' => 'Bosnian',
            ),
            342 =>
            array (
                'id' => 343,
                'language_code' => 'bs',
                'display_language_code' => 'fi',
                'name' => 'Bosnia',
            ),
            343 =>
            array (
                'id' => 344,
                'language_code' => 'bs',
                'display_language_code' => 'ga',
                'name' => 'Bosnian',
            ),
            344 =>
            array (
                'id' => 345,
                'language_code' => 'bs',
                'display_language_code' => 'he',
                'name' => 'בוסנית',
            ),
            345 =>
            array (
                'id' => 346,
                'language_code' => 'bs',
                'display_language_code' => 'hi',
                'name' => 'Bosnian',
            ),
            346 =>
            array (
                'id' => 347,
                'language_code' => 'bs',
                'display_language_code' => 'hr',
                'name' => 'Bosanski',
            ),
            347 =>
            array (
                'id' => 348,
                'language_code' => 'bs',
                'display_language_code' => 'hu',
                'name' => 'Bosnyák',
            ),
            348 =>
            array (
                'id' => 349,
                'language_code' => 'bs',
                'display_language_code' => 'hy',
                'name' => 'Bosnian',
            ),
            349 =>
            array (
                'id' => 350,
                'language_code' => 'bs',
                'display_language_code' => 'id',
                'name' => 'Bosnian',
            ),
            350 =>
            array (
                'id' => 351,
                'language_code' => 'bs',
                'display_language_code' => 'is',
                'name' => 'Bosnian',
            ),
            351 =>
            array (
                'id' => 352,
                'language_code' => 'bs',
                'display_language_code' => 'it',
                'name' => 'Bosniaco',
            ),
            352 =>
            array (
                'id' => 353,
                'language_code' => 'bs',
                'display_language_code' => 'ja',
                'name' => 'ボスニア語',
            ),
            353 =>
            array (
                'id' => 354,
                'language_code' => 'bs',
                'display_language_code' => 'ko',
                'name' => '보즈니아어',
            ),
            354 =>
            array (
                'id' => 355,
                'language_code' => 'bs',
                'display_language_code' => 'ku',
                'name' => 'Bosnian',
            ),
            355 =>
            array (
                'id' => 356,
                'language_code' => 'bs',
                'display_language_code' => 'lv',
                'name' => 'Bosnian',
            ),
            356 =>
            array (
                'id' => 357,
                'language_code' => 'bs',
                'display_language_code' => 'lt',
                'name' => 'Bosnian',
            ),
            357 =>
            array (
                'id' => 358,
                'language_code' => 'bs',
                'display_language_code' => 'mk',
                'name' => 'Bosnian',
            ),
            358 =>
            array (
                'id' => 359,
                'language_code' => 'bs',
                'display_language_code' => 'mt',
                'name' => 'Bosnian',
            ),
            359 =>
            array (
                'id' => 360,
                'language_code' => 'bs',
                'display_language_code' => 'mn',
                'name' => 'Bosnian',
            ),
            360 =>
            array (
                'id' => 361,
                'language_code' => 'bs',
                'display_language_code' => 'ne',
                'name' => 'Bosnian',
            ),
            361 =>
            array (
                'id' => 362,
                'language_code' => 'bs',
                'display_language_code' => 'nl',
                'name' => 'Bosnisch',
            ),
            362 =>
            array (
                'id' => 363,
                'language_code' => 'bs',
                'display_language_code' => 'no',
                'name' => 'Bosnisk',
            ),
            363 =>
            array (
                'id' => 364,
                'language_code' => 'bs',
                'display_language_code' => 'pa',
                'name' => 'Bosnian',
            ),
            364 =>
            array (
                'id' => 365,
                'language_code' => 'bs',
                'display_language_code' => 'pl',
                'name' => 'Bośniacki',
            ),
            365 =>
            array (
                'id' => 366,
                'language_code' => 'bs',
                'display_language_code' => 'pt-pt',
                'name' => 'Bósnio',
            ),
            366 =>
            array (
                'id' => 367,
                'language_code' => 'bs',
                'display_language_code' => 'pt-br',
                'name' => 'Bósnio',
            ),
            367 =>
            array (
                'id' => 368,
                'language_code' => 'bs',
                'display_language_code' => 'qu',
                'name' => 'Bosnian',
            ),
            368 =>
            array (
                'id' => 369,
                'language_code' => 'bs',
                'display_language_code' => 'ro',
                'name' => 'Bosniacă',
            ),
            369 =>
            array (
                'id' => 370,
                'language_code' => 'bs',
                'display_language_code' => 'ru',
                'name' => 'Боснийский',
            ),
            370 =>
            array (
                'id' => 371,
                'language_code' => 'bs',
                'display_language_code' => 'sl',
                'name' => 'Bosanski',
            ),
            371 =>
            array (
                'id' => 372,
                'language_code' => 'bs',
                'display_language_code' => 'so',
                'name' => 'Bosnian',
            ),
            372 =>
            array (
                'id' => 373,
                'language_code' => 'bs',
                'display_language_code' => 'sq',
                'name' => 'Bosnian',
            ),
            373 =>
            array (
                'id' => 374,
                'language_code' => 'bs',
                'display_language_code' => 'sr',
                'name' => 'босански',
            ),
            374 =>
            array (
                'id' => 375,
                'language_code' => 'bs',
                'display_language_code' => 'sv',
                'name' => 'Bosniska',
            ),
            375 =>
            array (
                'id' => 376,
                'language_code' => 'bs',
                'display_language_code' => 'ta',
                'name' => 'Bosnian',
            ),
            376 =>
            array (
                'id' => 377,
                'language_code' => 'bs',
                'display_language_code' => 'th',
                'name' => 'บอสเนียน',
            ),
            377 =>
            array (
                'id' => 378,
                'language_code' => 'bs',
                'display_language_code' => 'tr',
                'name' => 'Boşnakça',
            ),
            378 =>
            array (
                'id' => 379,
                'language_code' => 'bs',
                'display_language_code' => 'uk',
                'name' => 'Bosnian',
            ),
            379 =>
            array (
                'id' => 380,
                'language_code' => 'bs',
                'display_language_code' => 'ur',
                'name' => 'Bosnian',
            ),
            380 =>
            array (
                'id' => 381,
                'language_code' => 'bs',
                'display_language_code' => 'uz',
                'name' => 'Bosnian',
            ),
            381 =>
            array (
                'id' => 382,
                'language_code' => 'bs',
                'display_language_code' => 'vi',
                'name' => 'Bosnian',
            ),
            382 =>
            array (
                'id' => 383,
                'language_code' => 'bs',
                'display_language_code' => 'yi',
                'name' => 'Bosnian',
            ),
            383 =>
            array (
                'id' => 384,
                'language_code' => 'bs',
                'display_language_code' => 'zh-hans',
                'name' => '波斯尼亚语',
            ),
            384 =>
            array (
                'id' => 385,
                'language_code' => 'bs',
                'display_language_code' => 'zu',
                'name' => 'Bosnian',
            ),
            385 =>
            array (
                'id' => 386,
                'language_code' => 'bs',
                'display_language_code' => 'zh-hant',
                'name' => '波士尼亞語',
            ),
            386 =>
            array (
                'id' => 387,
                'language_code' => 'bs',
                'display_language_code' => 'ms',
                'name' => 'Bosnian',
            ),
            387 =>
            array (
                'id' => 388,
                'language_code' => 'bs',
                'display_language_code' => 'gl',
                'name' => 'Bosnian',
            ),
            388 =>
            array (
                'id' => 389,
                'language_code' => 'bs',
                'display_language_code' => 'bn',
                'name' => 'Bosnian',
            ),
            389 =>
            array (
                'id' => 390,
                'language_code' => 'bs',
                'display_language_code' => 'az',
                'name' => 'Bosnian',
            ),
            390 =>
            array (
                'id' => 391,
                'language_code' => 'bg',
                'display_language_code' => 'en',
                'name' => 'Bulgarian',
            ),
            391 =>
            array (
                'id' => 392,
                'language_code' => 'bg',
                'display_language_code' => 'es',
                'name' => 'Búlgaro',
            ),
            392 =>
            array (
                'id' => 393,
                'language_code' => 'bg',
                'display_language_code' => 'de',
                'name' => 'Bulgarisch',
            ),
            393 =>
            array (
                'id' => 394,
                'language_code' => 'bg',
                'display_language_code' => 'fr',
                'name' => 'Bulgare',
            ),
            394 =>
            array (
                'id' => 395,
                'language_code' => 'bg',
                'display_language_code' => 'ar',
                'name' => 'البلغارية',
            ),
            395 =>
            array (
                'id' => 396,
                'language_code' => 'bg',
                'display_language_code' => 'bs',
                'name' => 'Bulgarian',
            ),
            396 =>
            array (
                'id' => 397,
                'language_code' => 'bg',
                'display_language_code' => 'bg',
                'name' => 'Български',
            ),
            397 =>
            array (
                'id' => 398,
                'language_code' => 'bg',
                'display_language_code' => 'ca',
                'name' => 'Bulgarian',
            ),
            398 =>
            array (
                'id' => 399,
                'language_code' => 'bg',
                'display_language_code' => 'cs',
                'name' => 'Bulharština',
            ),
            399 =>
            array (
                'id' => 400,
                'language_code' => 'bg',
                'display_language_code' => 'sk',
                'name' => 'Bulharčina',
            ),
            400 =>
            array (
                'id' => 401,
                'language_code' => 'bg',
                'display_language_code' => 'cy',
                'name' => 'Bulgarian',
            ),
            401 =>
            array (
                'id' => 402,
                'language_code' => 'bg',
                'display_language_code' => 'da',
                'name' => 'Bulgarian',
            ),
            402 =>
            array (
                'id' => 403,
                'language_code' => 'bg',
                'display_language_code' => 'el',
                'name' => 'Βουλγαρικά',
            ),
            403 =>
            array (
                'id' => 404,
                'language_code' => 'bg',
                'display_language_code' => 'eo',
                'name' => 'Bulgarian',
            ),
            404 =>
            array (
                'id' => 405,
                'language_code' => 'bg',
                'display_language_code' => 'et',
                'name' => 'Bulgarian',
            ),
            405 =>
            array (
                'id' => 406,
                'language_code' => 'bg',
                'display_language_code' => 'eu',
                'name' => 'Bulgarian',
            ),
            406 =>
            array (
                'id' => 407,
                'language_code' => 'bg',
                'display_language_code' => 'fa',
                'name' => 'Bulgarian',
            ),
            407 =>
            array (
                'id' => 408,
                'language_code' => 'bg',
                'display_language_code' => 'fi',
                'name' => 'Bulgaria',
            ),
            408 =>
            array (
                'id' => 409,
                'language_code' => 'bg',
                'display_language_code' => 'ga',
                'name' => 'Bulgarian',
            ),
            409 =>
            array (
                'id' => 410,
                'language_code' => 'bg',
                'display_language_code' => 'he',
                'name' => 'בולגרית',
            ),
            410 =>
            array (
                'id' => 411,
                'language_code' => 'bg',
                'display_language_code' => 'hi',
                'name' => 'Bulgarian',
            ),
            411 =>
            array (
                'id' => 412,
                'language_code' => 'bg',
                'display_language_code' => 'hr',
                'name' => 'Bugarski',
            ),
            412 =>
            array (
                'id' => 413,
                'language_code' => 'bg',
                'display_language_code' => 'hu',
                'name' => 'Bolgár',
            ),
            413 =>
            array (
                'id' => 414,
                'language_code' => 'bg',
                'display_language_code' => 'hy',
                'name' => 'Bulgarian',
            ),
            414 =>
            array (
                'id' => 415,
                'language_code' => 'bg',
                'display_language_code' => 'id',
                'name' => 'Bulgarian',
            ),
            415 =>
            array (
                'id' => 416,
                'language_code' => 'bg',
                'display_language_code' => 'is',
                'name' => 'Bulgarian',
            ),
            416 =>
            array (
                'id' => 417,
                'language_code' => 'bg',
                'display_language_code' => 'it',
                'name' => 'Bulgaro',
            ),
            417 =>
            array (
                'id' => 418,
                'language_code' => 'bg',
                'display_language_code' => 'ja',
                'name' => 'ブルガリア語',
            ),
            418 =>
            array (
                'id' => 419,
                'language_code' => 'bg',
                'display_language_code' => 'ko',
                'name' => '불가리아어',
            ),
            419 =>
            array (
                'id' => 420,
                'language_code' => 'bg',
                'display_language_code' => 'ku',
                'name' => 'Bulgarian',
            ),
            420 =>
            array (
                'id' => 421,
                'language_code' => 'bg',
                'display_language_code' => 'lv',
                'name' => 'Bulgarian',
            ),
            421 =>
            array (
                'id' => 422,
                'language_code' => 'bg',
                'display_language_code' => 'lt',
                'name' => 'Bulgarian',
            ),
            422 =>
            array (
                'id' => 423,
                'language_code' => 'bg',
                'display_language_code' => 'mk',
                'name' => 'Bulgarian',
            ),
            423 =>
            array (
                'id' => 424,
                'language_code' => 'bg',
                'display_language_code' => 'mt',
                'name' => 'Bulgarian',
            ),
            424 =>
            array (
                'id' => 425,
                'language_code' => 'bg',
                'display_language_code' => 'mn',
                'name' => 'Bulgarian',
            ),
            425 =>
            array (
                'id' => 426,
                'language_code' => 'bg',
                'display_language_code' => 'ne',
                'name' => 'Bulgarian',
            ),
            426 =>
            array (
                'id' => 427,
                'language_code' => 'bg',
                'display_language_code' => 'nl',
                'name' => 'Bulgaars',
            ),
            427 =>
            array (
                'id' => 428,
                'language_code' => 'bg',
                'display_language_code' => 'no',
                'name' => 'Bulgarian',
            ),
            428 =>
            array (
                'id' => 429,
                'language_code' => 'bg',
                'display_language_code' => 'pa',
                'name' => 'Bulgarian',
            ),
            429 =>
            array (
                'id' => 430,
                'language_code' => 'bg',
                'display_language_code' => 'pl',
                'name' => 'Bułgarski',
            ),
            430 =>
            array (
                'id' => 431,
                'language_code' => 'bg',
                'display_language_code' => 'pt-pt',
                'name' => 'Búlgaro',
            ),
            431 =>
            array (
                'id' => 432,
                'language_code' => 'bg',
                'display_language_code' => 'pt-br',
                'name' => 'Búlgaro',
            ),
            432 =>
            array (
                'id' => 433,
                'language_code' => 'bg',
                'display_language_code' => 'qu',
                'name' => 'Bulgarian',
            ),
            433 =>
            array (
                'id' => 434,
                'language_code' => 'bg',
                'display_language_code' => 'ro',
                'name' => 'Bulgară',
            ),
            434 =>
            array (
                'id' => 435,
                'language_code' => 'bg',
                'display_language_code' => 'ru',
                'name' => 'Болгарский',
            ),
            435 =>
            array (
                'id' => 436,
                'language_code' => 'bg',
                'display_language_code' => 'sl',
                'name' => 'Bolgarščina',
            ),
            436 =>
            array (
                'id' => 437,
                'language_code' => 'bg',
                'display_language_code' => 'so',
                'name' => 'Bulgarian',
            ),
            437 =>
            array (
                'id' => 438,
                'language_code' => 'bg',
                'display_language_code' => 'sq',
                'name' => 'Bulgarian',
            ),
            438 =>
            array (
                'id' => 439,
                'language_code' => 'bg',
                'display_language_code' => 'sr',
                'name' => 'бугарски',
            ),
            439 =>
            array (
                'id' => 440,
                'language_code' => 'bg',
                'display_language_code' => 'sv',
                'name' => 'Bulgariska',
            ),
            440 =>
            array (
                'id' => 441,
                'language_code' => 'bg',
                'display_language_code' => 'ta',
                'name' => 'Bulgarian',
            ),
            441 =>
            array (
                'id' => 442,
                'language_code' => 'bg',
                'display_language_code' => 'th',
                'name' => 'บัลแกเรียน',
            ),
            442 =>
            array (
                'id' => 443,
                'language_code' => 'bg',
                'display_language_code' => 'tr',
                'name' => 'Bulgarca',
            ),
            443 =>
            array (
                'id' => 444,
                'language_code' => 'bg',
                'display_language_code' => 'uk',
                'name' => 'Bulgarian',
            ),
            444 =>
            array (
                'id' => 445,
                'language_code' => 'bg',
                'display_language_code' => 'ur',
                'name' => 'Bulgarian',
            ),
            445 =>
            array (
                'id' => 446,
                'language_code' => 'bg',
                'display_language_code' => 'uz',
                'name' => 'Bulgarian',
            ),
            446 =>
            array (
                'id' => 447,
                'language_code' => 'bg',
                'display_language_code' => 'vi',
                'name' => 'Bulgarian',
            ),
            447 =>
            array (
                'id' => 448,
                'language_code' => 'bg',
                'display_language_code' => 'yi',
                'name' => 'Bulgarian',
            ),
            448 =>
            array (
                'id' => 449,
                'language_code' => 'bg',
                'display_language_code' => 'zh-hans',
                'name' => '保加利亚语',
            ),
            449 =>
            array (
                'id' => 450,
                'language_code' => 'bg',
                'display_language_code' => 'zu',
                'name' => 'Bulgarian',
            ),
            450 =>
            array (
                'id' => 451,
                'language_code' => 'bg',
                'display_language_code' => 'zh-hant',
                'name' => '保加利亞語',
            ),
            451 =>
            array (
                'id' => 452,
                'language_code' => 'bg',
                'display_language_code' => 'ms',
                'name' => 'Bulgarian',
            ),
            452 =>
            array (
                'id' => 453,
                'language_code' => 'bg',
                'display_language_code' => 'gl',
                'name' => 'Bulgarian',
            ),
            453 =>
            array (
                'id' => 454,
                'language_code' => 'bg',
                'display_language_code' => 'bn',
                'name' => 'Bulgarian',
            ),
            454 =>
            array (
                'id' => 455,
                'language_code' => 'bg',
                'display_language_code' => 'az',
                'name' => 'Bulgarian',
            ),
            455 =>
            array (
                'id' => 456,
                'language_code' => 'ca',
                'display_language_code' => 'en',
                'name' => 'Catalan',
            ),
            456 =>
            array (
                'id' => 457,
                'language_code' => 'ca',
                'display_language_code' => 'es',
                'name' => 'Catalán',
            ),
            457 =>
            array (
                'id' => 458,
                'language_code' => 'ca',
                'display_language_code' => 'de',
                'name' => 'Katalanisch',
            ),
            458 =>
            array (
                'id' => 459,
                'language_code' => 'ca',
                'display_language_code' => 'fr',
                'name' => 'Catalan',
            ),
            459 =>
            array (
                'id' => 460,
                'language_code' => 'ca',
                'display_language_code' => 'ar',
                'name' => 'الكاتالوينية',
            ),
            460 =>
            array (
                'id' => 461,
                'language_code' => 'ca',
                'display_language_code' => 'bs',
                'name' => 'Catalan',
            ),
            461 =>
            array (
                'id' => 462,
                'language_code' => 'ca',
                'display_language_code' => 'bg',
                'name' => 'Каталонски',
            ),
            462 =>
            array (
                'id' => 463,
                'language_code' => 'ca',
                'display_language_code' => 'ca',
                'name' => 'Català',
            ),
            463 =>
            array (
                'id' => 464,
                'language_code' => 'ca',
                'display_language_code' => 'cs',
                'name' => 'Katalánština',
            ),
            464 =>
            array (
                'id' => 465,
                'language_code' => 'ca',
                'display_language_code' => 'sk',
                'name' => 'Katalánčina',
            ),
            465 =>
            array (
                'id' => 466,
                'language_code' => 'ca',
                'display_language_code' => 'cy',
                'name' => 'Catalan',
            ),
            466 =>
            array (
                'id' => 467,
                'language_code' => 'ca',
                'display_language_code' => 'da',
                'name' => 'Catalan',
            ),
            467 =>
            array (
                'id' => 468,
                'language_code' => 'ca',
                'display_language_code' => 'el',
                'name' => 'Καταλανικά',
            ),
            468 =>
            array (
                'id' => 469,
                'language_code' => 'ca',
                'display_language_code' => 'eo',
                'name' => 'Catalan',
            ),
            469 =>
            array (
                'id' => 470,
                'language_code' => 'ca',
                'display_language_code' => 'et',
                'name' => 'Catalan',
            ),
            470 =>
            array (
                'id' => 471,
                'language_code' => 'ca',
                'display_language_code' => 'eu',
                'name' => 'Catalan',
            ),
            471 =>
            array (
                'id' => 472,
                'language_code' => 'ca',
                'display_language_code' => 'fa',
                'name' => 'Catalan',
            ),
            472 =>
            array (
                'id' => 473,
                'language_code' => 'ca',
                'display_language_code' => 'fi',
                'name' => 'Katalaani',
            ),
            473 =>
            array (
                'id' => 474,
                'language_code' => 'ca',
                'display_language_code' => 'ga',
                'name' => 'Catalan',
            ),
            474 =>
            array (
                'id' => 475,
                'language_code' => 'ca',
                'display_language_code' => 'he',
                'name' => 'קטלאנית',
            ),
            475 =>
            array (
                'id' => 476,
                'language_code' => 'ca',
                'display_language_code' => 'hi',
                'name' => 'Catalan',
            ),
            476 =>
            array (
                'id' => 477,
                'language_code' => 'ca',
                'display_language_code' => 'hr',
                'name' => 'Katalonski',
            ),
            477 =>
            array (
                'id' => 478,
                'language_code' => 'ca',
                'display_language_code' => 'hu',
                'name' => 'Katalán',
            ),
            478 =>
            array (
                'id' => 479,
                'language_code' => 'ca',
                'display_language_code' => 'hy',
                'name' => 'Catalan',
            ),
            479 =>
            array (
                'id' => 480,
                'language_code' => 'ca',
                'display_language_code' => 'id',
                'name' => 'Catalan',
            ),
            480 =>
            array (
                'id' => 481,
                'language_code' => 'ca',
                'display_language_code' => 'is',
                'name' => 'Catalan',
            ),
            481 =>
            array (
                'id' => 482,
                'language_code' => 'ca',
                'display_language_code' => 'it',
                'name' => 'Catalano',
            ),
            482 =>
            array (
                'id' => 483,
                'language_code' => 'ca',
                'display_language_code' => 'ja',
                'name' => 'カタルーニャ語',
            ),
            483 =>
            array (
                'id' => 484,
                'language_code' => 'ca',
                'display_language_code' => 'ko',
                'name' => '카탈로니아어',
            ),
            484 =>
            array (
                'id' => 485,
                'language_code' => 'ca',
                'display_language_code' => 'ku',
                'name' => 'Catalan',
            ),
            485 =>
            array (
                'id' => 486,
                'language_code' => 'ca',
                'display_language_code' => 'lv',
                'name' => 'Catalan',
            ),
            486 =>
            array (
                'id' => 487,
                'language_code' => 'ca',
                'display_language_code' => 'lt',
                'name' => 'Catalan',
            ),
            487 =>
            array (
                'id' => 488,
                'language_code' => 'ca',
                'display_language_code' => 'mk',
                'name' => 'Catalan',
            ),
            488 =>
            array (
                'id' => 489,
                'language_code' => 'ca',
                'display_language_code' => 'mt',
                'name' => 'Catalan',
            ),
            489 =>
            array (
                'id' => 490,
                'language_code' => 'ca',
                'display_language_code' => 'mn',
                'name' => 'Catalan',
            ),
            490 =>
            array (
                'id' => 491,
                'language_code' => 'ca',
                'display_language_code' => 'ne',
                'name' => 'Catalan',
            ),
            491 =>
            array (
                'id' => 492,
                'language_code' => 'ca',
                'display_language_code' => 'nl',
                'name' => 'Catalaans',
            ),
            492 =>
            array (
                'id' => 493,
                'language_code' => 'ca',
                'display_language_code' => 'no',
                'name' => 'catalan',
            ),
            493 =>
            array (
                'id' => 494,
                'language_code' => 'ca',
                'display_language_code' => 'pa',
                'name' => 'Catalan',
            ),
            494 =>
            array (
                'id' => 495,
                'language_code' => 'ca',
                'display_language_code' => 'pl',
                'name' => 'Kataloński',
            ),
            495 =>
            array (
                'id' => 496,
                'language_code' => 'ca',
                'display_language_code' => 'pt-pt',
                'name' => 'Catalão',
            ),
            496 =>
            array (
                'id' => 497,
                'language_code' => 'ca',
                'display_language_code' => 'pt-br',
                'name' => 'Catalão',
            ),
            497 =>
            array (
                'id' => 498,
                'language_code' => 'ca',
                'display_language_code' => 'qu',
                'name' => 'Catalan',
            ),
            498 =>
            array (
                'id' => 499,
                'language_code' => 'ca',
                'display_language_code' => 'ro',
                'name' => 'Catalană',
            ),
            499 =>
            array (
                'id' => 500,
                'language_code' => 'ca',
                'display_language_code' => 'ru',
                'name' => 'Каталанский',
            ),
        ));
        \DB::table('language_translations')->insert(array (
            0 =>
            array (
                'id' => 501,
                'language_code' => 'ca',
                'display_language_code' => 'sl',
                'name' => 'Katalonščina',
            ),
            1 =>
            array (
                'id' => 502,
                'language_code' => 'ca',
                'display_language_code' => 'so',
                'name' => 'Catalan',
            ),
            2 =>
            array (
                'id' => 503,
                'language_code' => 'ca',
                'display_language_code' => 'sq',
                'name' => 'Catalan',
            ),
            3 =>
            array (
                'id' => 504,
                'language_code' => 'ca',
                'display_language_code' => 'sr',
                'name' => 'каталонски',
            ),
            4 =>
            array (
                'id' => 505,
                'language_code' => 'ca',
                'display_language_code' => 'sv',
                'name' => 'Katalanska',
            ),
            5 =>
            array (
                'id' => 506,
                'language_code' => 'ca',
                'display_language_code' => 'ta',
                'name' => 'Catalan',
            ),
            6 =>
            array (
                'id' => 507,
                'language_code' => 'ca',
                'display_language_code' => 'th',
                'name' => 'คะตะลาน',
            ),
            7 =>
            array (
                'id' => 508,
                'language_code' => 'ca',
                'display_language_code' => 'tr',
                'name' => 'Katalan dili',
            ),
            8 =>
            array (
                'id' => 509,
                'language_code' => 'ca',
                'display_language_code' => 'uk',
                'name' => 'Catalan',
            ),
            9 =>
            array (
                'id' => 510,
                'language_code' => 'ca',
                'display_language_code' => 'ur',
                'name' => 'Catalan',
            ),
            10 =>
            array (
                'id' => 511,
                'language_code' => 'ca',
                'display_language_code' => 'uz',
                'name' => 'Catalan',
            ),
            11 =>
            array (
                'id' => 512,
                'language_code' => 'ca',
                'display_language_code' => 'vi',
                'name' => 'Catalan',
            ),
            12 =>
            array (
                'id' => 513,
                'language_code' => 'ca',
                'display_language_code' => 'yi',
                'name' => 'Catalan',
            ),
            13 =>
            array (
                'id' => 514,
                'language_code' => 'ca',
                'display_language_code' => 'zh-hans',
                'name' => '加泰罗尼亚语',
            ),
            14 =>
            array (
                'id' => 515,
                'language_code' => 'ca',
                'display_language_code' => 'zu',
                'name' => 'Catalan',
            ),
            15 =>
            array (
                'id' => 516,
                'language_code' => 'ca',
                'display_language_code' => 'zh-hant',
                'name' => '加泰羅尼亞語',
            ),
            16 =>
            array (
                'id' => 517,
                'language_code' => 'ca',
                'display_language_code' => 'ms',
                'name' => 'Catalan',
            ),
            17 =>
            array (
                'id' => 518,
                'language_code' => 'ca',
                'display_language_code' => 'gl',
                'name' => 'Catalan',
            ),
            18 =>
            array (
                'id' => 519,
                'language_code' => 'ca',
                'display_language_code' => 'bn',
                'name' => 'Catalan',
            ),
            19 =>
            array (
                'id' => 520,
                'language_code' => 'ca',
                'display_language_code' => 'az',
                'name' => 'Catalan',
            ),
            20 =>
            array (
                'id' => 521,
                'language_code' => 'cz',
                'display_language_code' => 'en',
                'name' => 'Czech',
            ),
            21 =>
            array (
                'id' => 522,
                'language_code' => 'cz',
                'display_language_code' => 'es',
                'name' => 'Checo',
            ),
            22 =>
            array (
                'id' => 523,
                'language_code' => 'cz',
                'display_language_code' => 'de',
                'name' => 'Tschechisch',
            ),
            23 =>
            array (
                'id' => 524,
                'language_code' => 'cz',
                'display_language_code' => 'fr',
                'name' => 'Tchèque',
            ),
            24 =>
            array (
                'id' => 525,
                'language_code' => 'cz',
                'display_language_code' => 'ar',
                'name' => 'التشيكية',
            ),
            25 =>
            array (
                'id' => 526,
                'language_code' => 'cz',
                'display_language_code' => 'bs',
                'name' => 'Czech',
            ),
            26 =>
            array (
                'id' => 527,
                'language_code' => 'cz',
                'display_language_code' => 'bg',
                'name' => 'Чешки',
            ),
            27 =>
            array (
                'id' => 528,
                'language_code' => 'cz',
                'display_language_code' => 'ca',
                'name' => 'Czech',
            ),
            28 =>
            array (
                'id' => 529,
                'language_code' => 'cz',
                'display_language_code' => 'cs',
                'name' => 'Čeština',
            ),
            29 =>
            array (
                'id' => 530,
                'language_code' => 'cz',
                'display_language_code' => 'sk',
                'name' => 'Čeština',
            ),
            30 =>
            array (
                'id' => 531,
                'language_code' => 'cz',
                'display_language_code' => 'cy',
                'name' => 'Czech',
            ),
            31 =>
            array (
                'id' => 532,
                'language_code' => 'cz',
                'display_language_code' => 'da',
                'name' => 'Czech',
            ),
            32 =>
            array (
                'id' => 533,
                'language_code' => 'cz',
                'display_language_code' => 'el',
                'name' => 'Τσεχικά',
            ),
            33 =>
            array (
                'id' => 534,
                'language_code' => 'cz',
                'display_language_code' => 'eo',
                'name' => 'Czech',
            ),
            34 =>
            array (
                'id' => 535,
                'language_code' => 'cz',
                'display_language_code' => 'et',
                'name' => 'Czech',
            ),
            35 =>
            array (
                'id' => 536,
                'language_code' => 'cz',
                'display_language_code' => 'eu',
                'name' => 'Czech',
            ),
            36 =>
            array (
                'id' => 537,
                'language_code' => 'cz',
                'display_language_code' => 'fa',
                'name' => 'Czech',
            ),
            37 =>
            array (
                'id' => 538,
                'language_code' => 'cz',
                'display_language_code' => 'fi',
                'name' => 'Tsekki',
            ),
            38 =>
            array (
                'id' => 539,
                'language_code' => 'cz',
                'display_language_code' => 'ga',
                'name' => 'Czech',
            ),
            39 =>
            array (
                'id' => 540,
                'language_code' => 'cz',
                'display_language_code' => 'he',
                'name' => 'צ\'כית',
            ),
            40 =>
            array (
                'id' => 541,
                'language_code' => 'cz',
                'display_language_code' => 'hi',
                'name' => 'Czech',
            ),
            41 =>
            array (
                'id' => 542,
                'language_code' => 'cz',
                'display_language_code' => 'hr',
                'name' => 'češki',
            ),
            42 =>
            array (
                'id' => 543,
                'language_code' => 'cz',
                'display_language_code' => 'hu',
                'name' => 'Cseh',
            ),
            43 =>
            array (
                'id' => 544,
                'language_code' => 'cz',
                'display_language_code' => 'hy',
                'name' => 'Czech',
            ),
            44 =>
            array (
                'id' => 545,
                'language_code' => 'cz',
                'display_language_code' => 'id',
                'name' => 'Czech',
            ),
            45 =>
            array (
                'id' => 546,
                'language_code' => 'cz',
                'display_language_code' => 'is',
                'name' => 'Czech',
            ),
            46 =>
            array (
                'id' => 547,
                'language_code' => 'cz',
                'display_language_code' => 'it',
                'name' => 'Ceco',
            ),
            47 =>
            array (
                'id' => 548,
                'language_code' => 'cz',
                'display_language_code' => 'ja',
                'name' => 'チェコ語',
            ),
            48 =>
            array (
                'id' => 549,
                'language_code' => 'cz',
                'display_language_code' => 'ko',
                'name' => '체코슬로바키아어',
            ),
            49 =>
            array (
                'id' => 550,
                'language_code' => 'cz',
                'display_language_code' => 'ku',
                'name' => 'Czech',
            ),
            50 =>
            array (
                'id' => 551,
                'language_code' => 'cz',
                'display_language_code' => 'lv',
                'name' => 'Czech',
            ),
            51 =>
            array (
                'id' => 552,
                'language_code' => 'cz',
                'display_language_code' => 'lt',
                'name' => 'Czech',
            ),
            52 =>
            array (
                'id' => 553,
                'language_code' => 'cz',
                'display_language_code' => 'mk',
                'name' => 'Czech',
            ),
            53 =>
            array (
                'id' => 554,
                'language_code' => 'cz',
                'display_language_code' => 'mt',
                'name' => 'Czech',
            ),
            54 =>
            array (
                'id' => 555,
                'language_code' => 'cz',
                'display_language_code' => 'mn',
                'name' => 'Czech',
            ),
            55 =>
            array (
                'id' => 556,
                'language_code' => 'cz',
                'display_language_code' => 'ne',
                'name' => 'Czech',
            ),
            56 =>
            array (
                'id' => 557,
                'language_code' => 'cz',
                'display_language_code' => 'nl',
                'name' => 'Tsjechisch',
            ),
            57 =>
            array (
                'id' => 558,
                'language_code' => 'cz',
                'display_language_code' => 'no',
                'name' => 'Czech',
            ),
            58 =>
            array (
                'id' => 559,
                'language_code' => 'cz',
                'display_language_code' => 'pa',
                'name' => 'Czech',
            ),
            59 =>
            array (
                'id' => 560,
                'language_code' => 'cz',
                'display_language_code' => 'pl',
                'name' => 'Czeski',
            ),
            60 =>
            array (
                'id' => 561,
                'language_code' => 'cz',
                'display_language_code' => 'pt-pt',
                'name' => 'Tcheco',
            ),
            61 =>
            array (
                'id' => 562,
                'language_code' => 'cz',
                'display_language_code' => 'pt-br',
                'name' => 'Tcheco',
            ),
            62 =>
            array (
                'id' => 563,
                'language_code' => 'cz',
                'display_language_code' => 'qu',
                'name' => 'Czech',
            ),
            63 =>
            array (
                'id' => 564,
                'language_code' => 'cz',
                'display_language_code' => 'ro',
                'name' => 'Cehă',
            ),
            64 =>
            array (
                'id' => 565,
                'language_code' => 'cz',
                'display_language_code' => 'ru',
                'name' => 'Чешский',
            ),
            65 =>
            array (
                'id' => 566,
                'language_code' => 'cz',
                'display_language_code' => 'sl',
                'name' => 'Češčina',
            ),
            66 =>
            array (
                'id' => 567,
                'language_code' => 'cz',
                'display_language_code' => 'so',
                'name' => 'Czech',
            ),
            67 =>
            array (
                'id' => 568,
                'language_code' => 'cz',
                'display_language_code' => 'sq',
                'name' => 'Czech',
            ),
            68 =>
            array (
                'id' => 569,
                'language_code' => 'cz',
                'display_language_code' => 'sr',
                'name' => 'чешки',
            ),
            69 =>
            array (
                'id' => 570,
                'language_code' => 'cz',
                'display_language_code' => 'sv',
                'name' => 'Tjeckiska',
            ),
            70 =>
            array (
                'id' => 571,
                'language_code' => 'cz',
                'display_language_code' => 'ta',
                'name' => 'Czech',
            ),
            71 =>
            array (
                'id' => 572,
                'language_code' => 'cz',
                'display_language_code' => 'th',
                'name' => 'เช็ก',
            ),
            72 =>
            array (
                'id' => 573,
                'language_code' => 'cz',
                'display_language_code' => 'tr',
                'name' => 'Çekçe',
            ),
            73 =>
            array (
                'id' => 574,
                'language_code' => 'cz',
                'display_language_code' => 'uk',
                'name' => 'Czech',
            ),
            74 =>
            array (
                'id' => 575,
                'language_code' => 'cz',
                'display_language_code' => 'ur',
                'name' => 'Czech',
            ),
            75 =>
            array (
                'id' => 576,
                'language_code' => 'cz',
                'display_language_code' => 'uz',
                'name' => 'Czech',
            ),
            76 =>
            array (
                'id' => 577,
                'language_code' => 'cz',
                'display_language_code' => 'vi',
                'name' => 'Czech',
            ),
            77 =>
            array (
                'id' => 578,
                'language_code' => 'cz',
                'display_language_code' => 'yi',
                'name' => 'Czech',
            ),
            78 =>
            array (
                'id' => 579,
                'language_code' => 'cz',
                'display_language_code' => 'zh-hans',
                'name' => '捷克语',
            ),
            79 =>
            array (
                'id' => 580,
                'language_code' => 'cz',
                'display_language_code' => 'zu',
                'name' => 'Czech',
            ),
            80 =>
            array (
                'id' => 581,
                'language_code' => 'cz',
                'display_language_code' => 'zh-hant',
                'name' => '捷克語',
            ),
            81 =>
            array (
                'id' => 582,
                'language_code' => 'cz',
                'display_language_code' => 'ms',
                'name' => 'Czech',
            ),
            82 =>
            array (
                'id' => 583,
                'language_code' => 'cz',
                'display_language_code' => 'gl',
                'name' => 'Czech',
            ),
            83 =>
            array (
                'id' => 584,
                'language_code' => 'cz',
                'display_language_code' => 'bn',
                'name' => 'Czech',
            ),
            84 =>
            array (
                'id' => 585,
                'language_code' => 'cz',
                'display_language_code' => 'az',
                'name' => 'Czech',
            ),
            85 =>
            array (
                'id' => 586,
                'language_code' => 'sk',
                'display_language_code' => 'en',
                'name' => 'Slovak',
            ),
            86 =>
            array (
                'id' => 587,
                'language_code' => 'sk',
                'display_language_code' => 'es',
                'name' => 'Eslavo',
            ),
            87 =>
            array (
                'id' => 588,
                'language_code' => 'sk',
                'display_language_code' => 'de',
                'name' => 'Slowakisch',
            ),
            88 =>
            array (
                'id' => 589,
                'language_code' => 'sk',
                'display_language_code' => 'fr',
                'name' => 'Slave',
            ),
            89 =>
            array (
                'id' => 590,
                'language_code' => 'sk',
                'display_language_code' => 'ar',
                'name' => 'السلافية',
            ),
            90 =>
            array (
                'id' => 591,
                'language_code' => 'sk',
                'display_language_code' => 'bs',
                'name' => 'Slovak',
            ),
            91 =>
            array (
                'id' => 592,
                'language_code' => 'sk',
                'display_language_code' => 'bg',
                'name' => 'Словашки',
            ),
            92 =>
            array (
                'id' => 593,
                'language_code' => 'sk',
                'display_language_code' => 'ca',
                'name' => 'Slovak',
            ),
            93 =>
            array (
                'id' => 594,
                'language_code' => 'sk',
                'display_language_code' => 'cs',
                'name' => 'Slovenština',
            ),
            94 =>
            array (
                'id' => 595,
                'language_code' => 'sk',
                'display_language_code' => 'sk',
                'name' => 'Slovenčina',
            ),
            95 =>
            array (
                'id' => 596,
                'language_code' => 'sk',
                'display_language_code' => 'cy',
                'name' => 'Slovak',
            ),
            96 =>
            array (
                'id' => 597,
                'language_code' => 'sk',
                'display_language_code' => 'da',
                'name' => 'Slovak',
            ),
            97 =>
            array (
                'id' => 598,
                'language_code' => 'sk',
                'display_language_code' => 'el',
                'name' => 'Σλαβική',
            ),
            98 =>
            array (
                'id' => 599,
                'language_code' => 'sk',
                'display_language_code' => 'eo',
                'name' => 'Slovak',
            ),
            99 =>
            array (
                'id' => 600,
                'language_code' => 'sk',
                'display_language_code' => 'et',
                'name' => 'Slovak',
            ),
            100 =>
            array (
                'id' => 601,
                'language_code' => 'sk',
                'display_language_code' => 'eu',
                'name' => 'Slovak',
            ),
            101 =>
            array (
                'id' => 602,
                'language_code' => 'sk',
                'display_language_code' => 'fa',
                'name' => 'Slovak',
            ),
            102 =>
            array (
                'id' => 603,
                'language_code' => 'sk',
                'display_language_code' => 'fi',
                'name' => 'Slaavi',
            ),
            103 =>
            array (
                'id' => 604,
                'language_code' => 'sk',
                'display_language_code' => 'ga',
                'name' => 'Slovak',
            ),
            104 =>
            array (
                'id' => 605,
                'language_code' => 'sk',
                'display_language_code' => 'he',
                'name' => 'סלאבית',
            ),
            105 =>
            array (
                'id' => 606,
                'language_code' => 'sk',
                'display_language_code' => 'hi',
                'name' => 'Slovak',
            ),
            106 =>
            array (
                'id' => 607,
                'language_code' => 'sk',
                'display_language_code' => 'hr',
                'name' => 'Slovački',
            ),
            107 =>
            array (
                'id' => 608,
                'language_code' => 'sk',
                'display_language_code' => 'hu',
                'name' => 'Szlovák',
            ),
            108 =>
            array (
                'id' => 609,
                'language_code' => 'sk',
                'display_language_code' => 'hy',
                'name' => 'Slovak',
            ),
            109 =>
            array (
                'id' => 610,
                'language_code' => 'sk',
                'display_language_code' => 'id',
                'name' => 'Slovak',
            ),
            110 =>
            array (
                'id' => 611,
                'language_code' => 'sk',
                'display_language_code' => 'is',
                'name' => 'Slovak',
            ),
            111 =>
            array (
                'id' => 612,
                'language_code' => 'sk',
                'display_language_code' => 'it',
                'name' => 'Slavo',
            ),
            112 =>
            array (
                'id' => 613,
                'language_code' => 'sk',
                'display_language_code' => 'ja',
                'name' => 'スラヴ語派',
            ),
            113 =>
            array (
                'id' => 614,
                'language_code' => 'sk',
                'display_language_code' => 'ko',
                'name' => '슬라브어',
            ),
            114 =>
            array (
                'id' => 615,
                'language_code' => 'sk',
                'display_language_code' => 'ku',
                'name' => 'Slovak',
            ),
            115 =>
            array (
                'id' => 616,
                'language_code' => 'sk',
                'display_language_code' => 'lv',
                'name' => 'Slovak',
            ),
            116 =>
            array (
                'id' => 617,
                'language_code' => 'sk',
                'display_language_code' => 'lt',
                'name' => 'Slovak',
            ),
            117 =>
            array (
                'id' => 618,
                'language_code' => 'sk',
                'display_language_code' => 'mk',
                'name' => 'Slovak',
            ),
            118 =>
            array (
                'id' => 619,
                'language_code' => 'sk',
                'display_language_code' => 'mt',
                'name' => 'Slovak',
            ),
            119 =>
            array (
                'id' => 620,
                'language_code' => 'sk',
                'display_language_code' => 'mn',
                'name' => 'Slovak',
            ),
            120 =>
            array (
                'id' => 621,
                'language_code' => 'sk',
                'display_language_code' => 'ne',
                'name' => 'Slovak',
            ),
            121 =>
            array (
                'id' => 622,
                'language_code' => 'sk',
                'display_language_code' => 'nl',
                'name' => 'Slavisch',
            ),
            122 =>
            array (
                'id' => 623,
                'language_code' => 'sk',
                'display_language_code' => 'no',
                'name' => 'Slovak',
            ),
            123 =>
            array (
                'id' => 624,
                'language_code' => 'sk',
                'display_language_code' => 'pa',
                'name' => 'Slovak',
            ),
            124 =>
            array (
                'id' => 625,
                'language_code' => 'sk',
                'display_language_code' => 'pl',
                'name' => 'Słowacki',
            ),
            125 =>
            array (
                'id' => 626,
                'language_code' => 'sk',
                'display_language_code' => 'pt-pt',
                'name' => 'Eslavo',
            ),
            126 =>
            array (
                'id' => 627,
                'language_code' => 'sk',
                'display_language_code' => 'pt-br',
                'name' => 'Eslavo',
            ),
            127 =>
            array (
                'id' => 628,
                'language_code' => 'sk',
                'display_language_code' => 'qu',
                'name' => 'Slovak',
            ),
            128 =>
            array (
                'id' => 629,
                'language_code' => 'sk',
                'display_language_code' => 'ro',
                'name' => 'Slavă',
            ),
            129 =>
            array (
                'id' => 630,
                'language_code' => 'sk',
                'display_language_code' => 'ru',
                'name' => 'Славянский',
            ),
            130 =>
            array (
                'id' => 631,
                'language_code' => 'sk',
                'display_language_code' => 'sl',
                'name' => 'Slovaščina',
            ),
            131 =>
            array (
                'id' => 632,
                'language_code' => 'sk',
                'display_language_code' => 'so',
                'name' => 'Slovak',
            ),
            132 =>
            array (
                'id' => 633,
                'language_code' => 'sk',
                'display_language_code' => 'sq',
                'name' => 'Slovak',
            ),
            133 =>
            array (
                'id' => 634,
                'language_code' => 'sk',
                'display_language_code' => 'sr',
                'name' => 'словачки',
            ),
            134 =>
            array (
                'id' => 635,
                'language_code' => 'sk',
                'display_language_code' => 'sv',
                'name' => 'Slavisk',
            ),
            135 =>
            array (
                'id' => 636,
                'language_code' => 'sk',
                'display_language_code' => 'ta',
                'name' => 'Slovak',
            ),
            136 =>
            array (
                'id' => 637,
                'language_code' => 'sk',
                'display_language_code' => 'th',
                'name' => 'สลาวิก',
            ),
            137 =>
            array (
                'id' => 638,
                'language_code' => 'sk',
                'display_language_code' => 'tr',
                'name' => 'Slav dili',
            ),
            138 =>
            array (
                'id' => 639,
                'language_code' => 'sk',
                'display_language_code' => 'uk',
                'name' => 'Slovak',
            ),
            139 =>
            array (
                'id' => 640,
                'language_code' => 'sk',
                'display_language_code' => 'ur',
                'name' => 'Slovak',
            ),
            140 =>
            array (
                'id' => 641,
                'language_code' => 'sk',
                'display_language_code' => 'uz',
                'name' => 'Slovak',
            ),
            141 =>
            array (
                'id' => 642,
                'language_code' => 'sk',
                'display_language_code' => 'vi',
                'name' => 'Slovak',
            ),
            142 =>
            array (
                'id' => 643,
                'language_code' => 'sk',
                'display_language_code' => 'yi',
                'name' => 'Slovak',
            ),
            143 =>
            array (
                'id' => 644,
                'language_code' => 'sk',
                'display_language_code' => 'zh-hans',
                'name' => '斯拉夫语',
            ),
            144 =>
            array (
                'id' => 645,
                'language_code' => 'sk',
                'display_language_code' => 'zu',
                'name' => 'Slovak',
            ),
            145 =>
            array (
                'id' => 646,
                'language_code' => 'sk',
                'display_language_code' => 'zh-hant',
                'name' => '斯拉夫語',
            ),
            146 =>
            array (
                'id' => 647,
                'language_code' => 'sk',
                'display_language_code' => 'ms',
                'name' => 'Slovak',
            ),
            147 =>
            array (
                'id' => 648,
                'language_code' => 'sk',
                'display_language_code' => 'gl',
                'name' => 'Slovak',
            ),
            148 =>
            array (
                'id' => 649,
                'language_code' => 'sk',
                'display_language_code' => 'bn',
                'name' => 'Slovak',
            ),
            149 =>
            array (
                'id' => 650,
                'language_code' => 'sk',
                'display_language_code' => 'az',
                'name' => 'Slovak',
            ),
            150 =>
            array (
                'id' => 651,
                'language_code' => 'cy',
                'display_language_code' => 'en',
                'name' => 'Welsh',
            ),
            151 =>
            array (
                'id' => 652,
                'language_code' => 'cy',
                'display_language_code' => 'es',
                'name' => 'Galés',
            ),
            152 =>
            array (
                'id' => 653,
                'language_code' => 'cy',
                'display_language_code' => 'de',
                'name' => 'Walisisch',
            ),
            153 =>
            array (
                'id' => 654,
                'language_code' => 'cy',
                'display_language_code' => 'fr',
                'name' => 'Gallois',
            ),
            154 =>
            array (
                'id' => 655,
                'language_code' => 'cy',
                'display_language_code' => 'ar',
                'name' => 'الولزية',
            ),
            155 =>
            array (
                'id' => 656,
                'language_code' => 'cy',
                'display_language_code' => 'bs',
                'name' => 'Welsh',
            ),
            156 =>
            array (
                'id' => 657,
                'language_code' => 'cy',
                'display_language_code' => 'bg',
                'name' => 'Уелски',
            ),
            157 =>
            array (
                'id' => 658,
                'language_code' => 'cy',
                'display_language_code' => 'ca',
                'name' => 'Welsh',
            ),
            158 =>
            array (
                'id' => 659,
                'language_code' => 'cy',
                'display_language_code' => 'cs',
                'name' => 'Velšský',
            ),
            159 =>
            array (
                'id' => 660,
                'language_code' => 'cy',
                'display_language_code' => 'sk',
                'name' => 'Welština',
            ),
            160 =>
            array (
                'id' => 661,
                'language_code' => 'cy',
                'display_language_code' => 'cy',
                'name' => 'Cymraeg',
            ),
            161 =>
            array (
                'id' => 662,
                'language_code' => 'cy',
                'display_language_code' => 'da',
                'name' => 'Welsh',
            ),
            162 =>
            array (
                'id' => 663,
                'language_code' => 'cy',
                'display_language_code' => 'el',
                'name' => 'Oυαλικά',
            ),
            163 =>
            array (
                'id' => 664,
                'language_code' => 'cy',
                'display_language_code' => 'eo',
                'name' => 'Welsh',
            ),
            164 =>
            array (
                'id' => 665,
                'language_code' => 'cy',
                'display_language_code' => 'et',
                'name' => 'Welsh',
            ),
            165 =>
            array (
                'id' => 666,
                'language_code' => 'cy',
                'display_language_code' => 'eu',
                'name' => 'Welsh',
            ),
            166 =>
            array (
                'id' => 667,
                'language_code' => 'cy',
                'display_language_code' => 'fa',
                'name' => 'Welsh',
            ),
            167 =>
            array (
                'id' => 668,
                'language_code' => 'cy',
                'display_language_code' => 'fi',
                'name' => 'Kymri',
            ),
            168 =>
            array (
                'id' => 669,
                'language_code' => 'cy',
                'display_language_code' => 'ga',
                'name' => 'Welsh',
            ),
            169 =>
            array (
                'id' => 670,
                'language_code' => 'cy',
                'display_language_code' => 'he',
                'name' => 'וולשית',
            ),
            170 =>
            array (
                'id' => 671,
                'language_code' => 'cy',
                'display_language_code' => 'hi',
                'name' => 'Welsh',
            ),
            171 =>
            array (
                'id' => 672,
                'language_code' => 'cy',
                'display_language_code' => 'hr',
                'name' => 'Velški',
            ),
            172 =>
            array (
                'id' => 673,
                'language_code' => 'cy',
                'display_language_code' => 'hu',
                'name' => 'Vels',
            ),
            173 =>
            array (
                'id' => 674,
                'language_code' => 'cy',
                'display_language_code' => 'hy',
                'name' => 'Welsh',
            ),
            174 =>
            array (
                'id' => 675,
                'language_code' => 'cy',
                'display_language_code' => 'id',
                'name' => 'Welsh',
            ),
            175 =>
            array (
                'id' => 676,
                'language_code' => 'cy',
                'display_language_code' => 'is',
                'name' => 'Welsh',
            ),
            176 =>
            array (
                'id' => 677,
                'language_code' => 'cy',
                'display_language_code' => 'it',
                'name' => 'Gallese',
            ),
            177 =>
            array (
                'id' => 678,
                'language_code' => 'cy',
                'display_language_code' => 'ja',
                'name' => 'ウェールズ語',
            ),
            178 =>
            array (
                'id' => 679,
                'language_code' => 'cy',
                'display_language_code' => 'ko',
                'name' => '웨일즈어',
            ),
            179 =>
            array (
                'id' => 680,
                'language_code' => 'cy',
                'display_language_code' => 'ku',
                'name' => 'Welsh',
            ),
            180 =>
            array (
                'id' => 681,
                'language_code' => 'cy',
                'display_language_code' => 'lv',
                'name' => 'Welsh',
            ),
            181 =>
            array (
                'id' => 682,
                'language_code' => 'cy',
                'display_language_code' => 'lt',
                'name' => 'Welsh',
            ),
            182 =>
            array (
                'id' => 683,
                'language_code' => 'cy',
                'display_language_code' => 'mk',
                'name' => 'Welsh',
            ),
            183 =>
            array (
                'id' => 684,
                'language_code' => 'cy',
                'display_language_code' => 'mt',
                'name' => 'Welsh',
            ),
            184 =>
            array (
                'id' => 685,
                'language_code' => 'cy',
                'display_language_code' => 'mn',
                'name' => 'Welsh',
            ),
            185 =>
            array (
                'id' => 686,
                'language_code' => 'cy',
                'display_language_code' => 'ne',
                'name' => 'Welsh',
            ),
            186 =>
            array (
                'id' => 687,
                'language_code' => 'cy',
                'display_language_code' => 'nl',
                'name' => 'Welsh',
            ),
            187 =>
            array (
                'id' => 688,
                'language_code' => 'cy',
                'display_language_code' => 'no',
                'name' => 'Welsh',
            ),
            188 =>
            array (
                'id' => 689,
                'language_code' => 'cy',
                'display_language_code' => 'pa',
                'name' => 'Welsh',
            ),
            189 =>
            array (
                'id' => 690,
                'language_code' => 'cy',
                'display_language_code' => 'pl',
                'name' => 'Walijski',
            ),
            190 =>
            array (
                'id' => 691,
                'language_code' => 'cy',
                'display_language_code' => 'pt-pt',
                'name' => 'Galês',
            ),
            191 =>
            array (
                'id' => 692,
                'language_code' => 'cy',
                'display_language_code' => 'pt-br',
                'name' => 'Galês',
            ),
            192 =>
            array (
                'id' => 693,
                'language_code' => 'cy',
                'display_language_code' => 'qu',
                'name' => 'Welsh',
            ),
            193 =>
            array (
                'id' => 694,
                'language_code' => 'cy',
                'display_language_code' => 'ro',
                'name' => 'Galeză',
            ),
            194 =>
            array (
                'id' => 695,
                'language_code' => 'cy',
                'display_language_code' => 'ru',
                'name' => 'Валлийский',
            ),
            195 =>
            array (
                'id' => 696,
                'language_code' => 'cy',
                'display_language_code' => 'sl',
                'name' => 'Welsh',
            ),
            196 =>
            array (
                'id' => 697,
                'language_code' => 'cy',
                'display_language_code' => 'so',
                'name' => 'Welsh',
            ),
            197 =>
            array (
                'id' => 698,
                'language_code' => 'cy',
                'display_language_code' => 'sq',
                'name' => 'Welsh',
            ),
            198 =>
            array (
                'id' => 699,
                'language_code' => 'cy',
                'display_language_code' => 'sr',
                'name' => 'велшки',
            ),
            199 =>
            array (
                'id' => 700,
                'language_code' => 'cy',
                'display_language_code' => 'sv',
                'name' => 'Walesiska',
            ),
            200 =>
            array (
                'id' => 701,
                'language_code' => 'cy',
                'display_language_code' => 'ta',
                'name' => 'Welsh',
            ),
            201 =>
            array (
                'id' => 702,
                'language_code' => 'cy',
                'display_language_code' => 'th',
                'name' => 'เวลช์',
            ),
            202 =>
            array (
                'id' => 703,
                'language_code' => 'cy',
                'display_language_code' => 'tr',
                'name' => 'Galce',
            ),
            203 =>
            array (
                'id' => 704,
                'language_code' => 'cy',
                'display_language_code' => 'uk',
                'name' => 'Welsh',
            ),
            204 =>
            array (
                'id' => 705,
                'language_code' => 'cy',
                'display_language_code' => 'ur',
                'name' => 'Welsh',
            ),
            205 =>
            array (
                'id' => 706,
                'language_code' => 'cy',
                'display_language_code' => 'uz',
                'name' => 'Welsh',
            ),
            206 =>
            array (
                'id' => 707,
                'language_code' => 'cy',
                'display_language_code' => 'vi',
                'name' => 'Welsh',
            ),
            207 =>
            array (
                'id' => 708,
                'language_code' => 'cy',
                'display_language_code' => 'yi',
                'name' => 'Welsh',
            ),
            208 =>
            array (
                'id' => 709,
                'language_code' => 'cy',
                'display_language_code' => 'zh-hans',
                'name' => '威尔士语',
            ),
            209 =>
            array (
                'id' => 710,
                'language_code' => 'cy',
                'display_language_code' => 'zu',
                'name' => 'Welsh',
            ),
            210 =>
            array (
                'id' => 711,
                'language_code' => 'cy',
                'display_language_code' => 'zh-hant',
                'name' => '威爾士語',
            ),
            211 =>
            array (
                'id' => 712,
                'language_code' => 'cy',
                'display_language_code' => 'ms',
                'name' => 'Welsh',
            ),
            212 =>
            array (
                'id' => 713,
                'language_code' => 'cy',
                'display_language_code' => 'gl',
                'name' => 'Welsh',
            ),
            213 =>
            array (
                'id' => 714,
                'language_code' => 'cy',
                'display_language_code' => 'bn',
                'name' => 'Welsh',
            ),
            214 =>
            array (
                'id' => 715,
                'language_code' => 'cy',
                'display_language_code' => 'az',
                'name' => 'Welsh',
            ),
            215 =>
            array (
                'id' => 716,
                'language_code' => 'da',
                'display_language_code' => 'en',
                'name' => 'Danish',
            ),
            216 =>
            array (
                'id' => 717,
                'language_code' => 'da',
                'display_language_code' => 'es',
                'name' => 'Danés',
            ),
            217 =>
            array (
                'id' => 718,
                'language_code' => 'da',
                'display_language_code' => 'de',
                'name' => 'Dänisch',
            ),
            218 =>
            array (
                'id' => 719,
                'language_code' => 'da',
                'display_language_code' => 'fr',
                'name' => 'Danois',
            ),
            219 =>
            array (
                'id' => 720,
                'language_code' => 'da',
                'display_language_code' => 'ar',
                'name' => 'الدانماركية',
            ),
            220 =>
            array (
                'id' => 721,
                'language_code' => 'da',
                'display_language_code' => 'bs',
                'name' => 'Danish',
            ),
            221 =>
            array (
                'id' => 722,
                'language_code' => 'da',
                'display_language_code' => 'bg',
                'name' => 'Датски',
            ),
            222 =>
            array (
                'id' => 723,
                'language_code' => 'da',
                'display_language_code' => 'ca',
                'name' => 'Danish',
            ),
            223 =>
            array (
                'id' => 724,
                'language_code' => 'da',
                'display_language_code' => 'cs',
                'name' => 'Dánský',
            ),
            224 =>
            array (
                'id' => 725,
                'language_code' => 'da',
                'display_language_code' => 'sk',
                'name' => 'Dánčina',
            ),
            225 =>
            array (
                'id' => 726,
                'language_code' => 'da',
                'display_language_code' => 'cy',
                'name' => 'Danish',
            ),
            226 =>
            array (
                'id' => 727,
                'language_code' => 'da',
                'display_language_code' => 'da',
                'name' => 'Dansk',
            ),
            227 =>
            array (
                'id' => 728,
                'language_code' => 'da',
                'display_language_code' => 'el',
                'name' => 'Δανέζικα',
            ),
            228 =>
            array (
                'id' => 729,
                'language_code' => 'da',
                'display_language_code' => 'eo',
                'name' => 'Danish',
            ),
            229 =>
            array (
                'id' => 730,
                'language_code' => 'da',
                'display_language_code' => 'et',
                'name' => 'Danish',
            ),
            230 =>
            array (
                'id' => 731,
                'language_code' => 'da',
                'display_language_code' => 'eu',
                'name' => 'Danish',
            ),
            231 =>
            array (
                'id' => 732,
                'language_code' => 'da',
                'display_language_code' => 'fa',
                'name' => 'Danish',
            ),
            232 =>
            array (
                'id' => 733,
                'language_code' => 'da',
                'display_language_code' => 'fi',
                'name' => 'Tanska',
            ),
            233 =>
            array (
                'id' => 734,
                'language_code' => 'da',
                'display_language_code' => 'ga',
                'name' => 'Danish',
            ),
            234 =>
            array (
                'id' => 735,
                'language_code' => 'da',
                'display_language_code' => 'he',
                'name' => 'דנית',
            ),
            235 =>
            array (
                'id' => 736,
                'language_code' => 'da',
                'display_language_code' => 'hi',
                'name' => 'Danish',
            ),
            236 =>
            array (
                'id' => 737,
                'language_code' => 'da',
                'display_language_code' => 'hr',
                'name' => 'Danski',
            ),
            237 =>
            array (
                'id' => 738,
                'language_code' => 'da',
                'display_language_code' => 'hu',
                'name' => 'Dán',
            ),
            238 =>
            array (
                'id' => 739,
                'language_code' => 'da',
                'display_language_code' => 'hy',
                'name' => 'Danish',
            ),
            239 =>
            array (
                'id' => 740,
                'language_code' => 'da',
                'display_language_code' => 'id',
                'name' => 'Danish',
            ),
            240 =>
            array (
                'id' => 741,
                'language_code' => 'da',
                'display_language_code' => 'is',
                'name' => 'Danish',
            ),
            241 =>
            array (
                'id' => 742,
                'language_code' => 'da',
                'display_language_code' => 'it',
                'name' => 'Danese',
            ),
            242 =>
            array (
                'id' => 743,
                'language_code' => 'da',
                'display_language_code' => 'ja',
                'name' => 'デンマーク語',
            ),
            243 =>
            array (
                'id' => 744,
                'language_code' => 'da',
                'display_language_code' => 'ko',
                'name' => '덴마크어',
            ),
            244 =>
            array (
                'id' => 745,
                'language_code' => 'da',
                'display_language_code' => 'ku',
                'name' => 'Danish',
            ),
            245 =>
            array (
                'id' => 746,
                'language_code' => 'da',
                'display_language_code' => 'lv',
                'name' => 'Danish',
            ),
            246 =>
            array (
                'id' => 747,
                'language_code' => 'da',
                'display_language_code' => 'lt',
                'name' => 'Danish',
            ),
            247 =>
            array (
                'id' => 748,
                'language_code' => 'da',
                'display_language_code' => 'mk',
                'name' => 'Danish',
            ),
            248 =>
            array (
                'id' => 749,
                'language_code' => 'da',
                'display_language_code' => 'mt',
                'name' => 'Danish',
            ),
            249 =>
            array (
                'id' => 750,
                'language_code' => 'da',
                'display_language_code' => 'mn',
                'name' => 'Danish',
            ),
            250 =>
            array (
                'id' => 751,
                'language_code' => 'da',
                'display_language_code' => 'ne',
                'name' => 'Danish',
            ),
            251 =>
            array (
                'id' => 752,
                'language_code' => 'da',
                'display_language_code' => 'nl',
                'name' => 'Deens',
            ),
            252 =>
            array (
                'id' => 753,
                'language_code' => 'da',
                'display_language_code' => 'no',
                'name' => 'Danish',
            ),
            253 =>
            array (
                'id' => 754,
                'language_code' => 'da',
                'display_language_code' => 'pa',
                'name' => 'Danish',
            ),
            254 =>
            array (
                'id' => 755,
                'language_code' => 'da',
                'display_language_code' => 'pl',
                'name' => 'Duński',
            ),
            255 =>
            array (
                'id' => 756,
                'language_code' => 'da',
                'display_language_code' => 'pt-pt',
                'name' => 'Dinamarquês',
            ),
            256 =>
            array (
                'id' => 757,
                'language_code' => 'da',
                'display_language_code' => 'pt-br',
                'name' => 'Dinamarquês',
            ),
            257 =>
            array (
                'id' => 758,
                'language_code' => 'da',
                'display_language_code' => 'qu',
                'name' => 'Danish',
            ),
            258 =>
            array (
                'id' => 759,
                'language_code' => 'da',
                'display_language_code' => 'ro',
                'name' => 'Daneză',
            ),
            259 =>
            array (
                'id' => 760,
                'language_code' => 'da',
                'display_language_code' => 'ru',
                'name' => 'Датский',
            ),
            260 =>
            array (
                'id' => 761,
                'language_code' => 'da',
                'display_language_code' => 'sl',
                'name' => 'Danščina',
            ),
            261 =>
            array (
                'id' => 762,
                'language_code' => 'da',
                'display_language_code' => 'so',
                'name' => 'Danish',
            ),
            262 =>
            array (
                'id' => 763,
                'language_code' => 'da',
                'display_language_code' => 'sq',
                'name' => 'Danish',
            ),
            263 =>
            array (
                'id' => 764,
                'language_code' => 'da',
                'display_language_code' => 'sr',
                'name' => 'дански',
            ),
            264 =>
            array (
                'id' => 765,
                'language_code' => 'da',
                'display_language_code' => 'sv',
                'name' => 'Danska',
            ),
            265 =>
            array (
                'id' => 766,
                'language_code' => 'da',
                'display_language_code' => 'ta',
                'name' => 'Danish',
            ),
            266 =>
            array (
                'id' => 767,
                'language_code' => 'da',
                'display_language_code' => 'th',
                'name' => 'เดนมาร์ก',
            ),
            267 =>
            array (
                'id' => 768,
                'language_code' => 'da',
                'display_language_code' => 'tr',
                'name' => 'Danca',
            ),
            268 =>
            array (
                'id' => 769,
                'language_code' => 'da',
                'display_language_code' => 'uk',
                'name' => 'Danish',
            ),
            269 =>
            array (
                'id' => 770,
                'language_code' => 'da',
                'display_language_code' => 'ur',
                'name' => 'Danish',
            ),
            270 =>
            array (
                'id' => 771,
                'language_code' => 'da',
                'display_language_code' => 'uz',
                'name' => 'Danish',
            ),
            271 =>
            array (
                'id' => 772,
                'language_code' => 'da',
                'display_language_code' => 'vi',
                'name' => 'Danish',
            ),
            272 =>
            array (
                'id' => 773,
                'language_code' => 'da',
                'display_language_code' => 'yi',
                'name' => 'Danish',
            ),
            273 =>
            array (
                'id' => 774,
                'language_code' => 'da',
                'display_language_code' => 'zh-hans',
                'name' => '丹麦语',
            ),
            274 =>
            array (
                'id' => 775,
                'language_code' => 'da',
                'display_language_code' => 'zu',
                'name' => 'Danish',
            ),
            275 =>
            array (
                'id' => 776,
                'language_code' => 'da',
                'display_language_code' => 'zh-hant',
                'name' => '丹麥語',
            ),
            276 =>
            array (
                'id' => 777,
                'language_code' => 'da',
                'display_language_code' => 'ms',
                'name' => 'Danish',
            ),
            277 =>
            array (
                'id' => 778,
                'language_code' => 'da',
                'display_language_code' => 'gl',
                'name' => 'Danish',
            ),
            278 =>
            array (
                'id' => 779,
                'language_code' => 'da',
                'display_language_code' => 'bn',
                'name' => 'Danish',
            ),
            279 =>
            array (
                'id' => 780,
                'language_code' => 'da',
                'display_language_code' => 'az',
                'name' => 'Danish',
            ),
            280 =>
            array (
                'id' => 781,
                'language_code' => 'el',
                'display_language_code' => 'en',
                'name' => 'Greek',
            ),
            281 =>
            array (
                'id' => 782,
                'language_code' => 'el',
                'display_language_code' => 'es',
                'name' => 'Griego',
            ),
            282 =>
            array (
                'id' => 783,
                'language_code' => 'el',
                'display_language_code' => 'de',
                'name' => 'Griechisch',
            ),
            283 =>
            array (
                'id' => 784,
                'language_code' => 'el',
                'display_language_code' => 'fr',
                'name' => 'Grec moderne',
            ),
            284 =>
            array (
                'id' => 785,
                'language_code' => 'el',
                'display_language_code' => 'ar',
                'name' => 'اليونانية',
            ),
            285 =>
            array (
                'id' => 786,
                'language_code' => 'el',
                'display_language_code' => 'bs',
                'name' => 'Greek',
            ),
            286 =>
            array (
                'id' => 787,
                'language_code' => 'el',
                'display_language_code' => 'bg',
                'name' => 'Гръцки',
            ),
            287 =>
            array (
                'id' => 788,
                'language_code' => 'el',
                'display_language_code' => 'ca',
                'name' => 'Greek',
            ),
            288 =>
            array (
                'id' => 789,
                'language_code' => 'el',
                'display_language_code' => 'cs',
                'name' => 'Řečtina',
            ),
            289 =>
            array (
                'id' => 790,
                'language_code' => 'el',
                'display_language_code' => 'sk',
                'name' => 'Gréčtina',
            ),
            290 =>
            array (
                'id' => 791,
                'language_code' => 'el',
                'display_language_code' => 'cy',
                'name' => 'Greek',
            ),
            291 =>
            array (
                'id' => 792,
                'language_code' => 'el',
                'display_language_code' => 'da',
                'name' => 'Greek',
            ),
            292 =>
            array (
                'id' => 793,
                'language_code' => 'el',
                'display_language_code' => 'el',
                'name' => 'Ελληνικά',
            ),
            293 =>
            array (
                'id' => 794,
                'language_code' => 'el',
                'display_language_code' => 'eo',
                'name' => 'Greek',
            ),
            294 =>
            array (
                'id' => 795,
                'language_code' => 'el',
                'display_language_code' => 'et',
                'name' => 'Greek',
            ),
            295 =>
            array (
                'id' => 796,
                'language_code' => 'el',
                'display_language_code' => 'eu',
                'name' => 'Greek',
            ),
            296 =>
            array (
                'id' => 797,
                'language_code' => 'el',
                'display_language_code' => 'fa',
                'name' => 'Greek',
            ),
            297 =>
            array (
                'id' => 798,
                'language_code' => 'el',
                'display_language_code' => 'fi',
                'name' => 'Kreikka',
            ),
            298 =>
            array (
                'id' => 799,
                'language_code' => 'el',
                'display_language_code' => 'ga',
                'name' => 'Greek',
            ),
            299 =>
            array (
                'id' => 800,
                'language_code' => 'el',
                'display_language_code' => 'he',
                'name' => 'יוונית',
            ),
            300 =>
            array (
                'id' => 801,
                'language_code' => 'el',
                'display_language_code' => 'hi',
                'name' => 'Greek',
            ),
            301 =>
            array (
                'id' => 802,
                'language_code' => 'el',
                'display_language_code' => 'hr',
                'name' => 'Grčki',
            ),
            302 =>
            array (
                'id' => 803,
                'language_code' => 'el',
                'display_language_code' => 'hu',
                'name' => 'Görög',
            ),
            303 =>
            array (
                'id' => 804,
                'language_code' => 'el',
                'display_language_code' => 'hy',
                'name' => 'Greek',
            ),
            304 =>
            array (
                'id' => 805,
                'language_code' => 'el',
                'display_language_code' => 'id',
                'name' => 'Greek',
            ),
            305 =>
            array (
                'id' => 806,
                'language_code' => 'el',
                'display_language_code' => 'is',
                'name' => 'Greek',
            ),
            306 =>
            array (
                'id' => 807,
                'language_code' => 'el',
                'display_language_code' => 'it',
                'name' => 'Greco',
            ),
            307 =>
            array (
                'id' => 808,
                'language_code' => 'el',
                'display_language_code' => 'ja',
                'name' => 'ギリシア語',
            ),
            308 =>
            array (
                'id' => 809,
                'language_code' => 'el',
                'display_language_code' => 'ko',
                'name' => '그리스어',
            ),
            309 =>
            array (
                'id' => 810,
                'language_code' => 'el',
                'display_language_code' => 'ku',
                'name' => 'Greek',
            ),
            310 =>
            array (
                'id' => 811,
                'language_code' => 'el',
                'display_language_code' => 'lv',
                'name' => 'Greek',
            ),
            311 =>
            array (
                'id' => 812,
                'language_code' => 'el',
                'display_language_code' => 'lt',
                'name' => 'Greek',
            ),
            312 =>
            array (
                'id' => 813,
                'language_code' => 'el',
                'display_language_code' => 'mk',
                'name' => 'Greek',
            ),
            313 =>
            array (
                'id' => 814,
                'language_code' => 'el',
                'display_language_code' => 'mt',
                'name' => 'Greek',
            ),
            314 =>
            array (
                'id' => 815,
                'language_code' => 'el',
                'display_language_code' => 'mn',
                'name' => 'Greek',
            ),
            315 =>
            array (
                'id' => 816,
                'language_code' => 'el',
                'display_language_code' => 'ne',
                'name' => 'Greek',
            ),
            316 =>
            array (
                'id' => 817,
                'language_code' => 'el',
                'display_language_code' => 'nl',
                'name' => 'Grieks',
            ),
            317 =>
            array (
                'id' => 818,
                'language_code' => 'el',
                'display_language_code' => 'no',
                'name' => 'Gresk',
            ),
            318 =>
            array (
                'id' => 819,
                'language_code' => 'el',
                'display_language_code' => 'pa',
                'name' => 'Greek',
            ),
            319 =>
            array (
                'id' => 820,
                'language_code' => 'el',
                'display_language_code' => 'pl',
                'name' => 'Grecki',
            ),
            320 =>
            array (
                'id' => 821,
                'language_code' => 'el',
                'display_language_code' => 'pt-pt',
                'name' => 'Grego',
            ),
            321 =>
            array (
                'id' => 822,
                'language_code' => 'el',
                'display_language_code' => 'pt-br',
                'name' => 'Grego',
            ),
            322 =>
            array (
                'id' => 823,
                'language_code' => 'el',
                'display_language_code' => 'qu',
                'name' => 'Greek',
            ),
            323 =>
            array (
                'id' => 824,
                'language_code' => 'el',
                'display_language_code' => 'ro',
                'name' => 'Greacă',
            ),
            324 =>
            array (
                'id' => 825,
                'language_code' => 'el',
                'display_language_code' => 'ru',
                'name' => 'Греческий',
            ),
            325 =>
            array (
                'id' => 826,
                'language_code' => 'el',
                'display_language_code' => 'sl',
                'name' => 'Greek',
            ),
            326 =>
            array (
                'id' => 827,
                'language_code' => 'el',
                'display_language_code' => 'so',
                'name' => 'Greek',
            ),
            327 =>
            array (
                'id' => 828,
                'language_code' => 'el',
                'display_language_code' => 'sq',
                'name' => 'Greek',
            ),
            328 =>
            array (
                'id' => 829,
                'language_code' => 'el',
                'display_language_code' => 'sr',
                'name' => 'грчки',
            ),
            329 =>
            array (
                'id' => 830,
                'language_code' => 'el',
                'display_language_code' => 'sv',
                'name' => 'Grekiska',
            ),
            330 =>
            array (
                'id' => 831,
                'language_code' => 'el',
                'display_language_code' => 'ta',
                'name' => 'Greek',
            ),
            331 =>
            array (
                'id' => 832,
                'language_code' => 'el',
                'display_language_code' => 'th',
                'name' => 'กรีก',
            ),
            332 =>
            array (
                'id' => 833,
                'language_code' => 'el',
                'display_language_code' => 'tr',
                'name' => 'Yunanca',
            ),
            333 =>
            array (
                'id' => 834,
                'language_code' => 'el',
                'display_language_code' => 'uk',
                'name' => 'Greek',
            ),
            334 =>
            array (
                'id' => 835,
                'language_code' => 'el',
                'display_language_code' => 'ur',
                'name' => 'Greek',
            ),
            335 =>
            array (
                'id' => 836,
                'language_code' => 'el',
                'display_language_code' => 'uz',
                'name' => 'Greek',
            ),
            336 =>
            array (
                'id' => 837,
                'language_code' => 'el',
                'display_language_code' => 'vi',
                'name' => 'Greek',
            ),
            337 =>
            array (
                'id' => 838,
                'language_code' => 'el',
                'display_language_code' => 'yi',
                'name' => 'Greek',
            ),
            338 =>
            array (
                'id' => 839,
                'language_code' => 'el',
                'display_language_code' => 'zh-hans',
                'name' => '希腊语',
            ),
            339 =>
            array (
                'id' => 840,
                'language_code' => 'el',
                'display_language_code' => 'zu',
                'name' => 'Greek',
            ),
            340 =>
            array (
                'id' => 841,
                'language_code' => 'el',
                'display_language_code' => 'zh-hant',
                'name' => '希臘語',
            ),
            341 =>
            array (
                'id' => 842,
                'language_code' => 'el',
                'display_language_code' => 'ms',
                'name' => 'Greek',
            ),
            342 =>
            array (
                'id' => 843,
                'language_code' => 'el',
                'display_language_code' => 'gl',
                'name' => 'Greek',
            ),
            343 =>
            array (
                'id' => 844,
                'language_code' => 'el',
                'display_language_code' => 'bn',
                'name' => 'Greek',
            ),
            344 =>
            array (
                'id' => 845,
                'language_code' => 'el',
                'display_language_code' => 'az',
                'name' => 'Greek',
            ),
            345 =>
            array (
                'id' => 846,
                'language_code' => 'eo',
                'display_language_code' => 'en',
                'name' => 'Esperanto',
            ),
            346 =>
            array (
                'id' => 847,
                'language_code' => 'eo',
                'display_language_code' => 'es',
                'name' => 'Esperanto',
            ),
            347 =>
            array (
                'id' => 848,
                'language_code' => 'eo',
                'display_language_code' => 'de',
                'name' => 'Esperanto',
            ),
            348 =>
            array (
                'id' => 849,
                'language_code' => 'eo',
                'display_language_code' => 'fr',
                'name' => 'Espéranto',
            ),
            349 =>
            array (
                'id' => 850,
                'language_code' => 'eo',
                'display_language_code' => 'ar',
                'name' => 'الاسبرانتو',
            ),
            350 =>
            array (
                'id' => 851,
                'language_code' => 'eo',
                'display_language_code' => 'bs',
                'name' => 'Esperanto',
            ),
            351 =>
            array (
                'id' => 852,
                'language_code' => 'eo',
                'display_language_code' => 'bg',
                'name' => 'Есперанто',
            ),
            352 =>
            array (
                'id' => 853,
                'language_code' => 'eo',
                'display_language_code' => 'ca',
                'name' => 'Esperanto',
            ),
            353 =>
            array (
                'id' => 854,
                'language_code' => 'eo',
                'display_language_code' => 'cs',
                'name' => 'Esperanto',
            ),
            354 =>
            array (
                'id' => 855,
                'language_code' => 'eo',
                'display_language_code' => 'sk',
                'name' => 'Esperanto',
            ),
            355 =>
            array (
                'id' => 856,
                'language_code' => 'eo',
                'display_language_code' => 'cy',
                'name' => 'Esperanto',
            ),
            356 =>
            array (
                'id' => 857,
                'language_code' => 'eo',
                'display_language_code' => 'da',
                'name' => 'Esperanto',
            ),
            357 =>
            array (
                'id' => 858,
                'language_code' => 'eo',
                'display_language_code' => 'el',
                'name' => 'Εσπεράντο',
            ),
            358 =>
            array (
                'id' => 859,
                'language_code' => 'eo',
                'display_language_code' => 'eo',
                'name' => 'Esperanta',
            ),
            359 =>
            array (
                'id' => 860,
                'language_code' => 'eo',
                'display_language_code' => 'et',
                'name' => 'Esperanto',
            ),
            360 =>
            array (
                'id' => 861,
                'language_code' => 'eo',
                'display_language_code' => 'eu',
                'name' => 'Esperanto',
            ),
            361 =>
            array (
                'id' => 862,
                'language_code' => 'eo',
                'display_language_code' => 'fa',
                'name' => 'Esperanto',
            ),
            362 =>
            array (
                'id' => 863,
                'language_code' => 'eo',
                'display_language_code' => 'fi',
                'name' => 'Esperanto',
            ),
            363 =>
            array (
                'id' => 864,
                'language_code' => 'eo',
                'display_language_code' => 'ga',
                'name' => 'Esperanto',
            ),
            364 =>
            array (
                'id' => 865,
                'language_code' => 'eo',
                'display_language_code' => 'he',
                'name' => 'אספרנטו',
            ),
            365 =>
            array (
                'id' => 866,
                'language_code' => 'eo',
                'display_language_code' => 'hi',
                'name' => 'Esperanto',
            ),
            366 =>
            array (
                'id' => 867,
                'language_code' => 'eo',
                'display_language_code' => 'hr',
                'name' => 'Esperanto',
            ),
            367 =>
            array (
                'id' => 868,
                'language_code' => 'eo',
                'display_language_code' => 'hu',
                'name' => 'Eszperantó',
            ),
            368 =>
            array (
                'id' => 869,
                'language_code' => 'eo',
                'display_language_code' => 'hy',
                'name' => 'Esperanto',
            ),
            369 =>
            array (
                'id' => 870,
                'language_code' => 'eo',
                'display_language_code' => 'id',
                'name' => 'Esperanto',
            ),
            370 =>
            array (
                'id' => 871,
                'language_code' => 'eo',
                'display_language_code' => 'is',
                'name' => 'Esperanto',
            ),
            371 =>
            array (
                'id' => 872,
                'language_code' => 'eo',
                'display_language_code' => 'it',
                'name' => 'Esperanto',
            ),
            372 =>
            array (
                'id' => 873,
                'language_code' => 'eo',
                'display_language_code' => 'ja',
                'name' => 'エスペラント語',
            ),
            373 =>
            array (
                'id' => 874,
                'language_code' => 'eo',
                'display_language_code' => 'ko',
                'name' => '에스페란토어',
            ),
            374 =>
            array (
                'id' => 875,
                'language_code' => 'eo',
                'display_language_code' => 'ku',
                'name' => 'Esperanto',
            ),
            375 =>
            array (
                'id' => 876,
                'language_code' => 'eo',
                'display_language_code' => 'lv',
                'name' => 'Esperanto',
            ),
            376 =>
            array (
                'id' => 877,
                'language_code' => 'eo',
                'display_language_code' => 'lt',
                'name' => 'Esperanto',
            ),
            377 =>
            array (
                'id' => 878,
                'language_code' => 'eo',
                'display_language_code' => 'mk',
                'name' => 'Esperanto',
            ),
            378 =>
            array (
                'id' => 879,
                'language_code' => 'eo',
                'display_language_code' => 'mt',
                'name' => 'Esperanto',
            ),
            379 =>
            array (
                'id' => 880,
                'language_code' => 'eo',
                'display_language_code' => 'mn',
                'name' => 'Esperanto',
            ),
            380 =>
            array (
                'id' => 881,
                'language_code' => 'eo',
                'display_language_code' => 'ne',
                'name' => 'Esperanto',
            ),
            381 =>
            array (
                'id' => 882,
                'language_code' => 'eo',
                'display_language_code' => 'nl',
                'name' => 'Esperanto',
            ),
            382 =>
            array (
                'id' => 883,
                'language_code' => 'eo',
                'display_language_code' => 'no',
                'name' => 'Esperanto',
            ),
            383 =>
            array (
                'id' => 884,
                'language_code' => 'eo',
                'display_language_code' => 'pa',
                'name' => 'Esperanto',
            ),
            384 =>
            array (
                'id' => 885,
                'language_code' => 'eo',
                'display_language_code' => 'pl',
                'name' => 'Esperanto',
            ),
            385 =>
            array (
                'id' => 886,
                'language_code' => 'eo',
                'display_language_code' => 'pt-pt',
                'name' => 'Esperanto',
            ),
            386 =>
            array (
                'id' => 887,
                'language_code' => 'eo',
                'display_language_code' => 'pt-br',
                'name' => 'Esperanto',
            ),
            387 =>
            array (
                'id' => 888,
                'language_code' => 'eo',
                'display_language_code' => 'qu',
                'name' => 'Esperanto',
            ),
            388 =>
            array (
                'id' => 889,
                'language_code' => 'eo',
                'display_language_code' => 'ro',
                'name' => 'Esperanto',
            ),
            389 =>
            array (
                'id' => 890,
                'language_code' => 'eo',
                'display_language_code' => 'ru',
                'name' => 'Эсперанто',
            ),
            390 =>
            array (
                'id' => 891,
                'language_code' => 'eo',
                'display_language_code' => 'sl',
                'name' => 'Esperanto',
            ),
            391 =>
            array (
                'id' => 892,
                'language_code' => 'eo',
                'display_language_code' => 'so',
                'name' => 'Esperanto',
            ),
            392 =>
            array (
                'id' => 893,
                'language_code' => 'eo',
                'display_language_code' => 'sq',
                'name' => 'Esperanto',
            ),
            393 =>
            array (
                'id' => 894,
                'language_code' => 'eo',
                'display_language_code' => 'sr',
                'name' => 'есперанто',
            ),
            394 =>
            array (
                'id' => 895,
                'language_code' => 'eo',
                'display_language_code' => 'sv',
                'name' => 'Esperanto',
            ),
            395 =>
            array (
                'id' => 896,
                'language_code' => 'eo',
                'display_language_code' => 'ta',
                'name' => 'Esperanto',
            ),
            396 =>
            array (
                'id' => 897,
                'language_code' => 'eo',
                'display_language_code' => 'th',
                'name' => 'เอสเปอรันโต',
            ),
            397 =>
            array (
                'id' => 898,
                'language_code' => 'eo',
                'display_language_code' => 'tr',
                'name' => 'Esperanto',
            ),
            398 =>
            array (
                'id' => 899,
                'language_code' => 'eo',
                'display_language_code' => 'uk',
                'name' => 'Esperanto',
            ),
            399 =>
            array (
                'id' => 900,
                'language_code' => 'eo',
                'display_language_code' => 'ur',
                'name' => 'Esperanto',
            ),
            400 =>
            array (
                'id' => 901,
                'language_code' => 'eo',
                'display_language_code' => 'uz',
                'name' => 'Esperanto',
            ),
            401 =>
            array (
                'id' => 902,
                'language_code' => 'eo',
                'display_language_code' => 'vi',
                'name' => 'Esperanto',
            ),
            402 =>
            array (
                'id' => 903,
                'language_code' => 'eo',
                'display_language_code' => 'yi',
                'name' => 'Esperanto',
            ),
            403 =>
            array (
                'id' => 904,
                'language_code' => 'eo',
                'display_language_code' => 'zh-hans',
                'name' => '世界语',
            ),
            404 =>
            array (
                'id' => 905,
                'language_code' => 'eo',
                'display_language_code' => 'zu',
                'name' => 'Esperanto',
            ),
            405 =>
            array (
                'id' => 906,
                'language_code' => 'eo',
                'display_language_code' => 'zh-hant',
                'name' => '世界語',
            ),
            406 =>
            array (
                'id' => 907,
                'language_code' => 'eo',
                'display_language_code' => 'ms',
                'name' => 'Esperanto',
            ),
            407 =>
            array (
                'id' => 908,
                'language_code' => 'eo',
                'display_language_code' => 'gl',
                'name' => 'Esperanto',
            ),
            408 =>
            array (
                'id' => 909,
                'language_code' => 'eo',
                'display_language_code' => 'bn',
                'name' => 'Esperanto',
            ),
            409 =>
            array (
                'id' => 910,
                'language_code' => 'eo',
                'display_language_code' => 'az',
                'name' => 'Esperanto',
            ),
            410 =>
            array (
                'id' => 911,
                'language_code' => 'et',
                'display_language_code' => 'en',
                'name' => 'Estonian',
            ),
            411 =>
            array (
                'id' => 912,
                'language_code' => 'et',
                'display_language_code' => 'es',
                'name' => 'Estonio',
            ),
            412 =>
            array (
                'id' => 913,
                'language_code' => 'et',
                'display_language_code' => 'de',
                'name' => 'Estnisch',
            ),
            413 =>
            array (
                'id' => 914,
                'language_code' => 'et',
                'display_language_code' => 'fr',
                'name' => 'Estonien',
            ),
            414 =>
            array (
                'id' => 915,
                'language_code' => 'et',
                'display_language_code' => 'ar',
                'name' => 'الأستونية',
            ),
            415 =>
            array (
                'id' => 916,
                'language_code' => 'et',
                'display_language_code' => 'bs',
                'name' => 'Estonian',
            ),
            416 =>
            array (
                'id' => 917,
                'language_code' => 'et',
                'display_language_code' => 'bg',
                'name' => 'Естонски',
            ),
            417 =>
            array (
                'id' => 918,
                'language_code' => 'et',
                'display_language_code' => 'ca',
                'name' => 'Estonian',
            ),
            418 =>
            array (
                'id' => 919,
                'language_code' => 'et',
                'display_language_code' => 'cs',
                'name' => 'Estonština',
            ),
            419 =>
            array (
                'id' => 920,
                'language_code' => 'et',
                'display_language_code' => 'sk',
                'name' => 'Estónčina',
            ),
            420 =>
            array (
                'id' => 921,
                'language_code' => 'et',
                'display_language_code' => 'cy',
                'name' => 'Estonian',
            ),
            421 =>
            array (
                'id' => 922,
                'language_code' => 'et',
                'display_language_code' => 'da',
                'name' => 'Estonian',
            ),
            422 =>
            array (
                'id' => 923,
                'language_code' => 'et',
                'display_language_code' => 'el',
                'name' => 'Εσθονικά',
            ),
            423 =>
            array (
                'id' => 924,
                'language_code' => 'et',
                'display_language_code' => 'eo',
                'name' => 'Estonian',
            ),
            424 =>
            array (
                'id' => 925,
                'language_code' => 'et',
                'display_language_code' => 'et',
                'name' => 'Eesti',
            ),
            425 =>
            array (
                'id' => 926,
                'language_code' => 'et',
                'display_language_code' => 'eu',
                'name' => 'Estonian',
            ),
            426 =>
            array (
                'id' => 927,
                'language_code' => 'et',
                'display_language_code' => 'fa',
                'name' => 'Estonian',
            ),
            427 =>
            array (
                'id' => 928,
                'language_code' => 'et',
                'display_language_code' => 'fi',
                'name' => 'Eesti',
            ),
            428 =>
            array (
                'id' => 929,
                'language_code' => 'et',
                'display_language_code' => 'ga',
                'name' => 'Estonian',
            ),
            429 =>
            array (
                'id' => 930,
                'language_code' => 'et',
                'display_language_code' => 'he',
                'name' => 'אסטונית',
            ),
            430 =>
            array (
                'id' => 931,
                'language_code' => 'et',
                'display_language_code' => 'hi',
                'name' => 'Estonian',
            ),
            431 =>
            array (
                'id' => 932,
                'language_code' => 'et',
                'display_language_code' => 'hr',
                'name' => 'Estonski',
            ),
            432 =>
            array (
                'id' => 933,
                'language_code' => 'et',
                'display_language_code' => 'hu',
                'name' => 'észt',
            ),
            433 =>
            array (
                'id' => 934,
                'language_code' => 'et',
                'display_language_code' => 'hy',
                'name' => 'Estonian',
            ),
            434 =>
            array (
                'id' => 935,
                'language_code' => 'et',
                'display_language_code' => 'id',
                'name' => 'Estonian',
            ),
            435 =>
            array (
                'id' => 936,
                'language_code' => 'et',
                'display_language_code' => 'is',
                'name' => 'Estonian',
            ),
            436 =>
            array (
                'id' => 937,
                'language_code' => 'et',
                'display_language_code' => 'it',
                'name' => 'Estone',
            ),
            437 =>
            array (
                'id' => 938,
                'language_code' => 'et',
                'display_language_code' => 'ja',
                'name' => 'エストニア語',
            ),
            438 =>
            array (
                'id' => 939,
                'language_code' => 'et',
                'display_language_code' => 'ko',
                'name' => '에스토니아어',
            ),
            439 =>
            array (
                'id' => 940,
                'language_code' => 'et',
                'display_language_code' => 'ku',
                'name' => 'Estonian',
            ),
            440 =>
            array (
                'id' => 941,
                'language_code' => 'et',
                'display_language_code' => 'lv',
                'name' => 'Estonian',
            ),
            441 =>
            array (
                'id' => 942,
                'language_code' => 'et',
                'display_language_code' => 'lt',
                'name' => 'Estonian',
            ),
            442 =>
            array (
                'id' => 943,
                'language_code' => 'et',
                'display_language_code' => 'mk',
                'name' => 'Estonian',
            ),
            443 =>
            array (
                'id' => 944,
                'language_code' => 'et',
                'display_language_code' => 'mt',
                'name' => 'Estonian',
            ),
            444 =>
            array (
                'id' => 945,
                'language_code' => 'et',
                'display_language_code' => 'mn',
                'name' => 'Estonian',
            ),
            445 =>
            array (
                'id' => 946,
                'language_code' => 'et',
                'display_language_code' => 'ne',
                'name' => 'Estonian',
            ),
            446 =>
            array (
                'id' => 947,
                'language_code' => 'et',
                'display_language_code' => 'nl',
                'name' => 'Ests',
            ),
            447 =>
            array (
                'id' => 948,
                'language_code' => 'et',
                'display_language_code' => 'no',
                'name' => 'Estonian',
            ),
            448 =>
            array (
                'id' => 949,
                'language_code' => 'et',
                'display_language_code' => 'pa',
                'name' => 'Estonian',
            ),
            449 =>
            array (
                'id' => 950,
                'language_code' => 'et',
                'display_language_code' => 'pl',
                'name' => 'Estoński',
            ),
            450 =>
            array (
                'id' => 951,
                'language_code' => 'et',
                'display_language_code' => 'pt-pt',
                'name' => 'Estoniano',
            ),
            451 =>
            array (
                'id' => 952,
                'language_code' => 'et',
                'display_language_code' => 'pt-br',
                'name' => 'Estoniano',
            ),
            452 =>
            array (
                'id' => 953,
                'language_code' => 'et',
                'display_language_code' => 'qu',
                'name' => 'Estonian',
            ),
            453 =>
            array (
                'id' => 954,
                'language_code' => 'et',
                'display_language_code' => 'ro',
                'name' => 'Estoniană',
            ),
            454 =>
            array (
                'id' => 955,
                'language_code' => 'et',
                'display_language_code' => 'ru',
                'name' => 'Эстонский',
            ),
            455 =>
            array (
                'id' => 956,
                'language_code' => 'et',
                'display_language_code' => 'sl',
                'name' => 'Estonščina',
            ),
            456 =>
            array (
                'id' => 957,
                'language_code' => 'et',
                'display_language_code' => 'so',
                'name' => 'Estonian',
            ),
            457 =>
            array (
                'id' => 958,
                'language_code' => 'et',
                'display_language_code' => 'sq',
                'name' => 'Estonian',
            ),
            458 =>
            array (
                'id' => 959,
                'language_code' => 'et',
                'display_language_code' => 'sr',
                'name' => 'естонски',
            ),
            459 =>
            array (
                'id' => 960,
                'language_code' => 'et',
                'display_language_code' => 'sv',
                'name' => 'Estniska',
            ),
            460 =>
            array (
                'id' => 961,
                'language_code' => 'et',
                'display_language_code' => 'ta',
                'name' => 'Estonian',
            ),
            461 =>
            array (
                'id' => 962,
                'language_code' => 'et',
                'display_language_code' => 'th',
                'name' => 'เอสโตเนียน',
            ),
            462 =>
            array (
                'id' => 963,
                'language_code' => 'et',
                'display_language_code' => 'tr',
                'name' => 'Estonya dili',
            ),
            463 =>
            array (
                'id' => 964,
                'language_code' => 'et',
                'display_language_code' => 'uk',
                'name' => 'Estonian',
            ),
            464 =>
            array (
                'id' => 965,
                'language_code' => 'et',
                'display_language_code' => 'ur',
                'name' => 'Estonian',
            ),
            465 =>
            array (
                'id' => 966,
                'language_code' => 'et',
                'display_language_code' => 'uz',
                'name' => 'Estonian',
            ),
            466 =>
            array (
                'id' => 967,
                'language_code' => 'et',
                'display_language_code' => 'vi',
                'name' => 'Estonian',
            ),
            467 =>
            array (
                'id' => 968,
                'language_code' => 'et',
                'display_language_code' => 'yi',
                'name' => 'Estonian',
            ),
            468 =>
            array (
                'id' => 969,
                'language_code' => 'et',
                'display_language_code' => 'zh-hans',
                'name' => '爱沙尼亚语',
            ),
            469 =>
            array (
                'id' => 970,
                'language_code' => 'et',
                'display_language_code' => 'zu',
                'name' => 'Estonian',
            ),
            470 =>
            array (
                'id' => 971,
                'language_code' => 'et',
                'display_language_code' => 'zh-hant',
                'name' => '愛沙尼亞語',
            ),
            471 =>
            array (
                'id' => 972,
                'language_code' => 'et',
                'display_language_code' => 'ms',
                'name' => 'Estonian',
            ),
            472 =>
            array (
                'id' => 973,
                'language_code' => 'et',
                'display_language_code' => 'gl',
                'name' => 'Estonian',
            ),
            473 =>
            array (
                'id' => 974,
                'language_code' => 'et',
                'display_language_code' => 'bn',
                'name' => 'Estonian',
            ),
            474 =>
            array (
                'id' => 975,
                'language_code' => 'et',
                'display_language_code' => 'az',
                'name' => 'Estonian',
            ),
            475 =>
            array (
                'id' => 976,
                'language_code' => 'eu',
                'display_language_code' => 'en',
                'name' => 'Basque',
            ),
            476 =>
            array (
                'id' => 977,
                'language_code' => 'eu',
                'display_language_code' => 'es',
                'name' => 'Euskera',
            ),
            477 =>
            array (
                'id' => 978,
                'language_code' => 'eu',
                'display_language_code' => 'de',
                'name' => 'Baskisch',
            ),
            478 =>
            array (
                'id' => 979,
                'language_code' => 'eu',
                'display_language_code' => 'fr',
                'name' => 'Basque',
            ),
            479 =>
            array (
                'id' => 980,
                'language_code' => 'eu',
                'display_language_code' => 'ar',
                'name' => 'لغة الباسك',
            ),
            480 =>
            array (
                'id' => 981,
                'language_code' => 'eu',
                'display_language_code' => 'bs',
                'name' => 'Basque',
            ),
            481 =>
            array (
                'id' => 982,
                'language_code' => 'eu',
                'display_language_code' => 'bg',
                'name' => 'Баски',
            ),
            482 =>
            array (
                'id' => 983,
                'language_code' => 'eu',
                'display_language_code' => 'ca',
                'name' => 'Basque',
            ),
            483 =>
            array (
                'id' => 984,
                'language_code' => 'eu',
                'display_language_code' => 'cs',
                'name' => 'Basque',
            ),
            484 =>
            array (
                'id' => 985,
                'language_code' => 'eu',
                'display_language_code' => 'sk',
                'name' => 'Baskičtina',
            ),
            485 =>
            array (
                'id' => 986,
                'language_code' => 'eu',
                'display_language_code' => 'cy',
                'name' => 'Basque',
            ),
            486 =>
            array (
                'id' => 987,
                'language_code' => 'eu',
                'display_language_code' => 'da',
                'name' => 'Basque',
            ),
            487 =>
            array (
                'id' => 988,
                'language_code' => 'eu',
                'display_language_code' => 'el',
                'name' => 'Βασκικά',
            ),
            488 =>
            array (
                'id' => 989,
                'language_code' => 'eu',
                'display_language_code' => 'eo',
                'name' => 'Basque',
            ),
            489 =>
            array (
                'id' => 990,
                'language_code' => 'eu',
                'display_language_code' => 'et',
                'name' => 'Basque',
            ),
            490 =>
            array (
                'id' => 991,
                'language_code' => 'eu',
                'display_language_code' => 'eu',
                'name' => 'Euskara',
            ),
            491 =>
            array (
                'id' => 992,
                'language_code' => 'eu',
                'display_language_code' => 'fa',
                'name' => 'Basque',
            ),
            492 =>
            array (
                'id' => 993,
                'language_code' => 'eu',
                'display_language_code' => 'fi',
                'name' => 'Baski',
            ),
            493 =>
            array (
                'id' => 994,
                'language_code' => 'eu',
                'display_language_code' => 'ga',
                'name' => 'Basque',
            ),
            494 =>
            array (
                'id' => 995,
                'language_code' => 'eu',
                'display_language_code' => 'he',
                'name' => 'בסקית',
            ),
            495 =>
            array (
                'id' => 996,
                'language_code' => 'eu',
                'display_language_code' => 'hi',
                'name' => 'Basque',
            ),
            496 =>
            array (
                'id' => 997,
                'language_code' => 'eu',
                'display_language_code' => 'hr',
                'name' => 'Baskijski',
            ),
            497 =>
            array (
                'id' => 998,
                'language_code' => 'eu',
                'display_language_code' => 'hu',
                'name' => 'Baszk',
            ),
            498 =>
            array (
                'id' => 999,
                'language_code' => 'eu',
                'display_language_code' => 'hy',
                'name' => 'Basque',
            ),
            499 =>
            array (
                'id' => 1000,
                'language_code' => 'eu',
                'display_language_code' => 'id',
                'name' => 'Basque',
            ),
        ));
        \DB::table('language_translations')->insert(array (
            0 =>
            array (
                'id' => 1001,
                'language_code' => 'eu',
                'display_language_code' => 'is',
                'name' => 'Basque',
            ),
            1 =>
            array (
                'id' => 1002,
                'language_code' => 'eu',
                'display_language_code' => 'it',
                'name' => 'Basco',
            ),
            2 =>
            array (
                'id' => 1003,
                'language_code' => 'eu',
                'display_language_code' => 'ja',
                'name' => 'バスク語',
            ),
            3 =>
            array (
                'id' => 1004,
                'language_code' => 'eu',
                'display_language_code' => 'ko',
                'name' => '바스크어',
            ),
            4 =>
            array (
                'id' => 1005,
                'language_code' => 'eu',
                'display_language_code' => 'ku',
                'name' => 'Basque',
            ),
            5 =>
            array (
                'id' => 1006,
                'language_code' => 'eu',
                'display_language_code' => 'lv',
                'name' => 'Basque',
            ),
            6 =>
            array (
                'id' => 1007,
                'language_code' => 'eu',
                'display_language_code' => 'lt',
                'name' => 'Basque',
            ),
            7 =>
            array (
                'id' => 1008,
                'language_code' => 'eu',
                'display_language_code' => 'mk',
                'name' => 'Basque',
            ),
            8 =>
            array (
                'id' => 1009,
                'language_code' => 'eu',
                'display_language_code' => 'mt',
                'name' => 'Basque',
            ),
            9 =>
            array (
                'id' => 1010,
                'language_code' => 'eu',
                'display_language_code' => 'mn',
                'name' => 'Basque',
            ),
            10 =>
            array (
                'id' => 1011,
                'language_code' => 'eu',
                'display_language_code' => 'ne',
                'name' => 'Basque',
            ),
            11 =>
            array (
                'id' => 1012,
                'language_code' => 'eu',
                'display_language_code' => 'nl',
                'name' => 'Baskisch',
            ),
            12 =>
            array (
                'id' => 1013,
                'language_code' => 'eu',
                'display_language_code' => 'no',
                'name' => 'Basque',
            ),
            13 =>
            array (
                'id' => 1014,
                'language_code' => 'eu',
                'display_language_code' => 'pa',
                'name' => 'Basque',
            ),
            14 =>
            array (
                'id' => 1015,
                'language_code' => 'eu',
                'display_language_code' => 'pl',
                'name' => 'Baskijski',
            ),
            15 =>
            array (
                'id' => 1016,
                'language_code' => 'eu',
                'display_language_code' => 'pt-pt',
                'name' => 'Basco',
            ),
            16 =>
            array (
                'id' => 1017,
                'language_code' => 'eu',
                'display_language_code' => 'pt-br',
                'name' => 'Basco',
            ),
            17 =>
            array (
                'id' => 1018,
                'language_code' => 'eu',
                'display_language_code' => 'qu',
                'name' => 'Basque',
            ),
            18 =>
            array (
                'id' => 1019,
                'language_code' => 'eu',
                'display_language_code' => 'ro',
                'name' => 'Bască',
            ),
            19 =>
            array (
                'id' => 1020,
                'language_code' => 'eu',
                'display_language_code' => 'ru',
                'name' => 'Баскский',
            ),
            20 =>
            array (
                'id' => 1021,
                'language_code' => 'eu',
                'display_language_code' => 'sl',
                'name' => 'Baskovščina',
            ),
            21 =>
            array (
                'id' => 1022,
                'language_code' => 'eu',
                'display_language_code' => 'so',
                'name' => 'Basque',
            ),
            22 =>
            array (
                'id' => 1023,
                'language_code' => 'eu',
                'display_language_code' => 'sq',
                'name' => 'Basque',
            ),
            23 =>
            array (
                'id' => 1024,
                'language_code' => 'eu',
                'display_language_code' => 'sr',
                'name' => 'баскијски',
            ),
            24 =>
            array (
                'id' => 1025,
                'language_code' => 'eu',
                'display_language_code' => 'sv',
                'name' => 'Baskiska',
            ),
            25 =>
            array (
                'id' => 1026,
                'language_code' => 'eu',
                'display_language_code' => 'ta',
                'name' => 'Basque',
            ),
            26 =>
            array (
                'id' => 1027,
                'language_code' => 'eu',
                'display_language_code' => 'th',
                'name' => 'บาสค์',
            ),
            27 =>
            array (
                'id' => 1028,
                'language_code' => 'eu',
                'display_language_code' => 'tr',
                'name' => 'Bask dili',
            ),
            28 =>
            array (
                'id' => 1029,
                'language_code' => 'eu',
                'display_language_code' => 'uk',
                'name' => 'Basque',
            ),
            29 =>
            array (
                'id' => 1030,
                'language_code' => 'eu',
                'display_language_code' => 'ur',
                'name' => 'Basque',
            ),
            30 =>
            array (
                'id' => 1031,
                'language_code' => 'eu',
                'display_language_code' => 'uz',
                'name' => 'Basque',
            ),
            31 =>
            array (
                'id' => 1032,
                'language_code' => 'eu',
                'display_language_code' => 'vi',
                'name' => 'Basque',
            ),
            32 =>
            array (
                'id' => 1033,
                'language_code' => 'eu',
                'display_language_code' => 'yi',
                'name' => 'Basque',
            ),
            33 =>
            array (
                'id' => 1034,
                'language_code' => 'eu',
                'display_language_code' => 'zh-hans',
                'name' => '巴斯克语',
            ),
            34 =>
            array (
                'id' => 1035,
                'language_code' => 'eu',
                'display_language_code' => 'zu',
                'name' => 'Basque',
            ),
            35 =>
            array (
                'id' => 1036,
                'language_code' => 'eu',
                'display_language_code' => 'zh-hant',
                'name' => '巴斯克語',
            ),
            36 =>
            array (
                'id' => 1037,
                'language_code' => 'eu',
                'display_language_code' => 'ms',
                'name' => 'Basque',
            ),
            37 =>
            array (
                'id' => 1038,
                'language_code' => 'eu',
                'display_language_code' => 'gl',
                'name' => 'Basque',
            ),
            38 =>
            array (
                'id' => 1039,
                'language_code' => 'eu',
                'display_language_code' => 'bn',
                'name' => 'Basque',
            ),
            39 =>
            array (
                'id' => 1040,
                'language_code' => 'eu',
                'display_language_code' => 'az',
                'name' => 'Basque',
            ),
            40 =>
            array (
                'id' => 1041,
                'language_code' => 'fa',
                'display_language_code' => 'en',
                'name' => 'Persian',
            ),
            41 =>
            array (
                'id' => 1042,
                'language_code' => 'fa',
                'display_language_code' => 'es',
                'name' => 'Persa',
            ),
            42 =>
            array (
                'id' => 1043,
                'language_code' => 'fa',
                'display_language_code' => 'de',
                'name' => 'Persisch',
            ),
            43 =>
            array (
                'id' => 1044,
                'language_code' => 'fa',
                'display_language_code' => 'fr',
                'name' => 'Perse',
            ),
            44 =>
            array (
                'id' => 1045,
                'language_code' => 'fa',
                'display_language_code' => 'ar',
                'name' => 'الفارسية',
            ),
            45 =>
            array (
                'id' => 1046,
                'language_code' => 'fa',
                'display_language_code' => 'bs',
                'name' => 'Persian',
            ),
            46 =>
            array (
                'id' => 1047,
                'language_code' => 'fa',
                'display_language_code' => 'bg',
                'name' => 'Персийски',
            ),
            47 =>
            array (
                'id' => 1048,
                'language_code' => 'fa',
                'display_language_code' => 'ca',
                'name' => 'Persian',
            ),
            48 =>
            array (
                'id' => 1049,
                'language_code' => 'fa',
                'display_language_code' => 'cs',
                'name' => 'Perský',
            ),
            49 =>
            array (
                'id' => 1050,
                'language_code' => 'fa',
                'display_language_code' => 'sk',
                'name' => 'Perzština',
            ),
            50 =>
            array (
                'id' => 1051,
                'language_code' => 'fa',
                'display_language_code' => 'cy',
                'name' => 'Persian',
            ),
            51 =>
            array (
                'id' => 1052,
                'language_code' => 'fa',
                'display_language_code' => 'da',
                'name' => 'Persian',
            ),
            52 =>
            array (
                'id' => 1053,
                'language_code' => 'fa',
                'display_language_code' => 'el',
                'name' => 'Περσικά',
            ),
            53 =>
            array (
                'id' => 1054,
                'language_code' => 'fa',
                'display_language_code' => 'eo',
                'name' => 'Persian',
            ),
            54 =>
            array (
                'id' => 1055,
                'language_code' => 'fa',
                'display_language_code' => 'et',
                'name' => 'Persian',
            ),
            55 =>
            array (
                'id' => 1056,
                'language_code' => 'fa',
                'display_language_code' => 'eu',
                'name' => 'Persian',
            ),
            56 =>
            array (
                'id' => 1057,
                'language_code' => 'fa',
                'display_language_code' => 'fa',
                'name' => 'فارسی',
            ),
            57 =>
            array (
                'id' => 1058,
                'language_code' => 'fa',
                'display_language_code' => 'fi',
                'name' => 'Persia',
            ),
            58 =>
            array (
                'id' => 1059,
                'language_code' => 'fa',
                'display_language_code' => 'ga',
                'name' => 'Persian',
            ),
            59 =>
            array (
                'id' => 1060,
                'language_code' => 'fa',
                'display_language_code' => 'he',
                'name' => 'פרסית',
            ),
            60 =>
            array (
                'id' => 1061,
                'language_code' => 'fa',
                'display_language_code' => 'hi',
                'name' => 'Persian',
            ),
            61 =>
            array (
                'id' => 1062,
                'language_code' => 'fa',
                'display_language_code' => 'hr',
                'name' => 'Perzijski',
            ),
            62 =>
            array (
                'id' => 1063,
                'language_code' => 'fa',
                'display_language_code' => 'hu',
                'name' => 'Perzsa',
            ),
            63 =>
            array (
                'id' => 1064,
                'language_code' => 'fa',
                'display_language_code' => 'hy',
                'name' => 'Persian',
            ),
            64 =>
            array (
                'id' => 1065,
                'language_code' => 'fa',
                'display_language_code' => 'id',
                'name' => 'Persian',
            ),
            65 =>
            array (
                'id' => 1066,
                'language_code' => 'fa',
                'display_language_code' => 'is',
                'name' => 'Persian',
            ),
            66 =>
            array (
                'id' => 1067,
                'language_code' => 'fa',
                'display_language_code' => 'it',
                'name' => 'Persiano',
            ),
            67 =>
            array (
                'id' => 1068,
                'language_code' => 'fa',
                'display_language_code' => 'ja',
                'name' => 'ペルシア語',
            ),
            68 =>
            array (
                'id' => 1069,
                'language_code' => 'fa',
                'display_language_code' => 'ko',
                'name' => '페르시아어',
            ),
            69 =>
            array (
                'id' => 1070,
                'language_code' => 'fa',
                'display_language_code' => 'ku',
                'name' => 'Persian',
            ),
            70 =>
            array (
                'id' => 1071,
                'language_code' => 'fa',
                'display_language_code' => 'lv',
                'name' => 'Persian',
            ),
            71 =>
            array (
                'id' => 1072,
                'language_code' => 'fa',
                'display_language_code' => 'lt',
                'name' => 'Persian',
            ),
            72 =>
            array (
                'id' => 1073,
                'language_code' => 'fa',
                'display_language_code' => 'mk',
                'name' => 'Persian',
            ),
            73 =>
            array (
                'id' => 1074,
                'language_code' => 'fa',
                'display_language_code' => 'mt',
                'name' => 'Persian',
            ),
            74 =>
            array (
                'id' => 1075,
                'language_code' => 'fa',
                'display_language_code' => 'mn',
                'name' => 'Persian',
            ),
            75 =>
            array (
                'id' => 1076,
                'language_code' => 'fa',
                'display_language_code' => 'ne',
                'name' => 'Persian',
            ),
            76 =>
            array (
                'id' => 1077,
                'language_code' => 'fa',
                'display_language_code' => 'nl',
                'name' => 'Perzisch',
            ),
            77 =>
            array (
                'id' => 1078,
                'language_code' => 'fa',
                'display_language_code' => 'no',
                'name' => 'Persisk',
            ),
            78 =>
            array (
                'id' => 1079,
                'language_code' => 'fa',
                'display_language_code' => 'pa',
                'name' => 'Persian',
            ),
            79 =>
            array (
                'id' => 1080,
                'language_code' => 'fa',
                'display_language_code' => 'pl',
                'name' => 'Perski',
            ),
            80 =>
            array (
                'id' => 1081,
                'language_code' => 'fa',
                'display_language_code' => 'pt-pt',
                'name' => 'Persa',
            ),
            81 =>
            array (
                'id' => 1082,
                'language_code' => 'fa',
                'display_language_code' => 'pt-br',
                'name' => 'Persa',
            ),
            82 =>
            array (
                'id' => 1083,
                'language_code' => 'fa',
                'display_language_code' => 'qu',
                'name' => 'Persian',
            ),
            83 =>
            array (
                'id' => 1084,
                'language_code' => 'fa',
                'display_language_code' => 'ro',
                'name' => 'Persană',
            ),
            84 =>
            array (
                'id' => 1085,
                'language_code' => 'fa',
                'display_language_code' => 'ru',
                'name' => 'Персидский',
            ),
            85 =>
            array (
                'id' => 1086,
                'language_code' => 'fa',
                'display_language_code' => 'sl',
                'name' => 'Perzijski',
            ),
            86 =>
            array (
                'id' => 1087,
                'language_code' => 'fa',
                'display_language_code' => 'so',
                'name' => 'Persian',
            ),
            87 =>
            array (
                'id' => 1088,
                'language_code' => 'fa',
                'display_language_code' => 'sq',
                'name' => 'Persian',
            ),
            88 =>
            array (
                'id' => 1089,
                'language_code' => 'fa',
                'display_language_code' => 'sr',
                'name' => 'персијски',
            ),
            89 =>
            array (
                'id' => 1090,
                'language_code' => 'fa',
                'display_language_code' => 'sv',
                'name' => 'Persiska',
            ),
            90 =>
            array (
                'id' => 1091,
                'language_code' => 'fa',
                'display_language_code' => 'ta',
                'name' => 'Persian',
            ),
            91 =>
            array (
                'id' => 1092,
                'language_code' => 'fa',
                'display_language_code' => 'th',
                'name' => 'เปอร์เซียน',
            ),
            92 =>
            array (
                'id' => 1093,
                'language_code' => 'fa',
                'display_language_code' => 'tr',
                'name' => 'Farsça',
            ),
            93 =>
            array (
                'id' => 1094,
                'language_code' => 'fa',
                'display_language_code' => 'uk',
                'name' => 'Persian',
            ),
            94 =>
            array (
                'id' => 1095,
                'language_code' => 'fa',
                'display_language_code' => 'ur',
                'name' => 'Persian',
            ),
            95 =>
            array (
                'id' => 1096,
                'language_code' => 'fa',
                'display_language_code' => 'uz',
                'name' => 'Persian',
            ),
            96 =>
            array (
                'id' => 1097,
                'language_code' => 'fa',
                'display_language_code' => 'vi',
                'name' => 'Persian',
            ),
            97 =>
            array (
                'id' => 1098,
                'language_code' => 'fa',
                'display_language_code' => 'yi',
                'name' => 'Persian',
            ),
            98 =>
            array (
                'id' => 1099,
                'language_code' => 'fa',
                'display_language_code' => 'zh-hans',
                'name' => '波斯语',
            ),
            99 =>
            array (
                'id' => 1100,
                'language_code' => 'fa',
                'display_language_code' => 'zu',
                'name' => 'Persian',
            ),
            100 =>
            array (
                'id' => 1101,
                'language_code' => 'fa',
                'display_language_code' => 'zh-hant',
                'name' => '波斯語',
            ),
            101 =>
            array (
                'id' => 1102,
                'language_code' => 'fa',
                'display_language_code' => 'ms',
                'name' => 'Persian',
            ),
            102 =>
            array (
                'id' => 1103,
                'language_code' => 'fa',
                'display_language_code' => 'gl',
                'name' => 'Persian',
            ),
            103 =>
            array (
                'id' => 1104,
                'language_code' => 'fa',
                'display_language_code' => 'bn',
                'name' => 'Persian',
            ),
            104 =>
            array (
                'id' => 1105,
                'language_code' => 'fa',
                'display_language_code' => 'az',
                'name' => 'Persian',
            ),
            105 =>
            array (
                'id' => 1106,
                'language_code' => 'fi',
                'display_language_code' => 'en',
                'name' => 'Finnish',
            ),
            106 =>
            array (
                'id' => 1107,
                'language_code' => 'fi',
                'display_language_code' => 'es',
                'name' => 'Finlandés',
            ),
            107 =>
            array (
                'id' => 1108,
                'language_code' => 'fi',
                'display_language_code' => 'de',
                'name' => 'Finnisch',
            ),
            108 =>
            array (
                'id' => 1109,
                'language_code' => 'fi',
                'display_language_code' => 'fr',
                'name' => 'Finnois',
            ),
            109 =>
            array (
                'id' => 1110,
                'language_code' => 'fi',
                'display_language_code' => 'ar',
                'name' => 'الفنلندية',
            ),
            110 =>
            array (
                'id' => 1111,
                'language_code' => 'fi',
                'display_language_code' => 'bs',
                'name' => 'Finnish',
            ),
            111 =>
            array (
                'id' => 1112,
                'language_code' => 'fi',
                'display_language_code' => 'bg',
                'name' => 'Фински',
            ),
            112 =>
            array (
                'id' => 1113,
                'language_code' => 'fi',
                'display_language_code' => 'ca',
                'name' => 'Finnish',
            ),
            113 =>
            array (
                'id' => 1114,
                'language_code' => 'fi',
                'display_language_code' => 'cs',
                'name' => 'Finský',
            ),
            114 =>
            array (
                'id' => 1115,
                'language_code' => 'fi',
                'display_language_code' => 'sk',
                'name' => 'Fínština',
            ),
            115 =>
            array (
                'id' => 1116,
                'language_code' => 'fi',
                'display_language_code' => 'cy',
                'name' => 'Finnish',
            ),
            116 =>
            array (
                'id' => 1117,
                'language_code' => 'fi',
                'display_language_code' => 'da',
                'name' => 'Finnish',
            ),
            117 =>
            array (
                'id' => 1118,
                'language_code' => 'fi',
                'display_language_code' => 'el',
                'name' => 'Φινλανδικά',
            ),
            118 =>
            array (
                'id' => 1119,
                'language_code' => 'fi',
                'display_language_code' => 'eo',
                'name' => 'Finnish',
            ),
            119 =>
            array (
                'id' => 1120,
                'language_code' => 'fi',
                'display_language_code' => 'et',
                'name' => 'Finnish',
            ),
            120 =>
            array (
                'id' => 1121,
                'language_code' => 'fi',
                'display_language_code' => 'eu',
                'name' => 'Finnish',
            ),
            121 =>
            array (
                'id' => 1122,
                'language_code' => 'fi',
                'display_language_code' => 'fa',
                'name' => 'Finnish',
            ),
            122 =>
            array (
                'id' => 1123,
                'language_code' => 'fi',
                'display_language_code' => 'fi',
                'name' => 'Suomi',
            ),
            123 =>
            array (
                'id' => 1124,
                'language_code' => 'fi',
                'display_language_code' => 'ga',
                'name' => 'Finnish',
            ),
            124 =>
            array (
                'id' => 1125,
                'language_code' => 'fi',
                'display_language_code' => 'he',
                'name' => 'פינית',
            ),
            125 =>
            array (
                'id' => 1126,
                'language_code' => 'fi',
                'display_language_code' => 'hi',
                'name' => 'Finnish',
            ),
            126 =>
            array (
                'id' => 1127,
                'language_code' => 'fi',
                'display_language_code' => 'hr',
                'name' => 'Finski',
            ),
            127 =>
            array (
                'id' => 1128,
                'language_code' => 'fi',
                'display_language_code' => 'hu',
                'name' => 'Finn',
            ),
            128 =>
            array (
                'id' => 1129,
                'language_code' => 'fi',
                'display_language_code' => 'hy',
                'name' => 'Finnish',
            ),
            129 =>
            array (
                'id' => 1130,
                'language_code' => 'fi',
                'display_language_code' => 'id',
                'name' => 'Finnish',
            ),
            130 =>
            array (
                'id' => 1131,
                'language_code' => 'fi',
                'display_language_code' => 'is',
                'name' => 'Finnish',
            ),
            131 =>
            array (
                'id' => 1132,
                'language_code' => 'fi',
                'display_language_code' => 'it',
                'name' => 'Finlandese',
            ),
            132 =>
            array (
                'id' => 1133,
                'language_code' => 'fi',
                'display_language_code' => 'ja',
                'name' => 'フィンランド語',
            ),
            133 =>
            array (
                'id' => 1134,
                'language_code' => 'fi',
                'display_language_code' => 'ko',
                'name' => '핀란드어',
            ),
            134 =>
            array (
                'id' => 1135,
                'language_code' => 'fi',
                'display_language_code' => 'ku',
                'name' => 'Finnish',
            ),
            135 =>
            array (
                'id' => 1136,
                'language_code' => 'fi',
                'display_language_code' => 'lv',
                'name' => 'Finnish',
            ),
            136 =>
            array (
                'id' => 1137,
                'language_code' => 'fi',
                'display_language_code' => 'lt',
                'name' => 'Finnish',
            ),
            137 =>
            array (
                'id' => 1138,
                'language_code' => 'fi',
                'display_language_code' => 'mk',
                'name' => 'Finnish',
            ),
            138 =>
            array (
                'id' => 1139,
                'language_code' => 'fi',
                'display_language_code' => 'mt',
                'name' => 'Finnish',
            ),
            139 =>
            array (
                'id' => 1140,
                'language_code' => 'fi',
                'display_language_code' => 'mn',
                'name' => 'Finnish',
            ),
            140 =>
            array (
                'id' => 1141,
                'language_code' => 'fi',
                'display_language_code' => 'ne',
                'name' => 'Finnish',
            ),
            141 =>
            array (
                'id' => 1142,
                'language_code' => 'fi',
                'display_language_code' => 'nl',
                'name' => 'Fins',
            ),
            142 =>
            array (
                'id' => 1143,
                'language_code' => 'fi',
                'display_language_code' => 'no',
                'name' => 'Finsk',
            ),
            143 =>
            array (
                'id' => 1144,
                'language_code' => 'fi',
                'display_language_code' => 'pa',
                'name' => 'Finnish',
            ),
            144 =>
            array (
                'id' => 1145,
                'language_code' => 'fi',
                'display_language_code' => 'pl',
                'name' => 'Fiński',
            ),
            145 =>
            array (
                'id' => 1146,
                'language_code' => 'fi',
                'display_language_code' => 'pt-pt',
                'name' => 'Finlandês',
            ),
            146 =>
            array (
                'id' => 1147,
                'language_code' => 'fi',
                'display_language_code' => 'pt-br',
                'name' => 'Finlandês',
            ),
            147 =>
            array (
                'id' => 1148,
                'language_code' => 'fi',
                'display_language_code' => 'qu',
                'name' => 'Finnish',
            ),
            148 =>
            array (
                'id' => 1149,
                'language_code' => 'fi',
                'display_language_code' => 'ro',
                'name' => 'Finlandeză',
            ),
            149 =>
            array (
                'id' => 1150,
                'language_code' => 'fi',
                'display_language_code' => 'ru',
                'name' => 'Финский',
            ),
            150 =>
            array (
                'id' => 1151,
                'language_code' => 'fi',
                'display_language_code' => 'sl',
                'name' => 'Finski',
            ),
            151 =>
            array (
                'id' => 1152,
                'language_code' => 'fi',
                'display_language_code' => 'so',
                'name' => 'Finnish',
            ),
            152 =>
            array (
                'id' => 1153,
                'language_code' => 'fi',
                'display_language_code' => 'sq',
                'name' => 'Finnish',
            ),
            153 =>
            array (
                'id' => 1154,
                'language_code' => 'fi',
                'display_language_code' => 'sr',
                'name' => 'фински',
            ),
            154 =>
            array (
                'id' => 1155,
                'language_code' => 'fi',
                'display_language_code' => 'sv',
                'name' => 'Finska',
            ),
            155 =>
            array (
                'id' => 1156,
                'language_code' => 'fi',
                'display_language_code' => 'ta',
                'name' => 'Finnish',
            ),
            156 =>
            array (
                'id' => 1157,
                'language_code' => 'fi',
                'display_language_code' => 'th',
                'name' => 'ฟินนิช',
            ),
            157 =>
            array (
                'id' => 1158,
                'language_code' => 'fi',
                'display_language_code' => 'tr',
                'name' => 'Fince',
            ),
            158 =>
            array (
                'id' => 1159,
                'language_code' => 'fi',
                'display_language_code' => 'uk',
                'name' => 'Finnish',
            ),
            159 =>
            array (
                'id' => 1160,
                'language_code' => 'fi',
                'display_language_code' => 'ur',
                'name' => 'Finnish',
            ),
            160 =>
            array (
                'id' => 1161,
                'language_code' => 'fi',
                'display_language_code' => 'uz',
                'name' => 'Finnish',
            ),
            161 =>
            array (
                'id' => 1162,
                'language_code' => 'fi',
                'display_language_code' => 'vi',
                'name' => 'Finnish',
            ),
            162 =>
            array (
                'id' => 1163,
                'language_code' => 'fi',
                'display_language_code' => 'yi',
                'name' => 'Finnish',
            ),
            163 =>
            array (
                'id' => 1164,
                'language_code' => 'fi',
                'display_language_code' => 'zh-hans',
                'name' => '芬兰语',
            ),
            164 =>
            array (
                'id' => 1165,
                'language_code' => 'fi',
                'display_language_code' => 'zu',
                'name' => 'Finnish',
            ),
            165 =>
            array (
                'id' => 1166,
                'language_code' => 'fi',
                'display_language_code' => 'zh-hant',
                'name' => '芬蘭語',
            ),
            166 =>
            array (
                'id' => 1167,
                'language_code' => 'fi',
                'display_language_code' => 'ms',
                'name' => 'Finnish',
            ),
            167 =>
            array (
                'id' => 1168,
                'language_code' => 'fi',
                'display_language_code' => 'gl',
                'name' => 'Finnish',
            ),
            168 =>
            array (
                'id' => 1169,
                'language_code' => 'fi',
                'display_language_code' => 'bn',
                'name' => 'Finnish',
            ),
            169 =>
            array (
                'id' => 1170,
                'language_code' => 'fi',
                'display_language_code' => 'az',
                'name' => 'Finnish',
            ),
            170 =>
            array (
                'id' => 1171,
                'language_code' => 'ga',
                'display_language_code' => 'en',
                'name' => 'Irish',
            ),
            171 =>
            array (
                'id' => 1172,
                'language_code' => 'ga',
                'display_language_code' => 'es',
                'name' => 'Irlandés',
            ),
            172 =>
            array (
                'id' => 1173,
                'language_code' => 'ga',
                'display_language_code' => 'de',
                'name' => 'Irisch',
            ),
            173 =>
            array (
                'id' => 1174,
                'language_code' => 'ga',
                'display_language_code' => 'fr',
                'name' => 'Irlandais',
            ),
            174 =>
            array (
                'id' => 1175,
                'language_code' => 'ga',
                'display_language_code' => 'ar',
                'name' => 'الأيرلندية',
            ),
            175 =>
            array (
                'id' => 1176,
                'language_code' => 'ga',
                'display_language_code' => 'bs',
                'name' => 'Irish',
            ),
            176 =>
            array (
                'id' => 1177,
                'language_code' => 'ga',
                'display_language_code' => 'bg',
                'name' => 'Ирландски',
            ),
            177 =>
            array (
                'id' => 1178,
                'language_code' => 'ga',
                'display_language_code' => 'ca',
                'name' => 'Irish',
            ),
            178 =>
            array (
                'id' => 1179,
                'language_code' => 'ga',
                'display_language_code' => 'cs',
                'name' => 'Irský',
            ),
            179 =>
            array (
                'id' => 1180,
                'language_code' => 'ga',
                'display_language_code' => 'sk',
                'name' => 'Írština',
            ),
            180 =>
            array (
                'id' => 1181,
                'language_code' => 'ga',
                'display_language_code' => 'cy',
                'name' => 'Irish',
            ),
            181 =>
            array (
                'id' => 1182,
                'language_code' => 'ga',
                'display_language_code' => 'da',
                'name' => 'Irish',
            ),
            182 =>
            array (
                'id' => 1183,
                'language_code' => 'ga',
                'display_language_code' => 'el',
                'name' => 'Ιρλανδικά',
            ),
            183 =>
            array (
                'id' => 1184,
                'language_code' => 'ga',
                'display_language_code' => 'eo',
                'name' => 'Irish',
            ),
            184 =>
            array (
                'id' => 1185,
                'language_code' => 'ga',
                'display_language_code' => 'et',
                'name' => 'Irish',
            ),
            185 =>
            array (
                'id' => 1186,
                'language_code' => 'ga',
                'display_language_code' => 'eu',
                'name' => 'Irish',
            ),
            186 =>
            array (
                'id' => 1187,
                'language_code' => 'ga',
                'display_language_code' => 'fa',
                'name' => 'Irish',
            ),
            187 =>
            array (
                'id' => 1188,
                'language_code' => 'ga',
                'display_language_code' => 'fi',
                'name' => 'Iiri',
            ),
            188 =>
            array (
                'id' => 1189,
                'language_code' => 'ga',
                'display_language_code' => 'ga',
                'name' => 'Gaeilge',
            ),
            189 =>
            array (
                'id' => 1190,
                'language_code' => 'ga',
                'display_language_code' => 'he',
                'name' => 'אירית',
            ),
            190 =>
            array (
                'id' => 1191,
                'language_code' => 'ga',
                'display_language_code' => 'hi',
                'name' => 'Irish',
            ),
            191 =>
            array (
                'id' => 1192,
                'language_code' => 'ga',
                'display_language_code' => 'hr',
                'name' => 'Irski',
            ),
            192 =>
            array (
                'id' => 1193,
                'language_code' => 'ga',
                'display_language_code' => 'hu',
                'name' => 'ír',
            ),
            193 =>
            array (
                'id' => 1194,
                'language_code' => 'ga',
                'display_language_code' => 'hy',
                'name' => 'Irish',
            ),
            194 =>
            array (
                'id' => 1195,
                'language_code' => 'ga',
                'display_language_code' => 'id',
                'name' => 'Irish',
            ),
            195 =>
            array (
                'id' => 1196,
                'language_code' => 'ga',
                'display_language_code' => 'is',
                'name' => 'Irish',
            ),
            196 =>
            array (
                'id' => 1197,
                'language_code' => 'ga',
                'display_language_code' => 'it',
                'name' => 'Irlandese',
            ),
            197 =>
            array (
                'id' => 1198,
                'language_code' => 'ga',
                'display_language_code' => 'ja',
                'name' => 'アイルランド語',
            ),
            198 =>
            array (
                'id' => 1199,
                'language_code' => 'ga',
                'display_language_code' => 'ko',
                'name' => '아일랜드어',
            ),
            199 =>
            array (
                'id' => 1200,
                'language_code' => 'ga',
                'display_language_code' => 'ku',
                'name' => 'Irish',
            ),
            200 =>
            array (
                'id' => 1201,
                'language_code' => 'ga',
                'display_language_code' => 'lv',
                'name' => 'Irish',
            ),
            201 =>
            array (
                'id' => 1202,
                'language_code' => 'ga',
                'display_language_code' => 'lt',
                'name' => 'Irish',
            ),
            202 =>
            array (
                'id' => 1203,
                'language_code' => 'ga',
                'display_language_code' => 'mk',
                'name' => 'Irish',
            ),
            203 =>
            array (
                'id' => 1204,
                'language_code' => 'ga',
                'display_language_code' => 'mt',
                'name' => 'Irish',
            ),
            204 =>
            array (
                'id' => 1205,
                'language_code' => 'ga',
                'display_language_code' => 'mn',
                'name' => 'Irish',
            ),
            205 =>
            array (
                'id' => 1206,
                'language_code' => 'ga',
                'display_language_code' => 'ne',
                'name' => 'Irish',
            ),
            206 =>
            array (
                'id' => 1207,
                'language_code' => 'ga',
                'display_language_code' => 'nl',
                'name' => 'Iers',
            ),
            207 =>
            array (
                'id' => 1208,
                'language_code' => 'ga',
                'display_language_code' => 'no',
                'name' => 'Irish',
            ),
            208 =>
            array (
                'id' => 1209,
                'language_code' => 'ga',
                'display_language_code' => 'pa',
                'name' => 'Irish',
            ),
            209 =>
            array (
                'id' => 1210,
                'language_code' => 'ga',
                'display_language_code' => 'pl',
                'name' => 'Irlandzki',
            ),
            210 =>
            array (
                'id' => 1211,
                'language_code' => 'ga',
                'display_language_code' => 'pt-pt',
                'name' => 'Irlandês',
            ),
            211 =>
            array (
                'id' => 1212,
                'language_code' => 'ga',
                'display_language_code' => 'pt-br',
                'name' => 'Irlandês',
            ),
            212 =>
            array (
                'id' => 1213,
                'language_code' => 'ga',
                'display_language_code' => 'qu',
                'name' => 'Irish',
            ),
            213 =>
            array (
                'id' => 1214,
                'language_code' => 'ga',
                'display_language_code' => 'ro',
                'name' => 'Irlandeză',
            ),
            214 =>
            array (
                'id' => 1215,
                'language_code' => 'ga',
                'display_language_code' => 'ru',
                'name' => 'Ирландский',
            ),
            215 =>
            array (
                'id' => 1216,
                'language_code' => 'ga',
                'display_language_code' => 'sl',
                'name' => 'Irski',
            ),
            216 =>
            array (
                'id' => 1217,
                'language_code' => 'ga',
                'display_language_code' => 'so',
                'name' => 'Irish',
            ),
            217 =>
            array (
                'id' => 1218,
                'language_code' => 'ga',
                'display_language_code' => 'sq',
                'name' => 'Irish',
            ),
            218 =>
            array (
                'id' => 1219,
                'language_code' => 'ga',
                'display_language_code' => 'sr',
                'name' => 'ирски',
            ),
            219 =>
            array (
                'id' => 1220,
                'language_code' => 'ga',
                'display_language_code' => 'sv',
                'name' => 'Irländska',
            ),
            220 =>
            array (
                'id' => 1221,
                'language_code' => 'ga',
                'display_language_code' => 'ta',
                'name' => 'Irish',
            ),
            221 =>
            array (
                'id' => 1222,
                'language_code' => 'ga',
                'display_language_code' => 'th',
                'name' => 'ไอริช',
            ),
            222 =>
            array (
                'id' => 1223,
                'language_code' => 'ga',
                'display_language_code' => 'tr',
                'name' => 'İrlanda dili',
            ),
            223 =>
            array (
                'id' => 1224,
                'language_code' => 'ga',
                'display_language_code' => 'uk',
                'name' => 'Irish',
            ),
            224 =>
            array (
                'id' => 1225,
                'language_code' => 'ga',
                'display_language_code' => 'ur',
                'name' => 'Irish',
            ),
            225 =>
            array (
                'id' => 1226,
                'language_code' => 'ga',
                'display_language_code' => 'uz',
                'name' => 'Irish',
            ),
            226 =>
            array (
                'id' => 1227,
                'language_code' => 'ga',
                'display_language_code' => 'vi',
                'name' => 'Irish',
            ),
            227 =>
            array (
                'id' => 1228,
                'language_code' => 'ga',
                'display_language_code' => 'yi',
                'name' => 'Irish',
            ),
            228 =>
            array (
                'id' => 1229,
                'language_code' => 'ga',
                'display_language_code' => 'zh-hans',
                'name' => '爱尔兰语',
            ),
            229 =>
            array (
                'id' => 1230,
                'language_code' => 'ga',
                'display_language_code' => 'zu',
                'name' => 'Irish',
            ),
            230 =>
            array (
                'id' => 1231,
                'language_code' => 'ga',
                'display_language_code' => 'zh-hant',
                'name' => '愛爾蘭語',
            ),
            231 =>
            array (
                'id' => 1232,
                'language_code' => 'ga',
                'display_language_code' => 'ms',
                'name' => 'Irish',
            ),
            232 =>
            array (
                'id' => 1233,
                'language_code' => 'ga',
                'display_language_code' => 'gl',
                'name' => 'Irish',
            ),
            233 =>
            array (
                'id' => 1234,
                'language_code' => 'ga',
                'display_language_code' => 'bn',
                'name' => 'Irish',
            ),
            234 =>
            array (
                'id' => 1235,
                'language_code' => 'ga',
                'display_language_code' => 'az',
                'name' => 'Irish',
            ),
            235 =>
            array (
                'id' => 1236,
                'language_code' => 'he',
                'display_language_code' => 'en',
                'name' => 'Hebrew',
            ),
            236 =>
            array (
                'id' => 1237,
                'language_code' => 'he',
                'display_language_code' => 'es',
                'name' => 'Hebreo',
            ),
            237 =>
            array (
                'id' => 1238,
                'language_code' => 'he',
                'display_language_code' => 'de',
                'name' => 'Hebräisch',
            ),
            238 =>
            array (
                'id' => 1239,
                'language_code' => 'he',
                'display_language_code' => 'fr',
                'name' => 'Hébreu',
            ),
            239 =>
            array (
                'id' => 1240,
                'language_code' => 'he',
                'display_language_code' => 'ar',
                'name' => 'العبرية',
            ),
            240 =>
            array (
                'id' => 1241,
                'language_code' => 'he',
                'display_language_code' => 'bs',
                'name' => 'Hebrew',
            ),
            241 =>
            array (
                'id' => 1242,
                'language_code' => 'he',
                'display_language_code' => 'bg',
                'name' => 'Иврит',
            ),
            242 =>
            array (
                'id' => 1243,
                'language_code' => 'he',
                'display_language_code' => 'ca',
                'name' => 'Hebrew',
            ),
            243 =>
            array (
                'id' => 1244,
                'language_code' => 'he',
                'display_language_code' => 'cs',
                'name' => 'Hebrejština',
            ),
            244 =>
            array (
                'id' => 1245,
                'language_code' => 'he',
                'display_language_code' => 'sk',
                'name' => 'Hebrejčina',
            ),
            245 =>
            array (
                'id' => 1246,
                'language_code' => 'he',
                'display_language_code' => 'cy',
                'name' => 'Hebrew',
            ),
            246 =>
            array (
                'id' => 1247,
                'language_code' => 'he',
                'display_language_code' => 'da',
                'name' => 'Hebrew',
            ),
            247 =>
            array (
                'id' => 1248,
                'language_code' => 'he',
                'display_language_code' => 'el',
                'name' => 'Εβραϊκά',
            ),
            248 =>
            array (
                'id' => 1249,
                'language_code' => 'he',
                'display_language_code' => 'eo',
                'name' => 'Hebrew',
            ),
            249 =>
            array (
                'id' => 1250,
                'language_code' => 'he',
                'display_language_code' => 'et',
                'name' => 'Hebrew',
            ),
            250 =>
            array (
                'id' => 1251,
                'language_code' => 'he',
                'display_language_code' => 'eu',
                'name' => 'Hebrew',
            ),
            251 =>
            array (
                'id' => 1252,
                'language_code' => 'he',
                'display_language_code' => 'fa',
                'name' => 'Hebrew',
            ),
            252 =>
            array (
                'id' => 1253,
                'language_code' => 'he',
                'display_language_code' => 'fi',
                'name' => 'Heprea',
            ),
            253 =>
            array (
                'id' => 1254,
                'language_code' => 'he',
                'display_language_code' => 'ga',
                'name' => 'Hebrew',
            ),
            254 =>
            array (
                'id' => 1255,
                'language_code' => 'he',
                'display_language_code' => 'he',
                'name' => 'עברית',
            ),
            255 =>
            array (
                'id' => 1256,
                'language_code' => 'he',
                'display_language_code' => 'hi',
                'name' => 'Hebrew',
            ),
            256 =>
            array (
                'id' => 1257,
                'language_code' => 'he',
                'display_language_code' => 'hr',
                'name' => 'Hebrejski',
            ),
            257 =>
            array (
                'id' => 1258,
                'language_code' => 'he',
                'display_language_code' => 'hu',
                'name' => 'Héber',
            ),
            258 =>
            array (
                'id' => 1259,
                'language_code' => 'he',
                'display_language_code' => 'hy',
                'name' => 'Hebrew',
            ),
            259 =>
            array (
                'id' => 1260,
                'language_code' => 'he',
                'display_language_code' => 'id',
                'name' => 'Hebrew',
            ),
            260 =>
            array (
                'id' => 1261,
                'language_code' => 'he',
                'display_language_code' => 'is',
                'name' => 'Hebrew',
            ),
            261 =>
            array (
                'id' => 1262,
                'language_code' => 'he',
                'display_language_code' => 'it',
                'name' => 'Ebraico',
            ),
            262 =>
            array (
                'id' => 1263,
                'language_code' => 'he',
                'display_language_code' => 'ja',
                'name' => 'ヘブライ語',
            ),
            263 =>
            array (
                'id' => 1264,
                'language_code' => 'he',
                'display_language_code' => 'ko',
                'name' => '히브리어',
            ),
            264 =>
            array (
                'id' => 1265,
                'language_code' => 'he',
                'display_language_code' => 'ku',
                'name' => 'Hebrew',
            ),
            265 =>
            array (
                'id' => 1266,
                'language_code' => 'he',
                'display_language_code' => 'lv',
                'name' => 'Hebrew',
            ),
            266 =>
            array (
                'id' => 1267,
                'language_code' => 'he',
                'display_language_code' => 'lt',
                'name' => 'Hebrew',
            ),
            267 =>
            array (
                'id' => 1268,
                'language_code' => 'he',
                'display_language_code' => 'mk',
                'name' => 'Hebrew',
            ),
            268 =>
            array (
                'id' => 1269,
                'language_code' => 'he',
                'display_language_code' => 'mt',
                'name' => 'Hebrew',
            ),
            269 =>
            array (
                'id' => 1270,
                'language_code' => 'he',
                'display_language_code' => 'mn',
                'name' => 'Hebrew',
            ),
            270 =>
            array (
                'id' => 1271,
                'language_code' => 'he',
                'display_language_code' => 'ne',
                'name' => 'Hebrew',
            ),
            271 =>
            array (
                'id' => 1272,
                'language_code' => 'he',
                'display_language_code' => 'nl',
                'name' => 'Hebreeuws',
            ),
            272 =>
            array (
                'id' => 1273,
                'language_code' => 'he',
                'display_language_code' => 'no',
                'name' => 'Hebraisk',
            ),
            273 =>
            array (
                'id' => 1274,
                'language_code' => 'he',
                'display_language_code' => 'pa',
                'name' => 'Hebrew',
            ),
            274 =>
            array (
                'id' => 1275,
                'language_code' => 'he',
                'display_language_code' => 'pl',
                'name' => 'Hebrajski',
            ),
            275 =>
            array (
                'id' => 1276,
                'language_code' => 'he',
                'display_language_code' => 'pt-pt',
                'name' => 'Hebraico',
            ),
            276 =>
            array (
                'id' => 1277,
                'language_code' => 'he',
                'display_language_code' => 'pt-br',
                'name' => 'Hebraico',
            ),
            277 =>
            array (
                'id' => 1278,
                'language_code' => 'he',
                'display_language_code' => 'qu',
                'name' => 'Hebrew',
            ),
            278 =>
            array (
                'id' => 1279,
                'language_code' => 'he',
                'display_language_code' => 'ro',
                'name' => 'Ebraică',
            ),
            279 =>
            array (
                'id' => 1280,
                'language_code' => 'he',
                'display_language_code' => 'ru',
                'name' => 'Иврит',
            ),
            280 =>
            array (
                'id' => 1281,
                'language_code' => 'he',
                'display_language_code' => 'sl',
                'name' => 'Hebrejščina',
            ),
            281 =>
            array (
                'id' => 1282,
                'language_code' => 'he',
                'display_language_code' => 'so',
                'name' => 'Hebrew',
            ),
            282 =>
            array (
                'id' => 1283,
                'language_code' => 'he',
                'display_language_code' => 'sq',
                'name' => 'Hebrew',
            ),
            283 =>
            array (
                'id' => 1284,
                'language_code' => 'he',
                'display_language_code' => 'sr',
                'name' => 'Хебрејски',
            ),
            284 =>
            array (
                'id' => 1285,
                'language_code' => 'he',
                'display_language_code' => 'sv',
                'name' => 'Hebreiska',
            ),
            285 =>
            array (
                'id' => 1286,
                'language_code' => 'he',
                'display_language_code' => 'ta',
                'name' => 'Hebrew',
            ),
            286 =>
            array (
                'id' => 1287,
                'language_code' => 'he',
                'display_language_code' => 'th',
                'name' => 'ฮิบรู',
            ),
            287 =>
            array (
                'id' => 1288,
                'language_code' => 'he',
                'display_language_code' => 'tr',
                'name' => 'İbranice',
            ),
            288 =>
            array (
                'id' => 1289,
                'language_code' => 'he',
                'display_language_code' => 'uk',
                'name' => 'Hebrew',
            ),
            289 =>
            array (
                'id' => 1290,
                'language_code' => 'he',
                'display_language_code' => 'ur',
                'name' => 'Hebrew',
            ),
            290 =>
            array (
                'id' => 1291,
                'language_code' => 'he',
                'display_language_code' => 'uz',
                'name' => 'Hebrew',
            ),
            291 =>
            array (
                'id' => 1292,
                'language_code' => 'he',
                'display_language_code' => 'vi',
                'name' => 'Hebrew',
            ),
            292 =>
            array (
                'id' => 1293,
                'language_code' => 'he',
                'display_language_code' => 'yi',
                'name' => 'Hebrew',
            ),
            293 =>
            array (
                'id' => 1294,
                'language_code' => 'he',
                'display_language_code' => 'zh-hans',
                'name' => '希伯来语',
            ),
            294 =>
            array (
                'id' => 1295,
                'language_code' => 'he',
                'display_language_code' => 'zu',
                'name' => 'Hebrew',
            ),
            295 =>
            array (
                'id' => 1296,
                'language_code' => 'he',
                'display_language_code' => 'zh-hant',
                'name' => '希伯來語',
            ),
            296 =>
            array (
                'id' => 1297,
                'language_code' => 'he',
                'display_language_code' => 'ms',
                'name' => 'Hebrew',
            ),
            297 =>
            array (
                'id' => 1298,
                'language_code' => 'he',
                'display_language_code' => 'gl',
                'name' => 'Hebrew',
            ),
            298 =>
            array (
                'id' => 1299,
                'language_code' => 'he',
                'display_language_code' => 'bn',
                'name' => 'Hebrew',
            ),
            299 =>
            array (
                'id' => 1300,
                'language_code' => 'he',
                'display_language_code' => 'az',
                'name' => 'Hebrew',
            ),
            300 =>
            array (
                'id' => 1301,
                'language_code' => 'hi',
                'display_language_code' => 'en',
                'name' => 'Hindi',
            ),
            301 =>
            array (
                'id' => 1302,
                'language_code' => 'hi',
                'display_language_code' => 'es',
                'name' => 'Hindi',
            ),
            302 =>
            array (
                'id' => 1303,
                'language_code' => 'hi',
                'display_language_code' => 'de',
                'name' => 'Hindi',
            ),
            303 =>
            array (
                'id' => 1304,
                'language_code' => 'hi',
                'display_language_code' => 'fr',
                'name' => 'Hindi',
            ),
            304 =>
            array (
                'id' => 1305,
                'language_code' => 'hi',
                'display_language_code' => 'ar',
                'name' => 'الهندية',
            ),
            305 =>
            array (
                'id' => 1306,
                'language_code' => 'hi',
                'display_language_code' => 'bs',
                'name' => 'Hindi',
            ),
            306 =>
            array (
                'id' => 1307,
                'language_code' => 'hi',
                'display_language_code' => 'bg',
                'name' => 'Хинди',
            ),
            307 =>
            array (
                'id' => 1308,
                'language_code' => 'hi',
                'display_language_code' => 'ca',
                'name' => 'Hindi',
            ),
            308 =>
            array (
                'id' => 1309,
                'language_code' => 'hi',
                'display_language_code' => 'cs',
                'name' => 'Hindština',
            ),
            309 =>
            array (
                'id' => 1310,
                'language_code' => 'hi',
                'display_language_code' => 'sk',
                'name' => 'Hindčina',
            ),
            310 =>
            array (
                'id' => 1311,
                'language_code' => 'hi',
                'display_language_code' => 'cy',
                'name' => 'Hindi',
            ),
            311 =>
            array (
                'id' => 1312,
                'language_code' => 'hi',
                'display_language_code' => 'da',
                'name' => 'Hindi',
            ),
            312 =>
            array (
                'id' => 1313,
                'language_code' => 'hi',
                'display_language_code' => 'el',
                'name' => 'Ινδικά',
            ),
            313 =>
            array (
                'id' => 1314,
                'language_code' => 'hi',
                'display_language_code' => 'eo',
                'name' => 'Hindi',
            ),
            314 =>
            array (
                'id' => 1315,
                'language_code' => 'hi',
                'display_language_code' => 'et',
                'name' => 'Hindi',
            ),
            315 =>
            array (
                'id' => 1316,
                'language_code' => 'hi',
                'display_language_code' => 'eu',
                'name' => 'Hindi',
            ),
            316 =>
            array (
                'id' => 1317,
                'language_code' => 'hi',
                'display_language_code' => 'fa',
                'name' => 'Hindi',
            ),
            317 =>
            array (
                'id' => 1318,
                'language_code' => 'hi',
                'display_language_code' => 'fi',
                'name' => 'Hindi',
            ),
            318 =>
            array (
                'id' => 1319,
                'language_code' => 'hi',
                'display_language_code' => 'ga',
                'name' => 'Hindi',
            ),
            319 =>
            array (
                'id' => 1320,
                'language_code' => 'hi',
                'display_language_code' => 'he',
                'name' => 'הודית',
            ),
            320 =>
            array (
                'id' => 1321,
                'language_code' => 'hi',
                'display_language_code' => 'hi',
                'name' => 'हिन्दी',
            ),
            321 =>
            array (
                'id' => 1322,
                'language_code' => 'hi',
                'display_language_code' => 'hr',
                'name' => 'Hindski',
            ),
            322 =>
            array (
                'id' => 1323,
                'language_code' => 'hi',
                'display_language_code' => 'hu',
                'name' => 'Hindi',
            ),
            323 =>
            array (
                'id' => 1324,
                'language_code' => 'hi',
                'display_language_code' => 'hy',
                'name' => 'Hindi',
            ),
            324 =>
            array (
                'id' => 1325,
                'language_code' => 'hi',
                'display_language_code' => 'id',
                'name' => 'Hindi',
            ),
            325 =>
            array (
                'id' => 1326,
                'language_code' => 'hi',
                'display_language_code' => 'is',
                'name' => 'Hindi',
            ),
            326 =>
            array (
                'id' => 1327,
                'language_code' => 'hi',
                'display_language_code' => 'it',
                'name' => 'Hindi',
            ),
            327 =>
            array (
                'id' => 1328,
                'language_code' => 'hi',
                'display_language_code' => 'ja',
                'name' => 'ヒンディー語',
            ),
            328 =>
            array (
                'id' => 1329,
                'language_code' => 'hi',
                'display_language_code' => 'ko',
                'name' => '힌두어',
            ),
            329 =>
            array (
                'id' => 1330,
                'language_code' => 'hi',
                'display_language_code' => 'ku',
                'name' => 'Hindi',
            ),
            330 =>
            array (
                'id' => 1331,
                'language_code' => 'hi',
                'display_language_code' => 'lv',
                'name' => 'Hindi',
            ),
            331 =>
            array (
                'id' => 1332,
                'language_code' => 'hi',
                'display_language_code' => 'lt',
                'name' => 'Hindi',
            ),
            332 =>
            array (
                'id' => 1333,
                'language_code' => 'hi',
                'display_language_code' => 'mk',
                'name' => 'Hindi',
            ),
            333 =>
            array (
                'id' => 1334,
                'language_code' => 'hi',
                'display_language_code' => 'mt',
                'name' => 'Hindi',
            ),
            334 =>
            array (
                'id' => 1335,
                'language_code' => 'hi',
                'display_language_code' => 'mn',
                'name' => 'Hindi',
            ),
            335 =>
            array (
                'id' => 1336,
                'language_code' => 'hi',
                'display_language_code' => 'ne',
                'name' => 'Hindi',
            ),
            336 =>
            array (
                'id' => 1337,
                'language_code' => 'hi',
                'display_language_code' => 'nl',
                'name' => 'Hindi',
            ),
            337 =>
            array (
                'id' => 1338,
                'language_code' => 'hi',
                'display_language_code' => 'no',
                'name' => 'Hindi',
            ),
            338 =>
            array (
                'id' => 1339,
                'language_code' => 'hi',
                'display_language_code' => 'pa',
                'name' => 'Hindi',
            ),
            339 =>
            array (
                'id' => 1340,
                'language_code' => 'hi',
                'display_language_code' => 'pl',
                'name' => 'Hindi',
            ),
            340 =>
            array (
                'id' => 1341,
                'language_code' => 'hi',
                'display_language_code' => 'pt-pt',
                'name' => 'Hindi',
            ),
            341 =>
            array (
                'id' => 1342,
                'language_code' => 'hi',
                'display_language_code' => 'pt-br',
                'name' => 'Hindi',
            ),
            342 =>
            array (
                'id' => 1343,
                'language_code' => 'hi',
                'display_language_code' => 'qu',
                'name' => 'Hindi',
            ),
            343 =>
            array (
                'id' => 1344,
                'language_code' => 'hi',
                'display_language_code' => 'ro',
                'name' => 'Hindi',
            ),
            344 =>
            array (
                'id' => 1345,
                'language_code' => 'hi',
                'display_language_code' => 'ru',
                'name' => 'Хинди',
            ),
            345 =>
            array (
                'id' => 1346,
                'language_code' => 'hi',
                'display_language_code' => 'sl',
                'name' => 'Hindi',
            ),
            346 =>
            array (
                'id' => 1347,
                'language_code' => 'hi',
                'display_language_code' => 'so',
                'name' => 'Hindi',
            ),
            347 =>
            array (
                'id' => 1348,
                'language_code' => 'hi',
                'display_language_code' => 'sq',
                'name' => 'Hindi',
            ),
            348 =>
            array (
                'id' => 1349,
                'language_code' => 'hi',
                'display_language_code' => 'sr',
                'name' => 'хинди',
            ),
            349 =>
            array (
                'id' => 1350,
                'language_code' => 'hi',
                'display_language_code' => 'sv',
                'name' => 'Hindi',
            ),
            350 =>
            array (
                'id' => 1351,
                'language_code' => 'hi',
                'display_language_code' => 'ta',
                'name' => 'Hindi',
            ),
            351 =>
            array (
                'id' => 1352,
                'language_code' => 'hi',
                'display_language_code' => 'th',
                'name' => 'ฮินดิ',
            ),
            352 =>
            array (
                'id' => 1353,
                'language_code' => 'hi',
                'display_language_code' => 'tr',
                'name' => 'Hintçe',
            ),
            353 =>
            array (
                'id' => 1354,
                'language_code' => 'hi',
                'display_language_code' => 'uk',
                'name' => 'Hindi',
            ),
            354 =>
            array (
                'id' => 1355,
                'language_code' => 'hi',
                'display_language_code' => 'ur',
                'name' => 'Hindi',
            ),
            355 =>
            array (
                'id' => 1356,
                'language_code' => 'hi',
                'display_language_code' => 'uz',
                'name' => 'Hindi',
            ),
            356 =>
            array (
                'id' => 1357,
                'language_code' => 'hi',
                'display_language_code' => 'vi',
                'name' => 'Hindi',
            ),
            357 =>
            array (
                'id' => 1358,
                'language_code' => 'hi',
                'display_language_code' => 'yi',
                'name' => 'Hindi',
            ),
            358 =>
            array (
                'id' => 1359,
                'language_code' => 'hi',
                'display_language_code' => 'zh-hans',
                'name' => '印地语',
            ),
            359 =>
            array (
                'id' => 1360,
                'language_code' => 'hi',
                'display_language_code' => 'zu',
                'name' => 'Hindi',
            ),
            360 =>
            array (
                'id' => 1361,
                'language_code' => 'hi',
                'display_language_code' => 'zh-hant',
                'name' => '印地語',
            ),
            361 =>
            array (
                'id' => 1362,
                'language_code' => 'hi',
                'display_language_code' => 'ms',
                'name' => 'Hindi',
            ),
            362 =>
            array (
                'id' => 1363,
                'language_code' => 'hi',
                'display_language_code' => 'gl',
                'name' => 'Hindi',
            ),
            363 =>
            array (
                'id' => 1364,
                'language_code' => 'hi',
                'display_language_code' => 'bn',
                'name' => 'Hindi',
            ),
            364 =>
            array (
                'id' => 1365,
                'language_code' => 'hi',
                'display_language_code' => 'az',
                'name' => 'Hindi',
            ),
            365 =>
            array (
                'id' => 1366,
                'language_code' => 'hr',
                'display_language_code' => 'en',
                'name' => 'Croatian',
            ),
            366 =>
            array (
                'id' => 1367,
                'language_code' => 'hr',
                'display_language_code' => 'es',
                'name' => 'Croata',
            ),
            367 =>
            array (
                'id' => 1368,
                'language_code' => 'hr',
                'display_language_code' => 'de',
                'name' => 'Kroatisch',
            ),
            368 =>
            array (
                'id' => 1369,
                'language_code' => 'hr',
                'display_language_code' => 'fr',
                'name' => 'Croate',
            ),
            369 =>
            array (
                'id' => 1370,
                'language_code' => 'hr',
                'display_language_code' => 'ar',
                'name' => 'الكرواتية',
            ),
            370 =>
            array (
                'id' => 1371,
                'language_code' => 'hr',
                'display_language_code' => 'bs',
                'name' => 'Croatian',
            ),
            371 =>
            array (
                'id' => 1372,
                'language_code' => 'hr',
                'display_language_code' => 'bg',
                'name' => 'Хърватски',
            ),
            372 =>
            array (
                'id' => 1373,
                'language_code' => 'hr',
                'display_language_code' => 'ca',
                'name' => 'Croatian',
            ),
            373 =>
            array (
                'id' => 1374,
                'language_code' => 'hr',
                'display_language_code' => 'cs',
                'name' => 'Chorvatský',
            ),
            374 =>
            array (
                'id' => 1375,
                'language_code' => 'hr',
                'display_language_code' => 'sk',
                'name' => 'Chorvátština',
            ),
            375 =>
            array (
                'id' => 1376,
                'language_code' => 'hr',
                'display_language_code' => 'cy',
                'name' => 'Croatian',
            ),
            376 =>
            array (
                'id' => 1377,
                'language_code' => 'hr',
                'display_language_code' => 'da',
                'name' => 'Croatian',
            ),
            377 =>
            array (
                'id' => 1378,
                'language_code' => 'hr',
                'display_language_code' => 'el',
                'name' => 'Κροατικά',
            ),
            378 =>
            array (
                'id' => 1379,
                'language_code' => 'hr',
                'display_language_code' => 'eo',
                'name' => 'Croatian',
            ),
            379 =>
            array (
                'id' => 1380,
                'language_code' => 'hr',
                'display_language_code' => 'et',
                'name' => 'Croatian',
            ),
            380 =>
            array (
                'id' => 1381,
                'language_code' => 'hr',
                'display_language_code' => 'eu',
                'name' => 'Croatian',
            ),
            381 =>
            array (
                'id' => 1382,
                'language_code' => 'hr',
                'display_language_code' => 'fa',
                'name' => 'Croatian',
            ),
            382 =>
            array (
                'id' => 1383,
                'language_code' => 'hr',
                'display_language_code' => 'fi',
                'name' => 'Kroatia',
            ),
            383 =>
            array (
                'id' => 1384,
                'language_code' => 'hr',
                'display_language_code' => 'ga',
                'name' => 'Croatian',
            ),
            384 =>
            array (
                'id' => 1385,
                'language_code' => 'hr',
                'display_language_code' => 'he',
                'name' => 'קרוטאית',
            ),
            385 =>
            array (
                'id' => 1386,
                'language_code' => 'hr',
                'display_language_code' => 'hi',
                'name' => 'Croatian',
            ),
            386 =>
            array (
                'id' => 1387,
                'language_code' => 'hr',
                'display_language_code' => 'hr',
                'name' => 'Hrvatski',
            ),
            387 =>
            array (
                'id' => 1388,
                'language_code' => 'hr',
                'display_language_code' => 'hu',
                'name' => 'Horvát',
            ),
            388 =>
            array (
                'id' => 1389,
                'language_code' => 'hr',
                'display_language_code' => 'hy',
                'name' => 'Croatian',
            ),
            389 =>
            array (
                'id' => 1390,
                'language_code' => 'hr',
                'display_language_code' => 'id',
                'name' => 'Croatian',
            ),
            390 =>
            array (
                'id' => 1391,
                'language_code' => 'hr',
                'display_language_code' => 'is',
                'name' => 'Croatian',
            ),
            391 =>
            array (
                'id' => 1392,
                'language_code' => 'hr',
                'display_language_code' => 'it',
                'name' => 'Croato',
            ),
            392 =>
            array (
                'id' => 1393,
                'language_code' => 'hr',
                'display_language_code' => 'ja',
                'name' => 'クロアチア語',
            ),
            393 =>
            array (
                'id' => 1394,
                'language_code' => 'hr',
                'display_language_code' => 'ko',
                'name' => '크로아시아어',
            ),
            394 =>
            array (
                'id' => 1395,
                'language_code' => 'hr',
                'display_language_code' => 'ku',
                'name' => 'Croatian',
            ),
            395 =>
            array (
                'id' => 1396,
                'language_code' => 'hr',
                'display_language_code' => 'lv',
                'name' => 'Croatian',
            ),
            396 =>
            array (
                'id' => 1397,
                'language_code' => 'hr',
                'display_language_code' => 'lt',
                'name' => 'Croatian',
            ),
            397 =>
            array (
                'id' => 1398,
                'language_code' => 'hr',
                'display_language_code' => 'mk',
                'name' => 'Croatian',
            ),
            398 =>
            array (
                'id' => 1399,
                'language_code' => 'hr',
                'display_language_code' => 'mt',
                'name' => 'Croatian',
            ),
            399 =>
            array (
                'id' => 1400,
                'language_code' => 'hr',
                'display_language_code' => 'mn',
                'name' => 'Croatian',
            ),
            400 =>
            array (
                'id' => 1401,
                'language_code' => 'hr',
                'display_language_code' => 'ne',
                'name' => 'Croatian',
            ),
            401 =>
            array (
                'id' => 1402,
                'language_code' => 'hr',
                'display_language_code' => 'nl',
                'name' => 'Kroatisch',
            ),
            402 =>
            array (
                'id' => 1403,
                'language_code' => 'hr',
                'display_language_code' => 'no',
                'name' => 'Kroatisk',
            ),
            403 =>
            array (
                'id' => 1404,
                'language_code' => 'hr',
                'display_language_code' => 'pa',
                'name' => 'Croatian',
            ),
            404 =>
            array (
                'id' => 1405,
                'language_code' => 'hr',
                'display_language_code' => 'pl',
                'name' => 'Chorwacki',
            ),
            405 =>
            array (
                'id' => 1406,
                'language_code' => 'hr',
                'display_language_code' => 'pt-pt',
                'name' => 'Croata',
            ),
            406 =>
            array (
                'id' => 1407,
                'language_code' => 'hr',
                'display_language_code' => 'pt-br',
                'name' => 'Croata',
            ),
            407 =>
            array (
                'id' => 1408,
                'language_code' => 'hr',
                'display_language_code' => 'qu',
                'name' => 'Croatian',
            ),
            408 =>
            array (
                'id' => 1409,
                'language_code' => 'hr',
                'display_language_code' => 'ro',
                'name' => 'Croată',
            ),
            409 =>
            array (
                'id' => 1410,
                'language_code' => 'hr',
                'display_language_code' => 'ru',
                'name' => 'Хорватский',
            ),
            410 =>
            array (
                'id' => 1411,
                'language_code' => 'hr',
                'display_language_code' => 'sl',
                'name' => 'Hrvaški',
            ),
            411 =>
            array (
                'id' => 1412,
                'language_code' => 'hr',
                'display_language_code' => 'so',
                'name' => 'Croatian',
            ),
            412 =>
            array (
                'id' => 1413,
                'language_code' => 'hr',
                'display_language_code' => 'sq',
                'name' => 'Croatian',
            ),
            413 =>
            array (
                'id' => 1414,
                'language_code' => 'hr',
                'display_language_code' => 'sr',
                'name' => 'хрватски',
            ),
            414 =>
            array (
                'id' => 1415,
                'language_code' => 'hr',
                'display_language_code' => 'sv',
                'name' => 'Kroatiska',
            ),
            415 =>
            array (
                'id' => 1416,
                'language_code' => 'hr',
                'display_language_code' => 'ta',
                'name' => 'Croatian',
            ),
            416 =>
            array (
                'id' => 1417,
                'language_code' => 'hr',
                'display_language_code' => 'th',
                'name' => 'โครเอเชีย',
            ),
            417 =>
            array (
                'id' => 1418,
                'language_code' => 'hr',
                'display_language_code' => 'tr',
                'name' => 'Hırvatça',
            ),
            418 =>
            array (
                'id' => 1419,
                'language_code' => 'hr',
                'display_language_code' => 'uk',
                'name' => 'Croatian',
            ),
            419 =>
            array (
                'id' => 1420,
                'language_code' => 'hr',
                'display_language_code' => 'ur',
                'name' => 'Croatian',
            ),
            420 =>
            array (
                'id' => 1421,
                'language_code' => 'hr',
                'display_language_code' => 'uz',
                'name' => 'Croatian',
            ),
            421 =>
            array (
                'id' => 1422,
                'language_code' => 'hr',
                'display_language_code' => 'vi',
                'name' => 'Croatian',
            ),
            422 =>
            array (
                'id' => 1423,
                'language_code' => 'hr',
                'display_language_code' => 'yi',
                'name' => 'Croatian',
            ),
            423 =>
            array (
                'id' => 1424,
                'language_code' => 'hr',
                'display_language_code' => 'zh-hans',
                'name' => '克罗地亚语',
            ),
            424 =>
            array (
                'id' => 1425,
                'language_code' => 'hr',
                'display_language_code' => 'zu',
                'name' => 'Croatian',
            ),
            425 =>
            array (
                'id' => 1426,
                'language_code' => 'hr',
                'display_language_code' => 'zh-hant',
                'name' => '克羅地亞語',
            ),
            426 =>
            array (
                'id' => 1427,
                'language_code' => 'hr',
                'display_language_code' => 'ms',
                'name' => 'Croatian',
            ),
            427 =>
            array (
                'id' => 1428,
                'language_code' => 'hr',
                'display_language_code' => 'gl',
                'name' => 'Croatian',
            ),
            428 =>
            array (
                'id' => 1429,
                'language_code' => 'hr',
                'display_language_code' => 'bn',
                'name' => 'Croatian',
            ),
            429 =>
            array (
                'id' => 1430,
                'language_code' => 'hr',
                'display_language_code' => 'az',
                'name' => 'Croatian',
            ),
            430 =>
            array (
                'id' => 1431,
                'language_code' => 'hu',
                'display_language_code' => 'en',
                'name' => 'Hungarian',
            ),
            431 =>
            array (
                'id' => 1432,
                'language_code' => 'hu',
                'display_language_code' => 'es',
                'name' => 'Húngaro',
            ),
            432 =>
            array (
                'id' => 1433,
                'language_code' => 'hu',
                'display_language_code' => 'de',
                'name' => 'Ungarisch',
            ),
            433 =>
            array (
                'id' => 1434,
                'language_code' => 'hu',
                'display_language_code' => 'fr',
                'name' => 'Hongrois',
            ),
            434 =>
            array (
                'id' => 1435,
                'language_code' => 'hu',
                'display_language_code' => 'ar',
                'name' => 'الهنغارية',
            ),
            435 =>
            array (
                'id' => 1436,
                'language_code' => 'hu',
                'display_language_code' => 'bs',
                'name' => 'Hungarian',
            ),
            436 =>
            array (
                'id' => 1437,
                'language_code' => 'hu',
                'display_language_code' => 'bg',
                'name' => 'Унгарски',
            ),
            437 =>
            array (
                'id' => 1438,
                'language_code' => 'hu',
                'display_language_code' => 'ca',
                'name' => 'Hungarian',
            ),
            438 =>
            array (
                'id' => 1439,
                'language_code' => 'hu',
                'display_language_code' => 'cs',
                'name' => 'Maďarština',
            ),
            439 =>
            array (
                'id' => 1440,
                'language_code' => 'hu',
                'display_language_code' => 'sk',
                'name' => 'Maďarčina',
            ),
            440 =>
            array (
                'id' => 1441,
                'language_code' => 'hu',
                'display_language_code' => 'cy',
                'name' => 'Hungarian',
            ),
            441 =>
            array (
                'id' => 1442,
                'language_code' => 'hu',
                'display_language_code' => 'da',
                'name' => 'Hungarian',
            ),
            442 =>
            array (
                'id' => 1443,
                'language_code' => 'hu',
                'display_language_code' => 'el',
                'name' => 'Ουγγρικά',
            ),
            443 =>
            array (
                'id' => 1444,
                'language_code' => 'hu',
                'display_language_code' => 'eo',
                'name' => 'Hungarian',
            ),
            444 =>
            array (
                'id' => 1445,
                'language_code' => 'hu',
                'display_language_code' => 'et',
                'name' => 'Hungarian',
            ),
            445 =>
            array (
                'id' => 1446,
                'language_code' => 'hu',
                'display_language_code' => 'eu',
                'name' => 'Hungarian',
            ),
            446 =>
            array (
                'id' => 1447,
                'language_code' => 'hu',
                'display_language_code' => 'fa',
                'name' => 'Hungarian',
            ),
            447 =>
            array (
                'id' => 1448,
                'language_code' => 'hu',
                'display_language_code' => 'fi',
                'name' => 'Unkari',
            ),
            448 =>
            array (
                'id' => 1449,
                'language_code' => 'hu',
                'display_language_code' => 'ga',
                'name' => 'Hungarian',
            ),
            449 =>
            array (
                'id' => 1450,
                'language_code' => 'hu',
                'display_language_code' => 'he',
                'name' => 'הונגרית',
            ),
            450 =>
            array (
                'id' => 1451,
                'language_code' => 'hu',
                'display_language_code' => 'hi',
                'name' => 'Hungarian',
            ),
            451 =>
            array (
                'id' => 1452,
                'language_code' => 'hu',
                'display_language_code' => 'hr',
                'name' => 'Mađarski',
            ),
            452 =>
            array (
                'id' => 1453,
                'language_code' => 'hu',
                'display_language_code' => 'hu',
                'name' => 'Magyar',
            ),
            453 =>
            array (
                'id' => 1454,
                'language_code' => 'hu',
                'display_language_code' => 'hy',
                'name' => 'Hungarian',
            ),
            454 =>
            array (
                'id' => 1455,
                'language_code' => 'hu',
                'display_language_code' => 'id',
                'name' => 'Hungarian',
            ),
            455 =>
            array (
                'id' => 1456,
                'language_code' => 'hu',
                'display_language_code' => 'is',
                'name' => 'Hungarian',
            ),
            456 =>
            array (
                'id' => 1457,
                'language_code' => 'hu',
                'display_language_code' => 'it',
                'name' => 'Ungherese',
            ),
            457 =>
            array (
                'id' => 1458,
                'language_code' => 'hu',
                'display_language_code' => 'ja',
                'name' => 'ハンガリー語',
            ),
            458 =>
            array (
                'id' => 1459,
                'language_code' => 'hu',
                'display_language_code' => 'ko',
                'name' => '헝가리어',
            ),
            459 =>
            array (
                'id' => 1460,
                'language_code' => 'hu',
                'display_language_code' => 'ku',
                'name' => 'Hungarian',
            ),
            460 =>
            array (
                'id' => 1461,
                'language_code' => 'hu',
                'display_language_code' => 'lv',
                'name' => 'Hungarian',
            ),
            461 =>
            array (
                'id' => 1462,
                'language_code' => 'hu',
                'display_language_code' => 'lt',
                'name' => 'Hungarian',
            ),
            462 =>
            array (
                'id' => 1463,
                'language_code' => 'hu',
                'display_language_code' => 'mk',
                'name' => 'Hungarian',
            ),
            463 =>
            array (
                'id' => 1464,
                'language_code' => 'hu',
                'display_language_code' => 'mt',
                'name' => 'Hungarian',
            ),
            464 =>
            array (
                'id' => 1465,
                'language_code' => 'hu',
                'display_language_code' => 'mn',
                'name' => 'Hungarian',
            ),
            465 =>
            array (
                'id' => 1466,
                'language_code' => 'hu',
                'display_language_code' => 'ne',
                'name' => 'Hungarian',
            ),
            466 =>
            array (
                'id' => 1467,
                'language_code' => 'hu',
                'display_language_code' => 'nl',
                'name' => 'Hongaars',
            ),
            467 =>
            array (
                'id' => 1468,
                'language_code' => 'hu',
                'display_language_code' => 'no',
                'name' => 'Ungarsk',
            ),
            468 =>
            array (
                'id' => 1469,
                'language_code' => 'hu',
                'display_language_code' => 'pa',
                'name' => 'Hungarian',
            ),
            469 =>
            array (
                'id' => 1470,
                'language_code' => 'hu',
                'display_language_code' => 'pl',
                'name' => 'Węgierski',
            ),
            470 =>
            array (
                'id' => 1471,
                'language_code' => 'hu',
                'display_language_code' => 'pt-pt',
                'name' => 'Húngaro',
            ),
            471 =>
            array (
                'id' => 1472,
                'language_code' => 'hu',
                'display_language_code' => 'pt-br',
                'name' => 'Húngaro',
            ),
            472 =>
            array (
                'id' => 1473,
                'language_code' => 'hu',
                'display_language_code' => 'qu',
                'name' => 'Hungarian',
            ),
            473 =>
            array (
                'id' => 1474,
                'language_code' => 'hu',
                'display_language_code' => 'ro',
                'name' => 'Ungară',
            ),
            474 =>
            array (
                'id' => 1475,
                'language_code' => 'hu',
                'display_language_code' => 'ru',
                'name' => 'Венгерский',
            ),
            475 =>
            array (
                'id' => 1476,
                'language_code' => 'hu',
                'display_language_code' => 'sl',
                'name' => 'Madžarski',
            ),
            476 =>
            array (
                'id' => 1477,
                'language_code' => 'hu',
                'display_language_code' => 'so',
                'name' => 'Hungarian',
            ),
            477 =>
            array (
                'id' => 1478,
                'language_code' => 'hu',
                'display_language_code' => 'sq',
                'name' => 'Hungarian',
            ),
            478 =>
            array (
                'id' => 1479,
                'language_code' => 'hu',
                'display_language_code' => 'sr',
                'name' => 'мађарски',
            ),
            479 =>
            array (
                'id' => 1480,
                'language_code' => 'hu',
                'display_language_code' => 'sv',
                'name' => 'Ungerska',
            ),
            480 =>
            array (
                'id' => 1481,
                'language_code' => 'hu',
                'display_language_code' => 'ta',
                'name' => 'Hungarian',
            ),
            481 =>
            array (
                'id' => 1482,
                'language_code' => 'hu',
                'display_language_code' => 'th',
                'name' => 'ฮังการี',
            ),
            482 =>
            array (
                'id' => 1483,
                'language_code' => 'hu',
                'display_language_code' => 'tr',
                'name' => 'Macarca',
            ),
            483 =>
            array (
                'id' => 1484,
                'language_code' => 'hu',
                'display_language_code' => 'uk',
                'name' => 'Hungarian',
            ),
            484 =>
            array (
                'id' => 1485,
                'language_code' => 'hu',
                'display_language_code' => 'ur',
                'name' => 'Hungarian',
            ),
            485 =>
            array (
                'id' => 1486,
                'language_code' => 'hu',
                'display_language_code' => 'uz',
                'name' => 'Hungarian',
            ),
            486 =>
            array (
                'id' => 1487,
                'language_code' => 'hu',
                'display_language_code' => 'vi',
                'name' => 'Hungarian',
            ),
            487 =>
            array (
                'id' => 1488,
                'language_code' => 'hu',
                'display_language_code' => 'yi',
                'name' => 'Hungarian',
            ),
            488 =>
            array (
                'id' => 1489,
                'language_code' => 'hu',
                'display_language_code' => 'zh-hans',
                'name' => '匈牙利语',
            ),
            489 =>
            array (
                'id' => 1490,
                'language_code' => 'hu',
                'display_language_code' => 'zu',
                'name' => 'Hungarian',
            ),
            490 =>
            array (
                'id' => 1491,
                'language_code' => 'hu',
                'display_language_code' => 'zh-hant',
                'name' => '匈牙利語',
            ),
            491 =>
            array (
                'id' => 1492,
                'language_code' => 'hu',
                'display_language_code' => 'ms',
                'name' => 'Hungarian',
            ),
            492 =>
            array (
                'id' => 1493,
                'language_code' => 'hu',
                'display_language_code' => 'gl',
                'name' => 'Hungarian',
            ),
            493 =>
            array (
                'id' => 1494,
                'language_code' => 'hu',
                'display_language_code' => 'bn',
                'name' => 'Hungarian',
            ),
            494 =>
            array (
                'id' => 1495,
                'language_code' => 'hu',
                'display_language_code' => 'az',
                'name' => 'Hungarian',
            ),
            495 =>
            array (
                'id' => 1496,
                'language_code' => 'hy',
                'display_language_code' => 'en',
                'name' => 'Armenian',
            ),
            496 =>
            array (
                'id' => 1497,
                'language_code' => 'hy',
                'display_language_code' => 'es',
                'name' => 'Armenio',
            ),
            497 =>
            array (
                'id' => 1498,
                'language_code' => 'hy',
                'display_language_code' => 'de',
                'name' => 'Armenisch',
            ),
            498 =>
            array (
                'id' => 1499,
                'language_code' => 'hy',
                'display_language_code' => 'fr',
                'name' => 'Arménien',
            ),
            499 =>
            array (
                'id' => 1500,
                'language_code' => 'hy',
                'display_language_code' => 'ar',
                'name' => 'الأرمينية',
            ),
        ));
        \DB::table('language_translations')->insert(array (
            0 =>
            array (
                'id' => 1501,
                'language_code' => 'hy',
                'display_language_code' => 'bs',
                'name' => 'Armenian',
            ),
            1 =>
            array (
                'id' => 1502,
                'language_code' => 'hy',
                'display_language_code' => 'bg',
                'name' => 'Арменски',
            ),
            2 =>
            array (
                'id' => 1503,
                'language_code' => 'hy',
                'display_language_code' => 'ca',
                'name' => 'Armenian',
            ),
            3 =>
            array (
                'id' => 1504,
                'language_code' => 'hy',
                'display_language_code' => 'cs',
                'name' => 'Arménský',
            ),
            4 =>
            array (
                'id' => 1505,
                'language_code' => 'hy',
                'display_language_code' => 'sk',
                'name' => 'Arménčina',
            ),
            5 =>
            array (
                'id' => 1506,
                'language_code' => 'hy',
                'display_language_code' => 'cy',
                'name' => 'Armenian',
            ),
            6 =>
            array (
                'id' => 1507,
                'language_code' => 'hy',
                'display_language_code' => 'da',
                'name' => 'Armenian',
            ),
            7 =>
            array (
                'id' => 1508,
                'language_code' => 'hy',
                'display_language_code' => 'el',
                'name' => 'Αρμένικα',
            ),
            8 =>
            array (
                'id' => 1509,
                'language_code' => 'hy',
                'display_language_code' => 'eo',
                'name' => 'Armenian',
            ),
            9 =>
            array (
                'id' => 1510,
                'language_code' => 'hy',
                'display_language_code' => 'et',
                'name' => 'Armenian',
            ),
            10 =>
            array (
                'id' => 1511,
                'language_code' => 'hy',
                'display_language_code' => 'eu',
                'name' => 'Armenian',
            ),
            11 =>
            array (
                'id' => 1512,
                'language_code' => 'hy',
                'display_language_code' => 'fa',
                'name' => 'Armenian',
            ),
            12 =>
            array (
                'id' => 1513,
                'language_code' => 'hy',
                'display_language_code' => 'fi',
                'name' => 'Armenia',
            ),
            13 =>
            array (
                'id' => 1514,
                'language_code' => 'hy',
                'display_language_code' => 'ga',
                'name' => 'Armenian',
            ),
            14 =>
            array (
                'id' => 1515,
                'language_code' => 'hy',
                'display_language_code' => 'he',
                'name' => 'ארמנית',
            ),
            15 =>
            array (
                'id' => 1516,
                'language_code' => 'hy',
                'display_language_code' => 'hi',
                'name' => 'Armenian',
            ),
            16 =>
            array (
                'id' => 1517,
                'language_code' => 'hy',
                'display_language_code' => 'hr',
                'name' => 'Armenski',
            ),
            17 =>
            array (
                'id' => 1518,
                'language_code' => 'hy',
                'display_language_code' => 'hu',
                'name' => 'örmény',
            ),
            18 =>
            array (
                'id' => 1519,
                'language_code' => 'hy',
                'display_language_code' => 'hy',
                'name' => 'Հայերեն',
            ),
            19 =>
            array (
                'id' => 1520,
                'language_code' => 'hy',
                'display_language_code' => 'id',
                'name' => 'Armenian',
            ),
            20 =>
            array (
                'id' => 1521,
                'language_code' => 'hy',
                'display_language_code' => 'is',
                'name' => 'Armenian',
            ),
            21 =>
            array (
                'id' => 1522,
                'language_code' => 'hy',
                'display_language_code' => 'it',
                'name' => 'Armeno',
            ),
            22 =>
            array (
                'id' => 1523,
                'language_code' => 'hy',
                'display_language_code' => 'ja',
                'name' => 'アルメニア語',
            ),
            23 =>
            array (
                'id' => 1524,
                'language_code' => 'hy',
                'display_language_code' => 'ko',
                'name' => '아르메니아어',
            ),
            24 =>
            array (
                'id' => 1525,
                'language_code' => 'hy',
                'display_language_code' => 'ku',
                'name' => 'Armenian',
            ),
            25 =>
            array (
                'id' => 1526,
                'language_code' => 'hy',
                'display_language_code' => 'lv',
                'name' => 'Armenian',
            ),
            26 =>
            array (
                'id' => 1527,
                'language_code' => 'hy',
                'display_language_code' => 'lt',
                'name' => 'Armenian',
            ),
            27 =>
            array (
                'id' => 1528,
                'language_code' => 'hy',
                'display_language_code' => 'mk',
                'name' => 'Armenian',
            ),
            28 =>
            array (
                'id' => 1529,
                'language_code' => 'hy',
                'display_language_code' => 'mt',
                'name' => 'Armenian',
            ),
            29 =>
            array (
                'id' => 1530,
                'language_code' => 'hy',
                'display_language_code' => 'mn',
                'name' => 'Armenian',
            ),
            30 =>
            array (
                'id' => 1531,
                'language_code' => 'hy',
                'display_language_code' => 'ne',
                'name' => 'Armenian',
            ),
            31 =>
            array (
                'id' => 1532,
                'language_code' => 'hy',
                'display_language_code' => 'nl',
                'name' => 'Armeens',
            ),
            32 =>
            array (
                'id' => 1533,
                'language_code' => 'hy',
                'display_language_code' => 'no',
                'name' => 'Armensk',
            ),
            33 =>
            array (
                'id' => 1534,
                'language_code' => 'hy',
                'display_language_code' => 'pa',
                'name' => 'Armenian',
            ),
            34 =>
            array (
                'id' => 1535,
                'language_code' => 'hy',
                'display_language_code' => 'pl',
                'name' => 'Ormiański',
            ),
            35 =>
            array (
                'id' => 1536,
                'language_code' => 'hy',
                'display_language_code' => 'pt-pt',
                'name' => 'Arménio',
            ),
            36 =>
            array (
                'id' => 1537,
                'language_code' => 'hy',
                'display_language_code' => 'pt-br',
                'name' => 'Arménio',
            ),
            37 =>
            array (
                'id' => 1538,
                'language_code' => 'hy',
                'display_language_code' => 'qu',
                'name' => 'Armenian',
            ),
            38 =>
            array (
                'id' => 1539,
                'language_code' => 'hy',
                'display_language_code' => 'ro',
                'name' => 'Armeană',
            ),
            39 =>
            array (
                'id' => 1540,
                'language_code' => 'hy',
                'display_language_code' => 'ru',
                'name' => 'Армянский',
            ),
            40 =>
            array (
                'id' => 1541,
                'language_code' => 'hy',
                'display_language_code' => 'sl',
                'name' => 'Armenski',
            ),
            41 =>
            array (
                'id' => 1542,
                'language_code' => 'hy',
                'display_language_code' => 'so',
                'name' => 'Armenian',
            ),
            42 =>
            array (
                'id' => 1543,
                'language_code' => 'hy',
                'display_language_code' => 'sq',
                'name' => 'Armenian',
            ),
            43 =>
            array (
                'id' => 1544,
                'language_code' => 'hy',
                'display_language_code' => 'sr',
                'name' => 'јерменски',
            ),
            44 =>
            array (
                'id' => 1545,
                'language_code' => 'hy',
                'display_language_code' => 'sv',
                'name' => 'Armeniska',
            ),
            45 =>
            array (
                'id' => 1546,
                'language_code' => 'hy',
                'display_language_code' => 'ta',
                'name' => 'Armenian',
            ),
            46 =>
            array (
                'id' => 1547,
                'language_code' => 'hy',
                'display_language_code' => 'th',
                'name' => 'อาร์เมเนีย',
            ),
            47 =>
            array (
                'id' => 1548,
                'language_code' => 'hy',
                'display_language_code' => 'tr',
                'name' => 'Ermenice',
            ),
            48 =>
            array (
                'id' => 1549,
                'language_code' => 'hy',
                'display_language_code' => 'uk',
                'name' => 'Armenian',
            ),
            49 =>
            array (
                'id' => 1550,
                'language_code' => 'hy',
                'display_language_code' => 'ur',
                'name' => 'Armenian',
            ),
            50 =>
            array (
                'id' => 1551,
                'language_code' => 'hy',
                'display_language_code' => 'uz',
                'name' => 'Armenian',
            ),
            51 =>
            array (
                'id' => 1552,
                'language_code' => 'hy',
                'display_language_code' => 'vi',
                'name' => 'Armenian',
            ),
            52 =>
            array (
                'id' => 1553,
                'language_code' => 'hy',
                'display_language_code' => 'yi',
                'name' => 'Armenian',
            ),
            53 =>
            array (
                'id' => 1554,
                'language_code' => 'hy',
                'display_language_code' => 'zh-hans',
                'name' => '亚美尼亚语',
            ),
            54 =>
            array (
                'id' => 1555,
                'language_code' => 'hy',
                'display_language_code' => 'zu',
                'name' => 'Armenian',
            ),
            55 =>
            array (
                'id' => 1556,
                'language_code' => 'hy',
                'display_language_code' => 'zh-hant',
                'name' => '亞美尼亞語',
            ),
            56 =>
            array (
                'id' => 1557,
                'language_code' => 'hy',
                'display_language_code' => 'ms',
                'name' => 'Armenian',
            ),
            57 =>
            array (
                'id' => 1558,
                'language_code' => 'hy',
                'display_language_code' => 'gl',
                'name' => 'Armenian',
            ),
            58 =>
            array (
                'id' => 1559,
                'language_code' => 'hy',
                'display_language_code' => 'bn',
                'name' => 'Armenian',
            ),
            59 =>
            array (
                'id' => 1560,
                'language_code' => 'hy',
                'display_language_code' => 'az',
                'name' => 'Armenian',
            ),
            60 =>
            array (
                'id' => 1561,
                'language_code' => 'id',
                'display_language_code' => 'en',
                'name' => 'Indonesian',
            ),
            61 =>
            array (
                'id' => 1562,
                'language_code' => 'id',
                'display_language_code' => 'es',
                'name' => 'Indonesio',
            ),
            62 =>
            array (
                'id' => 1563,
                'language_code' => 'id',
                'display_language_code' => 'de',
                'name' => 'Indonesisch',
            ),
            63 =>
            array (
                'id' => 1564,
                'language_code' => 'id',
                'display_language_code' => 'fr',
                'name' => 'Indonésien',
            ),
            64 =>
            array (
                'id' => 1565,
                'language_code' => 'id',
                'display_language_code' => 'ar',
                'name' => 'الأندونيسية',
            ),
            65 =>
            array (
                'id' => 1566,
                'language_code' => 'id',
                'display_language_code' => 'bs',
                'name' => 'Indonesian',
            ),
            66 =>
            array (
                'id' => 1567,
                'language_code' => 'id',
                'display_language_code' => 'bg',
                'name' => 'Индонезийски',
            ),
            67 =>
            array (
                'id' => 1568,
                'language_code' => 'id',
                'display_language_code' => 'ca',
                'name' => 'Indonesian',
            ),
            68 =>
            array (
                'id' => 1569,
                'language_code' => 'id',
                'display_language_code' => 'cs',
                'name' => 'Indonéský',
            ),
            69 =>
            array (
                'id' => 1570,
                'language_code' => 'id',
                'display_language_code' => 'sk',
                'name' => 'Indonézčina',
            ),
            70 =>
            array (
                'id' => 1571,
                'language_code' => 'id',
                'display_language_code' => 'cy',
                'name' => 'Indonesian',
            ),
            71 =>
            array (
                'id' => 1572,
                'language_code' => 'id',
                'display_language_code' => 'da',
                'name' => 'Indonesian',
            ),
            72 =>
            array (
                'id' => 1573,
                'language_code' => 'id',
                'display_language_code' => 'el',
                'name' => 'Ινδονησιακά',
            ),
            73 =>
            array (
                'id' => 1574,
                'language_code' => 'id',
                'display_language_code' => 'eo',
                'name' => 'Indonesian',
            ),
            74 =>
            array (
                'id' => 1575,
                'language_code' => 'id',
                'display_language_code' => 'et',
                'name' => 'Indonesian',
            ),
            75 =>
            array (
                'id' => 1576,
                'language_code' => 'id',
                'display_language_code' => 'eu',
                'name' => 'Indonesian',
            ),
            76 =>
            array (
                'id' => 1577,
                'language_code' => 'id',
                'display_language_code' => 'fa',
                'name' => 'Indonesian',
            ),
            77 =>
            array (
                'id' => 1578,
                'language_code' => 'id',
                'display_language_code' => 'fi',
                'name' => 'Indonesia',
            ),
            78 =>
            array (
                'id' => 1579,
                'language_code' => 'id',
                'display_language_code' => 'ga',
                'name' => 'Indonesian',
            ),
            79 =>
            array (
                'id' => 1580,
                'language_code' => 'id',
                'display_language_code' => 'he',
                'name' => 'אינדונזית',
            ),
            80 =>
            array (
                'id' => 1581,
                'language_code' => 'id',
                'display_language_code' => 'hi',
                'name' => 'Indonesian',
            ),
            81 =>
            array (
                'id' => 1582,
                'language_code' => 'id',
                'display_language_code' => 'hr',
                'name' => 'Indonezijski',
            ),
            82 =>
            array (
                'id' => 1583,
                'language_code' => 'id',
                'display_language_code' => 'hu',
                'name' => 'Indonéz',
            ),
            83 =>
            array (
                'id' => 1584,
                'language_code' => 'id',
                'display_language_code' => 'hy',
                'name' => 'Indonesian',
            ),
            84 =>
            array (
                'id' => 1585,
                'language_code' => 'id',
                'display_language_code' => 'id',
                'name' => 'Indonesia',
            ),
            85 =>
            array (
                'id' => 1586,
                'language_code' => 'id',
                'display_language_code' => 'is',
                'name' => 'Indonesian',
            ),
            86 =>
            array (
                'id' => 1587,
                'language_code' => 'id',
                'display_language_code' => 'it',
                'name' => 'Indonesiano',
            ),
            87 =>
            array (
                'id' => 1588,
                'language_code' => 'id',
                'display_language_code' => 'ja',
                'name' => 'インドネシア語',
            ),
            88 =>
            array (
                'id' => 1589,
                'language_code' => 'id',
                'display_language_code' => 'ko',
                'name' => '인도네시아어',
            ),
            89 =>
            array (
                'id' => 1590,
                'language_code' => 'id',
                'display_language_code' => 'ku',
                'name' => 'Indonesian',
            ),
            90 =>
            array (
                'id' => 1591,
                'language_code' => 'id',
                'display_language_code' => 'lv',
                'name' => 'Indonesian',
            ),
            91 =>
            array (
                'id' => 1592,
                'language_code' => 'id',
                'display_language_code' => 'lt',
                'name' => 'Indonesian',
            ),
            92 =>
            array (
                'id' => 1593,
                'language_code' => 'id',
                'display_language_code' => 'mk',
                'name' => 'Indonesian',
            ),
            93 =>
            array (
                'id' => 1594,
                'language_code' => 'id',
                'display_language_code' => 'mt',
                'name' => 'Indonesian',
            ),
            94 =>
            array (
                'id' => 1595,
                'language_code' => 'id',
                'display_language_code' => 'mn',
                'name' => 'Indonesian',
            ),
            95 =>
            array (
                'id' => 1596,
                'language_code' => 'id',
                'display_language_code' => 'ne',
                'name' => 'Indonesian',
            ),
            96 =>
            array (
                'id' => 1597,
                'language_code' => 'id',
                'display_language_code' => 'nl',
                'name' => 'Indonesisch',
            ),
            97 =>
            array (
                'id' => 1598,
                'language_code' => 'id',
                'display_language_code' => 'no',
                'name' => 'Indonesian',
            ),
            98 =>
            array (
                'id' => 1599,
                'language_code' => 'id',
                'display_language_code' => 'pa',
                'name' => 'Indonesian',
            ),
            99 =>
            array (
                'id' => 1600,
                'language_code' => 'id',
                'display_language_code' => 'pl',
                'name' => 'Indonezyjski',
            ),
            100 =>
            array (
                'id' => 1601,
                'language_code' => 'id',
                'display_language_code' => 'pt-pt',
                'name' => 'Indonésio',
            ),
            101 =>
            array (
                'id' => 1602,
                'language_code' => 'id',
                'display_language_code' => 'pt-br',
                'name' => 'Indonésio',
            ),
            102 =>
            array (
                'id' => 1603,
                'language_code' => 'id',
                'display_language_code' => 'qu',
                'name' => 'Indonesian',
            ),
            103 =>
            array (
                'id' => 1604,
                'language_code' => 'id',
                'display_language_code' => 'ro',
                'name' => 'Indoneziană',
            ),
            104 =>
            array (
                'id' => 1605,
                'language_code' => 'id',
                'display_language_code' => 'ru',
                'name' => 'Индонезийский',
            ),
            105 =>
            array (
                'id' => 1606,
                'language_code' => 'id',
                'display_language_code' => 'sl',
                'name' => 'Indonezijski',
            ),
            106 =>
            array (
                'id' => 1607,
                'language_code' => 'id',
                'display_language_code' => 'so',
                'name' => 'Indonesian',
            ),
            107 =>
            array (
                'id' => 1608,
                'language_code' => 'id',
                'display_language_code' => 'sq',
                'name' => 'Indonesian',
            ),
            108 =>
            array (
                'id' => 1609,
                'language_code' => 'id',
                'display_language_code' => 'sr',
                'name' => 'индонезијски',
            ),
            109 =>
            array (
                'id' => 1610,
                'language_code' => 'id',
                'display_language_code' => 'sv',
                'name' => 'Indonesiska',
            ),
            110 =>
            array (
                'id' => 1611,
                'language_code' => 'id',
                'display_language_code' => 'ta',
                'name' => 'Indonesian',
            ),
            111 =>
            array (
                'id' => 1612,
                'language_code' => 'id',
                'display_language_code' => 'th',
                'name' => 'อินโดนีเซีย',
            ),
            112 =>
            array (
                'id' => 1613,
                'language_code' => 'id',
                'display_language_code' => 'tr',
                'name' => 'Endonezya dili',
            ),
            113 =>
            array (
                'id' => 1614,
                'language_code' => 'id',
                'display_language_code' => 'uk',
                'name' => 'Indonesian',
            ),
            114 =>
            array (
                'id' => 1615,
                'language_code' => 'id',
                'display_language_code' => 'ur',
                'name' => 'Indonesian',
            ),
            115 =>
            array (
                'id' => 1616,
                'language_code' => 'id',
                'display_language_code' => 'uz',
                'name' => 'Indonesian',
            ),
            116 =>
            array (
                'id' => 1617,
                'language_code' => 'id',
                'display_language_code' => 'vi',
                'name' => 'Indonesian',
            ),
            117 =>
            array (
                'id' => 1618,
                'language_code' => 'id',
                'display_language_code' => 'yi',
                'name' => 'Indonesian',
            ),
            118 =>
            array (
                'id' => 1619,
                'language_code' => 'id',
                'display_language_code' => 'zh-hans',
                'name' => '印度尼西亚语',
            ),
            119 =>
            array (
                'id' => 1620,
                'language_code' => 'id',
                'display_language_code' => 'zu',
                'name' => 'Indonesian',
            ),
            120 =>
            array (
                'id' => 1621,
                'language_code' => 'id',
                'display_language_code' => 'zh-hant',
                'name' => '印尼語',
            ),
            121 =>
            array (
                'id' => 1622,
                'language_code' => 'id',
                'display_language_code' => 'ms',
                'name' => 'Indonesian',
            ),
            122 =>
            array (
                'id' => 1623,
                'language_code' => 'id',
                'display_language_code' => 'gl',
                'name' => 'Indonesian',
            ),
            123 =>
            array (
                'id' => 1624,
                'language_code' => 'id',
                'display_language_code' => 'bn',
                'name' => 'Indonesian',
            ),
            124 =>
            array (
                'id' => 1625,
                'language_code' => 'id',
                'display_language_code' => 'az',
                'name' => 'Indonesian',
            ),
            125 =>
            array (
                'id' => 1626,
                'language_code' => 'is',
                'display_language_code' => 'en',
                'name' => 'Icelandic',
            ),
            126 =>
            array (
                'id' => 1627,
                'language_code' => 'is',
                'display_language_code' => 'es',
                'name' => 'Islandés',
            ),
            127 =>
            array (
                'id' => 1628,
                'language_code' => 'is',
                'display_language_code' => 'de',
                'name' => 'Isländisch',
            ),
            128 =>
            array (
                'id' => 1629,
                'language_code' => 'is',
                'display_language_code' => 'fr',
                'name' => 'Islandais',
            ),
            129 =>
            array (
                'id' => 1630,
                'language_code' => 'is',
                'display_language_code' => 'ar',
                'name' => 'الأيسلاندية',
            ),
            130 =>
            array (
                'id' => 1631,
                'language_code' => 'is',
                'display_language_code' => 'bs',
                'name' => 'Icelandic',
            ),
            131 =>
            array (
                'id' => 1632,
                'language_code' => 'is',
                'display_language_code' => 'bg',
                'name' => 'Исландски',
            ),
            132 =>
            array (
                'id' => 1633,
                'language_code' => 'is',
                'display_language_code' => 'ca',
                'name' => 'Icelandic',
            ),
            133 =>
            array (
                'id' => 1634,
                'language_code' => 'is',
                'display_language_code' => 'cs',
                'name' => 'Islandský',
            ),
            134 =>
            array (
                'id' => 1635,
                'language_code' => 'is',
                'display_language_code' => 'sk',
                'name' => 'Islančina',
            ),
            135 =>
            array (
                'id' => 1636,
                'language_code' => 'is',
                'display_language_code' => 'cy',
                'name' => 'Icelandic',
            ),
            136 =>
            array (
                'id' => 1637,
                'language_code' => 'is',
                'display_language_code' => 'da',
                'name' => 'Icelandic',
            ),
            137 =>
            array (
                'id' => 1638,
                'language_code' => 'is',
                'display_language_code' => 'el',
                'name' => 'Ισλανδικά',
            ),
            138 =>
            array (
                'id' => 1639,
                'language_code' => 'is',
                'display_language_code' => 'eo',
                'name' => 'Icelandic',
            ),
            139 =>
            array (
                'id' => 1640,
                'language_code' => 'is',
                'display_language_code' => 'et',
                'name' => 'Icelandic',
            ),
            140 =>
            array (
                'id' => 1641,
                'language_code' => 'is',
                'display_language_code' => 'eu',
                'name' => 'Icelandic',
            ),
            141 =>
            array (
                'id' => 1642,
                'language_code' => 'is',
                'display_language_code' => 'fa',
                'name' => 'Icelandic',
            ),
            142 =>
            array (
                'id' => 1643,
                'language_code' => 'is',
                'display_language_code' => 'fi',
                'name' => 'Islanti',
            ),
            143 =>
            array (
                'id' => 1644,
                'language_code' => 'is',
                'display_language_code' => 'ga',
                'name' => 'Icelandic',
            ),
            144 =>
            array (
                'id' => 1645,
                'language_code' => 'is',
                'display_language_code' => 'he',
                'name' => 'איסלנדית',
            ),
            145 =>
            array (
                'id' => 1646,
                'language_code' => 'is',
                'display_language_code' => 'hi',
                'name' => 'Icelandic',
            ),
            146 =>
            array (
                'id' => 1647,
                'language_code' => 'is',
                'display_language_code' => 'hr',
                'name' => 'Islandski',
            ),
            147 =>
            array (
                'id' => 1648,
                'language_code' => 'is',
                'display_language_code' => 'hu',
                'name' => 'Izlandi',
            ),
            148 =>
            array (
                'id' => 1649,
                'language_code' => 'is',
                'display_language_code' => 'hy',
                'name' => 'Icelandic',
            ),
            149 =>
            array (
                'id' => 1650,
                'language_code' => 'is',
                'display_language_code' => 'id',
                'name' => 'Icelandic',
            ),
            150 =>
            array (
                'id' => 1651,
                'language_code' => 'is',
                'display_language_code' => 'is',
                'name' => 'Íslenska',
            ),
            151 =>
            array (
                'id' => 1652,
                'language_code' => 'is',
                'display_language_code' => 'it',
                'name' => 'Islandese',
            ),
            152 =>
            array (
                'id' => 1653,
                'language_code' => 'is',
                'display_language_code' => 'ja',
                'name' => 'アイスランド語',
            ),
            153 =>
            array (
                'id' => 1654,
                'language_code' => 'is',
                'display_language_code' => 'ko',
                'name' => '아이슬랜드어',
            ),
            154 =>
            array (
                'id' => 1655,
                'language_code' => 'is',
                'display_language_code' => 'ku',
                'name' => 'Icelandic',
            ),
            155 =>
            array (
                'id' => 1656,
                'language_code' => 'is',
                'display_language_code' => 'lv',
                'name' => 'Icelandic',
            ),
            156 =>
            array (
                'id' => 1657,
                'language_code' => 'is',
                'display_language_code' => 'lt',
                'name' => 'Icelandic',
            ),
            157 =>
            array (
                'id' => 1658,
                'language_code' => 'is',
                'display_language_code' => 'mk',
                'name' => 'Icelandic',
            ),
            158 =>
            array (
                'id' => 1659,
                'language_code' => 'is',
                'display_language_code' => 'mt',
                'name' => 'Icelandic',
            ),
            159 =>
            array (
                'id' => 1660,
                'language_code' => 'is',
                'display_language_code' => 'mn',
                'name' => 'Icelandic',
            ),
            160 =>
            array (
                'id' => 1661,
                'language_code' => 'is',
                'display_language_code' => 'ne',
                'name' => 'Icelandic',
            ),
            161 =>
            array (
                'id' => 1662,
                'language_code' => 'is',
                'display_language_code' => 'nl',
                'name' => 'Ijslands',
            ),
            162 =>
            array (
                'id' => 1663,
                'language_code' => 'is',
                'display_language_code' => 'no',
                'name' => 'Islandsk',
            ),
            163 =>
            array (
                'id' => 1664,
                'language_code' => 'is',
                'display_language_code' => 'pa',
                'name' => 'Icelandic',
            ),
            164 =>
            array (
                'id' => 1665,
                'language_code' => 'is',
                'display_language_code' => 'pl',
                'name' => 'Islandzki',
            ),
            165 =>
            array (
                'id' => 1666,
                'language_code' => 'is',
                'display_language_code' => 'pt-pt',
                'name' => 'Islandês',
            ),
            166 =>
            array (
                'id' => 1667,
                'language_code' => 'is',
                'display_language_code' => 'pt-br',
                'name' => 'Islandês',
            ),
            167 =>
            array (
                'id' => 1668,
                'language_code' => 'is',
                'display_language_code' => 'qu',
                'name' => 'Icelandic',
            ),
            168 =>
            array (
                'id' => 1669,
                'language_code' => 'is',
                'display_language_code' => 'ro',
                'name' => 'Islandeză',
            ),
            169 =>
            array (
                'id' => 1670,
                'language_code' => 'is',
                'display_language_code' => 'ru',
                'name' => 'Исландский',
            ),
            170 =>
            array (
                'id' => 1671,
                'language_code' => 'is',
                'display_language_code' => 'sl',
                'name' => 'Islandski',
            ),
            171 =>
            array (
                'id' => 1672,
                'language_code' => 'is',
                'display_language_code' => 'so',
                'name' => 'Icelandic',
            ),
            172 =>
            array (
                'id' => 1673,
                'language_code' => 'is',
                'display_language_code' => 'sq',
                'name' => 'Icelandic',
            ),
            173 =>
            array (
                'id' => 1674,
                'language_code' => 'is',
                'display_language_code' => 'sr',
                'name' => 'исландски',
            ),
            174 =>
            array (
                'id' => 1675,
                'language_code' => 'is',
                'display_language_code' => 'sv',
                'name' => 'Isländska',
            ),
            175 =>
            array (
                'id' => 1676,
                'language_code' => 'is',
                'display_language_code' => 'ta',
                'name' => 'Icelandic',
            ),
            176 =>
            array (
                'id' => 1677,
                'language_code' => 'is',
                'display_language_code' => 'th',
                'name' => 'ไอซ์แลนด์',
            ),
            177 =>
            array (
                'id' => 1678,
                'language_code' => 'is',
                'display_language_code' => 'tr',
                'name' => 'İzlandaca',
            ),
            178 =>
            array (
                'id' => 1679,
                'language_code' => 'is',
                'display_language_code' => 'uk',
                'name' => 'Icelandic',
            ),
            179 =>
            array (
                'id' => 1680,
                'language_code' => 'is',
                'display_language_code' => 'ur',
                'name' => 'Icelandic',
            ),
            180 =>
            array (
                'id' => 1681,
                'language_code' => 'is',
                'display_language_code' => 'uz',
                'name' => 'Icelandic',
            ),
            181 =>
            array (
                'id' => 1682,
                'language_code' => 'is',
                'display_language_code' => 'vi',
                'name' => 'Icelandic',
            ),
            182 =>
            array (
                'id' => 1683,
                'language_code' => 'is',
                'display_language_code' => 'yi',
                'name' => 'Icelandic',
            ),
            183 =>
            array (
                'id' => 1684,
                'language_code' => 'is',
                'display_language_code' => 'zh-hans',
                'name' => '冰岛语',
            ),
            184 =>
            array (
                'id' => 1685,
                'language_code' => 'is',
                'display_language_code' => 'zu',
                'name' => 'Icelandic',
            ),
            185 =>
            array (
                'id' => 1686,
                'language_code' => 'is',
                'display_language_code' => 'zh-hant',
                'name' => '冰島語',
            ),
            186 =>
            array (
                'id' => 1687,
                'language_code' => 'is',
                'display_language_code' => 'ms',
                'name' => 'Icelandic',
            ),
            187 =>
            array (
                'id' => 1688,
                'language_code' => 'is',
                'display_language_code' => 'gl',
                'name' => 'Icelandic',
            ),
            188 =>
            array (
                'id' => 1689,
                'language_code' => 'is',
                'display_language_code' => 'bn',
                'name' => 'Icelandic',
            ),
            189 =>
            array (
                'id' => 1690,
                'language_code' => 'is',
                'display_language_code' => 'az',
                'name' => 'Icelandic',
            ),
            190 =>
            array (
                'id' => 1691,
                'language_code' => 'it',
                'display_language_code' => 'en',
                'name' => 'Italian',
            ),
            191 =>
            array (
                'id' => 1692,
                'language_code' => 'it',
                'display_language_code' => 'es',
                'name' => 'Italiano',
            ),
            192 =>
            array (
                'id' => 1693,
                'language_code' => 'it',
                'display_language_code' => 'de',
                'name' => 'Italienisch',
            ),
            193 =>
            array (
                'id' => 1694,
                'language_code' => 'it',
                'display_language_code' => 'fr',
                'name' => 'Italien',
            ),
            194 =>
            array (
                'id' => 1695,
                'language_code' => 'it',
                'display_language_code' => 'ar',
                'name' => 'الإيطالية',
            ),
            195 =>
            array (
                'id' => 1696,
                'language_code' => 'it',
                'display_language_code' => 'bs',
                'name' => 'Italian',
            ),
            196 =>
            array (
                'id' => 1697,
                'language_code' => 'it',
                'display_language_code' => 'bg',
                'name' => 'Италиански',
            ),
            197 =>
            array (
                'id' => 1698,
                'language_code' => 'it',
                'display_language_code' => 'ca',
                'name' => 'Italian',
            ),
            198 =>
            array (
                'id' => 1699,
                'language_code' => 'it',
                'display_language_code' => 'cs',
                'name' => 'Ital',
            ),
            199 =>
            array (
                'id' => 1700,
                'language_code' => 'it',
                'display_language_code' => 'sk',
                'name' => 'Taliančina',
            ),
            200 =>
            array (
                'id' => 1701,
                'language_code' => 'it',
                'display_language_code' => 'cy',
                'name' => 'Italian',
            ),
            201 =>
            array (
                'id' => 1702,
                'language_code' => 'it',
                'display_language_code' => 'da',
                'name' => 'Italian',
            ),
            202 =>
            array (
                'id' => 1703,
                'language_code' => 'it',
                'display_language_code' => 'el',
                'name' => 'Ιταλικά',
            ),
            203 =>
            array (
                'id' => 1704,
                'language_code' => 'it',
                'display_language_code' => 'eo',
                'name' => 'Italian',
            ),
            204 =>
            array (
                'id' => 1705,
                'language_code' => 'it',
                'display_language_code' => 'et',
                'name' => 'Italian',
            ),
            205 =>
            array (
                'id' => 1706,
                'language_code' => 'it',
                'display_language_code' => 'eu',
                'name' => 'Italian',
            ),
            206 =>
            array (
                'id' => 1707,
                'language_code' => 'it',
                'display_language_code' => 'fa',
                'name' => 'Italian',
            ),
            207 =>
            array (
                'id' => 1708,
                'language_code' => 'it',
                'display_language_code' => 'fi',
                'name' => 'Italia',
            ),
            208 =>
            array (
                'id' => 1709,
                'language_code' => 'it',
                'display_language_code' => 'ga',
                'name' => 'Italian',
            ),
            209 =>
            array (
                'id' => 1710,
                'language_code' => 'it',
                'display_language_code' => 'he',
                'name' => 'איטלקית',
            ),
            210 =>
            array (
                'id' => 1711,
                'language_code' => 'it',
                'display_language_code' => 'hi',
                'name' => 'Italian',
            ),
            211 =>
            array (
                'id' => 1712,
                'language_code' => 'it',
                'display_language_code' => 'hr',
                'name' => 'Talijanski',
            ),
            212 =>
            array (
                'id' => 1713,
                'language_code' => 'it',
                'display_language_code' => 'hu',
                'name' => 'Olasz',
            ),
            213 =>
            array (
                'id' => 1714,
                'language_code' => 'it',
                'display_language_code' => 'hy',
                'name' => 'Italian',
            ),
            214 =>
            array (
                'id' => 1715,
                'language_code' => 'it',
                'display_language_code' => 'id',
                'name' => 'Italian',
            ),
            215 =>
            array (
                'id' => 1716,
                'language_code' => 'it',
                'display_language_code' => 'is',
                'name' => 'Italian',
            ),
            216 =>
            array (
                'id' => 1717,
                'language_code' => 'it',
                'display_language_code' => 'it',
                'name' => 'Italiano',
            ),
            217 =>
            array (
                'id' => 1718,
                'language_code' => 'it',
                'display_language_code' => 'ja',
                'name' => 'イタリア語',
            ),
            218 =>
            array (
                'id' => 1719,
                'language_code' => 'it',
                'display_language_code' => 'ko',
                'name' => '이태리어',
            ),
            219 =>
            array (
                'id' => 1720,
                'language_code' => 'it',
                'display_language_code' => 'ku',
                'name' => 'Italian',
            ),
            220 =>
            array (
                'id' => 1721,
                'language_code' => 'it',
                'display_language_code' => 'lv',
                'name' => 'Italian',
            ),
            221 =>
            array (
                'id' => 1722,
                'language_code' => 'it',
                'display_language_code' => 'lt',
                'name' => 'Italian',
            ),
            222 =>
            array (
                'id' => 1723,
                'language_code' => 'it',
                'display_language_code' => 'mk',
                'name' => 'Italian',
            ),
            223 =>
            array (
                'id' => 1724,
                'language_code' => 'it',
                'display_language_code' => 'mt',
                'name' => 'Italian',
            ),
            224 =>
            array (
                'id' => 1725,
                'language_code' => 'it',
                'display_language_code' => 'mn',
                'name' => 'Italian',
            ),
            225 =>
            array (
                'id' => 1726,
                'language_code' => 'it',
                'display_language_code' => 'ne',
                'name' => 'Italian',
            ),
            226 =>
            array (
                'id' => 1727,
                'language_code' => 'it',
                'display_language_code' => 'nl',
                'name' => 'Italiaans',
            ),
            227 =>
            array (
                'id' => 1728,
                'language_code' => 'it',
                'display_language_code' => 'no',
                'name' => 'Italiensk',
            ),
            228 =>
            array (
                'id' => 1729,
                'language_code' => 'it',
                'display_language_code' => 'pa',
                'name' => 'Italian',
            ),
            229 =>
            array (
                'id' => 1730,
                'language_code' => 'it',
                'display_language_code' => 'pl',
                'name' => 'Włoski',
            ),
            230 =>
            array (
                'id' => 1731,
                'language_code' => 'it',
                'display_language_code' => 'pt-pt',
                'name' => 'Italiano',
            ),
            231 =>
            array (
                'id' => 1732,
                'language_code' => 'it',
                'display_language_code' => 'pt-br',
                'name' => 'Italiano',
            ),
            232 =>
            array (
                'id' => 1733,
                'language_code' => 'it',
                'display_language_code' => 'qu',
                'name' => 'Italian',
            ),
            233 =>
            array (
                'id' => 1734,
                'language_code' => 'it',
                'display_language_code' => 'ro',
                'name' => 'Italiană',
            ),
            234 =>
            array (
                'id' => 1735,
                'language_code' => 'it',
                'display_language_code' => 'ru',
                'name' => 'Итальянский',
            ),
            235 =>
            array (
                'id' => 1736,
                'language_code' => 'it',
                'display_language_code' => 'sl',
                'name' => 'Italijanski',
            ),
            236 =>
            array (
                'id' => 1737,
                'language_code' => 'it',
                'display_language_code' => 'so',
                'name' => 'Italian',
            ),
            237 =>
            array (
                'id' => 1738,
                'language_code' => 'it',
                'display_language_code' => 'sq',
                'name' => 'Italian',
            ),
            238 =>
            array (
                'id' => 1739,
                'language_code' => 'it',
                'display_language_code' => 'sr',
                'name' => 'италијански',
            ),
            239 =>
            array (
                'id' => 1740,
                'language_code' => 'it',
                'display_language_code' => 'sv',
                'name' => 'Italienska',
            ),
            240 =>
            array (
                'id' => 1741,
                'language_code' => 'it',
                'display_language_code' => 'ta',
                'name' => 'Italian',
            ),
            241 =>
            array (
                'id' => 1742,
                'language_code' => 'it',
                'display_language_code' => 'th',
                'name' => 'อิตาลี',
            ),
            242 =>
            array (
                'id' => 1743,
                'language_code' => 'it',
                'display_language_code' => 'tr',
                'name' => 'İtalyanca',
            ),
            243 =>
            array (
                'id' => 1744,
                'language_code' => 'it',
                'display_language_code' => 'uk',
                'name' => 'Italian',
            ),
            244 =>
            array (
                'id' => 1745,
                'language_code' => 'it',
                'display_language_code' => 'ur',
                'name' => 'Italian',
            ),
            245 =>
            array (
                'id' => 1746,
                'language_code' => 'it',
                'display_language_code' => 'uz',
                'name' => 'Italian',
            ),
            246 =>
            array (
                'id' => 1747,
                'language_code' => 'it',
                'display_language_code' => 'vi',
                'name' => 'Italian',
            ),
            247 =>
            array (
                'id' => 1748,
                'language_code' => 'it',
                'display_language_code' => 'yi',
                'name' => 'Italian',
            ),
            248 =>
            array (
                'id' => 1749,
                'language_code' => 'it',
                'display_language_code' => 'zh-hans',
                'name' => '意大利语',
            ),
            249 =>
            array (
                'id' => 1750,
                'language_code' => 'it',
                'display_language_code' => 'zu',
                'name' => 'Italian',
            ),
            250 =>
            array (
                'id' => 1751,
                'language_code' => 'it',
                'display_language_code' => 'zh-hant',
                'name' => '義大利語',
            ),
            251 =>
            array (
                'id' => 1752,
                'language_code' => 'it',
                'display_language_code' => 'ms',
                'name' => 'Italian',
            ),
            252 =>
            array (
                'id' => 1753,
                'language_code' => 'it',
                'display_language_code' => 'gl',
                'name' => 'Italian',
            ),
            253 =>
            array (
                'id' => 1754,
                'language_code' => 'it',
                'display_language_code' => 'bn',
                'name' => 'Italian',
            ),
            254 =>
            array (
                'id' => 1755,
                'language_code' => 'it',
                'display_language_code' => 'az',
                'name' => 'Italian',
            ),
            255 =>
            array (
                'id' => 1756,
                'language_code' => 'ja',
                'display_language_code' => 'en',
                'name' => 'Japanese',
            ),
            256 =>
            array (
                'id' => 1757,
                'language_code' => 'ja',
                'display_language_code' => 'es',
                'name' => 'Japonés',
            ),
            257 =>
            array (
                'id' => 1758,
                'language_code' => 'ja',
                'display_language_code' => 'de',
                'name' => 'Japanisch',
            ),
            258 =>
            array (
                'id' => 1759,
                'language_code' => 'ja',
                'display_language_code' => 'fr',
                'name' => 'Japonais',
            ),
            259 =>
            array (
                'id' => 1760,
                'language_code' => 'ja',
                'display_language_code' => 'ar',
                'name' => 'اليابانية',
            ),
            260 =>
            array (
                'id' => 1761,
                'language_code' => 'ja',
                'display_language_code' => 'bs',
                'name' => 'Japanese',
            ),
            261 =>
            array (
                'id' => 1762,
                'language_code' => 'ja',
                'display_language_code' => 'bg',
                'name' => 'Японски',
            ),
            262 =>
            array (
                'id' => 1763,
                'language_code' => 'ja',
                'display_language_code' => 'ca',
                'name' => 'Japanese',
            ),
            263 =>
            array (
                'id' => 1764,
                'language_code' => 'ja',
                'display_language_code' => 'cs',
                'name' => 'Japonský',
            ),
            264 =>
            array (
                'id' => 1765,
                'language_code' => 'ja',
                'display_language_code' => 'sk',
                'name' => 'Japonština',
            ),
            265 =>
            array (
                'id' => 1766,
                'language_code' => 'ja',
                'display_language_code' => 'cy',
                'name' => 'Japanese',
            ),
            266 =>
            array (
                'id' => 1767,
                'language_code' => 'ja',
                'display_language_code' => 'da',
                'name' => 'Japanese',
            ),
            267 =>
            array (
                'id' => 1768,
                'language_code' => 'ja',
                'display_language_code' => 'el',
                'name' => 'Ιαπωνικά',
            ),
            268 =>
            array (
                'id' => 1769,
                'language_code' => 'ja',
                'display_language_code' => 'eo',
                'name' => 'Japanese',
            ),
            269 =>
            array (
                'id' => 1770,
                'language_code' => 'ja',
                'display_language_code' => 'et',
                'name' => 'Japanese',
            ),
            270 =>
            array (
                'id' => 1771,
                'language_code' => 'ja',
                'display_language_code' => 'eu',
                'name' => 'Japanese',
            ),
            271 =>
            array (
                'id' => 1772,
                'language_code' => 'ja',
                'display_language_code' => 'fa',
                'name' => 'Japanese',
            ),
            272 =>
            array (
                'id' => 1773,
                'language_code' => 'ja',
                'display_language_code' => 'fi',
                'name' => 'Japani',
            ),
            273 =>
            array (
                'id' => 1774,
                'language_code' => 'ja',
                'display_language_code' => 'ga',
                'name' => 'Japanese',
            ),
            274 =>
            array (
                'id' => 1775,
                'language_code' => 'ja',
                'display_language_code' => 'he',
                'name' => 'יפנית',
            ),
            275 =>
            array (
                'id' => 1776,
                'language_code' => 'ja',
                'display_language_code' => 'hi',
                'name' => 'Japanese',
            ),
            276 =>
            array (
                'id' => 1777,
                'language_code' => 'ja',
                'display_language_code' => 'hr',
                'name' => 'Japanski',
            ),
            277 =>
            array (
                'id' => 1778,
                'language_code' => 'ja',
                'display_language_code' => 'hu',
                'name' => 'Japán',
            ),
            278 =>
            array (
                'id' => 1779,
                'language_code' => 'ja',
                'display_language_code' => 'hy',
                'name' => 'Japanese',
            ),
            279 =>
            array (
                'id' => 1780,
                'language_code' => 'ja',
                'display_language_code' => 'id',
                'name' => 'Japanese',
            ),
            280 =>
            array (
                'id' => 1781,
                'language_code' => 'ja',
                'display_language_code' => 'is',
                'name' => 'Japanese',
            ),
            281 =>
            array (
                'id' => 1782,
                'language_code' => 'ja',
                'display_language_code' => 'it',
                'name' => 'Giapponese',
            ),
            282 =>
            array (
                'id' => 1783,
                'language_code' => 'ja',
                'display_language_code' => 'ja',
                'name' => '日本語',
            ),
            283 =>
            array (
                'id' => 1784,
                'language_code' => 'ja',
                'display_language_code' => 'ko',
                'name' => '일어',
            ),
            284 =>
            array (
                'id' => 1785,
                'language_code' => 'ja',
                'display_language_code' => 'ku',
                'name' => 'Japanese',
            ),
            285 =>
            array (
                'id' => 1786,
                'language_code' => 'ja',
                'display_language_code' => 'lv',
                'name' => 'Japanese',
            ),
            286 =>
            array (
                'id' => 1787,
                'language_code' => 'ja',
                'display_language_code' => 'lt',
                'name' => 'Japanese',
            ),
            287 =>
            array (
                'id' => 1788,
                'language_code' => 'ja',
                'display_language_code' => 'mk',
                'name' => 'Japanese',
            ),
            288 =>
            array (
                'id' => 1789,
                'language_code' => 'ja',
                'display_language_code' => 'mt',
                'name' => 'Japanese',
            ),
            289 =>
            array (
                'id' => 1790,
                'language_code' => 'ja',
                'display_language_code' => 'mn',
                'name' => 'Japanese',
            ),
            290 =>
            array (
                'id' => 1791,
                'language_code' => 'ja',
                'display_language_code' => 'ne',
                'name' => 'Japanese',
            ),
            291 =>
            array (
                'id' => 1792,
                'language_code' => 'ja',
                'display_language_code' => 'nl',
                'name' => 'Japans',
            ),
            292 =>
            array (
                'id' => 1793,
                'language_code' => 'ja',
                'display_language_code' => 'no',
                'name' => 'Japansk',
            ),
            293 =>
            array (
                'id' => 1794,
                'language_code' => 'ja',
                'display_language_code' => 'pa',
                'name' => 'Japanese',
            ),
            294 =>
            array (
                'id' => 1795,
                'language_code' => 'ja',
                'display_language_code' => 'pl',
                'name' => 'Japoński',
            ),
            295 =>
            array (
                'id' => 1796,
                'language_code' => 'ja',
                'display_language_code' => 'pt-pt',
                'name' => 'Japonês',
            ),
            296 =>
            array (
                'id' => 1797,
                'language_code' => 'ja',
                'display_language_code' => 'pt-br',
                'name' => 'Japonês',
            ),
            297 =>
            array (
                'id' => 1798,
                'language_code' => 'ja',
                'display_language_code' => 'qu',
                'name' => 'Japanese',
            ),
            298 =>
            array (
                'id' => 1799,
                'language_code' => 'ja',
                'display_language_code' => 'ro',
                'name' => 'Japoneză',
            ),
            299 =>
            array (
                'id' => 1800,
                'language_code' => 'ja',
                'display_language_code' => 'ru',
                'name' => 'Японский',
            ),
            300 =>
            array (
                'id' => 1801,
                'language_code' => 'ja',
                'display_language_code' => 'sl',
                'name' => 'Japonski',
            ),
            301 =>
            array (
                'id' => 1802,
                'language_code' => 'ja',
                'display_language_code' => 'so',
                'name' => 'Japanese',
            ),
            302 =>
            array (
                'id' => 1803,
                'language_code' => 'ja',
                'display_language_code' => 'sq',
                'name' => 'Japanese',
            ),
            303 =>
            array (
                'id' => 1804,
                'language_code' => 'ja',
                'display_language_code' => 'sr',
                'name' => 'јапански',
            ),
            304 =>
            array (
                'id' => 1805,
                'language_code' => 'ja',
                'display_language_code' => 'sv',
                'name' => 'Japanska',
            ),
            305 =>
            array (
                'id' => 1806,
                'language_code' => 'ja',
                'display_language_code' => 'ta',
                'name' => 'Japanese',
            ),
            306 =>
            array (
                'id' => 1807,
                'language_code' => 'ja',
                'display_language_code' => 'th',
                'name' => 'ญี่ปุ่น',
            ),
            307 =>
            array (
                'id' => 1808,
                'language_code' => 'ja',
                'display_language_code' => 'tr',
                'name' => 'Japonca',
            ),
            308 =>
            array (
                'id' => 1809,
                'language_code' => 'ja',
                'display_language_code' => 'uk',
                'name' => 'Japanese',
            ),
            309 =>
            array (
                'id' => 1810,
                'language_code' => 'ja',
                'display_language_code' => 'ur',
                'name' => 'Japanese',
            ),
            310 =>
            array (
                'id' => 1811,
                'language_code' => 'ja',
                'display_language_code' => 'uz',
                'name' => 'Japanese',
            ),
            311 =>
            array (
                'id' => 1812,
                'language_code' => 'ja',
                'display_language_code' => 'vi',
                'name' => 'Japanese',
            ),
            312 =>
            array (
                'id' => 1813,
                'language_code' => 'ja',
                'display_language_code' => 'yi',
                'name' => 'Japanese',
            ),
            313 =>
            array (
                'id' => 1814,
                'language_code' => 'ja',
                'display_language_code' => 'zh-hans',
                'name' => '日语',
            ),
            314 =>
            array (
                'id' => 1815,
                'language_code' => 'ja',
                'display_language_code' => 'zu',
                'name' => 'Japanese',
            ),
            315 =>
            array (
                'id' => 1816,
                'language_code' => 'ja',
                'display_language_code' => 'zh-hant',
                'name' => '日語',
            ),
            316 =>
            array (
                'id' => 1817,
                'language_code' => 'ja',
                'display_language_code' => 'ms',
                'name' => 'Japanese',
            ),
            317 =>
            array (
                'id' => 1818,
                'language_code' => 'ja',
                'display_language_code' => 'gl',
                'name' => 'Japanese',
            ),
            318 =>
            array (
                'id' => 1819,
                'language_code' => 'ja',
                'display_language_code' => 'bn',
                'name' => 'Japanese',
            ),
            319 =>
            array (
                'id' => 1820,
                'language_code' => 'ja',
                'display_language_code' => 'az',
                'name' => 'Japanese',
            ),
            320 =>
            array (
                'id' => 1821,
                'language_code' => 'ko',
                'display_language_code' => 'en',
                'name' => 'Korean',
            ),
            321 =>
            array (
                'id' => 1822,
                'language_code' => 'ko',
                'display_language_code' => 'es',
                'name' => 'Coreano',
            ),
            322 =>
            array (
                'id' => 1823,
                'language_code' => 'ko',
                'display_language_code' => 'de',
                'name' => 'Koreanisch',
            ),
            323 =>
            array (
                'id' => 1824,
                'language_code' => 'ko',
                'display_language_code' => 'fr',
                'name' => 'Coréen',
            ),
            324 =>
            array (
                'id' => 1825,
                'language_code' => 'ko',
                'display_language_code' => 'ar',
                'name' => 'الكورية',
            ),
            325 =>
            array (
                'id' => 1826,
                'language_code' => 'ko',
                'display_language_code' => 'bs',
                'name' => 'Korean',
            ),
            326 =>
            array (
                'id' => 1827,
                'language_code' => 'ko',
                'display_language_code' => 'bg',
                'name' => 'Корейски',
            ),
            327 =>
            array (
                'id' => 1828,
                'language_code' => 'ko',
                'display_language_code' => 'ca',
                'name' => 'Korean',
            ),
            328 =>
            array (
                'id' => 1829,
                'language_code' => 'ko',
                'display_language_code' => 'cs',
                'name' => 'Korejský',
            ),
            329 =>
            array (
                'id' => 1830,
                'language_code' => 'ko',
                'display_language_code' => 'sk',
                'name' => 'Kórejčina',
            ),
            330 =>
            array (
                'id' => 1831,
                'language_code' => 'ko',
                'display_language_code' => 'cy',
                'name' => 'Korean',
            ),
            331 =>
            array (
                'id' => 1832,
                'language_code' => 'ko',
                'display_language_code' => 'da',
                'name' => 'Korean',
            ),
            332 =>
            array (
                'id' => 1833,
                'language_code' => 'ko',
                'display_language_code' => 'el',
                'name' => 'Κορεάτικα',
            ),
            333 =>
            array (
                'id' => 1834,
                'language_code' => 'ko',
                'display_language_code' => 'eo',
                'name' => 'Korean',
            ),
            334 =>
            array (
                'id' => 1835,
                'language_code' => 'ko',
                'display_language_code' => 'et',
                'name' => 'Korean',
            ),
            335 =>
            array (
                'id' => 1836,
                'language_code' => 'ko',
                'display_language_code' => 'eu',
                'name' => 'Korean',
            ),
            336 =>
            array (
                'id' => 1837,
                'language_code' => 'ko',
                'display_language_code' => 'fa',
                'name' => 'Korean',
            ),
            337 =>
            array (
                'id' => 1838,
                'language_code' => 'ko',
                'display_language_code' => 'fi',
                'name' => 'Korea',
            ),
            338 =>
            array (
                'id' => 1839,
                'language_code' => 'ko',
                'display_language_code' => 'ga',
                'name' => 'Korean',
            ),
            339 =>
            array (
                'id' => 1840,
                'language_code' => 'ko',
                'display_language_code' => 'he',
                'name' => 'קוראנית',
            ),
            340 =>
            array (
                'id' => 1841,
                'language_code' => 'ko',
                'display_language_code' => 'hi',
                'name' => 'Korean',
            ),
            341 =>
            array (
                'id' => 1842,
                'language_code' => 'ko',
                'display_language_code' => 'hr',
                'name' => 'Korejski',
            ),
            342 =>
            array (
                'id' => 1843,
                'language_code' => 'ko',
                'display_language_code' => 'hu',
                'name' => 'Koreai',
            ),
            343 =>
            array (
                'id' => 1844,
                'language_code' => 'ko',
                'display_language_code' => 'hy',
                'name' => 'Korean',
            ),
            344 =>
            array (
                'id' => 1845,
                'language_code' => 'ko',
                'display_language_code' => 'id',
                'name' => 'Korean',
            ),
            345 =>
            array (
                'id' => 1846,
                'language_code' => 'ko',
                'display_language_code' => 'is',
                'name' => 'Korean',
            ),
            346 =>
            array (
                'id' => 1847,
                'language_code' => 'ko',
                'display_language_code' => 'it',
                'name' => 'Coreano',
            ),
            347 =>
            array (
                'id' => 1848,
                'language_code' => 'ko',
                'display_language_code' => 'ja',
                'name' => '韓国語',
            ),
            348 =>
            array (
                'id' => 1849,
                'language_code' => 'ko',
                'display_language_code' => 'ko',
                'name' => '한국어',
            ),
            349 =>
            array (
                'id' => 1850,
                'language_code' => 'ko',
                'display_language_code' => 'ku',
                'name' => 'Korean',
            ),
            350 =>
            array (
                'id' => 1851,
                'language_code' => 'ko',
                'display_language_code' => 'lv',
                'name' => 'Korean',
            ),
            351 =>
            array (
                'id' => 1852,
                'language_code' => 'ko',
                'display_language_code' => 'lt',
                'name' => 'Korean',
            ),
            352 =>
            array (
                'id' => 1853,
                'language_code' => 'ko',
                'display_language_code' => 'mk',
                'name' => 'Korean',
            ),
            353 =>
            array (
                'id' => 1854,
                'language_code' => 'ko',
                'display_language_code' => 'mt',
                'name' => 'Korean',
            ),
            354 =>
            array (
                'id' => 1855,
                'language_code' => 'ko',
                'display_language_code' => 'mn',
                'name' => 'Korean',
            ),
            355 =>
            array (
                'id' => 1856,
                'language_code' => 'ko',
                'display_language_code' => 'ne',
                'name' => 'Korean',
            ),
            356 =>
            array (
                'id' => 1857,
                'language_code' => 'ko',
                'display_language_code' => 'nl',
                'name' => 'Koreaans',
            ),
            357 =>
            array (
                'id' => 1858,
                'language_code' => 'ko',
                'display_language_code' => 'no',
                'name' => 'Koreanske',
            ),
            358 =>
            array (
                'id' => 1859,
                'language_code' => 'ko',
                'display_language_code' => 'pa',
                'name' => 'Korean',
            ),
            359 =>
            array (
                'id' => 1860,
                'language_code' => 'ko',
                'display_language_code' => 'pl',
                'name' => 'Koreański',
            ),
            360 =>
            array (
                'id' => 1861,
                'language_code' => 'ko',
                'display_language_code' => 'pt-pt',
                'name' => 'Coreano',
            ),
            361 =>
            array (
                'id' => 1862,
                'language_code' => 'ko',
                'display_language_code' => 'pt-br',
                'name' => 'Coreano',
            ),
            362 =>
            array (
                'id' => 1863,
                'language_code' => 'ko',
                'display_language_code' => 'qu',
                'name' => 'Korean',
            ),
            363 =>
            array (
                'id' => 1864,
                'language_code' => 'ko',
                'display_language_code' => 'ro',
                'name' => 'Coreană',
            ),
            364 =>
            array (
                'id' => 1865,
                'language_code' => 'ko',
                'display_language_code' => 'ru',
                'name' => 'Корейский',
            ),
            365 =>
            array (
                'id' => 1866,
                'language_code' => 'ko',
                'display_language_code' => 'sl',
                'name' => 'Korejski',
            ),
            366 =>
            array (
                'id' => 1867,
                'language_code' => 'ko',
                'display_language_code' => 'so',
                'name' => 'Korean',
            ),
            367 =>
            array (
                'id' => 1868,
                'language_code' => 'ko',
                'display_language_code' => 'sq',
                'name' => 'Korean',
            ),
            368 =>
            array (
                'id' => 1869,
                'language_code' => 'ko',
                'display_language_code' => 'sr',
                'name' => 'корејски',
            ),
            369 =>
            array (
                'id' => 1870,
                'language_code' => 'ko',
                'display_language_code' => 'sv',
                'name' => 'Koreanska',
            ),
            370 =>
            array (
                'id' => 1871,
                'language_code' => 'ko',
                'display_language_code' => 'ta',
                'name' => 'Korean',
            ),
            371 =>
            array (
                'id' => 1872,
                'language_code' => 'ko',
                'display_language_code' => 'th',
                'name' => 'เกาหลี',
            ),
            372 =>
            array (
                'id' => 1873,
                'language_code' => 'ko',
                'display_language_code' => 'tr',
                'name' => 'Kore dili',
            ),
            373 =>
            array (
                'id' => 1874,
                'language_code' => 'ko',
                'display_language_code' => 'uk',
                'name' => 'Korean',
            ),
            374 =>
            array (
                'id' => 1875,
                'language_code' => 'ko',
                'display_language_code' => 'ur',
                'name' => 'Korean',
            ),
            375 =>
            array (
                'id' => 1876,
                'language_code' => 'ko',
                'display_language_code' => 'uz',
                'name' => 'Korean',
            ),
            376 =>
            array (
                'id' => 1877,
                'language_code' => 'ko',
                'display_language_code' => 'vi',
                'name' => 'Korean',
            ),
            377 =>
            array (
                'id' => 1878,
                'language_code' => 'ko',
                'display_language_code' => 'yi',
                'name' => 'Korean',
            ),
            378 =>
            array (
                'id' => 1879,
                'language_code' => 'ko',
                'display_language_code' => 'zh-hans',
                'name' => '韩语',
            ),
            379 =>
            array (
                'id' => 1880,
                'language_code' => 'ko',
                'display_language_code' => 'zu',
                'name' => 'Korean',
            ),
            380 =>
            array (
                'id' => 1881,
                'language_code' => 'ko',
                'display_language_code' => 'zh-hant',
                'name' => '韓語',
            ),
            381 =>
            array (
                'id' => 1882,
                'language_code' => 'ko',
                'display_language_code' => 'ms',
                'name' => 'Korean',
            ),
            382 =>
            array (
                'id' => 1883,
                'language_code' => 'ko',
                'display_language_code' => 'gl',
                'name' => 'Korean',
            ),
            383 =>
            array (
                'id' => 1884,
                'language_code' => 'ko',
                'display_language_code' => 'bn',
                'name' => 'Korean',
            ),
            384 =>
            array (
                'id' => 1885,
                'language_code' => 'ko',
                'display_language_code' => 'az',
                'name' => 'Korean',
            ),
            385 =>
            array (
                'id' => 1886,
                'language_code' => 'ku',
                'display_language_code' => 'en',
                'name' => 'Kurdish',
            ),
            386 =>
            array (
                'id' => 1887,
                'language_code' => 'ku',
                'display_language_code' => 'es',
                'name' => 'Kurdo',
            ),
            387 =>
            array (
                'id' => 1888,
                'language_code' => 'ku',
                'display_language_code' => 'de',
                'name' => 'Kurdisch',
            ),
            388 =>
            array (
                'id' => 1889,
                'language_code' => 'ku',
                'display_language_code' => 'fr',
                'name' => 'Kurde',
            ),
            389 =>
            array (
                'id' => 1890,
                'language_code' => 'ku',
                'display_language_code' => 'ar',
                'name' => 'الكردية',
            ),
            390 =>
            array (
                'id' => 1891,
                'language_code' => 'ku',
                'display_language_code' => 'bs',
                'name' => 'Kurdish',
            ),
            391 =>
            array (
                'id' => 1892,
                'language_code' => 'ku',
                'display_language_code' => 'bg',
                'name' => 'Кюрдски',
            ),
            392 =>
            array (
                'id' => 1893,
                'language_code' => 'ku',
                'display_language_code' => 'ca',
                'name' => 'Kurdish',
            ),
            393 =>
            array (
                'id' => 1894,
                'language_code' => 'ku',
                'display_language_code' => 'cs',
                'name' => 'Kurdský',
            ),
            394 =>
            array (
                'id' => 1895,
                'language_code' => 'ku',
                'display_language_code' => 'sk',
                'name' => 'Kurdština',
            ),
            395 =>
            array (
                'id' => 1896,
                'language_code' => 'ku',
                'display_language_code' => 'cy',
                'name' => 'Kurdish',
            ),
            396 =>
            array (
                'id' => 1897,
                'language_code' => 'ku',
                'display_language_code' => 'da',
                'name' => 'Kurdish',
            ),
            397 =>
            array (
                'id' => 1898,
                'language_code' => 'ku',
                'display_language_code' => 'el',
                'name' => 'Κουρδικά',
            ),
            398 =>
            array (
                'id' => 1899,
                'language_code' => 'ku',
                'display_language_code' => 'eo',
                'name' => 'Kurdish',
            ),
            399 =>
            array (
                'id' => 1900,
                'language_code' => 'ku',
                'display_language_code' => 'et',
                'name' => 'Kurdish',
            ),
            400 =>
            array (
                'id' => 1901,
                'language_code' => 'ku',
                'display_language_code' => 'eu',
                'name' => 'Kurdish',
            ),
            401 =>
            array (
                'id' => 1902,
                'language_code' => 'ku',
                'display_language_code' => 'fa',
                'name' => 'Kurdish',
            ),
            402 =>
            array (
                'id' => 1903,
                'language_code' => 'ku',
                'display_language_code' => 'fi',
                'name' => 'Kurdi',
            ),
            403 =>
            array (
                'id' => 1904,
                'language_code' => 'ku',
                'display_language_code' => 'ga',
                'name' => 'Kurdish',
            ),
            404 =>
            array (
                'id' => 1905,
                'language_code' => 'ku',
                'display_language_code' => 'he',
                'name' => 'כורדית',
            ),
            405 =>
            array (
                'id' => 1906,
                'language_code' => 'ku',
                'display_language_code' => 'hi',
                'name' => 'Kurdish',
            ),
            406 =>
            array (
                'id' => 1907,
                'language_code' => 'ku',
                'display_language_code' => 'hr',
                'name' => 'Kurdski',
            ),
            407 =>
            array (
                'id' => 1908,
                'language_code' => 'ku',
                'display_language_code' => 'hu',
                'name' => 'Kurd',
            ),
            408 =>
            array (
                'id' => 1909,
                'language_code' => 'ku',
                'display_language_code' => 'hy',
                'name' => 'Kurdish',
            ),
            409 =>
            array (
                'id' => 1910,
                'language_code' => 'ku',
                'display_language_code' => 'id',
                'name' => 'Kurdish',
            ),
            410 =>
            array (
                'id' => 1911,
                'language_code' => 'ku',
                'display_language_code' => 'is',
                'name' => 'Kurdish',
            ),
            411 =>
            array (
                'id' => 1912,
                'language_code' => 'ku',
                'display_language_code' => 'it',
                'name' => 'Curdo',
            ),
            412 =>
            array (
                'id' => 1913,
                'language_code' => 'ku',
                'display_language_code' => 'ja',
                'name' => 'クルド語',
            ),
            413 =>
            array (
                'id' => 1914,
                'language_code' => 'ku',
                'display_language_code' => 'ko',
                'name' => '쿠르드어',
            ),
            414 =>
            array (
                'id' => 1915,
                'language_code' => 'ku',
                'display_language_code' => 'ku',
                'name' => 'Kurdish',
            ),
            415 =>
            array (
                'id' => 1916,
                'language_code' => 'ku',
                'display_language_code' => 'lv',
                'name' => 'Kurdish',
            ),
            416 =>
            array (
                'id' => 1917,
                'language_code' => 'ku',
                'display_language_code' => 'lt',
                'name' => 'Kurdish',
            ),
            417 =>
            array (
                'id' => 1918,
                'language_code' => 'ku',
                'display_language_code' => 'mk',
                'name' => 'Kurdish',
            ),
            418 =>
            array (
                'id' => 1919,
                'language_code' => 'ku',
                'display_language_code' => 'mt',
                'name' => 'Kurdish',
            ),
            419 =>
            array (
                'id' => 1920,
                'language_code' => 'ku',
                'display_language_code' => 'mn',
                'name' => 'Kurdish',
            ),
            420 =>
            array (
                'id' => 1921,
                'language_code' => 'ku',
                'display_language_code' => 'ne',
                'name' => 'Kurdish',
            ),
            421 =>
            array (
                'id' => 1922,
                'language_code' => 'ku',
                'display_language_code' => 'nl',
                'name' => 'Koerdish',
            ),
            422 =>
            array (
                'id' => 1923,
                'language_code' => 'ku',
                'display_language_code' => 'no',
                'name' => 'Kurdisk',
            ),
            423 =>
            array (
                'id' => 1924,
                'language_code' => 'ku',
                'display_language_code' => 'pa',
                'name' => 'Kurdish',
            ),
            424 =>
            array (
                'id' => 1925,
                'language_code' => 'ku',
                'display_language_code' => 'pl',
                'name' => 'Kurdyjski',
            ),
            425 =>
            array (
                'id' => 1926,
                'language_code' => 'ku',
                'display_language_code' => 'pt-pt',
                'name' => 'Curdo',
            ),
            426 =>
            array (
                'id' => 1927,
                'language_code' => 'ku',
                'display_language_code' => 'pt-br',
                'name' => 'Curdo',
            ),
            427 =>
            array (
                'id' => 1928,
                'language_code' => 'ku',
                'display_language_code' => 'qu',
                'name' => 'Kurdish',
            ),
            428 =>
            array (
                'id' => 1929,
                'language_code' => 'ku',
                'display_language_code' => 'ro',
                'name' => 'Kurdă',
            ),
            429 =>
            array (
                'id' => 1930,
                'language_code' => 'ku',
                'display_language_code' => 'ru',
                'name' => 'Курдский',
            ),
            430 =>
            array (
                'id' => 1931,
                'language_code' => 'ku',
                'display_language_code' => 'sl',
                'name' => 'Kurdščina',
            ),
            431 =>
            array (
                'id' => 1932,
                'language_code' => 'ku',
                'display_language_code' => 'so',
                'name' => 'Kurdish',
            ),
            432 =>
            array (
                'id' => 1933,
                'language_code' => 'ku',
                'display_language_code' => 'sq',
                'name' => 'Kurdish',
            ),
            433 =>
            array (
                'id' => 1934,
                'language_code' => 'ku',
                'display_language_code' => 'sr',
                'name' => 'курдски',
            ),
            434 =>
            array (
                'id' => 1935,
                'language_code' => 'ku',
                'display_language_code' => 'sv',
                'name' => 'Kurdiska',
            ),
            435 =>
            array (
                'id' => 1936,
                'language_code' => 'ku',
                'display_language_code' => 'ta',
                'name' => 'Kurdish',
            ),
            436 =>
            array (
                'id' => 1937,
                'language_code' => 'ku',
                'display_language_code' => 'th',
                'name' => 'เคอร์ดิช',
            ),
            437 =>
            array (
                'id' => 1938,
                'language_code' => 'ku',
                'display_language_code' => 'tr',
                'name' => 'Kürtçe',
            ),
            438 =>
            array (
                'id' => 1939,
                'language_code' => 'ku',
                'display_language_code' => 'uk',
                'name' => 'Kurdish',
            ),
            439 =>
            array (
                'id' => 1940,
                'language_code' => 'ku',
                'display_language_code' => 'ur',
                'name' => 'Kurdish',
            ),
            440 =>
            array (
                'id' => 1941,
                'language_code' => 'ku',
                'display_language_code' => 'uz',
                'name' => 'Kurdish',
            ),
            441 =>
            array (
                'id' => 1942,
                'language_code' => 'ku',
                'display_language_code' => 'vi',
                'name' => 'Kurdish',
            ),
            442 =>
            array (
                'id' => 1943,
                'language_code' => 'ku',
                'display_language_code' => 'yi',
                'name' => 'Kurdish',
            ),
            443 =>
            array (
                'id' => 1944,
                'language_code' => 'ku',
                'display_language_code' => 'zh-hans',
                'name' => '库尔德语',
            ),
            444 =>
            array (
                'id' => 1945,
                'language_code' => 'ku',
                'display_language_code' => 'zu',
                'name' => 'Kurdish',
            ),
            445 =>
            array (
                'id' => 1946,
                'language_code' => 'ku',
                'display_language_code' => 'zh-hant',
                'name' => '庫爾德語',
            ),
            446 =>
            array (
                'id' => 1947,
                'language_code' => 'ku',
                'display_language_code' => 'ms',
                'name' => 'Kurdish',
            ),
            447 =>
            array (
                'id' => 1948,
                'language_code' => 'ku',
                'display_language_code' => 'gl',
                'name' => 'Kurdish',
            ),
            448 =>
            array (
                'id' => 1949,
                'language_code' => 'ku',
                'display_language_code' => 'bn',
                'name' => 'Kurdish',
            ),
            449 =>
            array (
                'id' => 1950,
                'language_code' => 'ku',
                'display_language_code' => 'az',
                'name' => 'Kurdish',
            ),
            450 =>
            array (
                'id' => 1951,
                'language_code' => 'lv',
                'display_language_code' => 'en',
                'name' => 'Latvian',
            ),
            451 =>
            array (
                'id' => 1952,
                'language_code' => 'lv',
                'display_language_code' => 'es',
                'name' => 'Letón',
            ),
            452 =>
            array (
                'id' => 1953,
                'language_code' => 'lv',
                'display_language_code' => 'de',
                'name' => 'Lettisch',
            ),
            453 =>
            array (
                'id' => 1954,
                'language_code' => 'lv',
                'display_language_code' => 'fr',
                'name' => 'Letton',
            ),
            454 =>
            array (
                'id' => 1955,
                'language_code' => 'lv',
                'display_language_code' => 'ar',
                'name' => 'اللاتفية',
            ),
            455 =>
            array (
                'id' => 1956,
                'language_code' => 'lv',
                'display_language_code' => 'bs',
                'name' => 'Latvian',
            ),
            456 =>
            array (
                'id' => 1957,
                'language_code' => 'lv',
                'display_language_code' => 'bg',
                'name' => 'Латвийски',
            ),
            457 =>
            array (
                'id' => 1958,
                'language_code' => 'lv',
                'display_language_code' => 'ca',
                'name' => 'Latvian',
            ),
            458 =>
            array (
                'id' => 1959,
                'language_code' => 'lv',
                'display_language_code' => 'cs',
                'name' => 'Lotyština',
            ),
            459 =>
            array (
                'id' => 1960,
                'language_code' => 'lv',
                'display_language_code' => 'sk',
                'name' => 'Lotyština',
            ),
            460 =>
            array (
                'id' => 1961,
                'language_code' => 'lv',
                'display_language_code' => 'cy',
                'name' => 'Latvian',
            ),
            461 =>
            array (
                'id' => 1962,
                'language_code' => 'lv',
                'display_language_code' => 'da',
                'name' => 'Latvian',
            ),
            462 =>
            array (
                'id' => 1963,
                'language_code' => 'lv',
                'display_language_code' => 'el',
                'name' => 'Λετονικά',
            ),
            463 =>
            array (
                'id' => 1964,
                'language_code' => 'lv',
                'display_language_code' => 'eo',
                'name' => 'Latvian',
            ),
            464 =>
            array (
                'id' => 1965,
                'language_code' => 'lv',
                'display_language_code' => 'et',
                'name' => 'Latvian',
            ),
            465 =>
            array (
                'id' => 1966,
                'language_code' => 'lv',
                'display_language_code' => 'eu',
                'name' => 'Latvian',
            ),
            466 =>
            array (
                'id' => 1967,
                'language_code' => 'lv',
                'display_language_code' => 'fa',
                'name' => 'Latvian',
            ),
            467 =>
            array (
                'id' => 1968,
                'language_code' => 'lv',
                'display_language_code' => 'fi',
                'name' => 'Latvia',
            ),
            468 =>
            array (
                'id' => 1969,
                'language_code' => 'lv',
                'display_language_code' => 'ga',
                'name' => 'Latvian',
            ),
            469 =>
            array (
                'id' => 1970,
                'language_code' => 'lv',
                'display_language_code' => 'he',
                'name' => 'לטבית',
            ),
            470 =>
            array (
                'id' => 1971,
                'language_code' => 'lv',
                'display_language_code' => 'hi',
                'name' => 'Latvian',
            ),
            471 =>
            array (
                'id' => 1972,
                'language_code' => 'lv',
                'display_language_code' => 'hr',
                'name' => 'Latvijski',
            ),
            472 =>
            array (
                'id' => 1973,
                'language_code' => 'lv',
                'display_language_code' => 'hu',
                'name' => 'Lett',
            ),
            473 =>
            array (
                'id' => 1974,
                'language_code' => 'lv',
                'display_language_code' => 'hy',
                'name' => 'Latvian',
            ),
            474 =>
            array (
                'id' => 1975,
                'language_code' => 'lv',
                'display_language_code' => 'id',
                'name' => 'Latvian',
            ),
            475 =>
            array (
                'id' => 1976,
                'language_code' => 'lv',
                'display_language_code' => 'is',
                'name' => 'Latvian',
            ),
            476 =>
            array (
                'id' => 1977,
                'language_code' => 'lv',
                'display_language_code' => 'it',
                'name' => 'Lettone',
            ),
            477 =>
            array (
                'id' => 1978,
                'language_code' => 'lv',
                'display_language_code' => 'ja',
                'name' => 'ラトビア語',
            ),
            478 =>
            array (
                'id' => 1979,
                'language_code' => 'lv',
                'display_language_code' => 'ko',
                'name' => '라트비아어',
            ),
            479 =>
            array (
                'id' => 1980,
                'language_code' => 'lv',
                'display_language_code' => 'ku',
                'name' => 'Latvian',
            ),
            480 =>
            array (
                'id' => 1981,
                'language_code' => 'lv',
                'display_language_code' => 'lv',
                'name' => 'Latviešu',
            ),
            481 =>
            array (
                'id' => 1982,
                'language_code' => 'lv',
                'display_language_code' => 'lt',
                'name' => 'Latvian',
            ),
            482 =>
            array (
                'id' => 1983,
                'language_code' => 'lv',
                'display_language_code' => 'mk',
                'name' => 'Latvian',
            ),
            483 =>
            array (
                'id' => 1984,
                'language_code' => 'lv',
                'display_language_code' => 'mt',
                'name' => 'Latvian',
            ),
            484 =>
            array (
                'id' => 1985,
                'language_code' => 'lv',
                'display_language_code' => 'mn',
                'name' => 'Latvian',
            ),
            485 =>
            array (
                'id' => 1986,
                'language_code' => 'lv',
                'display_language_code' => 'ne',
                'name' => 'Latvian',
            ),
            486 =>
            array (
                'id' => 1987,
                'language_code' => 'lv',
                'display_language_code' => 'nl',
                'name' => 'Lets',
            ),
            487 =>
            array (
                'id' => 1988,
                'language_code' => 'lv',
                'display_language_code' => 'no',
                'name' => 'Latvisk',
            ),
            488 =>
            array (
                'id' => 1989,
                'language_code' => 'lv',
                'display_language_code' => 'pa',
                'name' => 'Latvian',
            ),
            489 =>
            array (
                'id' => 1990,
                'language_code' => 'lv',
                'display_language_code' => 'pl',
                'name' => 'łotewski',
            ),
            490 =>
            array (
                'id' => 1991,
                'language_code' => 'lv',
                'display_language_code' => 'pt-pt',
                'name' => 'Letão',
            ),
            491 =>
            array (
                'id' => 1992,
                'language_code' => 'lv',
                'display_language_code' => 'pt-br',
                'name' => 'Letão',
            ),
            492 =>
            array (
                'id' => 1993,
                'language_code' => 'lv',
                'display_language_code' => 'qu',
                'name' => 'Latvian',
            ),
            493 =>
            array (
                'id' => 1994,
                'language_code' => 'lv',
                'display_language_code' => 'ro',
                'name' => 'Letoniană',
            ),
            494 =>
            array (
                'id' => 1995,
                'language_code' => 'lv',
                'display_language_code' => 'ru',
                'name' => 'латышский',
            ),
            495 =>
            array (
                'id' => 1996,
                'language_code' => 'lv',
                'display_language_code' => 'sl',
                'name' => 'Latvijščina',
            ),
            496 =>
            array (
                'id' => 1997,
                'language_code' => 'lv',
                'display_language_code' => 'so',
                'name' => 'Latvian',
            ),
            497 =>
            array (
                'id' => 1998,
                'language_code' => 'lv',
                'display_language_code' => 'sq',
                'name' => 'Latvian',
            ),
            498 =>
            array (
                'id' => 1999,
                'language_code' => 'lv',
                'display_language_code' => 'sr',
                'name' => 'летонски',
            ),
            499 =>
            array (
                'id' => 2000,
                'language_code' => 'lv',
                'display_language_code' => 'sv',
                'name' => 'Lettiska',
            ),
        ));
        \DB::table('language_translations')->insert(array (
            0 =>
            array (
                'id' => 2001,
                'language_code' => 'lv',
                'display_language_code' => 'ta',
                'name' => 'Latvian',
            ),
            1 =>
            array (
                'id' => 2002,
                'language_code' => 'lv',
                'display_language_code' => 'th',
                'name' => 'ลัตเวีย',
            ),
            2 =>
            array (
                'id' => 2003,
                'language_code' => 'lv',
                'display_language_code' => 'tr',
                'name' => 'Letonca',
            ),
            3 =>
            array (
                'id' => 2004,
                'language_code' => 'lv',
                'display_language_code' => 'uk',
                'name' => 'Latvian',
            ),
            4 =>
            array (
                'id' => 2005,
                'language_code' => 'lv',
                'display_language_code' => 'ur',
                'name' => 'Latvian',
            ),
            5 =>
            array (
                'id' => 2006,
                'language_code' => 'lv',
                'display_language_code' => 'uz',
                'name' => 'Latvian',
            ),
            6 =>
            array (
                'id' => 2007,
                'language_code' => 'lv',
                'display_language_code' => 'vi',
                'name' => 'Latvian',
            ),
            7 =>
            array (
                'id' => 2008,
                'language_code' => 'lv',
                'display_language_code' => 'yi',
                'name' => 'Latvian',
            ),
            8 =>
            array (
                'id' => 2009,
                'language_code' => 'lv',
                'display_language_code' => 'zh-hans',
                'name' => '拉脱维亚语',
            ),
            9 =>
            array (
                'id' => 2010,
                'language_code' => 'lv',
                'display_language_code' => 'zu',
                'name' => 'Latvian',
            ),
            10 =>
            array (
                'id' => 2011,
                'language_code' => 'lv',
                'display_language_code' => 'zh-hant',
                'name' => '拉脫維亞語',
            ),
            11 =>
            array (
                'id' => 2012,
                'language_code' => 'lv',
                'display_language_code' => 'ms',
                'name' => 'Latvian',
            ),
            12 =>
            array (
                'id' => 2013,
                'language_code' => 'lv',
                'display_language_code' => 'gl',
                'name' => 'Latvian',
            ),
            13 =>
            array (
                'id' => 2014,
                'language_code' => 'lv',
                'display_language_code' => 'bn',
                'name' => 'Latvian',
            ),
            14 =>
            array (
                'id' => 2015,
                'language_code' => 'lv',
                'display_language_code' => 'az',
                'name' => 'Latvian',
            ),
            15 =>
            array (
                'id' => 2016,
                'language_code' => 'lt',
                'display_language_code' => 'en',
                'name' => 'Lithuanian',
            ),
            16 =>
            array (
                'id' => 2017,
                'language_code' => 'lt',
                'display_language_code' => 'es',
                'name' => 'Lituano',
            ),
            17 =>
            array (
                'id' => 2018,
                'language_code' => 'lt',
                'display_language_code' => 'de',
                'name' => 'Litauisch',
            ),
            18 =>
            array (
                'id' => 2019,
                'language_code' => 'lt',
                'display_language_code' => 'fr',
                'name' => 'Lituanien',
            ),
            19 =>
            array (
                'id' => 2020,
                'language_code' => 'lt',
                'display_language_code' => 'ar',
                'name' => 'اللتوانية',
            ),
            20 =>
            array (
                'id' => 2021,
                'language_code' => 'lt',
                'display_language_code' => 'bs',
                'name' => 'Lithuanian',
            ),
            21 =>
            array (
                'id' => 2022,
                'language_code' => 'lt',
                'display_language_code' => 'bg',
                'name' => 'Литовски',
            ),
            22 =>
            array (
                'id' => 2023,
                'language_code' => 'lt',
                'display_language_code' => 'ca',
                'name' => 'Lithuanian',
            ),
            23 =>
            array (
                'id' => 2024,
                'language_code' => 'lt',
                'display_language_code' => 'cs',
                'name' => 'Litevský',
            ),
            24 =>
            array (
                'id' => 2025,
                'language_code' => 'lt',
                'display_language_code' => 'sk',
                'name' => 'Litovčina',
            ),
            25 =>
            array (
                'id' => 2026,
                'language_code' => 'lt',
                'display_language_code' => 'cy',
                'name' => 'Lithuanian',
            ),
            26 =>
            array (
                'id' => 2027,
                'language_code' => 'lt',
                'display_language_code' => 'da',
                'name' => 'Lithuanian',
            ),
            27 =>
            array (
                'id' => 2028,
                'language_code' => 'lt',
                'display_language_code' => 'el',
                'name' => 'Λιθουανικά',
            ),
            28 =>
            array (
                'id' => 2029,
                'language_code' => 'lt',
                'display_language_code' => 'eo',
                'name' => 'Lithuanian',
            ),
            29 =>
            array (
                'id' => 2030,
                'language_code' => 'lt',
                'display_language_code' => 'et',
                'name' => 'Lithuanian',
            ),
            30 =>
            array (
                'id' => 2031,
                'language_code' => 'lt',
                'display_language_code' => 'eu',
                'name' => 'Lithuanian',
            ),
            31 =>
            array (
                'id' => 2032,
                'language_code' => 'lt',
                'display_language_code' => 'fa',
                'name' => 'Lithuanian',
            ),
            32 =>
            array (
                'id' => 2033,
                'language_code' => 'lt',
                'display_language_code' => 'fi',
                'name' => 'Liettua',
            ),
            33 =>
            array (
                'id' => 2034,
                'language_code' => 'lt',
                'display_language_code' => 'ga',
                'name' => 'Lithuanian',
            ),
            34 =>
            array (
                'id' => 2035,
                'language_code' => 'lt',
                'display_language_code' => 'he',
                'name' => 'ליטאית',
            ),
            35 =>
            array (
                'id' => 2036,
                'language_code' => 'lt',
                'display_language_code' => 'hi',
                'name' => 'Lithuanian',
            ),
            36 =>
            array (
                'id' => 2037,
                'language_code' => 'lt',
                'display_language_code' => 'hr',
                'name' => 'Litavski',
            ),
            37 =>
            array (
                'id' => 2038,
                'language_code' => 'lt',
                'display_language_code' => 'hu',
                'name' => 'Litván',
            ),
            38 =>
            array (
                'id' => 2039,
                'language_code' => 'lt',
                'display_language_code' => 'hy',
                'name' => 'Lithuanian',
            ),
            39 =>
            array (
                'id' => 2040,
                'language_code' => 'lt',
                'display_language_code' => 'id',
                'name' => 'Lithuanian',
            ),
            40 =>
            array (
                'id' => 2041,
                'language_code' => 'lt',
                'display_language_code' => 'is',
                'name' => 'Lithuanian',
            ),
            41 =>
            array (
                'id' => 2042,
                'language_code' => 'lt',
                'display_language_code' => 'it',
                'name' => 'Lituano',
            ),
            42 =>
            array (
                'id' => 2043,
                'language_code' => 'lt',
                'display_language_code' => 'ja',
                'name' => 'リトアニア語',
            ),
            43 =>
            array (
                'id' => 2044,
                'language_code' => 'lt',
                'display_language_code' => 'ko',
                'name' => '리투아니아어',
            ),
            44 =>
            array (
                'id' => 2045,
                'language_code' => 'lt',
                'display_language_code' => 'ku',
                'name' => 'Lithuanian',
            ),
            45 =>
            array (
                'id' => 2046,
                'language_code' => 'lt',
                'display_language_code' => 'lv',
                'name' => 'Lithuanian',
            ),
            46 =>
            array (
                'id' => 2047,
                'language_code' => 'lt',
                'display_language_code' => 'lt',
                'name' => 'Lietuvių',
            ),
            47 =>
            array (
                'id' => 2048,
                'language_code' => 'lt',
                'display_language_code' => 'mk',
                'name' => 'Lithuanian',
            ),
            48 =>
            array (
                'id' => 2049,
                'language_code' => 'lt',
                'display_language_code' => 'mt',
                'name' => 'Lithuanian',
            ),
            49 =>
            array (
                'id' => 2050,
                'language_code' => 'lt',
                'display_language_code' => 'mn',
                'name' => 'Lithuanian',
            ),
            50 =>
            array (
                'id' => 2051,
                'language_code' => 'lt',
                'display_language_code' => 'ne',
                'name' => 'Lithuanian',
            ),
            51 =>
            array (
                'id' => 2052,
                'language_code' => 'lt',
                'display_language_code' => 'nl',
                'name' => 'Litouws',
            ),
            52 =>
            array (
                'id' => 2053,
                'language_code' => 'lt',
                'display_language_code' => 'no',
                'name' => 'Litauisk',
            ),
            53 =>
            array (
                'id' => 2054,
                'language_code' => 'lt',
                'display_language_code' => 'pa',
                'name' => 'Lithuanian',
            ),
            54 =>
            array (
                'id' => 2055,
                'language_code' => 'lt',
                'display_language_code' => 'pl',
                'name' => 'Litewski',
            ),
            55 =>
            array (
                'id' => 2056,
                'language_code' => 'lt',
                'display_language_code' => 'pt-pt',
                'name' => 'Lituano',
            ),
            56 =>
            array (
                'id' => 2057,
                'language_code' => 'lt',
                'display_language_code' => 'pt-br',
                'name' => 'Lituano',
            ),
            57 =>
            array (
                'id' => 2058,
                'language_code' => 'lt',
                'display_language_code' => 'qu',
                'name' => 'Lithuanian',
            ),
            58 =>
            array (
                'id' => 2059,
                'language_code' => 'lt',
                'display_language_code' => 'ro',
                'name' => 'Lituaniană',
            ),
            59 =>
            array (
                'id' => 2060,
                'language_code' => 'lt',
                'display_language_code' => 'ru',
                'name' => 'Литовский',
            ),
            60 =>
            array (
                'id' => 2061,
                'language_code' => 'lt',
                'display_language_code' => 'sl',
                'name' => 'Litovščina',
            ),
            61 =>
            array (
                'id' => 2062,
                'language_code' => 'lt',
                'display_language_code' => 'so',
                'name' => 'Lithuanian',
            ),
            62 =>
            array (
                'id' => 2063,
                'language_code' => 'lt',
                'display_language_code' => 'sq',
                'name' => 'Lithuanian',
            ),
            63 =>
            array (
                'id' => 2064,
                'language_code' => 'lt',
                'display_language_code' => 'sr',
                'name' => 'литвански',
            ),
            64 =>
            array (
                'id' => 2065,
                'language_code' => 'lt',
                'display_language_code' => 'sv',
                'name' => 'Litauiska',
            ),
            65 =>
            array (
                'id' => 2066,
                'language_code' => 'lt',
                'display_language_code' => 'ta',
                'name' => 'Lithuanian',
            ),
            66 =>
            array (
                'id' => 2067,
                'language_code' => 'lt',
                'display_language_code' => 'th',
                'name' => 'ลิธัวเนีย',
            ),
            67 =>
            array (
                'id' => 2068,
                'language_code' => 'lt',
                'display_language_code' => 'tr',
                'name' => 'Litvanyaca',
            ),
            68 =>
            array (
                'id' => 2069,
                'language_code' => 'lt',
                'display_language_code' => 'uk',
                'name' => 'Lithuanian',
            ),
            69 =>
            array (
                'id' => 2070,
                'language_code' => 'lt',
                'display_language_code' => 'ur',
                'name' => 'Lithuanian',
            ),
            70 =>
            array (
                'id' => 2071,
                'language_code' => 'lt',
                'display_language_code' => 'uz',
                'name' => 'Lithuanian',
            ),
            71 =>
            array (
                'id' => 2072,
                'language_code' => 'lt',
                'display_language_code' => 'vi',
                'name' => 'Lithuanian',
            ),
            72 =>
            array (
                'id' => 2073,
                'language_code' => 'lt',
                'display_language_code' => 'yi',
                'name' => 'Lithuanian',
            ),
            73 =>
            array (
                'id' => 2074,
                'language_code' => 'lt',
                'display_language_code' => 'zh-hans',
                'name' => '立陶宛语',
            ),
            74 =>
            array (
                'id' => 2075,
                'language_code' => 'lt',
                'display_language_code' => 'zu',
                'name' => 'Lithuanian',
            ),
            75 =>
            array (
                'id' => 2076,
                'language_code' => 'lt',
                'display_language_code' => 'zh-hant',
                'name' => '立陶宛語',
            ),
            76 =>
            array (
                'id' => 2077,
                'language_code' => 'lt',
                'display_language_code' => 'ms',
                'name' => 'Lithuanian',
            ),
            77 =>
            array (
                'id' => 2078,
                'language_code' => 'lt',
                'display_language_code' => 'gl',
                'name' => 'Lithuanian',
            ),
            78 =>
            array (
                'id' => 2079,
                'language_code' => 'lt',
                'display_language_code' => 'bn',
                'name' => 'Lithuanian',
            ),
            79 =>
            array (
                'id' => 2080,
                'language_code' => 'lt',
                'display_language_code' => 'az',
                'name' => 'Lithuanian',
            ),
            80 =>
            array (
                'id' => 2081,
                'language_code' => 'mk',
                'display_language_code' => 'en',
                'name' => 'Macedonian',
            ),
            81 =>
            array (
                'id' => 2082,
                'language_code' => 'mk',
                'display_language_code' => 'es',
                'name' => 'Macedonio',
            ),
            82 =>
            array (
                'id' => 2083,
                'language_code' => 'mk',
                'display_language_code' => 'de',
                'name' => 'Mazedonisch',
            ),
            83 =>
            array (
                'id' => 2084,
                'language_code' => 'mk',
                'display_language_code' => 'fr',
                'name' => 'Macédonien',
            ),
            84 =>
            array (
                'id' => 2085,
                'language_code' => 'mk',
                'display_language_code' => 'ar',
                'name' => 'المقدونية',
            ),
            85 =>
            array (
                'id' => 2086,
                'language_code' => 'mk',
                'display_language_code' => 'bs',
                'name' => 'Macedonian',
            ),
            86 =>
            array (
                'id' => 2087,
                'language_code' => 'mk',
                'display_language_code' => 'bg',
                'name' => 'Македонски',
            ),
            87 =>
            array (
                'id' => 2088,
                'language_code' => 'mk',
                'display_language_code' => 'ca',
                'name' => 'Macedonian',
            ),
            88 =>
            array (
                'id' => 2089,
                'language_code' => 'mk',
                'display_language_code' => 'cs',
                'name' => 'Makedonský',
            ),
            89 =>
            array (
                'id' => 2090,
                'language_code' => 'mk',
                'display_language_code' => 'sk',
                'name' => 'Macedónština',
            ),
            90 =>
            array (
                'id' => 2091,
                'language_code' => 'mk',
                'display_language_code' => 'cy',
                'name' => 'Macedonian',
            ),
            91 =>
            array (
                'id' => 2092,
                'language_code' => 'mk',
                'display_language_code' => 'da',
                'name' => 'Macedonian',
            ),
            92 =>
            array (
                'id' => 2093,
                'language_code' => 'mk',
                'display_language_code' => 'el',
                'name' => 'Μακεδονικά',
            ),
            93 =>
            array (
                'id' => 2094,
                'language_code' => 'mk',
                'display_language_code' => 'eo',
                'name' => 'Macedonian',
            ),
            94 =>
            array (
                'id' => 2095,
                'language_code' => 'mk',
                'display_language_code' => 'et',
                'name' => 'Macedonian',
            ),
            95 =>
            array (
                'id' => 2096,
                'language_code' => 'mk',
                'display_language_code' => 'eu',
                'name' => 'Macedonian',
            ),
            96 =>
            array (
                'id' => 2097,
                'language_code' => 'mk',
                'display_language_code' => 'fa',
                'name' => 'Macedonian',
            ),
            97 =>
            array (
                'id' => 2098,
                'language_code' => 'mk',
                'display_language_code' => 'fi',
                'name' => 'Makedonia',
            ),
            98 =>
            array (
                'id' => 2099,
                'language_code' => 'mk',
                'display_language_code' => 'ga',
                'name' => 'Macedonian',
            ),
            99 =>
            array (
                'id' => 2100,
                'language_code' => 'mk',
                'display_language_code' => 'he',
                'name' => 'מקדונית',
            ),
            100 =>
            array (
                'id' => 2101,
                'language_code' => 'mk',
                'display_language_code' => 'hi',
                'name' => 'Macedonian',
            ),
            101 =>
            array (
                'id' => 2102,
                'language_code' => 'mk',
                'display_language_code' => 'hr',
                'name' => 'Makedonski',
            ),
            102 =>
            array (
                'id' => 2103,
                'language_code' => 'mk',
                'display_language_code' => 'hu',
                'name' => 'Macedón',
            ),
            103 =>
            array (
                'id' => 2104,
                'language_code' => 'mk',
                'display_language_code' => 'hy',
                'name' => 'Macedonian',
            ),
            104 =>
            array (
                'id' => 2105,
                'language_code' => 'mk',
                'display_language_code' => 'id',
                'name' => 'Macedonian',
            ),
            105 =>
            array (
                'id' => 2106,
                'language_code' => 'mk',
                'display_language_code' => 'is',
                'name' => 'Macedonian',
            ),
            106 =>
            array (
                'id' => 2107,
                'language_code' => 'mk',
                'display_language_code' => 'it',
                'name' => 'Macedone',
            ),
            107 =>
            array (
                'id' => 2108,
                'language_code' => 'mk',
                'display_language_code' => 'ja',
                'name' => 'マケドニア語',
            ),
            108 =>
            array (
                'id' => 2109,
                'language_code' => 'mk',
                'display_language_code' => 'ko',
                'name' => '마케도니아어',
            ),
            109 =>
            array (
                'id' => 2110,
                'language_code' => 'mk',
                'display_language_code' => 'ku',
                'name' => 'Macedonian',
            ),
            110 =>
            array (
                'id' => 2111,
                'language_code' => 'mk',
                'display_language_code' => 'lv',
                'name' => 'Macedonian',
            ),
            111 =>
            array (
                'id' => 2112,
                'language_code' => 'mk',
                'display_language_code' => 'lt',
                'name' => 'Macedonian',
            ),
            112 =>
            array (
                'id' => 2113,
                'language_code' => 'mk',
                'display_language_code' => 'mk',
                'name' => 'македонски',
            ),
            113 =>
            array (
                'id' => 2114,
                'language_code' => 'mk',
                'display_language_code' => 'mt',
                'name' => 'Macedonian',
            ),
            114 =>
            array (
                'id' => 2115,
                'language_code' => 'mk',
                'display_language_code' => 'mn',
                'name' => 'Macedonian',
            ),
            115 =>
            array (
                'id' => 2116,
                'language_code' => 'mk',
                'display_language_code' => 'ne',
                'name' => 'Macedonian',
            ),
            116 =>
            array (
                'id' => 2117,
                'language_code' => 'mk',
                'display_language_code' => 'nl',
                'name' => 'Macedonisch',
            ),
            117 =>
            array (
                'id' => 2118,
                'language_code' => 'mk',
                'display_language_code' => 'no',
                'name' => 'Makedonsk',
            ),
            118 =>
            array (
                'id' => 2119,
                'language_code' => 'mk',
                'display_language_code' => 'pa',
                'name' => 'Macedonian',
            ),
            119 =>
            array (
                'id' => 2120,
                'language_code' => 'mk',
                'display_language_code' => 'pl',
                'name' => 'Macedoński',
            ),
            120 =>
            array (
                'id' => 2121,
                'language_code' => 'mk',
                'display_language_code' => 'pt-pt',
                'name' => 'Macedônio',
            ),
            121 =>
            array (
                'id' => 2122,
                'language_code' => 'mk',
                'display_language_code' => 'pt-br',
                'name' => 'Macedônio',
            ),
            122 =>
            array (
                'id' => 2123,
                'language_code' => 'mk',
                'display_language_code' => 'qu',
                'name' => 'Macedonian',
            ),
            123 =>
            array (
                'id' => 2124,
                'language_code' => 'mk',
                'display_language_code' => 'ro',
                'name' => 'Macedoniană',
            ),
            124 =>
            array (
                'id' => 2125,
                'language_code' => 'mk',
                'display_language_code' => 'ru',
                'name' => 'Македонский',
            ),
            125 =>
            array (
                'id' => 2126,
                'language_code' => 'mk',
                'display_language_code' => 'sl',
                'name' => 'Makedonski',
            ),
            126 =>
            array (
                'id' => 2127,
                'language_code' => 'mk',
                'display_language_code' => 'so',
                'name' => 'Macedonian',
            ),
            127 =>
            array (
                'id' => 2128,
                'language_code' => 'mk',
                'display_language_code' => 'sq',
                'name' => 'Macedonian',
            ),
            128 =>
            array (
                'id' => 2129,
                'language_code' => 'mk',
                'display_language_code' => 'sr',
                'name' => 'македонски',
            ),
            129 =>
            array (
                'id' => 2130,
                'language_code' => 'mk',
                'display_language_code' => 'sv',
                'name' => 'Makedonska',
            ),
            130 =>
            array (
                'id' => 2131,
                'language_code' => 'mk',
                'display_language_code' => 'ta',
                'name' => 'Macedonian',
            ),
            131 =>
            array (
                'id' => 2132,
                'language_code' => 'mk',
                'display_language_code' => 'th',
                'name' => 'มาซิโดเนีย',
            ),
            132 =>
            array (
                'id' => 2133,
                'language_code' => 'mk',
                'display_language_code' => 'tr',
                'name' => 'Makedonyaca',
            ),
            133 =>
            array (
                'id' => 2134,
                'language_code' => 'mk',
                'display_language_code' => 'uk',
                'name' => 'Macedonian',
            ),
            134 =>
            array (
                'id' => 2135,
                'language_code' => 'mk',
                'display_language_code' => 'ur',
                'name' => 'Macedonian',
            ),
            135 =>
            array (
                'id' => 2136,
                'language_code' => 'mk',
                'display_language_code' => 'uz',
                'name' => 'Macedonian',
            ),
            136 =>
            array (
                'id' => 2137,
                'language_code' => 'mk',
                'display_language_code' => 'vi',
                'name' => 'Macedonian',
            ),
            137 =>
            array (
                'id' => 2138,
                'language_code' => 'mk',
                'display_language_code' => 'yi',
                'name' => 'Macedonian',
            ),
            138 =>
            array (
                'id' => 2139,
                'language_code' => 'mk',
                'display_language_code' => 'zh-hans',
                'name' => '马其顿语',
            ),
            139 =>
            array (
                'id' => 2140,
                'language_code' => 'mk',
                'display_language_code' => 'zu',
                'name' => 'Macedonian',
            ),
            140 =>
            array (
                'id' => 2141,
                'language_code' => 'mk',
                'display_language_code' => 'zh-hant',
                'name' => '馬其頓語',
            ),
            141 =>
            array (
                'id' => 2142,
                'language_code' => 'mk',
                'display_language_code' => 'ms',
                'name' => 'Macedonian',
            ),
            142 =>
            array (
                'id' => 2143,
                'language_code' => 'mk',
                'display_language_code' => 'gl',
                'name' => 'Macedonian',
            ),
            143 =>
            array (
                'id' => 2144,
                'language_code' => 'mk',
                'display_language_code' => 'bn',
                'name' => 'Macedonian',
            ),
            144 =>
            array (
                'id' => 2145,
                'language_code' => 'mk',
                'display_language_code' => 'az',
                'name' => 'Macedonian',
            ),
            145 =>
            array (
                'id' => 2146,
                'language_code' => 'mt',
                'display_language_code' => 'en',
                'name' => 'Maltese',
            ),
            146 =>
            array (
                'id' => 2147,
                'language_code' => 'mt',
                'display_language_code' => 'es',
                'name' => 'Maltés',
            ),
            147 =>
            array (
                'id' => 2148,
                'language_code' => 'mt',
                'display_language_code' => 'de',
                'name' => 'Maltesisch',
            ),
            148 =>
            array (
                'id' => 2149,
                'language_code' => 'mt',
                'display_language_code' => 'fr',
                'name' => 'Maltais',
            ),
            149 =>
            array (
                'id' => 2150,
                'language_code' => 'mt',
                'display_language_code' => 'ar',
                'name' => 'المالطية',
            ),
            150 =>
            array (
                'id' => 2151,
                'language_code' => 'mt',
                'display_language_code' => 'bs',
                'name' => 'Maltese',
            ),
            151 =>
            array (
                'id' => 2152,
                'language_code' => 'mt',
                'display_language_code' => 'bg',
                'name' => 'Малтийски',
            ),
            152 =>
            array (
                'id' => 2153,
                'language_code' => 'mt',
                'display_language_code' => 'ca',
                'name' => 'Maltese',
            ),
            153 =>
            array (
                'id' => 2154,
                'language_code' => 'mt',
                'display_language_code' => 'cs',
                'name' => 'Maltština',
            ),
            154 =>
            array (
                'id' => 2155,
                'language_code' => 'mt',
                'display_language_code' => 'sk',
                'name' => 'Maltézština',
            ),
            155 =>
            array (
                'id' => 2156,
                'language_code' => 'mt',
                'display_language_code' => 'cy',
                'name' => 'Maltese',
            ),
            156 =>
            array (
                'id' => 2157,
                'language_code' => 'mt',
                'display_language_code' => 'da',
                'name' => 'Maltese',
            ),
            157 =>
            array (
                'id' => 2158,
                'language_code' => 'mt',
                'display_language_code' => 'el',
                'name' => 'Μαλτέζικα',
            ),
            158 =>
            array (
                'id' => 2159,
                'language_code' => 'mt',
                'display_language_code' => 'eo',
                'name' => 'Maltese',
            ),
            159 =>
            array (
                'id' => 2160,
                'language_code' => 'mt',
                'display_language_code' => 'et',
                'name' => 'Maltese',
            ),
            160 =>
            array (
                'id' => 2161,
                'language_code' => 'mt',
                'display_language_code' => 'eu',
                'name' => 'Maltese',
            ),
            161 =>
            array (
                'id' => 2162,
                'language_code' => 'mt',
                'display_language_code' => 'fa',
                'name' => 'Maltese',
            ),
            162 =>
            array (
                'id' => 2163,
                'language_code' => 'mt',
                'display_language_code' => 'fi',
                'name' => 'Malta',
            ),
            163 =>
            array (
                'id' => 2164,
                'language_code' => 'mt',
                'display_language_code' => 'ga',
                'name' => 'Maltese',
            ),
            164 =>
            array (
                'id' => 2165,
                'language_code' => 'mt',
                'display_language_code' => 'he',
                'name' => 'מלטזית',
            ),
            165 =>
            array (
                'id' => 2166,
                'language_code' => 'mt',
                'display_language_code' => 'hi',
                'name' => 'Maltese',
            ),
            166 =>
            array (
                'id' => 2167,
                'language_code' => 'mt',
                'display_language_code' => 'hr',
                'name' => 'Malteški',
            ),
            167 =>
            array (
                'id' => 2168,
                'language_code' => 'mt',
                'display_language_code' => 'hu',
                'name' => 'Máltai',
            ),
            168 =>
            array (
                'id' => 2169,
                'language_code' => 'mt',
                'display_language_code' => 'hy',
                'name' => 'Maltese',
            ),
            169 =>
            array (
                'id' => 2170,
                'language_code' => 'mt',
                'display_language_code' => 'id',
                'name' => 'Maltese',
            ),
            170 =>
            array (
                'id' => 2171,
                'language_code' => 'mt',
                'display_language_code' => 'is',
                'name' => 'Maltese',
            ),
            171 =>
            array (
                'id' => 2172,
                'language_code' => 'mt',
                'display_language_code' => 'it',
                'name' => 'Maltese',
            ),
            172 =>
            array (
                'id' => 2173,
                'language_code' => 'mt',
                'display_language_code' => 'ja',
                'name' => 'マルタ語',
            ),
            173 =>
            array (
                'id' => 2174,
                'language_code' => 'mt',
                'display_language_code' => 'ko',
                'name' => '몰타어',
            ),
            174 =>
            array (
                'id' => 2175,
                'language_code' => 'mt',
                'display_language_code' => 'ku',
                'name' => 'Maltese',
            ),
            175 =>
            array (
                'id' => 2176,
                'language_code' => 'mt',
                'display_language_code' => 'lv',
                'name' => 'Maltese',
            ),
            176 =>
            array (
                'id' => 2177,
                'language_code' => 'mt',
                'display_language_code' => 'lt',
                'name' => 'Maltese',
            ),
            177 =>
            array (
                'id' => 2178,
                'language_code' => 'mt',
                'display_language_code' => 'mk',
                'name' => 'Maltese',
            ),
            178 =>
            array (
                'id' => 2179,
                'language_code' => 'mt',
                'display_language_code' => 'mt',
                'name' => 'Malti',
            ),
            179 =>
            array (
                'id' => 2180,
                'language_code' => 'mt',
                'display_language_code' => 'mn',
                'name' => 'Maltese',
            ),
            180 =>
            array (
                'id' => 2181,
                'language_code' => 'mt',
                'display_language_code' => 'ne',
                'name' => 'Maltese',
            ),
            181 =>
            array (
                'id' => 2182,
                'language_code' => 'mt',
                'display_language_code' => 'nl',
                'name' => 'Maltees',
            ),
            182 =>
            array (
                'id' => 2183,
                'language_code' => 'mt',
                'display_language_code' => 'no',
                'name' => 'Maltese',
            ),
            183 =>
            array (
                'id' => 2184,
                'language_code' => 'mt',
                'display_language_code' => 'pa',
                'name' => 'Maltese',
            ),
            184 =>
            array (
                'id' => 2185,
                'language_code' => 'mt',
                'display_language_code' => 'pl',
                'name' => 'Maltański',
            ),
            185 =>
            array (
                'id' => 2186,
                'language_code' => 'mt',
                'display_language_code' => 'pt-pt',
                'name' => 'Maltês',
            ),
            186 =>
            array (
                'id' => 2187,
                'language_code' => 'mt',
                'display_language_code' => 'pt-br',
                'name' => 'Maltês',
            ),
            187 =>
            array (
                'id' => 2188,
                'language_code' => 'mt',
                'display_language_code' => 'qu',
                'name' => 'Maltese',
            ),
            188 =>
            array (
                'id' => 2189,
                'language_code' => 'mt',
                'display_language_code' => 'ro',
                'name' => 'Malteză',
            ),
            189 =>
            array (
                'id' => 2190,
                'language_code' => 'mt',
                'display_language_code' => 'ru',
                'name' => 'Мальтийский',
            ),
            190 =>
            array (
                'id' => 2191,
                'language_code' => 'mt',
                'display_language_code' => 'sl',
                'name' => 'Malteški',
            ),
            191 =>
            array (
                'id' => 2192,
                'language_code' => 'mt',
                'display_language_code' => 'so',
                'name' => 'Maltese',
            ),
            192 =>
            array (
                'id' => 2193,
                'language_code' => 'mt',
                'display_language_code' => 'sq',
                'name' => 'Maltese',
            ),
            193 =>
            array (
                'id' => 2194,
                'language_code' => 'mt',
                'display_language_code' => 'sr',
                'name' => 'малтешки',
            ),
            194 =>
            array (
                'id' => 2195,
                'language_code' => 'mt',
                'display_language_code' => 'sv',
                'name' => 'Maltesiska',
            ),
            195 =>
            array (
                'id' => 2196,
                'language_code' => 'mt',
                'display_language_code' => 'ta',
                'name' => 'Maltese',
            ),
            196 =>
            array (
                'id' => 2197,
                'language_code' => 'mt',
                'display_language_code' => 'th',
                'name' => 'มอลทีส',
            ),
            197 =>
            array (
                'id' => 2198,
                'language_code' => 'mt',
                'display_language_code' => 'tr',
                'name' => 'Malta dili',
            ),
            198 =>
            array (
                'id' => 2199,
                'language_code' => 'mt',
                'display_language_code' => 'uk',
                'name' => 'Maltese',
            ),
            199 =>
            array (
                'id' => 2200,
                'language_code' => 'mt',
                'display_language_code' => 'ur',
                'name' => 'Maltese',
            ),
            200 =>
            array (
                'id' => 2201,
                'language_code' => 'mt',
                'display_language_code' => 'uz',
                'name' => 'Maltese',
            ),
            201 =>
            array (
                'id' => 2202,
                'language_code' => 'mt',
                'display_language_code' => 'vi',
                'name' => 'Maltese',
            ),
            202 =>
            array (
                'id' => 2203,
                'language_code' => 'mt',
                'display_language_code' => 'yi',
                'name' => 'Maltese',
            ),
            203 =>
            array (
                'id' => 2204,
                'language_code' => 'mt',
                'display_language_code' => 'zh-hans',
                'name' => '马耳他语',
            ),
            204 =>
            array (
                'id' => 2205,
                'language_code' => 'mt',
                'display_language_code' => 'zu',
                'name' => 'Maltese',
            ),
            205 =>
            array (
                'id' => 2206,
                'language_code' => 'mt',
                'display_language_code' => 'zh-hant',
                'name' => '馬爾他語',
            ),
            206 =>
            array (
                'id' => 2207,
                'language_code' => 'mt',
                'display_language_code' => 'ms',
                'name' => 'Maltese',
            ),
            207 =>
            array (
                'id' => 2208,
                'language_code' => 'mt',
                'display_language_code' => 'gl',
                'name' => 'Maltese',
            ),
            208 =>
            array (
                'id' => 2209,
                'language_code' => 'mt',
                'display_language_code' => 'bn',
                'name' => 'Maltese',
            ),
            209 =>
            array (
                'id' => 2210,
                'language_code' => 'mt',
                'display_language_code' => 'az',
                'name' => 'Maltese',
            ),
            210 =>
            array (
                'id' => 2211,
                'language_code' => 'mn',
                'display_language_code' => 'en',
                'name' => 'Mongolian',
            ),
            211 =>
            array (
                'id' => 2212,
                'language_code' => 'mn',
                'display_language_code' => 'es',
                'name' => 'Mongol',
            ),
            212 =>
            array (
                'id' => 2213,
                'language_code' => 'mn',
                'display_language_code' => 'de',
                'name' => 'Mongolisch',
            ),
            213 =>
            array (
                'id' => 2214,
                'language_code' => 'mn',
                'display_language_code' => 'fr',
                'name' => 'Mongol',
            ),
            214 =>
            array (
                'id' => 2215,
                'language_code' => 'mn',
                'display_language_code' => 'ar',
                'name' => 'المنغولية',
            ),
            215 =>
            array (
                'id' => 2216,
                'language_code' => 'mn',
                'display_language_code' => 'bs',
                'name' => 'Mongolian',
            ),
            216 =>
            array (
                'id' => 2217,
                'language_code' => 'mn',
                'display_language_code' => 'bg',
                'name' => 'Монголски',
            ),
            217 =>
            array (
                'id' => 2218,
                'language_code' => 'mn',
                'display_language_code' => 'ca',
                'name' => 'Mongolian',
            ),
            218 =>
            array (
                'id' => 2219,
                'language_code' => 'mn',
                'display_language_code' => 'cs',
                'name' => 'Mongolský',
            ),
            219 =>
            array (
                'id' => 2220,
                'language_code' => 'mn',
                'display_language_code' => 'sk',
                'name' => 'Mongolština',
            ),
            220 =>
            array (
                'id' => 2221,
                'language_code' => 'mn',
                'display_language_code' => 'cy',
                'name' => 'Mongolian',
            ),
            221 =>
            array (
                'id' => 2222,
                'language_code' => 'mn',
                'display_language_code' => 'da',
                'name' => 'Mongolian',
            ),
            222 =>
            array (
                'id' => 2223,
                'language_code' => 'mn',
                'display_language_code' => 'el',
                'name' => 'Μογγολικά',
            ),
            223 =>
            array (
                'id' => 2224,
                'language_code' => 'mn',
                'display_language_code' => 'eo',
                'name' => 'Mongolian',
            ),
            224 =>
            array (
                'id' => 2225,
                'language_code' => 'mn',
                'display_language_code' => 'et',
                'name' => 'Mongolian',
            ),
            225 =>
            array (
                'id' => 2226,
                'language_code' => 'mn',
                'display_language_code' => 'eu',
                'name' => 'Mongolian',
            ),
            226 =>
            array (
                'id' => 2227,
                'language_code' => 'mn',
                'display_language_code' => 'fa',
                'name' => 'Mongolian',
            ),
            227 =>
            array (
                'id' => 2228,
                'language_code' => 'mn',
                'display_language_code' => 'fi',
                'name' => 'Mongoli',
            ),
            228 =>
            array (
                'id' => 2229,
                'language_code' => 'mn',
                'display_language_code' => 'ga',
                'name' => 'Mongolian',
            ),
            229 =>
            array (
                'id' => 2230,
                'language_code' => 'mn',
                'display_language_code' => 'he',
                'name' => 'מונגולית',
            ),
            230 =>
            array (
                'id' => 2231,
                'language_code' => 'mn',
                'display_language_code' => 'hi',
                'name' => 'Mongolian',
            ),
            231 =>
            array (
                'id' => 2232,
                'language_code' => 'mn',
                'display_language_code' => 'hr',
                'name' => 'Mongolski',
            ),
            232 =>
            array (
                'id' => 2233,
                'language_code' => 'mn',
                'display_language_code' => 'hu',
                'name' => 'Mongol',
            ),
            233 =>
            array (
                'id' => 2234,
                'language_code' => 'mn',
                'display_language_code' => 'hy',
                'name' => 'Mongolian',
            ),
            234 =>
            array (
                'id' => 2235,
                'language_code' => 'mn',
                'display_language_code' => 'id',
                'name' => 'Mongolian',
            ),
            235 =>
            array (
                'id' => 2236,
                'language_code' => 'mn',
                'display_language_code' => 'is',
                'name' => 'Mongolian',
            ),
            236 =>
            array (
                'id' => 2237,
                'language_code' => 'mn',
                'display_language_code' => 'it',
                'name' => 'Mongolo',
            ),
            237 =>
            array (
                'id' => 2238,
                'language_code' => 'mn',
                'display_language_code' => 'ja',
                'name' => 'モンゴル語',
            ),
            238 =>
            array (
                'id' => 2239,
                'language_code' => 'mn',
                'display_language_code' => 'ko',
                'name' => '몽골어',
            ),
            239 =>
            array (
                'id' => 2240,
                'language_code' => 'mn',
                'display_language_code' => 'ku',
                'name' => 'Mongolian',
            ),
            240 =>
            array (
                'id' => 2241,
                'language_code' => 'mn',
                'display_language_code' => 'lv',
                'name' => 'Mongolian',
            ),
            241 =>
            array (
                'id' => 2242,
                'language_code' => 'mn',
                'display_language_code' => 'lt',
                'name' => 'Mongolian',
            ),
            242 =>
            array (
                'id' => 2243,
                'language_code' => 'mn',
                'display_language_code' => 'mk',
                'name' => 'Mongolian',
            ),
            243 =>
            array (
                'id' => 2244,
                'language_code' => 'mn',
                'display_language_code' => 'mt',
                'name' => 'Mongolian',
            ),
            244 =>
            array (
                'id' => 2245,
                'language_code' => 'mn',
                'display_language_code' => 'mn',
                'name' => 'Mongolian',
            ),
            245 =>
            array (
                'id' => 2246,
                'language_code' => 'mn',
                'display_language_code' => 'ne',
                'name' => 'Mongolian',
            ),
            246 =>
            array (
                'id' => 2247,
                'language_code' => 'mn',
                'display_language_code' => 'nl',
                'name' => 'Mongools',
            ),
            247 =>
            array (
                'id' => 2248,
                'language_code' => 'mn',
                'display_language_code' => 'no',
                'name' => 'Mongolsk',
            ),
            248 =>
            array (
                'id' => 2249,
                'language_code' => 'mn',
                'display_language_code' => 'pa',
                'name' => 'Mongolian',
            ),
            249 =>
            array (
                'id' => 2250,
                'language_code' => 'mn',
                'display_language_code' => 'pl',
                'name' => 'Mongolski',
            ),
            250 =>
            array (
                'id' => 2251,
                'language_code' => 'mn',
                'display_language_code' => 'pt-pt',
                'name' => 'Mongolian',
            ),
            251 =>
            array (
                'id' => 2252,
                'language_code' => 'mn',
                'display_language_code' => 'pt-br',
                'name' => 'Mongolian',
            ),
            252 =>
            array (
                'id' => 2253,
                'language_code' => 'mn',
                'display_language_code' => 'qu',
                'name' => 'Mongolian',
            ),
            253 =>
            array (
                'id' => 2254,
                'language_code' => 'mn',
                'display_language_code' => 'ro',
                'name' => 'Mongoleză',
            ),
            254 =>
            array (
                'id' => 2255,
                'language_code' => 'mn',
                'display_language_code' => 'ru',
                'name' => 'Монгольский',
            ),
            255 =>
            array (
                'id' => 2256,
                'language_code' => 'mn',
                'display_language_code' => 'sl',
                'name' => 'Mongolski',
            ),
            256 =>
            array (
                'id' => 2257,
                'language_code' => 'mn',
                'display_language_code' => 'so',
                'name' => 'Mongolian',
            ),
            257 =>
            array (
                'id' => 2258,
                'language_code' => 'mn',
                'display_language_code' => 'sq',
                'name' => 'Mongolian',
            ),
            258 =>
            array (
                'id' => 2259,
                'language_code' => 'mn',
                'display_language_code' => 'sr',
                'name' => 'монголски',
            ),
            259 =>
            array (
                'id' => 2260,
                'language_code' => 'mn',
                'display_language_code' => 'sv',
                'name' => 'Mongoliska',
            ),
            260 =>
            array (
                'id' => 2261,
                'language_code' => 'mn',
                'display_language_code' => 'ta',
                'name' => 'Mongolian',
            ),
            261 =>
            array (
                'id' => 2262,
                'language_code' => 'mn',
                'display_language_code' => 'th',
                'name' => 'มองโกเลีย',
            ),
            262 =>
            array (
                'id' => 2263,
                'language_code' => 'mn',
                'display_language_code' => 'tr',
                'name' => 'Mongolca',
            ),
            263 =>
            array (
                'id' => 2264,
                'language_code' => 'mn',
                'display_language_code' => 'uk',
                'name' => 'Mongolian',
            ),
            264 =>
            array (
                'id' => 2265,
                'language_code' => 'mn',
                'display_language_code' => 'ur',
                'name' => 'Mongolian',
            ),
            265 =>
            array (
                'id' => 2266,
                'language_code' => 'mn',
                'display_language_code' => 'uz',
                'name' => 'Mongolian',
            ),
            266 =>
            array (
                'id' => 2267,
                'language_code' => 'mn',
                'display_language_code' => 'vi',
                'name' => 'Mongolian',
            ),
            267 =>
            array (
                'id' => 2268,
                'language_code' => 'mn',
                'display_language_code' => 'yi',
                'name' => 'Mongolian',
            ),
            268 =>
            array (
                'id' => 2269,
                'language_code' => 'mn',
                'display_language_code' => 'zh-hans',
                'name' => '蒙古语',
            ),
            269 =>
            array (
                'id' => 2270,
                'language_code' => 'mn',
                'display_language_code' => 'zu',
                'name' => 'Mongolian',
            ),
            270 =>
            array (
                'id' => 2271,
                'language_code' => 'mn',
                'display_language_code' => 'zh-hant',
                'name' => '蒙古語',
            ),
            271 =>
            array (
                'id' => 2272,
                'language_code' => 'mn',
                'display_language_code' => 'ms',
                'name' => 'Mongolian',
            ),
            272 =>
            array (
                'id' => 2273,
                'language_code' => 'mn',
                'display_language_code' => 'gl',
                'name' => 'Mongolian',
            ),
            273 =>
            array (
                'id' => 2274,
                'language_code' => 'mn',
                'display_language_code' => 'bn',
                'name' => 'Mongolian',
            ),
            274 =>
            array (
                'id' => 2275,
                'language_code' => 'mn',
                'display_language_code' => 'az',
                'name' => 'Mongolian',
            ),
            275 =>
            array (
                'id' => 2276,
                'language_code' => 'ne',
                'display_language_code' => 'en',
                'name' => 'Nepali',
            ),
            276 =>
            array (
                'id' => 2277,
                'language_code' => 'ne',
                'display_language_code' => 'es',
                'name' => 'Nepalí',
            ),
            277 =>
            array (
                'id' => 2278,
                'language_code' => 'ne',
                'display_language_code' => 'de',
                'name' => 'Nepali',
            ),
            278 =>
            array (
                'id' => 2279,
                'language_code' => 'ne',
                'display_language_code' => 'fr',
                'name' => 'Népalais',
            ),
            279 =>
            array (
                'id' => 2280,
                'language_code' => 'ne',
                'display_language_code' => 'ar',
                'name' => 'النيبالية',
            ),
            280 =>
            array (
                'id' => 2281,
                'language_code' => 'ne',
                'display_language_code' => 'bs',
                'name' => 'Nepali',
            ),
            281 =>
            array (
                'id' => 2282,
                'language_code' => 'ne',
                'display_language_code' => 'bg',
                'name' => 'Непалски',
            ),
            282 =>
            array (
                'id' => 2283,
                'language_code' => 'ne',
                'display_language_code' => 'ca',
                'name' => 'Nepali',
            ),
            283 =>
            array (
                'id' => 2284,
                'language_code' => 'ne',
                'display_language_code' => 'cs',
                'name' => 'Nepálský',
            ),
            284 =>
            array (
                'id' => 2285,
                'language_code' => 'ne',
                'display_language_code' => 'sk',
                'name' => 'Nepálčina',
            ),
            285 =>
            array (
                'id' => 2286,
                'language_code' => 'ne',
                'display_language_code' => 'cy',
                'name' => 'Nepali',
            ),
            286 =>
            array (
                'id' => 2287,
                'language_code' => 'ne',
                'display_language_code' => 'da',
                'name' => 'Nepali',
            ),
            287 =>
            array (
                'id' => 2288,
                'language_code' => 'ne',
                'display_language_code' => 'el',
                'name' => 'Νεπαλικά',
            ),
            288 =>
            array (
                'id' => 2289,
                'language_code' => 'ne',
                'display_language_code' => 'eo',
                'name' => 'Nepali',
            ),
            289 =>
            array (
                'id' => 2290,
                'language_code' => 'ne',
                'display_language_code' => 'et',
                'name' => 'Nepali',
            ),
            290 =>
            array (
                'id' => 2291,
                'language_code' => 'ne',
                'display_language_code' => 'eu',
                'name' => 'Nepali',
            ),
            291 =>
            array (
                'id' => 2292,
                'language_code' => 'ne',
                'display_language_code' => 'fa',
                'name' => 'Nepali',
            ),
            292 =>
            array (
                'id' => 2293,
                'language_code' => 'ne',
                'display_language_code' => 'fi',
                'name' => 'Nepali',
            ),
            293 =>
            array (
                'id' => 2294,
                'language_code' => 'ne',
                'display_language_code' => 'ga',
                'name' => 'Nepali',
            ),
            294 =>
            array (
                'id' => 2295,
                'language_code' => 'ne',
                'display_language_code' => 'he',
                'name' => 'נפאלית',
            ),
            295 =>
            array (
                'id' => 2296,
                'language_code' => 'ne',
                'display_language_code' => 'hi',
                'name' => 'Nepali',
            ),
            296 =>
            array (
                'id' => 2297,
                'language_code' => 'ne',
                'display_language_code' => 'hr',
                'name' => 'Nepali',
            ),
            297 =>
            array (
                'id' => 2298,
                'language_code' => 'ne',
                'display_language_code' => 'hu',
                'name' => 'Nepáli',
            ),
            298 =>
            array (
                'id' => 2299,
                'language_code' => 'ne',
                'display_language_code' => 'hy',
                'name' => 'Nepali',
            ),
            299 =>
            array (
                'id' => 2300,
                'language_code' => 'ne',
                'display_language_code' => 'id',
                'name' => 'Nepali',
            ),
            300 =>
            array (
                'id' => 2301,
                'language_code' => 'ne',
                'display_language_code' => 'is',
                'name' => 'Nepali',
            ),
            301 =>
            array (
                'id' => 2302,
                'language_code' => 'ne',
                'display_language_code' => 'it',
                'name' => 'Nepalese',
            ),
            302 =>
            array (
                'id' => 2303,
                'language_code' => 'ne',
                'display_language_code' => 'ja',
                'name' => 'ネパール語',
            ),
            303 =>
            array (
                'id' => 2304,
                'language_code' => 'ne',
                'display_language_code' => 'ko',
                'name' => '네팔어',
            ),
            304 =>
            array (
                'id' => 2305,
                'language_code' => 'ne',
                'display_language_code' => 'ku',
                'name' => 'Nepali',
            ),
            305 =>
            array (
                'id' => 2306,
                'language_code' => 'ne',
                'display_language_code' => 'lv',
                'name' => 'Nepali',
            ),
            306 =>
            array (
                'id' => 2307,
                'language_code' => 'ne',
                'display_language_code' => 'lt',
                'name' => 'Nepali',
            ),
            307 =>
            array (
                'id' => 2308,
                'language_code' => 'ne',
                'display_language_code' => 'mk',
                'name' => 'Nepali',
            ),
            308 =>
            array (
                'id' => 2309,
                'language_code' => 'ne',
                'display_language_code' => 'mt',
                'name' => 'Nepali',
            ),
            309 =>
            array (
                'id' => 2310,
                'language_code' => 'ne',
                'display_language_code' => 'mn',
                'name' => 'Nepali',
            ),
            310 =>
            array (
                'id' => 2311,
                'language_code' => 'ne',
                'display_language_code' => 'ne',
                'name' => 'Nepali',
            ),
            311 =>
            array (
                'id' => 2312,
                'language_code' => 'ne',
                'display_language_code' => 'nl',
                'name' => 'Nepalees',
            ),
            312 =>
            array (
                'id' => 2313,
                'language_code' => 'ne',
                'display_language_code' => 'no',
                'name' => 'Nepali',
            ),
            313 =>
            array (
                'id' => 2314,
                'language_code' => 'ne',
                'display_language_code' => 'pa',
                'name' => 'Nepali',
            ),
            314 =>
            array (
                'id' => 2315,
                'language_code' => 'ne',
                'display_language_code' => 'pl',
                'name' => 'Nepalski',
            ),
            315 =>
            array (
                'id' => 2316,
                'language_code' => 'ne',
                'display_language_code' => 'pt-pt',
                'name' => 'Nepali',
            ),
            316 =>
            array (
                'id' => 2317,
                'language_code' => 'ne',
                'display_language_code' => 'pt-br',
                'name' => 'Nepali',
            ),
            317 =>
            array (
                'id' => 2318,
                'language_code' => 'ne',
                'display_language_code' => 'qu',
                'name' => 'Nepali',
            ),
            318 =>
            array (
                'id' => 2319,
                'language_code' => 'ne',
                'display_language_code' => 'ro',
                'name' => 'Nepaleză',
            ),
            319 =>
            array (
                'id' => 2320,
                'language_code' => 'ne',
                'display_language_code' => 'ru',
                'name' => 'Непальский',
            ),
            320 =>
            array (
                'id' => 2321,
                'language_code' => 'ne',
                'display_language_code' => 'sl',
                'name' => 'Nepalščina',
            ),
            321 =>
            array (
                'id' => 2322,
                'language_code' => 'ne',
                'display_language_code' => 'so',
                'name' => 'Nepali',
            ),
            322 =>
            array (
                'id' => 2323,
                'language_code' => 'ne',
                'display_language_code' => 'sq',
                'name' => 'Nepali',
            ),
            323 =>
            array (
                'id' => 2324,
                'language_code' => 'ne',
                'display_language_code' => 'sr',
                'name' => 'непалски',
            ),
            324 =>
            array (
                'id' => 2325,
                'language_code' => 'ne',
                'display_language_code' => 'sv',
                'name' => 'Nepalesiska',
            ),
            325 =>
            array (
                'id' => 2326,
                'language_code' => 'ne',
                'display_language_code' => 'ta',
                'name' => 'Nepali',
            ),
            326 =>
            array (
                'id' => 2327,
                'language_code' => 'ne',
                'display_language_code' => 'th',
                'name' => 'เนปาล',
            ),
            327 =>
            array (
                'id' => 2328,
                'language_code' => 'ne',
                'display_language_code' => 'tr',
                'name' => 'Nepal dili',
            ),
            328 =>
            array (
                'id' => 2329,
                'language_code' => 'ne',
                'display_language_code' => 'uk',
                'name' => 'Nepali',
            ),
            329 =>
            array (
                'id' => 2330,
                'language_code' => 'ne',
                'display_language_code' => 'ur',
                'name' => 'Nepali',
            ),
            330 =>
            array (
                'id' => 2331,
                'language_code' => 'ne',
                'display_language_code' => 'uz',
                'name' => 'Nepali',
            ),
            331 =>
            array (
                'id' => 2332,
                'language_code' => 'ne',
                'display_language_code' => 'vi',
                'name' => 'Nepali',
            ),
            332 =>
            array (
                'id' => 2333,
                'language_code' => 'ne',
                'display_language_code' => 'yi',
                'name' => 'Nepali',
            ),
            333 =>
            array (
                'id' => 2334,
                'language_code' => 'ne',
                'display_language_code' => 'zh-hans',
                'name' => '尼泊尔语',
            ),
            334 =>
            array (
                'id' => 2335,
                'language_code' => 'ne',
                'display_language_code' => 'zu',
                'name' => 'Nepali',
            ),
            335 =>
            array (
                'id' => 2336,
                'language_code' => 'ne',
                'display_language_code' => 'zh-hant',
                'name' => '尼泊爾語',
            ),
            336 =>
            array (
                'id' => 2337,
                'language_code' => 'ne',
                'display_language_code' => 'ms',
                'name' => 'Nepali',
            ),
            337 =>
            array (
                'id' => 2338,
                'language_code' => 'ne',
                'display_language_code' => 'gl',
                'name' => 'Nepali',
            ),
            338 =>
            array (
                'id' => 2339,
                'language_code' => 'ne',
                'display_language_code' => 'bn',
                'name' => 'Nepali',
            ),
            339 =>
            array (
                'id' => 2340,
                'language_code' => 'ne',
                'display_language_code' => 'az',
                'name' => 'Nepali',
            ),
            340 =>
            array (
                'id' => 2341,
                'language_code' => 'nl',
                'display_language_code' => 'en',
                'name' => 'Dutch',
            ),
            341 =>
            array (
                'id' => 2342,
                'language_code' => 'nl',
                'display_language_code' => 'es',
                'name' => 'Holandés',
            ),
            342 =>
            array (
                'id' => 2343,
                'language_code' => 'nl',
                'display_language_code' => 'de',
                'name' => 'Niederländisch',
            ),
            343 =>
            array (
                'id' => 2344,
                'language_code' => 'nl',
                'display_language_code' => 'fr',
                'name' => 'Néerlandais',
            ),
            344 =>
            array (
                'id' => 2345,
                'language_code' => 'nl',
                'display_language_code' => 'ar',
                'name' => 'الهولندية',
            ),
            345 =>
            array (
                'id' => 2346,
                'language_code' => 'nl',
                'display_language_code' => 'bs',
                'name' => 'Dutch',
            ),
            346 =>
            array (
                'id' => 2347,
                'language_code' => 'nl',
                'display_language_code' => 'bg',
                'name' => 'Холандски',
            ),
            347 =>
            array (
                'id' => 2348,
                'language_code' => 'nl',
                'display_language_code' => 'ca',
                'name' => 'Dutch',
            ),
            348 =>
            array (
                'id' => 2349,
                'language_code' => 'nl',
                'display_language_code' => 'cs',
                'name' => 'Holandský',
            ),
            349 =>
            array (
                'id' => 2350,
                'language_code' => 'nl',
                'display_language_code' => 'sk',
                'name' => 'Holandčina',
            ),
            350 =>
            array (
                'id' => 2351,
                'language_code' => 'nl',
                'display_language_code' => 'cy',
                'name' => 'Dutch',
            ),
            351 =>
            array (
                'id' => 2352,
                'language_code' => 'nl',
                'display_language_code' => 'da',
                'name' => 'Dutch',
            ),
            352 =>
            array (
                'id' => 2353,
                'language_code' => 'nl',
                'display_language_code' => 'el',
                'name' => 'Ολλανδικά',
            ),
            353 =>
            array (
                'id' => 2354,
                'language_code' => 'nl',
                'display_language_code' => 'eo',
                'name' => 'Dutch',
            ),
            354 =>
            array (
                'id' => 2355,
                'language_code' => 'nl',
                'display_language_code' => 'et',
                'name' => 'Dutch',
            ),
            355 =>
            array (
                'id' => 2356,
                'language_code' => 'nl',
                'display_language_code' => 'eu',
                'name' => 'Dutch',
            ),
            356 =>
            array (
                'id' => 2357,
                'language_code' => 'nl',
                'display_language_code' => 'fa',
                'name' => 'Dutch',
            ),
            357 =>
            array (
                'id' => 2358,
                'language_code' => 'nl',
                'display_language_code' => 'fi',
                'name' => 'Hollanti',
            ),
            358 =>
            array (
                'id' => 2359,
                'language_code' => 'nl',
                'display_language_code' => 'ga',
                'name' => 'Dutch',
            ),
            359 =>
            array (
                'id' => 2360,
                'language_code' => 'nl',
                'display_language_code' => 'he',
                'name' => 'הולנדית',
            ),
            360 =>
            array (
                'id' => 2361,
                'language_code' => 'nl',
                'display_language_code' => 'hi',
                'name' => 'Dutch',
            ),
            361 =>
            array (
                'id' => 2362,
                'language_code' => 'nl',
                'display_language_code' => 'hr',
                'name' => 'Nizozemski',
            ),
            362 =>
            array (
                'id' => 2363,
                'language_code' => 'nl',
                'display_language_code' => 'hu',
                'name' => 'Holland',
            ),
            363 =>
            array (
                'id' => 2364,
                'language_code' => 'nl',
                'display_language_code' => 'hy',
                'name' => 'Dutch',
            ),
            364 =>
            array (
                'id' => 2365,
                'language_code' => 'nl',
                'display_language_code' => 'id',
                'name' => 'Dutch',
            ),
            365 =>
            array (
                'id' => 2366,
                'language_code' => 'nl',
                'display_language_code' => 'is',
                'name' => 'Dutch',
            ),
            366 =>
            array (
                'id' => 2367,
                'language_code' => 'nl',
                'display_language_code' => 'it',
                'name' => 'Olandese',
            ),
            367 =>
            array (
                'id' => 2368,
                'language_code' => 'nl',
                'display_language_code' => 'ja',
                'name' => 'オランダ語',
            ),
            368 =>
            array (
                'id' => 2369,
                'language_code' => 'nl',
                'display_language_code' => 'ko',
                'name' => '화란어',
            ),
            369 =>
            array (
                'id' => 2370,
                'language_code' => 'nl',
                'display_language_code' => 'ku',
                'name' => 'Dutch',
            ),
            370 =>
            array (
                'id' => 2371,
                'language_code' => 'nl',
                'display_language_code' => 'lv',
                'name' => 'Dutch',
            ),
            371 =>
            array (
                'id' => 2372,
                'language_code' => 'nl',
                'display_language_code' => 'lt',
                'name' => 'Dutch',
            ),
            372 =>
            array (
                'id' => 2373,
                'language_code' => 'nl',
                'display_language_code' => 'mk',
                'name' => 'Dutch',
            ),
            373 =>
            array (
                'id' => 2374,
                'language_code' => 'nl',
                'display_language_code' => 'mt',
                'name' => 'Dutch',
            ),
            374 =>
            array (
                'id' => 2375,
                'language_code' => 'nl',
                'display_language_code' => 'mn',
                'name' => 'Dutch',
            ),
            375 =>
            array (
                'id' => 2376,
                'language_code' => 'nl',
                'display_language_code' => 'ne',
                'name' => 'Dutch',
            ),
            376 =>
            array (
                'id' => 2377,
                'language_code' => 'nl',
                'display_language_code' => 'nl',
                'name' => 'Nederlands',
            ),
            377 =>
            array (
                'id' => 2378,
                'language_code' => 'nl',
                'display_language_code' => 'no',
                'name' => 'Nederlandsk',
            ),
            378 =>
            array (
                'id' => 2379,
                'language_code' => 'nl',
                'display_language_code' => 'pa',
                'name' => 'Dutch',
            ),
            379 =>
            array (
                'id' => 2380,
                'language_code' => 'nl',
                'display_language_code' => 'pl',
                'name' => 'Holenderski',
            ),
            380 =>
            array (
                'id' => 2381,
                'language_code' => 'nl',
                'display_language_code' => 'pt-pt',
                'name' => 'Holandês',
            ),
            381 =>
            array (
                'id' => 2382,
                'language_code' => 'nl',
                'display_language_code' => 'pt-br',
                'name' => 'Holandês',
            ),
            382 =>
            array (
                'id' => 2383,
                'language_code' => 'nl',
                'display_language_code' => 'qu',
                'name' => 'Dutch',
            ),
            383 =>
            array (
                'id' => 2384,
                'language_code' => 'nl',
                'display_language_code' => 'ro',
                'name' => 'Olaneză',
            ),
            384 =>
            array (
                'id' => 2385,
                'language_code' => 'nl',
                'display_language_code' => 'ru',
                'name' => 'Голландский',
            ),
            385 =>
            array (
                'id' => 2386,
                'language_code' => 'nl',
                'display_language_code' => 'sl',
                'name' => 'Nizozemščina',
            ),
            386 =>
            array (
                'id' => 2387,
                'language_code' => 'nl',
                'display_language_code' => 'so',
                'name' => 'Dutch',
            ),
            387 =>
            array (
                'id' => 2388,
                'language_code' => 'nl',
                'display_language_code' => 'sq',
                'name' => 'Dutch',
            ),
            388 =>
            array (
                'id' => 2389,
                'language_code' => 'nl',
                'display_language_code' => 'sr',
                'name' => 'холандски',
            ),
            389 =>
            array (
                'id' => 2390,
                'language_code' => 'nl',
                'display_language_code' => 'sv',
                'name' => 'Nederländska',
            ),
            390 =>
            array (
                'id' => 2391,
                'language_code' => 'nl',
                'display_language_code' => 'ta',
                'name' => 'Dutch',
            ),
            391 =>
            array (
                'id' => 2392,
                'language_code' => 'nl',
                'display_language_code' => 'th',
                'name' => 'ดัตช์',
            ),
            392 =>
            array (
                'id' => 2393,
                'language_code' => 'nl',
                'display_language_code' => 'tr',
                'name' => 'Hollandaca',
            ),
            393 =>
            array (
                'id' => 2394,
                'language_code' => 'nl',
                'display_language_code' => 'uk',
                'name' => 'Dutch',
            ),
            394 =>
            array (
                'id' => 2395,
                'language_code' => 'nl',
                'display_language_code' => 'ur',
                'name' => 'Dutch',
            ),
            395 =>
            array (
                'id' => 2396,
                'language_code' => 'nl',
                'display_language_code' => 'uz',
                'name' => 'Dutch',
            ),
            396 =>
            array (
                'id' => 2397,
                'language_code' => 'nl',
                'display_language_code' => 'vi',
                'name' => 'Dutch',
            ),
            397 =>
            array (
                'id' => 2398,
                'language_code' => 'nl',
                'display_language_code' => 'yi',
                'name' => 'Dutch',
            ),
            398 =>
            array (
                'id' => 2399,
                'language_code' => 'nl',
                'display_language_code' => 'zh-hans',
                'name' => '荷兰语',
            ),
            399 =>
            array (
                'id' => 2400,
                'language_code' => 'nl',
                'display_language_code' => 'zu',
                'name' => 'Dutch',
            ),
            400 =>
            array (
                'id' => 2401,
                'language_code' => 'nl',
                'display_language_code' => 'zh-hant',
                'name' => '荷蘭語',
            ),
            401 =>
            array (
                'id' => 2402,
                'language_code' => 'nl',
                'display_language_code' => 'ms',
                'name' => 'Dutch',
            ),
            402 =>
            array (
                'id' => 2403,
                'language_code' => 'nl',
                'display_language_code' => 'gl',
                'name' => 'Dutch',
            ),
            403 =>
            array (
                'id' => 2404,
                'language_code' => 'nl',
                'display_language_code' => 'bn',
                'name' => 'Dutch',
            ),
            404 =>
            array (
                'id' => 2405,
                'language_code' => 'nl',
                'display_language_code' => 'az',
                'name' => 'Dutch',
            ),
            405 =>
            array (
                'id' => 2406,
                'language_code' => 'no',
                'display_language_code' => 'en',
                'name' => 'Norwegian Bokmål',
            ),
            406 =>
            array (
                'id' => 2407,
                'language_code' => 'no',
                'display_language_code' => 'es',
                'name' => 'Bokmål',
            ),
            407 =>
            array (
                'id' => 2408,
                'language_code' => 'no',
                'display_language_code' => 'de',
            'name' => 'Norwegisch (Buchsprache)',
            ),
            408 =>
            array (
                'id' => 2409,
                'language_code' => 'no',
                'display_language_code' => 'fr',
                'name' => 'Norvégien Bokmål',
            ),
            409 =>
            array (
                'id' => 2410,
                'language_code' => 'no',
                'display_language_code' => 'ar',
                'name' => 'البوكمالية النرويجية',
            ),
            410 =>
            array (
                'id' => 2411,
                'language_code' => 'no',
                'display_language_code' => 'bs',
                'name' => 'Norwegian Bokmål',
            ),
            411 =>
            array (
                'id' => 2412,
                'language_code' => 'no',
                'display_language_code' => 'bg',
                'name' => 'Норвежки книжовен',
            ),
            412 =>
            array (
                'id' => 2413,
                'language_code' => 'no',
                'display_language_code' => 'ca',
                'name' => 'Norwegian Bokmål',
            ),
            413 =>
            array (
                'id' => 2414,
                'language_code' => 'no',
                'display_language_code' => 'cs',
                'name' => 'Norwegian bokmål',
            ),
            414 =>
            array (
                'id' => 2415,
                'language_code' => 'no',
                'display_language_code' => 'sk',
                'name' => 'Nórsky jazyk Bokmål',
            ),
            415 =>
            array (
                'id' => 2416,
                'language_code' => 'no',
                'display_language_code' => 'cy',
                'name' => 'Norwegian Bokmål',
            ),
            416 =>
            array (
                'id' => 2417,
                'language_code' => 'no',
                'display_language_code' => 'da',
                'name' => 'Norwegian Bokmål',
            ),
            417 =>
            array (
                'id' => 2418,
                'language_code' => 'no',
                'display_language_code' => 'el',
                'name' => 'Νορβηγικά',
            ),
            418 =>
            array (
                'id' => 2419,
                'language_code' => 'no',
                'display_language_code' => 'eo',
                'name' => 'Norwegian Bokmål',
            ),
            419 =>
            array (
                'id' => 2420,
                'language_code' => 'no',
                'display_language_code' => 'et',
                'name' => 'Norwegian Bokmål',
            ),
            420 =>
            array (
                'id' => 2421,
                'language_code' => 'no',
                'display_language_code' => 'eu',
                'name' => 'Norwegian Bokmål',
            ),
            421 =>
            array (
                'id' => 2422,
                'language_code' => 'no',
                'display_language_code' => 'fa',
                'name' => 'Norwegian Bokmål',
            ),
            422 =>
            array (
                'id' => 2423,
                'language_code' => 'no',
                'display_language_code' => 'fi',
                'name' => 'Kirjanorja',
            ),
            423 =>
            array (
                'id' => 2424,
                'language_code' => 'no',
                'display_language_code' => 'ga',
                'name' => 'Norwegian Bokmål',
            ),
            424 =>
            array (
                'id' => 2425,
                'language_code' => 'no',
                'display_language_code' => 'he',
                'name' => 'נורווגית',
            ),
            425 =>
            array (
                'id' => 2426,
                'language_code' => 'no',
                'display_language_code' => 'hi',
                'name' => 'Norwegian Bokmål',
            ),
            426 =>
            array (
                'id' => 2427,
                'language_code' => 'no',
                'display_language_code' => 'hr',
                'name' => 'Književni norveški',
            ),
            427 =>
            array (
                'id' => 2428,
                'language_code' => 'no',
                'display_language_code' => 'hu',
                'name' => 'Norvég bokmål',
            ),
            428 =>
            array (
                'id' => 2429,
                'language_code' => 'no',
                'display_language_code' => 'hy',
                'name' => 'Norwegian Bokmål',
            ),
            429 =>
            array (
                'id' => 2430,
                'language_code' => 'no',
                'display_language_code' => 'id',
                'name' => 'Norwegian Bokmål',
            ),
            430 =>
            array (
                'id' => 2431,
                'language_code' => 'no',
                'display_language_code' => 'is',
                'name' => 'Norwegian Bokmål',
            ),
            431 =>
            array (
                'id' => 2432,
                'language_code' => 'no',
                'display_language_code' => 'it',
                'name' => 'Norvegese Bokmål',
            ),
            432 =>
            array (
                'id' => 2433,
                'language_code' => 'no',
                'display_language_code' => 'ja',
                'name' => 'ノルウェー・ブークモール',
            ),
            433 =>
            array (
                'id' => 2434,
                'language_code' => 'no',
                'display_language_code' => 'ko',
                'name' => '노르웨이 보크말어',
            ),
            434 =>
            array (
                'id' => 2435,
                'language_code' => 'no',
                'display_language_code' => 'ku',
                'name' => 'Norwegian Bokmål',
            ),
            435 =>
            array (
                'id' => 2436,
                'language_code' => 'no',
                'display_language_code' => 'lv',
                'name' => 'Norwegian Bokmål',
            ),
            436 =>
            array (
                'id' => 2437,
                'language_code' => 'no',
                'display_language_code' => 'lt',
                'name' => 'Norwegian Bokmål',
            ),
            437 =>
            array (
                'id' => 2438,
                'language_code' => 'no',
                'display_language_code' => 'mk',
                'name' => 'Norwegian Bokmål',
            ),
            438 =>
            array (
                'id' => 2439,
                'language_code' => 'no',
                'display_language_code' => 'mt',
                'name' => 'Norwegian Bokmål',
            ),
            439 =>
            array (
                'id' => 2440,
                'language_code' => 'no',
                'display_language_code' => 'mn',
                'name' => 'Norwegian Bokmål',
            ),
            440 =>
            array (
                'id' => 2441,
                'language_code' => 'no',
                'display_language_code' => 'ne',
                'name' => 'Norwegian Bokmål',
            ),
            441 =>
            array (
                'id' => 2442,
                'language_code' => 'no',
                'display_language_code' => 'nl',
                'name' => 'Noors Bokmål',
            ),
            442 =>
            array (
                'id' => 2443,
                'language_code' => 'no',
                'display_language_code' => 'no',
                'name' => 'Norsk bokmål',
            ),
            443 =>
            array (
                'id' => 2444,
                'language_code' => 'no',
                'display_language_code' => 'pa',
                'name' => 'Norwegian Bokmål',
            ),
            444 =>
            array (
                'id' => 2445,
                'language_code' => 'no',
                'display_language_code' => 'pl',
                'name' => 'Norweski bokmål',
            ),
            445 =>
            array (
                'id' => 2446,
                'language_code' => 'no',
                'display_language_code' => 'pt-pt',
                'name' => 'Norueguês',
            ),
            446 =>
            array (
                'id' => 2447,
                'language_code' => 'no',
                'display_language_code' => 'pt-br',
                'name' => 'Norueguês',
            ),
            447 =>
            array (
                'id' => 2448,
                'language_code' => 'no',
                'display_language_code' => 'qu',
                'name' => 'Norwegian Bokmål',
            ),
            448 =>
            array (
                'id' => 2449,
                'language_code' => 'no',
                'display_language_code' => 'ro',
            'name' => 'Bokmål (norvegiană)',
            ),
            449 =>
            array (
                'id' => 2450,
                'language_code' => 'no',
                'display_language_code' => 'ru',
                'name' => 'Норвежский букмол',
            ),
            450 =>
            array (
                'id' => 2451,
                'language_code' => 'no',
                'display_language_code' => 'sl',
                'name' => 'Knjižna norveščina',
            ),
            451 =>
            array (
                'id' => 2452,
                'language_code' => 'no',
                'display_language_code' => 'so',
                'name' => 'Norwegian Bokmål',
            ),
            452 =>
            array (
                'id' => 2453,
                'language_code' => 'no',
                'display_language_code' => 'sq',
                'name' => 'Norwegian Bokmål',
            ),
            453 =>
            array (
                'id' => 2454,
                'language_code' => 'no',
                'display_language_code' => 'sr',
                'name' => 'Норвешки бокмал',
            ),
            454 =>
            array (
                'id' => 2455,
                'language_code' => 'no',
                'display_language_code' => 'sv',
                'name' => 'Norskt Bokmål',
            ),
            455 =>
            array (
                'id' => 2456,
                'language_code' => 'no',
                'display_language_code' => 'ta',
                'name' => 'Norwegian Bokmål',
            ),
            456 =>
            array (
                'id' => 2457,
                'language_code' => 'no',
                'display_language_code' => 'th',
                'name' => 'นอร์วิเจียนบอกมาล',
            ),
            457 =>
            array (
                'id' => 2458,
                'language_code' => 'no',
                'display_language_code' => 'tr',
                'name' => 'Bokmal Norveç dili',
            ),
            458 =>
            array (
                'id' => 2459,
                'language_code' => 'no',
                'display_language_code' => 'uk',
                'name' => 'Norwegian Bokmål',
            ),
            459 =>
            array (
                'id' => 2460,
                'language_code' => 'no',
                'display_language_code' => 'ur',
                'name' => 'Norwegian Bokmål',
            ),
            460 =>
            array (
                'id' => 2461,
                'language_code' => 'no',
                'display_language_code' => 'uz',
                'name' => 'Norwegian Bokmål',
            ),
            461 =>
            array (
                'id' => 2462,
                'language_code' => 'no',
                'display_language_code' => 'vi',
                'name' => 'Norwegian Bokmål',
            ),
            462 =>
            array (
                'id' => 2463,
                'language_code' => 'no',
                'display_language_code' => 'yi',
                'name' => 'Norwegian Bokmål',
            ),
            463 =>
            array (
                'id' => 2464,
                'language_code' => 'no',
                'display_language_code' => 'zh-hans',
                'name' => '挪威布克莫尔语',
            ),
            464 =>
            array (
                'id' => 2465,
                'language_code' => 'no',
                'display_language_code' => 'zu',
                'name' => 'Norwegian Bokmål',
            ),
            465 =>
            array (
                'id' => 2466,
                'language_code' => 'no',
                'display_language_code' => 'zh-hant',
                'name' => '挪威布克莫爾語',
            ),
            466 =>
            array (
                'id' => 2467,
                'language_code' => 'no',
                'display_language_code' => 'ms',
                'name' => 'Norwegian Bokmål',
            ),
            467 =>
            array (
                'id' => 2468,
                'language_code' => 'no',
                'display_language_code' => 'gl',
                'name' => 'Norwegian Bokmål',
            ),
            468 =>
            array (
                'id' => 2469,
                'language_code' => 'no',
                'display_language_code' => 'bn',
                'name' => 'Norwegian Bokmål',
            ),
            469 =>
            array (
                'id' => 2470,
                'language_code' => 'no',
                'display_language_code' => 'az',
                'name' => 'Norwegian Bokmål',
            ),
            470 =>
            array (
                'id' => 2471,
                'language_code' => 'pa',
                'display_language_code' => 'en',
                'name' => 'Punjabi',
            ),
            471 =>
            array (
                'id' => 2472,
                'language_code' => 'pa',
                'display_language_code' => 'es',
                'name' => 'Panyabí',
            ),
            472 =>
            array (
                'id' => 2473,
                'language_code' => 'pa',
                'display_language_code' => 'de',
                'name' => 'Pandschabi',
            ),
            473 =>
            array (
                'id' => 2474,
                'language_code' => 'pa',
                'display_language_code' => 'fr',
                'name' => 'Panjabi',
            ),
            474 =>
            array (
                'id' => 2475,
                'language_code' => 'pa',
                'display_language_code' => 'ar',
                'name' => 'البنجابية',
            ),
            475 =>
            array (
                'id' => 2476,
                'language_code' => 'pa',
                'display_language_code' => 'bs',
                'name' => 'Punjabi',
            ),
            476 =>
            array (
                'id' => 2477,
                'language_code' => 'pa',
                'display_language_code' => 'bg',
                'name' => 'Панджабски',
            ),
            477 =>
            array (
                'id' => 2478,
                'language_code' => 'pa',
                'display_language_code' => 'ca',
                'name' => 'Punjabi',
            ),
            478 =>
            array (
                'id' => 2479,
                'language_code' => 'pa',
                'display_language_code' => 'cs',
                'name' => 'Punjabi',
            ),
            479 =>
            array (
                'id' => 2480,
                'language_code' => 'pa',
                'display_language_code' => 'sk',
                'name' => 'Pandžábčina',
            ),
            480 =>
            array (
                'id' => 2481,
                'language_code' => 'pa',
                'display_language_code' => 'cy',
                'name' => 'Punjabi',
            ),
            481 =>
            array (
                'id' => 2482,
                'language_code' => 'pa',
                'display_language_code' => 'da',
                'name' => 'Punjabi',
            ),
            482 =>
            array (
                'id' => 2483,
                'language_code' => 'pa',
                'display_language_code' => 'el',
                'name' => 'Παντζάμπι',
            ),
            483 =>
            array (
                'id' => 2484,
                'language_code' => 'pa',
                'display_language_code' => 'eo',
                'name' => 'Punjabi',
            ),
            484 =>
            array (
                'id' => 2485,
                'language_code' => 'pa',
                'display_language_code' => 'et',
                'name' => 'Punjabi',
            ),
            485 =>
            array (
                'id' => 2486,
                'language_code' => 'pa',
                'display_language_code' => 'eu',
                'name' => 'Punjabi',
            ),
            486 =>
            array (
                'id' => 2487,
                'language_code' => 'pa',
                'display_language_code' => 'fa',
                'name' => 'Punjabi',
            ),
            487 =>
            array (
                'id' => 2488,
                'language_code' => 'pa',
                'display_language_code' => 'fi',
                'name' => 'Pandzabi',
            ),
            488 =>
            array (
                'id' => 2489,
                'language_code' => 'pa',
                'display_language_code' => 'ga',
                'name' => 'Punjabi',
            ),
            489 =>
            array (
                'id' => 2490,
                'language_code' => 'pa',
                'display_language_code' => 'he',
                'name' => 'פנג\'אבית',
            ),
            490 =>
            array (
                'id' => 2491,
                'language_code' => 'pa',
                'display_language_code' => 'hi',
                'name' => 'Punjabi',
            ),
            491 =>
            array (
                'id' => 2492,
                'language_code' => 'pa',
                'display_language_code' => 'hr',
                'name' => 'Pendžabljanin',
            ),
            492 =>
            array (
                'id' => 2493,
                'language_code' => 'pa',
                'display_language_code' => 'hu',
                'name' => 'Pandzsábi',
            ),
            493 =>
            array (
                'id' => 2494,
                'language_code' => 'pa',
                'display_language_code' => 'hy',
                'name' => 'Punjabi',
            ),
            494 =>
            array (
                'id' => 2495,
                'language_code' => 'pa',
                'display_language_code' => 'id',
                'name' => 'Punjabi',
            ),
            495 =>
            array (
                'id' => 2496,
                'language_code' => 'pa',
                'display_language_code' => 'is',
                'name' => 'Punjabi',
            ),
            496 =>
            array (
                'id' => 2497,
                'language_code' => 'pa',
                'display_language_code' => 'it',
                'name' => 'Panjabi',
            ),
            497 =>
            array (
                'id' => 2498,
                'language_code' => 'pa',
                'display_language_code' => 'ja',
                'name' => 'パンジャーブ語',
            ),
            498 =>
            array (
                'id' => 2499,
                'language_code' => 'pa',
                'display_language_code' => 'ko',
                'name' => '펀자브어',
            ),
            499 =>
            array (
                'id' => 2500,
                'language_code' => 'pa',
                'display_language_code' => 'ku',
                'name' => 'Punjabi',
            ),
        ));
        \DB::table('language_translations')->insert(array (
            0 =>
            array (
                'id' => 2501,
                'language_code' => 'pa',
                'display_language_code' => 'lv',
                'name' => 'Punjabi',
            ),
            1 =>
            array (
                'id' => 2502,
                'language_code' => 'pa',
                'display_language_code' => 'lt',
                'name' => 'Punjabi',
            ),
            2 =>
            array (
                'id' => 2503,
                'language_code' => 'pa',
                'display_language_code' => 'mk',
                'name' => 'Punjabi',
            ),
            3 =>
            array (
                'id' => 2504,
                'language_code' => 'pa',
                'display_language_code' => 'mt',
                'name' => 'Punjabi',
            ),
            4 =>
            array (
                'id' => 2505,
                'language_code' => 'pa',
                'display_language_code' => 'mn',
                'name' => 'Punjabi',
            ),
            5 =>
            array (
                'id' => 2506,
                'language_code' => 'pa',
                'display_language_code' => 'ne',
                'name' => 'Punjabi',
            ),
            6 =>
            array (
                'id' => 2507,
                'language_code' => 'pa',
                'display_language_code' => 'nl',
                'name' => 'Punjabi',
            ),
            7 =>
            array (
                'id' => 2508,
                'language_code' => 'pa',
                'display_language_code' => 'no',
                'name' => 'Punjabi',
            ),
            8 =>
            array (
                'id' => 2509,
                'language_code' => 'pa',
                'display_language_code' => 'pa',
                'name' => 'Punjabi',
            ),
            9 =>
            array (
                'id' => 2510,
                'language_code' => 'pa',
                'display_language_code' => 'pl',
                'name' => 'Pendżabi',
            ),
            10 =>
            array (
                'id' => 2511,
                'language_code' => 'pa',
                'display_language_code' => 'pt-pt',
                'name' => 'Panjabi',
            ),
            11 =>
            array (
                'id' => 2512,
                'language_code' => 'pa',
                'display_language_code' => 'pt-br',
                'name' => 'Panjabi',
            ),
            12 =>
            array (
                'id' => 2513,
                'language_code' => 'pa',
                'display_language_code' => 'qu',
                'name' => 'Punjabi',
            ),
            13 =>
            array (
                'id' => 2514,
                'language_code' => 'pa',
                'display_language_code' => 'ro',
                'name' => 'Punjabi',
            ),
            14 =>
            array (
                'id' => 2515,
                'language_code' => 'pa',
                'display_language_code' => 'ru',
                'name' => 'Панджаби',
            ),
            15 =>
            array (
                'id' => 2516,
                'language_code' => 'pa',
                'display_language_code' => 'sl',
                'name' => 'Pandžabščina',
            ),
            16 =>
            array (
                'id' => 2517,
                'language_code' => 'pa',
                'display_language_code' => 'so',
                'name' => 'Punjabi',
            ),
            17 =>
            array (
                'id' => 2518,
                'language_code' => 'pa',
                'display_language_code' => 'sq',
                'name' => 'Punjabi',
            ),
            18 =>
            array (
                'id' => 2519,
                'language_code' => 'pa',
                'display_language_code' => 'sr',
                'name' => 'панџаби',
            ),
            19 =>
            array (
                'id' => 2520,
                'language_code' => 'pa',
                'display_language_code' => 'sv',
                'name' => 'Punjabi',
            ),
            20 =>
            array (
                'id' => 2521,
                'language_code' => 'pa',
                'display_language_code' => 'ta',
                'name' => 'Punjabi',
            ),
            21 =>
            array (
                'id' => 2522,
                'language_code' => 'pa',
                'display_language_code' => 'th',
                'name' => 'ปัญจาบ',
            ),
            22 =>
            array (
                'id' => 2523,
                'language_code' => 'pa',
                'display_language_code' => 'tr',
                'name' => 'Pencapça',
            ),
            23 =>
            array (
                'id' => 2524,
                'language_code' => 'pa',
                'display_language_code' => 'uk',
                'name' => 'Punjabi',
            ),
            24 =>
            array (
                'id' => 2525,
                'language_code' => 'pa',
                'display_language_code' => 'ur',
                'name' => 'Punjabi',
            ),
            25 =>
            array (
                'id' => 2526,
                'language_code' => 'pa',
                'display_language_code' => 'uz',
                'name' => 'Punjabi',
            ),
            26 =>
            array (
                'id' => 2527,
                'language_code' => 'pa',
                'display_language_code' => 'vi',
                'name' => 'Punjabi',
            ),
            27 =>
            array (
                'id' => 2528,
                'language_code' => 'pa',
                'display_language_code' => 'yi',
                'name' => 'Punjabi',
            ),
            28 =>
            array (
                'id' => 2529,
                'language_code' => 'pa',
                'display_language_code' => 'zh-hans',
                'name' => '旁遮普语',
            ),
            29 =>
            array (
                'id' => 2530,
                'language_code' => 'pa',
                'display_language_code' => 'zu',
                'name' => 'Punjabi',
            ),
            30 =>
            array (
                'id' => 2531,
                'language_code' => 'pa',
                'display_language_code' => 'zh-hant',
                'name' => '旁遮普語',
            ),
            31 =>
            array (
                'id' => 2532,
                'language_code' => 'pa',
                'display_language_code' => 'ms',
                'name' => 'Punjabi',
            ),
            32 =>
            array (
                'id' => 2533,
                'language_code' => 'pa',
                'display_language_code' => 'gl',
                'name' => 'Punjabi',
            ),
            33 =>
            array (
                'id' => 2534,
                'language_code' => 'pa',
                'display_language_code' => 'bn',
                'name' => 'Punjabi',
            ),
            34 =>
            array (
                'id' => 2535,
                'language_code' => 'pa',
                'display_language_code' => 'az',
                'name' => 'Punjabi',
            ),
            35 =>
            array (
                'id' => 2536,
                'language_code' => 'pl',
                'display_language_code' => 'en',
                'name' => 'Polish',
            ),
            36 =>
            array (
                'id' => 2537,
                'language_code' => 'pl',
                'display_language_code' => 'es',
                'name' => 'Polaco',
            ),
            37 =>
            array (
                'id' => 2538,
                'language_code' => 'pl',
                'display_language_code' => 'de',
                'name' => 'Polnisch',
            ),
            38 =>
            array (
                'id' => 2539,
                'language_code' => 'pl',
                'display_language_code' => 'fr',
                'name' => 'Polonais',
            ),
            39 =>
            array (
                'id' => 2540,
                'language_code' => 'pl',
                'display_language_code' => 'ar',
                'name' => 'البولندية',
            ),
            40 =>
            array (
                'id' => 2541,
                'language_code' => 'pl',
                'display_language_code' => 'bs',
                'name' => 'Polish',
            ),
            41 =>
            array (
                'id' => 2542,
                'language_code' => 'pl',
                'display_language_code' => 'bg',
                'name' => 'Полски',
            ),
            42 =>
            array (
                'id' => 2543,
                'language_code' => 'pl',
                'display_language_code' => 'ca',
                'name' => 'Polish',
            ),
            43 =>
            array (
                'id' => 2544,
                'language_code' => 'pl',
                'display_language_code' => 'cs',
                'name' => 'Polský',
            ),
            44 =>
            array (
                'id' => 2545,
                'language_code' => 'pl',
                'display_language_code' => 'sk',
                'name' => 'Polština',
            ),
            45 =>
            array (
                'id' => 2546,
                'language_code' => 'pl',
                'display_language_code' => 'cy',
                'name' => 'Polish',
            ),
            46 =>
            array (
                'id' => 2547,
                'language_code' => 'pl',
                'display_language_code' => 'da',
                'name' => 'Polish',
            ),
            47 =>
            array (
                'id' => 2548,
                'language_code' => 'pl',
                'display_language_code' => 'el',
                'name' => 'Πολωνικά',
            ),
            48 =>
            array (
                'id' => 2549,
                'language_code' => 'pl',
                'display_language_code' => 'eo',
                'name' => 'Polish',
            ),
            49 =>
            array (
                'id' => 2550,
                'language_code' => 'pl',
                'display_language_code' => 'et',
                'name' => 'Polish',
            ),
            50 =>
            array (
                'id' => 2551,
                'language_code' => 'pl',
                'display_language_code' => 'eu',
                'name' => 'Polish',
            ),
            51 =>
            array (
                'id' => 2552,
                'language_code' => 'pl',
                'display_language_code' => 'fa',
                'name' => 'Polish',
            ),
            52 =>
            array (
                'id' => 2553,
                'language_code' => 'pl',
                'display_language_code' => 'fi',
                'name' => 'Puola',
            ),
            53 =>
            array (
                'id' => 2554,
                'language_code' => 'pl',
                'display_language_code' => 'ga',
                'name' => 'Polish',
            ),
            54 =>
            array (
                'id' => 2555,
                'language_code' => 'pl',
                'display_language_code' => 'he',
                'name' => 'פולנית',
            ),
            55 =>
            array (
                'id' => 2556,
                'language_code' => 'pl',
                'display_language_code' => 'hi',
                'name' => 'Polish',
            ),
            56 =>
            array (
                'id' => 2557,
                'language_code' => 'pl',
                'display_language_code' => 'hr',
                'name' => 'Poljski',
            ),
            57 =>
            array (
                'id' => 2558,
                'language_code' => 'pl',
                'display_language_code' => 'hu',
                'name' => 'Lengyel',
            ),
            58 =>
            array (
                'id' => 2559,
                'language_code' => 'pl',
                'display_language_code' => 'hy',
                'name' => 'Polish',
            ),
            59 =>
            array (
                'id' => 2560,
                'language_code' => 'pl',
                'display_language_code' => 'id',
                'name' => 'Polish',
            ),
            60 =>
            array (
                'id' => 2561,
                'language_code' => 'pl',
                'display_language_code' => 'is',
                'name' => 'Polish',
            ),
            61 =>
            array (
                'id' => 2562,
                'language_code' => 'pl',
                'display_language_code' => 'it',
                'name' => 'Polacco',
            ),
            62 =>
            array (
                'id' => 2563,
                'language_code' => 'pl',
                'display_language_code' => 'ja',
                'name' => 'ポーランド語',
            ),
            63 =>
            array (
                'id' => 2564,
                'language_code' => 'pl',
                'display_language_code' => 'ko',
                'name' => '폴란드어',
            ),
            64 =>
            array (
                'id' => 2565,
                'language_code' => 'pl',
                'display_language_code' => 'ku',
                'name' => 'Polish',
            ),
            65 =>
            array (
                'id' => 2566,
                'language_code' => 'pl',
                'display_language_code' => 'lv',
                'name' => 'Polish',
            ),
            66 =>
            array (
                'id' => 2567,
                'language_code' => 'pl',
                'display_language_code' => 'lt',
                'name' => 'Polish',
            ),
            67 =>
            array (
                'id' => 2568,
                'language_code' => 'pl',
                'display_language_code' => 'mk',
                'name' => 'Polish',
            ),
            68 =>
            array (
                'id' => 2569,
                'language_code' => 'pl',
                'display_language_code' => 'mt',
                'name' => 'Polish',
            ),
            69 =>
            array (
                'id' => 2570,
                'language_code' => 'pl',
                'display_language_code' => 'mn',
                'name' => 'Polish',
            ),
            70 =>
            array (
                'id' => 2571,
                'language_code' => 'pl',
                'display_language_code' => 'ne',
                'name' => 'Polish',
            ),
            71 =>
            array (
                'id' => 2572,
                'language_code' => 'pl',
                'display_language_code' => 'nl',
                'name' => 'Pools',
            ),
            72 =>
            array (
                'id' => 2573,
                'language_code' => 'pl',
                'display_language_code' => 'no',
                'name' => 'Polsk',
            ),
            73 =>
            array (
                'id' => 2574,
                'language_code' => 'pl',
                'display_language_code' => 'pa',
                'name' => 'Polish',
            ),
            74 =>
            array (
                'id' => 2575,
                'language_code' => 'pl',
                'display_language_code' => 'pl',
                'name' => 'Polski',
            ),
            75 =>
            array (
                'id' => 2576,
                'language_code' => 'pl',
                'display_language_code' => 'pt-pt',
                'name' => 'Polonês',
            ),
            76 =>
            array (
                'id' => 2577,
                'language_code' => 'pl',
                'display_language_code' => 'pt-br',
                'name' => 'Polonês',
            ),
            77 =>
            array (
                'id' => 2578,
                'language_code' => 'pl',
                'display_language_code' => 'qu',
                'name' => 'Polish',
            ),
            78 =>
            array (
                'id' => 2579,
                'language_code' => 'pl',
                'display_language_code' => 'ro',
                'name' => 'Poloneză',
            ),
            79 =>
            array (
                'id' => 2580,
                'language_code' => 'pl',
                'display_language_code' => 'ru',
                'name' => 'Польский',
            ),
            80 =>
            array (
                'id' => 2581,
                'language_code' => 'pl',
                'display_language_code' => 'sl',
                'name' => 'Poljski',
            ),
            81 =>
            array (
                'id' => 2582,
                'language_code' => 'pl',
                'display_language_code' => 'so',
                'name' => 'Polish',
            ),
            82 =>
            array (
                'id' => 2583,
                'language_code' => 'pl',
                'display_language_code' => 'sq',
                'name' => 'Polish',
            ),
            83 =>
            array (
                'id' => 2584,
                'language_code' => 'pl',
                'display_language_code' => 'sr',
                'name' => 'пољски',
            ),
            84 =>
            array (
                'id' => 2585,
                'language_code' => 'pl',
                'display_language_code' => 'sv',
                'name' => 'Polska',
            ),
            85 =>
            array (
                'id' => 2586,
                'language_code' => 'pl',
                'display_language_code' => 'ta',
                'name' => 'Polish',
            ),
            86 =>
            array (
                'id' => 2587,
                'language_code' => 'pl',
                'display_language_code' => 'th',
                'name' => 'โปแลนด์',
            ),
            87 =>
            array (
                'id' => 2588,
                'language_code' => 'pl',
                'display_language_code' => 'tr',
                'name' => 'Polonyaca',
            ),
            88 =>
            array (
                'id' => 2589,
                'language_code' => 'pl',
                'display_language_code' => 'uk',
                'name' => 'Polish',
            ),
            89 =>
            array (
                'id' => 2590,
                'language_code' => 'pl',
                'display_language_code' => 'ur',
                'name' => 'Polish',
            ),
            90 =>
            array (
                'id' => 2591,
                'language_code' => 'pl',
                'display_language_code' => 'uz',
                'name' => 'Polish',
            ),
            91 =>
            array (
                'id' => 2592,
                'language_code' => 'pl',
                'display_language_code' => 'vi',
                'name' => 'Polish',
            ),
            92 =>
            array (
                'id' => 2593,
                'language_code' => 'pl',
                'display_language_code' => 'yi',
                'name' => 'Polish',
            ),
            93 =>
            array (
                'id' => 2594,
                'language_code' => 'pl',
                'display_language_code' => 'zh-hans',
                'name' => '波兰语',
            ),
            94 =>
            array (
                'id' => 2595,
                'language_code' => 'pl',
                'display_language_code' => 'zu',
                'name' => 'Polish',
            ),
            95 =>
            array (
                'id' => 2596,
                'language_code' => 'pl',
                'display_language_code' => 'zh-hant',
                'name' => '波蘭語',
            ),
            96 =>
            array (
                'id' => 2597,
                'language_code' => 'pl',
                'display_language_code' => 'ms',
                'name' => 'Polish',
            ),
            97 =>
            array (
                'id' => 2598,
                'language_code' => 'pl',
                'display_language_code' => 'gl',
                'name' => 'Polish',
            ),
            98 =>
            array (
                'id' => 2599,
                'language_code' => 'pl',
                'display_language_code' => 'bn',
                'name' => 'Polish',
            ),
            99 =>
            array (
                'id' => 2600,
                'language_code' => 'pl',
                'display_language_code' => 'az',
                'name' => 'Polish',
            ),
            100 =>
            array (
                'id' => 2601,
                'language_code' => 'pt-pt',
                'display_language_code' => 'en',
            'name' => 'Portuguese (Portugal)',
            ),
            101 =>
            array (
                'id' => 2602,
                'language_code' => 'pt-pt',
                'display_language_code' => 'es',
                'name' => 'Portugués, Portugal',
            ),
            102 =>
            array (
                'id' => 2603,
                'language_code' => 'pt-pt',
                'display_language_code' => 'de',
                'name' => 'Portugiesisch, Portugal',
            ),
            103 =>
            array (
                'id' => 2604,
                'language_code' => 'pt-pt',
                'display_language_code' => 'fr',
                'name' => 'Portugais - du Portugal',
            ),
            104 =>
            array (
                'id' => 2605,
                'language_code' => 'pt-pt',
                'display_language_code' => 'ar',
                'name' => 'البرتغالية ، البرتغال',
            ),
            105 =>
            array (
                'id' => 2606,
                'language_code' => 'pt-pt',
                'display_language_code' => 'bs',
                'name' => 'Portuguese, Portugal',
            ),
            106 =>
            array (
                'id' => 2607,
                'language_code' => 'pt-pt',
                'display_language_code' => 'bg',
            'name' => 'Португалски (Португалия)',
            ),
            107 =>
            array (
                'id' => 2608,
                'language_code' => 'pt-pt',
                'display_language_code' => 'ca',
                'name' => 'Portuguese, Portugal',
            ),
            108 =>
            array (
                'id' => 2609,
                'language_code' => 'pt-pt',
                'display_language_code' => 'cs',
            'name' => 'Portugalština ( Portugalsko)',
            ),
            109 =>
            array (
                'id' => 2610,
                'language_code' => 'pt-pt',
                'display_language_code' => 'sk',
                'name' => 'Portugalština',
            ),
            110 =>
            array (
                'id' => 2611,
                'language_code' => 'pt-pt',
                'display_language_code' => 'cy',
                'name' => 'Portuguese, Portugal',
            ),
            111 =>
            array (
                'id' => 2612,
                'language_code' => 'pt-pt',
                'display_language_code' => 'da',
                'name' => 'Portuguese, Portugal',
            ),
            112 =>
            array (
                'id' => 2613,
                'language_code' => 'pt-pt',
                'display_language_code' => 'el',
                'name' => 'Πορτογαλικά',
            ),
            113 =>
            array (
                'id' => 2614,
                'language_code' => 'pt-pt',
                'display_language_code' => 'eo',
                'name' => 'Portuguese, Portugal',
            ),
            114 =>
            array (
                'id' => 2615,
                'language_code' => 'pt-pt',
                'display_language_code' => 'et',
                'name' => 'Portuguese, Portugal',
            ),
            115 =>
            array (
                'id' => 2616,
                'language_code' => 'pt-pt',
                'display_language_code' => 'eu',
                'name' => 'Portuguese, Portugal',
            ),
            116 =>
            array (
                'id' => 2617,
                'language_code' => 'pt-pt',
                'display_language_code' => 'fa',
                'name' => 'Portuguese, Portugal',
            ),
            117 =>
            array (
                'id' => 2618,
                'language_code' => 'pt-pt',
                'display_language_code' => 'fi',
                'name' => 'Portugali',
            ),
            118 =>
            array (
                'id' => 2619,
                'language_code' => 'pt-pt',
                'display_language_code' => 'ga',
                'name' => 'Portuguese, Portugal',
            ),
            119 =>
            array (
                'id' => 2620,
                'language_code' => 'pt-pt',
                'display_language_code' => 'he',
                'name' => 'פורטוגזית',
            ),
            120 =>
            array (
                'id' => 2621,
                'language_code' => 'pt-pt',
                'display_language_code' => 'hi',
                'name' => 'Portuguese, Portugal',
            ),
            121 =>
            array (
                'id' => 2622,
                'language_code' => 'pt-pt',
                'display_language_code' => 'hr',
            'name' => 'Portugalski (Portugal)',
            ),
            122 =>
            array (
                'id' => 2623,
                'language_code' => 'pt-pt',
                'display_language_code' => 'hu',
                'name' => 'Portugál',
            ),
            123 =>
            array (
                'id' => 2624,
                'language_code' => 'pt-pt',
                'display_language_code' => 'hy',
                'name' => 'Portuguese, Portugal',
            ),
            124 =>
            array (
                'id' => 2625,
                'language_code' => 'pt-pt',
                'display_language_code' => 'id',
                'name' => 'Portuguese, Portugal',
            ),
            125 =>
            array (
                'id' => 2626,
                'language_code' => 'pt-pt',
                'display_language_code' => 'is',
                'name' => 'Portuguese, Portugal',
            ),
            126 =>
            array (
                'id' => 2627,
                'language_code' => 'pt-pt',
                'display_language_code' => 'it',
                'name' => 'Portoghese, Portogallo',
            ),
            127 =>
            array (
                'id' => 2628,
                'language_code' => 'pt-pt',
                'display_language_code' => 'ja',
                'name' => 'ポルトガル語',
            ),
            128 =>
            array (
                'id' => 2629,
                'language_code' => 'pt-pt',
                'display_language_code' => 'ko',
                'name' => '포르투갈 포르투갈어',
            ),
            129 =>
            array (
                'id' => 2630,
                'language_code' => 'pt-pt',
                'display_language_code' => 'ku',
                'name' => 'Portuguese, Portugal',
            ),
            130 =>
            array (
                'id' => 2631,
                'language_code' => 'pt-pt',
                'display_language_code' => 'lv',
                'name' => 'Portuguese, Portugal',
            ),
            131 =>
            array (
                'id' => 2632,
                'language_code' => 'pt-pt',
                'display_language_code' => 'lt',
                'name' => 'Portuguese, Portugal',
            ),
            132 =>
            array (
                'id' => 2633,
                'language_code' => 'pt-pt',
                'display_language_code' => 'mk',
                'name' => 'Portuguese, Portugal',
            ),
            133 =>
            array (
                'id' => 2634,
                'language_code' => 'pt-pt',
                'display_language_code' => 'mt',
                'name' => 'Portuguese, Portugal',
            ),
            134 =>
            array (
                'id' => 2635,
                'language_code' => 'pt-pt',
                'display_language_code' => 'mn',
                'name' => 'Portuguese, Portugal',
            ),
            135 =>
            array (
                'id' => 2636,
                'language_code' => 'pt-pt',
                'display_language_code' => 'ne',
                'name' => 'Portuguese, Portugal',
            ),
            136 =>
            array (
                'id' => 2637,
                'language_code' => 'pt-pt',
                'display_language_code' => 'nl',
                'name' => 'Portugees, Portugal',
            ),
            137 =>
            array (
                'id' => 2638,
                'language_code' => 'pt-pt',
                'display_language_code' => 'no',
            'name' => 'Portugisisk (Portugal)',
            ),
            138 =>
            array (
                'id' => 2639,
                'language_code' => 'pt-pt',
                'display_language_code' => 'pa',
                'name' => 'Portuguese, Portugal',
            ),
            139 =>
            array (
                'id' => 2640,
                'language_code' => 'pt-pt',
                'display_language_code' => 'pl',
                'name' => 'Portugalski, Portugalia',
            ),
            140 =>
            array (
                'id' => 2641,
                'language_code' => 'pt-pt',
                'display_language_code' => 'pt-pt',
                'name' => 'Português',
            ),
            141 =>
            array (
                'id' => 2642,
                'language_code' => 'pt-pt',
                'display_language_code' => 'pt-br',
                'name' => 'Português',
            ),
            142 =>
            array (
                'id' => 2643,
                'language_code' => 'pt-pt',
                'display_language_code' => 'qu',
                'name' => 'Portuguese, Portugal',
            ),
            143 =>
            array (
                'id' => 2644,
                'language_code' => 'pt-pt',
                'display_language_code' => 'ro',
            'name' => 'Portugheză (Portugalia)',
            ),
            144 =>
            array (
                'id' => 2645,
                'language_code' => 'pt-pt',
                'display_language_code' => 'ru',
                'name' => 'Португальский, Португалия',
            ),
            145 =>
            array (
                'id' => 2646,
                'language_code' => 'pt-pt',
                'display_language_code' => 'sl',
            'name' => 'Portugalščina ( Portugalska )',
            ),
            146 =>
            array (
                'id' => 2647,
                'language_code' => 'pt-pt',
                'display_language_code' => 'so',
                'name' => 'Portuguese, Portugal',
            ),
            147 =>
            array (
                'id' => 2648,
                'language_code' => 'pt-pt',
                'display_language_code' => 'sq',
                'name' => 'Portuguese, Portugal',
            ),
            148 =>
            array (
                'id' => 2649,
                'language_code' => 'pt-pt',
                'display_language_code' => 'sr',
            'name' => 'Португалски (Португалија)',
            ),
            149 =>
            array (
                'id' => 2650,
                'language_code' => 'pt-pt',
                'display_language_code' => 'sv',
                'name' => 'Portugisiska, Portugal',
            ),
            150 =>
            array (
                'id' => 2651,
                'language_code' => 'pt-pt',
                'display_language_code' => 'ta',
                'name' => 'Portuguese, Portugal',
            ),
            151 =>
            array (
                'id' => 2652,
                'language_code' => 'pt-pt',
                'display_language_code' => 'th',
                'name' => 'โปรตุเกส',
            ),
            152 =>
            array (
                'id' => 2653,
                'language_code' => 'pt-pt',
                'display_language_code' => 'tr',
                'name' => 'Portekizce, Portekiz',
            ),
            153 =>
            array (
                'id' => 2654,
                'language_code' => 'pt-pt',
                'display_language_code' => 'uk',
                'name' => 'Portuguese, Portugal',
            ),
            154 =>
            array (
                'id' => 2655,
                'language_code' => 'pt-pt',
                'display_language_code' => 'ur',
                'name' => 'Portuguese, Portugal',
            ),
            155 =>
            array (
                'id' => 2656,
                'language_code' => 'pt-pt',
                'display_language_code' => 'uz',
                'name' => 'Portuguese, Portugal',
            ),
            156 =>
            array (
                'id' => 2657,
                'language_code' => 'pt-pt',
                'display_language_code' => 'vi',
                'name' => 'Portuguese, Portugal',
            ),
            157 =>
            array (
                'id' => 2658,
                'language_code' => 'pt-pt',
                'display_language_code' => 'yi',
                'name' => 'Portuguese, Portugal',
            ),
            158 =>
            array (
                'id' => 2659,
                'language_code' => 'pt-pt',
                'display_language_code' => 'zh-hans',
                'name' => '葡萄牙语（葡萄牙）',
            ),
            159 =>
            array (
                'id' => 2660,
                'language_code' => 'pt-pt',
                'display_language_code' => 'zu',
                'name' => 'Portuguese, Portugal',
            ),
            160 =>
            array (
                'id' => 2661,
                'language_code' => 'pt-pt',
                'display_language_code' => 'zh-hant',
                'name' => '葡萄牙語（葡萄牙）',
            ),
            161 =>
            array (
                'id' => 2662,
                'language_code' => 'pt-pt',
                'display_language_code' => 'ms',
                'name' => 'Portuguese, Portugal',
            ),
            162 =>
            array (
                'id' => 2663,
                'language_code' => 'pt-pt',
                'display_language_code' => 'gl',
                'name' => 'Portuguese, Portugal',
            ),
            163 =>
            array (
                'id' => 2664,
                'language_code' => 'pt-pt',
                'display_language_code' => 'bn',
                'name' => 'Portuguese, Portugal',
            ),
            164 =>
            array (
                'id' => 2665,
                'language_code' => 'pt-pt',
                'display_language_code' => 'az',
                'name' => 'Portuguese, Portugal',
            ),
            165 =>
            array (
                'id' => 2666,
                'language_code' => 'pt-br',
                'display_language_code' => 'en',
            'name' => 'Portuguese (Brazil)',
            ),
            166 =>
            array (
                'id' => 2667,
                'language_code' => 'pt-br',
                'display_language_code' => 'es',
                'name' => 'Portugués, Brasil',
            ),
            167 =>
            array (
                'id' => 2668,
                'language_code' => 'pt-br',
                'display_language_code' => 'de',
                'name' => 'Portugiesisch, Brasilien',
            ),
            168 =>
            array (
                'id' => 2669,
                'language_code' => 'pt-br',
                'display_language_code' => 'fr',
                'name' => 'Portugais - du Brésil',
            ),
            169 =>
            array (
                'id' => 2670,
                'language_code' => 'pt-br',
                'display_language_code' => 'ar',
                'name' => 'البرتغالية ،البرازيل',
            ),
            170 =>
            array (
                'id' => 2671,
                'language_code' => 'pt-br',
                'display_language_code' => 'bs',
                'name' => 'Portuguese, Brazil',
            ),
            171 =>
            array (
                'id' => 2672,
                'language_code' => 'pt-br',
                'display_language_code' => 'bg',
            'name' => 'Португалски (Бразилия)',
            ),
            172 =>
            array (
                'id' => 2673,
                'language_code' => 'pt-br',
                'display_language_code' => 'ca',
                'name' => 'Portuguese, Brazil',
            ),
            173 =>
            array (
                'id' => 2674,
                'language_code' => 'pt-br',
                'display_language_code' => 'cs',
            'name' => 'Portugalština ( Brazílie)',
            ),
            174 =>
            array (
                'id' => 2675,
                'language_code' => 'pt-br',
                'display_language_code' => 'sk',
                'name' => 'Brazílska Portugalština',
            ),
            175 =>
            array (
                'id' => 2676,
                'language_code' => 'pt-br',
                'display_language_code' => 'cy',
                'name' => 'Portuguese, Brazil',
            ),
            176 =>
            array (
                'id' => 2677,
                'language_code' => 'pt-br',
                'display_language_code' => 'da',
                'name' => 'Portuguese, Brazil',
            ),
            177 =>
            array (
                'id' => 2678,
                'language_code' => 'pt-br',
                'display_language_code' => 'el',
                'name' => 'Πορτογαλικά Βραζιλίας',
            ),
            178 =>
            array (
                'id' => 2679,
                'language_code' => 'pt-br',
                'display_language_code' => 'eo',
                'name' => 'Portuguese, Brazil',
            ),
            179 =>
            array (
                'id' => 2680,
                'language_code' => 'pt-br',
                'display_language_code' => 'et',
                'name' => 'Portuguese, Brazil',
            ),
            180 =>
            array (
                'id' => 2681,
                'language_code' => 'pt-br',
                'display_language_code' => 'eu',
                'name' => 'Portuguese, Brazil',
            ),
            181 =>
            array (
                'id' => 2682,
                'language_code' => 'pt-br',
                'display_language_code' => 'fa',
                'name' => 'Portuguese, Brazil',
            ),
            182 =>
            array (
                'id' => 2683,
                'language_code' => 'pt-br',
                'display_language_code' => 'fi',
                'name' => 'Brasilian portugali',
            ),
            183 =>
            array (
                'id' => 2684,
                'language_code' => 'pt-br',
                'display_language_code' => 'ga',
                'name' => 'Portuguese, Brazil',
            ),
            184 =>
            array (
                'id' => 2685,
                'language_code' => 'pt-br',
                'display_language_code' => 'he',
                'name' => 'פורטוגזית - ברזיל',
            ),
            185 =>
            array (
                'id' => 2686,
                'language_code' => 'pt-br',
                'display_language_code' => 'hi',
                'name' => 'Portuguese, Brazil',
            ),
            186 =>
            array (
                'id' => 2687,
                'language_code' => 'pt-br',
                'display_language_code' => 'hr',
            'name' => 'Portugalski (Brazil)',
            ),
            187 =>
            array (
                'id' => 2688,
                'language_code' => 'pt-br',
                'display_language_code' => 'hu',
                'name' => 'Brazil',
            ),
            188 =>
            array (
                'id' => 2689,
                'language_code' => 'pt-br',
                'display_language_code' => 'hy',
                'name' => 'Portuguese, Brazil',
            ),
            189 =>
            array (
                'id' => 2690,
                'language_code' => 'pt-br',
                'display_language_code' => 'id',
                'name' => 'Portuguese, Brazil',
            ),
            190 =>
            array (
                'id' => 2691,
                'language_code' => 'pt-br',
                'display_language_code' => 'is',
                'name' => 'Portuguese, Brazil',
            ),
            191 =>
            array (
                'id' => 2692,
                'language_code' => 'pt-br',
                'display_language_code' => 'it',
                'name' => 'Portoghese, Brasile',
            ),
            192 =>
            array (
                'id' => 2693,
                'language_code' => 'pt-br',
                'display_language_code' => 'ja',
                'name' => 'ポルトガル語（ブラジル）',
            ),
            193 =>
            array (
                'id' => 2694,
                'language_code' => 'pt-br',
                'display_language_code' => 'ko',
                'name' => '브라질 포르투갈어',
            ),
            194 =>
            array (
                'id' => 2695,
                'language_code' => 'pt-br',
                'display_language_code' => 'ku',
                'name' => 'Portuguese, Brazil',
            ),
            195 =>
            array (
                'id' => 2696,
                'language_code' => 'pt-br',
                'display_language_code' => 'lv',
                'name' => 'Portuguese, Brazil',
            ),
            196 =>
            array (
                'id' => 2697,
                'language_code' => 'pt-br',
                'display_language_code' => 'lt',
                'name' => 'Portuguese, Brazil',
            ),
            197 =>
            array (
                'id' => 2698,
                'language_code' => 'pt-br',
                'display_language_code' => 'mk',
                'name' => 'Portuguese, Brazil',
            ),
            198 =>
            array (
                'id' => 2699,
                'language_code' => 'pt-br',
                'display_language_code' => 'mt',
                'name' => 'Portuguese, Brazil',
            ),
            199 =>
            array (
                'id' => 2700,
                'language_code' => 'pt-br',
                'display_language_code' => 'mn',
                'name' => 'Portuguese, Brazil',
            ),
            200 =>
            array (
                'id' => 2701,
                'language_code' => 'pt-br',
                'display_language_code' => 'ne',
                'name' => 'Portuguese, Brazil',
            ),
            201 =>
            array (
                'id' => 2702,
                'language_code' => 'pt-br',
                'display_language_code' => 'nl',
                'name' => 'Portugees, Brazilië',
            ),
            202 =>
            array (
                'id' => 2703,
                'language_code' => 'pt-br',
                'display_language_code' => 'no',
            'name' => 'Portugisisk (Brasil)',
            ),
            203 =>
            array (
                'id' => 2704,
                'language_code' => 'pt-br',
                'display_language_code' => 'pa',
                'name' => 'Portuguese, Brazil',
            ),
            204 =>
            array (
                'id' => 2705,
                'language_code' => 'pt-br',
                'display_language_code' => 'pl',
                'name' => 'Portugalski, Brazylia',
            ),
            205 =>
            array (
                'id' => 2706,
                'language_code' => 'pt-br',
                'display_language_code' => 'pt-pt',
                'name' => 'Português',
            ),
            206 =>
            array (
                'id' => 2707,
                'language_code' => 'pt-br',
                'display_language_code' => 'pt-br',
                'name' => 'Português',
            ),
            207 =>
            array (
                'id' => 2708,
                'language_code' => 'pt-br',
                'display_language_code' => 'qu',
                'name' => 'Portuguese, Brazil',
            ),
            208 =>
            array (
                'id' => 2709,
                'language_code' => 'pt-br',
                'display_language_code' => 'ro',
            'name' => 'Portugheză (Brazilia)',
            ),
            209 =>
            array (
                'id' => 2710,
                'language_code' => 'pt-br',
                'display_language_code' => 'ru',
                'name' => 'Португальский, Бразилия',
            ),
            210 =>
            array (
                'id' => 2711,
                'language_code' => 'pt-br',
                'display_language_code' => 'sl',
            'name' => 'Portugalščina ( Brazilija )',
            ),
            211 =>
            array (
                'id' => 2712,
                'language_code' => 'pt-br',
                'display_language_code' => 'so',
                'name' => 'Portuguese, Brazil',
            ),
            212 =>
            array (
                'id' => 2713,
                'language_code' => 'pt-br',
                'display_language_code' => 'sq',
                'name' => 'Portuguese, Brazil',
            ),
            213 =>
            array (
                'id' => 2714,
                'language_code' => 'pt-br',
                'display_language_code' => 'sr',
            'name' => 'Португалски (Бразил)',
            ),
            214 =>
            array (
                'id' => 2715,
                'language_code' => 'pt-br',
                'display_language_code' => 'sv',
                'name' => 'Portugisiska, Brasilien',
            ),
            215 =>
            array (
                'id' => 2716,
                'language_code' => 'pt-br',
                'display_language_code' => 'ta',
                'name' => 'Portuguese, Brazil',
            ),
            216 =>
            array (
                'id' => 2717,
                'language_code' => 'pt-br',
                'display_language_code' => 'th',
                'name' => 'โปรตุเกสบราซิล',
            ),
            217 =>
            array (
                'id' => 2718,
                'language_code' => 'pt-br',
                'display_language_code' => 'tr',
                'name' => 'Portekizce, Brezilya',
            ),
            218 =>
            array (
                'id' => 2719,
                'language_code' => 'pt-br',
                'display_language_code' => 'uk',
                'name' => 'Portuguese, Brazil',
            ),
            219 =>
            array (
                'id' => 2720,
                'language_code' => 'pt-br',
                'display_language_code' => 'ur',
                'name' => 'Portuguese, Brazil',
            ),
            220 =>
            array (
                'id' => 2721,
                'language_code' => 'pt-br',
                'display_language_code' => 'uz',
                'name' => 'Portuguese, Brazil',
            ),
            221 =>
            array (
                'id' => 2722,
                'language_code' => 'pt-br',
                'display_language_code' => 'vi',
                'name' => 'Portuguese, Brazil',
            ),
            222 =>
            array (
                'id' => 2723,
                'language_code' => 'pt-br',
                'display_language_code' => 'yi',
                'name' => 'Portuguese, Brazil',
            ),
            223 =>
            array (
                'id' => 2724,
                'language_code' => 'pt-br',
                'display_language_code' => 'zh-hans',
                'name' => '葡萄牙语（巴西）',
            ),
            224 =>
            array (
                'id' => 2725,
                'language_code' => 'pt-br',
                'display_language_code' => 'zu',
                'name' => 'Portuguese, Brazil',
            ),
            225 =>
            array (
                'id' => 2726,
                'language_code' => 'pt-br',
                'display_language_code' => 'zh-hant',
                'name' => '葡萄牙語（巴西）',
            ),
            226 =>
            array (
                'id' => 2727,
                'language_code' => 'pt-br',
                'display_language_code' => 'ms',
                'name' => 'Portuguese, Brazil',
            ),
            227 =>
            array (
                'id' => 2728,
                'language_code' => 'pt-br',
                'display_language_code' => 'gl',
                'name' => 'Portuguese, Brazil',
            ),
            228 =>
            array (
                'id' => 2729,
                'language_code' => 'pt-br',
                'display_language_code' => 'bn',
                'name' => 'Portuguese, Brazil',
            ),
            229 =>
            array (
                'id' => 2730,
                'language_code' => 'pt-br',
                'display_language_code' => 'az',
                'name' => 'Portuguese, Brazil',
            ),
            230 =>
            array (
                'id' => 2731,
                'language_code' => 'qu',
                'display_language_code' => 'en',
                'name' => 'Quechua',
            ),
            231 =>
            array (
                'id' => 2732,
                'language_code' => 'qu',
                'display_language_code' => 'es',
                'name' => 'Quechua',
            ),
            232 =>
            array (
                'id' => 2733,
                'language_code' => 'qu',
                'display_language_code' => 'de',
                'name' => 'Quechua',
            ),
            233 =>
            array (
                'id' => 2734,
                'language_code' => 'qu',
                'display_language_code' => 'fr',
                'name' => 'Quechua',
            ),
            234 =>
            array (
                'id' => 2735,
                'language_code' => 'qu',
                'display_language_code' => 'ar',
                'name' => 'الكويتشوا',
            ),
            235 =>
            array (
                'id' => 2736,
                'language_code' => 'qu',
                'display_language_code' => 'bs',
                'name' => 'Quechua',
            ),
            236 =>
            array (
                'id' => 2737,
                'language_code' => 'qu',
                'display_language_code' => 'bg',
                'name' => 'Кечуа',
            ),
            237 =>
            array (
                'id' => 2738,
                'language_code' => 'qu',
                'display_language_code' => 'ca',
                'name' => 'Quechua',
            ),
            238 =>
            array (
                'id' => 2739,
                'language_code' => 'qu',
                'display_language_code' => 'cs',
                'name' => 'Quechua',
            ),
            239 =>
            array (
                'id' => 2740,
                'language_code' => 'qu',
                'display_language_code' => 'sk',
                'name' => 'Jazyk Quechua',
            ),
            240 =>
            array (
                'id' => 2741,
                'language_code' => 'qu',
                'display_language_code' => 'cy',
                'name' => 'Quechua',
            ),
            241 =>
            array (
                'id' => 2742,
                'language_code' => 'qu',
                'display_language_code' => 'da',
                'name' => 'Quechua',
            ),
            242 =>
            array (
                'id' => 2743,
                'language_code' => 'qu',
                'display_language_code' => 'el',
                'name' => 'Κέτσουα',
            ),
            243 =>
            array (
                'id' => 2744,
                'language_code' => 'qu',
                'display_language_code' => 'eo',
                'name' => 'Quechua',
            ),
            244 =>
            array (
                'id' => 2745,
                'language_code' => 'qu',
                'display_language_code' => 'et',
                'name' => 'Quechua',
            ),
            245 =>
            array (
                'id' => 2746,
                'language_code' => 'qu',
                'display_language_code' => 'eu',
                'name' => 'Quechua',
            ),
            246 =>
            array (
                'id' => 2747,
                'language_code' => 'qu',
                'display_language_code' => 'fa',
                'name' => 'Quechua',
            ),
            247 =>
            array (
                'id' => 2748,
                'language_code' => 'qu',
                'display_language_code' => 'fi',
                'name' => 'Ketsua',
            ),
            248 =>
            array (
                'id' => 2749,
                'language_code' => 'qu',
                'display_language_code' => 'ga',
                'name' => 'Quechua',
            ),
            249 =>
            array (
                'id' => 2750,
                'language_code' => 'qu',
                'display_language_code' => 'he',
                'name' => 'קצ\'ואה',
            ),
            250 =>
            array (
                'id' => 2751,
                'language_code' => 'qu',
                'display_language_code' => 'hi',
                'name' => 'Quechua',
            ),
            251 =>
            array (
                'id' => 2752,
                'language_code' => 'qu',
                'display_language_code' => 'hr',
                'name' => 'Quechua',
            ),
            252 =>
            array (
                'id' => 2753,
                'language_code' => 'qu',
                'display_language_code' => 'hu',
                'name' => 'Quechua',
            ),
            253 =>
            array (
                'id' => 2754,
                'language_code' => 'qu',
                'display_language_code' => 'hy',
                'name' => 'Quechua',
            ),
            254 =>
            array (
                'id' => 2755,
                'language_code' => 'qu',
                'display_language_code' => 'id',
                'name' => 'Quechua',
            ),
            255 =>
            array (
                'id' => 2756,
                'language_code' => 'qu',
                'display_language_code' => 'is',
                'name' => 'Quechua',
            ),
            256 =>
            array (
                'id' => 2757,
                'language_code' => 'qu',
                'display_language_code' => 'it',
                'name' => 'Quechua',
            ),
            257 =>
            array (
                'id' => 2758,
                'language_code' => 'qu',
                'display_language_code' => 'ja',
                'name' => 'ケチュア語',
            ),
            258 =>
            array (
                'id' => 2759,
                'language_code' => 'qu',
                'display_language_code' => 'ko',
                'name' => '케추아어',
            ),
            259 =>
            array (
                'id' => 2760,
                'language_code' => 'qu',
                'display_language_code' => 'ku',
                'name' => 'Quechua',
            ),
            260 =>
            array (
                'id' => 2761,
                'language_code' => 'qu',
                'display_language_code' => 'lv',
                'name' => 'Quechua',
            ),
            261 =>
            array (
                'id' => 2762,
                'language_code' => 'qu',
                'display_language_code' => 'lt',
                'name' => 'Quechua',
            ),
            262 =>
            array (
                'id' => 2763,
                'language_code' => 'qu',
                'display_language_code' => 'mk',
                'name' => 'Quechua',
            ),
            263 =>
            array (
                'id' => 2764,
                'language_code' => 'qu',
                'display_language_code' => 'mt',
                'name' => 'Quechua',
            ),
            264 =>
            array (
                'id' => 2765,
                'language_code' => 'qu',
                'display_language_code' => 'mn',
                'name' => 'Quechua',
            ),
            265 =>
            array (
                'id' => 2766,
                'language_code' => 'qu',
                'display_language_code' => 'ne',
                'name' => 'Quechua',
            ),
            266 =>
            array (
                'id' => 2767,
                'language_code' => 'qu',
                'display_language_code' => 'nl',
                'name' => 'Quechua',
            ),
            267 =>
            array (
                'id' => 2768,
                'language_code' => 'qu',
                'display_language_code' => 'no',
                'name' => 'Quechua',
            ),
            268 =>
            array (
                'id' => 2769,
                'language_code' => 'qu',
                'display_language_code' => 'pa',
                'name' => 'Quechua',
            ),
            269 =>
            array (
                'id' => 2770,
                'language_code' => 'qu',
                'display_language_code' => 'pl',
                'name' => 'Keczua',
            ),
            270 =>
            array (
                'id' => 2771,
                'language_code' => 'qu',
                'display_language_code' => 'pt-pt',
                'name' => 'Quechua',
            ),
            271 =>
            array (
                'id' => 2772,
                'language_code' => 'qu',
                'display_language_code' => 'pt-br',
                'name' => 'Quechua',
            ),
            272 =>
            array (
                'id' => 2773,
                'language_code' => 'qu',
                'display_language_code' => 'qu',
                'name' => 'Quechua',
            ),
            273 =>
            array (
                'id' => 2774,
                'language_code' => 'qu',
                'display_language_code' => 'ro',
                'name' => 'Quechuană',
            ),
            274 =>
            array (
                'id' => 2775,
                'language_code' => 'qu',
                'display_language_code' => 'ru',
                'name' => 'Кечуа',
            ),
            275 =>
            array (
                'id' => 2776,
                'language_code' => 'qu',
                'display_language_code' => 'sl',
                'name' => 'Quechua',
            ),
            276 =>
            array (
                'id' => 2777,
                'language_code' => 'qu',
                'display_language_code' => 'so',
                'name' => 'Quechua',
            ),
            277 =>
            array (
                'id' => 2778,
                'language_code' => 'qu',
                'display_language_code' => 'sq',
                'name' => 'Quechua',
            ),
            278 =>
            array (
                'id' => 2779,
                'language_code' => 'qu',
                'display_language_code' => 'sr',
                'name' => 'Кечуа',
            ),
            279 =>
            array (
                'id' => 2780,
                'language_code' => 'qu',
                'display_language_code' => 'sv',
                'name' => 'Quechua',
            ),
            280 =>
            array (
                'id' => 2781,
                'language_code' => 'qu',
                'display_language_code' => 'ta',
                'name' => 'Quechua',
            ),
            281 =>
            array (
                'id' => 2782,
                'language_code' => 'qu',
                'display_language_code' => 'th',
                'name' => 'คิวชัว',
            ),
            282 =>
            array (
                'id' => 2783,
                'language_code' => 'qu',
                'display_language_code' => 'tr',
                'name' => 'Quechua dili',
            ),
            283 =>
            array (
                'id' => 2784,
                'language_code' => 'qu',
                'display_language_code' => 'uk',
                'name' => 'Quechua',
            ),
            284 =>
            array (
                'id' => 2785,
                'language_code' => 'qu',
                'display_language_code' => 'ur',
                'name' => 'Quechua',
            ),
            285 =>
            array (
                'id' => 2786,
                'language_code' => 'qu',
                'display_language_code' => 'uz',
                'name' => 'Quechua',
            ),
            286 =>
            array (
                'id' => 2787,
                'language_code' => 'qu',
                'display_language_code' => 'vi',
                'name' => 'Quechua',
            ),
            287 =>
            array (
                'id' => 2788,
                'language_code' => 'qu',
                'display_language_code' => 'yi',
                'name' => 'Quechua',
            ),
            288 =>
            array (
                'id' => 2789,
                'language_code' => 'qu',
                'display_language_code' => 'zh-hans',
                'name' => '盖丘亚语',
            ),
            289 =>
            array (
                'id' => 2790,
                'language_code' => 'qu',
                'display_language_code' => 'zu',
                'name' => 'Quechua',
            ),
            290 =>
            array (
                'id' => 2791,
                'language_code' => 'qu',
                'display_language_code' => 'zh-hant',
                'name' => '蓋丘亞語',
            ),
            291 =>
            array (
                'id' => 2792,
                'language_code' => 'qu',
                'display_language_code' => 'ms',
                'name' => 'Quechua',
            ),
            292 =>
            array (
                'id' => 2793,
                'language_code' => 'qu',
                'display_language_code' => 'gl',
                'name' => 'Quechua',
            ),
            293 =>
            array (
                'id' => 2794,
                'language_code' => 'qu',
                'display_language_code' => 'bn',
                'name' => 'Quechua',
            ),
            294 =>
            array (
                'id' => 2795,
                'language_code' => 'qu',
                'display_language_code' => 'az',
                'name' => 'Quechua',
            ),
            295 =>
            array (
                'id' => 2796,
                'language_code' => 'ro',
                'display_language_code' => 'en',
                'name' => 'Romanian',
            ),
            296 =>
            array (
                'id' => 2797,
                'language_code' => 'ro',
                'display_language_code' => 'es',
                'name' => 'Rumano',
            ),
            297 =>
            array (
                'id' => 2798,
                'language_code' => 'ro',
                'display_language_code' => 'de',
                'name' => 'Rumänisch',
            ),
            298 =>
            array (
                'id' => 2799,
                'language_code' => 'ro',
                'display_language_code' => 'fr',
                'name' => 'Roumain',
            ),
            299 =>
            array (
                'id' => 2800,
                'language_code' => 'ro',
                'display_language_code' => 'ar',
                'name' => 'الرومانية',
            ),
            300 =>
            array (
                'id' => 2801,
                'language_code' => 'ro',
                'display_language_code' => 'bs',
                'name' => 'Romanian',
            ),
            301 =>
            array (
                'id' => 2802,
                'language_code' => 'ro',
                'display_language_code' => 'bg',
                'name' => 'Румънски',
            ),
            302 =>
            array (
                'id' => 2803,
                'language_code' => 'ro',
                'display_language_code' => 'ca',
                'name' => 'Romanian',
            ),
            303 =>
            array (
                'id' => 2804,
                'language_code' => 'ro',
                'display_language_code' => 'cs',
                'name' => 'Rumunština',
            ),
            304 =>
            array (
                'id' => 2805,
                'language_code' => 'ro',
                'display_language_code' => 'sk',
                'name' => 'Rumunčina',
            ),
            305 =>
            array (
                'id' => 2806,
                'language_code' => 'ro',
                'display_language_code' => 'cy',
                'name' => 'Romanian',
            ),
            306 =>
            array (
                'id' => 2807,
                'language_code' => 'ro',
                'display_language_code' => 'da',
                'name' => 'Romanian',
            ),
            307 =>
            array (
                'id' => 2808,
                'language_code' => 'ro',
                'display_language_code' => 'el',
                'name' => 'Ρουμανικά',
            ),
            308 =>
            array (
                'id' => 2809,
                'language_code' => 'ro',
                'display_language_code' => 'eo',
                'name' => 'Romanian',
            ),
            309 =>
            array (
                'id' => 2810,
                'language_code' => 'ro',
                'display_language_code' => 'et',
                'name' => 'Romanian',
            ),
            310 =>
            array (
                'id' => 2811,
                'language_code' => 'ro',
                'display_language_code' => 'eu',
                'name' => 'Romanian',
            ),
            311 =>
            array (
                'id' => 2812,
                'language_code' => 'ro',
                'display_language_code' => 'fa',
                'name' => 'Romanian',
            ),
            312 =>
            array (
                'id' => 2813,
                'language_code' => 'ro',
                'display_language_code' => 'fi',
                'name' => 'Romania',
            ),
            313 =>
            array (
                'id' => 2814,
                'language_code' => 'ro',
                'display_language_code' => 'ga',
                'name' => 'Romanian',
            ),
            314 =>
            array (
                'id' => 2815,
                'language_code' => 'ro',
                'display_language_code' => 'he',
                'name' => 'רומנית',
            ),
            315 =>
            array (
                'id' => 2816,
                'language_code' => 'ro',
                'display_language_code' => 'hi',
                'name' => 'Romanian',
            ),
            316 =>
            array (
                'id' => 2817,
                'language_code' => 'ro',
                'display_language_code' => 'hr',
                'name' => 'Rumunjski',
            ),
            317 =>
            array (
                'id' => 2818,
                'language_code' => 'ro',
                'display_language_code' => 'hu',
                'name' => 'Román',
            ),
            318 =>
            array (
                'id' => 2819,
                'language_code' => 'ro',
                'display_language_code' => 'hy',
                'name' => 'Romanian',
            ),
            319 =>
            array (
                'id' => 2820,
                'language_code' => 'ro',
                'display_language_code' => 'id',
                'name' => 'Romanian',
            ),
            320 =>
            array (
                'id' => 2821,
                'language_code' => 'ro',
                'display_language_code' => 'is',
                'name' => 'Romanian',
            ),
            321 =>
            array (
                'id' => 2822,
                'language_code' => 'ro',
                'display_language_code' => 'it',
                'name' => 'Rumeno',
            ),
            322 =>
            array (
                'id' => 2823,
                'language_code' => 'ro',
                'display_language_code' => 'ja',
                'name' => 'ルーマニア語',
            ),
            323 =>
            array (
                'id' => 2824,
                'language_code' => 'ro',
                'display_language_code' => 'ko',
                'name' => '로마니아어',
            ),
            324 =>
            array (
                'id' => 2825,
                'language_code' => 'ro',
                'display_language_code' => 'ku',
                'name' => 'Romanian',
            ),
            325 =>
            array (
                'id' => 2826,
                'language_code' => 'ro',
                'display_language_code' => 'lv',
                'name' => 'Romanian',
            ),
            326 =>
            array (
                'id' => 2827,
                'language_code' => 'ro',
                'display_language_code' => 'lt',
                'name' => 'Romanian',
            ),
            327 =>
            array (
                'id' => 2828,
                'language_code' => 'ro',
                'display_language_code' => 'mk',
                'name' => 'Romanian',
            ),
            328 =>
            array (
                'id' => 2829,
                'language_code' => 'ro',
                'display_language_code' => 'mt',
                'name' => 'Romanian',
            ),
            329 =>
            array (
                'id' => 2830,
                'language_code' => 'ro',
                'display_language_code' => 'mn',
                'name' => 'Romanian',
            ),
            330 =>
            array (
                'id' => 2831,
                'language_code' => 'ro',
                'display_language_code' => 'ne',
                'name' => 'Romanian',
            ),
            331 =>
            array (
                'id' => 2832,
                'language_code' => 'ro',
                'display_language_code' => 'nl',
                'name' => 'Roemeens',
            ),
            332 =>
            array (
                'id' => 2833,
                'language_code' => 'ro',
                'display_language_code' => 'no',
                'name' => 'Rumensk',
            ),
            333 =>
            array (
                'id' => 2834,
                'language_code' => 'ro',
                'display_language_code' => 'pa',
                'name' => 'Romanian',
            ),
            334 =>
            array (
                'id' => 2835,
                'language_code' => 'ro',
                'display_language_code' => 'pl',
                'name' => 'Rumuński',
            ),
            335 =>
            array (
                'id' => 2836,
                'language_code' => 'ro',
                'display_language_code' => 'pt-pt',
                'name' => 'Romeno',
            ),
            336 =>
            array (
                'id' => 2837,
                'language_code' => 'ro',
                'display_language_code' => 'pt-br',
                'name' => 'Romeno',
            ),
            337 =>
            array (
                'id' => 2838,
                'language_code' => 'ro',
                'display_language_code' => 'qu',
                'name' => 'Romanian',
            ),
            338 =>
            array (
                'id' => 2839,
                'language_code' => 'ro',
                'display_language_code' => 'ro',
                'name' => 'Română',
            ),
            339 =>
            array (
                'id' => 2840,
                'language_code' => 'ro',
                'display_language_code' => 'ru',
                'name' => 'Румынский',
            ),
            340 =>
            array (
                'id' => 2841,
                'language_code' => 'ro',
                'display_language_code' => 'sl',
                'name' => 'Romunščina',
            ),
            341 =>
            array (
                'id' => 2842,
                'language_code' => 'ro',
                'display_language_code' => 'so',
                'name' => 'Romanian',
            ),
            342 =>
            array (
                'id' => 2843,
                'language_code' => 'ro',
                'display_language_code' => 'sq',
                'name' => 'Romanian',
            ),
            343 =>
            array (
                'id' => 2844,
                'language_code' => 'ro',
                'display_language_code' => 'sr',
                'name' => 'румунски',
            ),
            344 =>
            array (
                'id' => 2845,
                'language_code' => 'ro',
                'display_language_code' => 'sv',
                'name' => 'Rumänska',
            ),
            345 =>
            array (
                'id' => 2846,
                'language_code' => 'ro',
                'display_language_code' => 'ta',
                'name' => 'Romanian',
            ),
            346 =>
            array (
                'id' => 2847,
                'language_code' => 'ro',
                'display_language_code' => 'th',
                'name' => 'โรมาเนีย',
            ),
            347 =>
            array (
                'id' => 2848,
                'language_code' => 'ro',
                'display_language_code' => 'tr',
                'name' => 'Rumence',
            ),
            348 =>
            array (
                'id' => 2849,
                'language_code' => 'ro',
                'display_language_code' => 'uk',
                'name' => 'Romanian',
            ),
            349 =>
            array (
                'id' => 2850,
                'language_code' => 'ro',
                'display_language_code' => 'ur',
                'name' => 'Romanian',
            ),
            350 =>
            array (
                'id' => 2851,
                'language_code' => 'ro',
                'display_language_code' => 'uz',
                'name' => 'Romanian',
            ),
            351 =>
            array (
                'id' => 2852,
                'language_code' => 'ro',
                'display_language_code' => 'vi',
                'name' => 'Romanian',
            ),
            352 =>
            array (
                'id' => 2853,
                'language_code' => 'ro',
                'display_language_code' => 'yi',
                'name' => 'Romanian',
            ),
            353 =>
            array (
                'id' => 2854,
                'language_code' => 'ro',
                'display_language_code' => 'zh-hans',
                'name' => '罗马尼亚语',
            ),
            354 =>
            array (
                'id' => 2855,
                'language_code' => 'ro',
                'display_language_code' => 'zu',
                'name' => 'Romanian',
            ),
            355 =>
            array (
                'id' => 2856,
                'language_code' => 'ro',
                'display_language_code' => 'zh-hant',
                'name' => '羅馬尼亞語',
            ),
            356 =>
            array (
                'id' => 2857,
                'language_code' => 'ro',
                'display_language_code' => 'ms',
                'name' => 'Romanian',
            ),
            357 =>
            array (
                'id' => 2858,
                'language_code' => 'ro',
                'display_language_code' => 'gl',
                'name' => 'Romanian',
            ),
            358 =>
            array (
                'id' => 2859,
                'language_code' => 'ro',
                'display_language_code' => 'bn',
                'name' => 'Romanian',
            ),
            359 =>
            array (
                'id' => 2860,
                'language_code' => 'ro',
                'display_language_code' => 'az',
                'name' => 'Romanian',
            ),
            360 =>
            array (
                'id' => 2861,
                'language_code' => 'ru',
                'display_language_code' => 'en',
                'name' => 'Russian',
            ),
            361 =>
            array (
                'id' => 2862,
                'language_code' => 'ru',
                'display_language_code' => 'es',
                'name' => 'Ruso',
            ),
            362 =>
            array (
                'id' => 2863,
                'language_code' => 'ru',
                'display_language_code' => 'de',
                'name' => 'Russisch',
            ),
            363 =>
            array (
                'id' => 2864,
                'language_code' => 'ru',
                'display_language_code' => 'fr',
                'name' => 'Russe',
            ),
            364 =>
            array (
                'id' => 2865,
                'language_code' => 'ru',
                'display_language_code' => 'ar',
                'name' => 'الروسية',
            ),
            365 =>
            array (
                'id' => 2866,
                'language_code' => 'ru',
                'display_language_code' => 'bs',
                'name' => 'Russian',
            ),
            366 =>
            array (
                'id' => 2867,
                'language_code' => 'ru',
                'display_language_code' => 'bg',
                'name' => 'Руски',
            ),
            367 =>
            array (
                'id' => 2868,
                'language_code' => 'ru',
                'display_language_code' => 'ca',
                'name' => 'Russian',
            ),
            368 =>
            array (
                'id' => 2869,
                'language_code' => 'ru',
                'display_language_code' => 'cs',
                'name' => 'Ruský',
            ),
            369 =>
            array (
                'id' => 2870,
                'language_code' => 'ru',
                'display_language_code' => 'sk',
                'name' => 'Ruština',
            ),
            370 =>
            array (
                'id' => 2871,
                'language_code' => 'ru',
                'display_language_code' => 'cy',
                'name' => 'Russian',
            ),
            371 =>
            array (
                'id' => 2872,
                'language_code' => 'ru',
                'display_language_code' => 'da',
                'name' => 'Russian',
            ),
            372 =>
            array (
                'id' => 2873,
                'language_code' => 'ru',
                'display_language_code' => 'el',
                'name' => 'Ρωσικά',
            ),
            373 =>
            array (
                'id' => 2874,
                'language_code' => 'ru',
                'display_language_code' => 'eo',
                'name' => 'Russian',
            ),
            374 =>
            array (
                'id' => 2875,
                'language_code' => 'ru',
                'display_language_code' => 'et',
                'name' => 'Russian',
            ),
            375 =>
            array (
                'id' => 2876,
                'language_code' => 'ru',
                'display_language_code' => 'eu',
                'name' => 'Russian',
            ),
            376 =>
            array (
                'id' => 2877,
                'language_code' => 'ru',
                'display_language_code' => 'fa',
                'name' => 'Russian',
            ),
            377 =>
            array (
                'id' => 2878,
                'language_code' => 'ru',
                'display_language_code' => 'fi',
                'name' => 'Venäjä',
            ),
            378 =>
            array (
                'id' => 2879,
                'language_code' => 'ru',
                'display_language_code' => 'ga',
                'name' => 'Russian',
            ),
            379 =>
            array (
                'id' => 2880,
                'language_code' => 'ru',
                'display_language_code' => 'he',
                'name' => 'רוסית',
            ),
            380 =>
            array (
                'id' => 2881,
                'language_code' => 'ru',
                'display_language_code' => 'hi',
                'name' => 'Russian',
            ),
            381 =>
            array (
                'id' => 2882,
                'language_code' => 'ru',
                'display_language_code' => 'hr',
                'name' => 'Ruski',
            ),
            382 =>
            array (
                'id' => 2883,
                'language_code' => 'ru',
                'display_language_code' => 'hu',
                'name' => 'Orosz',
            ),
            383 =>
            array (
                'id' => 2884,
                'language_code' => 'ru',
                'display_language_code' => 'hy',
                'name' => 'Russian',
            ),
            384 =>
            array (
                'id' => 2885,
                'language_code' => 'ru',
                'display_language_code' => 'id',
                'name' => 'Russian',
            ),
            385 =>
            array (
                'id' => 2886,
                'language_code' => 'ru',
                'display_language_code' => 'is',
                'name' => 'Russian',
            ),
            386 =>
            array (
                'id' => 2887,
                'language_code' => 'ru',
                'display_language_code' => 'it',
                'name' => 'Russo',
            ),
            387 =>
            array (
                'id' => 2888,
                'language_code' => 'ru',
                'display_language_code' => 'ja',
                'name' => 'ロシア語',
            ),
            388 =>
            array (
                'id' => 2889,
                'language_code' => 'ru',
                'display_language_code' => 'ko',
                'name' => '러시아어',
            ),
            389 =>
            array (
                'id' => 2890,
                'language_code' => 'ru',
                'display_language_code' => 'ku',
                'name' => 'Russian',
            ),
            390 =>
            array (
                'id' => 2891,
                'language_code' => 'ru',
                'display_language_code' => 'lv',
                'name' => 'Russian',
            ),
            391 =>
            array (
                'id' => 2892,
                'language_code' => 'ru',
                'display_language_code' => 'lt',
                'name' => 'Russian',
            ),
            392 =>
            array (
                'id' => 2893,
                'language_code' => 'ru',
                'display_language_code' => 'mk',
                'name' => 'Russian',
            ),
            393 =>
            array (
                'id' => 2894,
                'language_code' => 'ru',
                'display_language_code' => 'mt',
                'name' => 'Russian',
            ),
            394 =>
            array (
                'id' => 2895,
                'language_code' => 'ru',
                'display_language_code' => 'mn',
                'name' => 'Russian',
            ),
            395 =>
            array (
                'id' => 2896,
                'language_code' => 'ru',
                'display_language_code' => 'ne',
                'name' => 'Russian',
            ),
            396 =>
            array (
                'id' => 2897,
                'language_code' => 'ru',
                'display_language_code' => 'nl',
                'name' => 'Russisch',
            ),
            397 =>
            array (
                'id' => 2898,
                'language_code' => 'ru',
                'display_language_code' => 'no',
                'name' => 'Russisk',
            ),
            398 =>
            array (
                'id' => 2899,
                'language_code' => 'ru',
                'display_language_code' => 'pa',
                'name' => 'Russian',
            ),
            399 =>
            array (
                'id' => 2900,
                'language_code' => 'ru',
                'display_language_code' => 'pl',
                'name' => 'Rosyjski',
            ),
            400 =>
            array (
                'id' => 2901,
                'language_code' => 'ru',
                'display_language_code' => 'pt-pt',
                'name' => 'Russo',
            ),
            401 =>
            array (
                'id' => 2902,
                'language_code' => 'ru',
                'display_language_code' => 'pt-br',
                'name' => 'Russo',
            ),
            402 =>
            array (
                'id' => 2903,
                'language_code' => 'ru',
                'display_language_code' => 'qu',
                'name' => 'Russian',
            ),
            403 =>
            array (
                'id' => 2904,
                'language_code' => 'ru',
                'display_language_code' => 'ro',
                'name' => 'Rusă',
            ),
            404 =>
            array (
                'id' => 2905,
                'language_code' => 'ru',
                'display_language_code' => 'ru',
                'name' => 'Русский',
            ),
            405 =>
            array (
                'id' => 2906,
                'language_code' => 'ru',
                'display_language_code' => 'sl',
                'name' => 'Ruščina',
            ),
            406 =>
            array (
                'id' => 2907,
                'language_code' => 'ru',
                'display_language_code' => 'so',
                'name' => 'Russian',
            ),
            407 =>
            array (
                'id' => 2908,
                'language_code' => 'ru',
                'display_language_code' => 'sq',
                'name' => 'Russian',
            ),
            408 =>
            array (
                'id' => 2909,
                'language_code' => 'ru',
                'display_language_code' => 'sr',
                'name' => 'руски',
            ),
            409 =>
            array (
                'id' => 2910,
                'language_code' => 'ru',
                'display_language_code' => 'sv',
                'name' => 'Ryska',
            ),
            410 =>
            array (
                'id' => 2911,
                'language_code' => 'ru',
                'display_language_code' => 'ta',
                'name' => 'Russian',
            ),
            411 =>
            array (
                'id' => 2912,
                'language_code' => 'ru',
                'display_language_code' => 'th',
                'name' => 'รัสเซีย',
            ),
            412 =>
            array (
                'id' => 2913,
                'language_code' => 'ru',
                'display_language_code' => 'tr',
                'name' => 'Rusça',
            ),
            413 =>
            array (
                'id' => 2914,
                'language_code' => 'ru',
                'display_language_code' => 'uk',
                'name' => 'Russian',
            ),
            414 =>
            array (
                'id' => 2915,
                'language_code' => 'ru',
                'display_language_code' => 'ur',
                'name' => 'Russian',
            ),
            415 =>
            array (
                'id' => 2916,
                'language_code' => 'ru',
                'display_language_code' => 'uz',
                'name' => 'Russian',
            ),
            416 =>
            array (
                'id' => 2917,
                'language_code' => 'ru',
                'display_language_code' => 'vi',
                'name' => 'Russian',
            ),
            417 =>
            array (
                'id' => 2918,
                'language_code' => 'ru',
                'display_language_code' => 'yi',
                'name' => 'Russian',
            ),
            418 =>
            array (
                'id' => 2919,
                'language_code' => 'ru',
                'display_language_code' => 'zh-hans',
                'name' => '俄语',
            ),
            419 =>
            array (
                'id' => 2920,
                'language_code' => 'ru',
                'display_language_code' => 'zu',
                'name' => 'Russian',
            ),
            420 =>
            array (
                'id' => 2921,
                'language_code' => 'ru',
                'display_language_code' => 'zh-hant',
                'name' => '俄語',
            ),
            421 =>
            array (
                'id' => 2922,
                'language_code' => 'ru',
                'display_language_code' => 'ms',
                'name' => 'Russian',
            ),
            422 =>
            array (
                'id' => 2923,
                'language_code' => 'ru',
                'display_language_code' => 'gl',
                'name' => 'Russian',
            ),
            423 =>
            array (
                'id' => 2924,
                'language_code' => 'ru',
                'display_language_code' => 'bn',
                'name' => 'Russian',
            ),
            424 =>
            array (
                'id' => 2925,
                'language_code' => 'ru',
                'display_language_code' => 'az',
                'name' => 'Russian',
            ),
            425 =>
            array (
                'id' => 2926,
                'language_code' => 'sl',
                'display_language_code' => 'en',
                'name' => 'Slovenian',
            ),
            426 =>
            array (
                'id' => 2927,
                'language_code' => 'sl',
                'display_language_code' => 'es',
                'name' => 'Esloveno',
            ),
            427 =>
            array (
                'id' => 2928,
                'language_code' => 'sl',
                'display_language_code' => 'de',
                'name' => 'Slowenisch',
            ),
            428 =>
            array (
                'id' => 2929,
                'language_code' => 'sl',
                'display_language_code' => 'fr',
                'name' => 'Slovène',
            ),
            429 =>
            array (
                'id' => 2930,
                'language_code' => 'sl',
                'display_language_code' => 'ar',
                'name' => 'السلوفانية',
            ),
            430 =>
            array (
                'id' => 2931,
                'language_code' => 'sl',
                'display_language_code' => 'bs',
                'name' => 'Slovenian',
            ),
            431 =>
            array (
                'id' => 2932,
                'language_code' => 'sl',
                'display_language_code' => 'bg',
                'name' => 'Словенски',
            ),
            432 =>
            array (
                'id' => 2933,
                'language_code' => 'sl',
                'display_language_code' => 'ca',
                'name' => 'Slovenian',
            ),
            433 =>
            array (
                'id' => 2934,
                'language_code' => 'sl',
                'display_language_code' => 'cs',
                'name' => 'Slovinština',
            ),
            434 =>
            array (
                'id' => 2935,
                'language_code' => 'sl',
                'display_language_code' => 'sk',
                'name' => 'Slovinčina',
            ),
            435 =>
            array (
                'id' => 2936,
                'language_code' => 'sl',
                'display_language_code' => 'cy',
                'name' => 'Slovenian',
            ),
            436 =>
            array (
                'id' => 2937,
                'language_code' => 'sl',
                'display_language_code' => 'da',
                'name' => 'Slovenian',
            ),
            437 =>
            array (
                'id' => 2938,
                'language_code' => 'sl',
                'display_language_code' => 'el',
                'name' => 'Σλοβενικά',
            ),
            438 =>
            array (
                'id' => 2939,
                'language_code' => 'sl',
                'display_language_code' => 'eo',
                'name' => 'Slovenian',
            ),
            439 =>
            array (
                'id' => 2940,
                'language_code' => 'sl',
                'display_language_code' => 'et',
                'name' => 'Slovenian',
            ),
            440 =>
            array (
                'id' => 2941,
                'language_code' => 'sl',
                'display_language_code' => 'eu',
                'name' => 'Slovenian',
            ),
            441 =>
            array (
                'id' => 2942,
                'language_code' => 'sl',
                'display_language_code' => 'fa',
                'name' => 'Slovenian',
            ),
            442 =>
            array (
                'id' => 2943,
                'language_code' => 'sl',
                'display_language_code' => 'fi',
                'name' => 'Sloveeni',
            ),
            443 =>
            array (
                'id' => 2944,
                'language_code' => 'sl',
                'display_language_code' => 'ga',
                'name' => 'Slovenian',
            ),
            444 =>
            array (
                'id' => 2945,
                'language_code' => 'sl',
                'display_language_code' => 'he',
                'name' => 'סלובנית',
            ),
            445 =>
            array (
                'id' => 2946,
                'language_code' => 'sl',
                'display_language_code' => 'hi',
                'name' => 'Slovenian',
            ),
            446 =>
            array (
                'id' => 2947,
                'language_code' => 'sl',
                'display_language_code' => 'hr',
                'name' => 'Slovenski',
            ),
            447 =>
            array (
                'id' => 2948,
                'language_code' => 'sl',
                'display_language_code' => 'hu',
                'name' => 'Szlovén',
            ),
            448 =>
            array (
                'id' => 2949,
                'language_code' => 'sl',
                'display_language_code' => 'hy',
                'name' => 'Slovenian',
            ),
            449 =>
            array (
                'id' => 2950,
                'language_code' => 'sl',
                'display_language_code' => 'id',
                'name' => 'Slovenian',
            ),
            450 =>
            array (
                'id' => 2951,
                'language_code' => 'sl',
                'display_language_code' => 'is',
                'name' => 'Slovenian',
            ),
            451 =>
            array (
                'id' => 2952,
                'language_code' => 'sl',
                'display_language_code' => 'it',
                'name' => 'Sloveno',
            ),
            452 =>
            array (
                'id' => 2953,
                'language_code' => 'sl',
                'display_language_code' => 'ja',
                'name' => 'スロベニア語',
            ),
            453 =>
            array (
                'id' => 2954,
                'language_code' => 'sl',
                'display_language_code' => 'ko',
                'name' => '슬로베니아어',
            ),
            454 =>
            array (
                'id' => 2955,
                'language_code' => 'sl',
                'display_language_code' => 'ku',
                'name' => 'Slovenian',
            ),
            455 =>
            array (
                'id' => 2956,
                'language_code' => 'sl',
                'display_language_code' => 'lv',
                'name' => 'Slovenian',
            ),
            456 =>
            array (
                'id' => 2957,
                'language_code' => 'sl',
                'display_language_code' => 'lt',
                'name' => 'Slovenian',
            ),
            457 =>
            array (
                'id' => 2958,
                'language_code' => 'sl',
                'display_language_code' => 'mk',
                'name' => 'Slovenian',
            ),
            458 =>
            array (
                'id' => 2959,
                'language_code' => 'sl',
                'display_language_code' => 'mt',
                'name' => 'Slovenian',
            ),
            459 =>
            array (
                'id' => 2960,
                'language_code' => 'sl',
                'display_language_code' => 'mn',
                'name' => 'Slovenian',
            ),
            460 =>
            array (
                'id' => 2961,
                'language_code' => 'sl',
                'display_language_code' => 'ne',
                'name' => 'Slovenian',
            ),
            461 =>
            array (
                'id' => 2962,
                'language_code' => 'sl',
                'display_language_code' => 'nl',
                'name' => 'Sloveens',
            ),
            462 =>
            array (
                'id' => 2963,
                'language_code' => 'sl',
                'display_language_code' => 'no',
                'name' => 'Slovensk',
            ),
            463 =>
            array (
                'id' => 2964,
                'language_code' => 'sl',
                'display_language_code' => 'pa',
                'name' => 'Slovenian',
            ),
            464 =>
            array (
                'id' => 2965,
                'language_code' => 'sl',
                'display_language_code' => 'pl',
                'name' => 'Słoweński',
            ),
            465 =>
            array (
                'id' => 2966,
                'language_code' => 'sl',
                'display_language_code' => 'pt-pt',
                'name' => 'Esloveno',
            ),
            466 =>
            array (
                'id' => 2967,
                'language_code' => 'sl',
                'display_language_code' => 'pt-br',
                'name' => 'Esloveno',
            ),
            467 =>
            array (
                'id' => 2968,
                'language_code' => 'sl',
                'display_language_code' => 'qu',
                'name' => 'Slovenian',
            ),
            468 =>
            array (
                'id' => 2969,
                'language_code' => 'sl',
                'display_language_code' => 'ro',
                'name' => 'Slovenă',
            ),
            469 =>
            array (
                'id' => 2970,
                'language_code' => 'sl',
                'display_language_code' => 'ru',
                'name' => 'Словенский',
            ),
            470 =>
            array (
                'id' => 2971,
                'language_code' => 'sl',
                'display_language_code' => 'sl',
                'name' => 'Slovenščina',
            ),
            471 =>
            array (
                'id' => 2972,
                'language_code' => 'sl',
                'display_language_code' => 'so',
                'name' => 'Slovenian',
            ),
            472 =>
            array (
                'id' => 2973,
                'language_code' => 'sl',
                'display_language_code' => 'sq',
                'name' => 'Slovenian',
            ),
            473 =>
            array (
                'id' => 2974,
                'language_code' => 'sl',
                'display_language_code' => 'sr',
                'name' => 'словеначки',
            ),
            474 =>
            array (
                'id' => 2975,
                'language_code' => 'sl',
                'display_language_code' => 'sv',
                'name' => 'Slovenska',
            ),
            475 =>
            array (
                'id' => 2976,
                'language_code' => 'sl',
                'display_language_code' => 'ta',
                'name' => 'Slovenian',
            ),
            476 =>
            array (
                'id' => 2977,
                'language_code' => 'sl',
                'display_language_code' => 'th',
                'name' => 'สโลวีเนียน',
            ),
            477 =>
            array (
                'id' => 2978,
                'language_code' => 'sl',
                'display_language_code' => 'tr',
                'name' => 'Sloven dili',
            ),
            478 =>
            array (
                'id' => 2979,
                'language_code' => 'sl',
                'display_language_code' => 'uk',
                'name' => 'Slovenian',
            ),
            479 =>
            array (
                'id' => 2980,
                'language_code' => 'sl',
                'display_language_code' => 'ur',
                'name' => 'Slovenian',
            ),
            480 =>
            array (
                'id' => 2981,
                'language_code' => 'sl',
                'display_language_code' => 'uz',
                'name' => 'Slovenian',
            ),
            481 =>
            array (
                'id' => 2982,
                'language_code' => 'sl',
                'display_language_code' => 'vi',
                'name' => 'Slovenian',
            ),
            482 =>
            array (
                'id' => 2983,
                'language_code' => 'sl',
                'display_language_code' => 'yi',
                'name' => 'Slovenian',
            ),
            483 =>
            array (
                'id' => 2984,
                'language_code' => 'sl',
                'display_language_code' => 'zh-hans',
                'name' => '斯洛文尼亚语',
            ),
            484 =>
            array (
                'id' => 2985,
                'language_code' => 'sl',
                'display_language_code' => 'zu',
                'name' => 'Slovenian',
            ),
            485 =>
            array (
                'id' => 2986,
                'language_code' => 'sl',
                'display_language_code' => 'zh-hant',
                'name' => '斯洛文尼亞語',
            ),
            486 =>
            array (
                'id' => 2987,
                'language_code' => 'sl',
                'display_language_code' => 'ms',
                'name' => 'Slovenian',
            ),
            487 =>
            array (
                'id' => 2988,
                'language_code' => 'sl',
                'display_language_code' => 'gl',
                'name' => 'Slovenian',
            ),
            488 =>
            array (
                'id' => 2989,
                'language_code' => 'sl',
                'display_language_code' => 'bn',
                'name' => 'Slovenian',
            ),
            489 =>
            array (
                'id' => 2990,
                'language_code' => 'sl',
                'display_language_code' => 'az',
                'name' => 'Slovenian',
            ),
            490 =>
            array (
                'id' => 2991,
                'language_code' => 'so',
                'display_language_code' => 'en',
                'name' => 'Somali',
            ),
            491 =>
            array (
                'id' => 2992,
                'language_code' => 'so',
                'display_language_code' => 'es',
                'name' => 'Somalí',
            ),
            492 =>
            array (
                'id' => 2993,
                'language_code' => 'so',
                'display_language_code' => 'de',
                'name' => 'Somali',
            ),
            493 =>
            array (
                'id' => 2994,
                'language_code' => 'so',
                'display_language_code' => 'fr',
                'name' => 'Somali',
            ),
            494 =>
            array (
                'id' => 2995,
                'language_code' => 'so',
                'display_language_code' => 'ar',
                'name' => 'الصومالية',
            ),
            495 =>
            array (
                'id' => 2996,
                'language_code' => 'so',
                'display_language_code' => 'bs',
                'name' => 'Somali',
            ),
            496 =>
            array (
                'id' => 2997,
                'language_code' => 'so',
                'display_language_code' => 'bg',
                'name' => 'Сомалийски',
            ),
            497 =>
            array (
                'id' => 2998,
                'language_code' => 'so',
                'display_language_code' => 'ca',
                'name' => 'Somali',
            ),
            498 =>
            array (
                'id' => 2999,
                'language_code' => 'so',
                'display_language_code' => 'cs',
                'name' => 'Somali',
            ),
            499 =>
            array (
                'id' => 3000,
                'language_code' => 'so',
                'display_language_code' => 'sk',
                'name' => 'Somálčina',
            ),
        ));
        \DB::table('language_translations')->insert(array (
            0 =>
            array (
                'id' => 3001,
                'language_code' => 'so',
                'display_language_code' => 'cy',
                'name' => 'Somali',
            ),
            1 =>
            array (
                'id' => 3002,
                'language_code' => 'so',
                'display_language_code' => 'da',
                'name' => 'Somali',
            ),
            2 =>
            array (
                'id' => 3003,
                'language_code' => 'so',
                'display_language_code' => 'el',
                'name' => 'Σομαλικά',
            ),
            3 =>
            array (
                'id' => 3004,
                'language_code' => 'so',
                'display_language_code' => 'eo',
                'name' => 'Somali',
            ),
            4 =>
            array (
                'id' => 3005,
                'language_code' => 'so',
                'display_language_code' => 'et',
                'name' => 'Somali',
            ),
            5 =>
            array (
                'id' => 3006,
                'language_code' => 'so',
                'display_language_code' => 'eu',
                'name' => 'Somali',
            ),
            6 =>
            array (
                'id' => 3007,
                'language_code' => 'so',
                'display_language_code' => 'fa',
                'name' => 'Somali',
            ),
            7 =>
            array (
                'id' => 3008,
                'language_code' => 'so',
                'display_language_code' => 'fi',
                'name' => 'Somali',
            ),
            8 =>
            array (
                'id' => 3009,
                'language_code' => 'so',
                'display_language_code' => 'ga',
                'name' => 'Somali',
            ),
            9 =>
            array (
                'id' => 3010,
                'language_code' => 'so',
                'display_language_code' => 'he',
                'name' => 'סומלית',
            ),
            10 =>
            array (
                'id' => 3011,
                'language_code' => 'so',
                'display_language_code' => 'hi',
                'name' => 'Somali',
            ),
            11 =>
            array (
                'id' => 3012,
                'language_code' => 'so',
                'display_language_code' => 'hr',
                'name' => 'Somalski',
            ),
            12 =>
            array (
                'id' => 3013,
                'language_code' => 'so',
                'display_language_code' => 'hu',
                'name' => 'Szomáli',
            ),
            13 =>
            array (
                'id' => 3014,
                'language_code' => 'so',
                'display_language_code' => 'hy',
                'name' => 'Somali',
            ),
            14 =>
            array (
                'id' => 3015,
                'language_code' => 'so',
                'display_language_code' => 'id',
                'name' => 'Somali',
            ),
            15 =>
            array (
                'id' => 3016,
                'language_code' => 'so',
                'display_language_code' => 'is',
                'name' => 'Somali',
            ),
            16 =>
            array (
                'id' => 3017,
                'language_code' => 'so',
                'display_language_code' => 'it',
                'name' => 'Somalo',
            ),
            17 =>
            array (
                'id' => 3018,
                'language_code' => 'so',
                'display_language_code' => 'ja',
                'name' => 'ソマリ語',
            ),
            18 =>
            array (
                'id' => 3019,
                'language_code' => 'so',
                'display_language_code' => 'ko',
                'name' => '소말리아어',
            ),
            19 =>
            array (
                'id' => 3020,
                'language_code' => 'so',
                'display_language_code' => 'ku',
                'name' => 'Somali',
            ),
            20 =>
            array (
                'id' => 3021,
                'language_code' => 'so',
                'display_language_code' => 'lv',
                'name' => 'Somali',
            ),
            21 =>
            array (
                'id' => 3022,
                'language_code' => 'so',
                'display_language_code' => 'lt',
                'name' => 'Somali',
            ),
            22 =>
            array (
                'id' => 3023,
                'language_code' => 'so',
                'display_language_code' => 'mk',
                'name' => 'Somali',
            ),
            23 =>
            array (
                'id' => 3024,
                'language_code' => 'so',
                'display_language_code' => 'mt',
                'name' => 'Somali',
            ),
            24 =>
            array (
                'id' => 3025,
                'language_code' => 'so',
                'display_language_code' => 'mn',
                'name' => 'Somali',
            ),
            25 =>
            array (
                'id' => 3026,
                'language_code' => 'so',
                'display_language_code' => 'ne',
                'name' => 'Somali',
            ),
            26 =>
            array (
                'id' => 3027,
                'language_code' => 'so',
                'display_language_code' => 'nl',
                'name' => 'Somalisch',
            ),
            27 =>
            array (
                'id' => 3028,
                'language_code' => 'so',
                'display_language_code' => 'no',
                'name' => 'Somali',
            ),
            28 =>
            array (
                'id' => 3029,
                'language_code' => 'so',
                'display_language_code' => 'pa',
                'name' => 'Somali',
            ),
            29 =>
            array (
                'id' => 3030,
                'language_code' => 'so',
                'display_language_code' => 'pl',
                'name' => 'Somalijski',
            ),
            30 =>
            array (
                'id' => 3031,
                'language_code' => 'so',
                'display_language_code' => 'pt-pt',
                'name' => 'Somali',
            ),
            31 =>
            array (
                'id' => 3032,
                'language_code' => 'so',
                'display_language_code' => 'pt-br',
                'name' => 'Somali',
            ),
            32 =>
            array (
                'id' => 3033,
                'language_code' => 'so',
                'display_language_code' => 'qu',
                'name' => 'Somali',
            ),
            33 =>
            array (
                'id' => 3034,
                'language_code' => 'so',
                'display_language_code' => 'ro',
                'name' => 'Somaleză',
            ),
            34 =>
            array (
                'id' => 3035,
                'language_code' => 'so',
                'display_language_code' => 'ru',
                'name' => 'Сомалийский',
            ),
            35 =>
            array (
                'id' => 3036,
                'language_code' => 'so',
                'display_language_code' => 'sl',
                'name' => 'Somalski',
            ),
            36 =>
            array (
                'id' => 3037,
                'language_code' => 'so',
                'display_language_code' => 'so',
                'name' => 'Somali',
            ),
            37 =>
            array (
                'id' => 3038,
                'language_code' => 'so',
                'display_language_code' => 'sq',
                'name' => 'Somali',
            ),
            38 =>
            array (
                'id' => 3039,
                'language_code' => 'so',
                'display_language_code' => 'sr',
                'name' => 'Сомалијски',
            ),
            39 =>
            array (
                'id' => 3040,
                'language_code' => 'so',
                'display_language_code' => 'sv',
                'name' => 'Somaliska',
            ),
            40 =>
            array (
                'id' => 3041,
                'language_code' => 'so',
                'display_language_code' => 'ta',
                'name' => 'Somali',
            ),
            41 =>
            array (
                'id' => 3042,
                'language_code' => 'so',
                'display_language_code' => 'th',
                'name' => 'โซมาลี',
            ),
            42 =>
            array (
                'id' => 3043,
                'language_code' => 'so',
                'display_language_code' => 'tr',
                'name' => 'Somalice',
            ),
            43 =>
            array (
                'id' => 3044,
                'language_code' => 'so',
                'display_language_code' => 'uk',
                'name' => 'Somali',
            ),
            44 =>
            array (
                'id' => 3045,
                'language_code' => 'so',
                'display_language_code' => 'ur',
                'name' => 'Somali',
            ),
            45 =>
            array (
                'id' => 3046,
                'language_code' => 'so',
                'display_language_code' => 'uz',
                'name' => 'Somali',
            ),
            46 =>
            array (
                'id' => 3047,
                'language_code' => 'so',
                'display_language_code' => 'vi',
                'name' => 'Somali',
            ),
            47 =>
            array (
                'id' => 3048,
                'language_code' => 'so',
                'display_language_code' => 'yi',
                'name' => 'Somali',
            ),
            48 =>
            array (
                'id' => 3049,
                'language_code' => 'so',
                'display_language_code' => 'zh-hans',
                'name' => '索马里语',
            ),
            49 =>
            array (
                'id' => 3050,
                'language_code' => 'so',
                'display_language_code' => 'zu',
                'name' => 'Somali',
            ),
            50 =>
            array (
                'id' => 3051,
                'language_code' => 'so',
                'display_language_code' => 'zh-hant',
                'name' => '索馬里語',
            ),
            51 =>
            array (
                'id' => 3052,
                'language_code' => 'so',
                'display_language_code' => 'ms',
                'name' => 'Somali',
            ),
            52 =>
            array (
                'id' => 3053,
                'language_code' => 'so',
                'display_language_code' => 'gl',
                'name' => 'Somali',
            ),
            53 =>
            array (
                'id' => 3054,
                'language_code' => 'so',
                'display_language_code' => 'bn',
                'name' => 'Somali',
            ),
            54 =>
            array (
                'id' => 3055,
                'language_code' => 'so',
                'display_language_code' => 'az',
                'name' => 'Somali',
            ),
            55 =>
            array (
                'id' => 3056,
                'language_code' => 'sq',
                'display_language_code' => 'en',
                'name' => 'Albanian',
            ),
            56 =>
            array (
                'id' => 3057,
                'language_code' => 'sq',
                'display_language_code' => 'es',
                'name' => 'Albanés',
            ),
            57 =>
            array (
                'id' => 3058,
                'language_code' => 'sq',
                'display_language_code' => 'de',
                'name' => 'Albanisch',
            ),
            58 =>
            array (
                'id' => 3059,
                'language_code' => 'sq',
                'display_language_code' => 'fr',
                'name' => 'Albanais',
            ),
            59 =>
            array (
                'id' => 3060,
                'language_code' => 'sq',
                'display_language_code' => 'ar',
                'name' => 'الألبانية',
            ),
            60 =>
            array (
                'id' => 3061,
                'language_code' => 'sq',
                'display_language_code' => 'bs',
                'name' => 'Albanian',
            ),
            61 =>
            array (
                'id' => 3062,
                'language_code' => 'sq',
                'display_language_code' => 'bg',
                'name' => 'Албански',
            ),
            62 =>
            array (
                'id' => 3063,
                'language_code' => 'sq',
                'display_language_code' => 'ca',
                'name' => 'Albanian',
            ),
            63 =>
            array (
                'id' => 3064,
                'language_code' => 'sq',
                'display_language_code' => 'cs',
                'name' => 'Albánský',
            ),
            64 =>
            array (
                'id' => 3065,
                'language_code' => 'sq',
                'display_language_code' => 'sk',
                'name' => 'Albánčina',
            ),
            65 =>
            array (
                'id' => 3066,
                'language_code' => 'sq',
                'display_language_code' => 'cy',
                'name' => 'Albanian',
            ),
            66 =>
            array (
                'id' => 3067,
                'language_code' => 'sq',
                'display_language_code' => 'da',
                'name' => 'Albanian',
            ),
            67 =>
            array (
                'id' => 3068,
                'language_code' => 'sq',
                'display_language_code' => 'el',
                'name' => 'Αλβανικά',
            ),
            68 =>
            array (
                'id' => 3069,
                'language_code' => 'sq',
                'display_language_code' => 'eo',
                'name' => 'Albanian',
            ),
            69 =>
            array (
                'id' => 3070,
                'language_code' => 'sq',
                'display_language_code' => 'et',
                'name' => 'Albanian',
            ),
            70 =>
            array (
                'id' => 3071,
                'language_code' => 'sq',
                'display_language_code' => 'eu',
                'name' => 'Albanian',
            ),
            71 =>
            array (
                'id' => 3072,
                'language_code' => 'sq',
                'display_language_code' => 'fa',
                'name' => 'Albanian',
            ),
            72 =>
            array (
                'id' => 3073,
                'language_code' => 'sq',
                'display_language_code' => 'fi',
                'name' => 'Albania',
            ),
            73 =>
            array (
                'id' => 3074,
                'language_code' => 'sq',
                'display_language_code' => 'ga',
                'name' => 'Albanian',
            ),
            74 =>
            array (
                'id' => 3075,
                'language_code' => 'sq',
                'display_language_code' => 'he',
                'name' => 'אלבנית',
            ),
            75 =>
            array (
                'id' => 3076,
                'language_code' => 'sq',
                'display_language_code' => 'hi',
                'name' => 'Albanian',
            ),
            76 =>
            array (
                'id' => 3077,
                'language_code' => 'sq',
                'display_language_code' => 'hr',
                'name' => 'Albanski',
            ),
            77 =>
            array (
                'id' => 3078,
                'language_code' => 'sq',
                'display_language_code' => 'hu',
                'name' => 'Albán',
            ),
            78 =>
            array (
                'id' => 3079,
                'language_code' => 'sq',
                'display_language_code' => 'hy',
                'name' => 'Albanian',
            ),
            79 =>
            array (
                'id' => 3080,
                'language_code' => 'sq',
                'display_language_code' => 'id',
                'name' => 'Albanian',
            ),
            80 =>
            array (
                'id' => 3081,
                'language_code' => 'sq',
                'display_language_code' => 'is',
                'name' => 'Albanian',
            ),
            81 =>
            array (
                'id' => 3082,
                'language_code' => 'sq',
                'display_language_code' => 'it',
                'name' => 'Albanese',
            ),
            82 =>
            array (
                'id' => 3083,
                'language_code' => 'sq',
                'display_language_code' => 'ja',
                'name' => 'アルバニア語',
            ),
            83 =>
            array (
                'id' => 3084,
                'language_code' => 'sq',
                'display_language_code' => 'ko',
                'name' => '알바니아어',
            ),
            84 =>
            array (
                'id' => 3085,
                'language_code' => 'sq',
                'display_language_code' => 'ku',
                'name' => 'Albanian',
            ),
            85 =>
            array (
                'id' => 3086,
                'language_code' => 'sq',
                'display_language_code' => 'lv',
                'name' => 'Albanian',
            ),
            86 =>
            array (
                'id' => 3087,
                'language_code' => 'sq',
                'display_language_code' => 'lt',
                'name' => 'Albanian',
            ),
            87 =>
            array (
                'id' => 3088,
                'language_code' => 'sq',
                'display_language_code' => 'mk',
                'name' => 'Albanian',
            ),
            88 =>
            array (
                'id' => 3089,
                'language_code' => 'sq',
                'display_language_code' => 'mt',
                'name' => 'Albanian',
            ),
            89 =>
            array (
                'id' => 3090,
                'language_code' => 'sq',
                'display_language_code' => 'mn',
                'name' => 'Albanian',
            ),
            90 =>
            array (
                'id' => 3091,
                'language_code' => 'sq',
                'display_language_code' => 'ne',
                'name' => 'Albanian',
            ),
            91 =>
            array (
                'id' => 3092,
                'language_code' => 'sq',
                'display_language_code' => 'nl',
                'name' => 'Albaans',
            ),
            92 =>
            array (
                'id' => 3093,
                'language_code' => 'sq',
                'display_language_code' => 'no',
                'name' => 'Albansk',
            ),
            93 =>
            array (
                'id' => 3094,
                'language_code' => 'sq',
                'display_language_code' => 'pa',
                'name' => 'Albanian',
            ),
            94 =>
            array (
                'id' => 3095,
                'language_code' => 'sq',
                'display_language_code' => 'pl',
                'name' => 'Albański',
            ),
            95 =>
            array (
                'id' => 3096,
                'language_code' => 'sq',
                'display_language_code' => 'pt-pt',
                'name' => 'Albanês',
            ),
            96 =>
            array (
                'id' => 3097,
                'language_code' => 'sq',
                'display_language_code' => 'pt-br',
                'name' => 'Albanês',
            ),
            97 =>
            array (
                'id' => 3098,
                'language_code' => 'sq',
                'display_language_code' => 'qu',
                'name' => 'Albanian',
            ),
            98 =>
            array (
                'id' => 3099,
                'language_code' => 'sq',
                'display_language_code' => 'ro',
                'name' => 'Albaneză',
            ),
            99 =>
            array (
                'id' => 3100,
                'language_code' => 'sq',
                'display_language_code' => 'ru',
                'name' => 'Албанский',
            ),
            100 =>
            array (
                'id' => 3101,
                'language_code' => 'sq',
                'display_language_code' => 'sl',
                'name' => 'Albanski',
            ),
            101 =>
            array (
                'id' => 3102,
                'language_code' => 'sq',
                'display_language_code' => 'so',
                'name' => 'Albanian',
            ),
            102 =>
            array (
                'id' => 3103,
                'language_code' => 'sq',
                'display_language_code' => 'sq',
                'name' => 'Albanian',
            ),
            103 =>
            array (
                'id' => 3104,
                'language_code' => 'sq',
                'display_language_code' => 'sr',
                'name' => 'албански',
            ),
            104 =>
            array (
                'id' => 3105,
                'language_code' => 'sq',
                'display_language_code' => 'sv',
                'name' => 'Albanska',
            ),
            105 =>
            array (
                'id' => 3106,
                'language_code' => 'sq',
                'display_language_code' => 'ta',
                'name' => 'Albanian',
            ),
            106 =>
            array (
                'id' => 3107,
                'language_code' => 'sq',
                'display_language_code' => 'th',
                'name' => 'อัลเบเนีย',
            ),
            107 =>
            array (
                'id' => 3108,
                'language_code' => 'sq',
                'display_language_code' => 'tr',
                'name' => 'Arnavutça',
            ),
            108 =>
            array (
                'id' => 3109,
                'language_code' => 'sq',
                'display_language_code' => 'uk',
                'name' => 'Albanian',
            ),
            109 =>
            array (
                'id' => 3110,
                'language_code' => 'sq',
                'display_language_code' => 'ur',
                'name' => 'Albanian',
            ),
            110 =>
            array (
                'id' => 3111,
                'language_code' => 'sq',
                'display_language_code' => 'uz',
                'name' => 'Albanian',
            ),
            111 =>
            array (
                'id' => 3112,
                'language_code' => 'sq',
                'display_language_code' => 'vi',
                'name' => 'Albanian',
            ),
            112 =>
            array (
                'id' => 3113,
                'language_code' => 'sq',
                'display_language_code' => 'yi',
                'name' => 'Albanian',
            ),
            113 =>
            array (
                'id' => 3114,
                'language_code' => 'sq',
                'display_language_code' => 'zh-hans',
                'name' => '阿尔巴尼亚语',
            ),
            114 =>
            array (
                'id' => 3115,
                'language_code' => 'sq',
                'display_language_code' => 'zu',
                'name' => 'Albanian',
            ),
            115 =>
            array (
                'id' => 3116,
                'language_code' => 'sq',
                'display_language_code' => 'zh-hant',
                'name' => '阿爾巴尼亞語',
            ),
            116 =>
            array (
                'id' => 3117,
                'language_code' => 'sq',
                'display_language_code' => 'ms',
                'name' => 'Albanian',
            ),
            117 =>
            array (
                'id' => 3118,
                'language_code' => 'sq',
                'display_language_code' => 'gl',
                'name' => 'Albanian',
            ),
            118 =>
            array (
                'id' => 3119,
                'language_code' => 'sq',
                'display_language_code' => 'bn',
                'name' => 'Albanian',
            ),
            119 =>
            array (
                'id' => 3120,
                'language_code' => 'sq',
                'display_language_code' => 'az',
                'name' => 'Albanian',
            ),
            120 =>
            array (
                'id' => 3121,
                'language_code' => 'sr',
                'display_language_code' => 'en',
                'name' => 'Serbian',
            ),
            121 =>
            array (
                'id' => 3122,
                'language_code' => 'sr',
                'display_language_code' => 'es',
                'name' => 'Serbio',
            ),
            122 =>
            array (
                'id' => 3123,
                'language_code' => 'sr',
                'display_language_code' => 'de',
                'name' => 'Serbisch',
            ),
            123 =>
            array (
                'id' => 3124,
                'language_code' => 'sr',
                'display_language_code' => 'fr',
                'name' => 'Serbe',
            ),
            124 =>
            array (
                'id' => 3125,
                'language_code' => 'sr',
                'display_language_code' => 'ar',
                'name' => 'الصربية',
            ),
            125 =>
            array (
                'id' => 3126,
                'language_code' => 'sr',
                'display_language_code' => 'bs',
                'name' => 'Serbian',
            ),
            126 =>
            array (
                'id' => 3127,
                'language_code' => 'sr',
                'display_language_code' => 'bg',
                'name' => 'Сръбски',
            ),
            127 =>
            array (
                'id' => 3128,
                'language_code' => 'sr',
                'display_language_code' => 'ca',
                'name' => 'Serbian',
            ),
            128 =>
            array (
                'id' => 3129,
                'language_code' => 'sr',
                'display_language_code' => 'cs',
                'name' => 'Srbský',
            ),
            129 =>
            array (
                'id' => 3130,
                'language_code' => 'sr',
                'display_language_code' => 'sk',
                'name' => 'Srbština',
            ),
            130 =>
            array (
                'id' => 3131,
                'language_code' => 'sr',
                'display_language_code' => 'cy',
                'name' => 'Serbian',
            ),
            131 =>
            array (
                'id' => 3132,
                'language_code' => 'sr',
                'display_language_code' => 'da',
                'name' => 'Serbian',
            ),
            132 =>
            array (
                'id' => 3133,
                'language_code' => 'sr',
                'display_language_code' => 'el',
                'name' => 'Σερβικά',
            ),
            133 =>
            array (
                'id' => 3134,
                'language_code' => 'sr',
                'display_language_code' => 'eo',
                'name' => 'Serbian',
            ),
            134 =>
            array (
                'id' => 3135,
                'language_code' => 'sr',
                'display_language_code' => 'et',
                'name' => 'Serbian',
            ),
            135 =>
            array (
                'id' => 3136,
                'language_code' => 'sr',
                'display_language_code' => 'eu',
                'name' => 'Serbian',
            ),
            136 =>
            array (
                'id' => 3137,
                'language_code' => 'sr',
                'display_language_code' => 'fa',
                'name' => 'Serbian',
            ),
            137 =>
            array (
                'id' => 3138,
                'language_code' => 'sr',
                'display_language_code' => 'fi',
                'name' => 'Serbia',
            ),
            138 =>
            array (
                'id' => 3139,
                'language_code' => 'sr',
                'display_language_code' => 'ga',
                'name' => 'Serbian',
            ),
            139 =>
            array (
                'id' => 3140,
                'language_code' => 'sr',
                'display_language_code' => 'he',
                'name' => 'סרבית',
            ),
            140 =>
            array (
                'id' => 3141,
                'language_code' => 'sr',
                'display_language_code' => 'hi',
                'name' => 'Serbian',
            ),
            141 =>
            array (
                'id' => 3142,
                'language_code' => 'sr',
                'display_language_code' => 'hr',
                'name' => 'Srpski',
            ),
            142 =>
            array (
                'id' => 3143,
                'language_code' => 'sr',
                'display_language_code' => 'hu',
                'name' => 'Szerb',
            ),
            143 =>
            array (
                'id' => 3144,
                'language_code' => 'sr',
                'display_language_code' => 'hy',
                'name' => 'Serbian',
            ),
            144 =>
            array (
                'id' => 3145,
                'language_code' => 'sr',
                'display_language_code' => 'id',
                'name' => 'Serbian',
            ),
            145 =>
            array (
                'id' => 3146,
                'language_code' => 'sr',
                'display_language_code' => 'is',
                'name' => 'Serbian',
            ),
            146 =>
            array (
                'id' => 3147,
                'language_code' => 'sr',
                'display_language_code' => 'it',
                'name' => 'Serbo',
            ),
            147 =>
            array (
                'id' => 3148,
                'language_code' => 'sr',
                'display_language_code' => 'ja',
                'name' => 'セルビア語',
            ),
            148 =>
            array (
                'id' => 3149,
                'language_code' => 'sr',
                'display_language_code' => 'ko',
                'name' => '세르비아어',
            ),
            149 =>
            array (
                'id' => 3150,
                'language_code' => 'sr',
                'display_language_code' => 'ku',
                'name' => 'Serbian',
            ),
            150 =>
            array (
                'id' => 3151,
                'language_code' => 'sr',
                'display_language_code' => 'lv',
                'name' => 'Serbian',
            ),
            151 =>
            array (
                'id' => 3152,
                'language_code' => 'sr',
                'display_language_code' => 'lt',
                'name' => 'Serbian',
            ),
            152 =>
            array (
                'id' => 3153,
                'language_code' => 'sr',
                'display_language_code' => 'mk',
                'name' => 'Serbian',
            ),
            153 =>
            array (
                'id' => 3154,
                'language_code' => 'sr',
                'display_language_code' => 'mt',
                'name' => 'Serbian',
            ),
            154 =>
            array (
                'id' => 3155,
                'language_code' => 'sr',
                'display_language_code' => 'mn',
                'name' => 'Serbian',
            ),
            155 =>
            array (
                'id' => 3156,
                'language_code' => 'sr',
                'display_language_code' => 'ne',
                'name' => 'Serbian',
            ),
            156 =>
            array (
                'id' => 3157,
                'language_code' => 'sr',
                'display_language_code' => 'nl',
                'name' => 'Servisch',
            ),
            157 =>
            array (
                'id' => 3158,
                'language_code' => 'sr',
                'display_language_code' => 'no',
                'name' => 'Serbisk',
            ),
            158 =>
            array (
                'id' => 3159,
                'language_code' => 'sr',
                'display_language_code' => 'pa',
                'name' => 'Serbian',
            ),
            159 =>
            array (
                'id' => 3160,
                'language_code' => 'sr',
                'display_language_code' => 'pl',
                'name' => 'Serbski',
            ),
            160 =>
            array (
                'id' => 3161,
                'language_code' => 'sr',
                'display_language_code' => 'pt-pt',
                'name' => 'Sérvio',
            ),
            161 =>
            array (
                'id' => 3162,
                'language_code' => 'sr',
                'display_language_code' => 'pt-br',
                'name' => 'Sérvio',
            ),
            162 =>
            array (
                'id' => 3163,
                'language_code' => 'sr',
                'display_language_code' => 'qu',
                'name' => 'Serbian',
            ),
            163 =>
            array (
                'id' => 3164,
                'language_code' => 'sr',
                'display_language_code' => 'ro',
                'name' => 'Sârbă',
            ),
            164 =>
            array (
                'id' => 3165,
                'language_code' => 'sr',
                'display_language_code' => 'ru',
                'name' => 'Сербский',
            ),
            165 =>
            array (
                'id' => 3166,
                'language_code' => 'sr',
                'display_language_code' => 'sl',
                'name' => 'Srbski',
            ),
            166 =>
            array (
                'id' => 3167,
                'language_code' => 'sr',
                'display_language_code' => 'so',
                'name' => 'Serbian',
            ),
            167 =>
            array (
                'id' => 3168,
                'language_code' => 'sr',
                'display_language_code' => 'sq',
                'name' => 'Serbian',
            ),
            168 =>
            array (
                'id' => 3169,
                'language_code' => 'sr',
                'display_language_code' => 'sr',
                'name' => 'српски',
            ),
            169 =>
            array (
                'id' => 3170,
                'language_code' => 'sr',
                'display_language_code' => 'sv',
                'name' => 'Serbiska',
            ),
            170 =>
            array (
                'id' => 3171,
                'language_code' => 'sr',
                'display_language_code' => 'ta',
                'name' => 'Serbian',
            ),
            171 =>
            array (
                'id' => 3172,
                'language_code' => 'sr',
                'display_language_code' => 'th',
                'name' => 'เซอร์เบีย',
            ),
            172 =>
            array (
                'id' => 3173,
                'language_code' => 'sr',
                'display_language_code' => 'tr',
                'name' => 'Sırpça',
            ),
            173 =>
            array (
                'id' => 3174,
                'language_code' => 'sr',
                'display_language_code' => 'uk',
                'name' => 'Serbian',
            ),
            174 =>
            array (
                'id' => 3175,
                'language_code' => 'sr',
                'display_language_code' => 'ur',
                'name' => 'Serbian',
            ),
            175 =>
            array (
                'id' => 3176,
                'language_code' => 'sr',
                'display_language_code' => 'uz',
                'name' => 'Serbian',
            ),
            176 =>
            array (
                'id' => 3177,
                'language_code' => 'sr',
                'display_language_code' => 'vi',
                'name' => 'Serbian',
            ),
            177 =>
            array (
                'id' => 3178,
                'language_code' => 'sr',
                'display_language_code' => 'yi',
                'name' => 'Serbian',
            ),
            178 =>
            array (
                'id' => 3179,
                'language_code' => 'sr',
                'display_language_code' => 'zh-hans',
                'name' => '赛尔维亚语',
            ),
            179 =>
            array (
                'id' => 3180,
                'language_code' => 'sr',
                'display_language_code' => 'zu',
                'name' => 'Serbian',
            ),
            180 =>
            array (
                'id' => 3181,
                'language_code' => 'sr',
                'display_language_code' => 'zh-hant',
                'name' => '賽爾維亞語',
            ),
            181 =>
            array (
                'id' => 3182,
                'language_code' => 'sr',
                'display_language_code' => 'ms',
                'name' => 'Serbian',
            ),
            182 =>
            array (
                'id' => 3183,
                'language_code' => 'sr',
                'display_language_code' => 'gl',
                'name' => 'Serbian',
            ),
            183 =>
            array (
                'id' => 3184,
                'language_code' => 'sr',
                'display_language_code' => 'bn',
                'name' => 'Serbian',
            ),
            184 =>
            array (
                'id' => 3185,
                'language_code' => 'sr',
                'display_language_code' => 'az',
                'name' => 'Serbian',
            ),
            185 =>
            array (
                'id' => 3186,
                'language_code' => 'sv',
                'display_language_code' => 'en',
                'name' => 'Swedish',
            ),
            186 =>
            array (
                'id' => 3187,
                'language_code' => 'sv',
                'display_language_code' => 'es',
                'name' => 'Sueco',
            ),
            187 =>
            array (
                'id' => 3188,
                'language_code' => 'sv',
                'display_language_code' => 'de',
                'name' => 'Schwedisch',
            ),
            188 =>
            array (
                'id' => 3189,
                'language_code' => 'sv',
                'display_language_code' => 'fr',
                'name' => 'Suédois',
            ),
            189 =>
            array (
                'id' => 3190,
                'language_code' => 'sv',
                'display_language_code' => 'ar',
                'name' => 'السويدية',
            ),
            190 =>
            array (
                'id' => 3191,
                'language_code' => 'sv',
                'display_language_code' => 'bs',
                'name' => 'Swedish',
            ),
            191 =>
            array (
                'id' => 3192,
                'language_code' => 'sv',
                'display_language_code' => 'bg',
                'name' => 'Шведски',
            ),
            192 =>
            array (
                'id' => 3193,
                'language_code' => 'sv',
                'display_language_code' => 'ca',
                'name' => 'Swedish',
            ),
            193 =>
            array (
                'id' => 3194,
                'language_code' => 'sv',
                'display_language_code' => 'cs',
                'name' => 'Švédský',
            ),
            194 =>
            array (
                'id' => 3195,
                'language_code' => 'sv',
                'display_language_code' => 'sk',
                'name' => 'Švédština',
            ),
            195 =>
            array (
                'id' => 3196,
                'language_code' => 'sv',
                'display_language_code' => 'cy',
                'name' => 'Swedish',
            ),
            196 =>
            array (
                'id' => 3197,
                'language_code' => 'sv',
                'display_language_code' => 'da',
                'name' => 'Swedish',
            ),
            197 =>
            array (
                'id' => 3198,
                'language_code' => 'sv',
                'display_language_code' => 'el',
                'name' => 'Σουηδικά',
            ),
            198 =>
            array (
                'id' => 3199,
                'language_code' => 'sv',
                'display_language_code' => 'eo',
                'name' => 'Swedish',
            ),
            199 =>
            array (
                'id' => 3200,
                'language_code' => 'sv',
                'display_language_code' => 'et',
                'name' => 'Swedish',
            ),
            200 =>
            array (
                'id' => 3201,
                'language_code' => 'sv',
                'display_language_code' => 'eu',
                'name' => 'Swedish',
            ),
            201 =>
            array (
                'id' => 3202,
                'language_code' => 'sv',
                'display_language_code' => 'fa',
                'name' => 'Swedish',
            ),
            202 =>
            array (
                'id' => 3203,
                'language_code' => 'sv',
                'display_language_code' => 'fi',
                'name' => 'Ruotsi',
            ),
            203 =>
            array (
                'id' => 3204,
                'language_code' => 'sv',
                'display_language_code' => 'ga',
                'name' => 'Swedish',
            ),
            204 =>
            array (
                'id' => 3205,
                'language_code' => 'sv',
                'display_language_code' => 'he',
                'name' => 'שוודית',
            ),
            205 =>
            array (
                'id' => 3206,
                'language_code' => 'sv',
                'display_language_code' => 'hi',
                'name' => 'Swedish',
            ),
            206 =>
            array (
                'id' => 3207,
                'language_code' => 'sv',
                'display_language_code' => 'hr',
                'name' => 'švedski',
            ),
            207 =>
            array (
                'id' => 3208,
                'language_code' => 'sv',
                'display_language_code' => 'hu',
                'name' => 'Svéd',
            ),
            208 =>
            array (
                'id' => 3209,
                'language_code' => 'sv',
                'display_language_code' => 'hy',
                'name' => 'Swedish',
            ),
            209 =>
            array (
                'id' => 3210,
                'language_code' => 'sv',
                'display_language_code' => 'id',
                'name' => 'Swedish',
            ),
            210 =>
            array (
                'id' => 3211,
                'language_code' => 'sv',
                'display_language_code' => 'is',
                'name' => 'Swedish',
            ),
            211 =>
            array (
                'id' => 3212,
                'language_code' => 'sv',
                'display_language_code' => 'it',
                'name' => 'Svedese',
            ),
            212 =>
            array (
                'id' => 3213,
                'language_code' => 'sv',
                'display_language_code' => 'ja',
                'name' => 'スウェーデン語',
            ),
            213 =>
            array (
                'id' => 3214,
                'language_code' => 'sv',
                'display_language_code' => 'ko',
                'name' => '스웨덴어',
            ),
            214 =>
            array (
                'id' => 3215,
                'language_code' => 'sv',
                'display_language_code' => 'ku',
                'name' => 'Swedish',
            ),
            215 =>
            array (
                'id' => 3216,
                'language_code' => 'sv',
                'display_language_code' => 'lv',
                'name' => 'Swedish',
            ),
            216 =>
            array (
                'id' => 3217,
                'language_code' => 'sv',
                'display_language_code' => 'lt',
                'name' => 'Swedish',
            ),
            217 =>
            array (
                'id' => 3218,
                'language_code' => 'sv',
                'display_language_code' => 'mk',
                'name' => 'Swedish',
            ),
            218 =>
            array (
                'id' => 3219,
                'language_code' => 'sv',
                'display_language_code' => 'mt',
                'name' => 'Swedish',
            ),
            219 =>
            array (
                'id' => 3220,
                'language_code' => 'sv',
                'display_language_code' => 'mn',
                'name' => 'Swedish',
            ),
            220 =>
            array (
                'id' => 3221,
                'language_code' => 'sv',
                'display_language_code' => 'ne',
                'name' => 'Swedish',
            ),
            221 =>
            array (
                'id' => 3222,
                'language_code' => 'sv',
                'display_language_code' => 'nl',
                'name' => 'Zweeds',
            ),
            222 =>
            array (
                'id' => 3223,
                'language_code' => 'sv',
                'display_language_code' => 'no',
                'name' => 'Swedish',
            ),
            223 =>
            array (
                'id' => 3224,
                'language_code' => 'sv',
                'display_language_code' => 'pa',
                'name' => 'Swedish',
            ),
            224 =>
            array (
                'id' => 3225,
                'language_code' => 'sv',
                'display_language_code' => 'pl',
                'name' => 'Szwedzki',
            ),
            225 =>
            array (
                'id' => 3226,
                'language_code' => 'sv',
                'display_language_code' => 'pt-pt',
                'name' => 'Sueco',
            ),
            226 =>
            array (
                'id' => 3227,
                'language_code' => 'sv',
                'display_language_code' => 'pt-br',
                'name' => 'Sueco',
            ),
            227 =>
            array (
                'id' => 3228,
                'language_code' => 'sv',
                'display_language_code' => 'qu',
                'name' => 'Swedish',
            ),
            228 =>
            array (
                'id' => 3229,
                'language_code' => 'sv',
                'display_language_code' => 'ro',
                'name' => 'Suedeză',
            ),
            229 =>
            array (
                'id' => 3230,
                'language_code' => 'sv',
                'display_language_code' => 'ru',
                'name' => 'Шведский',
            ),
            230 =>
            array (
                'id' => 3231,
                'language_code' => 'sv',
                'display_language_code' => 'sl',
                'name' => 'Švedščina',
            ),
            231 =>
            array (
                'id' => 3232,
                'language_code' => 'sv',
                'display_language_code' => 'so',
                'name' => 'Swedish',
            ),
            232 =>
            array (
                'id' => 3233,
                'language_code' => 'sv',
                'display_language_code' => 'sq',
                'name' => 'Swedish',
            ),
            233 =>
            array (
                'id' => 3234,
                'language_code' => 'sv',
                'display_language_code' => 'sr',
                'name' => 'шведски',
            ),
            234 =>
            array (
                'id' => 3235,
                'language_code' => 'sv',
                'display_language_code' => 'sv',
                'name' => 'Svenska',
            ),
            235 =>
            array (
                'id' => 3236,
                'language_code' => 'sv',
                'display_language_code' => 'ta',
                'name' => 'Swedish',
            ),
            236 =>
            array (
                'id' => 3237,
                'language_code' => 'sv',
                'display_language_code' => 'th',
                'name' => 'สวีเดน',
            ),
            237 =>
            array (
                'id' => 3238,
                'language_code' => 'sv',
                'display_language_code' => 'tr',
                'name' => 'İsveççe',
            ),
            238 =>
            array (
                'id' => 3239,
                'language_code' => 'sv',
                'display_language_code' => 'uk',
                'name' => 'Swedish',
            ),
            239 =>
            array (
                'id' => 3240,
                'language_code' => 'sv',
                'display_language_code' => 'ur',
                'name' => 'Swedish',
            ),
            240 =>
            array (
                'id' => 3241,
                'language_code' => 'sv',
                'display_language_code' => 'uz',
                'name' => 'Swedish',
            ),
            241 =>
            array (
                'id' => 3242,
                'language_code' => 'sv',
                'display_language_code' => 'vi',
                'name' => 'Swedish',
            ),
            242 =>
            array (
                'id' => 3243,
                'language_code' => 'sv',
                'display_language_code' => 'yi',
                'name' => 'Swedish',
            ),
            243 =>
            array (
                'id' => 3244,
                'language_code' => 'sv',
                'display_language_code' => 'zh-hans',
                'name' => '瑞典语',
            ),
            244 =>
            array (
                'id' => 3245,
                'language_code' => 'sv',
                'display_language_code' => 'zu',
                'name' => 'Swedish',
            ),
            245 =>
            array (
                'id' => 3246,
                'language_code' => 'sv',
                'display_language_code' => 'zh-hant',
                'name' => '瑞典語',
            ),
            246 =>
            array (
                'id' => 3247,
                'language_code' => 'sv',
                'display_language_code' => 'ms',
                'name' => 'Swedish',
            ),
            247 =>
            array (
                'id' => 3248,
                'language_code' => 'sv',
                'display_language_code' => 'gl',
                'name' => 'Swedish',
            ),
            248 =>
            array (
                'id' => 3249,
                'language_code' => 'sv',
                'display_language_code' => 'bn',
                'name' => 'Swedish',
            ),
            249 =>
            array (
                'id' => 3250,
                'language_code' => 'sv',
                'display_language_code' => 'az',
                'name' => 'Swedish',
            ),
            250 =>
            array (
                'id' => 3251,
                'language_code' => 'ta',
                'display_language_code' => 'en',
                'name' => 'Tamil',
            ),
            251 =>
            array (
                'id' => 3252,
                'language_code' => 'ta',
                'display_language_code' => 'es',
                'name' => 'Tamil',
            ),
            252 =>
            array (
                'id' => 3253,
                'language_code' => 'ta',
                'display_language_code' => 'de',
                'name' => 'Tamil',
            ),
            253 =>
            array (
                'id' => 3254,
                'language_code' => 'ta',
                'display_language_code' => 'fr',
                'name' => 'Tamoul',
            ),
            254 =>
            array (
                'id' => 3255,
                'language_code' => 'ta',
                'display_language_code' => 'ar',
                'name' => 'التاميلية',
            ),
            255 =>
            array (
                'id' => 3256,
                'language_code' => 'ta',
                'display_language_code' => 'bs',
                'name' => 'Tamil',
            ),
            256 =>
            array (
                'id' => 3257,
                'language_code' => 'ta',
                'display_language_code' => 'bg',
                'name' => 'Тамилски',
            ),
            257 =>
            array (
                'id' => 3258,
                'language_code' => 'ta',
                'display_language_code' => 'ca',
                'name' => 'Tamil',
            ),
            258 =>
            array (
                'id' => 3259,
                'language_code' => 'ta',
                'display_language_code' => 'cs',
                'name' => 'Tamil',
            ),
            259 =>
            array (
                'id' => 3260,
                'language_code' => 'ta',
                'display_language_code' => 'sk',
                'name' => 'Tamilčina',
            ),
            260 =>
            array (
                'id' => 3261,
                'language_code' => 'ta',
                'display_language_code' => 'cy',
                'name' => 'Tamil',
            ),
            261 =>
            array (
                'id' => 3262,
                'language_code' => 'ta',
                'display_language_code' => 'da',
                'name' => 'Tamil',
            ),
            262 =>
            array (
                'id' => 3263,
                'language_code' => 'ta',
                'display_language_code' => 'el',
                'name' => 'Ταμίλ',
            ),
            263 =>
            array (
                'id' => 3264,
                'language_code' => 'ta',
                'display_language_code' => 'eo',
                'name' => 'Tamil',
            ),
            264 =>
            array (
                'id' => 3265,
                'language_code' => 'ta',
                'display_language_code' => 'et',
                'name' => 'Tamil',
            ),
            265 =>
            array (
                'id' => 3266,
                'language_code' => 'ta',
                'display_language_code' => 'eu',
                'name' => 'Tamil',
            ),
            266 =>
            array (
                'id' => 3267,
                'language_code' => 'ta',
                'display_language_code' => 'fa',
                'name' => 'Tamil',
            ),
            267 =>
            array (
                'id' => 3268,
                'language_code' => 'ta',
                'display_language_code' => 'fi',
                'name' => 'Tamili',
            ),
            268 =>
            array (
                'id' => 3269,
                'language_code' => 'ta',
                'display_language_code' => 'ga',
                'name' => 'Tamil',
            ),
            269 =>
            array (
                'id' => 3270,
                'language_code' => 'ta',
                'display_language_code' => 'he',
                'name' => 'טמילית',
            ),
            270 =>
            array (
                'id' => 3271,
                'language_code' => 'ta',
                'display_language_code' => 'hi',
                'name' => 'Tamil',
            ),
            271 =>
            array (
                'id' => 3272,
                'language_code' => 'ta',
                'display_language_code' => 'hr',
                'name' => 'Tamil',
            ),
            272 =>
            array (
                'id' => 3273,
                'language_code' => 'ta',
                'display_language_code' => 'hu',
                'name' => 'Tamil',
            ),
            273 =>
            array (
                'id' => 3274,
                'language_code' => 'ta',
                'display_language_code' => 'hy',
                'name' => 'Tamil',
            ),
            274 =>
            array (
                'id' => 3275,
                'language_code' => 'ta',
                'display_language_code' => 'id',
                'name' => 'Tamil',
            ),
            275 =>
            array (
                'id' => 3276,
                'language_code' => 'ta',
                'display_language_code' => 'is',
                'name' => 'Tamil',
            ),
            276 =>
            array (
                'id' => 3277,
                'language_code' => 'ta',
                'display_language_code' => 'it',
                'name' => 'Tamil',
            ),
            277 =>
            array (
                'id' => 3278,
                'language_code' => 'ta',
                'display_language_code' => 'ja',
                'name' => 'タミル語',
            ),
            278 =>
            array (
                'id' => 3279,
                'language_code' => 'ta',
                'display_language_code' => 'ko',
                'name' => '타밀어',
            ),
            279 =>
            array (
                'id' => 3280,
                'language_code' => 'ta',
                'display_language_code' => 'ku',
                'name' => 'Tamil',
            ),
            280 =>
            array (
                'id' => 3281,
                'language_code' => 'ta',
                'display_language_code' => 'lv',
                'name' => 'Tamil',
            ),
            281 =>
            array (
                'id' => 3282,
                'language_code' => 'ta',
                'display_language_code' => 'lt',
                'name' => 'Tamil',
            ),
            282 =>
            array (
                'id' => 3283,
                'language_code' => 'ta',
                'display_language_code' => 'mk',
                'name' => 'Tamil',
            ),
            283 =>
            array (
                'id' => 3284,
                'language_code' => 'ta',
                'display_language_code' => 'mt',
                'name' => 'Tamil',
            ),
            284 =>
            array (
                'id' => 3285,
                'language_code' => 'ta',
                'display_language_code' => 'mn',
                'name' => 'Tamil',
            ),
            285 =>
            array (
                'id' => 3286,
                'language_code' => 'ta',
                'display_language_code' => 'ne',
                'name' => 'Tamil',
            ),
            286 =>
            array (
                'id' => 3287,
                'language_code' => 'ta',
                'display_language_code' => 'nl',
                'name' => 'Tamil',
            ),
            287 =>
            array (
                'id' => 3288,
                'language_code' => 'ta',
                'display_language_code' => 'no',
                'name' => 'Tamil',
            ),
            288 =>
            array (
                'id' => 3289,
                'language_code' => 'ta',
                'display_language_code' => 'pa',
                'name' => 'Tamil',
            ),
            289 =>
            array (
                'id' => 3290,
                'language_code' => 'ta',
                'display_language_code' => 'pl',
                'name' => 'Tamilski',
            ),
            290 =>
            array (
                'id' => 3291,
                'language_code' => 'ta',
                'display_language_code' => 'pt-pt',
                'name' => 'Tamil',
            ),
            291 =>
            array (
                'id' => 3292,
                'language_code' => 'ta',
                'display_language_code' => 'pt-br',
                'name' => 'Tamil',
            ),
            292 =>
            array (
                'id' => 3293,
                'language_code' => 'ta',
                'display_language_code' => 'qu',
                'name' => 'Tamil',
            ),
            293 =>
            array (
                'id' => 3294,
                'language_code' => 'ta',
                'display_language_code' => 'ro',
                'name' => 'Tamilă',
            ),
            294 =>
            array (
                'id' => 3295,
                'language_code' => 'ta',
                'display_language_code' => 'ru',
                'name' => 'Тамильский',
            ),
            295 =>
            array (
                'id' => 3296,
                'language_code' => 'ta',
                'display_language_code' => 'sl',
                'name' => 'Tamilščina',
            ),
            296 =>
            array (
                'id' => 3297,
                'language_code' => 'ta',
                'display_language_code' => 'so',
                'name' => 'Tamil',
            ),
            297 =>
            array (
                'id' => 3298,
                'language_code' => 'ta',
                'display_language_code' => 'sq',
                'name' => 'Tamil',
            ),
            298 =>
            array (
                'id' => 3299,
                'language_code' => 'ta',
                'display_language_code' => 'sr',
                'name' => 'тамилски',
            ),
            299 =>
            array (
                'id' => 3300,
                'language_code' => 'ta',
                'display_language_code' => 'sv',
                'name' => 'Tamil',
            ),
            300 =>
            array (
                'id' => 3301,
                'language_code' => 'ta',
                'display_language_code' => 'ta',
                'name' => 'Tamil',
            ),
            301 =>
            array (
                'id' => 3302,
                'language_code' => 'ta',
                'display_language_code' => 'th',
                'name' => 'ทมิฬ',
            ),
            302 =>
            array (
                'id' => 3303,
                'language_code' => 'ta',
                'display_language_code' => 'tr',
                'name' => 'Tamil dili',
            ),
            303 =>
            array (
                'id' => 3304,
                'language_code' => 'ta',
                'display_language_code' => 'uk',
                'name' => 'Tamil',
            ),
            304 =>
            array (
                'id' => 3305,
                'language_code' => 'ta',
                'display_language_code' => 'ur',
                'name' => 'Tamil',
            ),
            305 =>
            array (
                'id' => 3306,
                'language_code' => 'ta',
                'display_language_code' => 'uz',
                'name' => 'Tamil',
            ),
            306 =>
            array (
                'id' => 3307,
                'language_code' => 'ta',
                'display_language_code' => 'vi',
                'name' => 'Tamil',
            ),
            307 =>
            array (
                'id' => 3308,
                'language_code' => 'ta',
                'display_language_code' => 'yi',
                'name' => 'Tamil',
            ),
            308 =>
            array (
                'id' => 3309,
                'language_code' => 'ta',
                'display_language_code' => 'zh-hans',
                'name' => '泰米尔语',
            ),
            309 =>
            array (
                'id' => 3310,
                'language_code' => 'ta',
                'display_language_code' => 'zu',
                'name' => 'Tamil',
            ),
            310 =>
            array (
                'id' => 3311,
                'language_code' => 'ta',
                'display_language_code' => 'zh-hant',
                'name' => '泰米爾語',
            ),
            311 =>
            array (
                'id' => 3312,
                'language_code' => 'ta',
                'display_language_code' => 'ms',
                'name' => 'Tamil',
            ),
            312 =>
            array (
                'id' => 3313,
                'language_code' => 'ta',
                'display_language_code' => 'gl',
                'name' => 'Tamil',
            ),
            313 =>
            array (
                'id' => 3314,
                'language_code' => 'ta',
                'display_language_code' => 'bn',
                'name' => 'Tamil',
            ),
            314 =>
            array (
                'id' => 3315,
                'language_code' => 'ta',
                'display_language_code' => 'az',
                'name' => 'Tamil',
            ),
            315 =>
            array (
                'id' => 3316,
                'language_code' => 'th',
                'display_language_code' => 'en',
                'name' => 'Thai',
            ),
            316 =>
            array (
                'id' => 3317,
                'language_code' => 'th',
                'display_language_code' => 'es',
                'name' => 'Tailandés',
            ),
            317 =>
            array (
                'id' => 3318,
                'language_code' => 'th',
                'display_language_code' => 'de',
                'name' => 'Thai',
            ),
            318 =>
            array (
                'id' => 3319,
                'language_code' => 'th',
                'display_language_code' => 'fr',
                'name' => 'Thaï',
            ),
            319 =>
            array (
                'id' => 3320,
                'language_code' => 'th',
                'display_language_code' => 'ar',
                'name' => 'التايلندية',
            ),
            320 =>
            array (
                'id' => 3321,
                'language_code' => 'th',
                'display_language_code' => 'bs',
                'name' => 'Thai',
            ),
            321 =>
            array (
                'id' => 3322,
                'language_code' => 'th',
                'display_language_code' => 'bg',
                'name' => 'Тайски',
            ),
            322 =>
            array (
                'id' => 3323,
                'language_code' => 'th',
                'display_language_code' => 'ca',
                'name' => 'Thai',
            ),
            323 =>
            array (
                'id' => 3324,
                'language_code' => 'th',
                'display_language_code' => 'cs',
                'name' => 'Thai',
            ),
            324 =>
            array (
                'id' => 3325,
                'language_code' => 'th',
                'display_language_code' => 'sk',
                'name' => 'Thajština',
            ),
            325 =>
            array (
                'id' => 3326,
                'language_code' => 'th',
                'display_language_code' => 'cy',
                'name' => 'Thai',
            ),
            326 =>
            array (
                'id' => 3327,
                'language_code' => 'th',
                'display_language_code' => 'da',
                'name' => 'Thai',
            ),
            327 =>
            array (
                'id' => 3328,
                'language_code' => 'th',
                'display_language_code' => 'el',
                'name' => 'Ταϊλανδέζικα',
            ),
            328 =>
            array (
                'id' => 3329,
                'language_code' => 'th',
                'display_language_code' => 'eo',
                'name' => 'Thai',
            ),
            329 =>
            array (
                'id' => 3330,
                'language_code' => 'th',
                'display_language_code' => 'et',
                'name' => 'Thai',
            ),
            330 =>
            array (
                'id' => 3331,
                'language_code' => 'th',
                'display_language_code' => 'eu',
                'name' => 'Thai',
            ),
            331 =>
            array (
                'id' => 3332,
                'language_code' => 'th',
                'display_language_code' => 'fa',
                'name' => 'Thai',
            ),
            332 =>
            array (
                'id' => 3333,
                'language_code' => 'th',
                'display_language_code' => 'fi',
                'name' => 'Thai',
            ),
            333 =>
            array (
                'id' => 3334,
                'language_code' => 'th',
                'display_language_code' => 'ga',
                'name' => 'Thai',
            ),
            334 =>
            array (
                'id' => 3335,
                'language_code' => 'th',
                'display_language_code' => 'he',
                'name' => 'תאילנדית',
            ),
            335 =>
            array (
                'id' => 3336,
                'language_code' => 'th',
                'display_language_code' => 'hi',
                'name' => 'Thai',
            ),
            336 =>
            array (
                'id' => 3337,
                'language_code' => 'th',
                'display_language_code' => 'hr',
                'name' => 'Thai',
            ),
            337 =>
            array (
                'id' => 3338,
                'language_code' => 'th',
                'display_language_code' => 'hu',
                'name' => 'Tájföldi',
            ),
            338 =>
            array (
                'id' => 3339,
                'language_code' => 'th',
                'display_language_code' => 'hy',
                'name' => 'Thai',
            ),
            339 =>
            array (
                'id' => 3340,
                'language_code' => 'th',
                'display_language_code' => 'id',
                'name' => 'Thai',
            ),
            340 =>
            array (
                'id' => 3341,
                'language_code' => 'th',
                'display_language_code' => 'is',
                'name' => 'Thai',
            ),
            341 =>
            array (
                'id' => 3342,
                'language_code' => 'th',
                'display_language_code' => 'it',
                'name' => 'Thai',
            ),
            342 =>
            array (
                'id' => 3343,
                'language_code' => 'th',
                'display_language_code' => 'ja',
                'name' => 'タイ語',
            ),
            343 =>
            array (
                'id' => 3344,
                'language_code' => 'th',
                'display_language_code' => 'ko',
                'name' => '태국어',
            ),
            344 =>
            array (
                'id' => 3345,
                'language_code' => 'th',
                'display_language_code' => 'ku',
                'name' => 'Thai',
            ),
            345 =>
            array (
                'id' => 3346,
                'language_code' => 'th',
                'display_language_code' => 'lv',
                'name' => 'Thai',
            ),
            346 =>
            array (
                'id' => 3347,
                'language_code' => 'th',
                'display_language_code' => 'lt',
                'name' => 'Thai',
            ),
            347 =>
            array (
                'id' => 3348,
                'language_code' => 'th',
                'display_language_code' => 'mk',
                'name' => 'Thai',
            ),
            348 =>
            array (
                'id' => 3349,
                'language_code' => 'th',
                'display_language_code' => 'mt',
                'name' => 'Thai',
            ),
            349 =>
            array (
                'id' => 3350,
                'language_code' => 'th',
                'display_language_code' => 'mn',
                'name' => 'Thai',
            ),
            350 =>
            array (
                'id' => 3351,
                'language_code' => 'th',
                'display_language_code' => 'ne',
                'name' => 'Thai',
            ),
            351 =>
            array (
                'id' => 3352,
                'language_code' => 'th',
                'display_language_code' => 'nl',
                'name' => 'Thai',
            ),
            352 =>
            array (
                'id' => 3353,
                'language_code' => 'th',
                'display_language_code' => 'no',
                'name' => 'Thai',
            ),
            353 =>
            array (
                'id' => 3354,
                'language_code' => 'th',
                'display_language_code' => 'pa',
                'name' => 'Thai',
            ),
            354 =>
            array (
                'id' => 3355,
                'language_code' => 'th',
                'display_language_code' => 'pl',
                'name' => 'Tajski',
            ),
            355 =>
            array (
                'id' => 3356,
                'language_code' => 'th',
                'display_language_code' => 'pt-pt',
                'name' => 'Tailandês',
            ),
            356 =>
            array (
                'id' => 3357,
                'language_code' => 'th',
                'display_language_code' => 'pt-br',
                'name' => 'Tailandês',
            ),
            357 =>
            array (
                'id' => 3358,
                'language_code' => 'th',
                'display_language_code' => 'qu',
                'name' => 'Thai',
            ),
            358 =>
            array (
                'id' => 3359,
                'language_code' => 'th',
                'display_language_code' => 'ro',
                'name' => 'Tailandeză',
            ),
            359 =>
            array (
                'id' => 3360,
                'language_code' => 'th',
                'display_language_code' => 'ru',
                'name' => 'Тайский',
            ),
            360 =>
            array (
                'id' => 3361,
                'language_code' => 'th',
                'display_language_code' => 'sl',
                'name' => 'Tajski',
            ),
            361 =>
            array (
                'id' => 3362,
                'language_code' => 'th',
                'display_language_code' => 'so',
                'name' => 'Thai',
            ),
            362 =>
            array (
                'id' => 3363,
                'language_code' => 'th',
                'display_language_code' => 'sq',
                'name' => 'Thai',
            ),
            363 =>
            array (
                'id' => 3364,
                'language_code' => 'th',
                'display_language_code' => 'sr',
                'name' => 'Тајландски',
            ),
            364 =>
            array (
                'id' => 3365,
                'language_code' => 'th',
                'display_language_code' => 'sv',
                'name' => 'Thailändska',
            ),
            365 =>
            array (
                'id' => 3366,
                'language_code' => 'th',
                'display_language_code' => 'ta',
                'name' => 'Thai',
            ),
            366 =>
            array (
                'id' => 3367,
                'language_code' => 'th',
                'display_language_code' => 'th',
                'name' => 'ไทย',
            ),
            367 =>
            array (
                'id' => 3368,
                'language_code' => 'th',
                'display_language_code' => 'tr',
                'name' => 'Tayca',
            ),
            368 =>
            array (
                'id' => 3369,
                'language_code' => 'th',
                'display_language_code' => 'uk',
                'name' => 'Thai',
            ),
            369 =>
            array (
                'id' => 3370,
                'language_code' => 'th',
                'display_language_code' => 'ur',
                'name' => 'Thai',
            ),
            370 =>
            array (
                'id' => 3371,
                'language_code' => 'th',
                'display_language_code' => 'uz',
                'name' => 'Thai',
            ),
            371 =>
            array (
                'id' => 3372,
                'language_code' => 'th',
                'display_language_code' => 'vi',
                'name' => 'Thai',
            ),
            372 =>
            array (
                'id' => 3373,
                'language_code' => 'th',
                'display_language_code' => 'yi',
                'name' => 'Thai',
            ),
            373 =>
            array (
                'id' => 3374,
                'language_code' => 'th',
                'display_language_code' => 'zh-hans',
                'name' => '泰语',
            ),
            374 =>
            array (
                'id' => 3375,
                'language_code' => 'th',
                'display_language_code' => 'zu',
                'name' => 'Thai',
            ),
            375 =>
            array (
                'id' => 3376,
                'language_code' => 'th',
                'display_language_code' => 'zh-hant',
                'name' => '泰語',
            ),
            376 =>
            array (
                'id' => 3377,
                'language_code' => 'th',
                'display_language_code' => 'ms',
                'name' => 'Thai',
            ),
            377 =>
            array (
                'id' => 3378,
                'language_code' => 'th',
                'display_language_code' => 'gl',
                'name' => 'Thai',
            ),
            378 =>
            array (
                'id' => 3379,
                'language_code' => 'th',
                'display_language_code' => 'bn',
                'name' => 'Thai',
            ),
            379 =>
            array (
                'id' => 3380,
                'language_code' => 'th',
                'display_language_code' => 'az',
                'name' => 'Thai',
            ),
            380 =>
            array (
                'id' => 3381,
                'language_code' => 'tr',
                'display_language_code' => 'en',
                'name' => 'Turkish',
            ),
            381 =>
            array (
                'id' => 3382,
                'language_code' => 'tr',
                'display_language_code' => 'es',
                'name' => 'Turco',
            ),
            382 =>
            array (
                'id' => 3383,
                'language_code' => 'tr',
                'display_language_code' => 'de',
                'name' => 'Türkisch',
            ),
            383 =>
            array (
                'id' => 3384,
                'language_code' => 'tr',
                'display_language_code' => 'fr',
                'name' => 'Turc',
            ),
            384 =>
            array (
                'id' => 3385,
                'language_code' => 'tr',
                'display_language_code' => 'ar',
                'name' => 'التركية',
            ),
            385 =>
            array (
                'id' => 3386,
                'language_code' => 'tr',
                'display_language_code' => 'bs',
                'name' => 'Turkish',
            ),
            386 =>
            array (
                'id' => 3387,
                'language_code' => 'tr',
                'display_language_code' => 'bg',
                'name' => 'Турски',
            ),
            387 =>
            array (
                'id' => 3388,
                'language_code' => 'tr',
                'display_language_code' => 'ca',
                'name' => 'Turkish',
            ),
            388 =>
            array (
                'id' => 3389,
                'language_code' => 'tr',
                'display_language_code' => 'cs',
                'name' => 'Turečtina',
            ),
            389 =>
            array (
                'id' => 3390,
                'language_code' => 'tr',
                'display_language_code' => 'sk',
                'name' => 'Turečtina',
            ),
            390 =>
            array (
                'id' => 3391,
                'language_code' => 'tr',
                'display_language_code' => 'cy',
                'name' => 'Turkish',
            ),
            391 =>
            array (
                'id' => 3392,
                'language_code' => 'tr',
                'display_language_code' => 'da',
                'name' => 'Turkish',
            ),
            392 =>
            array (
                'id' => 3393,
                'language_code' => 'tr',
                'display_language_code' => 'el',
                'name' => 'Τουρκικά',
            ),
            393 =>
            array (
                'id' => 3394,
                'language_code' => 'tr',
                'display_language_code' => 'eo',
                'name' => 'Turkish',
            ),
            394 =>
            array (
                'id' => 3395,
                'language_code' => 'tr',
                'display_language_code' => 'et',
                'name' => 'Turkish',
            ),
            395 =>
            array (
                'id' => 3396,
                'language_code' => 'tr',
                'display_language_code' => 'eu',
                'name' => 'Turkish',
            ),
            396 =>
            array (
                'id' => 3397,
                'language_code' => 'tr',
                'display_language_code' => 'fa',
                'name' => 'Turkish',
            ),
            397 =>
            array (
                'id' => 3398,
                'language_code' => 'tr',
                'display_language_code' => 'fi',
                'name' => 'Turkki',
            ),
            398 =>
            array (
                'id' => 3399,
                'language_code' => 'tr',
                'display_language_code' => 'ga',
                'name' => 'Turkish',
            ),
            399 =>
            array (
                'id' => 3400,
                'language_code' => 'tr',
                'display_language_code' => 'he',
                'name' => 'תורכית',
            ),
            400 =>
            array (
                'id' => 3401,
                'language_code' => 'tr',
                'display_language_code' => 'hi',
                'name' => 'Turkish',
            ),
            401 =>
            array (
                'id' => 3402,
                'language_code' => 'tr',
                'display_language_code' => 'hr',
                'name' => 'Turski',
            ),
            402 =>
            array (
                'id' => 3403,
                'language_code' => 'tr',
                'display_language_code' => 'hu',
                'name' => 'Török',
            ),
            403 =>
            array (
                'id' => 3404,
                'language_code' => 'tr',
                'display_language_code' => 'hy',
                'name' => 'Turkish',
            ),
            404 =>
            array (
                'id' => 3405,
                'language_code' => 'tr',
                'display_language_code' => 'id',
                'name' => 'Turkish',
            ),
            405 =>
            array (
                'id' => 3406,
                'language_code' => 'tr',
                'display_language_code' => 'is',
                'name' => 'Turkish',
            ),
            406 =>
            array (
                'id' => 3407,
                'language_code' => 'tr',
                'display_language_code' => 'it',
                'name' => 'Turco',
            ),
            407 =>
            array (
                'id' => 3408,
                'language_code' => 'tr',
                'display_language_code' => 'ja',
                'name' => 'トルコ語',
            ),
            408 =>
            array (
                'id' => 3409,
                'language_code' => 'tr',
                'display_language_code' => 'ko',
                'name' => '터어키어',
            ),
            409 =>
            array (
                'id' => 3410,
                'language_code' => 'tr',
                'display_language_code' => 'ku',
                'name' => 'Turkish',
            ),
            410 =>
            array (
                'id' => 3411,
                'language_code' => 'tr',
                'display_language_code' => 'lv',
                'name' => 'Turkish',
            ),
            411 =>
            array (
                'id' => 3412,
                'language_code' => 'tr',
                'display_language_code' => 'lt',
                'name' => 'Turkish',
            ),
            412 =>
            array (
                'id' => 3413,
                'language_code' => 'tr',
                'display_language_code' => 'mk',
                'name' => 'Turkish',
            ),
            413 =>
            array (
                'id' => 3414,
                'language_code' => 'tr',
                'display_language_code' => 'mt',
                'name' => 'Turkish',
            ),
            414 =>
            array (
                'id' => 3415,
                'language_code' => 'tr',
                'display_language_code' => 'mn',
                'name' => 'Turkish',
            ),
            415 =>
            array (
                'id' => 3416,
                'language_code' => 'tr',
                'display_language_code' => 'ne',
                'name' => 'Turkish',
            ),
            416 =>
            array (
                'id' => 3417,
                'language_code' => 'tr',
                'display_language_code' => 'nl',
                'name' => 'Turks',
            ),
            417 =>
            array (
                'id' => 3418,
                'language_code' => 'tr',
                'display_language_code' => 'no',
                'name' => 'Turkish',
            ),
            418 =>
            array (
                'id' => 3419,
                'language_code' => 'tr',
                'display_language_code' => 'pa',
                'name' => 'Turkish',
            ),
            419 =>
            array (
                'id' => 3420,
                'language_code' => 'tr',
                'display_language_code' => 'pl',
                'name' => 'Turecki',
            ),
            420 =>
            array (
                'id' => 3421,
                'language_code' => 'tr',
                'display_language_code' => 'pt-pt',
                'name' => 'Turco',
            ),
            421 =>
            array (
                'id' => 3422,
                'language_code' => 'tr',
                'display_language_code' => 'pt-br',
                'name' => 'Turco',
            ),
            422 =>
            array (
                'id' => 3423,
                'language_code' => 'tr',
                'display_language_code' => 'qu',
                'name' => 'Turkish',
            ),
            423 =>
            array (
                'id' => 3424,
                'language_code' => 'tr',
                'display_language_code' => 'ro',
                'name' => 'Turcă',
            ),
            424 =>
            array (
                'id' => 3425,
                'language_code' => 'tr',
                'display_language_code' => 'ru',
                'name' => 'Турецкий',
            ),
            425 =>
            array (
                'id' => 3426,
                'language_code' => 'tr',
                'display_language_code' => 'sl',
                'name' => 'Turščina',
            ),
            426 =>
            array (
                'id' => 3427,
                'language_code' => 'tr',
                'display_language_code' => 'so',
                'name' => 'Turkish',
            ),
            427 =>
            array (
                'id' => 3428,
                'language_code' => 'tr',
                'display_language_code' => 'sq',
                'name' => 'Turkish',
            ),
            428 =>
            array (
                'id' => 3429,
                'language_code' => 'tr',
                'display_language_code' => 'sr',
                'name' => 'турски',
            ),
            429 =>
            array (
                'id' => 3430,
                'language_code' => 'tr',
                'display_language_code' => 'sv',
                'name' => 'Turkiska',
            ),
            430 =>
            array (
                'id' => 3431,
                'language_code' => 'tr',
                'display_language_code' => 'ta',
                'name' => 'Turkish',
            ),
            431 =>
            array (
                'id' => 3432,
                'language_code' => 'tr',
                'display_language_code' => 'th',
                'name' => 'ตุรกี',
            ),
            432 =>
            array (
                'id' => 3433,
                'language_code' => 'tr',
                'display_language_code' => 'tr',
                'name' => 'Türkçe',
            ),
            433 =>
            array (
                'id' => 3434,
                'language_code' => 'tr',
                'display_language_code' => 'uk',
                'name' => 'Turkish',
            ),
            434 =>
            array (
                'id' => 3435,
                'language_code' => 'tr',
                'display_language_code' => 'ur',
                'name' => 'Turkish',
            ),
            435 =>
            array (
                'id' => 3436,
                'language_code' => 'tr',
                'display_language_code' => 'uz',
                'name' => 'Turkish',
            ),
            436 =>
            array (
                'id' => 3437,
                'language_code' => 'tr',
                'display_language_code' => 'vi',
                'name' => 'Turkish',
            ),
            437 =>
            array (
                'id' => 3438,
                'language_code' => 'tr',
                'display_language_code' => 'yi',
                'name' => 'Turkish',
            ),
            438 =>
            array (
                'id' => 3439,
                'language_code' => 'tr',
                'display_language_code' => 'zh-hans',
                'name' => '土耳其语',
            ),
            439 =>
            array (
                'id' => 3440,
                'language_code' => 'tr',
                'display_language_code' => 'zu',
                'name' => 'Turkish',
            ),
            440 =>
            array (
                'id' => 3441,
                'language_code' => 'tr',
                'display_language_code' => 'zh-hant',
                'name' => '土耳其語',
            ),
            441 =>
            array (
                'id' => 3442,
                'language_code' => 'tr',
                'display_language_code' => 'ms',
                'name' => 'Turkish',
            ),
            442 =>
            array (
                'id' => 3443,
                'language_code' => 'tr',
                'display_language_code' => 'gl',
                'name' => 'Turkish',
            ),
            443 =>
            array (
                'id' => 3444,
                'language_code' => 'tr',
                'display_language_code' => 'bn',
                'name' => 'Turkish',
            ),
            444 =>
            array (
                'id' => 3445,
                'language_code' => 'tr',
                'display_language_code' => 'az',
                'name' => 'Turkish',
            ),
            445 =>
            array (
                'id' => 3446,
                'language_code' => 'uk',
                'display_language_code' => 'en',
                'name' => 'Ukrainian',
            ),
            446 =>
            array (
                'id' => 3447,
                'language_code' => 'uk',
                'display_language_code' => 'es',
                'name' => 'Ucraniano',
            ),
            447 =>
            array (
                'id' => 3448,
                'language_code' => 'uk',
                'display_language_code' => 'de',
                'name' => 'Ukrainisch',
            ),
            448 =>
            array (
                'id' => 3449,
                'language_code' => 'uk',
                'display_language_code' => 'fr',
                'name' => 'Ukrainien',
            ),
            449 =>
            array (
                'id' => 3450,
                'language_code' => 'uk',
                'display_language_code' => 'ar',
                'name' => 'الأوكرانية',
            ),
            450 =>
            array (
                'id' => 3451,
                'language_code' => 'uk',
                'display_language_code' => 'bs',
                'name' => 'Ukrainian',
            ),
            451 =>
            array (
                'id' => 3452,
                'language_code' => 'uk',
                'display_language_code' => 'bg',
                'name' => 'Украински',
            ),
            452 =>
            array (
                'id' => 3453,
                'language_code' => 'uk',
                'display_language_code' => 'ca',
                'name' => 'Ukrainian',
            ),
            453 =>
            array (
                'id' => 3454,
                'language_code' => 'uk',
                'display_language_code' => 'cs',
                'name' => 'Ukrajinský',
            ),
            454 =>
            array (
                'id' => 3455,
                'language_code' => 'uk',
                'display_language_code' => 'sk',
                'name' => 'Ukrajinčina',
            ),
            455 =>
            array (
                'id' => 3456,
                'language_code' => 'uk',
                'display_language_code' => 'cy',
                'name' => 'Ukrainian',
            ),
            456 =>
            array (
                'id' => 3457,
                'language_code' => 'uk',
                'display_language_code' => 'da',
                'name' => 'Ukrainian',
            ),
            457 =>
            array (
                'id' => 3458,
                'language_code' => 'uk',
                'display_language_code' => 'el',
                'name' => 'Ουκρανικά',
            ),
            458 =>
            array (
                'id' => 3459,
                'language_code' => 'uk',
                'display_language_code' => 'eo',
                'name' => 'Ukrainian',
            ),
            459 =>
            array (
                'id' => 3460,
                'language_code' => 'uk',
                'display_language_code' => 'et',
                'name' => 'Ukrainian',
            ),
            460 =>
            array (
                'id' => 3461,
                'language_code' => 'uk',
                'display_language_code' => 'eu',
                'name' => 'Ukrainian',
            ),
            461 =>
            array (
                'id' => 3462,
                'language_code' => 'uk',
                'display_language_code' => 'fa',
                'name' => 'Ukrainian',
            ),
            462 =>
            array (
                'id' => 3463,
                'language_code' => 'uk',
                'display_language_code' => 'fi',
                'name' => 'Ukraina',
            ),
            463 =>
            array (
                'id' => 3464,
                'language_code' => 'uk',
                'display_language_code' => 'ga',
                'name' => 'Ukrainian',
            ),
            464 =>
            array (
                'id' => 3465,
                'language_code' => 'uk',
                'display_language_code' => 'he',
                'name' => 'אוקראינית',
            ),
            465 =>
            array (
                'id' => 3466,
                'language_code' => 'uk',
                'display_language_code' => 'hi',
                'name' => 'Ukrainian',
            ),
            466 =>
            array (
                'id' => 3467,
                'language_code' => 'uk',
                'display_language_code' => 'hr',
                'name' => 'Ukrajinski',
            ),
            467 =>
            array (
                'id' => 3468,
                'language_code' => 'uk',
                'display_language_code' => 'hu',
                'name' => 'Ukrán',
            ),
            468 =>
            array (
                'id' => 3469,
                'language_code' => 'uk',
                'display_language_code' => 'hy',
                'name' => 'Ukrainian',
            ),
            469 =>
            array (
                'id' => 3470,
                'language_code' => 'uk',
                'display_language_code' => 'id',
                'name' => 'Ukrainian',
            ),
            470 =>
            array (
                'id' => 3471,
                'language_code' => 'uk',
                'display_language_code' => 'is',
                'name' => 'Ukrainian',
            ),
            471 =>
            array (
                'id' => 3472,
                'language_code' => 'uk',
                'display_language_code' => 'it',
                'name' => 'Ucraino',
            ),
            472 =>
            array (
                'id' => 3473,
                'language_code' => 'uk',
                'display_language_code' => 'ja',
                'name' => 'ウクライナ語',
            ),
            473 =>
            array (
                'id' => 3474,
                'language_code' => 'uk',
                'display_language_code' => 'ko',
                'name' => '우크라이나어',
            ),
            474 =>
            array (
                'id' => 3475,
                'language_code' => 'uk',
                'display_language_code' => 'ku',
                'name' => 'Ukrainian',
            ),
            475 =>
            array (
                'id' => 3476,
                'language_code' => 'uk',
                'display_language_code' => 'lv',
                'name' => 'Ukrainian',
            ),
            476 =>
            array (
                'id' => 3477,
                'language_code' => 'uk',
                'display_language_code' => 'lt',
                'name' => 'Ukrainian',
            ),
            477 =>
            array (
                'id' => 3478,
                'language_code' => 'uk',
                'display_language_code' => 'mk',
                'name' => 'Ukrainian',
            ),
            478 =>
            array (
                'id' => 3479,
                'language_code' => 'uk',
                'display_language_code' => 'mt',
                'name' => 'Ukrainian',
            ),
            479 =>
            array (
                'id' => 3480,
                'language_code' => 'uk',
                'display_language_code' => 'mn',
                'name' => 'Ukrainian',
            ),
            480 =>
            array (
                'id' => 3481,
                'language_code' => 'uk',
                'display_language_code' => 'ne',
                'name' => 'Ukrainian',
            ),
            481 =>
            array (
                'id' => 3482,
                'language_code' => 'uk',
                'display_language_code' => 'nl',
                'name' => 'Oekraïens',
            ),
            482 =>
            array (
                'id' => 3483,
                'language_code' => 'uk',
                'display_language_code' => 'no',
                'name' => 'Ukrainsk',
            ),
            483 =>
            array (
                'id' => 3484,
                'language_code' => 'uk',
                'display_language_code' => 'pa',
                'name' => 'Ukrainian',
            ),
            484 =>
            array (
                'id' => 3485,
                'language_code' => 'uk',
                'display_language_code' => 'pl',
                'name' => 'Ukraiński',
            ),
            485 =>
            array (
                'id' => 3486,
                'language_code' => 'uk',
                'display_language_code' => 'pt-pt',
                'name' => 'Ucraniano',
            ),
            486 =>
            array (
                'id' => 3487,
                'language_code' => 'uk',
                'display_language_code' => 'pt-br',
                'name' => 'Ucraniano',
            ),
            487 =>
            array (
                'id' => 3488,
                'language_code' => 'uk',
                'display_language_code' => 'qu',
                'name' => 'Ukrainian',
            ),
            488 =>
            array (
                'id' => 3489,
                'language_code' => 'uk',
                'display_language_code' => 'ro',
                'name' => 'Ucrainiană',
            ),
            489 =>
            array (
                'id' => 3490,
                'language_code' => 'uk',
                'display_language_code' => 'ru',
                'name' => 'Украинский',
            ),
            490 =>
            array (
                'id' => 3491,
                'language_code' => 'uk',
                'display_language_code' => 'sl',
                'name' => 'Ukrajinski',
            ),
            491 =>
            array (
                'id' => 3492,
                'language_code' => 'uk',
                'display_language_code' => 'so',
                'name' => 'Ukrainian',
            ),
            492 =>
            array (
                'id' => 3493,
                'language_code' => 'uk',
                'display_language_code' => 'sq',
                'name' => 'Ukrainian',
            ),
            493 =>
            array (
                'id' => 3494,
                'language_code' => 'uk',
                'display_language_code' => 'sr',
                'name' => 'украјински',
            ),
            494 =>
            array (
                'id' => 3495,
                'language_code' => 'uk',
                'display_language_code' => 'sv',
                'name' => 'Ukrainska',
            ),
            495 =>
            array (
                'id' => 3496,
                'language_code' => 'uk',
                'display_language_code' => 'ta',
                'name' => 'Ukrainian',
            ),
            496 =>
            array (
                'id' => 3497,
                'language_code' => 'uk',
                'display_language_code' => 'th',
                'name' => 'ยูเครน',
            ),
            497 =>
            array (
                'id' => 3498,
                'language_code' => 'uk',
                'display_language_code' => 'tr',
                'name' => 'Ukraynaca',
            ),
            498 =>
            array (
                'id' => 3499,
                'language_code' => 'uk',
                'display_language_code' => 'uk',
                'name' => 'Українська',
            ),
            499 =>
            array (
                'id' => 3500,
                'language_code' => 'uk',
                'display_language_code' => 'ur',
                'name' => 'Ukrainian',
            ),
        ));
        \DB::table('language_translations')->insert(array (
            0 =>
            array (
                'id' => 3501,
                'language_code' => 'uk',
                'display_language_code' => 'uz',
                'name' => 'Ukrainian',
            ),
            1 =>
            array (
                'id' => 3502,
                'language_code' => 'uk',
                'display_language_code' => 'vi',
                'name' => 'Ukrainian',
            ),
            2 =>
            array (
                'id' => 3503,
                'language_code' => 'uk',
                'display_language_code' => 'yi',
                'name' => 'Ukrainian',
            ),
            3 =>
            array (
                'id' => 3504,
                'language_code' => 'uk',
                'display_language_code' => 'zh-hans',
                'name' => '乌克兰语',
            ),
            4 =>
            array (
                'id' => 3505,
                'language_code' => 'uk',
                'display_language_code' => 'zu',
                'name' => 'Ukrainian',
            ),
            5 =>
            array (
                'id' => 3506,
                'language_code' => 'uk',
                'display_language_code' => 'zh-hant',
                'name' => '烏克蘭語',
            ),
            6 =>
            array (
                'id' => 3507,
                'language_code' => 'uk',
                'display_language_code' => 'ms',
                'name' => 'Ukrainian',
            ),
            7 =>
            array (
                'id' => 3508,
                'language_code' => 'uk',
                'display_language_code' => 'gl',
                'name' => 'Ukrainian',
            ),
            8 =>
            array (
                'id' => 3509,
                'language_code' => 'uk',
                'display_language_code' => 'bn',
                'name' => 'Ukrainian',
            ),
            9 =>
            array (
                'id' => 3510,
                'language_code' => 'uk',
                'display_language_code' => 'az',
                'name' => 'Ukrainian',
            ),
            10 =>
            array (
                'id' => 3511,
                'language_code' => 'ur',
                'display_language_code' => 'en',
                'name' => 'Urdu',
            ),
            11 =>
            array (
                'id' => 3512,
                'language_code' => 'ur',
                'display_language_code' => 'es',
                'name' => 'Urdu',
            ),
            12 =>
            array (
                'id' => 3513,
                'language_code' => 'ur',
                'display_language_code' => 'de',
                'name' => 'Urdu',
            ),
            13 =>
            array (
                'id' => 3514,
                'language_code' => 'ur',
                'display_language_code' => 'fr',
                'name' => 'Ourdu',
            ),
            14 =>
            array (
                'id' => 3515,
                'language_code' => 'ur',
                'display_language_code' => 'ar',
                'name' => 'الأردية',
            ),
            15 =>
            array (
                'id' => 3516,
                'language_code' => 'ur',
                'display_language_code' => 'bs',
                'name' => 'Urdu',
            ),
            16 =>
            array (
                'id' => 3517,
                'language_code' => 'ur',
                'display_language_code' => 'bg',
                'name' => 'Урду',
            ),
            17 =>
            array (
                'id' => 3518,
                'language_code' => 'ur',
                'display_language_code' => 'ca',
                'name' => 'Urdu',
            ),
            18 =>
            array (
                'id' => 3519,
                'language_code' => 'ur',
                'display_language_code' => 'cs',
                'name' => 'Urdu',
            ),
            19 =>
            array (
                'id' => 3520,
                'language_code' => 'ur',
                'display_language_code' => 'sk',
                'name' => 'Urdština',
            ),
            20 =>
            array (
                'id' => 3521,
                'language_code' => 'ur',
                'display_language_code' => 'cy',
                'name' => 'Urdu',
            ),
            21 =>
            array (
                'id' => 3522,
                'language_code' => 'ur',
                'display_language_code' => 'da',
                'name' => 'Urdu',
            ),
            22 =>
            array (
                'id' => 3523,
                'language_code' => 'ur',
                'display_language_code' => 'el',
                'name' => 'Ούρντου',
            ),
            23 =>
            array (
                'id' => 3524,
                'language_code' => 'ur',
                'display_language_code' => 'eo',
                'name' => 'Urdu',
            ),
            24 =>
            array (
                'id' => 3525,
                'language_code' => 'ur',
                'display_language_code' => 'et',
                'name' => 'Urdu',
            ),
            25 =>
            array (
                'id' => 3526,
                'language_code' => 'ur',
                'display_language_code' => 'eu',
                'name' => 'Urdu',
            ),
            26 =>
            array (
                'id' => 3527,
                'language_code' => 'ur',
                'display_language_code' => 'fa',
                'name' => 'Urdu',
            ),
            27 =>
            array (
                'id' => 3528,
                'language_code' => 'ur',
                'display_language_code' => 'fi',
                'name' => 'Urdu',
            ),
            28 =>
            array (
                'id' => 3529,
                'language_code' => 'ur',
                'display_language_code' => 'ga',
                'name' => 'Urdu',
            ),
            29 =>
            array (
                'id' => 3530,
                'language_code' => 'ur',
                'display_language_code' => 'he',
                'name' => 'אורדו',
            ),
            30 =>
            array (
                'id' => 3531,
                'language_code' => 'ur',
                'display_language_code' => 'hi',
                'name' => 'Urdu',
            ),
            31 =>
            array (
                'id' => 3532,
                'language_code' => 'ur',
                'display_language_code' => 'hr',
                'name' => 'Urdski',
            ),
            32 =>
            array (
                'id' => 3533,
                'language_code' => 'ur',
                'display_language_code' => 'hu',
                'name' => 'Urdu',
            ),
            33 =>
            array (
                'id' => 3534,
                'language_code' => 'ur',
                'display_language_code' => 'hy',
                'name' => 'Urdu',
            ),
            34 =>
            array (
                'id' => 3535,
                'language_code' => 'ur',
                'display_language_code' => 'id',
                'name' => 'Urdu',
            ),
            35 =>
            array (
                'id' => 3536,
                'language_code' => 'ur',
                'display_language_code' => 'is',
                'name' => 'Urdu',
            ),
            36 =>
            array (
                'id' => 3537,
                'language_code' => 'ur',
                'display_language_code' => 'it',
                'name' => 'Urdu',
            ),
            37 =>
            array (
                'id' => 3538,
                'language_code' => 'ur',
                'display_language_code' => 'ja',
                'name' => 'ウルドゥー語',
            ),
            38 =>
            array (
                'id' => 3539,
                'language_code' => 'ur',
                'display_language_code' => 'ko',
                'name' => '우르두어',
            ),
            39 =>
            array (
                'id' => 3540,
                'language_code' => 'ur',
                'display_language_code' => 'ku',
                'name' => 'Urdu',
            ),
            40 =>
            array (
                'id' => 3541,
                'language_code' => 'ur',
                'display_language_code' => 'lv',
                'name' => 'Urdu',
            ),
            41 =>
            array (
                'id' => 3542,
                'language_code' => 'ur',
                'display_language_code' => 'lt',
                'name' => 'Urdu',
            ),
            42 =>
            array (
                'id' => 3543,
                'language_code' => 'ur',
                'display_language_code' => 'mk',
                'name' => 'Urdu',
            ),
            43 =>
            array (
                'id' => 3544,
                'language_code' => 'ur',
                'display_language_code' => 'mt',
                'name' => 'Urdu',
            ),
            44 =>
            array (
                'id' => 3545,
                'language_code' => 'ur',
                'display_language_code' => 'mn',
                'name' => 'Urdu',
            ),
            45 =>
            array (
                'id' => 3546,
                'language_code' => 'ur',
                'display_language_code' => 'ne',
                'name' => 'Urdu',
            ),
            46 =>
            array (
                'id' => 3547,
                'language_code' => 'ur',
                'display_language_code' => 'nl',
                'name' => 'Urdu',
            ),
            47 =>
            array (
                'id' => 3548,
                'language_code' => 'ur',
                'display_language_code' => 'no',
                'name' => 'Urdu',
            ),
            48 =>
            array (
                'id' => 3549,
                'language_code' => 'ur',
                'display_language_code' => 'pa',
                'name' => 'Urdu',
            ),
            49 =>
            array (
                'id' => 3550,
                'language_code' => 'ur',
                'display_language_code' => 'pl',
                'name' => 'Urdu',
            ),
            50 =>
            array (
                'id' => 3551,
                'language_code' => 'ur',
                'display_language_code' => 'pt-pt',
                'name' => 'Urdu',
            ),
            51 =>
            array (
                'id' => 3552,
                'language_code' => 'ur',
                'display_language_code' => 'pt-br',
                'name' => 'Urdu',
            ),
            52 =>
            array (
                'id' => 3553,
                'language_code' => 'ur',
                'display_language_code' => 'qu',
                'name' => 'Urdu',
            ),
            53 =>
            array (
                'id' => 3554,
                'language_code' => 'ur',
                'display_language_code' => 'ro',
                'name' => 'Urdu',
            ),
            54 =>
            array (
                'id' => 3555,
                'language_code' => 'ur',
                'display_language_code' => 'ru',
                'name' => 'Урду',
            ),
            55 =>
            array (
                'id' => 3556,
                'language_code' => 'ur',
                'display_language_code' => 'sl',
                'name' => 'Urdujščina',
            ),
            56 =>
            array (
                'id' => 3557,
                'language_code' => 'ur',
                'display_language_code' => 'so',
                'name' => 'Urdu',
            ),
            57 =>
            array (
                'id' => 3558,
                'language_code' => 'ur',
                'display_language_code' => 'sq',
                'name' => 'Urdu',
            ),
            58 =>
            array (
                'id' => 3559,
                'language_code' => 'ur',
                'display_language_code' => 'sr',
                'name' => 'урду',
            ),
            59 =>
            array (
                'id' => 3560,
                'language_code' => 'ur',
                'display_language_code' => 'sv',
                'name' => 'Urdu',
            ),
            60 =>
            array (
                'id' => 3561,
                'language_code' => 'ur',
                'display_language_code' => 'ta',
                'name' => 'Urdu',
            ),
            61 =>
            array (
                'id' => 3562,
                'language_code' => 'ur',
                'display_language_code' => 'th',
                'name' => 'อุรดู',
            ),
            62 =>
            array (
                'id' => 3563,
                'language_code' => 'ur',
                'display_language_code' => 'tr',
                'name' => 'Urduca',
            ),
            63 =>
            array (
                'id' => 3564,
                'language_code' => 'ur',
                'display_language_code' => 'uk',
                'name' => 'Urdu',
            ),
            64 =>
            array (
                'id' => 3565,
                'language_code' => 'ur',
                'display_language_code' => 'ur',
                'name' => 'اردو ',
            ),
            65 =>
            array (
                'id' => 3566,
                'language_code' => 'ur',
                'display_language_code' => 'uz',
                'name' => 'Urdu',
            ),
            66 =>
            array (
                'id' => 3567,
                'language_code' => 'ur',
                'display_language_code' => 'vi',
                'name' => 'Urdu',
            ),
            67 =>
            array (
                'id' => 3568,
                'language_code' => 'ur',
                'display_language_code' => 'yi',
                'name' => 'Urdu',
            ),
            68 =>
            array (
                'id' => 3569,
                'language_code' => 'ur',
                'display_language_code' => 'zh-hans',
                'name' => '乌尔都语',
            ),
            69 =>
            array (
                'id' => 3570,
                'language_code' => 'ur',
                'display_language_code' => 'zu',
                'name' => 'Urdu',
            ),
            70 =>
            array (
                'id' => 3571,
                'language_code' => 'ur',
                'display_language_code' => 'zh-hant',
                'name' => '烏爾都語',
            ),
            71 =>
            array (
                'id' => 3572,
                'language_code' => 'ur',
                'display_language_code' => 'ms',
                'name' => 'Urdu',
            ),
            72 =>
            array (
                'id' => 3573,
                'language_code' => 'ur',
                'display_language_code' => 'gl',
                'name' => 'Urdu',
            ),
            73 =>
            array (
                'id' => 3574,
                'language_code' => 'ur',
                'display_language_code' => 'bn',
                'name' => 'Urdu',
            ),
            74 =>
            array (
                'id' => 3575,
                'language_code' => 'ur',
                'display_language_code' => 'az',
                'name' => 'Urdu',
            ),
            75 =>
            array (
                'id' => 3576,
                'language_code' => 'uz',
                'display_language_code' => 'en',
                'name' => 'Uzbek',
            ),
            76 =>
            array (
                'id' => 3577,
                'language_code' => 'uz',
                'display_language_code' => 'es',
                'name' => 'Uzbeko',
            ),
            77 =>
            array (
                'id' => 3578,
                'language_code' => 'uz',
                'display_language_code' => 'de',
                'name' => 'Usbekisch',
            ),
            78 =>
            array (
                'id' => 3579,
                'language_code' => 'uz',
                'display_language_code' => 'fr',
                'name' => 'Ouzbek',
            ),
            79 =>
            array (
                'id' => 3580,
                'language_code' => 'uz',
                'display_language_code' => 'ar',
                'name' => 'الاوزباكية',
            ),
            80 =>
            array (
                'id' => 3581,
                'language_code' => 'uz',
                'display_language_code' => 'bs',
                'name' => 'Uzbek',
            ),
            81 =>
            array (
                'id' => 3582,
                'language_code' => 'uz',
                'display_language_code' => 'bg',
                'name' => 'Узбекски',
            ),
            82 =>
            array (
                'id' => 3583,
                'language_code' => 'uz',
                'display_language_code' => 'ca',
                'name' => 'Uzbek',
            ),
            83 =>
            array (
                'id' => 3584,
                'language_code' => 'uz',
                'display_language_code' => 'cs',
                'name' => 'Uzbek',
            ),
            84 =>
            array (
                'id' => 3585,
                'language_code' => 'uz',
                'display_language_code' => 'sk',
                'name' => 'Uzbekčina',
            ),
            85 =>
            array (
                'id' => 3586,
                'language_code' => 'uz',
                'display_language_code' => 'cy',
                'name' => 'Uzbek',
            ),
            86 =>
            array (
                'id' => 3587,
                'language_code' => 'uz',
                'display_language_code' => 'da',
                'name' => 'Uzbek',
            ),
            87 =>
            array (
                'id' => 3588,
                'language_code' => 'uz',
                'display_language_code' => 'el',
                'name' => 'Ουζμπεκικά',
            ),
            88 =>
            array (
                'id' => 3589,
                'language_code' => 'uz',
                'display_language_code' => 'eo',
                'name' => 'Uzbek',
            ),
            89 =>
            array (
                'id' => 3590,
                'language_code' => 'uz',
                'display_language_code' => 'et',
                'name' => 'Uzbek',
            ),
            90 =>
            array (
                'id' => 3591,
                'language_code' => 'uz',
                'display_language_code' => 'eu',
                'name' => 'Uzbek',
            ),
            91 =>
            array (
                'id' => 3592,
                'language_code' => 'uz',
                'display_language_code' => 'fa',
                'name' => 'Uzbek',
            ),
            92 =>
            array (
                'id' => 3593,
                'language_code' => 'uz',
                'display_language_code' => 'fi',
                'name' => 'Uzbekki',
            ),
            93 =>
            array (
                'id' => 3594,
                'language_code' => 'uz',
                'display_language_code' => 'ga',
                'name' => 'Uzbek',
            ),
            94 =>
            array (
                'id' => 3595,
                'language_code' => 'uz',
                'display_language_code' => 'he',
                'name' => 'אוזבקית',
            ),
            95 =>
            array (
                'id' => 3596,
                'language_code' => 'uz',
                'display_language_code' => 'hi',
                'name' => 'Uzbek',
            ),
            96 =>
            array (
                'id' => 3597,
                'language_code' => 'uz',
                'display_language_code' => 'hr',
                'name' => 'Uzbečki',
            ),
            97 =>
            array (
                'id' => 3598,
                'language_code' => 'uz',
                'display_language_code' => 'hu',
                'name' => 'üzbég',
            ),
            98 =>
            array (
                'id' => 3599,
                'language_code' => 'uz',
                'display_language_code' => 'hy',
                'name' => 'Uzbek',
            ),
            99 =>
            array (
                'id' => 3600,
                'language_code' => 'uz',
                'display_language_code' => 'id',
                'name' => 'Uzbek',
            ),
            100 =>
            array (
                'id' => 3601,
                'language_code' => 'uz',
                'display_language_code' => 'is',
                'name' => 'Uzbek',
            ),
            101 =>
            array (
                'id' => 3602,
                'language_code' => 'uz',
                'display_language_code' => 'it',
                'name' => 'Uzbeco',
            ),
            102 =>
            array (
                'id' => 3603,
                'language_code' => 'uz',
                'display_language_code' => 'ja',
                'name' => 'ウズベク語',
            ),
            103 =>
            array (
                'id' => 3604,
                'language_code' => 'uz',
                'display_language_code' => 'ko',
                'name' => '우즈베크어',
            ),
            104 =>
            array (
                'id' => 3605,
                'language_code' => 'uz',
                'display_language_code' => 'ku',
                'name' => 'Uzbek',
            ),
            105 =>
            array (
                'id' => 3606,
                'language_code' => 'uz',
                'display_language_code' => 'lv',
                'name' => 'Uzbek',
            ),
            106 =>
            array (
                'id' => 3607,
                'language_code' => 'uz',
                'display_language_code' => 'lt',
                'name' => 'Uzbek',
            ),
            107 =>
            array (
                'id' => 3608,
                'language_code' => 'uz',
                'display_language_code' => 'mk',
                'name' => 'Uzbek',
            ),
            108 =>
            array (
                'id' => 3609,
                'language_code' => 'uz',
                'display_language_code' => 'mt',
                'name' => 'Uzbek',
            ),
            109 =>
            array (
                'id' => 3610,
                'language_code' => 'uz',
                'display_language_code' => 'mn',
                'name' => 'Uzbek',
            ),
            110 =>
            array (
                'id' => 3611,
                'language_code' => 'uz',
                'display_language_code' => 'ne',
                'name' => 'Uzbek',
            ),
            111 =>
            array (
                'id' => 3612,
                'language_code' => 'uz',
                'display_language_code' => 'nl',
                'name' => 'Oezbeeks',
            ),
            112 =>
            array (
                'id' => 3613,
                'language_code' => 'uz',
                'display_language_code' => 'no',
                'name' => 'Usbekisk',
            ),
            113 =>
            array (
                'id' => 3614,
                'language_code' => 'uz',
                'display_language_code' => 'pa',
                'name' => 'Uzbek',
            ),
            114 =>
            array (
                'id' => 3615,
                'language_code' => 'uz',
                'display_language_code' => 'pl',
                'name' => 'Uzbecki',
            ),
            115 =>
            array (
                'id' => 3616,
                'language_code' => 'uz',
                'display_language_code' => 'pt-pt',
                'name' => 'Uzbeque',
            ),
            116 =>
            array (
                'id' => 3617,
                'language_code' => 'uz',
                'display_language_code' => 'pt-br',
                'name' => 'Uzbeque',
            ),
            117 =>
            array (
                'id' => 3618,
                'language_code' => 'uz',
                'display_language_code' => 'qu',
                'name' => 'Uzbek',
            ),
            118 =>
            array (
                'id' => 3619,
                'language_code' => 'uz',
                'display_language_code' => 'ro',
                'name' => 'Uzbecă',
            ),
            119 =>
            array (
                'id' => 3620,
                'language_code' => 'uz',
                'display_language_code' => 'ru',
                'name' => 'Узбекский',
            ),
            120 =>
            array (
                'id' => 3621,
                'language_code' => 'uz',
                'display_language_code' => 'sl',
                'name' => 'Uzbek',
            ),
            121 =>
            array (
                'id' => 3622,
                'language_code' => 'uz',
                'display_language_code' => 'so',
                'name' => 'Uzbek',
            ),
            122 =>
            array (
                'id' => 3623,
                'language_code' => 'uz',
                'display_language_code' => 'sq',
                'name' => 'Uzbek',
            ),
            123 =>
            array (
                'id' => 3624,
                'language_code' => 'uz',
                'display_language_code' => 'sr',
                'name' => 'Узбек',
            ),
            124 =>
            array (
                'id' => 3625,
                'language_code' => 'uz',
                'display_language_code' => 'sv',
                'name' => 'Uzbekiska',
            ),
            125 =>
            array (
                'id' => 3626,
                'language_code' => 'uz',
                'display_language_code' => 'ta',
                'name' => 'Uzbek',
            ),
            126 =>
            array (
                'id' => 3627,
                'language_code' => 'uz',
                'display_language_code' => 'th',
                'name' => 'อุซเบก',
            ),
            127 =>
            array (
                'id' => 3628,
                'language_code' => 'uz',
                'display_language_code' => 'tr',
                'name' => 'Özbekçe',
            ),
            128 =>
            array (
                'id' => 3629,
                'language_code' => 'uz',
                'display_language_code' => 'uk',
                'name' => 'Uzbek',
            ),
            129 =>
            array (
                'id' => 3630,
                'language_code' => 'uz',
                'display_language_code' => 'ur',
                'name' => 'Uzbek',
            ),
            130 =>
            array (
                'id' => 3631,
                'language_code' => 'uz',
                'display_language_code' => 'uz',
                'name' => 'Uzbek',
            ),
            131 =>
            array (
                'id' => 3632,
                'language_code' => 'uz',
                'display_language_code' => 'vi',
                'name' => 'Uzbek',
            ),
            132 =>
            array (
                'id' => 3633,
                'language_code' => 'uz',
                'display_language_code' => 'yi',
                'name' => 'Uzbek',
            ),
            133 =>
            array (
                'id' => 3634,
                'language_code' => 'uz',
                'display_language_code' => 'zh-hans',
                'name' => '乌兹别克语',
            ),
            134 =>
            array (
                'id' => 3635,
                'language_code' => 'uz',
                'display_language_code' => 'zu',
                'name' => 'Uzbek',
            ),
            135 =>
            array (
                'id' => 3636,
                'language_code' => 'uz',
                'display_language_code' => 'zh-hant',
                'name' => '烏茲別克語',
            ),
            136 =>
            array (
                'id' => 3637,
                'language_code' => 'uz',
                'display_language_code' => 'ms',
                'name' => 'Uzbek',
            ),
            137 =>
            array (
                'id' => 3638,
                'language_code' => 'uz',
                'display_language_code' => 'gl',
                'name' => 'Uzbek',
            ),
            138 =>
            array (
                'id' => 3639,
                'language_code' => 'uz',
                'display_language_code' => 'bn',
                'name' => 'Uzbek',
            ),
            139 =>
            array (
                'id' => 3640,
                'language_code' => 'uz',
                'display_language_code' => 'az',
                'name' => 'Uzbek',
            ),
            140 =>
            array (
                'id' => 3641,
                'language_code' => 'vi',
                'display_language_code' => 'en',
                'name' => 'Vietnamese',
            ),
            141 =>
            array (
                'id' => 3642,
                'language_code' => 'vi',
                'display_language_code' => 'es',
                'name' => 'Vietnamita',
            ),
            142 =>
            array (
                'id' => 3643,
                'language_code' => 'vi',
                'display_language_code' => 'de',
                'name' => 'Vietnamesisch',
            ),
            143 =>
            array (
                'id' => 3644,
                'language_code' => 'vi',
                'display_language_code' => 'fr',
                'name' => 'Vietnamien',
            ),
            144 =>
            array (
                'id' => 3645,
                'language_code' => 'vi',
                'display_language_code' => 'ar',
                'name' => 'الفيتنامية',
            ),
            145 =>
            array (
                'id' => 3646,
                'language_code' => 'vi',
                'display_language_code' => 'bs',
                'name' => 'Vietnamese',
            ),
            146 =>
            array (
                'id' => 3647,
                'language_code' => 'vi',
                'display_language_code' => 'bg',
                'name' => 'Виетнамски',
            ),
            147 =>
            array (
                'id' => 3648,
                'language_code' => 'vi',
                'display_language_code' => 'ca',
                'name' => 'Vietnamese',
            ),
            148 =>
            array (
                'id' => 3649,
                'language_code' => 'vi',
                'display_language_code' => 'cs',
                'name' => 'Vietnamský',
            ),
            149 =>
            array (
                'id' => 3650,
                'language_code' => 'vi',
                'display_language_code' => 'sk',
                'name' => 'Vietnamčina',
            ),
            150 =>
            array (
                'id' => 3651,
                'language_code' => 'vi',
                'display_language_code' => 'cy',
                'name' => 'Vietnamese',
            ),
            151 =>
            array (
                'id' => 3652,
                'language_code' => 'vi',
                'display_language_code' => 'da',
                'name' => 'Vietnamese',
            ),
            152 =>
            array (
                'id' => 3653,
                'language_code' => 'vi',
                'display_language_code' => 'el',
                'name' => 'Βιετναμέζικα',
            ),
            153 =>
            array (
                'id' => 3654,
                'language_code' => 'vi',
                'display_language_code' => 'eo',
                'name' => 'Vietnamese',
            ),
            154 =>
            array (
                'id' => 3655,
                'language_code' => 'vi',
                'display_language_code' => 'et',
                'name' => 'Vietnamese',
            ),
            155 =>
            array (
                'id' => 3656,
                'language_code' => 'vi',
                'display_language_code' => 'eu',
                'name' => 'Vietnamese',
            ),
            156 =>
            array (
                'id' => 3657,
                'language_code' => 'vi',
                'display_language_code' => 'fa',
                'name' => 'Vietnamese',
            ),
            157 =>
            array (
                'id' => 3658,
                'language_code' => 'vi',
                'display_language_code' => 'fi',
                'name' => 'Vietnam',
            ),
            158 =>
            array (
                'id' => 3659,
                'language_code' => 'vi',
                'display_language_code' => 'ga',
                'name' => 'Vietnamese',
            ),
            159 =>
            array (
                'id' => 3660,
                'language_code' => 'vi',
                'display_language_code' => 'he',
                'name' => 'וייטנאמית',
            ),
            160 =>
            array (
                'id' => 3661,
                'language_code' => 'vi',
                'display_language_code' => 'hi',
                'name' => 'Vietnamese',
            ),
            161 =>
            array (
                'id' => 3662,
                'language_code' => 'vi',
                'display_language_code' => 'hr',
                'name' => 'Vijetnamski',
            ),
            162 =>
            array (
                'id' => 3663,
                'language_code' => 'vi',
                'display_language_code' => 'hu',
                'name' => 'Vietnámi',
            ),
            163 =>
            array (
                'id' => 3664,
                'language_code' => 'vi',
                'display_language_code' => 'hy',
                'name' => 'Vietnamese',
            ),
            164 =>
            array (
                'id' => 3665,
                'language_code' => 'vi',
                'display_language_code' => 'id',
                'name' => 'Vietnamese',
            ),
            165 =>
            array (
                'id' => 3666,
                'language_code' => 'vi',
                'display_language_code' => 'is',
                'name' => 'Vietnamese',
            ),
            166 =>
            array (
                'id' => 3667,
                'language_code' => 'vi',
                'display_language_code' => 'it',
                'name' => 'Vietnamita',
            ),
            167 =>
            array (
                'id' => 3668,
                'language_code' => 'vi',
                'display_language_code' => 'ja',
                'name' => 'ベトナム語',
            ),
            168 =>
            array (
                'id' => 3669,
                'language_code' => 'vi',
                'display_language_code' => 'ko',
                'name' => '베트남어',
            ),
            169 =>
            array (
                'id' => 3670,
                'language_code' => 'vi',
                'display_language_code' => 'ku',
                'name' => 'Vietnamese',
            ),
            170 =>
            array (
                'id' => 3671,
                'language_code' => 'vi',
                'display_language_code' => 'lv',
                'name' => 'Vietnamese',
            ),
            171 =>
            array (
                'id' => 3672,
                'language_code' => 'vi',
                'display_language_code' => 'lt',
                'name' => 'Vietnamese',
            ),
            172 =>
            array (
                'id' => 3673,
                'language_code' => 'vi',
                'display_language_code' => 'mk',
                'name' => 'Vietnamese',
            ),
            173 =>
            array (
                'id' => 3674,
                'language_code' => 'vi',
                'display_language_code' => 'mt',
                'name' => 'Vietnamese',
            ),
            174 =>
            array (
                'id' => 3675,
                'language_code' => 'vi',
                'display_language_code' => 'mn',
                'name' => 'Vietnamese',
            ),
            175 =>
            array (
                'id' => 3676,
                'language_code' => 'vi',
                'display_language_code' => 'ne',
                'name' => 'Vietnamese',
            ),
            176 =>
            array (
                'id' => 3677,
                'language_code' => 'vi',
                'display_language_code' => 'nl',
                'name' => 'Vietnamees',
            ),
            177 =>
            array (
                'id' => 3678,
                'language_code' => 'vi',
                'display_language_code' => 'no',
                'name' => 'Vietnamesisk',
            ),
            178 =>
            array (
                'id' => 3679,
                'language_code' => 'vi',
                'display_language_code' => 'pa',
                'name' => 'Vietnamese',
            ),
            179 =>
            array (
                'id' => 3680,
                'language_code' => 'vi',
                'display_language_code' => 'pl',
                'name' => 'Wietnamski',
            ),
            180 =>
            array (
                'id' => 3681,
                'language_code' => 'vi',
                'display_language_code' => 'pt-pt',
                'name' => 'Vietnamita',
            ),
            181 =>
            array (
                'id' => 3682,
                'language_code' => 'vi',
                'display_language_code' => 'pt-br',
                'name' => 'Vietnamita',
            ),
            182 =>
            array (
                'id' => 3683,
                'language_code' => 'vi',
                'display_language_code' => 'qu',
                'name' => 'Vietnamese',
            ),
            183 =>
            array (
                'id' => 3684,
                'language_code' => 'vi',
                'display_language_code' => 'ro',
                'name' => 'Vietnameză',
            ),
            184 =>
            array (
                'id' => 3685,
                'language_code' => 'vi',
                'display_language_code' => 'ru',
                'name' => 'Вьетнамский',
            ),
            185 =>
            array (
                'id' => 3686,
                'language_code' => 'vi',
                'display_language_code' => 'sl',
                'name' => 'Vietnamščina',
            ),
            186 =>
            array (
                'id' => 3687,
                'language_code' => 'vi',
                'display_language_code' => 'so',
                'name' => 'Vietnamese',
            ),
            187 =>
            array (
                'id' => 3688,
                'language_code' => 'vi',
                'display_language_code' => 'sq',
                'name' => 'Vietnamese',
            ),
            188 =>
            array (
                'id' => 3689,
                'language_code' => 'vi',
                'display_language_code' => 'sr',
                'name' => 'вијетнамски',
            ),
            189 =>
            array (
                'id' => 3690,
                'language_code' => 'vi',
                'display_language_code' => 'sv',
                'name' => 'Vietnamesiska',
            ),
            190 =>
            array (
                'id' => 3691,
                'language_code' => 'vi',
                'display_language_code' => 'ta',
                'name' => 'Vietnamese',
            ),
            191 =>
            array (
                'id' => 3692,
                'language_code' => 'vi',
                'display_language_code' => 'th',
                'name' => 'เวียดนาม',
            ),
            192 =>
            array (
                'id' => 3693,
                'language_code' => 'vi',
                'display_language_code' => 'tr',
                'name' => 'Vietnamca',
            ),
            193 =>
            array (
                'id' => 3694,
                'language_code' => 'vi',
                'display_language_code' => 'uk',
                'name' => 'Vietnamese',
            ),
            194 =>
            array (
                'id' => 3695,
                'language_code' => 'vi',
                'display_language_code' => 'ur',
                'name' => 'Vietnamese',
            ),
            195 =>
            array (
                'id' => 3696,
                'language_code' => 'vi',
                'display_language_code' => 'uz',
                'name' => 'Vietnamese',
            ),
            196 =>
            array (
                'id' => 3697,
                'language_code' => 'vi',
                'display_language_code' => 'vi',
                'name' => 'Tiếng Việt',
            ),
            197 =>
            array (
                'id' => 3698,
                'language_code' => 'vi',
                'display_language_code' => 'yi',
                'name' => 'Vietnamese',
            ),
            198 =>
            array (
                'id' => 3699,
                'language_code' => 'vi',
                'display_language_code' => 'zh-hans',
                'name' => '越南语',
            ),
            199 =>
            array (
                'id' => 3700,
                'language_code' => 'vi',
                'display_language_code' => 'zu',
                'name' => 'Vietnamese',
            ),
            200 =>
            array (
                'id' => 3701,
                'language_code' => 'vi',
                'display_language_code' => 'zh-hant',
                'name' => '越南語',
            ),
            201 =>
            array (
                'id' => 3702,
                'language_code' => 'vi',
                'display_language_code' => 'ms',
                'name' => 'Vietnamese',
            ),
            202 =>
            array (
                'id' => 3703,
                'language_code' => 'vi',
                'display_language_code' => 'gl',
                'name' => 'Vietnamese',
            ),
            203 =>
            array (
                'id' => 3704,
                'language_code' => 'vi',
                'display_language_code' => 'bn',
                'name' => 'Vietnamese',
            ),
            204 =>
            array (
                'id' => 3705,
                'language_code' => 'vi',
                'display_language_code' => 'az',
                'name' => 'Vietnamese',
            ),
            205 =>
            array (
                'id' => 3706,
                'language_code' => 'yi',
                'display_language_code' => 'en',
                'name' => 'Yiddish',
            ),
            206 =>
            array (
                'id' => 3707,
                'language_code' => 'yi',
                'display_language_code' => 'es',
                'name' => 'Yidis',
            ),
            207 =>
            array (
                'id' => 3708,
                'language_code' => 'yi',
                'display_language_code' => 'de',
                'name' => 'Jiddisch',
            ),
            208 =>
            array (
                'id' => 3709,
                'language_code' => 'yi',
                'display_language_code' => 'fr',
                'name' => 'Yiddish',
            ),
            209 =>
            array (
                'id' => 3710,
                'language_code' => 'yi',
                'display_language_code' => 'ar',
                'name' => 'اليديشية',
            ),
            210 =>
            array (
                'id' => 3711,
                'language_code' => 'yi',
                'display_language_code' => 'bs',
                'name' => 'Yiddish',
            ),
            211 =>
            array (
                'id' => 3712,
                'language_code' => 'yi',
                'display_language_code' => 'bg',
                'name' => 'Идиш',
            ),
            212 =>
            array (
                'id' => 3713,
                'language_code' => 'yi',
                'display_language_code' => 'ca',
                'name' => 'Yiddish',
            ),
            213 =>
            array (
                'id' => 3714,
                'language_code' => 'yi',
                'display_language_code' => 'cs',
                'name' => 'Jidiš',
            ),
            214 =>
            array (
                'id' => 3715,
                'language_code' => 'yi',
                'display_language_code' => 'sk',
                'name' => 'Jidiš',
            ),
            215 =>
            array (
                'id' => 3716,
                'language_code' => 'yi',
                'display_language_code' => 'cy',
                'name' => 'Yiddish',
            ),
            216 =>
            array (
                'id' => 3717,
                'language_code' => 'yi',
                'display_language_code' => 'da',
                'name' => 'Yiddish',
            ),
            217 =>
            array (
                'id' => 3718,
                'language_code' => 'yi',
                'display_language_code' => 'el',
                'name' => 'Γίντις',
            ),
            218 =>
            array (
                'id' => 3719,
                'language_code' => 'yi',
                'display_language_code' => 'eo',
                'name' => 'Yiddish',
            ),
            219 =>
            array (
                'id' => 3720,
                'language_code' => 'yi',
                'display_language_code' => 'et',
                'name' => 'Yiddish',
            ),
            220 =>
            array (
                'id' => 3721,
                'language_code' => 'yi',
                'display_language_code' => 'eu',
                'name' => 'Yiddish',
            ),
            221 =>
            array (
                'id' => 3722,
                'language_code' => 'yi',
                'display_language_code' => 'fa',
                'name' => 'Yiddish',
            ),
            222 =>
            array (
                'id' => 3723,
                'language_code' => 'yi',
                'display_language_code' => 'fi',
                'name' => 'Jiddi',
            ),
            223 =>
            array (
                'id' => 3724,
                'language_code' => 'yi',
                'display_language_code' => 'ga',
                'name' => 'Yiddish',
            ),
            224 =>
            array (
                'id' => 3725,
                'language_code' => 'yi',
                'display_language_code' => 'he',
                'name' => 'יידיש',
            ),
            225 =>
            array (
                'id' => 3726,
                'language_code' => 'yi',
                'display_language_code' => 'hi',
                'name' => 'Yiddish',
            ),
            226 =>
            array (
                'id' => 3727,
                'language_code' => 'yi',
                'display_language_code' => 'hr',
                'name' => 'Jidiš',
            ),
            227 =>
            array (
                'id' => 3728,
                'language_code' => 'yi',
                'display_language_code' => 'hu',
                'name' => 'Jiddis',
            ),
            228 =>
            array (
                'id' => 3729,
                'language_code' => 'yi',
                'display_language_code' => 'hy',
                'name' => 'Yiddish',
            ),
            229 =>
            array (
                'id' => 3730,
                'language_code' => 'yi',
                'display_language_code' => 'id',
                'name' => 'Yiddish',
            ),
            230 =>
            array (
                'id' => 3731,
                'language_code' => 'yi',
                'display_language_code' => 'is',
                'name' => 'Yiddish',
            ),
            231 =>
            array (
                'id' => 3732,
                'language_code' => 'yi',
                'display_language_code' => 'it',
                'name' => 'Yiddish',
            ),
            232 =>
            array (
                'id' => 3733,
                'language_code' => 'yi',
                'display_language_code' => 'ja',
                'name' => 'イディッシュ語',
            ),
            233 =>
            array (
                'id' => 3734,
                'language_code' => 'yi',
                'display_language_code' => 'ko',
                'name' => '이디시어',
            ),
            234 =>
            array (
                'id' => 3735,
                'language_code' => 'yi',
                'display_language_code' => 'ku',
                'name' => 'Yiddish',
            ),
            235 =>
            array (
                'id' => 3736,
                'language_code' => 'yi',
                'display_language_code' => 'lv',
                'name' => 'Yiddish',
            ),
            236 =>
            array (
                'id' => 3737,
                'language_code' => 'yi',
                'display_language_code' => 'lt',
                'name' => 'Yiddish',
            ),
            237 =>
            array (
                'id' => 3738,
                'language_code' => 'yi',
                'display_language_code' => 'mk',
                'name' => 'Yiddish',
            ),
            238 =>
            array (
                'id' => 3739,
                'language_code' => 'yi',
                'display_language_code' => 'mt',
                'name' => 'Yiddish',
            ),
            239 =>
            array (
                'id' => 3740,
                'language_code' => 'yi',
                'display_language_code' => 'mn',
                'name' => 'Yiddish',
            ),
            240 =>
            array (
                'id' => 3741,
                'language_code' => 'yi',
                'display_language_code' => 'ne',
                'name' => 'Yiddish',
            ),
            241 =>
            array (
                'id' => 3742,
                'language_code' => 'yi',
                'display_language_code' => 'nl',
                'name' => 'Jiddisch',
            ),
            242 =>
            array (
                'id' => 3743,
                'language_code' => 'yi',
                'display_language_code' => 'no',
                'name' => 'Yiddish',
            ),
            243 =>
            array (
                'id' => 3744,
                'language_code' => 'yi',
                'display_language_code' => 'pa',
                'name' => 'Yiddish',
            ),
            244 =>
            array (
                'id' => 3745,
                'language_code' => 'yi',
                'display_language_code' => 'pl',
                'name' => 'Jidysz',
            ),
            245 =>
            array (
                'id' => 3746,
                'language_code' => 'yi',
                'display_language_code' => 'pt-pt',
                'name' => 'Yiddish',
            ),
            246 =>
            array (
                'id' => 3747,
                'language_code' => 'yi',
                'display_language_code' => 'pt-br',
                'name' => 'Yiddish',
            ),
            247 =>
            array (
                'id' => 3748,
                'language_code' => 'yi',
                'display_language_code' => 'qu',
                'name' => 'Yiddish',
            ),
            248 =>
            array (
                'id' => 3749,
                'language_code' => 'yi',
                'display_language_code' => 'ro',
                'name' => 'Idiş',
            ),
            249 =>
            array (
                'id' => 3750,
                'language_code' => 'yi',
                'display_language_code' => 'ru',
                'name' => 'Идиш',
            ),
            250 =>
            array (
                'id' => 3751,
                'language_code' => 'yi',
                'display_language_code' => 'sl',
                'name' => 'Jidiš',
            ),
            251 =>
            array (
                'id' => 3752,
                'language_code' => 'yi',
                'display_language_code' => 'so',
                'name' => 'Yiddish',
            ),
            252 =>
            array (
                'id' => 3753,
                'language_code' => 'yi',
                'display_language_code' => 'sq',
                'name' => 'Yiddish',
            ),
            253 =>
            array (
                'id' => 3754,
                'language_code' => 'yi',
                'display_language_code' => 'sr',
                'name' => 'јидиш',
            ),
            254 =>
            array (
                'id' => 3755,
                'language_code' => 'yi',
                'display_language_code' => 'sv',
                'name' => 'Jiddisch',
            ),
            255 =>
            array (
                'id' => 3756,
                'language_code' => 'yi',
                'display_language_code' => 'ta',
                'name' => 'Yiddish',
            ),
            256 =>
            array (
                'id' => 3757,
                'language_code' => 'yi',
                'display_language_code' => 'th',
                'name' => 'ยิชดิช',
            ),
            257 =>
            array (
                'id' => 3758,
                'language_code' => 'yi',
                'display_language_code' => 'tr',
                'name' => 'Eski İbranice',
            ),
            258 =>
            array (
                'id' => 3759,
                'language_code' => 'yi',
                'display_language_code' => 'uk',
                'name' => 'Yiddish',
            ),
            259 =>
            array (
                'id' => 3760,
                'language_code' => 'yi',
                'display_language_code' => 'ur',
                'name' => 'Yiddish',
            ),
            260 =>
            array (
                'id' => 3761,
                'language_code' => 'yi',
                'display_language_code' => 'uz',
                'name' => 'Yiddish',
            ),
            261 =>
            array (
                'id' => 3762,
                'language_code' => 'yi',
                'display_language_code' => 'vi',
                'name' => 'Yiddish',
            ),
            262 =>
            array (
                'id' => 3763,
                'language_code' => 'yi',
                'display_language_code' => 'yi',
                'name' => 'Yiddish',
            ),
            263 =>
            array (
                'id' => 3764,
                'language_code' => 'yi',
                'display_language_code' => 'zh-hans',
                'name' => '依地语',
            ),
            264 =>
            array (
                'id' => 3765,
                'language_code' => 'yi',
                'display_language_code' => 'zu',
                'name' => 'Yiddish',
            ),
            265 =>
            array (
                'id' => 3766,
                'language_code' => 'yi',
                'display_language_code' => 'zh-hant',
                'name' => '依地語',
            ),
            266 =>
            array (
                'id' => 3767,
                'language_code' => 'yi',
                'display_language_code' => 'ms',
                'name' => 'Yiddish',
            ),
            267 =>
            array (
                'id' => 3768,
                'language_code' => 'yi',
                'display_language_code' => 'gl',
                'name' => 'Yiddish',
            ),
            268 =>
            array (
                'id' => 3769,
                'language_code' => 'yi',
                'display_language_code' => 'bn',
                'name' => 'Yiddish',
            ),
            269 =>
            array (
                'id' => 3770,
                'language_code' => 'yi',
                'display_language_code' => 'az',
                'name' => 'Yiddish',
            ),
            270 =>
            array (
                'id' => 3771,
                'language_code' => 'zh-hans',
                'display_language_code' => 'en',
            'name' => 'Chinese (Simplified)',
            ),
            271 =>
            array (
                'id' => 3772,
                'language_code' => 'zh-hans',
                'display_language_code' => 'es',
                'name' => 'Chino simplificado',
            ),
            272 =>
            array (
                'id' => 3773,
                'language_code' => 'zh-hans',
                'display_language_code' => 'de',
                'name' => 'Vereinfachtes Chinesisch',
            ),
            273 =>
            array (
                'id' => 3774,
                'language_code' => 'zh-hans',
                'display_language_code' => 'fr',
                'name' => 'Chinois simplifié',
            ),
            274 =>
            array (
                'id' => 3775,
                'language_code' => 'zh-hans',
                'display_language_code' => 'ar',
                'name' => 'الصينية المبسطة',
            ),
            275 =>
            array (
                'id' => 3776,
                'language_code' => 'zh-hans',
                'display_language_code' => 'bs',
            'name' => 'Chinese (Simplified)',
            ),
            276 =>
            array (
                'id' => 3777,
                'language_code' => 'zh-hans',
                'display_language_code' => 'bg',
            'name' => 'Китайски  (опростен)',
            ),
            277 =>
            array (
                'id' => 3778,
                'language_code' => 'zh-hans',
                'display_language_code' => 'ca',
            'name' => 'Chinese (Simplified)',
            ),
            278 =>
            array (
                'id' => 3779,
                'language_code' => 'zh-hans',
                'display_language_code' => 'cs',
            'name' => 'Čínština ( Zjednodušený )',
            ),
            279 =>
            array (
                'id' => 3780,
                'language_code' => 'zh-hans',
                'display_language_code' => 'sk',
                'name' => 'Zjednodušená Čínština',
            ),
            280 =>
            array (
                'id' => 3781,
                'language_code' => 'zh-hans',
                'display_language_code' => 'cy',
            'name' => 'Chinese (Simplified)',
            ),
            281 =>
            array (
                'id' => 3782,
                'language_code' => 'zh-hans',
                'display_language_code' => 'da',
            'name' => 'Chinese (Simplified)',
            ),
            282 =>
            array (
                'id' => 3783,
                'language_code' => 'zh-hans',
                'display_language_code' => 'el',
            'name' => 'Κινεζικά (Απλοποιημένα)',
            ),
            283 =>
            array (
                'id' => 3784,
                'language_code' => 'zh-hans',
                'display_language_code' => 'eo',
            'name' => 'Chinese (Simplified)',
            ),
            284 =>
            array (
                'id' => 3785,
                'language_code' => 'zh-hans',
                'display_language_code' => 'et',
            'name' => 'Chinese (Simplified)',
            ),
            285 =>
            array (
                'id' => 3786,
                'language_code' => 'zh-hans',
                'display_language_code' => 'eu',
            'name' => 'Chinese (Simplified)',
            ),
            286 =>
            array (
                'id' => 3787,
                'language_code' => 'zh-hans',
                'display_language_code' => 'fa',
            'name' => 'Chinese (Simplified)',
            ),
            287 =>
            array (
                'id' => 3788,
                'language_code' => 'zh-hans',
                'display_language_code' => 'fi',
                'name' => 'Kiina',
            ),
            288 =>
            array (
                'id' => 3789,
                'language_code' => 'zh-hans',
                'display_language_code' => 'ga',
            'name' => 'Chinese (Simplified)',
            ),
            289 =>
            array (
                'id' => 3790,
                'language_code' => 'zh-hans',
                'display_language_code' => 'he',
                'name' => 'סינית',
            ),
            290 =>
            array (
                'id' => 3791,
                'language_code' => 'zh-hans',
                'display_language_code' => 'hi',
            'name' => 'Chinese (Simplified)',
            ),
            291 =>
            array (
                'id' => 3792,
                'language_code' => 'zh-hans',
                'display_language_code' => 'hr',
            'name' => 'Kineski (pojednostavljeni)',
            ),
            292 =>
            array (
                'id' => 3793,
                'language_code' => 'zh-hans',
                'display_language_code' => 'hu',
                'name' => 'Egyszerűsített kínai',
            ),
            293 =>
            array (
                'id' => 3794,
                'language_code' => 'zh-hans',
                'display_language_code' => 'hy',
            'name' => 'Chinese (Simplified)',
            ),
            294 =>
            array (
                'id' => 3795,
                'language_code' => 'zh-hans',
                'display_language_code' => 'id',
            'name' => 'Chinese (Simplified)',
            ),
            295 =>
            array (
                'id' => 3796,
                'language_code' => 'zh-hans',
                'display_language_code' => 'is',
            'name' => 'Chinese (Simplified)',
            ),
            296 =>
            array (
                'id' => 3797,
                'language_code' => 'zh-hans',
                'display_language_code' => 'it',
                'name' => 'Cinese semplificato',
            ),
            297 =>
            array (
                'id' => 3798,
                'language_code' => 'zh-hans',
                'display_language_code' => 'ja',
                'name' => '簡体中国語',
            ),
            298 =>
            array (
                'id' => 3799,
                'language_code' => 'zh-hans',
                'display_language_code' => 'ko',
                'name' => '중국어 간체',
            ),
            299 =>
            array (
                'id' => 3800,
                'language_code' => 'zh-hans',
                'display_language_code' => 'ku',
            'name' => 'Chinese (Simplified)',
            ),
            300 =>
            array (
                'id' => 3801,
                'language_code' => 'zh-hans',
                'display_language_code' => 'lv',
            'name' => 'Chinese (Simplified)',
            ),
            301 =>
            array (
                'id' => 3802,
                'language_code' => 'zh-hans',
                'display_language_code' => 'lt',
            'name' => 'Chinese (Simplified)',
            ),
            302 =>
            array (
                'id' => 3803,
                'language_code' => 'zh-hans',
                'display_language_code' => 'mk',
            'name' => 'Chinese (Simplified)',
            ),
            303 =>
            array (
                'id' => 3804,
                'language_code' => 'zh-hans',
                'display_language_code' => 'mt',
            'name' => 'Chinese (Simplified)',
            ),
            304 =>
            array (
                'id' => 3805,
                'language_code' => 'zh-hans',
                'display_language_code' => 'mn',
            'name' => 'Chinese (Simplified)',
            ),
            305 =>
            array (
                'id' => 3806,
                'language_code' => 'zh-hans',
                'display_language_code' => 'ne',
            'name' => 'Chinese (Simplified)',
            ),
            306 =>
            array (
                'id' => 3807,
                'language_code' => 'zh-hans',
                'display_language_code' => 'nl',
                'name' => 'Vereenvoudigd Chinees',
            ),
            307 =>
            array (
                'id' => 3808,
                'language_code' => 'zh-hans',
                'display_language_code' => 'no',
            'name' => 'Kinesisk (forenklet)',
            ),
            308 =>
            array (
                'id' => 3809,
                'language_code' => 'zh-hans',
                'display_language_code' => 'pa',
            'name' => 'Chinese (Simplified)',
            ),
            309 =>
            array (
                'id' => 3810,
                'language_code' => 'zh-hans',
                'display_language_code' => 'pl',
                'name' => 'Chiński uproszczony',
            ),
            310 =>
            array (
                'id' => 3811,
                'language_code' => 'zh-hans',
                'display_language_code' => 'pt-pt',
            'name' => 'Chinês (Simplificado)',
            ),
            311 =>
            array (
                'id' => 3812,
                'language_code' => 'zh-hans',
                'display_language_code' => 'pt-br',
            'name' => 'Chinês (Simplificado)',
            ),
            312 =>
            array (
                'id' => 3813,
                'language_code' => 'zh-hans',
                'display_language_code' => 'qu',
            'name' => 'Chinese (Simplified)',
            ),
            313 =>
            array (
                'id' => 3814,
                'language_code' => 'zh-hans',
                'display_language_code' => 'ro',
                'name' => 'Chineza simplificată',
            ),
            314 =>
            array (
                'id' => 3815,
                'language_code' => 'zh-hans',
                'display_language_code' => 'ru',
            'name' => 'Китайский (упрощенный)',
            ),
            315 =>
            array (
                'id' => 3816,
                'language_code' => 'zh-hans',
                'display_language_code' => 'sl',
            'name' => 'Kitajščina (poenostavljena )',
            ),
            316 =>
            array (
                'id' => 3817,
                'language_code' => 'zh-hans',
                'display_language_code' => 'so',
            'name' => 'Chinese (Simplified)',
            ),
            317 =>
            array (
                'id' => 3818,
                'language_code' => 'zh-hans',
                'display_language_code' => 'sq',
            'name' => 'Chinese (Simplified)',
            ),
            318 =>
            array (
                'id' => 3819,
                'language_code' => 'zh-hans',
                'display_language_code' => 'sr',
            'name' => 'Кинески (поједностављени)',
            ),
            319 =>
            array (
                'id' => 3820,
                'language_code' => 'zh-hans',
                'display_language_code' => 'sv',
                'name' => 'Förenklad kinesiska',
            ),
            320 =>
            array (
                'id' => 3821,
                'language_code' => 'zh-hans',
                'display_language_code' => 'ta',
            'name' => 'Chinese (Simplified)',
            ),
            321 =>
            array (
                'id' => 3822,
                'language_code' => 'zh-hans',
                'display_language_code' => 'th',
                'name' => 'จีนประยุกต์',
            ),
            322 =>
            array (
                'id' => 3823,
                'language_code' => 'zh-hans',
                'display_language_code' => 'tr',
                'name' => 'Modern Çince',
            ),
            323 =>
            array (
                'id' => 3824,
                'language_code' => 'zh-hans',
                'display_language_code' => 'uk',
            'name' => 'Chinese (Simplified)',
            ),
            324 =>
            array (
                'id' => 3825,
                'language_code' => 'zh-hans',
                'display_language_code' => 'ur',
            'name' => 'Chinese (Simplified)',
            ),
            325 =>
            array (
                'id' => 3826,
                'language_code' => 'zh-hans',
                'display_language_code' => 'uz',
            'name' => 'Chinese (Simplified)',
            ),
            326 =>
            array (
                'id' => 3827,
                'language_code' => 'zh-hans',
                'display_language_code' => 'vi',
            'name' => 'Chinese (Simplified)',
            ),
            327 =>
            array (
                'id' => 3828,
                'language_code' => 'zh-hans',
                'display_language_code' => 'yi',
            'name' => 'Chinese (Simplified)',
            ),
            328 =>
            array (
                'id' => 3829,
                'language_code' => 'zh-hans',
                'display_language_code' => 'zh-hans',
                'name' => '简体中文',
            ),
            329 =>
            array (
                'id' => 3830,
                'language_code' => 'zh-hans',
                'display_language_code' => 'zu',
            'name' => 'Chinese (Simplified)',
            ),
            330 =>
            array (
                'id' => 3831,
                'language_code' => 'zh-hans',
                'display_language_code' => 'zh-hant',
                'name' => '簡體中文',
            ),
            331 =>
            array (
                'id' => 3832,
                'language_code' => 'zh-hans',
                'display_language_code' => 'ms',
            'name' => 'Chinese (Simplified)',
            ),
            332 =>
            array (
                'id' => 3833,
                'language_code' => 'zh-hans',
                'display_language_code' => 'gl',
            'name' => 'Chinese (Simplified)',
            ),
            333 =>
            array (
                'id' => 3834,
                'language_code' => 'zh-hans',
                'display_language_code' => 'bn',
            'name' => 'Chinese (Simplified)',
            ),
            334 =>
            array (
                'id' => 3835,
                'language_code' => 'zh-hans',
                'display_language_code' => 'az',
            'name' => 'Chinese (Simplified)',
            ),
            335 =>
            array (
                'id' => 3836,
                'language_code' => 'zu',
                'display_language_code' => 'en',
                'name' => 'Zulu',
            ),
            336 =>
            array (
                'id' => 3837,
                'language_code' => 'zu',
                'display_language_code' => 'es',
                'name' => 'Zulú',
            ),
            337 =>
            array (
                'id' => 3838,
                'language_code' => 'zu',
                'display_language_code' => 'de',
                'name' => 'Zulu',
            ),
            338 =>
            array (
                'id' => 3839,
                'language_code' => 'zu',
                'display_language_code' => 'fr',
                'name' => 'Zoulou',
            ),
            339 =>
            array (
                'id' => 3840,
                'language_code' => 'zu',
                'display_language_code' => 'ar',
                'name' => 'الزولو',
            ),
            340 =>
            array (
                'id' => 3841,
                'language_code' => 'zu',
                'display_language_code' => 'bs',
                'name' => 'Zulu',
            ),
            341 =>
            array (
                'id' => 3842,
                'language_code' => 'zu',
                'display_language_code' => 'bg',
                'name' => 'Зулу',
            ),
            342 =>
            array (
                'id' => 3843,
                'language_code' => 'zu',
                'display_language_code' => 'ca',
                'name' => 'Zulu',
            ),
            343 =>
            array (
                'id' => 3844,
                'language_code' => 'zu',
                'display_language_code' => 'cs',
                'name' => 'Zulu',
            ),
            344 =>
            array (
                'id' => 3845,
                'language_code' => 'zu',
                'display_language_code' => 'sk',
                'name' => 'Jazyk Zulu',
            ),
            345 =>
            array (
                'id' => 3846,
                'language_code' => 'zu',
                'display_language_code' => 'cy',
                'name' => 'Zulu',
            ),
            346 =>
            array (
                'id' => 3847,
                'language_code' => 'zu',
                'display_language_code' => 'da',
                'name' => 'Zulu',
            ),
            347 =>
            array (
                'id' => 3848,
                'language_code' => 'zu',
                'display_language_code' => 'el',
                'name' => 'Ζουλού',
            ),
            348 =>
            array (
                'id' => 3849,
                'language_code' => 'zu',
                'display_language_code' => 'eo',
                'name' => 'Zulu',
            ),
            349 =>
            array (
                'id' => 3850,
                'language_code' => 'zu',
                'display_language_code' => 'et',
                'name' => 'Zulu',
            ),
            350 =>
            array (
                'id' => 3851,
                'language_code' => 'zu',
                'display_language_code' => 'eu',
                'name' => 'Zulu',
            ),
            351 =>
            array (
                'id' => 3852,
                'language_code' => 'zu',
                'display_language_code' => 'fa',
                'name' => 'Zulu',
            ),
            352 =>
            array (
                'id' => 3853,
                'language_code' => 'zu',
                'display_language_code' => 'fi',
                'name' => 'Zulu',
            ),
            353 =>
            array (
                'id' => 3854,
                'language_code' => 'zu',
                'display_language_code' => 'ga',
                'name' => 'Zulu',
            ),
            354 =>
            array (
                'id' => 3855,
                'language_code' => 'zu',
                'display_language_code' => 'he',
                'name' => 'זולו ',
            ),
            355 =>
            array (
                'id' => 3856,
                'language_code' => 'zu',
                'display_language_code' => 'hi',
                'name' => 'Zulu',
            ),
            356 =>
            array (
                'id' => 3857,
                'language_code' => 'zu',
                'display_language_code' => 'hr',
                'name' => 'Zulu',
            ),
            357 =>
            array (
                'id' => 3858,
                'language_code' => 'zu',
                'display_language_code' => 'hu',
                'name' => 'Zulu',
            ),
            358 =>
            array (
                'id' => 3859,
                'language_code' => 'zu',
                'display_language_code' => 'hy',
                'name' => 'Zulu',
            ),
            359 =>
            array (
                'id' => 3860,
                'language_code' => 'zu',
                'display_language_code' => 'id',
                'name' => 'Zulu',
            ),
            360 =>
            array (
                'id' => 3861,
                'language_code' => 'zu',
                'display_language_code' => 'is',
                'name' => 'Zulu',
            ),
            361 =>
            array (
                'id' => 3862,
                'language_code' => 'zu',
                'display_language_code' => 'it',
                'name' => 'Zulu',
            ),
            362 =>
            array (
                'id' => 3863,
                'language_code' => 'zu',
                'display_language_code' => 'ja',
                'name' => 'ズールー語',
            ),
            363 =>
            array (
                'id' => 3864,
                'language_code' => 'zu',
                'display_language_code' => 'ko',
                'name' => '줄루어',
            ),
            364 =>
            array (
                'id' => 3865,
                'language_code' => 'zu',
                'display_language_code' => 'ku',
                'name' => 'Zulu',
            ),
            365 =>
            array (
                'id' => 3866,
                'language_code' => 'zu',
                'display_language_code' => 'lv',
                'name' => 'Zulu',
            ),
            366 =>
            array (
                'id' => 3867,
                'language_code' => 'zu',
                'display_language_code' => 'lt',
                'name' => 'Zulu',
            ),
            367 =>
            array (
                'id' => 3868,
                'language_code' => 'zu',
                'display_language_code' => 'mk',
                'name' => 'Zulu',
            ),
            368 =>
            array (
                'id' => 3869,
                'language_code' => 'zu',
                'display_language_code' => 'mt',
                'name' => 'Zulu',
            ),
            369 =>
            array (
                'id' => 3870,
                'language_code' => 'zu',
                'display_language_code' => 'mn',
                'name' => 'Zulu',
            ),
            370 =>
            array (
                'id' => 3871,
                'language_code' => 'zu',
                'display_language_code' => 'ne',
                'name' => 'Zulu',
            ),
            371 =>
            array (
                'id' => 3872,
                'language_code' => 'zu',
                'display_language_code' => 'nl',
                'name' => 'Zulu',
            ),
            372 =>
            array (
                'id' => 3873,
                'language_code' => 'zu',
                'display_language_code' => 'no',
                'name' => 'Zulu',
            ),
            373 =>
            array (
                'id' => 3874,
                'language_code' => 'zu',
                'display_language_code' => 'pa',
                'name' => 'Zulu',
            ),
            374 =>
            array (
                'id' => 3875,
                'language_code' => 'zu',
                'display_language_code' => 'pl',
                'name' => 'Zuluski',
            ),
            375 =>
            array (
                'id' => 3876,
                'language_code' => 'zu',
                'display_language_code' => 'pt-pt',
                'name' => 'Zulu',
            ),
            376 =>
            array (
                'id' => 3877,
                'language_code' => 'zu',
                'display_language_code' => 'pt-br',
                'name' => 'Zulu',
            ),
            377 =>
            array (
                'id' => 3878,
                'language_code' => 'zu',
                'display_language_code' => 'qu',
                'name' => 'Zulu',
            ),
            378 =>
            array (
                'id' => 3879,
                'language_code' => 'zu',
                'display_language_code' => 'ro',
                'name' => 'Zulu',
            ),
            379 =>
            array (
                'id' => 3880,
                'language_code' => 'zu',
                'display_language_code' => 'ru',
                'name' => 'Зулу',
            ),
            380 =>
            array (
                'id' => 3881,
                'language_code' => 'zu',
                'display_language_code' => 'sl',
                'name' => 'Zulu',
            ),
            381 =>
            array (
                'id' => 3882,
                'language_code' => 'zu',
                'display_language_code' => 'so',
                'name' => 'Zulu',
            ),
            382 =>
            array (
                'id' => 3883,
                'language_code' => 'zu',
                'display_language_code' => 'sq',
                'name' => 'Zulu',
            ),
            383 =>
            array (
                'id' => 3884,
                'language_code' => 'zu',
                'display_language_code' => 'sr',
                'name' => 'зулу',
            ),
            384 =>
            array (
                'id' => 3885,
                'language_code' => 'zu',
                'display_language_code' => 'sv',
                'name' => 'Zulu',
            ),
            385 =>
            array (
                'id' => 3886,
                'language_code' => 'zu',
                'display_language_code' => 'ta',
                'name' => 'Zulu',
            ),
            386 =>
            array (
                'id' => 3887,
                'language_code' => 'zu',
                'display_language_code' => 'th',
                'name' => 'ซูลู',
            ),
            387 =>
            array (
                'id' => 3888,
                'language_code' => 'zu',
                'display_language_code' => 'tr',
                'name' => 'Zulu dili',
            ),
            388 =>
            array (
                'id' => 3889,
                'language_code' => 'zu',
                'display_language_code' => 'uk',
                'name' => 'Zulu',
            ),
            389 =>
            array (
                'id' => 3890,
                'language_code' => 'zu',
                'display_language_code' => 'ur',
                'name' => 'Zulu',
            ),
            390 =>
            array (
                'id' => 3891,
                'language_code' => 'zu',
                'display_language_code' => 'uz',
                'name' => 'Zulu',
            ),
            391 =>
            array (
                'id' => 3892,
                'language_code' => 'zu',
                'display_language_code' => 'vi',
                'name' => 'Zulu',
            ),
            392 =>
            array (
                'id' => 3893,
                'language_code' => 'zu',
                'display_language_code' => 'yi',
                'name' => 'Zulu',
            ),
            393 =>
            array (
                'id' => 3894,
                'language_code' => 'zu',
                'display_language_code' => 'zh-hans',
                'name' => '祖鲁语',
            ),
            394 =>
            array (
                'id' => 3895,
                'language_code' => 'zu',
                'display_language_code' => 'zu',
                'name' => 'Zulu',
            ),
            395 =>
            array (
                'id' => 3896,
                'language_code' => 'zu',
                'display_language_code' => 'zh-hant',
                'name' => '祖魯語',
            ),
            396 =>
            array (
                'id' => 3897,
                'language_code' => 'zu',
                'display_language_code' => 'ms',
                'name' => 'Zulu',
            ),
            397 =>
            array (
                'id' => 3898,
                'language_code' => 'zu',
                'display_language_code' => 'gl',
                'name' => 'Zulu',
            ),
            398 =>
            array (
                'id' => 3899,
                'language_code' => 'zu',
                'display_language_code' => 'bn',
                'name' => 'Zulu',
            ),
            399 =>
            array (
                'id' => 3900,
                'language_code' => 'zu',
                'display_language_code' => 'az',
                'name' => 'Zulu',
            ),
            400 =>
            array (
                'id' => 3901,
                'language_code' => 'zh-hant',
                'display_language_code' => 'en',
            'name' => 'Chinese (Traditional)',
            ),
            401 =>
            array (
                'id' => 3902,
                'language_code' => 'zh-hant',
                'display_language_code' => 'es',
                'name' => 'Chino tradicional',
            ),
            402 =>
            array (
                'id' => 3903,
                'language_code' => 'zh-hant',
                'display_language_code' => 'de',
                'name' => 'Traditionelles Chinesisch',
            ),
            403 =>
            array (
                'id' => 3904,
                'language_code' => 'zh-hant',
                'display_language_code' => 'fr',
                'name' => 'Chinois traditionnel',
            ),
            404 =>
            array (
                'id' => 3905,
                'language_code' => 'zh-hant',
                'display_language_code' => 'ar',
                'name' => 'الصينية التقليدية',
            ),
            405 =>
            array (
                'id' => 3906,
                'language_code' => 'zh-hant',
                'display_language_code' => 'bs',
            'name' => 'Chinese (Traditional)',
            ),
            406 =>
            array (
                'id' => 3907,
                'language_code' => 'zh-hant',
                'display_language_code' => 'bg',
            'name' => 'Китайски (традиционен)',
            ),
            407 =>
            array (
                'id' => 3908,
                'language_code' => 'zh-hant',
                'display_language_code' => 'ca',
            'name' => 'Chinese (Traditional)',
            ),
            408 =>
            array (
                'id' => 3909,
                'language_code' => 'zh-hant',
                'display_language_code' => 'cs',
            'name' => 'Čínština (tradiční )',
            ),
            409 =>
            array (
                'id' => 3910,
                'language_code' => 'zh-hant',
                'display_language_code' => 'sk',
                'name' => 'Tradičná Čínština',
            ),
            410 =>
            array (
                'id' => 3911,
                'language_code' => 'zh-hant',
                'display_language_code' => 'cy',
            'name' => 'Chinese (Traditional)',
            ),
            411 =>
            array (
                'id' => 3912,
                'language_code' => 'zh-hant',
                'display_language_code' => 'da',
            'name' => 'Chinese (Traditional)',
            ),
            412 =>
            array (
                'id' => 3913,
                'language_code' => 'zh-hant',
                'display_language_code' => 'el',
            'name' => 'Κινεζικά (Παραδοσιακά)',
            ),
            413 =>
            array (
                'id' => 3914,
                'language_code' => 'zh-hant',
                'display_language_code' => 'eo',
            'name' => 'Chinese (Traditional)',
            ),
            414 =>
            array (
                'id' => 3915,
                'language_code' => 'zh-hant',
                'display_language_code' => 'et',
            'name' => 'Chinese (Traditional)',
            ),
            415 =>
            array (
                'id' => 3916,
                'language_code' => 'zh-hant',
                'display_language_code' => 'eu',
            'name' => 'Chinese (Traditional)',
            ),
            416 =>
            array (
                'id' => 3917,
                'language_code' => 'zh-hant',
                'display_language_code' => 'fa',
            'name' => 'Chinese (Traditional)',
            ),
            417 =>
            array (
                'id' => 3918,
                'language_code' => 'zh-hant',
                'display_language_code' => 'fi',
                'name' => 'Perinteinen kiina',
            ),
            418 =>
            array (
                'id' => 3919,
                'language_code' => 'zh-hant',
                'display_language_code' => 'ga',
            'name' => 'Chinese (Traditional)',
            ),
            419 =>
            array (
                'id' => 3920,
                'language_code' => 'zh-hant',
                'display_language_code' => 'he',
                'name' => 'סינית מסורתית',
            ),
            420 =>
            array (
                'id' => 3921,
                'language_code' => 'zh-hant',
                'display_language_code' => 'hi',
            'name' => 'Chinese (Traditional)',
            ),
            421 =>
            array (
                'id' => 3922,
                'language_code' => 'zh-hant',
                'display_language_code' => 'hr',
            'name' => 'Kineski (tradicionalni)',
            ),
            422 =>
            array (
                'id' => 3923,
                'language_code' => 'zh-hant',
                'display_language_code' => 'hu',
                'name' => 'Hagyományos kínai',
            ),
            423 =>
            array (
                'id' => 3924,
                'language_code' => 'zh-hant',
                'display_language_code' => 'hy',
            'name' => 'Chinese (Traditional)',
            ),
            424 =>
            array (
                'id' => 3925,
                'language_code' => 'zh-hant',
                'display_language_code' => 'id',
            'name' => 'Chinese (Traditional)',
            ),
            425 =>
            array (
                'id' => 3926,
                'language_code' => 'zh-hant',
                'display_language_code' => 'is',
            'name' => 'Chinese (Traditional)',
            ),
            426 =>
            array (
                'id' => 3927,
                'language_code' => 'zh-hant',
                'display_language_code' => 'it',
                'name' => 'Cinese tradizionale',
            ),
            427 =>
            array (
                'id' => 3928,
                'language_code' => 'zh-hant',
                'display_language_code' => 'ja',
                'name' => '繁体中国語',
            ),
            428 =>
            array (
                'id' => 3929,
                'language_code' => 'zh-hant',
                'display_language_code' => 'ko',
                'name' => '중국어 번체',
            ),
            429 =>
            array (
                'id' => 3930,
                'language_code' => 'zh-hant',
                'display_language_code' => 'ku',
            'name' => 'Chinese (Traditional)',
            ),
            430 =>
            array (
                'id' => 3931,
                'language_code' => 'zh-hant',
                'display_language_code' => 'lv',
            'name' => 'Chinese (Traditional)',
            ),
            431 =>
            array (
                'id' => 3932,
                'language_code' => 'zh-hant',
                'display_language_code' => 'lt',
            'name' => 'Chinese (Traditional)',
            ),
            432 =>
            array (
                'id' => 3933,
                'language_code' => 'zh-hant',
                'display_language_code' => 'mk',
            'name' => 'Chinese (Traditional)',
            ),
            433 =>
            array (
                'id' => 3934,
                'language_code' => 'zh-hant',
                'display_language_code' => 'mt',
            'name' => 'Chinese (Traditional)',
            ),
            434 =>
            array (
                'id' => 3935,
                'language_code' => 'zh-hant',
                'display_language_code' => 'mn',
            'name' => 'Chinese (Traditional)',
            ),
            435 =>
            array (
                'id' => 3936,
                'language_code' => 'zh-hant',
                'display_language_code' => 'ne',
            'name' => 'Chinese (Traditional)',
            ),
            436 =>
            array (
                'id' => 3937,
                'language_code' => 'zh-hant',
                'display_language_code' => 'nl',
                'name' => 'Traditioneel Chinees',
            ),
            437 =>
            array (
                'id' => 3938,
                'language_code' => 'zh-hant',
                'display_language_code' => 'no',
            'name' => 'Kinesisk (tradisjonell)',
            ),
            438 =>
            array (
                'id' => 3939,
                'language_code' => 'zh-hant',
                'display_language_code' => 'pa',
            'name' => 'Chinese (Traditional)',
            ),
            439 =>
            array (
                'id' => 3940,
                'language_code' => 'zh-hant',
                'display_language_code' => 'pl',
                'name' => 'Chiński tradycyjny',
            ),
            440 =>
            array (
                'id' => 3941,
                'language_code' => 'zh-hant',
                'display_language_code' => 'pt-pt',
            'name' => 'Chinês (Tradicional)',
            ),
            441 =>
            array (
                'id' => 3942,
                'language_code' => 'zh-hant',
                'display_language_code' => 'pt-br',
            'name' => 'Chinês (Tradicional)',
            ),
            442 =>
            array (
                'id' => 3943,
                'language_code' => 'zh-hant',
                'display_language_code' => 'qu',
            'name' => 'Chinese (Traditional)',
            ),
            443 =>
            array (
                'id' => 3944,
                'language_code' => 'zh-hant',
                'display_language_code' => 'ro',
                'name' => 'Chineza tradiţională',
            ),
            444 =>
            array (
                'id' => 3945,
                'language_code' => 'zh-hant',
                'display_language_code' => 'ru',
            'name' => 'Китайский (традиционный)',
            ),
            445 =>
            array (
                'id' => 3946,
                'language_code' => 'zh-hant',
                'display_language_code' => 'sl',
            'name' => 'Kitajščina (tradicionalna)',
            ),
            446 =>
            array (
                'id' => 3947,
                'language_code' => 'zh-hant',
                'display_language_code' => 'so',
            'name' => 'Chinese (Traditional)',
            ),
            447 =>
            array (
                'id' => 3948,
                'language_code' => 'zh-hant',
                'display_language_code' => 'sq',
            'name' => 'Chinese (Traditional)',
            ),
            448 =>
            array (
                'id' => 3949,
                'language_code' => 'zh-hant',
                'display_language_code' => 'sr',
            'name' => 'Кинески (традиционални)',
            ),
            449 =>
            array (
                'id' => 3950,
                'language_code' => 'zh-hant',
                'display_language_code' => 'sv',
                'name' => 'Traditionell kinesiska',
            ),
            450 =>
            array (
                'id' => 3951,
                'language_code' => 'zh-hant',
                'display_language_code' => 'ta',
            'name' => 'Chinese (Traditional)',
            ),
            451 =>
            array (
                'id' => 3952,
                'language_code' => 'zh-hant',
                'display_language_code' => 'th',
                'name' => 'จีนดั้งเดิม',
            ),
            452 =>
            array (
                'id' => 3953,
                'language_code' => 'zh-hant',
                'display_language_code' => 'tr',
                'name' => 'Klasik Çince',
            ),
            453 =>
            array (
                'id' => 3954,
                'language_code' => 'zh-hant',
                'display_language_code' => 'uk',
            'name' => 'Chinese (Traditional)',
            ),
            454 =>
            array (
                'id' => 3955,
                'language_code' => 'zh-hant',
                'display_language_code' => 'ur',
            'name' => 'Chinese (Traditional)',
            ),
            455 =>
            array (
                'id' => 3956,
                'language_code' => 'zh-hant',
                'display_language_code' => 'uz',
            'name' => 'Chinese (Traditional)',
            ),
            456 =>
            array (
                'id' => 3957,
                'language_code' => 'zh-hant',
                'display_language_code' => 'vi',
            'name' => 'Chinese (Traditional)',
            ),
            457 =>
            array (
                'id' => 3958,
                'language_code' => 'zh-hant',
                'display_language_code' => 'yi',
            'name' => 'Chinese (Traditional)',
            ),
            458 =>
            array (
                'id' => 3959,
                'language_code' => 'zh-hant',
                'display_language_code' => 'zh-hans',
                'name' => '繁体中文',
            ),
            459 =>
            array (
                'id' => 3960,
                'language_code' => 'zh-hant',
                'display_language_code' => 'zu',
            'name' => 'Chinese (Traditional)',
            ),
            460 =>
            array (
                'id' => 3961,
                'language_code' => 'zh-hant',
                'display_language_code' => 'zh-hant',
                'name' => '繁體中文',
            ),
            461 =>
            array (
                'id' => 3962,
                'language_code' => 'zh-hant',
                'display_language_code' => 'ms',
            'name' => 'Chinese (Traditional)',
            ),
            462 =>
            array (
                'id' => 3963,
                'language_code' => 'zh-hant',
                'display_language_code' => 'gl',
            'name' => 'Chinese (Traditional)',
            ),
            463 =>
            array (
                'id' => 3964,
                'language_code' => 'zh-hant',
                'display_language_code' => 'bn',
            'name' => 'Chinese (Traditional)',
            ),
            464 =>
            array (
                'id' => 3965,
                'language_code' => 'zh-hant',
                'display_language_code' => 'az',
            'name' => 'Chinese (Traditional)',
            ),
            465 =>
            array (
                'id' => 3966,
                'language_code' => 'ms',
                'display_language_code' => 'en',
                'name' => 'Malay',
            ),
            466 =>
            array (
                'id' => 3967,
                'language_code' => 'ms',
                'display_language_code' => 'es',
                'name' => 'Malayo',
            ),
            467 =>
            array (
                'id' => 3968,
                'language_code' => 'ms',
                'display_language_code' => 'de',
                'name' => 'Malaiisch',
            ),
            468 =>
            array (
                'id' => 3969,
                'language_code' => 'ms',
                'display_language_code' => 'fr',
                'name' => 'Malay',
            ),
            469 =>
            array (
                'id' => 3970,
                'language_code' => 'ms',
                'display_language_code' => 'ar',
                'name' => 'لغة الملايو',
            ),
            470 =>
            array (
                'id' => 3971,
                'language_code' => 'ms',
                'display_language_code' => 'bs',
                'name' => 'Malay',
            ),
            471 =>
            array (
                'id' => 3972,
                'language_code' => 'ms',
                'display_language_code' => 'bg',
                'name' => 'Малайски',
            ),
            472 =>
            array (
                'id' => 3973,
                'language_code' => 'ms',
                'display_language_code' => 'ca',
                'name' => 'Malay',
            ),
            473 =>
            array (
                'id' => 3974,
                'language_code' => 'ms',
                'display_language_code' => 'cs',
                'name' => 'Malajský',
            ),
            474 =>
            array (
                'id' => 3975,
                'language_code' => 'ms',
                'display_language_code' => 'sk',
                'name' => 'Malay',
            ),
            475 =>
            array (
                'id' => 3976,
                'language_code' => 'ms',
                'display_language_code' => 'cy',
                'name' => 'Malay',
            ),
            476 =>
            array (
                'id' => 3977,
                'language_code' => 'ms',
                'display_language_code' => 'da',
                'name' => 'Malay',
            ),
            477 =>
            array (
                'id' => 3978,
                'language_code' => 'ms',
                'display_language_code' => 'el',
                'name' => 'Malay',
            ),
            478 =>
            array (
                'id' => 3979,
                'language_code' => 'ms',
                'display_language_code' => 'eo',
                'name' => 'Malay',
            ),
            479 =>
            array (
                'id' => 3980,
                'language_code' => 'ms',
                'display_language_code' => 'et',
                'name' => 'Malay',
            ),
            480 =>
            array (
                'id' => 3981,
                'language_code' => 'ms',
                'display_language_code' => 'eu',
                'name' => 'Malay',
            ),
            481 =>
            array (
                'id' => 3982,
                'language_code' => 'ms',
                'display_language_code' => 'fa',
                'name' => 'Malay',
            ),
            482 =>
            array (
                'id' => 3983,
                'language_code' => 'ms',
                'display_language_code' => 'fi',
                'name' => 'Malay',
            ),
            483 =>
            array (
                'id' => 3984,
                'language_code' => 'ms',
                'display_language_code' => 'ga',
                'name' => 'Malay',
            ),
            484 =>
            array (
                'id' => 3985,
                'language_code' => 'ms',
                'display_language_code' => 'he',
                'name' => 'מלאית',
            ),
            485 =>
            array (
                'id' => 3986,
                'language_code' => 'ms',
                'display_language_code' => 'hi',
                'name' => 'Malay',
            ),
            486 =>
            array (
                'id' => 3987,
                'language_code' => 'ms',
                'display_language_code' => 'hr',
                'name' => 'Malajski',
            ),
            487 =>
            array (
                'id' => 3988,
                'language_code' => 'ms',
                'display_language_code' => 'hu',
                'name' => 'Malay',
            ),
            488 =>
            array (
                'id' => 3989,
                'language_code' => 'ms',
                'display_language_code' => 'hy',
                'name' => 'Malay',
            ),
            489 =>
            array (
                'id' => 3990,
                'language_code' => 'ms',
                'display_language_code' => 'id',
                'name' => 'Malay',
            ),
            490 =>
            array (
                'id' => 3991,
                'language_code' => 'ms',
                'display_language_code' => 'is',
                'name' => 'Malay',
            ),
            491 =>
            array (
                'id' => 3992,
                'language_code' => 'ms',
                'display_language_code' => 'it',
                'name' => 'Malay',
            ),
            492 =>
            array (
                'id' => 3993,
                'language_code' => 'ms',
                'display_language_code' => 'ja',
                'name' => 'Malay',
            ),
            493 =>
            array (
                'id' => 3994,
                'language_code' => 'ms',
                'display_language_code' => 'ko',
                'name' => 'Malay',
            ),
            494 =>
            array (
                'id' => 3995,
                'language_code' => 'ms',
                'display_language_code' => 'ku',
                'name' => 'Malay',
            ),
            495 =>
            array (
                'id' => 3996,
                'language_code' => 'ms',
                'display_language_code' => 'lv',
                'name' => 'Malay',
            ),
            496 =>
            array (
                'id' => 3997,
                'language_code' => 'ms',
                'display_language_code' => 'lt',
                'name' => 'Malay',
            ),
            497 =>
            array (
                'id' => 3998,
                'language_code' => 'ms',
                'display_language_code' => 'mk',
                'name' => 'Malay',
            ),
            498 =>
            array (
                'id' => 3999,
                'language_code' => 'ms',
                'display_language_code' => 'mt',
                'name' => 'Malay',
            ),
            499 =>
            array (
                'id' => 4000,
                'language_code' => 'ms',
                'display_language_code' => 'mn',
                'name' => 'Malay',
            ),
        ));
        \DB::table('language_translations')->insert(array (
            0 =>
            array (
                'id' => 4001,
                'language_code' => 'ms',
                'display_language_code' => 'ne',
                'name' => 'Malay',
            ),
            1 =>
            array (
                'id' => 4002,
                'language_code' => 'ms',
                'display_language_code' => 'nl',
                'name' => 'Malay',
            ),
            2 =>
            array (
                'id' => 4003,
                'language_code' => 'ms',
                'display_language_code' => 'no',
                'name' => 'Malay',
            ),
            3 =>
            array (
                'id' => 4004,
                'language_code' => 'ms',
                'display_language_code' => 'pa',
                'name' => 'Malay',
            ),
            4 =>
            array (
                'id' => 4005,
                'language_code' => 'ms',
                'display_language_code' => 'pl',
                'name' => 'Malay',
            ),
            5 =>
            array (
                'id' => 4006,
                'language_code' => 'ms',
                'display_language_code' => 'pt-pt',
                'name' => 'Malaio',
            ),
            6 =>
            array (
                'id' => 4007,
                'language_code' => 'ms',
                'display_language_code' => 'pt-br',
                'name' => 'Malaio',
            ),
            7 =>
            array (
                'id' => 4008,
                'language_code' => 'ms',
                'display_language_code' => 'qu',
                'name' => 'Malay',
            ),
            8 =>
            array (
                'id' => 4009,
                'language_code' => 'ms',
                'display_language_code' => 'ro',
                'name' => 'Malay',
            ),
            9 =>
            array (
                'id' => 4010,
                'language_code' => 'ms',
                'display_language_code' => 'ru',
                'name' => 'Малайский',
            ),
            10 =>
            array (
                'id' => 4011,
                'language_code' => 'ms',
                'display_language_code' => 'sl',
                'name' => 'Malajščina',
            ),
            11 =>
            array (
                'id' => 4012,
                'language_code' => 'ms',
                'display_language_code' => 'so',
                'name' => 'Malay',
            ),
            12 =>
            array (
                'id' => 4013,
                'language_code' => 'ms',
                'display_language_code' => 'sq',
                'name' => 'Malay',
            ),
            13 =>
            array (
                'id' => 4014,
                'language_code' => 'ms',
                'display_language_code' => 'sr',
                'name' => 'малајски',
            ),
            14 =>
            array (
                'id' => 4015,
                'language_code' => 'ms',
                'display_language_code' => 'sv',
                'name' => 'Malay',
            ),
            15 =>
            array (
                'id' => 4016,
                'language_code' => 'ms',
                'display_language_code' => 'ta',
                'name' => 'Malay',
            ),
            16 =>
            array (
                'id' => 4017,
                'language_code' => 'ms',
                'display_language_code' => 'th',
                'name' => 'Malay',
            ),
            17 =>
            array (
                'id' => 4018,
                'language_code' => 'ms',
                'display_language_code' => 'tr',
                'name' => 'Malay',
            ),
            18 =>
            array (
                'id' => 4019,
                'language_code' => 'ms',
                'display_language_code' => 'uk',
                'name' => 'Malay',
            ),
            19 =>
            array (
                'id' => 4020,
                'language_code' => 'ms',
                'display_language_code' => 'ur',
                'name' => 'Malay',
            ),
            20 =>
            array (
                'id' => 4021,
                'language_code' => 'ms',
                'display_language_code' => 'uz',
                'name' => 'Malay',
            ),
            21 =>
            array (
                'id' => 4022,
                'language_code' => 'ms',
                'display_language_code' => 'vi',
                'name' => 'Malay',
            ),
            22 =>
            array (
                'id' => 4023,
                'language_code' => 'ms',
                'display_language_code' => 'yi',
                'name' => 'Malay',
            ),
            23 =>
            array (
                'id' => 4024,
                'language_code' => 'ms',
                'display_language_code' => 'zh-hans',
                'name' => 'Malay',
            ),
            24 =>
            array (
                'id' => 4025,
                'language_code' => 'ms',
                'display_language_code' => 'zu',
                'name' => 'Malay',
            ),
            25 =>
            array (
                'id' => 4026,
                'language_code' => 'ms',
                'display_language_code' => 'zh-hant',
                'name' => 'Malay',
            ),
            26 =>
            array (
                'id' => 4027,
                'language_code' => 'ms',
                'display_language_code' => 'ms',
                'name' => 'Melayu',
            ),
            27 =>
            array (
                'id' => 4028,
                'language_code' => 'ms',
                'display_language_code' => 'gl',
                'name' => 'Malay',
            ),
            28 =>
            array (
                'id' => 4029,
                'language_code' => 'ms',
                'display_language_code' => 'bn',
                'name' => 'Malay',
            ),
            29 =>
            array (
                'id' => 4030,
                'language_code' => 'ms',
                'display_language_code' => 'az',
                'name' => 'Malay',
            ),
            30 =>
            array (
                'id' => 4031,
                'language_code' => 'gl',
                'display_language_code' => 'en',
                'name' => 'Galician',
            ),
            31 =>
            array (
                'id' => 4032,
                'language_code' => 'gl',
                'display_language_code' => 'es',
                'name' => 'Gallego',
            ),
            32 =>
            array (
                'id' => 4033,
                'language_code' => 'gl',
                'display_language_code' => 'de',
                'name' => 'Galicisch',
            ),
            33 =>
            array (
                'id' => 4034,
                'language_code' => 'gl',
                'display_language_code' => 'fr',
                'name' => 'Galicien',
            ),
            34 =>
            array (
                'id' => 4035,
                'language_code' => 'gl',
                'display_language_code' => 'ar',
                'name' => 'Galician',
            ),
            35 =>
            array (
                'id' => 4036,
                'language_code' => 'gl',
                'display_language_code' => 'bs',
                'name' => 'Galician',
            ),
            36 =>
            array (
                'id' => 4037,
                'language_code' => 'gl',
                'display_language_code' => 'bg',
                'name' => 'Galician',
            ),
            37 =>
            array (
                'id' => 4038,
                'language_code' => 'gl',
                'display_language_code' => 'ca',
                'name' => 'Galician',
            ),
            38 =>
            array (
                'id' => 4039,
                'language_code' => 'gl',
                'display_language_code' => 'cs',
                'name' => 'Galician',
            ),
            39 =>
            array (
                'id' => 4040,
                'language_code' => 'gl',
                'display_language_code' => 'sk',
                'name' => 'Galician',
            ),
            40 =>
            array (
                'id' => 4041,
                'language_code' => 'gl',
                'display_language_code' => 'cy',
                'name' => 'Galician',
            ),
            41 =>
            array (
                'id' => 4042,
                'language_code' => 'gl',
                'display_language_code' => 'da',
                'name' => 'Galician',
            ),
            42 =>
            array (
                'id' => 4043,
                'language_code' => 'gl',
                'display_language_code' => 'el',
                'name' => 'Galician',
            ),
            43 =>
            array (
                'id' => 4044,
                'language_code' => 'gl',
                'display_language_code' => 'eo',
                'name' => 'Galician',
            ),
            44 =>
            array (
                'id' => 4045,
                'language_code' => 'gl',
                'display_language_code' => 'et',
                'name' => 'Galician',
            ),
            45 =>
            array (
                'id' => 4046,
                'language_code' => 'gl',
                'display_language_code' => 'eu',
                'name' => 'Galician',
            ),
            46 =>
            array (
                'id' => 4047,
                'language_code' => 'gl',
                'display_language_code' => 'fa',
                'name' => 'Galician',
            ),
            47 =>
            array (
                'id' => 4048,
                'language_code' => 'gl',
                'display_language_code' => 'fi',
                'name' => 'Galician',
            ),
            48 =>
            array (
                'id' => 4049,
                'language_code' => 'gl',
                'display_language_code' => 'ga',
                'name' => 'Galician',
            ),
            49 =>
            array (
                'id' => 4050,
                'language_code' => 'gl',
                'display_language_code' => 'he',
                'name' => 'Galician',
            ),
            50 =>
            array (
                'id' => 4051,
                'language_code' => 'gl',
                'display_language_code' => 'hi',
                'name' => 'Galician',
            ),
            51 =>
            array (
                'id' => 4052,
                'language_code' => 'gl',
                'display_language_code' => 'hr',
                'name' => 'Galician',
            ),
            52 =>
            array (
                'id' => 4053,
                'language_code' => 'gl',
                'display_language_code' => 'hu',
                'name' => 'Galician',
            ),
            53 =>
            array (
                'id' => 4054,
                'language_code' => 'gl',
                'display_language_code' => 'hy',
                'name' => 'Galician',
            ),
            54 =>
            array (
                'id' => 4055,
                'language_code' => 'gl',
                'display_language_code' => 'id',
                'name' => 'Galician',
            ),
            55 =>
            array (
                'id' => 4056,
                'language_code' => 'gl',
                'display_language_code' => 'is',
                'name' => 'Galician',
            ),
            56 =>
            array (
                'id' => 4057,
                'language_code' => 'gl',
                'display_language_code' => 'it',
                'name' => 'Gallego',
            ),
            57 =>
            array (
                'id' => 4058,
                'language_code' => 'gl',
                'display_language_code' => 'ja',
                'name' => 'Galician',
            ),
            58 =>
            array (
                'id' => 4059,
                'language_code' => 'gl',
                'display_language_code' => 'ko',
                'name' => 'Galician',
            ),
            59 =>
            array (
                'id' => 4060,
                'language_code' => 'gl',
                'display_language_code' => 'ku',
                'name' => 'Galician',
            ),
            60 =>
            array (
                'id' => 4061,
                'language_code' => 'gl',
                'display_language_code' => 'lv',
                'name' => 'Galician',
            ),
            61 =>
            array (
                'id' => 4062,
                'language_code' => 'gl',
                'display_language_code' => 'lt',
                'name' => 'Galician',
            ),
            62 =>
            array (
                'id' => 4063,
                'language_code' => 'gl',
                'display_language_code' => 'mk',
                'name' => 'Galician',
            ),
            63 =>
            array (
                'id' => 4064,
                'language_code' => 'gl',
                'display_language_code' => 'mt',
                'name' => 'Galician',
            ),
            64 =>
            array (
                'id' => 4065,
                'language_code' => 'gl',
                'display_language_code' => 'mn',
                'name' => 'Galician',
            ),
            65 =>
            array (
                'id' => 4066,
                'language_code' => 'gl',
                'display_language_code' => 'ne',
                'name' => 'Galician',
            ),
            66 =>
            array (
                'id' => 4067,
                'language_code' => 'gl',
                'display_language_code' => 'nl',
                'name' => 'Galician',
            ),
            67 =>
            array (
                'id' => 4068,
                'language_code' => 'gl',
                'display_language_code' => 'no',
                'name' => 'Galician',
            ),
            68 =>
            array (
                'id' => 4069,
                'language_code' => 'gl',
                'display_language_code' => 'pa',
                'name' => 'Galician',
            ),
            69 =>
            array (
                'id' => 4070,
                'language_code' => 'gl',
                'display_language_code' => 'pl',
                'name' => 'Galician',
            ),
            70 =>
            array (
                'id' => 4071,
                'language_code' => 'gl',
                'display_language_code' => 'pt-pt',
                'name' => 'Galego',
            ),
            71 =>
            array (
                'id' => 4072,
                'language_code' => 'gl',
                'display_language_code' => 'pt-br',
                'name' => 'Galego',
            ),
            72 =>
            array (
                'id' => 4073,
                'language_code' => 'gl',
                'display_language_code' => 'qu',
                'name' => 'Galician',
            ),
            73 =>
            array (
                'id' => 4074,
                'language_code' => 'gl',
                'display_language_code' => 'ro',
                'name' => 'Galician',
            ),
            74 =>
            array (
                'id' => 4075,
                'language_code' => 'gl',
                'display_language_code' => 'ru',
                'name' => 'Галисийский',
            ),
            75 =>
            array (
                'id' => 4076,
                'language_code' => 'gl',
                'display_language_code' => 'sl',
                'name' => 'Galician',
            ),
            76 =>
            array (
                'id' => 4077,
                'language_code' => 'gl',
                'display_language_code' => 'so',
                'name' => 'Galician',
            ),
            77 =>
            array (
                'id' => 4078,
                'language_code' => 'gl',
                'display_language_code' => 'sq',
                'name' => 'Galician',
            ),
            78 =>
            array (
                'id' => 4079,
                'language_code' => 'gl',
                'display_language_code' => 'sr',
                'name' => 'Galician',
            ),
            79 =>
            array (
                'id' => 4080,
                'language_code' => 'gl',
                'display_language_code' => 'sv',
                'name' => 'Galician',
            ),
            80 =>
            array (
                'id' => 4081,
                'language_code' => 'gl',
                'display_language_code' => 'ta',
                'name' => 'Galician',
            ),
            81 =>
            array (
                'id' => 4082,
                'language_code' => 'gl',
                'display_language_code' => 'th',
                'name' => 'Galician',
            ),
            82 =>
            array (
                'id' => 4083,
                'language_code' => 'gl',
                'display_language_code' => 'tr',
                'name' => 'Galician',
            ),
            83 =>
            array (
                'id' => 4084,
                'language_code' => 'gl',
                'display_language_code' => 'uk',
                'name' => 'Galician',
            ),
            84 =>
            array (
                'id' => 4085,
                'language_code' => 'gl',
                'display_language_code' => 'ur',
                'name' => 'Galician',
            ),
            85 =>
            array (
                'id' => 4086,
                'language_code' => 'gl',
                'display_language_code' => 'uz',
                'name' => 'Galician',
            ),
            86 =>
            array (
                'id' => 4087,
                'language_code' => 'gl',
                'display_language_code' => 'vi',
                'name' => 'Galician',
            ),
            87 =>
            array (
                'id' => 4088,
                'language_code' => 'gl',
                'display_language_code' => 'yi',
                'name' => 'Galician',
            ),
            88 =>
            array (
                'id' => 4089,
                'language_code' => 'gl',
                'display_language_code' => 'zh-hans',
                'name' => 'Galician',
            ),
            89 =>
            array (
                'id' => 4090,
                'language_code' => 'gl',
                'display_language_code' => 'zu',
                'name' => 'Galician',
            ),
            90 =>
            array (
                'id' => 4091,
                'language_code' => 'gl',
                'display_language_code' => 'zh-hant',
                'name' => 'Galician',
            ),
            91 =>
            array (
                'id' => 4092,
                'language_code' => 'gl',
                'display_language_code' => 'ms',
                'name' => 'Galician',
            ),
            92 =>
            array (
                'id' => 4093,
                'language_code' => 'gl',
                'display_language_code' => 'gl',
                'name' => 'Galego',
            ),
            93 =>
            array (
                'id' => 4094,
                'language_code' => 'gl',
                'display_language_code' => 'bn',
                'name' => 'Galician',
            ),
            94 =>
            array (
                'id' => 4095,
                'language_code' => 'gl',
                'display_language_code' => 'az',
                'name' => 'Galician',
            ),
            95 =>
            array (
                'id' => 4096,
                'language_code' => 'bn',
                'display_language_code' => 'en',
                'name' => 'Bengali',
            ),
            96 =>
            array (
                'id' => 4097,
                'language_code' => 'bn',
                'display_language_code' => 'es',
                'name' => 'Bengalí',
            ),
            97 =>
            array (
                'id' => 4098,
                'language_code' => 'bn',
                'display_language_code' => 'de',
                'name' => 'Bengalisch',
            ),
            98 =>
            array (
                'id' => 4099,
                'language_code' => 'bn',
                'display_language_code' => 'fr',
                'name' => 'Bengali',
            ),
            99 =>
            array (
                'id' => 4100,
                'language_code' => 'bn',
                'display_language_code' => 'ar',
                'name' => 'Bengali',
            ),
            100 =>
            array (
                'id' => 4101,
                'language_code' => 'bn',
                'display_language_code' => 'bs',
                'name' => 'Bengali',
            ),
            101 =>
            array (
                'id' => 4102,
                'language_code' => 'bn',
                'display_language_code' => 'bg',
                'name' => 'Bengali',
            ),
            102 =>
            array (
                'id' => 4103,
                'language_code' => 'bn',
                'display_language_code' => 'ca',
                'name' => 'Bengali',
            ),
            103 =>
            array (
                'id' => 4104,
                'language_code' => 'bn',
                'display_language_code' => 'cs',
                'name' => 'Bengali',
            ),
            104 =>
            array (
                'id' => 4105,
                'language_code' => 'bn',
                'display_language_code' => 'sk',
                'name' => 'Bengali',
            ),
            105 =>
            array (
                'id' => 4106,
                'language_code' => 'bn',
                'display_language_code' => 'cy',
                'name' => 'Bengali',
            ),
            106 =>
            array (
                'id' => 4107,
                'language_code' => 'bn',
                'display_language_code' => 'da',
                'name' => 'Bengali',
            ),
            107 =>
            array (
                'id' => 4108,
                'language_code' => 'bn',
                'display_language_code' => 'el',
                'name' => 'Bengali',
            ),
            108 =>
            array (
                'id' => 4109,
                'language_code' => 'bn',
                'display_language_code' => 'eo',
                'name' => 'Bengali',
            ),
            109 =>
            array (
                'id' => 4110,
                'language_code' => 'bn',
                'display_language_code' => 'et',
                'name' => 'Bengali',
            ),
            110 =>
            array (
                'id' => 4111,
                'language_code' => 'bn',
                'display_language_code' => 'eu',
                'name' => 'Bengali',
            ),
            111 =>
            array (
                'id' => 4112,
                'language_code' => 'bn',
                'display_language_code' => 'fa',
                'name' => 'Bengali',
            ),
            112 =>
            array (
                'id' => 4113,
                'language_code' => 'bn',
                'display_language_code' => 'fi',
                'name' => 'Bengali',
            ),
            113 =>
            array (
                'id' => 4114,
                'language_code' => 'bn',
                'display_language_code' => 'ga',
                'name' => 'Bengali',
            ),
            114 =>
            array (
                'id' => 4115,
                'language_code' => 'bn',
                'display_language_code' => 'he',
                'name' => 'Bengali',
            ),
            115 =>
            array (
                'id' => 4116,
                'language_code' => 'bn',
                'display_language_code' => 'hi',
                'name' => 'Bengali',
            ),
            116 =>
            array (
                'id' => 4117,
                'language_code' => 'bn',
                'display_language_code' => 'hr',
                'name' => 'Bengali',
            ),
            117 =>
            array (
                'id' => 4118,
                'language_code' => 'bn',
                'display_language_code' => 'hu',
                'name' => 'Bengali',
            ),
            118 =>
            array (
                'id' => 4119,
                'language_code' => 'bn',
                'display_language_code' => 'hy',
                'name' => 'Bengali',
            ),
            119 =>
            array (
                'id' => 4120,
                'language_code' => 'bn',
                'display_language_code' => 'id',
                'name' => 'Bengali',
            ),
            120 =>
            array (
                'id' => 4121,
                'language_code' => 'bn',
                'display_language_code' => 'is',
                'name' => 'Bengali',
            ),
            121 =>
            array (
                'id' => 4122,
                'language_code' => 'bn',
                'display_language_code' => 'it',
                'name' => 'Bengalese',
            ),
            122 =>
            array (
                'id' => 4123,
                'language_code' => 'bn',
                'display_language_code' => 'ja',
                'name' => 'Bengali',
            ),
            123 =>
            array (
                'id' => 4124,
                'language_code' => 'bn',
                'display_language_code' => 'ko',
                'name' => 'Bengali',
            ),
            124 =>
            array (
                'id' => 4125,
                'language_code' => 'bn',
                'display_language_code' => 'ku',
                'name' => 'Bengali',
            ),
            125 =>
            array (
                'id' => 4126,
                'language_code' => 'bn',
                'display_language_code' => 'lv',
                'name' => 'Bengali',
            ),
            126 =>
            array (
                'id' => 4127,
                'language_code' => 'bn',
                'display_language_code' => 'lt',
                'name' => 'Bengali',
            ),
            127 =>
            array (
                'id' => 4128,
                'language_code' => 'bn',
                'display_language_code' => 'mk',
                'name' => 'Bengali',
            ),
            128 =>
            array (
                'id' => 4129,
                'language_code' => 'bn',
                'display_language_code' => 'mt',
                'name' => 'Bengali',
            ),
            129 =>
            array (
                'id' => 4130,
                'language_code' => 'bn',
                'display_language_code' => 'mn',
                'name' => 'Bengali',
            ),
            130 =>
            array (
                'id' => 4131,
                'language_code' => 'bn',
                'display_language_code' => 'ne',
                'name' => 'Bengali',
            ),
            131 =>
            array (
                'id' => 4132,
                'language_code' => 'bn',
                'display_language_code' => 'nl',
                'name' => 'Bengali',
            ),
            132 =>
            array (
                'id' => 4133,
                'language_code' => 'bn',
                'display_language_code' => 'no',
                'name' => 'Bengali',
            ),
            133 =>
            array (
                'id' => 4134,
                'language_code' => 'bn',
                'display_language_code' => 'pa',
                'name' => 'Bengali',
            ),
            134 =>
            array (
                'id' => 4135,
                'language_code' => 'bn',
                'display_language_code' => 'pl',
                'name' => 'Bengali',
            ),
            135 =>
            array (
                'id' => 4136,
                'language_code' => 'bn',
                'display_language_code' => 'pt-pt',
                'name' => 'Bengalês',
            ),
            136 =>
            array (
                'id' => 4137,
                'language_code' => 'bn',
                'display_language_code' => 'pt-br',
                'name' => 'Bengalês',
            ),
            137 =>
            array (
                'id' => 4138,
                'language_code' => 'bn',
                'display_language_code' => 'qu',
                'name' => 'Bengali',
            ),
            138 =>
            array (
                'id' => 4139,
                'language_code' => 'bn',
                'display_language_code' => 'ro',
                'name' => 'Bengali',
            ),
            139 =>
            array (
                'id' => 4140,
                'language_code' => 'bn',
                'display_language_code' => 'ru',
                'name' => 'Бенгальский',
            ),
            140 =>
            array (
                'id' => 4141,
                'language_code' => 'bn',
                'display_language_code' => 'sl',
                'name' => 'Bengali',
            ),
            141 =>
            array (
                'id' => 4142,
                'language_code' => 'bn',
                'display_language_code' => 'so',
                'name' => 'Bengali',
            ),
            142 =>
            array (
                'id' => 4143,
                'language_code' => 'bn',
                'display_language_code' => 'sq',
                'name' => 'Bengali',
            ),
            143 =>
            array (
                'id' => 4144,
                'language_code' => 'bn',
                'display_language_code' => 'sr',
                'name' => 'Bengali',
            ),
            144 =>
            array (
                'id' => 4145,
                'language_code' => 'bn',
                'display_language_code' => 'sv',
                'name' => 'Bengali',
            ),
            145 =>
            array (
                'id' => 4146,
                'language_code' => 'bn',
                'display_language_code' => 'ta',
                'name' => 'Bengali',
            ),
            146 =>
            array (
                'id' => 4147,
                'language_code' => 'bn',
                'display_language_code' => 'th',
                'name' => 'Bengali',
            ),
            147 =>
            array (
                'id' => 4148,
                'language_code' => 'bn',
                'display_language_code' => 'tr',
                'name' => 'Bengali',
            ),
            148 =>
            array (
                'id' => 4149,
                'language_code' => 'bn',
                'display_language_code' => 'uk',
                'name' => 'Bengali',
            ),
            149 =>
            array (
                'id' => 4150,
                'language_code' => 'bn',
                'display_language_code' => 'ur',
                'name' => 'Bengali',
            ),
            150 =>
            array (
                'id' => 4151,
                'language_code' => 'bn',
                'display_language_code' => 'uz',
                'name' => 'Bengali',
            ),
            151 =>
            array (
                'id' => 4152,
                'language_code' => 'bn',
                'display_language_code' => 'vi',
                'name' => 'Bengali',
            ),
            152 =>
            array (
                'id' => 4153,
                'language_code' => 'bn',
                'display_language_code' => 'yi',
                'name' => 'Bengali',
            ),
            153 =>
            array (
                'id' => 4154,
                'language_code' => 'bn',
                'display_language_code' => 'zh-hans',
                'name' => 'Bengali',
            ),
            154 =>
            array (
                'id' => 4155,
                'language_code' => 'bn',
                'display_language_code' => 'zu',
                'name' => 'Bengali',
            ),
            155 =>
            array (
                'id' => 4156,
                'language_code' => 'bn',
                'display_language_code' => 'zh-hant',
                'name' => 'Bengali',
            ),
            156 =>
            array (
                'id' => 4157,
                'language_code' => 'bn',
                'display_language_code' => 'ms',
                'name' => 'Bengali',
            ),
            157 =>
            array (
                'id' => 4158,
                'language_code' => 'bn',
                'display_language_code' => 'gl',
                'name' => 'Bengali',
            ),
            158 =>
            array (
                'id' => 4159,
                'language_code' => 'bn',
                'display_language_code' => 'bn',
                'name' => 'বাংলাদেশ',
            ),
            159 =>
            array (
                'id' => 4160,
                'language_code' => 'bn',
                'display_language_code' => 'az',
                'name' => 'Bengali',
            ),
            160 =>
            array (
                'id' => 4161,
                'language_code' => 'az',
                'display_language_code' => 'en',
                'name' => 'Azerbaijani',
            ),
            161 =>
            array (
                'id' => 4162,
                'language_code' => 'az',
                'display_language_code' => 'es',
                'name' => 'Azerí',
            ),
            162 =>
            array (
                'id' => 4163,
                'language_code' => 'az',
                'display_language_code' => 'de',
                'name' => 'Aserbeidschanisch',
            ),
            163 =>
            array (
                'id' => 4164,
                'language_code' => 'az',
                'display_language_code' => 'fr',
                'name' => 'Azéri',
            ),
            164 =>
            array (
                'id' => 4165,
                'language_code' => 'az',
                'display_language_code' => 'ar',
                'name' => 'الأذربيجانية',
            ),
            165 =>
            array (
                'id' => 4166,
                'language_code' => 'az',
                'display_language_code' => 'bs',
                'name' => 'Azerbaijani',
            ),
            166 =>
            array (
                'id' => 4167,
                'language_code' => 'az',
                'display_language_code' => 'bg',
                'name' => 'Azerbaijani',
            ),
            167 =>
            array (
                'id' => 4168,
                'language_code' => 'az',
                'display_language_code' => 'ca',
                'name' => 'Azerbaijani',
            ),
            168 =>
            array (
                'id' => 4169,
                'language_code' => 'az',
                'display_language_code' => 'cs',
                'name' => 'Azerbaijani',
            ),
            169 =>
            array (
                'id' => 4170,
                'language_code' => 'az',
                'display_language_code' => 'sk',
                'name' => 'Azerbaijani',
            ),
            170 =>
            array (
                'id' => 4171,
                'language_code' => 'az',
                'display_language_code' => 'cy',
                'name' => 'Azerbaijani',
            ),
            171 =>
            array (
                'id' => 4172,
                'language_code' => 'az',
                'display_language_code' => 'da',
                'name' => 'Azerbaijani',
            ),
            172 =>
            array (
                'id' => 4173,
                'language_code' => 'az',
                'display_language_code' => 'el',
                'name' => 'Azerbaijani',
            ),
            173 =>
            array (
                'id' => 4174,
                'language_code' => 'az',
                'display_language_code' => 'eo',
                'name' => 'Azerbaijani',
            ),
            174 =>
            array (
                'id' => 4175,
                'language_code' => 'az',
                'display_language_code' => 'et',
                'name' => 'Azerbaijani',
            ),
            175 =>
            array (
                'id' => 4176,
                'language_code' => 'az',
                'display_language_code' => 'eu',
                'name' => 'Azerbaijani',
            ),
            176 =>
            array (
                'id' => 4177,
                'language_code' => 'az',
                'display_language_code' => 'fa',
                'name' => 'Azerbaijani',
            ),
            177 =>
            array (
                'id' => 4178,
                'language_code' => 'az',
                'display_language_code' => 'fi',
                'name' => 'Azerbaijani',
            ),
            178 =>
            array (
                'id' => 4179,
                'language_code' => 'az',
                'display_language_code' => 'ga',
                'name' => 'Azerbaijani',
            ),
            179 =>
            array (
                'id' => 4180,
                'language_code' => 'az',
                'display_language_code' => 'he',
                'name' => 'אזרביג\'נית',
            ),
            180 =>
            array (
                'id' => 4181,
                'language_code' => 'az',
                'display_language_code' => 'hi',
                'name' => 'Azerbaijani',
            ),
            181 =>
            array (
                'id' => 4182,
                'language_code' => 'az',
                'display_language_code' => 'hr',
                'name' => 'Azerbaijani',
            ),
            182 =>
            array (
                'id' => 4183,
                'language_code' => 'az',
                'display_language_code' => 'hu',
                'name' => 'Azerbaijani',
            ),
            183 =>
            array (
                'id' => 4184,
                'language_code' => 'az',
                'display_language_code' => 'hy',
                'name' => 'Azerbaijani',
            ),
            184 =>
            array (
                'id' => 4185,
                'language_code' => 'az',
                'display_language_code' => 'id',
                'name' => 'Azerbaijani',
            ),
            185 =>
            array (
                'id' => 4186,
                'language_code' => 'az',
                'display_language_code' => 'is',
                'name' => 'Azerbaijani',
            ),
            186 =>
            array (
                'id' => 4187,
                'language_code' => 'az',
                'display_language_code' => 'it',
                'name' => 'Azerbaigiano',
            ),
            187 =>
            array (
                'id' => 4188,
                'language_code' => 'az',
                'display_language_code' => 'ja',
                'name' => 'アゼルバイジャン語',
            ),
            188 =>
            array (
                'id' => 4189,
                'language_code' => 'az',
                'display_language_code' => 'ko',
                'name' => 'Azerbaijani',
            ),
            189 =>
            array (
                'id' => 4190,
                'language_code' => 'az',
                'display_language_code' => 'ku',
                'name' => 'Azerbaijani',
            ),
            190 =>
            array (
                'id' => 4191,
                'language_code' => 'az',
                'display_language_code' => 'lv',
                'name' => 'Azerbaijani',
            ),
            191 =>
            array (
                'id' => 4192,
                'language_code' => 'az',
                'display_language_code' => 'lt',
                'name' => 'Azerbaijani',
            ),
            192 =>
            array (
                'id' => 4193,
                'language_code' => 'az',
                'display_language_code' => 'mk',
                'name' => 'Azerbaijani',
            ),
            193 =>
            array (
                'id' => 4194,
                'language_code' => 'az',
                'display_language_code' => 'mt',
                'name' => 'Azerbaijani',
            ),
            194 =>
            array (
                'id' => 4195,
                'language_code' => 'az',
                'display_language_code' => 'mn',
                'name' => 'Azerbaijani',
            ),
            195 =>
            array (
                'id' => 4196,
                'language_code' => 'az',
                'display_language_code' => 'ne',
                'name' => 'Azerbaijani',
            ),
            196 =>
            array (
                'id' => 4197,
                'language_code' => 'az',
                'display_language_code' => 'nl',
                'name' => 'Azerbaijani',
            ),
            197 =>
            array (
                'id' => 4198,
                'language_code' => 'az',
                'display_language_code' => 'no',
                'name' => 'Azerbaijani',
            ),
            198 =>
            array (
                'id' => 4199,
                'language_code' => 'az',
                'display_language_code' => 'pa',
                'name' => 'Azerbaijani',
            ),
            199 =>
            array (
                'id' => 4200,
                'language_code' => 'az',
                'display_language_code' => 'pl',
                'name' => 'Azerski',
            ),
            200 =>
            array (
                'id' => 4201,
                'language_code' => 'az',
                'display_language_code' => 'pt-pt',
                'name' => 'Azerbaijano',
            ),
            201 =>
            array (
                'id' => 4202,
                'language_code' => 'az',
                'display_language_code' => 'pt-br',
                'name' => 'Azerbaijano',
            ),
            202 =>
            array (
                'id' => 4203,
                'language_code' => 'az',
                'display_language_code' => 'qu',
                'name' => 'Azerbaijani',
            ),
            203 =>
            array (
                'id' => 4204,
                'language_code' => 'az',
                'display_language_code' => 'ro',
                'name' => 'Azerbaijani',
            ),
            204 =>
            array (
                'id' => 4205,
                'language_code' => 'az',
                'display_language_code' => 'ru',
                'name' => 'Азербайджанский',
            ),
            205 =>
            array (
                'id' => 4206,
                'language_code' => 'az',
                'display_language_code' => 'sl',
                'name' => 'Azerbaijani',
            ),
            206 =>
            array (
                'id' => 4207,
                'language_code' => 'az',
                'display_language_code' => 'so',
                'name' => 'Azerbaijani',
            ),
            207 =>
            array (
                'id' => 4208,
                'language_code' => 'az',
                'display_language_code' => 'sq',
                'name' => 'Azerbaijani',
            ),
            208 =>
            array (
                'id' => 4209,
                'language_code' => 'az',
                'display_language_code' => 'sr',
                'name' => 'Azerbaijani',
            ),
            209 =>
            array (
                'id' => 4210,
                'language_code' => 'az',
                'display_language_code' => 'sv',
                'name' => 'Azerbaijani',
            ),
            210 =>
            array (
                'id' => 4211,
                'language_code' => 'az',
                'display_language_code' => 'ta',
                'name' => 'Azerbaijani',
            ),
            211 =>
            array (
                'id' => 4212,
                'language_code' => 'az',
                'display_language_code' => 'th',
                'name' => 'Azerbaijani',
            ),
            212 =>
            array (
                'id' => 4213,
                'language_code' => 'az',
                'display_language_code' => 'tr',
                'name' => 'Azerbaijani',
            ),
            213 =>
            array (
                'id' => 4214,
                'language_code' => 'az',
                'display_language_code' => 'uk',
                'name' => 'Azerbaijani',
            ),
            214 =>
            array (
                'id' => 4215,
                'language_code' => 'az',
                'display_language_code' => 'ur',
                'name' => 'Azerbaijani',
            ),
            215 =>
            array (
                'id' => 4216,
                'language_code' => 'az',
                'display_language_code' => 'uz',
                'name' => 'Azerbaijani',
            ),
            216 =>
            array (
                'id' => 4217,
                'language_code' => 'az',
                'display_language_code' => 'vi',
                'name' => 'Azerbaijani',
            ),
            217 =>
            array (
                'id' => 4218,
                'language_code' => 'az',
                'display_language_code' => 'yi',
                'name' => 'Azerbaijani',
            ),
            218 =>
            array (
                'id' => 4219,
                'language_code' => 'az',
                'display_language_code' => 'zh-hans',
                'name' => '阿塞拜疆语',
            ),
            219 =>
            array (
                'id' => 4220,
                'language_code' => 'az',
                'display_language_code' => 'zu',
                'name' => 'Azerbaijani',
            ),
            220 =>
            array (
                'id' => 4221,
                'language_code' => 'az',
                'display_language_code' => 'zh-hant',
                'name' => 'Azerbaijani',
            ),
            221 =>
            array (
                'id' => 4222,
                'language_code' => 'az',
                'display_language_code' => 'ms',
                'name' => 'Azerbaijani',
            ),
            222 =>
            array (
                'id' => 4223,
                'language_code' => 'az',
                'display_language_code' => 'gl',
                'name' => 'Azerbaijani',
            ),
            223 =>
            array (
                'id' => 4224,
                'language_code' => 'az',
                'display_language_code' => 'bn',
                'name' => 'Azerbaijani',
            ),
            224 =>
            array (
                'id' => 4225,
                'language_code' => 'az',
                'display_language_code' => 'az',
                'name' => 'Azərbaycan',
            ),
        ));


    }
}
