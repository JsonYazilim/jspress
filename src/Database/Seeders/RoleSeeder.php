<?php

namespace JsPress\JsPress\Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        \DB::table('roles')->truncate();
        \DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        $roles = [
            [
                'name' => 'Super-Admin',
                'guard_name' => 'admin'
            ],[
                'name' => 'Admin',
                'guard_name' => 'admin'
            ],[
                'name' => 'Moderator',
                'guard_name' => 'admin'
            ],[
                'name' => 'Editor',
                'guard_name' => 'admin'
            ]
        ];
        foreach($roles as $role){
            Role::create($role);
        }


    }
}
