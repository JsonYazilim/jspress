<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PostTypeSeeder.php
 * Author: Json Yazılım
 * Class: PostTypeSeeder.php
 * Current Username: Erdinc
 * Last Modified: 29.09.2023 22:13
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Database\Seeders;

use Illuminate\Database\Seeder;

class PostTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $post_type = \DB::table('post_types')->insert(
            [
                'id' => 1,
                'post_type_id' => 0,
                'name' => 'Sayfalar',
                'key' => 'page',
                'icon' => '<i class="far fa-list-alt fs-4"></i>',
                'is_category' => 0,
                'is_terms' => 0,
                'is_image' => 0,
                'is_url' => 1,
                'is_segment_disable' => 1,
                'is_menu' => 1,
                'status' => 1,
                'order' => 0
            ]
        );
        \DB::table('post_type_details')->insert(
            [
                'post_type_id' => 1,
                'region' => 1,
                'locale' => 'tr',
                'model' => 'JsPress\JsPress\Models\Post',
                'singular_name' => 'Sayfa',
                'plural_name' => 'Sayfalar',
                'slug' => 'page',
                'status' => 1
            ]
        );
    }
}
