<?php

namespace JsPress\JsPress\Database\Seeders;

use Illuminate\Database\Seeder;
use JsPress\JsPress\Foundations\JsPress;
use JsPress\JsPress\Models\PermissionGroup;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $permissions = [
            [
                'name' => 'view users',
                'guard_name' => 'admin',
                'group_title' => 'Kullanıcı İşlemleri',
                'description' => 'Kullanıcı listesi görüntüleme',
                'group' => 'user',
                'title' => 'Görüntüleme',
                'group_id' => 1
            ],[
                'name' => 'edit user',
                'guard_name' => 'admin',
                'group_title' => 'Kullanıcı İşlemleri',
                'description' => 'Kullanıcı düzenleme işlemi',
                'group' => 'user',
                'title' => 'Düzenleme',
                'group_id' => 1
            ],[
                'name' => 'add user',
                'guard_name' => 'admin',
                'group_title' => 'Kullanıcı İşlemleri',
                'description' => 'Kullanıcı ekleme işlemi',
                'group' => 'user',
                'title' => 'Ekleme',
                'group_id' => 1
            ],[
                'name' => 'delete user',
                'guard_name' => 'admin',
                'group_title' => 'Kullanıcı İşlemleri',
                'description' => 'Kullanıcı silme işlemi',
                'group' => 'user',
                'title' => 'Silme',
                'group_id' => 1
            ],
            [
                'name' => 'view admins',
                'guard_name' => 'admin',
                'group_title' => 'Yönetici İşlemleri',
                'description' => 'Yönetici listesi görüntüleme',
                'group' => 'admin',
                'title' => 'Görüntüleme',
                'group_id' => 2
            ],[
                'name' => 'edit admin',
                'guard_name' => 'admin',
                'group_title' => 'Yönetici İşlemleri',
                'description' => 'Yönetici düzenleme işlemi',
                'group' => 'admin',
                'title' => 'Düzenleme',
                'group_id' => 2
            ],[
                'name' => 'add admin',
                'guard_name' => 'admin',
                'group_title' => 'Yönetici İşlemleri',
                'description' => 'Yönetici ekleme işlemi',
                'group' => 'admin',
                'title' => 'Ekleme',
                'group_id' => 2
            ],[
                'name' => 'delete admin',
                'guard_name' => 'admin',
                'group_title' => 'Yönetici İşlemleri',
                'description' => 'Yönetici silme işlemi',
                'group' => 'admin',
                'title' => 'Silme',
                'group_id' => 2
            ],
            [
                'name' => 'view roles',
                'guard_name' => 'admin',
                'group_title' => 'Rol İşlemleri',
                'description' => 'Rol listesi görüntüleme',
                'group' => 'role',
                'title' => 'Görüntüleme',
                'group_id' => 3
            ],[
                'name' => 'edit role',
                'guard_name' => 'admin',
                'group_title' => 'Rol İşlemleri',
                'description' => 'Rol düzenleme işlemi',
                'group' => 'role',
                'title' => 'Düzenleme',
                'group_id' => 3
            ],[
                'name' => 'add role',
                'guard_name' => 'admin',
                'group_title' => 'Rol İşlemleri',
                'description' => 'Rol ekleme işlemi',
                'group' => 'role',
                'title' => 'Ekleme',
                'group_id' => 3
            ],[
                'name' => 'delete role',
                'guard_name' => 'admin',
                'group_title' => 'Rol İşlemleri',
                'description' => 'Rol silme işlemi',
                'group' => 'role',
                'title' => 'Silme',
                'group_id' => 3
            ],
            [
                'name' => 'view languages',
                'guard_name' => 'admin',
                'group_title' => 'Dil Yönetimi',
                'description' => 'Dil listesi görüntüleme',
                'group' => 'language',
                'title' => 'Görüntüleme',
                'group_id' => 4
            ],[
                'name' => 'edit language',
                'guard_name' => 'admin',
                'group_title' => 'Dil Yönetimi',
                'description' => 'Dil düzenleme işlemi',
                'group' => 'language',
                'title' => 'Düzenleme',
                'group_id' => 4
            ],[
                'name' => 'add language',
                'guard_name' => 'admin',
                'group_title' => 'Dil Yönetimi',
                'description' => 'Dil ekleme işlemi',
                'group' => 'language',
                'title' => 'Ekleme',
                'group_id' => 4
            ],[
                'name' => 'delete language',
                'guard_name' => 'admin',
                'group_title' => 'Dil Yönetimi',
                'description' => 'Dil silme işlemi',
                'group' => 'language',
                'title' => 'Silme',
                'group_id' => 4
            ],
            [
                'name' => 'view translations',
                'guard_name' => 'admin',
                'group_title' => 'Çeviri Yönetimi',
                'description' => 'Çeviri listesi görüntüleme',
                'group' => 'translation',
                'title' => 'Görüntüleme',
                'group_id' => 5
            ],[
                'name' => 'edit translation',
                'guard_name' => 'admin',
                'group_title' => 'Çeviri Yönetimi',
                'description' => 'Çeviri düzenleme işlemi',
                'group' => 'translation',
                'title' => 'Düzenleme',
                'group_id' => 5
            ],[
                'name' => 'delete translation',
                'guard_name' => 'admin',
                'group_title' => 'Çeviri Yönetimi',
                'description' => 'Çeviri silme işlemi',
                'group' => 'translation',
                'title' => 'Silme',
                'group_id' => 5
            ],
            [
                'name' => 'view post_types',
                'guard_name' => 'admin',
                'group_title' => 'İçerik Yönetimi',
                'description' => 'İçerik listesi görüntüleme',
                'group' => 'post_type',
                'title' => 'Görüntüleme',
                'group_id' => 6
            ],[
                'name' => 'edit post_type',
                'guard_name' => 'admin',
                'group_title' => 'İçerik Yönetimi',
                'description' => 'İçerik düzenleme işlemi',
                'group' => 'post_type',
                'title' => 'Düzenleme',
                'group_id' => 6
            ],[
                'name' => 'add post_type',
                'guard_name' => 'admin',
                'group_title' => 'İçerik Yönetimi',
                'description' => 'İçerik ekleme işlemi',
                'group' => 'post_type',
                'title' => 'Ekleme',
                'group_id' => 6
            ],[
                'name' => 'delete post_type',
                'guard_name' => 'admin',
                'group_title' => 'İçerik Yönetimi',
                'description' => 'İçerik silme işlemi',
                'group' => 'post_type',
                'title' => 'Silme',
                'group_id' => 6
            ],
            [
                'name' => 'view post_field',
                'guard_name' => 'admin',
                'group_title' => 'Özel Alanlar',
                'description' => 'Özel alan listesi görüntüleme',
                'group' => 'post_field',
                'title' => 'Görüntüleme',
                'group_id' => 7
            ],[
                'name' => 'edit post_field',
                'guard_name' => 'admin',
                'group_title' => 'Özel Alanlar',
                'description' => 'Özel alan düzenleme işlemi',
                'group' => 'post_field',
                'title' => 'Düzenleme',
                'group_id' => 7
            ],[
                'name' => 'add post_field',
                'guard_name' => 'admin',
                'group_title' => 'Özel Alanlar',
                'description' => 'Özel alan ekleme işlemi',
                'group' => 'post_field',
                'title' => 'Ekleme',
                'group_id' => 7
            ],[
                'name' => 'delete post_field',
                'guard_name' => 'admin',
                'group_title' => 'Özel Alanlar',
                'description' => 'Özel alan silme işlemi',
                'group' => 'post_field',
                'title' => 'Silme',
                'group_id' => 7
            ],
            [
                'name' => 'view menu',
                'guard_name' => 'admin',
                'group_title' => 'Menü İşlemleri',
                'description' => 'Menü listesi görüntüleme',
                'group' => 'menu',
                'title' => 'Görüntüleme',
                'group_id' => 8
            ],[
                'name' => 'edit menu',
                'guard_name' => 'admin',
                'group_title' => 'Menü İşlemleri',
                'description' => 'Menü düzenleme işlemi',
                'group' => 'menu',
                'title' => 'Düzenleme',
                'group_id' => 8
            ],[
                'name' => 'add menu',
                'guard_name' => 'admin',
                'group_title' => 'Menü İşlemleri',
                'description' => 'Menü ekleme işlemi',
                'group' => 'menu',
                'title' => 'Ekleme',
                'group_id' => 8
            ],[
                'name' => 'delete menu',
                'guard_name' => 'admin',
                'group_title' => 'Menü İşlemleri',
                'description' => 'Menü silme işlemi',
                'group' => 'menu',
                'title' => 'Silme',
                'group_id' => 8
            ],
            [
                'name' => 'view media',
                'guard_name' => 'admin',
                'group_title' => 'Medya İşlemleri',
                'description' => 'Medya listesi görüntüleme',
                'group' => 'media',
                'title' => 'Görüntüleme',
                'group_id' => 9
            ],[
                'name' => 'edit media',
                'guard_name' => 'admin',
                'group_title' => 'Medya İşlemleri',
                'description' => 'Medya düzenleme işlemi',
                'group' => 'media',
                'title' => 'Düzenleme',
                'group_id' => 9
            ],[
                'name' => 'add media',
                'guard_name' => 'admin',
                'group_title' => 'Medya İşlemleri',
                'description' => 'Medya ekleme işlemi',
                'group' => 'media',
                'title' => 'Ekleme',
                'group_id' => 9
            ],[
                'name' => 'delete media',
                'guard_name' => 'admin',
                'group_title' => 'Medya İşlemleri',
                'description' => 'Medya silme işlemi',
                'group' => 'media',
                'title' => 'Silme',
                'group_id' => 9
            ],
            [
                'name' => 'all media',
                'guard_name' => 'admin',
                'group_title' => 'Medya İşlemleri',
                'description' => 'Medya listesi görüntüleme',
                'group' => 'media',
                'title' => 'Tümünü Görüntüleme',
                'group_id' => 9
            ],
            [
                'name' => 'view popup',
                'guard_name' => 'admin',
                'group_title' => 'Popup İşlemleri',
                'description' => 'Popup listesi görüntüleme',
                'group' => 'popup',
                'title' => 'Görüntüleme',
                'group_id' => 10
            ],[
                'name' => 'edit popup',
                'guard_name' => 'admin',
                'group_title' => 'Popup İşlemleri',
                'description' => 'Popup düzenleme işlemi',
                'group' => 'popup',
                'title' => 'Düzenleme',
                'group_id' => 10
            ],[
                'name' => 'add popup',
                'guard_name' => 'admin',
                'group_title' => 'Popup İşlemleri',
                'description' => 'Popup ekleme işlemi',
                'group' => 'popup',
                'title' => 'Ekleme',
                'group_id' => 10
            ],[
                'name' => 'delete popup',
                'guard_name' => 'admin',
                'group_title' => 'Popup İşlemleri',
                'description' => 'Popup silme işlemi',
                'group' => 'popup',
                'title' => 'Silme',
                'group_id' => 10
            ],
            [
                'name' => 'view settings',
                'guard_name' => 'admin',
                'group_title' => 'Genel Ayarlar',
                'description' => 'Website ayarları logo,banner vb.',
                'group' => 'default',
                'title' => 'Site Ayarları',
                'group_id' => 0
            ],
            [
                'name' => 'view system_settings',
                'guard_name' => 'admin',
                'group_title' => 'Genel Ayarlar',
                'description' => 'Sistemsel ayarların düzenlenmesi',
                'group' => 'default',
                'title' => 'Sistem Ayarları',
                'group_id' => 0
            ],
            [
                'name' => 'view cookie',
                'guard_name' => 'admin',
                'group_title' => 'Genel Ayarlar',
                'description' => 'Websitesi cookie çerez yönetim ayarlaması',
                'group' => 'default',
                'title' => 'Çerez Yönetimi Ayarları',
                'group_id' => 0
            ],
            [
                'name' => 'view error',
                'guard_name' => 'admin',
                'group_title' => 'Genel Ayarlar',
                'description' => 'Sistem ve api hata logları görüntüleme',
                'group' => 'default',
                'title' => 'Hata Logları',
                'group_id' => 0
            ],
            [
                'name' => 'view redirects',
                'guard_name' => 'admin',
                'group_title' => 'Genel Ayarlar',
                'description' => 'Yönlendirme işlemlerini görüntüleme',
                'group' => 'default',
                'title' => 'Yönlendirmeler',
                'group_id' => 0
            ],
            [
                'name' => 'view translation',
                'guard_name' => 'admin',
                'group_title' => 'Genel Ayarlar',
                'description' => 'Çeviri merkezi görüntüleme ve düzenleme',
                'group' => 'default',
                'title' => 'Çeviri Yönetimi',
                'group_id' => 0
            ]
        ];
        foreach($permissions as $permission){
            $permission_item = Permission::firstOrCreate([
                'name' => $permission['name'],
                'guard_name' => $permission['guard_name']
            ]);

            PermissionGroup::firstOrCreate([
                'group_id' => $permission['group_id'],
                'permission_id' => $permission_item->id,
                'group_name' => $permission['group_title'],
                'title' => $permission['title'],
                'description' => $permission['description']
            ]);
        }

    }
}
