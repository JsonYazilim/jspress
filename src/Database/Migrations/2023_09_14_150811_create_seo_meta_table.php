<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: 2023_09_14_150811_create_seo_meta_table.php
 * Author: Json Yazılım
 * Class: 2023_09_14_150811_create_seo_meta_table.php
 * Current Username: Erdinc
 * Last Modified: 14.09.2023 15:08
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('seo_meta', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('element_type', 100);
            $table->bigInteger('element_id')->unsigned();
            $table->string('type', 100);
            $table->string('prefix', 10)->nullable();
            $table->string('key', 100);
            $table->text('content')->nullable();
            $table->timestamps();

            $table->index('element_id');
            $table->index('type');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('seo_meta');
    }
};
