<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: 2023_09_27_143445_create_post_extras_table.php
 * Author: Json Yazılım
 * Class: 2023_09_27_143445_create_post_extras_table.php
 * Current Username: Erdinc
 * Last Modified: 27.09.2023 14:34
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('post_extras', function (Blueprint $table) {
            $table->id();
            $table->string('type', 125);
            $table->bigInteger('element_id');
            $table->string('key', 255);
            $table->longText('value')->nullable();
            $table->timestamps();

            $table->index('key');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('post_extras');
    }
};
