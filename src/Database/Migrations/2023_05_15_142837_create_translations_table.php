<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('translations', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->bigIncrements('id')->unsigned()->autoIncrement();
            $table->bigInteger('translation_string_id')->unsigned();
            $table->string('locale', 7);
            $table->text('value')->nullable();
            $table->timestamps();

            $table->foreign('translation_string_id')->references('id')->on('translation_strings')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('translations');
    }
};
