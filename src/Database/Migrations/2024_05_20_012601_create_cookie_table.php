<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: 2024_05_20_012601_create_cookie_table.php
 * Author: Json Yazılım
 * Class: 2024_05_20_012601_create_cookie_table.php
 * Current Username: Erdinc
 * Last Modified: 20.05.2024 01:26
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cookie', function (Blueprint $table) {
            $table->id();
            $table->string('cookie_name')->default('jspress_cookie');
            $table->json('consentModal');
            $table->json('preferencesModal');
            $table->json('categories');
            $table->json('languages');
            $table->json('translations');
            $table->string('theme')->default('default-light');
            $table->smallInteger('page_interaction')->default(0);
            $table->smallInteger('status')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cookie');
    }
};
