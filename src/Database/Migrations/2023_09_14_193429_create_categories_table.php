<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: 2023_09_14_193429_create_categories_table.php
 * Author: Json Yazılım
 * Class: 2023_09_14_193429_create_categories_table.php
 * Current Username: Erdinc
 * Last Modified: 14.09.2023 19:34
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->bigInteger('category_id')->default(0);
            $table->string('category_type', 100);
            $table->string('title', 1000);
            $table->longText('content')->nullable();
            $table->string('slug', 1000);
            $table->bigInteger('author')->nullable()->unsigned();
            $table->dateTime('publish_date');
            $table->dateTime('publish_date_gmt');
            $table->bigInteger('order');
            $table->tinyInteger('status');
            $table->timestamps();
            $table->foreign('author')->references('id')->on('admins')->nullOnDelete();

            $table->index('category_type');
            $table->index('slug');
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('categories');
    }
};
