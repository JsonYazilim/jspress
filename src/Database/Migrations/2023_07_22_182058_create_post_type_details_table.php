<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: 2023_07_22_182058_create_post_type_details_table.php
 * Author: Json Yazılım
 * Class: 2023_07_22_182058_create_post_type_details_table.php
 * Current Username: Erdinc
 * Last Modified: 22.07.2023 18:20
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('post_type_details', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('id')->unsigned();
            $table->integer('post_type_id')->unsigned();
            $table->smallInteger('region')->default(1);
            $table->string('locale', 3);
            $table->string('model', 255);
            $table->string('singular_name', 255);
            $table->string('plural_name', 255);
            $table->string('slug', 255);
            $table->smallInteger('status')->default(1);
            $table->timestamps();
            $table->foreign('post_type_id')->references('id')->on('post_types')->onDelete('cascade');

            $table->index('locale');
            $table->index('region');
            $table->index('model');
            $table->index('status');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('post_type_details');
    }
};
