<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: 2023_07_06_142006_create_media_details_table.php
 * Author: Json Yazılım
 * Class: 2023_07_06_142006_create_media_details_table.php
 * Current Username: Erdinc
 * Last Modified: 6.07.2023 14:20
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('media_details', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigInteger('media_id')->unsigned();
            $table->string('region', 3)->default(1);
            $table->string('lang', 3);
            $table->string('tag', 1000)->nullable();
            $table->string('title', 1000)->nullable();
            $table->string('caption', 1000)->nullable();
            $table->timestamps();
            $table->foreign('media_id')->references('id')->on('media')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('media_details');
    }
};
