<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: 2023_09_29_220737_create_post_fields_table.php
 * Author: Json Yazılım
 * Class: 2023_09_29_220737_create_post_fields_table.php
 * Current Username: Erdinc
 * Last Modified: 29.09.2023 22:07
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('post_fields', function (Blueprint $table) {
            $table->id();
            $table->string('title', 255);
            $table->string('key', 255);
            $table->string('position', 100);
            $table->string('description', 500)->nullable();
            $table->enum('appearence', ['accordion', 'card', 'none'])->default('accordion');
            $table->smallInteger('is_accordion_open')->default(0);
            $table->json('data');
            $table->json('conditions');
            $table->integer('order');
            $table->timestamps();

            $table->index('key');
            $table->index('position');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('post_fields');
    }
};
