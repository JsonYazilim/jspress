<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: 2024_05_09_004329_create_popup_custom_urls_table.php
 * Author: Json Yazılım
 * Class: 2024_05_09_004329_create_popup_custom_urls_table.php
 * Current Username: Erdinc
 * Last Modified: 9.05.2024 00:43
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('popup_custom_urls', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->bigInteger('popup_id')->unsigned();
            $table->string('url', 1000);
            $table->timestamps();

            $table->foreign('popup_id')->references('id')->on('popups')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('popup_custom_urls');
    }
};
