<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: 2023_06_24_113013_create_media_table.php
 * Author: Json Yazılım
 * Class: 2023_06_24_113013_create_media_table.php
 * Current Username: Erdinc
 * Last Modified: 24.06.2023 11:30
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('media', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->integer('user_id')->default(1);
            $table->string('upload_name', 1000);
            $table->string('file_name', 1000);
            $table->string('extension', 100);
            $table->string('mime', 100);
            $table->string('type', 100);
            $table->integer('width')->nullable();
            $table->integer('height')->nullable();
            $table->string('size', 100);
            $table->string('path', 100);
            $table->string('original_url', 1000);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('media');
    }
};
