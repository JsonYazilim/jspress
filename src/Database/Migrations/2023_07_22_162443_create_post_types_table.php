<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: 2023_07_22_162443_create_post_types_table.php
 * Author: Json Yazılım
 * Class: 2023_07_22_162443_create_post_types_table.php
 * Current Username: Erdinc
 * Last Modified: 22.07.2023 16:24
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('post_types', function (Blueprint $table) {
            $table->engine = "InnoDB";
            $table->increments('id')->unsigned();
            $table->integer('post_type_id')->default(0);
            $table->string('name', 255);
            $table->string('key', 500);
            $table->text('icon');
            $table->smallInteger('is_category')->default(0);
            $table->smallInteger('is_editor')->default(1);
            $table->smallInteger('is_terms')->default(0);
            $table->smallInteger('is_image')->default(0);
            $table->smallInteger('is_seo')->default(1);
            $table->smallInteger('is_url')->default(1);
            $table->smallInteger('is_category_url')->default(0);
            $table->smallInteger('is_segment_disable')->default(0);
            $table->smallInteger('is_menu')->default(1);
            $table->smallInteger('is_searchable')->default(1);
            $table->integer('order')->default(999);
            $table->smallInteger('status')->default(1);
            $table->json('columns')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('post_types');
    }
};
