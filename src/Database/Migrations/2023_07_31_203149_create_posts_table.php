<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: 2023_07_31_203149_create_posts_table.php
 * Author: Json Yazılım
 * Class: 2023_07_31_203149_create_posts_table.php
 * Current Username: Erdinc
 * Last Modified: 31.07.2023 20:31
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->bigInteger('post_id')->default(0);
            $table->string('post_type', 100);
            $table->string('title', 1000);
            $table->longText('content')->nullable();
            $table->string('slug', 1000)->nullable();
            $table->bigInteger('author')->nullable()->unsigned();
            $table->dateTime('publish_date');
            $table->dateTime('publish_date_gmt');
            $table->tinyInteger('status');
            $table->bigInteger('order')->default(0);
            $table->smallInteger('is_homepage')->default(0);
            $table->string('view', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('author')->references('id')->on('admins')->nullOnDelete();

            $table->index('post_type');
            $table->index('status');
            $table->index('slug');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('posts');
    }
};
