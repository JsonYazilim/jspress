<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: 2024_09_04_161702_update_admin_table.php
 * Author: Json Yazılım
 * Class: 2024_09_04_161702_update_admin_table.php
 * Current Username: Erdinc
 * Last Modified: 4.09.2024 16:17
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('admins', function (Blueprint $table) {
            $table->smallInteger('is_two_factor')->default(0)->after('theme');
            $table->integer('verification_code')->after('is_two_factor')->nullable();
            $table->timestamp('last_verification_at')->nullable()->after('verification_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        //
    }
};
