<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: 2023_10_12_143701_create_post_views_table.php
 * Author: Json Yazılım
 * Class: 2023_10_12_143701_create_post_views_table.php
 * Current Username: Erdinc
 * Last Modified: 12.10.2023 14:37
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('post_views', function (Blueprint $table) {
            $table->id();
            $table->string('type', 20)->nullable();
            $table->bigInteger('element_id')->nullable();
            $table->string('locale', 4);
            $table->integer('region')->default(1);
            $table->string('ip', 20)->nullable();
            $table->string('user_agent', 255)->nullable();
            $table->text('url')->nullable();
            $table->string('device', 15)->nullable();
            $table->string('device_name', 100)->nullable();
            $table->string('browser', 50)->nullable();
            $table->string('platform', 50)->nullable();
            $table->timestamps();

            $table->index('element_id');
            $table->index('type');
            $table->index('created_at');
            $table->index('locale');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('post_views');
    }
};
