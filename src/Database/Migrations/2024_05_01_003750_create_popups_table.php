<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: 2024_05_01_003750_create_popups_table.php
 * Author: Json Yazılım
 * Class: 2024_05_01_003750_create_popups_table.php
 * Current Username: Erdinc
 * Last Modified: 1.05.2024 00:37
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('popups', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('title', 255);
            $table->text('content')->nullable();
            $table->enum('type',['lightbox', 'tooltip', 'bar', 'screen']);
            $table->string('place', 10)->nullable();
            $table->smallInteger('page_show_type')->default(0);
            $table->smallInteger('is_display_name')->default(0);
            $table->smallInteger('is_content_image')->default(0);
            $table->smallInteger('button_type')->default(1);
            $table->string('button_text', 100)->nullable();
            $table->smallInteger('impressions')->default(0);
            $table->integer('impression_count')->default(3);
            $table->smallInteger('date_type')->default(0);
            $table->date('start_date')->nullable();
            $table->date('end_date')->nullable();
            $table->smallInteger('time_type')->default(0);
            $table->integer('display_time')->nullable();
            $table->smallInteger('conditions')->default(0);
            $table->integer('condition_time')->default(1);
            $table->integer('condition_scroll')->default(100);
            $table->json('design');
            $table->text('custom_css')->nullable();
            $table->smallInteger('status')->default(1);
            $table->integer('order')->default(999);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('popups');
    }
};
