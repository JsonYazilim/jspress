<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: 2023_08_06_154701_create_post_translations_table.php
 * Author: Json Yazılım
 * Class: 2023_08_06_154701_create_post_translations_table.php
 * Current Username: Erdinc
 * Last Modified: 6.08.2023 15:47
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('post_translations', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->string('type');
            $table->bigInteger('element_id');
            $table->bigInteger('transaction_id');
            $table->smallInteger('region')->default(1);
            $table->string('locale', 5);
            $table->string('source_locale', 5)->nullable();

            $table->index('element_id');
            $table->index('transaction_id');
            $table->index('locale');
            $table->index('region');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('post_translations');
    }
};
