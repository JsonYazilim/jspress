<?php

namespace JsPress\JsPress\Interfaces;

interface PostTranslationInterface{

    /**
     * @param int $element_id
     * @return PostTranslationInterface
     */
    public function setElementId(int $element_id): PostTranslationInterface;

    /**
     * @param int|null $transaction_id
     * @return PostTranslationInterface
     */
    public function setTransactionId(int|null $transaction_id): PostTranslationInterface;

    /**
     * @param int $region
     * @return PostTranslationInterface
     */
    public function setRegion(int $region): PostTranslationInterface;

    /**
     * @param string $locale
     * @return PostTranslationInterface
     */
    public function setLocale(string $locale): PostTranslationInterface;

    /**
     * @param string|null $source_locale
     * @return PostTranslationInterface
     */
    public function setSourceLocale(?string $source_locale): PostTranslationInterface;

    /**
     * @param string $type
     * @return PostTranslationInterface
     */
    public function setType(string $type): PostTranslationInterface;

    /**
     * @return PostTranslationInterface
     */
    public function generate(): PostTranslationInterface;

    /**
     * @return void
     */
    public function save(): void;

    /**
     * @return void
     */
    public function delete(): void;

    /**
     * @return void
     */
    public function deleteItem(): void;
}
