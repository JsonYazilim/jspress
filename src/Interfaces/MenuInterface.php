<?php

namespace JsPress\JsPress\Interfaces;

interface MenuInterface{

    /**
     * @param int|null $id
     * @return MenuInterface
     */
    public function setId(?int $id): MenuInterface;

    /**
     * @param string $title
     * @return MenuInterface|null
     */
    public function setTitle(string $title):? MenuInterface;

    /**
     * @param string $slug
     * @return MenuInterface|null
     */
    public function setSlug(string $slug):? MenuInterface;

    /**
     * @param int $region
     * @return MenuInterface
     */
    public function setRegion(int $region): MenuInterface;

    /**
     * @param string $locale
     * @return MenuInterface
     */
    public function setLocale(string $locale): MenuInterface;

    /**
     * @param array|null $data
     * @return MenuInterface
     */
    public function setData(?array $data): MenuInterface;

    /**
     * @param int|null $transaction_id
     * @return MenuInterface
     */
    public function setTransactionId(?int $transaction_id): MenuInterface;

    /**
     * @param string|null $source_locale
     * @return MenuInterface
     */
    public function setSourceLocale(?string $source_locale): MenuInterface;

    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @return array
     */
    public function getMenuOptions(): array;

    /**
     * @return MenuInterface
     */
    public function save(): MenuInterface;

    /**
     * @return array|null
     */
    public function get():? array;

    /**
     * @return array|null
     */
    public function getPostByTransactionId():? array;

    /**
     * @return array|null
     */
    public function getMenuData():? array;

    /**
     * @return void
     */
    public function delete(): void;


}
