<?php

namespace JsPress\JsPress\Interfaces;

interface CategoryInterface{

    public function category(string $slug, string $category_type): ?object;

}
