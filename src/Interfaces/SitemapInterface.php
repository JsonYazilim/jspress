<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: SitemapInterface.php
 * Author: Json Yazılım
 * Class: SitemapInterface.php
 * Current Username: Erdinc
 * Last Modified: 18.04.2024 01:27
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Interfaces;

interface SitemapInterface{

    /**
     * @return array
     */
    public function getPosts(): array;

}
