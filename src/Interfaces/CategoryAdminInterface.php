<?php

namespace JsPress\JsPress\Interfaces;

interface CategoryAdminInterface{

    /**
     * @param string $post_type
     * @return object|null
     */
    public function setPostType(string $post_type):? object;

    /**
     * @param int|null $id
     * @return CategoryAdminInterface
     */
    public function setId(?int $id): CategoryAdminInterface;
    /**
     * @param int $category_id
     * @return CategoryAdminInterface
     */
    public function setCategoryId(int $category_id): CategoryAdminInterface;

    /**
     * @param string $title
     * @return CategoryAdminInterface
     */
    public function setTitle(string $title): CategoryAdminInterface;

    /**
     * @param string|null $slug
     * @return CategoryAdminInterface
     */
    public function setSlug(?string $slug): CategoryAdminInterface;

    /**
     * @param int|null $author
     * @return CategoryAdminInterface
     */
    public function setAuthor(?int $author): CategoryAdminInterface;

    /**
     * @param string|null $content
     * @return CategoryAdminInterface
     */
    public function setContent(?string $content): CategoryAdminInterface;

    /**
     * @param array|null $seo
     * @return CategoryAdminInterface
     */
    public function setSeo(?array $seo): CategoryAdminInterface;

    /**
     * @param int $status
     * @return CategoryAdminInterface
     */
    public function setStatus(int $status): CategoryAdminInterface;

    /**
     * @param array|null $extras
     * @return CategoryAdminInterface
     */
    public function setExtras(?array $extras): CategoryAdminInterface;

    /**
     * @param string $publish_date
     * @return CategoryAdminInterface
     */
    public function setPublishDate(string $publish_date): CategoryAdminInterface;

    /**
     * @param int $order
     * @return CategoryAdminInterface
     */
    public function setOrder(int $order): CategoryAdminInterface;

    /**
     * @param string $locale
     * @return CategoryAdminInterface
     */
    public function setLocale(string $locale): CategoryAdminInterface;

    /**
     * @param int $region
     * @return CategoryAdminInterface
     */
    public function setRegion(int $region): CategoryAdminInterface;

    /**
     * @param int|null $transaction_id
     * @return CategoryAdminInterface
     */
    public function setTransactionId(?int $transaction_id): CategoryAdminInterface;

    /**
     * @param string|null $source_locale
     * @return CategoryAdminInterface
     */
    public function setSourceLocale(?string $source_locale): CategoryAdminInterface;

    /**
     * @return void
     */
    public function save(): void;

    /**
     * @return void
     */
    public function delete(): void;

    /**
     * @param int $id
     * @return array
     */
    public function getCategory(int $id): array;

    /**
     * @param array $category_data
     * @param int $category_id
     * @param int $current_category
     * @return array
     */
    public function getCategories(int $current_category = 0, array &$category_data = [], int $category_id = 0): array;

    /**
     * @param int $post_type_id
     * @return array
     */
    public function getActiveLanguages(int $post_type_id): array;

    /**
     * @return array
     */
    public function getOtherLanguagesCategories(): array;

    /**
     * @param int $transaction_id
     * @return array
     */
    public function getCategoryTranslation(int $transaction_id): array;

    /**
     * @param int $category_id
     * @return void
     */
    public function deleteTranslationRelation(int $category_id): void;

    /**
     * @param array|null $post
     * @return array
     */
    public function getPostField(array|null $post = null): array;

}
