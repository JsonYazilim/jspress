<?php

namespace JsPress\JsPress\Interfaces;

interface SeoInterface{

    /**
     * @param string $element_type
     * @return SeoInterface
     */
    public function setElementType(string $element_type): SeoInterface;

    /**
     * @param int $element_id
     * @return SeoInterface
     */
    public function setElementId(int $element_id): SeoInterface;

    /**
     * @param string $type
     * @return SeoInterface
     */
    public function setType(string $type): SeoInterface;

    /**
     * @param string|null $prefix
     * @return SeoInterface
     */
    public function setPrefix(?string $prefix): SeoInterface;

    /**
     * @param string $key
     * @return SeoInterface
     */
    public function setKey(string $key): SeoInterface;

    /**
     * @param string|null $content
     * @return SeoInterface
     */
    public function setContent(string|null $content): SeoInterface;

    /**
     * @return void
     */
    public function save(): void;

    /**
     * @return void
     */
    public function delete():void;
}
