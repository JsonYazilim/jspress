<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: UserInterface.php
 * Author: Json Yazılım
 * Class: UserInterface.php
 * Current Username: Erdinc
 * Last Modified: 14.07.2023 11:23
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Interfaces;

interface UserInterface{

    public function save(object $object, int $id, array $data);

}
