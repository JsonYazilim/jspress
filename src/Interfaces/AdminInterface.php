<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: AdminInterface.php
 * Author: Json Yazılım
 * Class: AdminInterface.php
 * Current Username: Erdinc
 * Last Modified: 14.07.2023 14:00
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Interfaces;

use Illuminate\Http\Request;
use JsPress\JsPress\Models\Admin;
use Illuminate\Pagination\LengthAwarePaginator;

interface AdminInterface{

    /**
     * @param object|null $file
     * @return self
     */
    public function setAvatar(object|null $file): self;

    /**
     * @param int $id
     * @return Admin
     */
    public function get(int $id): Admin;

    /**
     * @param Admin $admin
     * @param array $data
     * @return Admin
     */
    public function save(Admin $admin, array $data): Admin;

    /**
     * @param Admin $admin
     * @param string $role
     * @return void
     */
    public function assign_role(Admin $admin, string $role): void;

    /**
     * @param Admin $admin
     * @param object $role
     * @return void
     */
    public function sync_role(Admin $admin, object $role): void;

    /**
     * @param Admin $admins
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function list(Admin $admins, Request $request): LengthAwarePaginator;

}
