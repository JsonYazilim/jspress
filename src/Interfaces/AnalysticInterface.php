<?php

namespace JsPress\JsPress\Interfaces;

interface AnalysticInterface{

    /**
     * @param string|null $start_date
     * @return AnalysticInterface
     */
    public function setStartDate(?string $start_date): AnalysticInterface;

    /**
     * @param string|null $end_date
     * @return AnalysticInterface
     */
    public function setEndDate(?string $end_date): AnalysticInterface;

    /**
     * @param string $type
     * @return AnalysticInterface
     */
    public function setType(string $type): AnalysticInterface;

    /**
     * @return array
     */
    public function track(): array;

}
