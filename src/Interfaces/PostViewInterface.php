<?php

namespace JsPress\JsPress\Interfaces;

interface PostViewInterface{

    /**
     * @param int $element_id
     * @return PostViewInterface
     */
    public function setElementId(int $element_id): PostViewInterface;

    /**
     * @param string $type
     * @return PostViewInterface
     */
    public function setType(string $type): PostViewInterface;

    /**
     * @param string $locale
     * @return PostViewInterface
     */
    public function setLocale(string $locale): PostViewInterface;

    /**
     * @param int $region
     * @return PostViewInterface
     */
    public function setRegion(int $region): PostViewInterface;

    /**
     * @param string|null $ip
     * @return PostViewInterface
     */
    public function setIp(?string $ip): PostViewInterface;

    /**
     * @param string|null $user_agent
     * @return PostViewInterface
     */
    public function setUserAgent(?string $user_agent): PostViewInterface;

    /**
     * @param string|null $url
     * @return PostViewInterface
     */
    public function setUrl(?string $url): PostViewInterface;

    /**
     * @param string|null $device
     * @return PostViewInterface
     */
    public function setDevice(?string $device): PostViewInterface;

    /**
     * @param string|null $device_name
     * @return PostViewInterface
     */
    public function setDeviceName(?string $device_name): PostViewInterface;

    /**
     * @param string|null $browser
     * @return PostViewInterface
     */
    public function setBrowser(?string $browser): PostViewInterface;

    /**
     * @param string|null $platform
     * @return PostViewInterface
     */
    public function setPlatform(?string $platform): PostViewInterface;

    /**
     * @return void
     */
    public function save(): void;

}
