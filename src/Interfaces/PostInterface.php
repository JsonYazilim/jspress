<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PostInterface.php
 * Author: Json Yazılım
 * Class: PostInterface.php
 * Current Username: Erdinc
 * Last Modified: 24.07.2023 15:32
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Interfaces;

use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;

interface PostInterface{

    /**
     * @param string $slug
     * @param int $parent_id
     * @return object|null
     */
    public function post(string $slug, int $parent_id = 0): ?object;

    /**
     * @return object|null
     */
    public function homepage():? object;
}
