<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PostAdminInterface.php
 * Author: Json Yazılım
 * Class: PostAdminInterface.php
 * Current Username: Erdinc
 * Last Modified: 29.09.2023 16:10
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Interfaces;

interface PostAdminInterface{

    /**
     * @param string $post_type
     * @return object|null
     */
    public function setPostType(string $post_type):? object;

    /**
     * @param array $data
     * @param bool $trash
     * @return array
     */
    public function getTable(array $data, bool $trash = false): array;
    /**
     * @return array
     */
    public function getTableColumns(): array;

    /**
     * @return array
     */
    public function getPostData(): array;

    /**
     * @return array
     */
    public function getPost(): array;

    /**
     * @return array|null
     */
    public function getPostByTransactionId():? array;

    /**
     * @return PostAdminInterface
     */
    public function save(): PostAdminInterface;

    /**
     * @return void
     */
    public function destroy(): void;

    /**
     * @return void
     */
    public function restore(): void;

    /**
     * @return void
     */
    public function delete(): void;

    /**
     * @param object $post_type
     * @return array
     */
    public function prepare(object $post_type): array;

    /**
     * @param string $slug
     * @param int $count
     * @param string|null $base_slug
     * @return string
     */
    public function generateSlug(string $slug, int $count = 0, string $base_slug = null): string;

    /**
     * @param int|null $id
     * @return PostAdminInterface
     */
    public function setId(?int $id): PostAdminInterface;

    /**
     * @param int $post_id
     * @return PostAdminInterface
     */
    public function setPostId(int $post_id): PostAdminInterface;

    /**
     * @return int|null
     */
    public function getId(): ?int;
    /**
     * @param string $title
     * @return PostAdminInterface
     */
    public function setTitle(string $title): PostAdminInterface;

    /**
     * @param string|null $content
     * @return PostAdminInterface
     */
    public function setContent(?string $content): PostAdminInterface;

    /**
     * @param array|null $categories
     * @return PostAdminInterface
     */
    public function setCategories(?array $categories): PostAdminInterface;

    /**
     * @param array|null $seo
     * @return PostAdminInterface
     */
    public function setSeo(?array $seo): PostAdminInterface;

    /**
     * @param int $status
     * @return PostAdminInterface
     */
    public function setStatus(int $status): PostAdminInterface;

    /**
     * @param array|null $extras
     * @return PostAdminInterface
     */
    public function setExtras(?array $extras): PostAdminInterface;

    /**
     * @param string $publish_date
     * @return PostAdminInterface
     */
    public function setPublishDate(string $publish_date): PostAdminInterface;

    /**
     * @param string|null $slug
     * @return PostAdminInterface
     */
    public function setSlug(?string $slug): PostAdminInterface;

    /**
     * @param int|null $author
     * @return PostAdminInterface
     */
    public function setAuthor(?int $author): PostAdminInterface;

    /**
     * @param int $order
     * @return PostAdminInterface
     */
    public function setOrder(int $order): PostAdminInterface;

    /**
     * @param string $locale
     * @return PostAdminInterface
     */
    public function setLocale(string $locale): PostAdminInterface;

    /**
     * @return string
     */
    public function getLocale(): string;

    /**
     * @param int $region
     * @return PostAdminInterface
     */
    public function setRegion(int $region): PostAdminInterface;

    /**
     * @param int|null $transaction_id
     * @return PostAdminInterface
     */
    public function setTransactionId(?int $transaction_id): PostAdminInterface;

    /**
     * @param string|null $source_locale
     * @return PostAdminInterface
     */
    public function setSourceLocale(?string $source_locale): PostAdminInterface;

    /**
     * @param int $is_homepage
     * @return PostAdminInterface
     */
    public function setIsHomepage(int $is_homepage): PostAdminInterface;

    /**
     * @param string|null $view
     * @return PostAdminInterface
     */
    public function setView(?string $view): PostAdminInterface;

    /**
     * @param array|null $post
     * @return array
     */
    public function getPostField(array|null $post = null): array;
}

