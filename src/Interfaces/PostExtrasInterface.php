<?php

namespace JsPress\JsPress\Interfaces;

interface PostExtrasInterface{

    /**
     * @param string $type
     * @return PostExtrasInterface
     */
    public function setType(string $type): PostExtrasInterface;

    /**
     * @param int $element_id
     * @return PostExtrasInterface
     */
    public function setElementId(int $element_id): PostExtrasInterface;

    /**
     * @param string $key
     * @return PostExtrasInterface
     */
    public function setKey(string $key): PostExtrasInterface;

    /**
     * @param string|null $value
     * @return PostExtrasInterface
     */
    public function setValue(?string $value): PostExtrasInterface;

    /**
     * @param array|null $extras
     * @return PostExtrasInterface
     */
    public function setExtras(?array $extras): PostExtrasInterface;

    /**
     * @return void
     */
    public function save(): void;

    /**
     * @return void
     */
    public function saveMany(): void;

    /**
     * @return void
     */
    public function delete(): void;

    /**
     * @return void
     */
    public function deleteItems(): void;

}
