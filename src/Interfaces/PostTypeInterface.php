<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PostTypeInterface.php
 * Author: Json Yazılım
 * Class: PostTypeInterface.php
 * Current Username: Erdinc
 * Last Modified: 24.07.2023 15:32
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Interfaces;


use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use JsPress\JsPress\Models\PostType;
use Illuminate\Http\Request;

interface PostTypeInterface{

    /**
     * @param string $key
     * @param int|string|null $model_id
     * @return bool
     */
    public function validateKey(string $key, null|int|string $model_id): bool;

    /**
     * @param PostType $postType
     * @param array $data
     * @return self
     */
    public function save(PostType $postType, array $data): self;

    /**
     * @param Request|null $request
     * @param bool $is_menu
     * @return LengthAwarePaginator|Collection
     */
    public function list(Request|null $request, bool $is_menu = false): LengthAwarePaginator|Collection;

    /**
     * @return array
     */
    public function permissionElements(): array;

    /**
     * @param array $permissions
     * @return void
     */
    public function setPermissions(array $permissions): void;

    /**
     * @param PostType $postType
     * @return array
     */
    public function getPermissionsValues(PostType $postType): array;

    /**
     * @return void
     */
    public function createFiles(): void;

    /**
     * @param string $key
     * @return bool
     */
    public function is_exist(string $key): bool;

}
