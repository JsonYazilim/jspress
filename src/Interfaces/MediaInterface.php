<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: MediaInterface.php
 * Author: Json Yazılım
 * Class: MediaInterface.php
 * Current Username: Erdinc
 * Last Modified: 3.07.2023 10:27
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */


namespace JsPress\JsPress\Interfaces;


use Illuminate\Support\Facades\File;
use JsPress\JsPress\Models\Media;

interface MediaInterface{

    /**
     * @param string $extension
     * @return bool
     */
    public function hasSize(string $extension): bool;

    /**
     * @param object $file
     * @return array
     */
    public function getSizes(object $file): array;
    /**
     * @return array
     */
    public function allowed_mime_types(): array;

    /**
     * @return self
     */
    public function crop(): self;

    /**
     * @param File $file
     * @return self
     */
    public function setFile(File $file): self;

    /**
     * @param int $id
     * @return self
     */
    public function setId(int $id): self;

    /**
     * @param string $upload_name
     * @return self
     */
    public function setUploadName(string $upload_name): self;

    /**
     * @param string $file_name
     * @return self
     */
    public function setFileName(string $file_name): self;

    /**
     * @param string $extension
     * @return self
     */
    public function setExtension(string $extension): self;

    /**
     * @param string $mime
     * @return self
     */
    public function setMime(string $mime): self;

    /**
     * @param string $type
     * @return self
     */
    public function setType(string $type): self;

    /**
     * @param int $width
     * @return self
     */
    public function setWidth(int $width): self;

    /**
     * @param int $height
     * @return self
     */
    public function setHeight(int $height): self;

    /**
     * @param int $size
     * @return self
     */
    public function setSize(int $size): self;

    /**
     * @param string|null $path
     * @return self
     */
    public function setPath(string|null $path): self;

    /**
     * @param string|null $directory
     * @return self
     */
    public function setDirectory(string|null $directory): self;

    /**
     * @param int $user_id
     * @return self
     */
    public function setUserId(int $user_id): self;

    /**
     * @return string
     */
    public function getMimeType(): string;

    /**
     * @return Media
     */
    public function save(): Media;

    /**
     * @return self
     */
    public function move(): self;

    /**
     * @return self
     */
    public function unlink(): self;

    /**
     * @param int $id
     * @return self
     */
    public function get(int $id): self;

    /**
     * @param array|null $data
     * @return array
     */
    public function getData(array|null $data): array;

    /**
     * @param int $id
     * @return array|null
     */
    public function getDetail(int $id): array|null;

    /**
     * @param int $id
     * @param array $details
     * @return void
     */
    public function saveDetails(int $id, array $details): void;

    /**
     * @return bool
     */
    public function delete(): bool;
}
