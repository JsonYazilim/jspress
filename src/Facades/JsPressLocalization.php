<?php

namespace JsPress\JsPress\Facades;

use Illuminate\Support\Facades\Facade;

class JsPressLocalization extends Facade{

    /**
     * @method static string setLocale(string $locale = null)
     *
     * @see \JsPress\JsPress\Services\JsPressLocalization
     */

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'jspresslocalization';
    }

}
