<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: Translation.php
 * Author: Json Yazılım
 * Class: Translation.php
 * Current Username: Erdinc
 * Last Modified: 29.05.2023 12:28
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Imports;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;

class Translation implements ToCollection, WithHeadingRow, WithChunkReading
{

    /**
     *
     */
    protected const SIZE = 500;
    /**
     * @var string
     */
    protected string $locale;

    /**
     * @param string $locale
     */
    public function __construct(string $locale)
    {
        $this->locale = $locale;
    }

    /**
     * @param Collection $rows
     */
    public function collection(Collection $rows): void
    {
        foreach($rows as $row){
            if(!is_null($row['translation'])){
                $translation_string = \DB::table('translation_strings')->where('group', $row['group'])->where('key', $row['key'])->first();
                \JsPress\JsPress\Models\Translation::updateOrCreate(
                    [
                        'translation_string_id' => $translation_string->id,
                        'locale' => $this->locale
                    ],
                    [
                        'value' => $row['translation']
                    ]
                );
                $cacheKey = "translation.".$this->locale.".".$row['group'].".".$row['key'];
                Cache::forget($cacheKey);
                Cache::forever($cacheKey, $row['translation']);
            }
        }
    }

    /**
     * @return int
     */
    public function chunkSize(): int
    {
        return self::SIZE;
    }
}
