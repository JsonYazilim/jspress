<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: Redirect.php
 * Author: Json Yazılım
 * Class: Redirect.php
 * Current Username: Erdinc
 * Last Modified: 1.03.2024 16:04
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Imports;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use \Maatwebsite\Excel\Concerns\WithStrictNullComparison;

class Redirect implements ToCollection, WithHeadingRow, WithChunkReading, WithStrictNullComparison
{
    protected const SIZE = 500;

    /**
    * @param Collection $rows
    */
    public function collection(Collection $rows): void
    {
        if($rows->count() > 0){
            foreach($rows as $row){
                if(is_null($row['new_url'])){
                    dd($row);
                }
                \JsPress\JsPress\Models\Redirect::updateOrCreate(
                    [
                        'old_url' => $row['old_url'],
                        'new_url' => $row['new_url']
                    ],
                    [
                        'status_code' => $row['status_code']
                    ]
                );
            }
        }
    }

    public function chunkSize(): int
    {
        return self::SIZE;
    }
}
