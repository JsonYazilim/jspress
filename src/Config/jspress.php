<?php

return [
    'language' => [
        'default_language' => env('DEFAULT_LANGUAGE', 'tr'),
        'hide_url_default_language' => env('HIDE_URL_DEFAULT_LANGUAGE', true),
        'enable_browser_language_redirect' => env('ENABLE_BROWSER_REDIRECT', true)
    ],
    'files' => [
        'images' => [
            'sizes' => [
                [
                    'width' => 150,
                    'height' => 150,
                    'name' => 'thumbnail'
                ],
                [
                    'width' => 300,
                    'height' => 300,
                    'name' => 'medium'
                ],
                [
                    'width' => 1024,
                    'height' => 1024,
                    'name' => 'large'
                ]
            ]
        ]
    ],
    'panel' => [
        'locales' => ['tr', 'en', 'de'],
        'logos' => [
            'aside_logo' => env('ADMIN_ASIDE_LOGO', 'jspress/assets/media/logos/jsonlogo.png'),
            'logo' => env('ADMIN_LOGO', 'jspress/assets/media/logos/jsonlogo.png'),
            'favicon' => env('ADMIN_FAVICON', 'jspress/assets/media/logos/json.png')
        ],
        'title' => env('ADMIN_PANEL_TITLE', 'JsPress CMS'),
        'login_title' => env('ADMIN_LOGIN_TITLE', 'JS-Press Yönetim Paneli'),
        'hide_menu' => [],
        'auth' => [
            '2fa' => env('ADMIN_AUTH_2FA', false),
        ]
    ],
    'image' => [
        'webp' => env('WEBP_IS_ACTIVE', false),
        'quality' => env('IMAGE_QUALITY', 100)
    ],
    'sitemap' => env('SITEMAP_ACTIVE', true)
];
