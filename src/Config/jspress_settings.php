<?php

return [
    'tabs' => [
        [
            'title' => 'Genel Ayarlar',
            'icon' => '<i class="bi bi-house fs-2x"></i>',
            'id' => 'general',
            'settings' => [
                [
                    'label' => 'Website Logo',
                    'type' => 'image',
                    'name' => 'logo',
                    'width' => 50
                ],
                [
                    'label' => 'Favicon',
                    'type' => 'image',
                    'name' => 'favicon',
                    'width' => 50
                ]
            ]
        ]
    ]
];
