<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: General.php
 * Author: Json Yazılım
 * Class: General.php
 * Current Username: Erdinc
 * Last Modified: 13.07.2023 20:50
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Helpers;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Cache;
use JsPress\JsPress\Collections\PostCollection;
use JsPress\JsPress\DataSources\CategoryDataSource;
use JsPress\JsPress\DataSources\PostDataSource;
use JsPress\JsPress\Models\Category;
use JsPress\JsPress\Models\Menu;
use JsPress\JsPress\Models\Post;
use Spatie\Permission\Models\Role;
use Illuminate\Pagination\LengthAwarePaginator;

class General{

    /**
     * Get the roles for create new user
     *
     * @return Collection
     */
    public static function getRoles(): Collection
    {
        return Role::orderBy('created_at')->get();
    }

    /**
     * @param $locale
     * @return mixed
     */
    public static function getLanguageNameByLocale($locale): mixed
    {
        return display_language($locale)->display_language_name;
    }

    /**
     * @param $slug
     * @param $locale
     * @return array
     */
    public static function getMenuItems($slug, $locale): array
    {
        $key = 'menu_'.$slug.'_'.$locale;
        return Cache::rememberForever('menu_'.$key, function()use($slug,$locale){
            $menu = Menu::where('slug', $slug)
                ->whereHas('translation', function($query)use($locale){
                    return $query->where('locale', $locale);
                })
                ->first();
            if(!is_null($menu)){
                return self::convertMenuItems($menu->data);
            }
            return [];
        });
    }

    /**
     * @param $args
     * @return PostCollection|null
     */
    public static function getThePosts($args):? PostCollection
    {
        $args = self::setTheArguements($args);
        $posts = self::getThePostData($args);
        $post_data = [];
        foreach($posts as $key => $post){
            $post_data[$key] = new PostDataSource($post, false, $args['include_category']);
        }
        $collection = new PostCollection();
        $collection->setItems($post_data);
        $collection->setLinks($args['paginate'] ? $posts->withQueryString()->links() : null);
        return $collection;
    }

    /**
     * @param $args
     * @return Post|LengthAwarePaginator|null
     */
    public static function getThePostsModel($args): Post|LengthAwarePaginator|null
    {
        $args = self::setTheArguements($args);
        return self::getThePostData($args);
    }

    /**
     * @param $args
     * @return array|null
     */
    public static function getThePostsArray($args):? array
    {

        if(!isset($args['post_id'])){
            $args['post_id'] = 0;
        }
        if(!isset($args['order'])){
            $args['order'] = 'created_at';
        }
        if(!isset($args['orderby'])){
            $args['orderby'] = 'desc';
        }
        if(!isset($args['categories'])){
            $args['categories'] = [];
        }
        if(!isset($args['per_page'])){
            $args['per_page'] = 10;
        }
        if(!isset($args['offset'])){
            $args['offset'] = 0;
        }
        if(!isset($args['paginate'])){
            $args['paginate'] = true;
        }
        if(!isset($args['include_category'])){
            $args['include_category'] = false;
        }

        $posts = Post::where('status', 1)
            ->whereHas('translation', function($query){
                return $query->where('locale', app()->getLocale())->where('region', 1);
            })
            ->when(isset($args['post_type']), function($query)use($args){
                return $query->where('post_type', $args['post_type']);
            })
            ->where('post_id', $args['post_id'])
            ->when(count($args['categories']) > 0, function($query)use($args){
                return $query->whereHas('categories', function($q) use($args){
                    return $q->whereIn('categories.id', $args['categories']);
                });
            })
            ->when(isset($args['filters']), function($query)use($args){
                foreach( $args['filters'] as $key => $filter ){
                    if( $key == 'extras' ){
                        return $query->whereHas('extras', function($q)use($args){
                            foreach( $args['filters']['extras'] as $key => $extra ){
                                $q->where('key', $key)->where('value', $extra);
                            }
                        });
                    }
                }

            })
            ->when(isset($args['search_query']), function($query)use($args){
                foreach($args['search_query'] as $key => $search_query){
                    $query->where($search_query['key'], $search_query['operator'], $search_query['value']);
                }
            })
            ->when(isset($args['excluded']), function($query)use($args){
                return $query->whereNotIn('id', $args['excluded']);
            })
            ->orderBy($args['order'], $args['orderby'])
            ->with(['extras', 'post_type_item']);

        if($args['paginate']){
            $posts = $posts->offset($args['offset'])
                ->paginate(intval($args['per_page']));
        }else{
            $posts = $posts
                ->skip($args['offset'])
                ->take($args['per_page'])
                ->get();
        }
        if($posts->isNotEmpty()){
            $post_data = [];
            foreach($posts as $post){
                $post_data[] = new PostDataSource($post, false, $args['include_category']);
            }

            return $post_data;
        }
        return [];
    }

    /**
     * @param $post_id
     * @return PostDataSource|null
     */
    public static function getThePostById($post_id):? PostDataSource
    {

        $post = Post::where('id', $post_id)
            ->where('status', 1)
            ->with(['extras','post_type_item'])
            ->first();
        if(!$post){
            return null;
        }
        return new PostDataSource($post, false, false);
    }

    /**
     * @param $post_id
     * @return PostDataSource|null
     */
    public static function getThePostByIdWithoutStatus($post_id):? PostDataSource
    {

        $post = Post::where('id', $post_id)
            ->with(['extras','post_type_item'])
            ->first();
        if(!$post){
            return null;
        }
        return new PostDataSource($post, false, false);
    }

    /**
     * @param $post_id
     * @return Post|null
     */
    public static function getThePostByIdForTranslation($post_id):? Post
    {

        return Post::where('status', 1)
            ->where('id', $post_id)
            ->with(['post_type_item'])
            ->first();
    }

    /**
     * @param $template
     * @return PostDataSource|null
     */
    public static function getThePostByTemplate($template):? PostDataSource
    {
        $key = 'template-'.$template;
        $post = Post::where('status', 1)
            ->whereHas('translation', function($query){
                return $query->where('locale', app()->getLocale())->where('region', 1);
            })
            ->where('view', $key)
            ->with(['extras'])
            ->first();

        if(!$post){
            return null;
        }
        return new PostDataSource($post, false, false);
    }

    /**
     * @param $args
     * @return array|null
     */
    public static function getTheCategories($args):? array
    {

        if(!is_array($args)){
            eval("\$args = $args;");
        }
        if(!isset($args['post_type'])){
            $post_type = \DB::table('post_types')->where('status', 1)->where('is_category', 1)->where('post_type_id', 0)->first();
            $args['post_type'] = $post_type->key;
        }
        if(!isset($args['category_id'])){
            $args['category_id'] = 0;
        }
        if(!isset($args['deep'])){
            $args['deep'] = false;
        }
        if(!isset($args['order'])){
            $args['order'] = 'title';
        }
        if(!isset($args['orderby'])){
            $args['orderby'] = 'asc';
        }
        if(!isset($args['posts'])){
            $args['posts'] = [];
        }
        $categories = Category::where('status', 1)
            ->whereHas('translation', function($query){
                return $query->where('locale', app()->getLocale())->where('region', 1);
            })
            ->when(isset($args['post_type']), function($query)use($args){
                return $query->where('category_type', $args['post_type']);
            })
            ->where('category_id', $args['category_id'])
            ->when(count($args['posts']) > 0, function($query)use($args){
                return $query->whereHas('posts', function($q) use($args){
                    return $q->whereIn('posts.id', $args['posts']);
                });
            })
            ->orderBy($args['order'], $args['orderby'])
            ->get();

        if($categories->isNotEmpty()){
            $category_data = [];
            foreach($categories as $category){
                $category_data[] = new CategoryDataSource($category, false);
            }
            return $category_data;
        }
        return [];
    }

    /**
     * @param $id
     * @param $locale
     * @return object
     */
    public static function getThePostByLocale($id, $locale): object
    {
        $post = Post::where('status', 1)
            ->whereHas('translation', function($query)use($locale){
                return $query->where('locale', $locale)->where('region', 1);
            })
            ->where('id', $id)
            ->whereHas('post_type_item', function($query)use($locale){
                return $query->where('status', 1)
                    ->where('is_segment_disable', 1)
                    ->whereHas('post_details', function($q)use($locale){
                        return $q->where('region', 1)->where('locale', $locale);
                    });
            })
            ->with(['post_type_item', 'post_type_item.post_type_detail', 'categories'])
            ->first();
        return new PostDataSource($post, true, false);
    }

    /**
     * @param array $post_types
     * @return array
     */
    public static function getPostsByPostType(array $post_types): array
    {
        $posts = \DB::table('posts')
            ->select([
                'posts.id',
                'posts.title'
            ])
            ->join('post_translations', function($join){
                $join->on('post_translations.element_id', '=', 'posts.id')
                    ->where('post_translations.type', 'post')
                    ->where('post_translations.region', 1)
                    ->where('post_translations.locale', JsPressPanel::contentLocale());;
            })
            ->whereIn('posts.post_type', $post_types)
            ->orderBy('created_at', 'DESC')
            ->get();
        $post_data = [];
        foreach($posts as $post){
            $post_data[] = [
                'value' => $post->id,
                'label' => $post->title
            ];
        }
        return $post_data;
    }

    /**
     * @param array $post_types
     * @return array
     */
    public static function getCategoriesByPostType(array $post_types): array
    {
        $categories = \DB::table('categories')
            ->select([
                'categories.id',
                'categories.title'
            ])
            ->join('post_translations', function($join){
                $join->on('post_translations.element_id', '=', 'categories.id')
                    ->where('post_translations.type', 'category')
                    ->where('post_translations.region', 1)
                    ->where('post_translations.locale', JsPressPanel::contentLocale());;
            })
            ->whereIn('categories.category_type', $post_types)
            ->orderBy('created_at', 'DESC')
            ->get();
        $category_data = [];
        foreach($categories as $category){
            $category_data[] = [
                'value' => $category->id,
                'label' => $category->title
            ];
        }
        return $category_data;
    }

    /**
     * @param $id
     * @return Category
     */
    public static function getTheCategory($id): Category
    {
        return Category::select(
            'categories.id',
            'categories.category_id',
            'categories.title',
            'categories.content',
            'categories.slug',
            'categories.author',
            'categories.publish_date',
            'categories.publish_date_gmt',
            'categories.order',
            'categories.status'
        )
            ->join('post_translations as category_translation', function($join){
                $join->on('category_translation.element_id', '=', 'categories.id')
                    ->where('category_translation.type', 'category')
                    ->where('category_translation.region', 1)
                    ->where('category_translation.locale', app()->getLocale());
            })
            ->where('categories.id', $id)
            ->first();
    }

    /**
     * @param $id
     * @param $locale
     * @return Category
     */
    public static function getTheCategoryByLocale($id, $locale): Category
    {
        return Category::select(
            'categories.id',
            'categories.category_id',
            'categories.category_type',
            'categories.title',
            'categories.content',
            'categories.slug',
            'categories.author',
            'categories.publish_date',
            'categories.publish_date_gmt',
            'categories.order',
            'categories.status'
        )
            ->join('post_translations as category_translation', function($join)use($locale){
                $join->on('category_translation.element_id', '=', 'categories.id')
                    ->where('category_translation.type', 'category')
                    ->where('category_translation.region', 1)
                    ->where('category_translation.locale', $locale);
            })
            ->where('categories.id', $id)
            ->with(['post_type_item'])
            ->first();
    }

    /**
     * @param $post_id
     * @param $lang
     * @return object
     */
    public static function getThePostCategories($post_id, $lang): object
    {
        return Category::select(
            'categories.id',
            'categories.title',
            'categories.category_id',
            'categories.title',
            'categories.content',
            'categories.slug',
            'categories.author',
            'categories.publish_date',
            'categories.publish_date_gmt',
            'categories.order',
            'categories.status'
        )
            ->join('post_translations as category_translation', function($join)use($lang){
                $join->on('category_translation.element_id', '=', 'categories.id')
                    ->where('category_translation.type', 'category')
                    ->where('category_translation.region', 1)
                    ->where('category_translation.locale', is_null($lang) ? app()->getLocale() : JsPressPanel::contentLocale());
            })
            ->join('category_post', function($join)use($post_id){
                $join->on('category_post.category_id', '=', 'categories.id')
                    ->where('category_post.post_id', $post_id);
            })
            ->get();
    }

    /**
     * @param $args
     * @return mixed
     */
    private static function setTheArguements($args): mixed
    {
        if(!isset($args['post_id'])){
            $args['post_id'] = 0;
        }
        if(!isset($args['order'])){
            $args['order'] = 'created_at';
        }
        if(!isset($args['orderby'])){
            $args['orderby'] = 'desc';
        }
        if(!isset($args['categories'])){
            $args['categories'] = [];
        }
        if(!isset($args['per_page'])){
            $args['per_page'] = 10;
        }
        if(!isset($args['offset'])){
            $args['offset'] = 0;
        }
        if(!isset($args['paginate'])){
            $args['paginate'] = true;
        }

        if(!isset($args['include_category'])){
            $args['include_category'] = false;
        }
        return $args;
    }

    /**
     * @param $args
     * @return Post|LengthAwarePaginator|null
     */
    private static function getThePostData($args): Post|LengthAwarePaginator|null
    {
        $posts = Post::where('status', 1)
            ->whereHas('translation', function($query){
                return $query->where('locale', app()->getLocale())->where('region', 1);
            })
            ->when(isset($args['post_type']), function($query)use($args){
                return $query->where('post_type', $args['post_type']);
            })
            ->where('post_id', $args['post_id'])
            ->when(count($args['categories']) > 0, function($query)use($args){
                return $query->whereHas('categories', function($q) use($args){
                    return $q->whereIn('categories.id', $args['categories']);
                });
            })
            ->when(isset($args['filters']), function($query)use($args){
                foreach( $args['filters'] as $key => $filter ){
                    if( $key == 'extras' ){
                        return $query->whereHas('extras', function($q)use($args){
                            foreach( $args['filters']['extras'] as $key => $extra ){
                                $q->where('key', $key)->where('value', $extra);
                            }
                        });
                    }
                }

            })
            ->when(isset($args['search_query']), function($query)use($args){
                foreach($args['search_query'] as $key => $search_query){
                    $query->where($search_query['key'], $search_query['operator'], $search_query['value']);
                }
            })
            ->when(isset($args['excluded']), function($query)use($args){
                return $query->whereNotIn('id', $args['excluded']);
            })
            ->orderBy($args['order'], $args['orderby'])
            ->with(['extras', 'post_type_item']);
        if($args['paginate']){
            $posts = $posts->offset($args['offset'])
                ->paginate(intval($args['per_page']));
        }else{
            $posts = $posts
                ->skip($args['offset'])
                ->take($args['per_page'])
                ->get();
        }

        return $posts;
    }

    /**
     * @param $menu_data
     * @return array
     */
    private static function convertMenuItems($menu_data): array
    {
        foreach($menu_data as $key => $data){
            if($data['type'] == 'post'){
                $post = Post::where('id',$data['id'])->with('post_type_item')->first();
                $url = get_the_url($post, $post->post_type_item);
            }
            if($data['type'] == 'category'){
                $category = Category::where('id',$data['id'])->with('post_type_item')->first();
                $url = get_the_url($category, $category->post_type_item);
            }
            if($data['type'] == 'custom'){
                $url = $data['url'];
            }
            $menu_data[$key] = $data;
            $menu_data[$key]['url'] = $url;
            if( array_key_exists('children', $data) ){
                $children = self::convertMenuItems($data['children']);
                $menu_data[$key]['children'] = $children;
            }
        }
        return $menu_data;
    }


}
