<?php

namespace JsPress\JsPress\Helpers;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use JsPress\JsPress\Interfaces\MediaInterface;
use JsPress\JsPress\Models\Media;

class Image{

    /**
     * @var MediaInterface
     */
    private static MediaInterface $media;

    private static string|File $file;

    /**
     * Create a new interface instance.
     * MediaInterface constructor
     *
     * @param MediaInterface $media
     */
    public function __construct(MediaInterface $media)
    {
        self::$media = $media;
    }

    public static function resize(string|UploadedFile $file, int $width, int $height)
    {
        self::$width = $width;
        self::$height = $height;
        if( $file instanceof UploadedFile){
            $fileType = self::getFileType($file);
            $fileName = self::getFileName($file);
            $extension = $file->getClientOriginalExtension();
            [$width, $height] = getimagesize($file->path());
            $media = self::$media->setFile($file)
                ->setUploadName($fileName['upload_name'])
                ->setFileName($fileName['file_name'])
                ->setExtension($extension)
                ->setMime($file->getClientMimeType())
                ->setType($fileType)
                ->setWidth($width)
                ->setHeight($height)
                ->setSize($file->getSize())
                ->setPath()
                ->setDirectory()
                ->move()
                ->crop()
                ->save();
        }
        return $media;
    }

    /**
     * @param $file
     * @return string
     */
    public static function getFileType($file): string
    {
        $mimeType = $file->getClientMimeType();
        if (str_starts_with($mimeType, 'image/')) {
            $fileType = 'image';
        } elseif (str_starts_with($mimeType, 'video/')) {
            $fileType = 'video';
        } elseif(str_starts_with($mimeType, 'application/')) {
            $fileType = 'application';
        }elseif(str_starts_with($mimeType, 'audio/')) {
            $fileType = 'audio';
        }elseif(str_starts_with($mimeType, 'text/')) {
            $fileType = 'text';
        }else{
            $fileType = 'file';
        }
        return $fileType;
    }

    /**
     * @param $file
     * @param $fileName
     * @param int $count
     * @return array
     */
    public static function getFileName($file, $fileName = null, int $count = 0): array
    {
        $extension = $file->getClientOriginalExtension();
        $uploadName = $file->getClientOriginalName();
        $originalFileName = str_replace('.'.$extension, '', $uploadName);
        $originalFileName = Str::slug($originalFileName, '-');
        if(is_null($fileName)){
            $fileName = str_replace('.'.$extension, '', $uploadName);
            $fileName = Str::slug($fileName, '-');
        }
        $checkFile = Media::where('file_name', $fileName)->where('extension', $extension)->first();
        if($checkFile){
            $count++;
            $fileName = $originalFileName.'-'.$count;
            return self::getFileName($file, $fileName, $count);
        }
        $upload_name = $fileName.'.'.$extension;
        return [
            'file_name' => $fileName,
            'upload_name' => $upload_name
        ];
    }
}
