<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: JsPress.php
 * Author: Json Yazılım
 * Class: JsPress.php
 * Current Username: Erdinc
 * Last Modified: 22.07.2023 14:31
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Helpers;

use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use JsPress\JsPress\Models\Post;
use JsPress\JsPress\Models\PostType;
use Spatie\Permission\Models\Role;

class JsPressPanel {

    /**
     *
     */
    const supportedLanguages = ['tr','en','de'];

    /**
     * @return mixed
     */
    public static function theme(): mixed
    {
        return auth()->guard('admin')->user()->theme_color();
    }

    /**
     * @return array[]
     */
    public static function getPanelSupportedLangauges(): array
    {
        return self::supportedLanguages;
    }

    /**
     * @return mixed
     */
    public static function panelLanguage(): mixed
    {
        return display_language(app()->getLocale())->display_language_name;
    }

    /**
     * @return mixed
     */
    public static function contentLanguageName(): mixed
    {
        $content_language = self::getAdminContentLanguage();
        $default_locale = default_language();
        return is_null($content_language) ?
            display_language($default_locale)->display_language_name
            :
            display_language($content_language)->display_language_name;
    }

    /**
     * @return mixed
     */
    public static function contentLocale(): mixed
    {
        $content_language = self::getAdminContentLanguage();
        $default_locale = default_language();
        return is_null($content_language) ? $default_locale : $content_language;
    }

    /**
     * @return mixed
     */
    public static function panelLocale(): mixed
    {
        $panel_language = auth()->guard('admin')->user()->panel_language;
        $default_locale = default_language();
        return is_null($panel_language) ? $default_locale : $panel_language;
    }

    /**
     * @return string|null
     */
    public static function getAdminContentLanguage(): ?string
    {
        return session('content_language');
    }

    /**
     * @param $locale
     * @return string|null
     */
    public static function setContentLanguage($locale): ?string
    {
        return \Session::put('content_language', $locale);
    }

    /**
     * @param bool $is_super
     * @return array
     */
    public static function getRoles(bool $is_super = false): array
    {
        $role_data = [];
        $roles = Role::when(!$is_super, function($query){
            return $query->where('name', '!=', 'Super-Admin');
        })
        ->get();
        foreach($roles as $key => $role){
            $role_data[$key]['key'] = $role->id;
            $role_data[$key]['value'] = $role->name;
        }
        return $role_data;
    }

    /**
     * @return Collection
     */
    private static function getPostTypes(): Collection
    {
        return PostType::where('post_types.is_menu', 1)
            ->where('post_types.post_type_id', 0)
            ->orderBy('post_types.order')
            ->get();
    }

    /**
     * @return array
     */
    public static function appViewShare(): array
    {
        return [
            'website_languages' => website_languages(),
            'post_panel_menu' => self::getPostTypes(),
            'current_locale' => app()->getLocale(),
            'content_locale' => self::contentLocale(),
            'panel_locale' => self::panelLocale(),
            'panel_language' => self::panelLanguage(),
            'content_language' => self::contentLanguageName(),
            'dir' => auth()->user()->direction(),
            'theme' => self::theme(),
            'admin' => [
                'avatar' => Auth()->guard('admin')->user()->avatar,
                'name' => Auth()->guard('admin')->user()->name,
                'email' => Auth()->guard('admin')->user()->email
            ]
        ];
    }

    /**
     * @return void
     */
    public static function clearRoute(): void
    {
        Artisan::call('route:clear');
    }

    /**
     * @param $post_type
     * @param $type
     * @return string
     */
    public static function postTypeLangKey($post_type, $type): string
    {

        $key = "";
        switch ($type){
            case 'list':
               $key = 'post_type_post_'.$post_type->key;
               break;
            case 'category':
                $key = 'post_type_category_'.$post_type->key;
                break;
            case 'breadcrumb':
                $key = 'post_type_breadcrumb_'.$post_type->key;
                break;
        }
        return $key;
    }

    /**
     * @param $post_type
     * @param $type
     * @return string
     */
    public static function postTypeLangKeyFromKey($post_type, $type): string
    {

        $key = "";
        switch ($type){
            case 'list':
                $key = 'post_type_post_'.$post_type;
                break;
            case 'category':
                $key = 'post_type_category_'.$post_type;
                break;
            case 'breadcrumb':
                $key = 'post_type_breadcrumb_'.$post_type;
                break;
        }
        return $key;
    }

    /**
     * @return array
     */
    public static function getPostTypeRoutes(): array
    {
        $route_data = [];
        if(Schema::hasTable('post_types')){
            $post_types = \DB::table('post_types')
                ->select([
                    'post_types.*'
                ])
                ->where('post_types.status', 1)
                ->orderBy('post_types.order')
                ->get();
            foreach($post_types as $post_type){
                $post_details = \DB::table('post_type_details')
                    ->where('post_type_id', $post_type->id)
                    ->where('status', 1)
                    ->where('locale', app()->getLocale())
                    ->get();
                foreach( $post_details->where('model', 'JsPress\JsPress\Models\Post') as $detail ){
                    if( $post_type->is_url == 1 && $post_type->post_type_id == 0 && $post_type->is_segment_disable == 0){
                        $route_data[] = [
                            'name' => get_the_route_name($post_type->key, 'post'),
                            'route' => $detail->slug.'/{slug}',
                            'order' => 1,
                            'type' => 'post'
                        ];
                    }
                }
                foreach( $post_details->where('model', 'JsPress\JsPress\Models\Category') as $detail ){
                    if( $post_type->is_category_url == 1){
                        $route_data[] = [
                            'name' => get_the_route_name($post_type->key, 'category'),
                            'route' => $detail->slug.'/{slug}',
                            'order' => 2,
                            'type' => 'category'
                        ];
                    }
                }
            }
        }
        collect($route_data)->sortBy('order')->toArray();
        return $route_data;
    }

    /**
     * @return array
     */
    public static function getTemplates(): array
    {
        $files = resource_path('views');
        $template_data = [];
        foreach(\File::files($files) as $file){
            $file_name = str_replace('.blade.php', '', $file->getFilename());
            $is_template = str_starts_with($file_name, 'template-');
            if($is_template){
                $file_name = str_replace('.blade.php', '', $file_name);
                $file_names = explode('-', $file_name);
                $file_names = explode('-', $file_name);
                $file_name_results = explode('_', $file_names[1]);
                $name_data = [];
                foreach($file_name_results as $result){
                    $name_data[] = ucfirst($result);
                }
                $template_data[] = [
                    'value' => $file_name,
                    'label' => implode(' ', $name_data)
                ];
            }
        }
        return $template_data;
    }

    /**
     * @return array
     */
    public static function getParentPages(): array
    {

        $posts = Post::where('status', 1)
            ->whereHas('post_type_item', function($query){
                return $query->where('status', 1);
            })
            ->whereHas('translation', function($query){
                return $query->where('locale', self::contentLocale())->where('region', 1);
            })
            ->where('post_type', 'page')
            ->orderBy('created_at', 'ASC')
            ->get();
        $post_data = [];
        foreach($posts as $post){
            $post_data[] = [
                'value' => $post->id,
                'label' => $post->title
            ];
        }
        return $post_data;
    }

}
