<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PostTypePermission.php
 * Author: Json Yazılım
 * Class: PostTypePermission.php
 * Current Username: Erdinc
 * Last Modified: 25.07.2023 14:57
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostTypePermission extends Model
{
    use HasFactory;

    protected $table = 'post_type_permissions';
    protected $primaryKey = 'id';
    protected $guarded  = [];
    public $timestamps = false;
}
