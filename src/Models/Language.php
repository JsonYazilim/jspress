<?php

namespace JsPress\JsPress\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    use HasFactory;

    protected $table = 'languages';
    protected $primaryKey = 'id';
    protected $guarded  = [];
    public $timestamps = false;

    public function display_language()
    {
        return $this->hasOne(Language::class, 'language_code', 'locale')->withPivot('language_code');
    }
}
