<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PostTypeDetail.php
 * Author: Json Yazılım
 * Class: PostTypeDetail.php
 * Current Username: Erdinc
 * Last Modified: 24.07.2023 16:37
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostTypeDetail extends Model
{
    use HasFactory;

    protected $table = 'post_type_details';
    protected $primaryKey = 'id';
    protected $guarded  = [];


    public function post_type()
    {
        return $this->belongsTo(PostType::class);
    }
}
