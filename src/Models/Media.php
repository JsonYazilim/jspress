<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: Media.php
 * Author: Json Yazılım
 * Class: Media.php
 * Current Username: Erdinc
 * Last Modified: 27.06.2023 12:33
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Media extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $table = 'media';
    /**
     * @var string
     */
    protected $primaryKey = 'id';
    /**
     * @var array
     */
    protected $guarded  = [];

    /**
     * @return HasMany
     */
    public function details(): HasMany
    {
        return $this->hasMany(MediaDetail::class);
    }

    /**
     * @param string|null $language
     * @return HasOne
     */
    public function detail(string|null $language = null): HasOne
    {
        return $this->hasOne(MediaDetail::class)->where('lang', !is_null($language) ? $language : app()->getLocale());
    }

}
