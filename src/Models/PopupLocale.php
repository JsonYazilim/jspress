<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PopupLocale.php
 * Author: Json Yazılım
 * Class: PopupLocale.php
 * Current Username: Erdinc
 * Last Modified: 9.05.2024 01:00
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PopupLocale extends Model
{
    use HasFactory;

    protected $table = 'popup_locales';

    protected $guarded  = [];
    public $timestamps = false;
}
