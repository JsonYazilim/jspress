<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: Category.php
 * Author: Json Yazılım
 * Class: Category.php
 * Current Username: Erdinc
 * Last Modified: 16.09.2023 19:18
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use JsPress\JsPress\Models\PostView;

class Category extends Model
{
    use HasFactory;

    protected $table = 'categories';
    protected $primaryKey = 'id';
    protected $guarded  = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function posts()
    {
        return $this->belongsToMany(Post::class);
    }

    /**
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(Category::class, 'category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    /**
     * @return HasMany
     */
    public function extras(): HasMany
    {
        return $this->hasMany(PostExtra::class, 'element_id')->where('type', 'category');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function post_type_item(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(PostType::class, 'key', 'category_type');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function translation(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(PostTranslation::class, 'element_id')->where('type', 'category');
    }

    /**
     * @return HasMany
     */
    public function views(): HasMany
    {
        return $this->hasMany(PostView::class, 'element_id')->where('type', 'category');
    }

    /**
     * @param $key
     * @return \Illuminate\Database\Eloquent\HigherOrderBuilderProxy|mixed|null
     */
    public function __get($key){
        $attribute = $this->getAttribute($key);

        if ($attribute !== null) {
            return $attribute;
        }

        if($key === 'url'){
            return get_the_url($this, $this->post_type_item);
        }

        if (strpos($key, 'ext_') !== false){
            $field_key = mb_substr($key, 4);
            $extra = null;
            if (isset($this->extras) && $this->extras instanceof Collection) {
                $extra = $this->extras->where('key', $field_key)->get();
            } else if (method_exists($this, 'extras')) {
                $extra = $this->extras()->where('key', $field_key)->get();
            }
            if(!is_null($extra) && count($extra) > 0){
                if($extra->first()->is_multiple == 1){
                    $extra_value = $extra->pluck('value')->toArray();
                }else{
                    $extra_value = is_json($extra->first()->value) ? json_decode($extra->first()->value) : $extra->first()->value;
                }
                return $extra_value;
            }
        }

        return null;

    }
}
