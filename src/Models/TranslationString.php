<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: TranslationString.php
 * Author: Json Yazılım
 * Class: TranslationString.php
 * Current Username: Erdinc
 * Last Modified: 28.05.2023 14:43
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TranslationString extends Model
{
    use HasFactory;

    protected $table = 'translation_strings';
    protected $primaryKey = 'id';
    protected $guarded  = [];

    public function translations(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Translation::class, 'translation_string_id ');
    }
}
