<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: Menu.php
 * Author: Json Yazılım
 * Class: Menu.php
 * Current Username: Erdinc
 * Last Modified: 6.10.2023 23:42
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use JsPress\JsPress\Models\PostTranslation;

class Menu extends Model
{
    use HasFactory;
    protected $table = 'menu';
    protected $primaryKey = 'id';
    protected $guarded  = [];

    protected $casts = [
        'data' => 'array'
    ];

    public function translation(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(PostTranslation::class, 'element_id')->where('type', 'menu');
    }

    public function other_translations(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(PostTranslation::class, 'element_id')->where('type', 'menu');
    }
}
