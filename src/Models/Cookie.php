<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: Cookie.php
 * Author: Json Yazılım
 * Class: Cookie.php
 * Current Username: Erdinc
 * Last Modified: 20.05.2024 22:17
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cookie extends Model
{
    use HasFactory;

    protected $table = 'cookie';
    protected $primaryKey = 'id';
    protected $guarded  = [];

    protected $casts = [
        'consentModal' => 'array',
        'preferencesModal' => 'array',
        'categories' => 'array',
        'translations' => 'array',
        'languages' => 'array'
    ];
}
