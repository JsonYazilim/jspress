<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PostExtra.php
 * Author: Json Yazılım
 * Class: PostExtra.php
 * Current Username: Erdinc
 * Last Modified: 27.09.2023 14:39
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostExtra extends Model
{
    use HasFactory;

    protected $table = 'post_extras';
    protected $primaryKey = 'id';
    protected $guarded  = [];
}
