<?php

namespace JsPress\JsPress\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use JsPressPanel;

class PostType extends Model
{
    use HasFactory;

    protected $table = 'post_types';
    protected $primaryKey = 'id';
    protected $guarded  = [];

    /**
     * @return HasMany
     */
    public function children(): HasMany
    {
        return $this->hasMany(PostType::class);
    }

    /**
     * @return HasMany
     */
    public function post_details(): HasMany
    {
        return $this->hasMany(PostTypeDetail::class)->where('model', 'JsPress\JsPress\Models\Post');
    }

    /**
     * @return object|null
     */
    public function post_detail(): object|null
    {
        return $this->hasOne(PostTypeDetail::class)->where('locale', JsPressPanel::panelLocale())->where('model', 'JsPress\JsPress\Models\Post');
    }

    /**
     * @return object|null
     */
    public function post_type_detail(): object|null
    {
        return $this->hasOne(PostTypeDetail::class)->where('locale', app()->getLocale())->where('model', 'JsPress\JsPress\Models\Post');
    }

    /**
     * @return HasOne
     */
    public function category_detail(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(PostTypeDetail::class)->where('locale', JsPressPanel::panelLocale())->where('model', 'JsPress\JsPress\Models\Category');
    }

    /**
     * @return HasOne
     */
    public function category_type_detail(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(PostTypeDetail::class)->where('locale', app()->getLocale())->where('model', 'JsPress\JsPress\Models\Category');
    }

    /**
     * @return HasMany
     */
    public function permissions(): HasMany
    {
        return $this->hasMany(PostTypePermission::class);
    }

    /**
     * @param string $locale
     * @return object|null
     */
    public function post_detail_locale(string $locale): object|null
    {
        return $this->hasOne(PostTypeDetail::class)->where('locale', $locale)->where('model', 'JsPress\JsPress\Models\Post')->first();
    }

    /**
     * @param string $locale
     * @return object|null
     */
    public function post_category_locale(string $locale): object|null
    {
        return $this->hasOne(PostTypeDetail::class)->where('locale', $locale)->where('model', 'JsPress\JsPress\Models\Category')->first();
    }


}
