<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: Popup.php
 * Author: Json Yazılım
 * Class: Popup.php
 * Current Username: Erdinc
 * Last Modified: 9.05.2024 01:00
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Popup extends Model
{
    use HasFactory;


    protected $table = 'popups';
    protected $primaryKey = 'id';
    protected $guarded  = [];
    protected $casts = [
        'desing' => 'array'
    ];

    public function getDesignAttribute($value)
    {
        return json_decode($value, TRUE);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function locales(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PopupLocale::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function devices(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PopupDevice::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pages(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PopupPage::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function urls(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(PopupUrl::class);
    }
}
