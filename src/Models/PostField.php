<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PostField.php
 * Author: Json Yazılım
 * Class: PostField.php
 * Current Username: Erdinc
 * Last Modified: 30.09.2023 12:51
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PostField extends Model
{
    use HasFactory;
    protected $table = 'post_fields';
    protected $primaryKey = 'id';
    protected $guarded  = [];

    protected $casts = [
        'conditions' => 'array',
        'data' => 'array'
    ];
}
