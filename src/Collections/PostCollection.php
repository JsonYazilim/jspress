<?php

namespace JsPress\JsPress\Collections;

class PostCollection{

    /**
     * @var array|null
     */
    public ?array $items = [];

    /**
     * @var
     */
    public $links;

    /**
     * @param array|null $items
     * @return void
     */
    public function setItems(?array $items): void
    {
        $this->items = is_null($items) ? [] : $items;
    }

    /**
     * @param $links
     * @return void
     */
    public function setLinks($links): void
    {
        $this->links = is_null($links) ? "" : $links;
    }

}
