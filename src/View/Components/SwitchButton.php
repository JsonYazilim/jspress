<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: SwitchButton.php
 * Author: Json Yazılım
 * Class: SwitchButton.php
 * Current Username: Erdinc
 * Last Modified: 1.10.2023 19:51
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class SwitchButton extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public ?string $id,
        public ?string $classes,
        public ?string $width,
        public ?string $label,
        public string $name,
        public ?string $selected,
        public ?string $message,
        public int $isopen,
        public bool $required
    )
    {
        //
    }

    public function isChecked(): bool
    {
        return $this->selected == 1;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('JsPress::components.elements.switch-button');
    }
}
