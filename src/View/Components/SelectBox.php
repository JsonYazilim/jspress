<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: SelectBox.php
 * Author: Json Yazılım
 * Class: SelectBox.php
 * Current Username: Erdinc
 * Last Modified: 1.10.2023 19:17
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class SelectBox extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public ?string $id,
        public ?string $classes,
        public ?string $width,
        public ?string $label,
        public string $name,
        public null|string|array $selected,
        public array $options,
        public bool $required,
        public int $multiple
    )
    {
        //
    }

    /**
     * Determine if the given option is the currently selected option.
     */
    public function isSelected(int|string|array $option): bool
    {
        if(is_array($this->selected)){
            return in_array($option, $this->selected);
        }
        return $option == $this->selected;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('JsPress::components.elements.select-box');
    }
}
