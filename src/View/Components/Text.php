<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: Text.php
 * Author: Json Yazılım
 * Class: Text.php
 * Current Username: Erdinc
 * Last Modified: 1.10.2023 16:27
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Text extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public ?string $width,
        public ?string $label,
        public string $name,
        public string $type,
        public ?string $placeholder,
        public bool $required,
        public ?string $classes,
        public ?string $id,
        public ?string $value
    )
    {

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('JsPress::components.elements.text');
    }
}
