<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: Editor.php
 * Author: Json Yazılım
 * Class: Editor.php
 * Current Username: Erdinc
 * Last Modified: 8.10.2023 15:29
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Editor extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public ?string $width,
        public ?string $label,
        public string $name,
        public bool $required,
        public ?string $classes,
        public ?string $id,
        public ?string $value
    )
    {

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('JsPress::components.elements.editor');
    }
}
