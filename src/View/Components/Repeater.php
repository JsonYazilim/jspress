<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: Repeater.php
 * Author: Json Yazılım
 * Class: Repeater.php
 * Current Username: Erdinc
 * Last Modified: 24.07.2023 19:07
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Repeater extends Component
{
    /**
     * Create the component instance.
     */
    public function __construct(
        public string $name,
        public array $elements,
        public null|array $values,
        public string $id
    ) {}

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('JsPress::components.elements.repeater');
    }
}
