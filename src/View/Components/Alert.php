<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: alert.php
 * Author: Json Yazılım
 * Class: alert.php
 * Current Username: Erdinc
 * Last Modified: 16.09.2023 19:38
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Alert extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public string $type,
        public string $title,
        public string $message
    )
    {

    }

    public function alertClass(): string
    {
        return [
            'error' => 'bg-light-danger',
            'success' => 'bg-light-success',
            'warning' => 'bg-light-warning',
            'info' => 'bg-light-info'
        ][$this->type];
    }

    public function iconClass(): string
    {
        return [
            'error' => 'svg-icon-danger',
            'success' => 'svg-icon-success',
            'warning' => 'svg-icon-warning',
            'info' => 'svg-icon-info'
        ][$this->type];
    }

    public function textClass(): string
    {
        return [
            'error' => 'text-danger',
            'success' => 'text-success',
            'warning' => 'text-warning',
            'info' => 'text-info'
        ][$this->type];
    }


    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('JsPress::components.alert');
    }
}
