<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: Select.php
 * Author: Json Yazılım
 * Class: Select.php
 * Current Username: Erdinc
 * Last Modified: 24.07.2023 20:16
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Select extends Component
{
    /**
     * Create the component instance.
     */
    public function __construct(
        public string $width,
        public string $label,
        public string $name,
        public string $selected,
        public array $options,
        public bool $required,
        public bool $repeater
    ) {}

    /**
     * Determine if the given option is the currently selected option.
     */
    public function isSelected(int|string $option): bool
    {
        return $option == $this->selected;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('JsPress::components.elements.select');
    }
}
