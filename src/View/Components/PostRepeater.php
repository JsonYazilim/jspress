<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PostRepeater.php
 * Author: Json Yazılım
 * Class: PostRepeater.php
 * Current Username: Erdinc
 * Last Modified: 1.10.2023 16:53
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class PostRepeater extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public string $name,
        public array $elements,
        public ?array $values,
        public string $id
    )
    {

    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('JsPress::components.elements.post-repeater');
    }
}
