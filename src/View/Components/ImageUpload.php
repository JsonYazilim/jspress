<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: ImageUpload.php
 * Author: Json Yazılım
 * Class: ImageUpload.php
 * Current Username: Erdinc
 * Last Modified: 12.09.2023 15:06
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class ImageUpload extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public string $name,
        public ?int $image,
        public ?string $message
    )
    {}

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('JsPress::components.elements.image-upload');
    }
}
