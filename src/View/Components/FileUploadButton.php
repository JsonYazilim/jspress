<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: SingleFileUploadButton.php
 * Author: Json Yazılım
 * Class: SingleFileUploadButton.php
 * Current Username: Erdinc
 * Last Modified: 10.09.2023 16:53
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class FileUploadButton extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public string $id,
        public string $type,
        public int $maxfile,
        public string $action,
        public string $name,
        public ?int $image,
        public ?int $width = null,
        public int $required = 0
    )
    {}

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('JsPress::components.elements.file-upload-button');
    }
}
