<?php

namespace JsPress\JsPress\Services;

use Illuminate\Contracts\Routing\UrlRoutable;
use Illuminate\Support\Env;
use Illuminate\Support\Str;
use JsPress\JsPress\Exceptions\SupportedLocalesNotDefined;
use JsPress\JsPress\Exceptions\UnsupportedLocaleException;
use \JsPress\JsPress\Services\LanguageNegotiator;

class JsPressLocalization{

    const ENV_ROUTE_KEY = 'ROUTING_LOCALE';

    /**
     * Illuminate request class.
     *
     * @var Illuminate\Foundation\Application
     */
    protected mixed $app;

    /**
     * Illuminate router class.
     *
     * @var \Illuminate\Routing\Router
     */
    protected \Illuminate\Routing\Router $router;

    /**
     * Config repository.
     *
     * @var \Illuminate\Config\Repository
     */
    protected mixed $configRepository;

    /**
     * Illuminate request class.
     *
     * @var \Illuminate\Routing\Request
     */
    protected mixed $request;

    protected array $supportedLocales;

    protected bool|string $current_locale = false;

    protected ?string $default_locale;

    /**
     * Illuminate request class.
     *
     * @var string
     */
    protected string $baseUrl;

    /**
     * Name of the translation key of the current route, it is used for url translations.
     *
     * @var string
     */
    protected string $routeName;

    /**
     * Locales mapping.
     *
     * @var array
     */
    protected array $localesMapping;

    public function __construct()
    {
        $this->app = app();

        $this->configRepository = $this->app['config'];
        $this->router = $this->app['router'];
        $this->request = $this->app['request'];
        $this->default_locale = default_language();
        $supportedLocales = $this->getSupportedLocales();
    }

    public function setLocale($locale = null): ?string
    {
        if (empty($locale) || !\is_string($locale)) {

            $locale = $this->request->segment(1);

            if ( ! $locale) {
                $locale = $this->getForcedLocale();
            }
        }

        $locale = $this->getInversedLocaleFromMapping($locale);

        if (!empty($this->supportedLocales[$locale])) {
            $this->current_locale = $locale;
        }else{
            $locale = null;
            if ($this->hideDefaultLocaleInURL()) {
                $this->current_locale = $this->default_locale;
            }else{
                $this->current_locale = $this->getCurrentLocale();
            }
        }
        $this->app->setLocale($this->current_locale);

        return $this->getLocaleFromMapping($locale);
    }

    /**
     * Returns the forced environment set route locale.
     *
     * @return string|null
     */
    protected function getForcedLocale(): ?string
    {
        if (version_compare($this->app->version(), '6') >= 0) {
            return Env::get(static::ENV_ROUTE_KEY, function () {
                $value = getenv(static::ENV_ROUTE_KEY);

                if ($value !== false) {
                    return $value;
                }
            });
        } else {
            return env(static::ENV_ROUTE_KEY, function () {
                $value = getenv(static::ENV_ROUTE_KEY);

                if ($value !== false) {
                    return $value;
                }
            });
        }
    }

    public function getSupportedLocales()
    {
        $locales = json_decode(json_encode(supported_locales()), TRUE);
        $this->supportedLocales = $locales;
        return $locales;
    }

    /**
     * Returns the translation key for a given path.
     *
     * @return bool Returns value of hideDefaultLocaleInURL in config.
     */
    public function hideDefaultLocaleInURL(): bool
    {
        return $this->configRepository->get('jspress.language.hide_url_default_language') ?? true;
    }

    /**
     * Returns a locale from the mapping.
     *
     * @param string|null $locale
     *
     * @return string|null
     */
    public function getLocaleFromMapping(?string $locale): ?string
    {
        return $this->getLocalesMapping()[$locale] ?? $locale;
    }

    /**
     * Return locales mapping.
     *
     * @return array
     */
    public function getLocalesMapping(): array
    {
        if (empty($this->localesMapping)) {
            $this->localesMapping = [];
        }

        return $this->localesMapping;
    }

    /**
     * Check if Locale exists on the supported locales array.
     *
     * @param bool|string $locale string|bool Locale to be checked
     *
     * @return bool is the locale supported?
     */
    public function checkLocaleInSupportedLocales(bool|string $locale): bool
    {
        $inversedLocale = $this->getInversedLocaleFromMapping($locale);
        $locales = $this->getSupportedLocales();
        if ($locale !== false && empty($locales[$locale]) && empty($locales[$inversedLocale])) {
            return false;
        }

        return true;
    }

    /**
     * Returns inversed locale from the mapping.
     *
     * @param string|null $locale
     *
     * @return string|null
     */
    public function getInversedLocaleFromMapping($locale): ?string
    {
        return \array_flip($this->getLocalesMapping())[$locale] ?? $locale;
    }

    /**
     * Check if $locale is default locale and supposed to be hidden in url
     *
     * @param string $locale Locale to be checked
     *
     * @return boolean Returns true if above requirement are met, otherwise false
     */

    public function isHiddenDefault($locale): bool
    {
        return  ($this->getDefaultLocale() === $locale && $this->hideDefaultLocaleInURL());
    }

    /**
     * Returns default locale.
     *
     * @return string
     */
    public function getDefaultLocale(): string
    {
        return $this->default_locale;
    }

    /**
     * It returns a URL without locale (if it has it)
     * Convenience function wrapping getLocalizedURL(false).
     *
     * @param bool|string|null $url URL to clean, if false, current url would be taken
     * @return string URL with no locale in path
     * @throws UnsupportedLocaleException
     */
    public function getNonLocalizedURL(bool|string $url = null): string
    {
        try {
            return $this->getLocalizedURL(false, $url);
        } catch (SupportedLocalesNotDefined $e) {
            return "";
        }
    }

    /**
     * Returns a URL adapted to $locale.
     *
     *
     * @param bool|string|null $locale     Locale to adapt, false to remove locale
     * @param bool|string|null $url        URL to adapt in the current language. If not passed, the current url would be taken.
     * @param array $attributes Attributes to add to the route, if empty, the system would try to extract them from the url.
     * @param bool $forceDefaultLocation Force to show default location even hideDefaultLocaleInURL set as TRUE
     *
     * @return string|false URL translated, False if url does not exist
     *@throws UnsupportedLocaleException
     *
     * @throws SupportedLocalesNotDefined
     */
    public function getLocalizedURL(bool|string $locale = null, bool|string $url = null, array $attributes = [], bool $forceDefaultLocation = false): bool|string
    {
        if ($locale === null) {
            $locale = $this->getCurrentLocale();
        }

        if (!$this->checkLocaleInSupportedLocales($locale)) {
            throw new UnsupportedLocaleException('Locale \''.$locale.'\' is not in the list of supported locales.');
        }

        if (empty($attributes)) {
            $attributes = $this->extractAttributes($url, $locale);
        }

        if (empty($url)) {
            $url = $this->request->fullUrl();
            $urlQuery = parse_url($url, PHP_URL_QUERY);
            $urlQuery = $urlQuery ? '?'.$urlQuery : '';
        } else {
            $urlQuery = parse_url($url, PHP_URL_QUERY);
            $urlQuery = $urlQuery ? '?'.$urlQuery : '';

            $url = $this->url->to($url);
        }

        $url = preg_replace('/'. preg_quote($urlQuery, '/') . '$/', '', $url);

        $base_path = $this->request->getBaseUrl();
        $parsed_url = parse_url($url);
        $url_locale = $this->getDefaultLocale();

        if (!$parsed_url || empty($parsed_url['path'])) {
            $path = $parsed_url['path'] = '';
        } else {
            $parsed_url['path'] = str_replace($base_path, '', '/'.ltrim($parsed_url['path'], '/'));
            $path = $parsed_url['path'];
            foreach ($this->getSupportedLocales() as $localeCode => $lang) {
                $localeCode = $this->getLocaleFromMapping($localeCode);

                $parsed_url['path'] = preg_replace('%^/?'.$localeCode.'/%', '$1', $parsed_url['path']);
                if ($parsed_url['path'] !== $path) {
                    $url_locale = $localeCode;
                    break;
                }

                $parsed_url['path'] = preg_replace('%^/?'.$localeCode.'$%', '$1', $parsed_url['path']);
                if ($parsed_url['path'] !== $path) {
                    $url_locale = $localeCode;
                    break;
                }
            }
        }

        $parsed_url['path'] = ltrim($parsed_url['path'], '/');

        $locale = $this->getLocaleFromMapping($locale);

        if (!empty($locale)) {
            if ($forceDefaultLocation || $locale != $this->getDefaultLocale() || !$this->hideDefaultLocaleInURL()) {
                $parsed_url['path'] = $locale.'/'.ltrim($parsed_url['path'], '/');
            }
        }
        $parsed_url['path'] = ltrim(ltrim($base_path, '/').'/'.$parsed_url['path'], '/');

        if (Str::startsWith($path, '/') === true) {
            $parsed_url['path'] = '/'.$parsed_url['path'];
        }
        $parsed_url['path'] = rtrim($parsed_url['path'], '/');

        $url = $this->unparseUrl($parsed_url);

        if ($this->checkUrl($url)) {
            return $url.$urlQuery;
        }

        return $this->createUrlFromUri($url).$urlQuery;
    }

    /**
     * Extract attributes for current url.
     *
     * @param bool|string|null $url    to extract attributes, if not present, the system will look for attributes in the current call
     * @param string $locale
     *
     * @return array Array with attributes
     */
    protected function extractAttributes(bool|string|null $url = false, string $locale = ''): array
    {
        if (!empty($url)) {
            $attributes = [];
            $parse = parse_url($url);
            if (isset($parse['path'])) {
                $parse['path'] = trim(str_replace('/'.$this->current_locale.'/', '', $parse['path']), "/");
                $url = explode('/', trim($parse['path'], '/'));
            } else {
                $url = [];
            }

            foreach ($this->router->getRoutes() as $route) {
                $attributes = [];
                $path = method_exists($route, 'uri') ? $route->uri() : $route->getUri();

                if (!preg_match("/{[\w]+\??}/", $path)) {
                    continue;
                }

                $path = explode('/', $path);
                $i = 0;

                // The system's route can't be smaller
                // only the $url can be missing segments (optional parameters)
                // We can assume it's the wrong route
                if (count($path) < count($url)) {
                    continue;
                }

                $match = true;
                foreach ($path as $j => $segment) {
                    if (isset($url[$i])) {
                        if ($segment === $url[$i]) {
                            $i++;
                            continue;
                        } elseif (preg_match("/{[\w]+}/", $segment)) {
                            // must-have parameters
                            $attribute_name = preg_replace(['/}/', '/{/', "/\?/"], '', $segment);
                            $attributes[$attribute_name] = $url[$i];
                            $i++;
                            continue;
                        } elseif (preg_match("/{[\w]+\?}/", $segment)) {
                            // optional parameters
                            if (!isset($path[$j + 1]) || $path[$j + 1] !== $url[$i]) {
                                // optional parameter taken
                                $attribute_name = preg_replace(['/}/', '/{/', "/\?/"], '', $segment);
                                $attributes[$attribute_name] = $url[$i];
                                $i++;
                                continue;
                            } else {
                                $match = false;
                                break;
                            }
                        } else {
                            // As soon as one segment doesn't match, then we have the wrong route
                            $match = false;
                            break;
                        }
                    } elseif (preg_match("/{[\w]+\?}/", $segment)) {
                        $attribute_name = preg_replace(['/}/', '/{/', "/\?/"], '', $segment);
                        $attributes[$attribute_name] = null;
                        $i++;
                    } else {
                        // no optional parameters but no more $url given
                        // this route does not match the url
                        $match = false;
                        break;
                    }
                }

                if (isset($url[$i + 1])) {
                    $match = false;
                }

                if ($match) {
                    return $attributes;
                }
            }
        } else {
            if (!$this->router->current()) {
                return [];
            }

            $attributes = $this->normalizeAttributes($this->router->current()->parameters());
            $response = event('routes.translation', [$locale, $attributes]);

            if (!empty($response)) {
                $response = array_shift($response);
            }

            if (\is_array($response)) {
                $attributes = array_merge($attributes, $response);
            }
        }

        return $attributes;
    }

    /**
     * Build URL using array data from parse_url.
     *
     * @param array|false $parsed_url Array of data from parse_url function
     *
     * @return string Returns URL as string.
     */
    protected function unparseUrl($parsed_url): string
    {
        if (empty($parsed_url)) {
            return '';
        }

        $url = '';
        $url .= isset($parsed_url['scheme']) ? $parsed_url['scheme'].'://' : '';
        $url .= $parsed_url['host'] ?? '';
        $url .= isset($parsed_url['port']) ? ':'.$parsed_url['port'] : '';
        $user = $parsed_url['user'] ?? '';
        $pass = isset($parsed_url['pass']) ? ':'.$parsed_url['pass'] : '';
        $url .= $user.(($user || $pass) ? "$pass@" : '');

        if (!empty($url)) {
            $url .= isset($parsed_url['path']) ? '/'.ltrim($parsed_url['path'], '/') : '';
        } else {
            $url .= $parsed_url['path'] ?? '';
        }

        $url .= isset($parsed_url['query']) ? '?'.$parsed_url['query'] : '';
        $url .= isset($parsed_url['fragment']) ? '#'.$parsed_url['fragment'] : '';

        return $url;
    }

    /**
     * Returns true if the string given is a valid url.
     *
     * @param string $url String to check if it is a valid url
     *
     * @return bool Is the string given a valid url?
     */
    protected function checkUrl($url): bool
    {
        return filter_var($url, FILTER_VALIDATE_URL);
    }

    /**
     * Create an url from the uri.
     *
     * @param string $uri Uri
     *
     * @return string Url for the given uri
     */
    public function createUrlFromUri($uri): string
    {
        $uri = ltrim($uri, '/');

        if (empty($this->baseUrl)) {
            return app('url')->to($uri);
        }

        return $this->baseUrl.$uri;
    }

    /**
     * Sets the base url for the site.
     *
     * @param string $url Base url for the site
     */
    public function setBaseUrl($url): void
    {
        if (substr($url, -1) != '/') {
            $url .= '/';
        }

        $this->baseUrl = $url;
    }

    /**
     * Normalize attributes gotten from request parameters.
     *
     * @param      array  $attributes  The attributes
     * @return     array  The normalized attributes
     */
    protected function normalizeAttributes($attributes): array
    {
        if (array_key_exists('data', $attributes) && \is_array($attributes['data']) && ! \count($attributes['data'])) {
            $attributes['data'] = null;
            return $attributes;
        }
        return $attributes;
    }

    /**
     * Returns current language.
     *
     * @return string current language
     */
    public function getCurrentLocale(): string
    {
        if ($this->current_locale) {
            return $this->current_locale;
        }

        if ($this->useAcceptLanguageHeader() && !$this->app->runningInConsole()) {
            $negotiator = new LanguageNegotiator($this->default_locale, $this->getSupportedLocales(), $this->request);

            return $negotiator->negotiateLanguage();
        }

        return $this->configRepository->get('app.locale');
    }

    /**
     * Returns the translation key for a given path.
     *
     * @param string $path Path to get the key translated
     *
     * @return string|false Key for translation, false if not exist
     */
    public function getRouteNameFromAPath(string $path): bool|string
    {
        $attributes = $this->extractAttributes($path);

        $path = parse_url($path)['path'];
        $path = trim(str_replace('/'.$this->current_locale.'/', '', $path), "/");

        return false;
    }

    /**
     * Change route attributes for the ones in the $attributes array.
     *
     * @param $attributes array Array of attributes
     * @param string $route string route to substitute
     *
     * @return string route with attributes changed
     */
    protected function substituteAttributesInRoute($attributes, $route, $locale = null): string
    {
        foreach ($attributes as $key => $value) {
            if ($value instanceOf Interfaces\LocalizedUrlRoutable) {
                $value = $value->getLocalizedRouteKey($locale);
            }
            elseif ($value instanceOf UrlRoutable) {
                $value = $value->getRouteKey();
            }
            $route = str_replace(array('{'.$key.'}', '{'.$key.'?}'), $value, $route);
        }

        // delete empty optional arguments that are not in the $attributes array
        $route = preg_replace('/\/{[^)]+\?}/', '', $route);

        return $route;
    }

    /**
     * Set current route name.
     *
     * @param string $routeName current route name
     */
    public function setRouteName(string $routeName): void
    {
        $this->routeName = $routeName;
    }

    /**
     * Returns the translation key for a given path.
     *
     * @return bool Returns value of useAcceptLanguageHeader in config.
     */
    protected function useAcceptLanguageHeader(): bool
    {
        return $this->configRepository->get('jspress.language.enable_browser_language_redirect');
    }

}
