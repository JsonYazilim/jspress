<?php

namespace JsPress\JsPress\Services;

use Illuminate\Support\Facades\Cache;
use JsPress\JsPress\Models\Translation;
class TranslationService
{
    private bool $has_locale;
    private bool $browser_redirect;
    private bool $hide_url_default;

    public function __construct(){
        $this->has_locale = \Session::has('locale');
        $this->browser_redirect = config('jspress.language.enable_browser_language_redirect');
        $this->hide_url_default = config('jspress.language.hide_url_default_language');
    }

    /**
     * @param $locale
     * @param $group
     * @param $key
     * @param $default_value
     * @param $params
     * @return array|mixed|string|string[]
     */
    public function get($locale, $group, $key, $default_value, $params): mixed
    {
        $cacheKey = "translation.$locale.$group.$key";
        $value = Cache::get($cacheKey);
        if ($value === null) {
            $translation_string = $this->getDefaultString($key, $group);
            if(!$translation_string){
                $this->setDefaultString($key, $group, $default_value);
                Cache::forever($cacheKey, $default_value, 86400);
            }else{
                $translation = Translation::where('locale', $locale)
                    ->where('translation_string_id', $translation_string->id)
                    ->first();
                $value = $translation ? $translation->value : $translation_string->value;
                Cache::forever($cacheKey, $value, 86400);
            }
            $value = Cache::get($cacheKey);
        }

        return $this->replaceParameters($value, $params);
    }

    /**
     * @param $locale
     * @param $group
     * @param $key
     * @param $value
     * @return void
     */
    public function set($locale, $group, $key, $value): void
    {
        Translation::updateOrCreate(
            ['locale' => $locale, 'group' => $group, 'key' => $key],
            ['value' => $value]
        );

        Cache::forget("translation.$locale.$group.$key");
    }

    /**
     * @param $key
     * @param $group
     * @return mixed
     */
    private function getDefaultString($key, $group): mixed
    {
        return \DB::table('translation_strings')->where('key', $key)->where('group', $group)->first();
    }

    /**
     * @param $key
     * @param $group
     * @param $value
     * @return void
     */
    private function setDefaultString($key, $group, $value): void
    {
        \DB::table('translation_strings')
            ->insert([
                'group' => $group,
                'key' => $key,
                'value' => $value,
                'created_at' => \Carbon\Carbon::now()->format('Y-m-d H:i'),
                'updated_at' => \Carbon\Carbon::now()->format('Y-m-d H:i')
            ]);
    }

    /**
     * @param $text
     * @param $params
     * @return string
     */
    private function replaceParameters($text, $params): string
    {
        foreach ($params as $key => $value) {
            $text = str_replace(":{$key}", $value, $text);
        }
        return $text;
    }

    public function hasLocale(): bool
    {
        return $this->has_locale;
    }

    /**
     * @return bool
     */
    public function browserRedirect(): bool
    {
        return $this->browser_redirect;
    }

    /**
     * @return bool
     */
    public function hideUrlDefault(): bool
    {
        return $this->hide_url_default;
    }

    /**
     * @param $locale
     * @param $browser_language
     * @param $defaultLocale
     * @return bool
     */
    public function is_first_request($locale, $browser_language, $defaultLocale): bool
    {
        return !$this->has_locale && is_null($locale) && $defaultLocale != $browser_language;
    }

}
