<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: Translation.php
 * Author: Json Yazılım
 * Class: Translation.php
 * Current Username: Erdinc
 * Last Modified: 29.05.2023 12:10
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

class Translation implements FromCollection
{

    protected string $locale;

    /**
     * @param string $locale
     */
    public function __construct(string $locale)
    {
        $this->locale = $locale;
    }


    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection(): \Illuminate\Support\Collection
    {
        $translations = \DB::table('translation_strings')
            ->select([
                'translation_strings.group as Group',
                'translation_strings.key as Key',
                'translation_strings.value as DefaultTranslation',
                'translations.value as Translation'
            ])
            ->leftJoin('translations', function($join){
                $join->on('translations.translation_string_id', '=', 'translation_strings.id')
                    ->where('translations.locale', $this->locale);
            })
            ->orderBy('translation_strings.group')
            ->get();
        $headers = collect([["Group", "Key", "Locale", "Translation"]]);
        $translation_data = [];
        foreach($translations as $translation){
            $translation_data[] = [$translation->Group, $translation->Key, $this->locale, $translation->DefaultTranslation, $translation->Translation];
        }
        $translation_collection = collect($translation_data);
        return $headers->merge($translation_collection);
    }
}
