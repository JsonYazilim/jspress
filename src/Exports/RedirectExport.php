<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: RedirectExport.php
 * Author: Json Yazılım
 * Class: RedirectExport.php
 * Current Username: Erdinc
 * Last Modified: 1.03.2024 21:24
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;

class RedirectExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection(): \Illuminate\Support\Collection
    {
        $redirects = \DB::table('redirects')
            ->orderBy('id')
            ->get();
        $headers = collect([["old_url", "new_url", "status_code"]]);
        $redirect_data = [];
        foreach($redirects as $redirect){
            $redirect_data[] = [$redirect->old_url, $redirect->new_url, $redirect->status_code];
        }
        $redirect_collection = collect($redirect_data);
        return $headers->merge($redirect_collection);
    }
}
