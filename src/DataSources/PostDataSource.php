<?php

namespace JsPress\JsPress\DataSources;

use JsPress\JsPress\Helpers\General;
use JsPress\JsPress\Models\Admin;
use JsPress\JsPress\Models\Language;
use JsPress\JsPress\Models\Popup;
use JsPress\JsPress\Models\Post;
use JsPress\JsPress\Models\PostTranslation;

class PostDataSource {

    /**
     * @var int|mixed
     */
    public int $id;
    /**
     * @var int|mixed
     */
    public int $post_id;
    /**
     * @var string|mixed
     */
    public string $title;
    /**
     * @var string|mixed|null
     */
    public ?string $content;
    /**
     * @var string|mixed|null
     */
    public ?string $slug;
    /**
     * @var string|null
     */
    public ?string $post_type_slug;
    /**
     * @var array|null
     */
    public ?array $author;
    /**
     * @var string|mixed
     */
    public string $publish_date;
    /**
     * @var string|mixed
     */
    public string $publish_date_gmt;
    /**
     * @var int|mixed
     */
    public int $status;
    /**
     * @var int|mixed
     */
    public int $order;
    /**
     * @var string|null
     */
    public ?string $view;
    /**
     * @var string|null
     */
    public ?string $url;
    /**
     * @var array|null
     */
    public ?array $extras;
    /**
     * @var array|null
     */
    public ?array $seo;
    /**
     * @var array|null
     */
    public ?array $parent = null;
    /**
     * @var array|null
     */
    public ?array $children = null;
    /**
     * @var array|null
     */
    public ?array $other_languages;
    /**
     * @var array|null
     */
    public ?array $categories;
    /**
     * @var string|null
     */
    public ?string $type;
    /**
     * @var array|mixed
     */
    public array $post_type;
    /**
     * @var array|mixed|null
     */
    public ?array $current_translation;
    /**
     * @var array|null
     */
    public ?array $current_language;
    /**
     * @var array|null
     */
    public ?array $breadcrumbs;

    /**
     * @var array
     */
    public array $popups = [];

    /**
     * @param Post $post
     * @param bool $is_collect
     * @param bool $include_category
     */
    public function __construct(Post $post, bool $is_collect = true, bool $include_category = false){

        $post_array = json_decode(json_encode($post), TRUE);
        $post_type = $post->post_type_item;
        $this->type = 'post';
        $this->id = $post->id;
        $this->post_id = $post->post_id;
        $this->title = $post->title;
        $this->content = $post->content;
        $this->slug = $post->slug;
        $this->post_type_slug = $post->post_type;
        $this->author = $this->setAuthor($post->author);
        $this->publish_date = $post->publish_date;
        $this->publish_date_gmt = $post->publish_date_gmt;
        $this->status = $post->status;
        $this->order = $post->order;
        $this->extras = [];
        if(isset($post_array['extras'])){
            $this->extras = $this->setExtras($post_array['extras']);
        }

        $this->post_type = json_decode(json_encode($post_type), TRUE);
        if($include_category){
            $this->categories = $this->setCategories($post->categories);
        }
        if($is_collect){
            if(!$include_category){
                $this->categories = $this->setCategories($post->categories);
            }
            $this->seo = $this->setSeoMeta($post->seo_meta);
            $this->breadcrumbs = $this->setBreadCrumbs($post, $post);
            $this->view = $this->setView($post->view);
            $this->current_translation = json_decode(json_encode($post->translation), TRUE);
            $this->other_languages = $this->setPostTranslations();
            $this->current_language = $this->getCurrentLanguage();
            $this->popups = $this->getPopups($post);
        }
        $url = get_the_url($post, $post_type);
        $this->url = $url;

        return $this;
    }

    /**
     * @param $categories
     * @return array|null
     */
    private function setCategories($categories):? array
    {
        $category_data = [];
        foreach($categories as $category){
            $category_data[] = new CategoryDataSource($category, false);
        }
        return $category_data;
    }

    /**
     * @param $post
     * @param $first_post
     * @param array $breadcrumb_data
     * @return array|null
     */
    private function setBreadCrumbs($post, $first_post, array &$breadcrumb_data = []):? array
    {

        if($post->post_id != 0){
            $post = get_the_post($post->post_id);
            $breadcrumb_data[] = $post->id;
            $this->setBreadCrumbs($post, $first_post, $breadcrumb_data);
        }
        $post_ids = array_reverse($breadcrumb_data);
        $homepage = get_the_homepage();
        if(!is_null($homepage)){
            $result_data[] = [
                'title' => $homepage->title,
                'url' => get_base_url()
            ];
        }
        foreach($post_ids as $post_id){
            $post = Post::find($post_id);
            $result_data[] = [
                'title' => $post->title,
                'url' => get_the_url($post)
            ];
        }
        $result_data[] = [
            'title' => $first_post->title,
            'url' => get_the_url($first_post)
        ];
        return $result_data;
    }

    /**
     * @param $author_id
     * @return array|null
     */
    private function setAuthor($author_id):? array
    {
        if( !is_null($author_id) ){
            return json_decode(json_encode(Admin::find($author_id)), TRUE);
        }
        return null;
    }

    /**
     * @param $extras
     * @return array
     */
    private function setExtras($extras): array
    {
        $extra_data = [];
        foreach(collect($extras)->groupBy('key') as $key => $extra){
            if($extra->first()['is_multiple'] == 1){
                $extra_data[$key] = $extra->map(function($item){
                    return is_numeric($item['value']) ? intval($item['value']) : $item['value'];
                })
                    ->toArray();
            }else{
                $extra_data[$key] = is_json($extra->first()['value']) ? json_decode($extra->first()['value'], TRUE) : $extra->first()['value'];
            }

        }
        return $extra_data;
    }

    /**
     * @param $metas
     * @return array|null
     */
    private function setSeoMeta($metas):? array
    {
        $seo_data = [];
        if(!$metas->isNotEmpty()){
            $seo_data['title'] =  $this->title;
            $seo_data['description'] = substr(strip_tags($this->content), 0,160);
        }
        foreach($metas as $meta){
            if( is_null($meta['prefix'])){
                if( $meta['key'] == 'title' ){
                    $seo_data['title'] = empty($meta['content']) ? $this->title : $meta['content'];
                }
                if( $meta['key'] == 'description' ){
                    $seo_data['description'] = empty($meta['content']) ? substr(strip_tags($this->content), 0,160) : $meta['content'];
                }
            }elseif( $meta['prefix'] == 'og' ){
                if( $meta['key'] == 'title' ){
                    $seo_data['facebook']['title'] = empty($meta['content']) ? $this->title : $meta['content'];
                }
                if( $meta['key'] == 'description' ){
                    $seo_data['facebook']['description'] = empty($meta['content']) ? substr(strip_tags($this->content), 0,160) : $meta['content'];
                }
                if( $meta['key'] == 'image' ){
                    $seo_data['facebook']['image'] = $meta['content'];
                }
            }elseif( $meta['prefix'] == 'twitter' ){
                if( $meta['key'] == 'title' ){
                    $seo_data['twitter']['title'] = empty($meta['content']) ? $this->title : $meta['content'];
                }
                if( $meta['key'] == 'description' ){
                    $seo_data['twitter']['description'] = empty($meta['content']) ? substr(strip_tags($this->content), 0,160) : $meta['content'];
                }
                if( $meta['key'] == 'image' ){
                    $seo_data['twitter']['image'] = $meta['content'];
                }
            }
        }
        $seo_data['canonical'] = request()->fullUrl();
        return $seo_data;
    }

    /**
     * @param string|null $view
     * @return string
     */
    private function setView(string|null $view): string
    {
        if( is_null($view) ){
            if($this->type == 'post'){
                if( \View::exists('post-'.$this->post_type['key']) && $this->post_type['key'] != 'page'){
                    return 'post-'.$this->post_type['key'];
                }else if($this->post_type['key'] == 'page'){
                    return 'page';
                }else{
                    return 'post';
                }
            }

        }

        return $view;
    }

    /**
     * @return array
     */
    private function getCurrentLanguage(): array
    {
        $current_language = Language::where('locale', app()->getLocale())->where('status', 1)->first();
        $display_language = json_decode(json_encode(display_language($current_language->locale)), TRUE);
        return [
            'locale' => $current_language->locale,
            'flag' => asset('jspress/assets/media/flags/svg/'.$current_language->locale).'.svg',
            'english_name' => $display_language['english_name'],
            'display_name' => $display_language['display_language_name'],
            'default_locale' => $display_language['default_locale'],
            'display_from' => $display_language['display_language_code'],
        ];
    }

    /**
     * @return array|null
     */
    private function setPostTranslations():? array
    {
        $post_translations = PostTranslation::where('post_translations.transaction_id', $this->current_translation['transaction_id'])
            ->join('posts', function($join){
                $join->on('posts.id', '=', 'post_translations.element_id')->where('posts.status', 1);
            })
            ->whereIn('post_translations.locale', website_languages()->pluck('locale')->toArray())
            ->where('post_translations.type', 'post')
            ->where('post_translations.locale', '!=', app()->getLocale())
            ->get();
        $data = [];
        if($post_translations->isNotEmpty()){
            foreach($post_translations as $post_translation){
                $display_language = json_decode(json_encode(display_language($post_translation->locale)), TRUE);
                $post_item = General::getThePostByIdForTranslation($post_translation->element_id);
                if($post_item){
                    $data[] = [
                        'id' => $display_language['id'],
                        'status' => $display_language['status'],
                        'is_default' => $display_language['is_default'],
                        'locale' => $post_translation->locale,
                        'flag' => asset('jspress/assets/media/flags/svg/'.$post_translation->locale).'.svg',
                        'english_name' => $display_language['english_name'],
                        'display_name' => $display_language['display_language_name'],
                        'default_locale' => $display_language['default_locale'],
                        'display_from' => $display_language['display_language_code'],
                        'url' => $this->setTranslationUrl($post_item, $post_translation->locale)
                    ];
                }
            }
        }

        return collect($data)->sort(function ($a, $b) {
            $result = $b['is_default'] <=> $a['is_default'];
            if ($result === 0) {
                $result = $b['status'] <=> $a['status'];
            }
            if ($result === 0) {
                $result = $a['id'] <=> $b['id'];
            }
            return $result;
        })->values()->toArray();
    }

    /**
     * @param $post_item
     * @param $locale
     * @return string
     */
    private function setTranslationUrl($post_item, $locale): string
    {

        if($locale == default_language()){
            $base_url = url('/');
        }else{
            $base_url = url($locale);
        }

        if($post_item->post_type_item->is_url == 0){
            return 'javascript:void(0);';
        }
        if($post_item->is_homepage == 1){
            return $base_url;
        }
        if($post_item->post_type_item->is_segment_disable == 1 && $post_item->post_id == 0){
            return $base_url.'/'.$post_item->slug;
        }
        if( $post_item->post_type_item->is_segment_disable == 0 && $post_item->post_id == 0 ){
            return $base_url.'/'.$post_item->post_type_item->post_detail_locale($locale)->slug.'/'.$post_item->slug;
        }
        if( $post_item->post_id != 0 ){
            $post = get_the_post($post_item->post_id);
            return $base_url.'/'.$post->slug.'/'.$post_item->slug;
        }

        return 'javascript:void(0);';
    }

    /**
     * @param $post
     * @return array
     */
    private function getPopups($post): array
    {

        $popup_data = [];
        $device = get_device_type();
        $popups = Popup::where('status', 1)
            ->where(function($query){
                return $query->where('date_type', 0)
                    ->orWhere(function($query){
                        return $query->where('date_type', 1)
                            ->whereDate('start_date', '<=', \Carbon\Carbon::now()->format('Y-m-d'))
                            ->whereDate('end_date', '>=', \Carbon\Carbon::now()->format('Y-m-d'));
                    });
            })
            ->where(function($query)use($post){
                return $query->where('page_show_type', 0)
                    ->when($post->is_homepage === 1, function($query){
                        $query->orWhere('page_show_type', 2);
                    })
                    ->orWhere(function($query)use($post){
                        return $query->where('page_show_type', 1)
                            ->whereHas('pages',function ($query) use ($post){
                                return $query->where('element_id', $post->id)->where('type', 'post');
                            });
                    })
                    ->orWhere(function($query){
                        return $query->where('page_show_type', 3)
                            ->whereHas('urls', function($q){
                                return $q->where('url', '/'.request()->path());
                            });
                    });
            })
            ->whereHas('devices', function($query)use($device){
                return $query->where('device', $device);
            })
            ->orderBy('order', 'asc')
            ->get();

        foreach($popups as $key => $popup){
            $popup_data[$key] = collect($popup)->except(['created_at','updated_at','status','order','start_date','end_date',''])->toArray();
            $popup_data[$key]['devices'] = $popup->devices->pluck('device')->toArray();
        }

        return $popup_data;
    }

}
