<?php

namespace JsPress\JsPress\DataSources;

use JsPress\JsPress\Models\Admin;
use JsPress\JsPress\Models\Category;
use JsPress\JsPress\Models\Language;
use JsPress\JsPress\Models\Popup;
use JsPress\JsPress\Models\PostExtra;
use JsPress\JsPress\Models\PostTranslation;
use JsPress\JsPress\Models\PostTypeDetail;

class CategoryDataSource{

    /**
     * @var int|mixed
     */
    public int $id;
    /**
     * @var int|mixed
     */
    public int $category_id;
    /**
     * @var string|mixed
     */
    public string $title;
    /**
     * @var string|mixed|null
     */
    public ?string $content;
    /**
     * @var string|mixed|null
     */
    public ?string $slug;
    /**
     * @var string|null
     */
    public ?string $author;
    /**
     * @var string|mixed
     */
    public string $publish_date;
    /**
     * @var string|mixed
     */
    public string $publish_date_gmt;
    /**
     * @var int|mixed
     */
    public int $status;
    /**
     * @var int|mixed
     */
    public int $order;
    /**
     * @var string|null
     */
    public ?string $view;
    /**
     * @var string|null
     */
    public ?string $url;
    /**
     * @var array|null
     */
    public ?array $extras;
    /**
     * @var array|null
     */
    public ?array $seo = [];
    /**
     * @var array|null
     */
    public ?array $parent = null;
    /**
     * @var array|null
     */
    public ?array $children = null;
    /**
     * @var array|null
     */
    public ?array $other_languages;
    /**
     * @var string|null
     */
    public ?string $type;
    /**
     * @var array|mixed
     */
    public array $post_type;
    /**
     * @var array|mixed|null
     */
    public ?array $current_translation;
    /**
     * @var array|null
     */
    public ?array $current_language;
    /**
     * @var array|null
     */
    public ?array $breadcrumbs;

    /**
     * @var array
     */
    public array $popups = [];

    /**
     * @param $author_id
     * @return string|null
     */
    private function setAuthor($author_id):? string
    {
        $author = Admin::find($author_id);
        return !is_null($author) ? $author->name : NULL;
    }

    /**
     * @param Category $category
     * @param $is_collect
     */
    public function __construct(Category $category, $is_collect = true){

        $post_type = $category->post_type_item;
        $this->type = 'category';
        $this->id = $category->id;
        $this->category_id = $category->category_id;
        $this->title = $category->title;
        $this->content = $category->content;
        $this->slug = $category->slug;
        $this->author = $this->setAuthor($category->author);
        $this->publish_date = $category->publish_date;
        $this->publish_date_gmt = $category->publish_date_gmt;
        $this->status = $category->status;
        $this->order = $category->order;
        $this->extras = $this->setExtras();
        $this->post_type = json_decode(json_encode($post_type), TRUE);
        if($is_collect){
            if($category->children->isNotEmpty()){
                foreach($category->children as $child){
                    $this->children[] = new self($child);
                }
            }
            $this->breadcrumbs = $this->setBreadCrumbs($category, $category);
            $this->seo = $this->setSeoMeta();
            $this->view = $this->setView();
            $this->current_translation = json_decode(json_encode($category->translation), TRUE);
            $this->other_languages = $this->setCategoryTranslations();
            $this->current_language = $this->getCurrentLanguage();
            $this->popups = $this->getPopups($category);
        }
        $url = get_the_url($category, $post_type);
        $this->url = $url;

        return $this;
    }

    /**
     * @param $category
     * @param $first_category
     * @param $breadcrumb_data
     * @return array|null
     */
    private function setBreadCrumbs($category, $first_category, &$breadcrumb_data = []):? array
    {
        if($category->category_id != 0){
            $category = get_the_category($category->category_id);
            $breadcrumb_data[] = $category->id;
            $this->setBreadCrumbs($category, $first_category, $breadcrumb_data);
        }
        $category_ids = array_reverse($breadcrumb_data);
        $homepage = get_the_homepage();
        if(!is_null($homepage)){
            $result_data[] = [
                'title' => $homepage->title,
                'url' => get_base_url()
            ];
        }
        foreach($category_ids as $category_id){
            $category = Category::find($category_id);
            $result_data[] = [
                'title' => $category->title,
                'url' => get_the_url($category, $category->post_type_item)
            ];
        }
        $result_data[] = [
            'title' => $first_category->title,
            'url' => get_the_url($first_category, $category->post_type_item)
        ];
        return $result_data;
    }

    /**
     * @return array
     */
    private function setExtras(): array
    {
        $extra_data = [];
        $extras = PostExtra::where('element_id', $this->id)->where('type', 'category')->get()->toArray();
        $extra_data = [];
        foreach(collect($extras)->groupBy('key') as $key => $extra){
            if($extra->first()['is_multiple'] == 1){
                $extra_data[$key] = $extra->map(function($item){
                    return is_numeric($item['value']) ? intval($item['value']) : $item['value'];
                })
                    ->toArray();
            }else{
                $extra_data[$key] = is_json($extra->first()['value']) ? json_decode($extra->first()['value'], TRUE) : $extra->first()['value'];
            }

        }

        return $extra_data;
    }

    /**
     * @return array|null
     */
    private function setSeoMeta():? array
    {
        $seo_data['title'] =  $this->title;
        $seo_data['description'] = substr(strip_tags($this->content),0,160);
        $seo_data['facebook']['title'] = $this->title;
        $seo_data['facebook']['description'] = substr(strip_tags($this->content),0,160);
        $seo_data['facebook']['image'] = null;
        $seo_data['twitter']['title'] = $this->title;
        $seo_data['twitter']['description'] = substr(strip_tags($this->content),0,160);
        $seo_data['twitter']['image'] = null;
        $seo_data['canonical'] = request()->fullUrl();
        return $seo_data;
    }

    /**
     * @return string
     */
    private function setView(): string
    {
        if( \View::exists('category-'.$this->post_type['key'])){
            return 'category-'.$this->post_type['key'];
        }else{
            return 'category';
        }
    }


    /**
     * @return array|null
     */
    private function setCategoryTranslations():? array
    {
        $category_translations = PostTranslation::select(
            'post_translations.*',
            'post_types.id as post_type_id',
            'post_types.key',
            'post_types.is_category',
            'post_types.is_url',
            'post_types.is_category_url',
            'post_types.is_segment_disable'
        )
            ->where('post_translations.transaction_id', $this->current_translation['transaction_id'])
            ->join('categories', function($join){
                $join->on('categories.id', '=', 'post_translations.element_id')->where('categories.status', 1);
            })
            ->join('post_types', function($join){
                $join->on('post_types.key', '=', 'categories.category_type');
            })
            ->whereIn('post_translations.locale', website_languages()->pluck('locale')->toArray())
            ->where('post_translations.type', 'category')
            ->where('post_translations.locale', '!=', app()->getLocale())
            ->get();

        $data = [];
        if($category_translations->isNotEmpty()){
            foreach($category_translations as $category_translation){
                $display_language = json_decode(json_encode(display_language($category_translation->locale)), TRUE);
                $category_item = get_the_category_by_locale($category_translation->element_id, $category_translation->locale);
                $category_type_detail = PostTypeDetail::where('post_type_id', $category_translation->post_type_id)->where('model', 'JsPress\JsPress\Models\Category')->where('locale', $category_translation->locale)->first();
                if($category_item){
                    $data[] = [
                        'id' => $display_language['id'],
                        'status' => $display_language['status'],
                        'is_default' => $display_language['is_default'],
                        'locale' => $category_translation->locale,
                        'flag' => asset('jspress/assets/media/flags/svg/'.$category_translation->locale).'.svg',
                        'english_name' => $display_language['english_name'],
                        'display_name' => $display_language['display_language_name'],
                        'default_locale' => $display_language['default_locale'],
                        'display_from' => $display_language['display_language_code'],
                        'url' => $this->setTranslationUrl($category_item, $category_translation->locale, $category_type_detail)
                    ];
                }
            }
        }
        return collect($data)->sort(function ($a, $b) {
            $result = $b['is_default'] <=> $a['is_default'];
            if ($result === 0) {
                $result = $b['status'] <=> $a['status'];
            }
            if ($result === 0) {
                $result = $a['id'] <=> $b['id'];
            }
            return $result;
        })->values()->toArray();
    }

    /**
     * @param $category_item
     * @param $locale
     * @return string
     */
    private function setTranslationUrl($category_item, $locale, $category_type_detail): string
    {
        if($locale == default_language()){
            $base_url = url('/');
        }else{
            $base_url = url($locale);
        }

        if($category_item->post_type_item->is_category_url == 0){
            return 'javascript:void(0);';
        }else{
            return $base_url.'/'.$category_type_detail->slug.'/'.$category_item->slug;
        }
    }

    /**
     * @return array
     */
    private function getCurrentLanguage(): array
    {
        $current_language = Language::where('locale', app()->getLocale())->where('status', 1)->first();
        $display_language = json_decode(json_encode(display_language($current_language->locale)), TRUE);
        return [
            'locale' => $current_language->locale,
            'flag' => asset('jspress/assets/media/flags/svg/'.$current_language->locale).'.svg',
            'english_name' => $display_language['english_name'],
            'display_name' => $display_language['display_language_name'],
            'default_locale' => $display_language['default_locale'],
            'display_from' => $display_language['display_language_code'],
        ];
    }

    /**
     * @param $category
     * @return array
     */
    private function getPopups($category): array
    {

        $popup_data = [];
        $device = get_device_type();
        $popups = Popup::where('status', 1)
            ->where(function($query){
                return $query->where('date_type', 0)
                    ->orWhere(function($query){
                        return $query->where('date_type', 1)
                            ->whereDate('start_date', '<=', \Carbon\Carbon::now()->format('Y-m-d'))
                            ->whereDate('end_date', '>=', \Carbon\Carbon::now()->format('Y-m-d'));
                    });
            })
            ->where(function($query)use($category){
                return $query->where('page_show_type', 0)
                    ->orWhere(function($query)use($category){
                        return $query->where('page_show_type', 1)
                            ->whereHas('pages',function ($query) use ($category){
                                return $query->where('element_id', $category->id)->where('type', 'category');
                            });
                    });
            })
            ->whereHas('devices', function($query)use($device){
                return $query->where('device', $device);
            })
            ->orderBy('order', 'asc')
            ->get();
        foreach($popups as $key => $popup){
            $popup_data[$key] = collect($popup)->except(['created_at','updated_at','status','order','start_date','end_date',''])->toArray();
            $popup_data[$key]['devices'] = $popup->devices->pluck('device')->toArray();
        }

        return $popup_data;
    }

}
