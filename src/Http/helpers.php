<?php

/**
 * @helpers
 * @author JSON Yazılım
 * @website www.jsonyazilim.com
 *
 */

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use JsPress\JsPress\Helpers\JsPressPanel;
use JsPress\JsPress\Models\Post;
use JsPress\JsPress\Helpers\General;
use JsPress\JsPress\Models\PostExtra;
use Illuminate\Pagination\LengthAwarePaginator;
use Jenssegers\Agent\Agent;

if( !function_exists('switch_roles') ){
    function switch_roles($role): array
    {
        $roleClassData = [];
        switch ($role) {
            case "Super-Admin":
                $roleClassData = [
                    'class' => 'badge-light-danger',
                    'text' => 'text-danger'
                ];
                break;
            case "Admin":
                $roleClassData = [
                    'class' => 'badge-light-success',
                    'text' => 'text-success'
                ];
                break;
            case "Moderator":
                $roleClassData = [
                    'class' => 'badge-light-info',
                    'text' => 'text-info'
                ];
                break;
            case "Editor":
                $roleClassData = [
                    'class' => 'badge-light-warning',
                    'text' => 'text-warning'
                ];
                break;
            default:
                $roleClassData = [
                    'class' => 'badge-light-primary',
                    'text' => 'text-primary'
                ];
        }
        return $roleClassData;
    }
}

if(!function_exists('setting')){
    function setting($key){
        $settings = \Illuminate\Support\Facades\Cache::remember('website_settings', 7*24*60*60, function(){
            $settings = \JsPress\JsPress\Models\Setting::all();
            $settings_data = [];
            foreach( $settings as $setting ){
                $settings_data[$setting->key] = $setting->value;
            }
            return $settings_data;
        });
        return $settings[$key] ?? null;

    }
}

if(!function_exists('__js')){
    function __js($group, $key, $value, $params = [])
    {
        $translationService = new \JsPress\JsPress\Services\TranslationService();
        return $translationService->get(app()->getLocale(), $group, $key, $value, $params);
    }
}

if(!function_exists('default_language')){
    function default_language(){
        return \Illuminate\Support\Facades\Cache::rememberForever('default_locale', function(){
            if (Schema::hasTable('languages')) {
                $defaultLanguage = DB::table('languages')->where('is_default', 1)->value('locale');
                if(!$defaultLanguage){
                    $defaultLanguage = config('jspress.language.default_language');
                }
            } else {
                $defaultLanguage = config('jspress.language.default_language') ?? 'tr';
            }
            return $defaultLanguage;
        });
    }
}

if(!function_exists('setPrefixLocale')){
    function setPrefixLocale(): string
    {
        $defaultLanguage = default_language();
        $currentLanguage = app()->getLocale();
        $hideUrlDefaultLanguage = config('jspress.language.hide_url_default_language');
        if($hideUrlDefaultLanguage && ($defaultLanguage == $currentLanguage)){
            return '';
        }else{
            return '{locale}';
        }
    }
}

if(!function_exists('website_languages')){
    function website_languages(){
        if( \Schema::hasTable('languages') ){
            return \DB::table('languages')->where('status', 1)->orderBy('is_default', 'DESC')->get();
        }else{
            return [];
        }

    }
}

if(!function_exists('supported_locales')){
    function supported_locales(){
        if(Schema::hasTable('languages')){
            return \Illuminate\Support\Facades\Cache::rememberForever('supported_locales', function(){
                $data = [];
                $locales = \DB::table('languages')->where('status',1)->get()->groupBy('locale');
                foreach($locales as $key => $locale){
                    $data[$key] = $locale[0];
                }
                return $data;
            });
        }else{
            return [
                'tr' => [
                    "id" => 53,
                    "locale" => "tr",
                    "english_name" => "Turkish",
                    "status" => 1,
                    "is_default" => 1,
                    "default_locale" => "tr_TR"
                ]
            ];
        }
    }
}

if(!function_exists('display_language')){
    function display_language($locale)
    {
        return \DB::table('languages')
            ->select([
                'languages.id',
                'languages.locale',
                'languages.english_name',
                'languages.status',
                'languages.is_default',
                'languages.default_locale',
                'language_translations.display_language_code as display_language_code',
                'language_translations.name as display_language_name'
            ])
            ->join('language_translations', function($join)use($locale){
                $join->on('language_translations.language_code', '=', 'languages.locale')
                    ->where('language_translations.language_code', $locale)
                    ->where('language_translations.display_language_code', app()->getLocale());
            })
            ->where('locale', $locale)
            ->first();
    }
}

if(!function_exists('parse_size')){
    function parse_size($size): string
    {
        $unit = preg_replace('/[^bkmgtpezy]/i', '', $size);
        $size = preg_replace('/[^0-9\.]/', '', $size);
        if ($unit) {
            return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
        }
        else {
            return round($size);
        }
    }
}

if(!function_exists('get_max_upload_size')){
    function get_max_upload_size(): string
    {
        static $max_size = -1;

        if ($max_size < 0) {
            $post_max_size = parse_size(ini_get('post_max_size'));
            if ($post_max_size > 0) {
                $max_size = $post_max_size;
            }

            $upload_max = parse_size(ini_get('upload_max_filesize'));
            if ($upload_max > 0 && $upload_max < $max_size) {
                $max_size = $upload_max;
            }
        }
        return $max_size;
    }
}

if(!function_exists('convert_max_file_size_to_string')){
    function convert_max_file_size_to_string(): string
    {
        $max_size = get_max_upload_size();
        return readable_file_size($max_size);
    }
}

if(!function_exists('readable_file_size')){
    function readable_file_size($size): string
    {
        $base = log($size) / log(1024);
        $suffix = array("", "KB", "MB", "GB", "TB");
        $f_base = floor($base);
        return round(pow(1024, $base - floor($base)), 1) . $suffix[$f_base];
    }
}

if(!function_exists('get_media_icon')){
    function get_media_icon($mime, $type): string
    {
        $icon = asset('jspress/assets/media/svg/files/fil004.svg');
        if( $type == 'application' ){
            if($mime == 'application/pdf'){
                $icon = asset('jspress/assets/media/svg/files/pdf.svg');
            }
            if($mime == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'){
                $icon = asset('jspress/assets/media/svg/files/doc.svg');
            }
            if($mime == 'application/vnd.ms-excel'){
                $icon = asset('jspress/assets/media/svg/files/excel.svg');
            }
            if($mime == 'application/application/zip' || $mime == 'application/x-zip-compressed' || $mime == 'multipart/x-zip'){
                $icon = asset('jspress/assets/media/svg/files/zip.svg');
            }
        }
        if( $type == 'video' ){
            $icon = asset('jspress/assets/media/svg/misc/video-play.svg');
        }
        if( $type == 'audio' ){
            $icon = asset('jspress/assets/media/svg/files/audio.svg');
        }
        if( $type == 'text' ){
            if( $mime == 'text/csv' ){
                $icon = asset('jspress/assets/media/svg/files/csv.svg');
            }
        }
        return $icon;
    }
}

if(!function_exists('status_badge')){
    function status_badge($status): string
    {
        return match (intval($status)) {
            0 => '<span class="badge badge-light-danger">' . __('JsPress::backend.deactive') . '</span>',
            1 => '<span class="badge badge-light-success">' . __('JsPress::backend.active') . '</span>',
            2 => '<span class="badge badge-light-warning">' . __('JsPress::backend.draft') . '</span>',
            3 => '<span class="badge badge-light-info">' . __('JsPress::backend.schedule') . '</span>'
        };
    }
}
if(!function_exists('switch_post_field_position')){
    function switch_post_field_position($position): string
    {
        return match ($position) {
            'after_title' => '<span class="badge badge-light-danger">' . __('JsPress::backend.post_field.after_title') . '</span>',
            'after_editor' => '<span class="badge badge-light-success">' . __('JsPress::backend.post_field.after_editor') . '</span>',
            'after_seo' => '<span class="badge badge-light-warning">' . __('JsPress::backend.post_field.after_seo') . '</span>',
            'sidebar' => '<span class="badge badge-light-info">' . __('JsPress::backend.post_field.sidebar') . '</span>',
            'post_bottom' => '<span class="badge badge-primary">' . __('JsPress::backend.post_field.post_bottom') . '</span>'
        };
    }
}

if(!function_exists('get_image_mime_type')){
    function get_image_mime_type($image_path): bool|string
    {
        $mimes  = array(
            IMAGETYPE_GIF => "image/gif",
            IMAGETYPE_JPEG => "image/jpg",
            IMAGETYPE_PNG => "image/png",
            IMAGETYPE_SWF => "image/swf",
            IMAGETYPE_PSD => "image/psd",
            IMAGETYPE_BMP => "image/bmp",
            IMAGETYPE_TIFF_II => "image/tiff",
            IMAGETYPE_TIFF_MM => "image/tiff",
            IMAGETYPE_JPC => "image/jpc",
            IMAGETYPE_JP2 => "image/jp2",
            IMAGETYPE_JPX => "image/jpx",
            IMAGETYPE_JB2 => "image/jb2",
            IMAGETYPE_SWC => "image/swc",
            IMAGETYPE_IFF => "image/iff",
            IMAGETYPE_WBMP => "image/wbmp",
            IMAGETYPE_XBM => "image/xbm",
            IMAGETYPE_ICO => "image/ico",
            IMAGETYPE_WEBP => "image/webp"
        );

        if (\File::exists(public_path($image_path)) && ($image_type = exif_imagetype(public_path($image_path)))
            && (array_key_exists($image_type ,$mimes)))
        {
            return $mimes[$image_type];
        }
        else
        {
            return FALSE;
        }
    }
}

if(!function_exists('create_language_redirect_url')){
    function create_language_redirect_url($locale): string
    {
        if(\Illuminate\Support\Facades\Route::is('admin.post.add')){
            $url = route('admin.post.index', ['post-type' => request()->input('post-type'), 'lang' => $locale ]);
        }elseif(\Illuminate\Support\Facades\Route::is('admin.post.edit')){
            $url = route('admin.post.index', ['post-type' => request()->input('post-type'), 'lang' => $locale ]);
        }elseif(\Illuminate\Support\Facades\Route::is('admin.category.edit')){
            $url = route('admin.category.index', ['post-type' => request()->input('post-type'), 'lang' => $locale ]);
        }else{
            $url = request()->fullUrlWithQuery(['lang' => $locale]);
        }

        return $url;
    }
}

if(!function_exists('get_the_categories')){
    function get_the_categories(
        $args = []
    ):? array
    {
        return General::getTheCategories($args);
    }
}

if(!function_exists('get_the_admin_categories')){
    function get_the_admin_categories(
        $args = [],
        array &$category_data = []
    ): array
    {
        if(!is_array($args)){
            eval("\$args = $args;");
        }
        if(!isset($args['post_type'])){
            $post_type = \DB::table('post_types')->where('status', 1)->where('is_category', 1)->where('post_type_id', 0)->first();
            $args['post_type'] = $post_type->key;
        }
        if(!isset($args['category_id'])){
            $args['category_id'] = 0;
        }
        if(!isset($args['deep'])){
            $args['deep'] = false;
        }
        if(!isset($args['order'])){
            $args['order'] = 'title';
        }
        if(!isset($args['orderby'])){
            $args['orderby'] = 'asc';
        }

        $categories = \DB::table('categories')
            ->select(
                'categories.id',
                'categories.category_id',
                'categories.title',
                'categories.content',
                'categories.slug',
                'categories.author',
                'categories.publish_date',
                'categories.publish_date_gmt',
                'categories.order',
                'categories.status'
            )
            ->join('post_translations as category_translation', function($join){
                $join->on('category_translation.element_id', '=', 'categories.id')
                    ->where('category_translation.type', 'category')
                    ->where('category_translation.region', 1)
                    ->where('category_translation.locale', JsPressPanel::contentLocale());
            })
            ->where('categories.category_type', $args['post_type'])
            ->where('categories.category_id', $args['category_id'])
            ->when(isset($args['current_category_id']), function($query)use($args){
                return $query->where('categories.id', '!=', $args['current_category_id']);
            })
            ->orderBy('categories.'.$args['order'], $args['orderby'])
            ->get()
            ->toArray();

        $categories = json_decode(json_encode($categories), TRUE);
        foreach ($categories as $category) {
            $category_data[$category['id']] = $category;
            if($args['deep']){
                $category_data[$category['id']]['children'] = [];
                $args['current_category_id'] = $category['id'];
                $args['category_id'] = $category['id'];
                get_the_admin_categories($args, $category_data[$category['id']]['children']);
            }
        }
        return collect($category_data)->values()->map(function ($category) {
            return reindex_categories($category);
        })->all();
    }
}

if(!function_exists('get_the_category')){
    function get_the_category($id): \JsPress\JsPress\Models\Category
    {
        return General::getTheCategory($id);
    }
}

if(!function_exists('get_the_category_by_locale')){
    function get_the_category_by_locale($id, $locale): \JsPress\JsPress\Models\Category
    {
        return General::getTheCategoryByLocale($id, $locale);
    }
}

if(!function_exists('get_the_post_category')){
    function get_the_post_category($post_id, $lang = null): object
    {
        return General::getThePostCategories($post_id, $lang);
    }
}

if(!function_exists('reindex_categories')){
    function reindex_categories($category)
    {
        if (isset($category['children'])) {
            $subcategories = collect($category['children']);
            $category['children'] = $subcategories->values()->map(function ($subcategory) {
                return reindex_categories($subcategory);
            })->all();
        }
        return $category;
    }
}

if(!function_exists('get_file')){
    function get_file($id): array|null
    {
        return Cache::remember('media_detail_'.$id, 7*24*60*60, function()use($id){
            $media_detail = new \JsPress\JsPress\Repositories\MediaRepository();
            return $media_detail->getDetail($id);
        });
    }
}

if(!function_exists('the_thumbnail_url')){
    function the_thumbnail_url($file_id, $dimension = null):string
    {
        if(is_null($file_id)) return "";
        $file_id = intval($file_id);
        $media = get_file($file_id);

        if(is_null($media)){
            return "";
        }
        $image = $media['original_url'];
        if(is_null($dimension)){
            $image = $media['original_url'];
            if(!is_null($media['optimized_url'])){
                $image = $media['optimized_url'];
            }
        }
        if($media['mime'] == 'image/svg+xml' || $media['mime'] == 'image/x-icon'){
            $image = $media['original_url'];
        }
        if( array_key_exists($dimension, $media['dimensions']) && $media['type'] == 'image' ){
            $image = $media['dimensions'][$dimension]['url'];
            if(!is_null($media['dimensions'][$dimension]['optimized_url'])){
                $image = $media['dimensions'][$dimension]['optimized_url'];
            }
        }
        return $image;
    }
}

if(!function_exists('get_the_image_html')){
    function get_the_image_html($file_id, $dimension = null, $args = []):string
    {
        if(!isset($args['lazy'])){
            $args['lazy'] = false;
        }
        if(!isset($args['lazy_url'])){
            $args['lazy'] = "";
        }

        if(is_null($file_id)){
            $image_data = [
                'src' => "",
                'tag' => "",
                'title' => ""
            ];
        }else{
            $file_id = intval($file_id);
            $media = get_file($file_id);

            if(is_null($media)){
                $image_data = [
                    'src' => "",
                    'tag' => "",
                    'title' => ""
                ];
            }else{
                if(is_null($dimension)){
                    $image_data = [
                        'src' => !is_null($media['optimized_url']) ? $media['optimized_url'] : $media['original_url'],
                        'tag' => $media['details'][app()->getLocale()]['tag'] ?? "",
                        'title' => $media['details'][app()->getLocale()]['tag'] ?? ""
                    ];
                }else if($media['mime'] == 'image/svg+xml' || $media['mime'] == 'image/x-icon'){
                    $image_data = [
                        'src' => $media['original_url'],
                        'tag' => $media['details'][app()->getLocale()]['tag'] ?? "",
                        'title' => $media['details'][app()->getLocale()]['tag'] ?? ""
                    ];
                }else if( array_key_exists($dimension, $media['dimensions']) && $media['type'] == 'image' ){
                    $image_data = [
                        'src' => !is_null($media['dimensions'][$dimension]['optimized_url']) ? $media['dimensions'][$dimension]['optimized_url'] : $media['dimensions'][$dimension]['url'] ?? $media['original_url'],
                        'tag' => $media['details'][app()->getLocale()]['tag'] ?? "",
                        'title' => $media['details'][app()->getLocale()]['tag'] ?? ""
                    ];
                }else{
                    $image_data = [
                        'src' => !is_null($media['optimized_url']) ? $media['optimized_url'] : $media['original_url'],
                        'tag' => $media['details'][app()->getLocale()]['tag'] ?? "",
                        'title' => $media['details'][app()->getLocale()]['tag'] ?? ""
                    ];
                }
            }
        }

        if($args['lazy']){
            return '<img data-src="'.$image_data["src"].'" class="lazy" src="'.$args["lazy_url"].'" title="'.$image_data["title"].'" alt="'.$image_data["tag"].'"/>';
        }
        return '<img src="'.$image_data["src"].'" title="'.$image_data["title"].'" alt="'.$image_data["tag"].'"/>';
    }
}

if(!function_exists('the_thumbnail_file')){
    function the_thumbnail_file($file_id):string
    {
        if(is_null($file_id)) return "";
        $file_id = intval($file_id);
        $media = get_file($file_id);

        return the_file_image($media);
    }
}
if(!function_exists('the_file_image')){
    function the_file_image($media):string
    {
        if(is_null($media)) return "";

        if($media['type'] === 'application'){
            if($media['mime'] === 'application/pdf'){
                return  url("/jspress/assets/media/svg/files/pdf.svg");
            }else if($media['mime'] === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'){
                return url("/jspress/assets/media/svg/files/doc.svg");
            }else if( $media['mime'] === 'application/vnd.ms-excel' ){
                return url("/jspress/assets/media/svg/files/excel.svg");
            }else if( $media['mime'] === 'application/application/zip' || $media['mime'] === 'application/x-zip-compressed' || $media['mime'] === 'multipart/x-zip'){
                return url("/jspress/assets/media/svg/files/zip.svg");
            }else{
                return url("/jspress/assets/media/icons/duotune/files/fil004.svg");
            }
        }
        if($media['type'] === 'video'){
            return url("/jspress/assets/media/svg/misc/video-play.svg");
        }
        if($media['type'] === 'audio'){
            return url("/jspress/assets/media/svg/files/audio.svg");
        }
        if($media['type'] === 'text'){
            if($media['mime'] === 'text/csv'){
               return url("/jspress/assets/media/svg/files/csv.svg");
            }else{
               return url("/jspress/assets/media/icons/duotune/files/txt.svg");
            }
        }
        return "";
    }
}

if(!function_exists('dynamic_condition')){
    function dynamic_condition ($var1, $op, $var2): bool
    {
        return match ($op) {
            "=" => $var1 == $var2,
            "!=" => $var1 != $var2,
            ">=" => $var1 >= $var2,
            "<=" => $var1 <= $var2,
            ">" => $var1 > $var2,
            "<" => $var1 < $var2,
            default => true,
        };
    }
}
if(!function_exists('repeater_select_parse')){
    function repeater_select_parse($options): array
    {
        $data = [];
        $option_data = explode(PHP_EOL, $options);
        foreach($option_data as $option_item){
            $option_item = explode(':', $option_item);
            $data[] = [
                'value' =>  $option_item[0],
                'label' => isset($option_item[1]) ? $option_item[1] : $option_item[0]
            ];
        }
        return $data;
    }
}
if(!function_exists('repeater_selectbox_parse')){
    function repeater_selectbox_parse($field_data): array
    {
        $data = [];
        if($field_data['option_type'] == 'manual' ){
            return repeater_select_parse($field_data['options']);
        }
        if($field_data['option_type'] == 'database' ){
            $options = json_decode(json_encode(\DB::table($field_data['select_options_table'])->get()), TRUE);
            foreach($options as $option){
                $data[] = [
                    'value' =>  $option[$field_data['select_options_column_value']],
                    'label' => $option[$field_data['select_options_column_label']]
                ];
            }
        }
        return $data;
    }
}
if(!function_exists('get_posts_by_post_types')){
    function get_posts_by_post_types(array $post_types): array
    {
        return General::getPostsByPostType($post_types);
    }
}

if(!function_exists('get_categories_by_post_types')){
    function get_categories_by_post_types(array $post_types): array
    {
        return General::getCategoriesByPostType($post_types);
    }
}


if(!function_exists('get_the_field')){
    function get_the_field($key, $element_id, $type = null){
        $field = \JsPress\JsPress\Models\PostExtra::where('key', $key)
            ->where('element_id', $element_id)
            ->when(!is_null($type), function($query)use($type){
                return $query->where('type', $type);
            })
            ->get();
        if($field->isNotEmpty()){
            if( $field->first()->is_multiple == 1 ){
                return $field->pluck('value')->toArray();
            }else{
                if(is_json($field->value)){
                    return json_decode($field->value, TRUE);
                }
                return $field->value;
            }
        }
        return null;
    }
}

if(!function_exists('is_json')){
    function is_json($string): bool
    {
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }
}

if(!function_exists('get_the_posts_model')){
    function get_the_posts_model(
        $args = []
    ): Post|LengthAwarePaginator|null
    {
        return General::getThePostsModel($args);
    }
}

if(!function_exists('get_the_posts')){
    function get_the_posts(
        $args = []
    ):? \JsPress\JsPress\Collections\PostCollection
    {
        return General::getThePosts($args);
    }
}

if(!function_exists('get_the_posts_array')){
    function get_the_posts_array(
        $args = []
    ):? array
    {
        return General::getThePostsArray($args);
    }
}

if(!function_exists('the_post_thumbnail_url')){
    function the_post_thumbnail_url($post_id, $dimension = null):string
    {
        if(is_null($post_id)) return "";
        $featured_image = \JsPress\JsPress\Models\PostExtra::where('element_id', $post_id)->where('type', 'post')->where('key', 'featured_image')->first();
        if(is_null($featured_image) && is_null($featured_image->value)) return "";
        $file_id = intval($featured_image->value);
        $media = get_file($file_id);

        if(is_null($media)){
            return "";
        }

        if(is_null($dimension)){
            return $media['original_url'];
        }
        if($media['mime'] == 'image/svg+xml' || $media['mime'] == 'image/x-icon'){
            return $media['original_url'];
        }
        if( array_key_exists($dimension, $media['dimensions']) && $media['type'] == 'image' ){
            return $media['dimensions'][$dimension]['url'];
        }
        return "";
    }
}

if(!function_exists('the_press_info')){
    function the_press_info($type): ?string
    {
        if( $type == 'base_url' ){
            return get_base_url();
        }
        return null;
    }
}

if(!function_exists('get_base_url')){
    function get_base_url(): string
    {
        if(app()->getLocale() == default_language() && config('jspress.language.hide_url_default_language')){
            return url('/');
        }
        return url(app()->getLocale());
    }
}

if(!function_exists('get_the_post_by_locale')){
    function get_the_post_by_locale($id, $locale): object
    {
        return General::getThePostByLocale($id, $locale);
    }
}

if(!function_exists('get_the_post')){
    function get_the_post($id):? \JsPress\JsPress\DataSources\PostDataSource
    {
        return General::getThePostById($id);
    }
}

if(!function_exists('get_the_post_without_status')){
    function get_the_post_without_status($id):? \JsPress\JsPress\DataSources\PostDataSource
    {
        return General::getThePostByIdWithoutStatus($id);
    }
}

if(!function_exists('get_the_post_by_template')){
    function get_the_post_by_template($template):? \JsPress\JsPress\DataSources\PostDataSource
    {
        return General::getThePostByTemplate($template);
    }
}

if(!function_exists('get_the_parent_post')){
    function get_the_parent_post($post_id){
        return Post::where('id',$post_id)->with('post_type_item')->first();
    }
}

if(!function_exists('get_the_route_name')){
    function get_the_route_name($key, $type): string
    {
        $route_name = "";
        switch($type){
            case('post'):
                $route_name =  'post.'.$key;
                break;
            case('category'):
                $route_name =  'category.'.$key;
                break;
        }
        return $route_name;
    }
}

if(!function_exists('get_the_url')){
    function get_the_url($item, $post_type = null): string
    {

        if(is_null($post_type)){
            $post_type = \JsPress\JsPress\Models\PostType::where('key', $item->post_type)->first();
        }
        if( $item instanceof Post){
            if($post_type->is_url == 0){
                return 'javascript:void(0);';
            }

            if($post_type->is_segment_disable == 1 && $item->post_id == 0){
                return route('post', ['slug'=>$item->slug]);
            }
            if( $post_type->is_segment_disable == 0 && $item->post_id == 0 ){
                $route_name = get_the_route_name($post_type->key, 'post');
                return route($route_name, ['slug'=>$item->slug]);
            }
            if( $item->post_id != 0 ){
                $parent_post = \DB::table('posts')->where('id', $item->post_id)->first();
                return route('subPost', ['parent_slug'=>$parent_post->slug, 'slug' => $item->slug]);
            }
        }
        if( $item instanceof \JsPress\JsPress\Models\Category){
            if($post_type->is_category_url == 0){
                return 'javascript:void(0);';
            }else{
                $route_name = get_the_route_name($post_type->key, 'category');
                return route($route_name, ['slug'=>$item->slug]);
            }
        }
        return 'javascript:void(0);';
    }
}
if(!function_exists('get_the_view_url')){
    function get_the_view_url($post_item, $post_type): string
    {

        $base_url = the_press_info('base_url');

        if($post_type->is_url == 0){
            return 'javascript:void(0);';
        }
        if($post_type->is_segment_disable == 1 && $post_item->post_id == 0){
            return $base_url.'/'.$post_item->slug;
        }
        if( $post_type->is_segment_disable == 0 && $post_item->post_id == 0 ){
            return $base_url.'/'.$post_type->slug.'/'.$post_item->slug;
        }
        if( $post_item->post_id != 0 ){
            $post = get_the_post($post_item->post_id);
            return $base_url.'/'.$post->slug.'/'.$post_item->slug;
        }

        return 'javascript:void(0);';
    }
}

if(!function_exists('get_the_menu')){
    function get_the_menu($slug, $locale = null): ?array
    {
        return \General::getMenuItems($slug, is_null($locale) ? app()->getLocale() : $locale);
    }
}

if(!function_exists('get_the_homepage')){
    function get_the_homepage()
    {
        $post = Post::where('is_homepage', 1)->where('status', 1)
            ->whereHas('translation', function($query){
                return $query->where('locale', app()->getLocale())->where('region', 1);
            })
            ->first();
        if(!is_null($post)){
            $post->url = get_the_url($post);
        }
        return $post;
    }
}

if(!function_exists('convert_extras')){
    function convert_extras($extras, $element_id):array
    {
        $extra_data = [];
        $extras = PostExtra::where('element_id', $element_id)->where('type', 'category')->get();
        foreach($extras as $extra){
            $extra_data[$extra['key']] = is_json($extra['value']) ? json_decode($extra['value'], TRUE) : $extra['value'];
        }
        return $extra_data;
    }
}

if(!function_exists('generate_post_order_session_key')){
    function generate_post_order_session_key($post_type): string
    {
        return $post_type.'_post_order';
    }
}

if(!function_exists('get_gravatar')){
    function get_gravatar($email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array())
    {
        $url = 'https://www.gravatar.com/avatar/';
        $url .= md5(strtolower(trim($email)));
        $url .= "?s=$s&d=$d&r=$r";
        if ($img) {
            $url = '<img src="' . $url . '"';
            foreach ($atts as $key => $val) {
                $url .= ' ' . $key . '="' . $val . '"';
            }
            $url .= ' />';
        }

        return $url;
    }
}

if(!function_exists('is_active_children_menu')){
    function is_active_children_menu($menu): bool
    {
        $child_menu_array = collect($menu['children'])->pluck('url')->toArray();

        return in_array(request()->fullUrl(), $child_menu_array);
    }
}

if(!function_exists('get_popup_pages')){
    function get_popup_pages($popup): array
    {
        $data = [];
        if($popup->page_show_type == 1){
            foreach($popup->pages as $item){
                if($item->type == 'post'){
                    $post = Post::find($item->element_id);
                    $data['items'][] = [
                        'id' => $post->id,
                        'text' => $post->title .' ('. __js('post_type', JsPressPanel::postTypeLangKey($post->post_type_item, 'list'), $post->post_type_item->name).')',
                        'type' => 'post'
                    ];
                }
                if($item->type == 'category'){
                    $category = \JsPress\JsPress\Models\Category::find($item->element_id);
                    $data['items'][] = [
                        'id' => $category->id,
                        'text' => $category->title. ' ('.__js('post_type', JsPressPanel::postTypeLangKey($category->post_type_item, 'category'), $category->post_type_item->category_detail->plural_name).')',
                        'type' => 'category'
                    ];
                }
            }
            $data['value'] = json_encode($data['items']);
        }

        return $data;
    }
}

if(!function_exists('get_device_type')){
    function get_device_type(): string
    {
        $agent = new Agent();
        $device_type = 'default';

        if(  $agent->isMobile() && !$agent->isTablet() ) {
            $device_type = 'mobile';
        }

        if($agent->isTablet()){
            $device_type = 'tablet';
        }

        if($agent->isDesktop()){
            $device_type = 'desktop';
        }
        return $device_type;
    }
}

if(!function_exists('get_cookie')){
    function get_cookie(): array
    {
        $cookie_data = [];
        $cookie = \JsPress\JsPress\Models\Cookie::where('id', 1)
            ->whereJsonContains('languages', app()->getLocale())
            ->where('status', 1)
            ->first();
        if($cookie){
            $cookie_data['cookie']['name'] = $cookie->cookie_name;
            $cookie_data['guiOptions'] = [
                'consentModal' => $cookie->consentModal,
                'preferencesModal' => $cookie->preferencesModal
            ];
            $cookie_data['guiOptions']['consentModal']['layout'] = implode(' ', explode('_', $cookie->consentModal['layout']));
            $cookie_data['guiOptions']['consentModal']['position'] = implode(' ', explode('_', $cookie->consentModal['position']));
            $cookie_data['guiOptions']['preferencesModal']['layout'] = implode(' ', explode('_', $cookie->preferencesModal['layout']));
            $cookie_data['guiOptions']['preferencesModal']['position'] = implode(' ', explode('_', $cookie->preferencesModal['position']));
            $cookie_data['categories']['necessary']['readOnly'] = true;
            $categories = ['necessary'];
            if(in_array(2, $cookie->categories)){
                $cookie_data['categories']['functionality']['enabled'] = false;
                $categories[] = 'functionality';
            }
            if(in_array(3, $cookie->categories)){
                $cookie_data['categories']['analytics']['enabled'] = false;
                $categories[] = 'analytics';
            }
            if(in_array(4, $cookie->categories)){
                $cookie_data['categories']['marketing']['enabled'] = false;
                $categories[] = 'marketing';
            }

            $cookie_data['language']['default'] = app()->getLocale();
            $cookie_translation = $cookie->translations[app()->getLocale()];
            $cookie_data['language']['translations'][app()->getLocale()]['consentModal'] = $cookie_translation['consentModal'];
            $cookie_data['language']['translations'][app()->getLocale()]['preferencesModal'] = $cookie_translation['preferencesModal'];
            $cookie_data['language']['translations'][app()->getLocale()]['preferencesModal']['acceptAllBtn'] = $cookie_translation['consentModal']['acceptAllBtn'];
            $cookie_data['language']['translations'][app()->getLocale()]['preferencesModal']['acceptNecessaryBtn'] = $cookie_translation['consentModal']['acceptNecessaryBtn'];
            $cookie_data['language']['translations'][app()->getLocale()]['preferencesModal']['closeIconLabel'] = 'Close Modal';
            $cookie_data['language']['translations'][app()->getLocale()]['preferencesModal']['serviceCounterLabel'] = 'Service|Services';
            $sections = collect($cookie_data['language']['translations'][app()->getLocale()]['preferencesModal']['sections'])->filter(function($item)use($categories){
                return !isset($item['linkedCategory']) || ( isset($item['linkedCategory']) && in_array($item['linkedCategory'], $categories) );
            })->values()->toArray();
            $cookie_data['language']['translations'][app()->getLocale()]['preferencesModal']['sections'] = $sections;
            $cookie_data['disablePageInteraction'] = $cookie->page_interaction === 1;
            $cookie_data['theme'] = $cookie->theme;
        }

        return $cookie_data;
    }
}
