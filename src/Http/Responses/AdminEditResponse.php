<?php

namespace JsPress\JsPress\Http\Responses;

use Illuminate\Contracts\Support\Responsable;
use JsPress\JsPress\Models\Admin;
use General;
class AdminEditResponse implements Responsable{

    public function __construct(private readonly Admin $admin, private readonly array $breadcrumbs)
    {

    }

    public function toResponse($request)
    {
        $roles = General::getRoles();
        $admin = $this->admin;
        $breadcrumbs = $this->breadcrumbs;
        return view('JsPress::admin.admin.edit', compact('admin', 'roles','breadcrumbs'));
    }
}
