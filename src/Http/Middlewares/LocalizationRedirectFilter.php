<?php

namespace JsPress\JsPress\Http\Middlewares;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Closure;

class LocalizationRedirectFilter{

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {

        $params = explode('/', $request->getPathInfo());
        array_shift($params);
        if (\count($params) > 0) {
            $locale = $params[0];
            if (app('jspresslocalization')->checkLocaleInSupportedLocales($locale)) {
                if (app('jspresslocalization')->isHiddenDefault($locale)) {
                    $redirection = app('jspresslocalization')->getNonLocalizedURL();
                    app('session')->reflash();
                    return new RedirectResponse($redirection, 302, ['Vary' => 'Accept-Language']);
                }
            }
        }
        return $next($request);
    }

}
