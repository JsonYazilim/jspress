<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: LaravelLocalizationRoutes.php
 * Author: Json Yazılım
 * Class: LaravelLocalizationRoutes.php
 * Current Username: Erdinc
 * Last Modified: 31.07.2023 19:30
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Middlewares;

use Illuminate\Http\Request;
use Closure;

class LaravelLocalizationRoutes
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        $app = app();

        $routeName = $app['jspresslocalization']->getRouteNameFromAPath($request->getUri());

        $app['jspresslocalization']->setRouteName($routeName);

        return $next($request);
    }
}
