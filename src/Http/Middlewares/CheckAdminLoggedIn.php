<?php

namespace JsPress\JsPress\Http\Middlewares;

use JsPress\JsPress\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class CheckAdminLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(Request): (Response) $next
     * @return Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        if( Auth::guard('admin')->check() ){
            return redirect(RouteServiceProvider::ADMIN);
        }

        return $next($request);
    }
}
