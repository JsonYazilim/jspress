<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PanelMiddleware.php
 * Author: Json Yazılım
 * Class: PanelMiddleware.php
 * Current Username: Erdinc
 * Last Modified: 9.07.2023 20:59
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Middlewares;

use Closure;
use Illuminate\Http\Request;
use JsPress\JsPress\Models\Language;
use Symfony\Component\HttpFoundation\Response;
use JsPressPanel;

class PanelMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure(Request): (Response) $next
     * @return Response
     */
    public function handle(Request $request, Closure $next): Response
    {
        $admin = Auth()->guard('admin')->user();
        if( !is_null($admin->panel_language) ){
            app()->setLocale($admin->panel_language);
        }
        if($request->input('lang')){
            JsPressPanel::setContentLanguage($request->input('lang'));
        }
        $website_langauges = website_languages();
        if(\Session::has('content_language') && !in_array(session('content_language'), $website_langauges->pluck('locale')->toArray())){
            $language = Language::where('status', 1)->where('is_default', 1)->first();
            if($request->input('lang')){
                JsPressPanel::setContentLanguage($language->locale);
                $route = create_language_redirect_url($language->locale);
                return redirect($route, 301);
            }else{
                JsPressPanel::setContentLanguage($language->locale);
            }
        }
        return $next($request);
    }
}
