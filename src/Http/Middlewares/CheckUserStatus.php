<?php

namespace JsPress\JsPress\Http\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Auth;

class CheckUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        if(auth()->check() && (auth()->user()->status == 0)){

            $roles = Role::all()->pluck('name')->toArray();
            $route = 'login';

            if( auth()->user()->hasAnyRole($roles) ){
                $route = 'admin.index';
            }

            Auth::logout();

            $request->session()->invalidate();

            $request->session()->regenerateToken();

            return redirect()->route($route)->with('status_error', __('frontend.user_banned'));

        }

        return $next($request);
    }
}
