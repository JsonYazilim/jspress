<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: TwoFactorVerification.php
 * Author: Json Yazılım
 * Class: TwoFactorVerification.php
 * Current Username: Erdinc
 * Last Modified: 7.09.2024 12:59
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class VerifiedAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param Closure(Request): (Response) $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if( ( auth()->guard('admin')->user()->is_two_factor == 1 && auth()->guard('admin')->user()->verification_code === null) || auth()->guard('admin')->user()->is_two_factor == 0 ){
            return redirect()->route('admin.dashboard');
        }

        return $next($request);
    }
}
