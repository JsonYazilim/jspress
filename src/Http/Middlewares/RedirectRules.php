<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: RedirectRules.php
 * Author: Json Yazılım
 * Class: RedirectRules.php
 * Current Username: Erdinc
 * Last Modified: 1.03.2024 22:04
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Middlewares;

use Closure;
use Illuminate\Http\Request;
use JsPress\JsPress\Models\Redirect;
use Symfony\Component\HttpFoundation\Response;

class RedirectRules
{
    /**
     * Handle an incoming request.
     *
     * @param Closure(Request): (Response) $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $current_url = $request->fullUrl();

        if($current_url !== \Config::get('app.url')){
            $current_full_path = $request->getRequestUri();
            $redirection = Redirect::where('old_url', $current_full_path)->first();
            if($redirection){
                return \Illuminate\Support\Facades\Redirect::to(url($redirection->new_url), $redirection->status_code);
            }
        }

        return $next($request);
    }
}
