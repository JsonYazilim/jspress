<?php

namespace JsPress\JsPress\Http\Middlewares;


use Closure;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use JsPress\JsPress\Services\TranslationService;

class SetLocale
{
    public function handle(Request $request, Closure $next)
    {
        $locale = $request->segment(1);

        $browser_language = substr(request()->server('HTTP_ACCEPT_LANGUAGE'), 0, 2);
        $defaultLocale  = default_language();
        $translationService = new TranslationService();
        if( $translationService->browserRedirect() && $translationService->is_first_request($locale, $browser_language, $defaultLocale) ){
            return new RedirectResponse(url('/'.$browser_language), 302, ['Vary' => 'Accept-Language']);
        }

        if (is_null($locale)) {
            if(!$translationService->hideUrlDefault()){
                return new RedirectResponse(url('/'.$browser_language), 302, ['Vary' => 'Accept-Language']);
            }
            app()->setLocale($defaultLocale);
            return $next($request);
        }
        if (!is_null($locale) && $translationService->hideUrlDefault() && $locale == $defaultLocale) {
            abort(404);
        }
        $locales = \DB::table('languages')->where('status',1)->get()->pluck('locale')->toArray();
        if(!in_array($locale, $locales)){
            abort(404);
        }

        app()->setLocale($locale);
        Session::put('locale', $locale);

        return $next($request);
    }


}
