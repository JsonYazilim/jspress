<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: TwoFactorVerification.php
 * Author: Json Yazılım
 * Class: TwoFactorVerification.php
 * Current Username: Erdinc
 * Last Modified: 7.09.2024 12:59
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class TwoFactorVerification
{
    /**
     * Handle an incoming request.
     *
     * @param Closure(Request): (Response) $next
     */
    public function handle(Request $request, Closure $next): Response
    {

        $admin = Auth()->guard('admin')->user();
        if(( config('jspress.panel.auth.2fa') || $admin->is_two_factor === 1 ) && !is_null($admin->verification_code)){
            return redirect()->route('admin.verify_login');
        }

        return $next($request);
    }
}
