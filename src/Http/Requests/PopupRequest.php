<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PopupRequest.php
 * Author: Json Yazılım
 * Class: PopupRequest.php
 * Current Username: Erdinc
 * Last Modified: 7.05.2024 09:41
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\Rule;

class PopupRequest extends FormRequest
{

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'status' => 0,
            'errors' => $validator->errors(),
            'msg' => __('JsPress::backend.popup.validation_error')
        ], 200));
    }

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => ['required', 'string', 'max:255'],
            'type' => ['required', Rule::in(['lightbox', 'tooltip', 'bar', 'screen'])],
            'place' => [
                Rule::requiredIf(function () {
                    return $this->input('type') == 'tooltip' || $this->input('type') == 'bar';
                }),
                'string',
                'max:10'
            ],
            'is_display_name' => ['required', 'numeric', 'digits:1'],
            'is_content_image' => ['required_if:type,tooltip', 'numeric', 'digits:1'],
            'button_type' => ['required_if:type,bar', 'numeric', 'digits:1'],
            'button_text' => ['required_if:type,screen', 'string', 'max:100'],
            'content' => ['nullable', 'string', 'max:1000'],
            'status' => ['required', 'numeric', 'digits:1'],
            'order' => ['required', 'numeric'],
            'devices' => ['required', 'array'],
            'page_show_type' => ['required', 'numeric', 'digits:1'],
            'languages' => ['required_if:page_show_type,=,1', 'array'],
            'element_ids' => ['required_if:page_show_type,=,1', 'array'],
            'custom_url' => ['required_if:page_show_type,=,3', 'nullable', 'string'],
            'impressions' => ['required','numeric','digits:1'],
            'impression_count' => ['required_unless:impression,in:1,2', 'numeric'],
            'date_type' => ['required','numeric','digits:1'],
            'start_date' => ['required_if:date_type,=,1', 'nullable', 'date'],
            'end_date' => ['required_if:date_type,=,1', 'nullable', 'date'],
            'time_type' => ['required', 'numeric', 'digits:1'],
            'display_time' => ['required_if:time_type,=,1', 'numeric'],
            'conditions' => ['required', 'numeric', 'digits:1'],
            'condition_time' => ['required_if:conditions,=,0', 'numeric'],
            'condition_scroll' => ['required_if:conditions,=,1', 'numeric']
        ];
    }

    public function messages(): array
    {
        return [
            'title.required' => __('JsPress::backend.popup.errors.required'),
            'title.string' => __('JsPress::backend.popup.errors.string'),
            'title.max' => __('JsPress::backend.popup.errors.max', ['max'=>255]),
            'type.required' => __('JsPress::backend.popup.errors.required'),
            'place.required' => __('JsPress::backend.popup.errors.required'),
            'place.string' => __('JsPress::backend.popup.errors.string'),
            'place.max' => __('JsPress::backend.popup.errors.max', ['max'=>255]),
            'is_display_name.required' => __('JsPress::backend.popup.errors.required'),
            'is_display_name.numeric' => __('JsPress::backend.popup.errors.numeric'),
            'is_display_name.digits' => __('JsPress::backend.popup.errors.digits', ['digit' => 1]),
            'is_content_image.required_if' => __('JsPress::backend.popup.errors.required'),
            'is_content_image.numeric' => __('JsPress::backend.popup.errors.numeric'),
            'is_content_image.digits' => __('JsPress::backend.popup.errors.digits', ['digit' => 1]),
            'button_type.required_if' => __('JsPress::backend.popup.errors.required'),
            'button_type.numeric' => __('JsPress::backend.popup.errors.numeric'),
            'button_type.digits' => __('JsPress::backend.popup.errors.digits', ['digit' => 1]),
            'button_text.required_if' => __('JsPress::backend.popup.errors.required'),
            'button_text.string' => __('JsPress::backend.popup.errors.string'),
            'button_text.max' => __('JsPress::backend.popup.errors.max', ['max'=>100]),
            'content.string' => __('JsPress::backend.popup.errors.string'),
            'content.max' => __('JsPress::backend.popup.errors.max', ['max'=>1000]),
            'status.required' => __('JsPress::backend.popup.errors.required'),
            'status.numeric' => __('JsPress::backend.popup.errors.numeric'),
            'status.digits' => __('JsPress::backend.popup.errors.digits', ['digit' => 1]),
            'order.required' => __('JsPress::backend.popup.errors.required'),
            'order.numeric' => __('JsPress::backend.popup.errors.numeric'),
            'devices.required' => __('JsPress::backend.popup.errors.required'),
            'devices.array' => __('JsPress::backend.popup.errors.array'),
            'page_show_type.required' => __('JsPress::backend.popup.errors.required'),
            'page_show_type.numeric' => __('JsPress::backend.popup.errors.numeric'),
            'page_show_type.digits' => __('JsPress::backend.popup.errors.digits', ['digit' => 1]),
            'languages.required_if' => __('JsPress::backend.popup.errors.required'),
            'languages.array' => __('JsPress::backend.popup.errors.array'),
            'element_ids.required_if' => __('JsPress::backend.popup.errors.required'),
            'element_ids.array' => __('JsPress::backend.popup.errors.array'),
            'custom_url.required_if' => __('JsPress::backend.popup.errors.required'),
            'custom_url.string' => __('JsPress::backend.popup.errors.string'),
            'impressions.required' => __('JsPress::backend.popup.errors.required'),
            'impressions.numeric' => __('JsPress::backend.popup.errors.numeric'),
            'impressions.digits' => __('JsPress::backend.popup.errors.digits', ['digit' => 1]),
            'impression_count.required_unless' => __('JsPress::backend.popup.errors.required'),
            'impression_count.numeric' => __('JsPress::backend.popup.errors.numeric'),
            'date_type.required' => __('JsPress::backend.popup.errors.required'),
            'date_type.numeric' => __('JsPress::backend.popup.errors.numeric'),
            'date_type.digits' => __('JsPress::backend.popup.errors.digits', ['digit' => 1]),
            'start_date.required_if' => __('JsPress::backend.popup.errors.required'),
            'start_date.date' => __('JsPress::backend.popup.errors.date'),
            'end_date.required_if' => __('JsPress::backend.popup.errors.required'),
            'end_date.date' => __('JsPress::backend.popup.errors.date'),
            'time_type.required' => __('JsPress::backend.popup.errors.required'),
            'time_type.numeric' => __('JsPress::backend.popup.errors.numeric'),
            'time_type.digits' => __('JsPress::backend.popup.errors.digits', ['digit' => 1]),
            'display_time.required_if' => __('JsPress::backend.popup.errors.required'),
            'display_time.numeric' => __('JsPress::backend.popup.errors.numeric'),
            'conditions.required' => __('JsPress::backend.popup.errors.required'),
            'conditions.numeric' => __('JsPress::backend.popup.errors.numeric'),
            'conditions.digits' => __('JsPress::backend.popup.errors.digits', ['digit' => 1]),
            'condition_time.required_if' => __('JsPress::backend.popup.errors.required'),
            'condition_time.numeric' => __('JsPress::backend.popup.errors.numeric'),
            'condition_scroll.required_if' => __('JsPress::backend.popup.errors.required'),
            'condition_scroll.numeric' => __('JsPress::backend.popup.errors.numeric')
        ];
    }

}
