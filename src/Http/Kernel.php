<?php

namespace JsPress\JsPress\Http;


use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{

    /**
     * The application's route middleware.
     *
     * These middleware may be assigned to groups or used individually.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'admin' => \JsPress\JsPress\Http\Middlewares\CheckAdminLoggedIn::class,
        'js-auth' => \JsPress\JsPress\Http\Middlewares\Authenticate::class,
        'role' => \JsPress\JsPress\Http\Middlewares\RoleMiddleware::class,
        'panel' => \JsPress\JsPress\Http\Middlewares\PanelMiddleware::class,
        'localize' => \JsPress\JsPress\Http\Middlewares\LaravelLocalizationRoutes::class,
        'localizationRedirect' => \JsPress\JsPress\Http\Middlewares\LocalizationRedirectFilter::class,
        '2fa' => \JsPress\JsPress\Http\Middlewares\TwoFactorVerification::class,
        'verified' => \JsPress\JsPress\Http\Middlewares\VerifiedAdmin::class
    ];

}
