<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: CreateAdmin.php
 * Author: Json Yazılım
 * Class: CreateAdmin.php
 * Current Username: Erdinc
 * Last Modified: 13.07.2023 13:28
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Commands;


use Illuminate\Console\Command;
use Illuminate\Support\Facades\Hash;
use JsPress\JsPress\Models\Admin;
use Spatie\Permission\Models\Role;

class CreateAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'create:admin';

    /**
     * @var string
     */
    public string $firstName;
    /**
     * @var string
     */
    public string $lastName;
    /**
     * @var string
     */
    public string $email;
    /**
     * @var string
     */
    public string $password;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'You can easily create your admin credentials with this command';


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->firstName = $this->ask('Your Name');
        $this->lastName = $this->ask('Your Surname');

        while (true) {
            $email = $this->ask('Type email address you will login');

            if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $this->email = $email;
                break;
            } else {
                $this->error('Please type a valid email address!');
            }
        }

        $this->password = $this->secret('Please type a password');
        $this->createAdmin();
        $this->info(PHP_EOL . 'Super Admin is created successfully. You can go "/js-admin" to login your dashboard.' . PHP_EOL);
    }

    /**
     * @return void
     */
    private function createAdmin(): void
    {
        $superAdmin = \Spatie\Permission\Models\Role::where('name', 'Super-Admin')->first();
        if( !$superAdmin ){
            $superAdmin = Role::create([
                'name' => 'Super-Admin',
                'guard_name' => 'admin'
            ]);
        }

        $admin = Admin::create([
            'name' => trim($this->firstName) . ' ' . trim($this->lastName),
            'email' => $this->email,
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => Hash::make($this->password)
        ]);

        $admin->assignRole('Super-Admin');
    }
}
