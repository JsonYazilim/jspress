<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PostController.php
 * Author: Json Yazılım
 * Class: PostController.php
 * Current Username: Erdinc
 * Last Modified: 31.07.2023 13:18
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JsPress\JsPress\Interfaces\PostInterface;
use JsPress\JsPress\Models\Post;
use Illuminate\Support\Facades\Route;
use JsPress\JsPress\Models\PostTypeDetail;

class PostController extends Controller
{

    /**
     * @var PostInterface
     */
    private PostInterface $post_data;
    public function __construct(PostInterface $post_data)
    {
        $this->post_data = $post_data;
    }
    public function post(string $slug)
    {
        $jspress = null;
        $uri_segments = explode('/',Route::current()->uri());
        if(count($uri_segments) > 1){
            $post_type_detail = PostTypeDetail::where('locale', app()->getLocale())->where('slug', $uri_segments[0])->first();
            if( $post_type_detail ){
                $post_type = $post_type_detail->post_type;
                $jspress = $this->post_data->post($slug, 0, $post_type->key);
            }else{
                $jspress = $this->post_data->post($slug);
            }
        }else{
            $jspress = $this->post_data->post($slug);
        }

        if(!is_null($jspress)){
            return view($jspress->view, ['item'=>$jspress]);
        }
        abort(404);
    }

    public function subPost(string $parent_slug, string $slug)
    {

        $jspress = null;
        $parent_post = Post::where('slug', $parent_slug)
            ->whereHas('translation', function($query){
                return $query->where('locale', app()->getLocale())
                    ->where('region', 1)
                    ->whereIn('locale', website_languages()->pluck('locale')->toArray());
            })
            ->where('status', 1)
            ->first();
        if(!$parent_post){
            abort(404);
        }
        $jspress = $this->post_data->post($slug, $parent_post->id);

        if(!is_null($jspress)){
            return view($jspress->view, ['item'=>$jspress]);
        }
        abort(404);
    }

}
