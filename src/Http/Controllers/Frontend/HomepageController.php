<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: HomepageController.php
 * Author: Json Yazılım
 * Class: HomepageController.php
 * Current Username: Erdinc
 * Last Modified: 30.09.2023 00:00
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use JsPress\JsPress\Interfaces\PostInterface;
use General;

class HomepageController extends Controller
{
    private PostInterface $post;

    public function __construct(
        PostInterface $post
    ){
        $this->post = $post;
    }
    public function index(): View
    {

        $jspress = $this->post->homepage();

        return view($jspress->view, ['item'=>$jspress]);
    }
}
