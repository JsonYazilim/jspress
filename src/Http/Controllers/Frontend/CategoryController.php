<?php

namespace JsPress\JsPress\Http\Controllers\Frontend;

use JsPress\JsPress\Interfaces\CategoryInterface;

class CategoryController{

    private CategoryInterface $category_data;

    public function __construct(CategoryInterface $category_data){
        $this->category_data = $category_data;
    }

    public function index(string $slug){

        $segments = request()->segments();
        $category_type = app()->getLocale() == default_language() ? $segments[0] : $segments[1];
        $jspress = null;
        $jspress = $this->category_data->category($slug, $category_type);
        if(!is_null($jspress)){
            return view($jspress->view, ['item'=>$jspress]);
        }
        abort(404);
    }

}
