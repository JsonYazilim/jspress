<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: SitemapController.php
 * Author: Json Yazılım
 * Class: SitemapController.php
 * Current Username: Erdinc
 * Last Modified: 18.04.2024 01:35
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Frontend;

use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use JsPress\JsPress\Interfaces\SitemapInterface;
use JsPress\JsPress\Models\Language;
use JsPress\JsPress\Models\PostType;

class SitemapController extends Controller
{
    private SitemapInterface $sitemap;

    public function __construct(SitemapInterface $sitemap)
    {
        $this->sitemap = $sitemap;
    }

    /**
     * @return Response
     */
    public function index(): Response
    {
        $data = [];
        $language_sitemap_data = [];
        $languages = website_languages();
        foreach($languages as $language){
            $language_sitemap_data[] = $this->getSitemapByLocale($language->locale);
        }
        foreach($language_sitemap_data as $language_data){
            foreach($language_data as $sitemap_data){
                $data[] = $sitemap_data;
            }
        }

        return response()->view('JsPress::frontend.sitemap.index', ['data'=>$data])->header('Content-Type', 'text/xml');
    }

    /**
     * @param $post_type
     * @param $locale
     * @return Response
     */
    public function sitemaps($locale, $post_type): Response
    {
        $sitemaps = $this->sitemap->setPostType($post_type)->setLocale($locale)->getPosts();

        return response()->view('JsPress::frontend.sitemap.sitemap', ['sitemaps'=>$sitemaps])->header('Content-Type', 'text/xml');
    }

    /**
     * @param $locale
     * @return array
     */
    private function getSitemapByLocale($locale):array
    {
        $language = $this->isLanguage($locale);
        return $this->getPostTypeSitemaps($language->locale);
    }

    /**
     * @param $locale
     * @return array
     */
    private function getPostTypeSitemaps($locale): array
    {
        $sitemaps = [];
        $post_types = PostType::select('post_types.*', 'post_type_details.locale')
            ->where('post_types.is_url', 1)
            ->where('post_types.status', 1)
            ->whereNotIn('post_types.key', ['event'])
            ->join('post_type_details', function($join)use($locale){
                $join->on('post_type_details.post_type_id', '=', 'post_types.id')->where('post_type_details.locale', $locale)->where('post_type_details.model', 'JsPress\JsPress\Models\Post');
            })
            ->get();
        foreach($post_types as $post_type){
            $sitemaps[] = route('sitemap.post_type', ['slug'=>$post_type->key, 'locale'=>$locale]);
        }
        return $sitemaps;
    }

    /**
     * @param $locale
     * @return mixed
     */
    private function isLanguage($locale): mixed
    {
        return Language::where('locale', $locale)->first();
    }

}
