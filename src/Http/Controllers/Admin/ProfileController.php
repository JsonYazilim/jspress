<?php

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use JsPress\JsPress\Http\Responses\AdminEditResponse;
use JsPress\JsPress\Models\Admin;
use Spatie\Permission\Models\Role;

class ProfileController extends Controller
{

    /**
     * @return AdminEditResponse
     */
    public function index(): AdminEditResponse
    {
        $admin = Auth()->guard('admin')->user();
        $breadcrumbs = [
            Route('admin.profile.index') => 'Profilim'
        ];
        return new AdminEditResponse($admin, $breadcrumbs);
    }

}
