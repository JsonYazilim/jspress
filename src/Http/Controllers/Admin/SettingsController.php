<?php

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use JsPress\JsPress\DataSources\JsPressPanelData;
use JsPress\JsPress\Models\Setting;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(JsPressPanelData $jspressPanel): View
    {
        if( !auth()->user()->hasPermissionTo('view settings') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }
        $breadcrumbs = [
            Route('admin.setting.index') => __('JsPress::backend.site_settings')
        ];
        $tabs = collect(config('jspress_settings'))->toArray()['tabs'];
        $jspressPanel->meta['title'] = __('JsPress::backend.site_settings');
        return view('JsPress::admin.settings.index', compact('breadcrumbs', 'tabs', 'jspressPanel'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $settings = $request->input('settings');

        if( $settings ){
            foreach( $settings as $key => $setting ){
                Setting::updateOrCreate(
                    [
                        'key' => $key
                    ],
                    [
                        'value' => $setting
                    ]
                );
            }
        }
        Cache::forget('website_settings');
        return redirect()->back()->with('success', __('JsPress::backend.settings_updated'));
    }


}
