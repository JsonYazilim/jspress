<?php

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use JsPress\JsPress\DataSources\JsPressPanelData;
use JsPress\JsPress\Exceptions\PostTypeNotFoundException;
use JsPress\JsPress\Interfaces\CategoryAdminInterface;
use JsPress\JsPress\Models\Category;
use JsPress\JsPress\Models\CategoryDetail;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use JsPressPanel;

class CategoryController extends Controller
{

    /**
     * @var object|null
     */
    private ?object $type;
    private CategoryAdminInterface $category_admin;
    private string $language;
    private array $active_languages;
    private array $other_languages = [];

    /**
     * @param Request $request
     * @param CategoryAdminInterface $category_admin
     * @throws PostTypeNotFoundException
     */
    public function __construct(Request $request, CategoryAdminInterface $category_admin)
    {
        $this->category_admin = $category_admin;
        if(!$request->input('post-type')){
            throw new PostTypeNotFoundException(__('JsPress::backend.post_type.not_found'));
        }else{
            $key = $request->input('post-type');
            $this->middleware(function ($request, $next)use($key) {
                $this->type = $this->category_admin->setPostType($key);
                if(is_null($this->type)){
                    throw new PostTypeNotFoundException(__('JsPress::backend.post_type.null_error'));
                }
                return $next($request);
            });
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param JsPressPanelData $jspressPanel
     * @return View
     */
    public function index(Request $request, JsPressPanelData $jspressPanel): View
    {
        if( !auth()->user()->hasPostPermission('category', $this->type->id) && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403, __('JsPress::backend.permission_error'));
        }
        $jspressPanel->meta['title'] = __('JsPress::backend.categories');
        $breadcrumbs = [
            Route('admin.post.index', ['post-type' => $this->type->key]) => __js('post_type', JsPressPanel::postTypeLangKey($this->type, 'breadcrumb'), $this->type->name),
            Route('admin.category.index', ['post-type' => $this->type->key]) => __js('post_type', JsPressPanel::postTypeLangKey($this->type, 'category'), $this->type->plural_name)
        ];
        $current_language = $this->type->locale;
        $active_languages = $this->type->active_languages;
        if(!in_array($current_language, $active_languages)){
            $button = [
                'url' => route('admin.post_type.edit', ['post_type' => $this->type->id]),
                'text' => __('JsPress::backend.go_to_post_type')
            ];
            $display_language = display_language($current_language);
            return view('JsPress::admin.category.404', compact('breadcrumbs', 'display_language', 'button'));
        }
        $categories = $this->category_admin->getCategories();
        $related_categories = $this->category_admin->getOtherLanguagesCategories();
        $category_type = $this->type;
        $post_fields = $this->category_admin->getPostField();

        return view('JsPress::admin.category.index', compact('jspressPanel','breadcrumbs', 'category_type','categories', 'current_language', 'active_languages', 'related_categories', 'post_fields'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        if( !auth()->user()->hasPostPermission('category', $this->type->id) && !Auth()->user()->hasRole('Super-Admin') ){
            return redirect()->back()->with('error', __('JsPress::backend.permission_error'));
        }
        $data = $request->input();
        $source_locale = null;
        $transaction_id = null;
        if(isset($data['current_transaction_id']) && !isset($data['transaction_id'])){
            $default_category = $this->category_admin->getCategory($data['current_transaction_id']);
            $source_locale = $default_category['source_locale'];
            $transaction_id = $default_category['transaction_id'];
        }else if(isset($data['transaction_id'])){
            $default_category = $this->category_admin->getCategory($data['transaction_id']);
            $source_locale = $default_category['locale'];
            $transaction_id = $default_category['transaction_id'];
        }

        $this->category_admin->setId($data['id'] ?? null)
            ->setCategoryId($data['category_id'] ?? 0)
            ->setTitle($data['title'])
            ->setSlug($data['slug'] ?? null)
            ->setContent($data['content'])
            ->setAuthor(auth()->user()->id)
            ->setExtras($data['extras'] ?? null)
            ->setPublishDate(\Carbon\Carbon::now()->format('Y-m-d H:i:s'))
            ->setStatus($data['status'])
            ->setOrder($data['order'])
            ->setLocale($this->type->locale)
            ->setRegion(1)
            ->setTransactionId($transaction_id ?? null)
            ->setSourceLocale($source_locale ?? null)
            ->save();
        if(isset($data['id'])){
            $message = __('JsPress::backend.category_created_success');
        }else{
            $message = __('JsPress::backend.category_updated_success');
        }
        return redirect()->back()->with('success', __('JsPress::backend.category_created_success'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @param JsPressPanelData $jspressPanel
     * @return View|RedirectResponse
     */
    public function edit(int $id, JsPressPanelData $jspressPanel): View|RedirectResponse
    {
        if( !auth()->user()->hasPostPermission('category', $this->type->id) && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403, __('JsPress::backend.permission_error'));
        }
        $jspressPanel->meta['title'] = __('JsPress::backend.category_edit');
        $breadcrumbs = [
            Route('admin.post.index', ['post-type' => $this->type->key]) => __js('post_type', JsPressPanel::postTypeLangKey($this->type, 'breadcrumb'), $this->type->name),
            Route('admin.category.index', ['post-type' => $this->type->key]) => __js('post_type', JsPressPanel::postTypeLangKey($this->type, 'category'), $this->type->plural_name),
            request()->fullUrl() =>  __('JsPress::backend.category_edit')
        ];

        $current_category = $this->category_admin->getCategory($id);
        if($current_category['locale'] != $this->type->locale){
            return redirect()->route('admin.category.index', ['post-type' => $this->type->key]);
        }
        $related_categories = $this->category_admin->getOtherLanguagesCategories();
        $categories = $this->category_admin->getCategories($current_category['id']);
        $category_translations = [];
        if(!is_null($current_category['transaction_id'])){
            $category_translations = $this->category_admin->getCategoryTranslation($current_category['transaction_id']);
        }
        $category_type = $this->type;

        $post_fields = $this->category_admin->getPostField();
        return view('JsPress::admin.category.edit', compact('jspressPanel','breadcrumbs', 'current_category', 'related_categories', 'categories', 'category_translations', 'category_type', 'post_fields'));
    }

    public function deleteTranslationRelation(Request $request): JsonResponse
    {
        if($request->input('element_id')){
            $category_id = $request->input('element_id');
            $transaction_id = $request->input('transaction_id');
            $this->category_admin->setLocale($this->type->locale)
                ->setTransactionId($transaction_id)
                ->deleteTranslationRelation($category_id);
        }
        return response()->json([
            'status' => 1
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request): JsonResponse
    {
        if( !auth()->user()->hasPostPermission('delete', $this->type->id) && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('backend.permission_error')
            ]);
        }
        $category_id = $request->input('id');
        $category = $this->category_admin->getCategory($category_id);
        $this->category_admin->setId($category['id'])->setTransactionId($category['transaction_id'])->delete();
        $response = [
            'status' => 1,
            'msg' => __('JsPress::backend.category_delete_success')
        ];
        return response()->json($response);
    }

}
