<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: MenuController.php
 * Author: Json Yazılım
 * Class: MenuController.php
 * Current Username: Erdinc
 * Last Modified: 5.10.2023 20:22
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Cache;
use JsPress\JsPress\DataSources\JsPressPanelData;
use JsPress\JsPress\Interfaces\MenuInterface;
use JsPressPanel;

class MenuController extends Controller
{

    private MenuInterface $menu_admin;


    public function __construct(MenuInterface $menu_admin)
    {
        $this->menu_admin = $menu_admin;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(JsPressPanelData $jspressPanel)
    {
        if( !auth()->user()->hasPermissionTo('view menu') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }
        $jspressPanel->meta['title'] = 'Menüler';
        $breadcrumbs = [
            Route('admin.menu.index')  => __('JsPress::backend.menus')
        ];
        $menus = $this->menu_admin->setLocale(JsPressPanel::contentLocale())->getMenuData();
        $data = [
            'jspressPanel' => $jspressPanel,
            'breadcrumbs' => $breadcrumbs,
            'menus' => $menus
        ];
        return view('JsPress::admin.menu.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(JsPressPanelData $jspressPanel): View
    {
        if( !auth()->user()->hasPermissionTo('add menu') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        $jspressPanel->meta['title'] = 'Yeni Menü Ekle';
        $breadcrumbs = [
            Route('admin.menu.index')  => __('JsPress::backend.menus'),
            Route('admin.menu.create') => __('JsPress::backend.add_new_menu')
        ];
        $menu_options = $this->menu_admin->getMenuOptions();
        $menu_translations = null;
        $data = [
            'jspressPanel' => $jspressPanel,
            'breadcrumbs' => $breadcrumbs,
            'menu_options' => $menu_options,
            'menu_translations' => $menu_translations
        ];
        return view('JsPress::admin.menu.create', $data);

    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = $request->input();
        $menu = $this->menu_admin->setId($data['id'] ?? null)
            ->setTitle($data['title'])
            ->setSlug($data['slug'])
            ->setLocale($data['locale'])
            ->setSourceLocale($data['source_locale'] ?? null)
            ->setTransactionId($data['transaction_id'] ?? null)
            ->setData($data['data'] ?? [])
            ->save();
        if(isset($data['id'])){
            $message = __('JsPress::backend.menu_update_succes');
        }else{
            $message = __('JsPress::backend.menu_added_succes');
        }
        $key = 'menu_'.$data['slug'].'_'.$data['locale'];
        Artisan::call('cache:clear');
        return response()->json([
            'status' => 1,
            'route' => Route('admin.menu.edit', ['id'=>$menu->getId(), 'lang'=>$data['locale']]),
            'msg' => $message
        ]);
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id, JsPressPanelData $jspressPanel)
    {
        if( !auth()->user()->hasPermissionTo('edit menu') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        $jspressPanel->meta['title'] = 'Yeni Menü Ekle';

        $menu = $this->menu_admin->setId($id)->get();
        $breadcrumbs = [
            Route('admin.menu.index')  => __('JsPress::backend.menus'),
            Route('admin.menu.edit', ['id'=>$id]) => __('JsPress::backend.edit_menu', ['title'=>$menu['title']])
        ];
        if(is_null($menu)){
            abort(404);
        }
        $menu_translations = null;
        if( JsPressPanel::contentLocale() != default_language() ){
            $menu_translations = $this->menu_admin->setLocale(default_language())->setTransactionId($menu['translation']['transaction_id'])->getPostByTransactionId();
        }
        $menu_options = $this->menu_admin->getMenuOptions();
        $data = [
            'jspressPanel' => $jspressPanel,
            'breadcrumbs' => $breadcrumbs,
            'menu_options' => $menu_options,
            'menu_translations' => $menu_translations,
            'menu' => $menu
        ];

        return view('JsPress::admin.menu.create', $data);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request): \Illuminate\Http\JsonResponse
    {
        if( !auth()->user()->hasPermissionTo('delete menu') && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('JsPress::backend.permission_error')
            ]);
        }

        $menu_id = $request->input('id');
        $lang = $request->input('lang');
        $this->menu_admin->setId($menu_id)->setLocale($lang)->delete();
        $response = [
            'status' => 1,
            'msg' => __('JsPress::backend.menu_delete_successfully')
        ];
        return response()->json($response);
    }
}
