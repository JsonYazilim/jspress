<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: RoleController.php
 * Author: Json Yazılım
 * Class: RoleController.php
 * Current Username: Erdinc
 * Last Modified: 14.05.2023 11:38
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use JsPress\JsPress\DataSources\JsPressPanelData;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{


    /**
     * @param JsPressPanelData $jspressPanel
     * @return View|Application|Factory
     */
    public function index(JsPressPanelData $jspressPanel): View|Application|Factory
    {
        if( !auth()->user()->hasPermissionTo('view roles') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }
        $roles = Role::all();
        $permissions = \DB::table('permission_group')
            ->select([
                'permission_group.group_id as group_id',
                'permission_group.group_name',
                'permission_group.title as title',
                'permission_group.description as description',
                'permissions.id as permission_id',
                'permissions.name as permission'
            ])
            ->join('permissions', function($join){
                $join->on('permissions.id', '=', 'permission_group.permission_id');
            })
            ->get()
            ->groupBy('group_id');
        $breadcrumbs = [
            'javascript:void(0);' => __('JsPress::backend.pages.roles.title'),
            Route('admin.role.list') => __('JsPress::backend.pages.roles.list.title')
        ];
        $jspressPanel->meta['title'] = __('JsPress::backend.pages.roles.list.title');
        return view('JsPress::admin.roles.index', compact('roles', 'breadcrumbs', 'permissions', 'jspressPanel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        if( !auth()->user()->hasPermissionTo('add role') && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('backend.permission_error')
            ]);
        }

        $request->validate(
            [
                'role_name' => ['required']
            ],
            [
                'role_name.required' => 'Role adı alanı zorunludur.'
            ]
        );
        $role_name = $request->input('role_name');
        $checkRole = Role::where('name', $role_name)->first();
        if($checkRole){
            return response()->json([
                'status' => 0,
                'msg' => 'Belirtmiş olduğunuz rol adı zaten sistemde mevcuttur. Lütfen farklı bir rol adı giriniz.'
            ]);
        }

        $role = Role::create([
            'name' => $role_name,
            'guard_name' => 'admin'
        ]);
        $permission_data = [];
        if($request->input('permissions')){
            $permissions = $request->input('permissions');

            foreach( $permissions as $key => $permission ){
                $permission_data[] = $key;
            }
        }
        $role->syncPermissions($permission_data);
        return response()->json([
            'status' => 1,
            'msg' => $role->name.' rolünün yetkileri başarılı şekilde güncellendi'
        ]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function update(Request $request): JsonResponse
    {
        if( !auth()->user()->hasPermissionTo('edit role') && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('backend.permission_error')
            ]);
        }

        if( $request->input('role_id') ){
            $role = Role::find($request->input('role_id'));
            $permission_data = [];
            if($request->input('permissions')){
                $permissions = $request->input('permissions');

                foreach( $permissions as $key => $permission ){
                    $permission_data[] = $key;
                }
            }
            $role->syncPermissions($permission_data);
            $role->name = $request->input('role_name');
            $role->save();
            return response()->json([
                'status' => 1,
                'msg' => $role->name.' rolünün yetkileri başarılı şekilde güncellendi'
            ]);
        }else{
            return response()->json([
                'status' => 0,
                'msg' => 'Bir hata meydana geldi'
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request): JsonResponse
    {
        $role = Role::find($request->input('role_id'));
        $role->delete();
        return response()->json([
            'status' => 1,
            'msg' => 'Belirtilen rol başarılı şekilde kaldırıldı.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getRolePermissions(Request $request): JsonResponse
    {
        if( $request->input('role_id') ){
            $role = Role::find($request->input('role_id'));
            $permissions = \DB::table('permission_group')
                ->select([
                    'permission_group.group_id as group_id',
                    'permission_group.group_name',
                    'permission_group.title as title',
                    'permission_group.description as description',
                    'permissions.id as permission_id',
                    'permissions.name as permission'
                ])
                ->join('permissions', function($join){
                    $join->on('permissions.id', '=', 'permission_group.permission_id');
                })
                ->get()
                ->groupBy('group_id');
            $render = view('JsPress::admin.roles.modal', compact('role', 'permissions'))->render();
            return response()->json([
                'status' => 1,
                'render' => $render
            ]);
        }

        return response()->json([
            'status' => 0,
            'msg' => 'Bir hata meydana geldi'
        ]);
    }

}
