<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PostTypeController.php
 * Author: Json Yazılım
 * Class: PostTypeController.php
 * Current Username: Erdinc
 * Last Modified: 22.07.2023 18:37
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use JsPress\JsPress\Helpers\JsPressPanel;
use JsPress\JsPress\Interfaces\PostTypeInterface;
use Illuminate\Http\RedirectResponse;
use JsPress\JsPress\Models\PostType;
use Illuminate\Contracts\View\View;

class PostTypeController extends Controller
{

    /**
     * @var PostTypeInterface
     */
    private PostTypeInterface $post_type;


    /**
     * Create a new interface instance.
     * MediaInterface constructor
     *
     * @param PostTypeInterface $post_type
     */
    public function __construct(PostTypeInterface $post_type)
    {
        $this->post_type = $post_type;
    }


    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        if( !auth()->user()->hasPermissionTo('view post_types') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        $post_types = $this->post_type->list($request);
        $breadcrumbs = [
            Route('admin.post_type.index') => __('JsPress::backend.post_type.title')
        ];
        return view('JsPress::admin.post-type.index', compact('breadcrumbs', 'post_types'));
    }


    /**
     * @return View
     */
    public function create(): View
    {
        if( !auth()->user()->hasPermissionTo('add post_type') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        $breadcrumbs = [
            'javascript:void(0);' => __('JsPress::backend.post_type.title'),
            Route('admin.post_type.add') => __('JsPress::backend.post_type.add')
        ];
        $permission_data = $this->post_type->permissionElements();
        return view('JsPress::admin.post-type.add', compact('breadcrumbs', 'permission_data'));
    }

    /**
     * Store post type information.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $post_type = $request->except(['_token']);
        $edit = false;
        $data = [
            'details' => $post_type['post_type']['details'],
            'name' => $post_type['post_type']['name'],
            'key' => $post_type['post_type']['key'],
            'order' => $post_type['post_type']['order'],
            'icon' => $post_type['post_type']['icon'] ?? '<i class="fas fa-list-ul fs-4"></i>',
            'is_category' => isset($post_type['is_category']) ? 1:0,
            'is_category_url' => isset($post_type['is_category_url']) ? 1:0,
            'is_editor' => isset($post_type['is_editor']) ? 1:0,
            'is_searchable' => isset($post_type['is_searchable']) ? 1:0,
            'parent_id' => $post_type['parent_id'] ?? 0,
            'is_terms' => isset($post_type['is_terms']) ? 1:0,
            'is_image' => isset($post_type['is_image']) ? 1:0,
            'is_seo' => isset($post_type['is_seo']) ? 1:0,
            'is_url' => isset($post_type['is_url']) ? 1:0,
            'is_menu' => isset($post_type['is_menu']) ? 1:0,
            'is_segment_disable' => (isset($post_type['is_menu']) && $post_type['is_menu'] == 1) && isset($post_type['is_segment_disable']) ? 1 : 0
        ];

        if(isset($post_type['post_type_id'])){
            $postTypeModel = PostType::find($post_type['post_type_id']);
            $edit = true;
        }else{
            $postTypeModel = new PostType();
        }
        if( $this->post_type->is_exist($data['key']) && !$edit ){
            return response()->json([
                'status' => 0,
                'msg' => 'Kullanmış olduğunuz key değerine sahip başka bir yazı tipi bulunmaktadır.'
            ]);
        }
        $this->post_type->save($postTypeModel, $data)->setPermissions($request->permissions ?? []);
        if($edit){
            return response()->json([
                'status' => 1,
                'msg' => __('JsPress::backend.post_type.add_success_msg'),
                'redirect' => null
            ]);
        }
        return response()->json([
            'status' => 1,
            'msg' => __('JsPress::backend.post_type.add_success_msg'),
            'redirect' => route('admin.post_type.index')
        ]);
    }


    /**
     * @param PostType $postType
     * @return View
     */
    public function edit(PostType $postType): View
    {
        if( !auth()->user()->hasPermissionTo('edit post_type') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }
        $breadcrumbs = [
            'javascript:void(0);' => __('JsPress::backend.post_type.title'),
            Route('admin.post_type.edit', ['post_type' => $postType->id]) => __('JsPress::backend.post_type.edit')
        ];
        $permission_data = $this->post_type->permissionElements();
        $permission_values = $this->post_type->getPermissionsValues($postType);
        return view('JsPress::admin.post-type.edit', compact('breadcrumbs', 'postType', 'permission_data', 'permission_values'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request): JsonResponse
    {
        $id = $request->id;
        $postType = PostType::find($id);
        $postType->delete();
        JsPressPanel::clearRoute();
        return response()->json([
            'status' => 1,
            'msg' => __('JsPress::backend.post_type.success_delete_post_type')
        ]);
    }

    /**
     * Change status of specified post type.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function toggleStatus(Request $request): JsonResponse
    {
        $id = $request->id;
        $status = $request->status;
        PostType::where('id', $id)->update(['status'=>$status]);
        return response()->json([
            'status' => 1,
            'msg' => __('JsPress::backend.post_type.success_update_status')
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function validateKey(Request $request): JsonResponse
    {
        $data = $request->post_type;
        $model_id = $request->model_id;
        return response()->json([
            'valid' => !$this->post_type->validateKey($data['key'], $model_id)
        ]);
    }
}
