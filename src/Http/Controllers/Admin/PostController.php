<?php

namespace JsPress\JsPress\Http\Controllers\Admin;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use JsPress\JsPress\DataSources\Admin\CategoryDS;
use App\Http\Controllers\Controller;
use JsPress\JsPress\DataSources\JsPressPanelData;
use JsPress\JsPress\Exceptions\PostTypeNotFoundException;
use JsPress\JsPress\Interfaces\PostAdminInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use JsPress\JsPress\Models\Post;
use JsPressPanel;


class PostController extends Controller
{

    /**
     * @var object|null
     */
    private ?object $type;

    /**
     * @var PostAdminInterface
     */
    private PostAdminInterface $post_admin;


    /**
     * @param Request $request
     * @param PostAdminInterface $post_admin
     * @throws PostTypeNotFoundException
     */
    public function __construct(Request $request, PostAdminInterface $post_admin)
    {
        $this->post_admin = $post_admin;
        if(!$request->input('post-type')){
            throw new PostTypeNotFoundException(__('JsPress::backend.post_type.not_found'));
        }else{
            $key = $request->input('post-type');
            $this->middleware(function ($request, $next)use($key) {
                $this->type = $this->post_admin->setPostType($key);
                if(is_null($this->type)){
                    throw new PostTypeNotFoundException(__('JsPress::backend.post_type.null_error'));
                }
                $this->type->className = $classname = "App\\Http\\Controllers\\Panel\\".ucfirst(\Str::slug($this->type->key, ''))."Controller";
                return $next($request);
            });
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param JsPressPanelData $jspressPanel
     * @return Application|Factory|View|\Illuminate\Foundation\Application|mixed
     */
    public function index(Request $request, JsPressPanelData $jspressPanel): mixed
    {
        if( $this->type->post_type_id == 0 ){
            if( !auth()->user()->hasPostPermission('view', $this->type->id) && !Auth()->user()->hasRole('Super-Admin') ){
                abort(403);
            }
        }
        $jspressPanel->meta['title'] = __js('post_type', JsPressPanel::postTypeLangKey($this->type, 'breadcrumb'), __('JsPress::backend.post.title', ['name' => $this->type->name]));
        if($request->input('post_id')){
            $post_id = $request->input('post_id');
            $post = $this->post_admin->setId($post_id)->getPostData();
            $breadcrumbs = [
                Route('admin.post.index', ['post-type' => $this->type->parent->key]) => __js('post_type', JsPressPanel::postTypeLangKey($this->type->parent, 'breadcrumb'), $this->type->parent->name),
                'javascript:void(0);' => $post['title'],
                Route('admin.post.index', ['post-type' => $this->type->key, 'post_id'=>$post_id]) => __js('post_type', JsPressPanel::postTypeLangKey($this->type, 'breadcrumb'), $this->type->name),
            ];
        }else{
            $breadcrumbs = [
                Route('admin.post.index', ['post-type' => $this->type->key]) => __js('post_type', JsPressPanel::postTypeLangKey($this->type, 'breadcrumb'), $this->type->name),
            ];
        }
        $post_type = $this->type;
        $current_language = $post_type->locale;
        $active_languages = $post_type->active_languages;
        if(!in_array($current_language, $active_languages) && $this->type->key != 'page'){
            $button = [
                'url' => route('admin.post_type.edit', ['post_type' => $this->type->id]),
                'text' => __('JsPress::backend.go_to_post_type')
            ];
            $display_language = display_language($current_language);
            return view('JsPress::admin.posts.404', compact('breadcrumbs','post_type', 'display_language', 'button'));
        }
        $post_data = $this->post_admin->getTable($request->input());

        $data = [
            'jspressPanel' => $jspressPanel,
            'breadcrumbs' =>$breadcrumbs,
            'post_type' => $post_type,
            'post_data' => $post_data
        ];

        if(method_exists($this->type->className,'index')){
            $class = new $this->type->className();
            return $class->index($data, $request);
        }
        return view('JsPress::admin.posts.index', $data);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function table(Request $request): JsonResponse
    {
        $table = $this->post_admin->getTable($request->input());
        return response()->json($table);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @param JsPressPanelData $jspressPanel
     * @return Application|Factory|View|\Illuminate\Foundation\Application|mixed
     */
    public function create(Request $request, JsPressPanelData $jspressPanel): mixed
    {
        if( !auth()->user()->hasPostPermission('add', $this->type->id) && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }
        $jspressPanel->meta['title'] = __('JsPress::backend.add_new');
        if($request->input('post_id')){
            $post_id = $request->input('post_id');
            $post = $this->post_admin->setId($post_id)->getPostData();
            $breadcrumbs = [
                Route('admin.post.index', ['post-type' => $this->type->parent->key]) => __js('post_type', JsPressPanel::postTypeLangKey($this->type->parent, 'breadcrumb'), $this->type->parent->name),
                'javascript:void(0);' => $post['title'],
                Route('admin.post.index', ['post-type' => $this->type->key, 'post_id'=>$post_id]) => __js('post_type', JsPressPanel::postTypeLangKey($this->type, 'breadcrumb'), $this->type->name),
                url()->full() => __('JsPress::backend.add_new')
            ];
        }else{
            $breadcrumbs = [
                Route('admin.post.index', ['post-type' => $this->type->key]) => __js('post_type', JsPressPanel::postTypeLangKey($this->type, 'breadcrumb'), $this->type->name),
                url()->full() => __('JsPress::backend.add_new')
            ];
        }

        $data = $this->post_admin->prepare($this->type);
        $post_fields = $this->post_admin->getPostField();
        $post_translation = null;
        if($request->input('trid')){
            $post_translation = $this->post_admin->setTransactionId($request->input('trid'))->setLocale($request->input('source_locale'))->getPostByTransactionId();
        }
        $data = [
            'jspressPanel' => $jspressPanel,
            'breadcrumbs' =>$breadcrumbs,
            'data' => $data,
            'post_translation' => $post_translation,
            'post_fields' => $post_fields
        ];
        if(method_exists($this->type->className,'create')){
            $class = new $this->type->className();
            return $class->create($data, $request);
        }
        return view('JsPress::admin.posts.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $data = $request->input();
        try{
            $this->post_admin->setTitle($data['title'])
                ->setSlug($data['slug'] ?? null)
                ->setId($data['id'] ?? null)
                ->setPostId($data['post_id'] ?? 0)
                ->setContent($data['content'] ?? null)
                ->setAuthor($data['author'] ?? auth()->user()->id)
                ->setExtras($data['extras'] ?? null)
                ->setSeo($data['seo'] ?? null)
                ->setCategories($data['categories'] ?? [])
                ->setPublishDate($data['publish_date'] ?? \Carbon\Carbon::now()->format('Y-m-d H:i:s'))
                ->setStatus($data['status'])
                ->setOrder($data['order'])
                ->setLocale($data['language'])
                ->setRegion(1)
                ->setTransactionId($data['transaction_id'] ?? null)
                ->setSourceLocale($data['source_locale'] ?? null)
                ->setIsHomepage($data['is_homepage'] ?? 0)
                ->setView($data['view'] ?? null)
                ->save();
            $route_url = [
                'id' => $this->post_admin->getId(),
                'post-type' => $data['post-type'],
                'lang' => $this->post_admin->getLocale()
            ];
            if(isset($data['post_id'])){
                $route_url['post_id'] = $data['post_id'];
            }
            return response()->json([
                'status' => 1,
                'route' => Route('admin.post.edit', $route_url),
                'msg' => isset($data['id']) ? __('JsPress::backend.post_updated_success') : __('JsPress::backend.post_created_success')
            ]);
        }catch (\Exception $e){
            return response()->json([
                'status' => 0,
                'msg' => $e->getMessage()
            ]);
        }
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @param Request $request
     * @param JsPressPanelData $jspressPanel
     * @return \Illuminate\View\View
     */
    public function edit(int $id, Request $request, JsPressPanelData $jspressPanel): View
    {
        if( !auth()->user()->hasPostPermission('edit', $this->type->id) && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }
        $jspressPanel->meta['title'] = __('JsPress::backend.post_edit', ['no' => $id]);
        if(!is_null($this->type->parent)){
            $post_id = $request->input('post_id');
            $parent_post = $this->post_admin->setId($post_id)->getPostData();
            $breadcrumbs = [
                Route('admin.post.index', ['post-type' => $this->type->parent->key]) => __js('post_type', JsPressPanel::postTypeLangKey($this->type->parent, 'breadcrumb'), $this->type->parent->name),
                'javascript:void(0);' => $parent_post['title'],
                Route('admin.post.index', ['post-type' => $this->type->key, 'post_id' => request()->input('post_id')]) => __js('post_type', JsPressPanel::postTypeLangKey($this->type, 'breadcrumb'), $this->type->name),
                url()->full() => __('JsPress::backend.post_edit', ['no' => $id])
            ];
        }else{
            $breadcrumbs = [
                Route('admin.post.index', ['post-type' => $this->type->key]) => __js('post_type', JsPressPanel::postTypeLangKey($this->type, 'breadcrumb'), $this->type->name),
                url()->full() => __('JsPress::backend.post_edit', ['no' => $id])
            ];
        }

        $data = $this->post_admin->prepare($this->type);
        $post = $this->post_admin->setId($id)->getPostData();

        $post_fields = $this->post_admin->getPostField($post);
        $post_translation = null;
        if( $this->type->locale != default_language() ){
            $post_translation = $this->post_admin->setTransactionId($post['translation']['transaction_id'])->setLocale(default_language())->getPostByTransactionId();
        }

        $data = [
            'jspressPanel' => $jspressPanel,
            'breadcrumbs' =>$breadcrumbs,
            'post' => $post,
            'data' => $data,
            'post_translation' => $post_translation,
            'post_fields' => $post_fields
        ];
        if(method_exists($this->type->className,'edit')){
            $class = new $this->type->className();
            return $class->edit($data, $request);
        }
        return view('JsPress::admin.posts.create', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request): JsonResponse
    {
        if( !auth()->user()->hasPostPermission('delete', $this->type->id) && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('JsPress::backend.permission_error')
            ]);
        }
        $this->post_admin->setId($request->input('id'))->destroy();
        $response = [
            'status' => 1,
            'msg' => __('JsPress::backend.post_delete_success')
        ];
        return response()->json($response);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param JsPressPanelData $jspressPanel
     * @return \Illuminate\View\View
     */
    public function trash(Request $request, JsPressPanelData $jspressPanel): View
    {
        if( $this->type->post_type_id == 0 ){
            if( !auth()->user()->hasPostPermission('trash', $this->type->id) && !Auth()->user()->hasRole('Super-Admin') ){
                abort(403);
            }
        }
        $jspressPanel->meta['title'] = __('JsPress::backend.trash');
        $breadcrumbs = [
            Route('admin.post.index', ['post-type' => $this->type->key]) => __js('post_type', JsPressPanel::postTypeLangKey($this->type, 'breadcrumb'), __('JsPress::backend.post.title', ['name' => $this->type->name])),
            url()->full() => __('JsPress::backend.trash')
        ];
        $post_type = $this->type;
        $current_language = $post_type->locale;
        $active_languages = $post_type->active_languages;
        if(!in_array($current_language, $active_languages)){
            $button = [
                'url' => route('admin.post_type.edit', ['post_type' => $this->type->id]),
                'text' => __('JsPress::backend.go_to_post_type')
            ];
            $display_language = display_language($current_language);
            return view('JsPress::admin.posts.404', compact('breadcrumbs','post_type', 'display_language', 'button'));
        }
        $post_data = $this->post_admin->getTable($request->input(), true);

        return view('JsPress::admin.posts.trash', compact('jspressPanel','breadcrumbs', 'post_type', 'post_data'));
    }

    /**
     * Force remove the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function delete(Request $request): JsonResponse
    {
        if( !auth()->user()->hasPostPermission('delete', $this->type->id) && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('JsPress::backend.permission_error')
            ]);
        }
        $post_id = $request->input('id');
        $lang = $request->input('lang');
        $post = $this->post_admin->setId($post_id)->getPost();
        $this->post_admin->setId($post['id'])->setTransactionId($post['transaction_id'])->delete();
        $response = [
            'status' => 1,
            'msg' => __('JsPress::backend.content_delete_fully')
        ];
        return response()->json($response);
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function restore(Request $request): JsonResponse
    {
        if( !auth()->user()->hasPostPermission('delete', $this->type->id) && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('JsPress::backend.permission_error')
            ]);
        }
        $this->post_admin->setId($request->input('id'))->restore();
        $response = [
            'status' => 1,
            'msg' => __('JsPress::backend.restore_post_success')
        ];
        return response()->json($response);
    }

    /**
     * Find and replace if post slug is exist
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function generateSlug(Request $request): JsonResponse
    {
        $slug = \Str::slug($request->input('title'), '-');
        return response()->json([
            'status' => 1,
            'slug' => $this->post_admin->generateSlug($slug)
        ]);
    }
}
