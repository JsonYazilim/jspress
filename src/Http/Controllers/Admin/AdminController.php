<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: AdminController.php
 * Author: Json Yazılım
 * Class: AdminController.php
 * Current Username: Erdinc
 * Last Modified: 4.07.2023 15:43
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use JsPress\JsPress\DataSources\JsPressPanelData;
use JsPress\JsPress\Http\Responses\AdminEditResponse;
use JsPress\JsPress\Interfaces\AdminInterface;
use JsPress\JsPress\Models\Admin;
use Spatie\Permission\Models\Role;
use Illuminate\Contracts\View\View;
use JsImage;
use General;

class AdminController extends Controller
{

    /**
     * @var AdminInterface
     */
    private AdminInterface $admin_i;

    /**
     * Create a new interface instance.
     * MediaInterface constructor
     *
     * @param AdminInterface $admin_i
     */
    public function __construct(AdminInterface $admin_i)
    {
        $this->admin_i = $admin_i;
    }

    /**
     * @return View
     */
    public function index(): View
    {
        return view('JsPress::admin.auth.login');
    }

    /**
     * @return View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function verifiyTwoFactor(): View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        return view('JsPress::admin.auth.verify_two_factor');
    }

    /**
     * @param Request $request
     * @param Admin $admins
     * @param JsPressPanelData $jspressPanel
     * @return View
     */
    public function list(Request $request, Admin $admins, JsPressPanelData $jspressPanel): View
    {
        if( !auth()->user()->hasPermissionTo('view admins') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }
        $admins = $this->admin_i->list($admins, $request);
        $roles = General::getRoles();
        $breadcrumbs = [
            Route('admin.admin.index') => __('JsPress::backend.admin.title')
        ];
        $jspressPanel->meta['title'] = __('JsPress::backend.admin.title');
        return view('JsPress::admin.admin.index', compact('breadcrumbs', 'admins', 'roles', 'jspressPanel'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        if( !auth()->user()->hasPermissionTo('add admin') && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('JsPress::backend.permission_error')
            ]);
        }
        $data = [
            'name' => $request->input('first_name').' '.$request->input('last_name'),
            'email' => $request->input('email'),
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => $request->input('password')
        ];
        $file = null;
        if( $request->file('avatar') ){
            $file = $request->file('avatar');
        }

        $admin = $this->admin_i->setAvatar($file)->save(new Admin(), $data);

        if( $request->input('role') ){
            $role = $request->input('role');
            $this->admin_i->assign_role($admin, $role);
        }
        return response()->json([
            'status' => 1,
            'msg' => __('JsPress::backend.admin.add_success')
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Admin $admin
     * @return AdminEditResponse
     */
    public function edit(Admin $admin): AdminEditResponse
    {
        if( !auth()->user()->hasPermissionTo('edit admin') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }
        if( $admin->hasRole('Super-Admin') && !Auth()->user()->hasRole('Super-Admin') && $admin->id != Auth()->user()->id ){
            abort(403);
        }

        $breadcrumbs = [
            Route('admin.admin.index') => __('JsPress::backend.admin.title'),
            Route('admin.admin.edit', ['admin' => $admin->id]) => __('JsPress::backend.admin.edit')
        ];

        return new AdminEditResponse($admin, $breadcrumbs);
    }

    /**
     * Update profile basic information.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Request $request): RedirectResponse
    {
        if( $request->input('admin_id') ){
            $admin = Admin::find($request->admin_id);
        }else{
            $admin = Auth()->guard('admin')->user();
        }
        $data['name'] = $request->name;
        $file = $request->file('avatar');
        $this->admin_i->setAvatar($file)->save($admin, $data);

        return redirect()->back()->with('success', __('JsPress::backend.profile.update_success'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updatePassword(Request $request): JsonResponse
    {
        $admin = Admin::find($request->admin_id);
        $data['password'] = $request->password;
        $this->admin_i->save($admin, $data);
        return response()->json([
            'status' => 1
        ]);
    }

    /**
     * Update user role.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function updateRole(Request $request): JsonResponse
    {
        $admin = Admin::find($request->admin_id);
        $role = Role::find($request->role);
        $this->admin_i->sync_role($admin, $role);

        return response()->json([
            'status' => 1
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function toggleTwoFactor(
        Request $request
    ): JsonResponse
    {
        $admin = Admin::find($request->admin_id);
        $admin->is_two_factor = $request->is_two_factor;
        $admin->save();

        return response()->json([
            'status' => 1
        ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function updateAdminPanel(Request $request): JsonResponse
    {
        $admin = Admin::find($request->admin_id);
        $data = $request->except(['admin_id', '_token']);
        if(!array_key_exists('theme', $data)){
            $data['theme'] = 0;
        }
        if(!array_key_exists('rtl', $data)){
            $data['rtl'] = 0;
        }
        $this->admin_i->save($admin, $data);

        return response()->json([
            'status' => 1
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request): JsonResponse
    {
        if( !auth()->user()->hasPermissionTo('delete admin') && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('JsPress::backend.permission_error')
            ]);
        }
        $admin = Admin::find($request->admin_id);
        $admin->syncRoles([]);
        $admin->delete();
        $response = [
            'status' => 1,
            'msg' => __('JsPress::backend.admin.delete_success')
        ];
        return response()->json($response);
    }

    /**
     * @param JsPressPanelData $jspressPanel
     * @return View
     */
    public function dashboard(JsPressPanelData $jspressPanel): View
    {
        $jspressPanel->meta = [
            'title' => __('JsPress::backend.dashboard')
        ];

        return view('JsPress::admin.index', compact('jspressPanel'));
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function changeTemplate(Request $request): bool
    {
        $data['theme'] =  $request->input('theme') == 'dark' ? 1 : 0;
        $admin = auth()->guard('admin')->user();
        $this->admin_i->save($admin, $data);
        return true;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function changeLanguage(Request $request): bool
    {
        $data['panel_language'] =  $request->input('locale');
        $admin = auth()->guard('admin')->user();
        $this->admin_i->save($admin, $data);
        return true;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function changeContentLanguage(Request $request): bool
    {
        Session::put('content_language', $request->input('locale'));
        return true;
    }

    /**
     * @return RedirectResponse
     */
    public function logout(): RedirectResponse
    {

        Auth::logout();
        return redirect()->route('admin.login');
    }

    /**
     * @return string
     */
    private function getRoles(): string
    {
        $roles = Role::all()->pluck('name')->toArray();
        return implode('|', $roles);
    }

    /**
     * Check if any user has email that is used
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function checkEmailExists(Request $request): JsonResponse
    {
        $checkEmail = Admin::where('email', $request->input('email'))->first();
        if( $checkEmail ){
            return response()->json([
                "valid" => "false"
            ]);
        }else{
            return response()->json([
                "valid" => "true"
            ]);
        }

    }


}
