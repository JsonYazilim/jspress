<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: CookieConsentController.php
 * Author: Json Yazılım
 * Class: CookieConsentController.php
 * Current Username: Erdinc
 * Last Modified: 19.05.2024 00:08
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JsPress\JsPress\DataSources\JsPressPanelData;
use JsPress\JsPress\Models\Cookie;

class CookieConsentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(JsPressPanelData $jspressPanel): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        if( !auth()->user()->hasPermissionTo('view cookie') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        $jspressPanel->meta['title'] = __('JsPress::backend.cookie_consent.title');
        $breadcrumbs = [
            Route('admin.cookie.index') => __('JsPress::backend.cookie_consent.title')
        ];
        $cookie = Cookie::find(1);
        return view('JsPress::admin.cookie.index', compact('jspressPanel', 'breadcrumbs', 'cookie'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        if( !auth()->user()->hasPermissionTo('view cookie') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        $data = $request->input();

        $categories = [1];
        if(isset($data['categories'])){
            foreach($data['categories'] as $category){
                $categories[] = intval($category);
            }
        }
        if(!isset($data['consentModal']['flipButtons'])){
            $data['consentModal']['flipButtons'] = false;
        }else{
            $data['consentModal']['flipButtons'] = true;
        }
        if(!isset($data['consentModal']['equalWeightButtons'])){
            $data['consentModal']['equalWeightButtons'] = false;
        }else{
            $data['consentModal']['equalWeightButtons'] = true;
        }

        if(!isset($data['preferencesModal']['flipButtons'])){
            $data['preferencesModal']['flipButtons'] = false;
        }else{
            $data['preferencesModal']['flipButtons'] = true;
        }
        if(!isset($data['preferencesModal']['equalWeightButtons'])){
            $data['preferencesModal']['equalWeightButtons'] = false;
        }else{
            $data['preferencesModal']['equalWeightButtons'] = true;
        }
        Cookie::updateOrCreate(
            [
                'id' => 1
            ],
            [
                'cookie_name' => $data['cookie_name'],
                'consentModal' => $data['consentModal'],
                'preferencesModal' => $data['preferencesModal'],
                'languages' => $data['languages_data'],
                'translations' => $data['languages'],
                'categories' => $categories,
                'theme' => $data['theme'],
                'page_interaction' => intval($data['disablePageInteraction']),
                'status' => intval($data['status'])
            ]
        );

        return redirect()->back()->with('success', __('JsPress::backend.cookie_consent.success_message'));
    }
}
