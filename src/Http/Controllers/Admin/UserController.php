<?php

namespace JsPress\JsPress\Http\Controllers\Admin;

use Illuminate\Contracts\View\View;
use JsPress\JsPress\Http\Controllers\Admin\PanelController;
use JsPress\JsPress\Models\Admin;
use JsPress\JsPress\Models\User;
use JsPress\JsPress\Models\UserMeta;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Intervention\Image\Facades\Image;

class UserController extends PanelController
{


    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @param User $user
     * @return View
     */
    public function index(Request $request, User $user): View
    {
        if( !auth()->user()->hasPermissionTo('view users') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }
        $users = $user;
        if( $request->input('q') ){
            $users = $users->where(function($query)use($request){
                return $query->where('name', 'like', '%'.$request->q.'%')
                    ->orWhere('email', 'like', $request->q.'%');
            });
        }

        $users = $users->orderBy('created_at', 'DESC');
        $users = $users->paginate(10);
        $breadcrumbs = [
            'javascript:void(0);' => __('JsPress::backend.pages.users.title'),
            Route('admin.user.list') => __('JsPress::backend.pages.users.list.title')
        ];

        return view('JsPress::admin.user.index', compact('users', 'breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        if( !auth()->user()->hasPermissionTo('add user') && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('JsPress::backend.permission_error')
            ]);
        }

        $userData = [
            'name' => $request->input('first_name').' '.$request->input('last_name'),
            'email' => $request->input('email'),
            'email_verified_at' => date('Y-m-d H:i:s'),
            'password' => Hash::make($request->input('password'))
        ];

        $user = User::create($userData);
        if( $user ){
            $userMetaData = [
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'user_id' => $user->id
            ];
            if( $request->file('avatar') ){
                $file = $request->file('avatar');
                $fileName = time().'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/avatar');
                $imgFile = Image::make($file->getRealPath());
                $imgFile->resize(150, 150)->save($destinationPath.'/'.$fileName);
                $userMetaData['avatar'] = $fileName;
            }
            UserMeta::create($userMetaData);
        }
        return response()->json([
            'status' => 1,
            'msg' => __('JsPress::backend.pages.users.add.success')
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return View
     */
    public function edit(User $user): View
    {
        if( !auth()->user()->hasPermissionTo('edit user') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }
        if( $user->hasRole('Super-Admin') && !Auth()->user()->hasRole('Super-Admin') && $user->id != Auth()->user()->id )
            return abort('403');

        $breadcrumbs = [
            'javascript:void(0);' => __('JsPress::backend.pages.users.title'),
            Route('admin.user.list') => __('JsPress::backend.pages.users.list.title'),
            Route('admin.user.edit', ['user'=>$user->id]) => __('JsPress::backend.pages.users.edit.title')
        ];

        $usermeta = $user->meta;

        return view('JsPress::admin.user.edit', compact('user','usermeta','breadcrumbs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, User $user)
    {

        if( !auth()->user()->hasPermissionTo('edit user') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }
        if(!$user){
            abort(404);
        }
        $user->name = $request->input('first_name').' '.$request->input('last_name');
        $user->email = $request->input('email');
        $user->save();
        if( $user ){
            $userMetaData = [
                'first_name' => $request->input('first_name'),
                'last_name' => $request->input('last_name'),
                'user_id' => $user->id,
                'phone_code' => $request->input('phone_code'),
                'phone' => $request->input('phone')
            ];
            if( $request->file('avatar') ){
                $file = $request->file('avatar');
                $fileName = time().'.'.$file->getClientOriginalExtension();
                $destinationPath = public_path('/uploads/avatar');
                $imgFile = Image::make($file->getRealPath());
                $imgFile->resize(150, 150)->save($destinationPath.'/'.$fileName);
                $userMetaData['avatar'] = $fileName;
            }
            UserMeta::where('user_id', $user->id)->update($userMetaData);
        }
        return redirect()->back()->with('success', __('JsPress::backend.pages.users.edit.update_profile_success_message'));
    }

    /**
     * Update user password.
     *
     * @param Request $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function updatePassword(Request $request, User $user): JsonResponse
    {
        if( !auth()->user()->hasPermissionTo('edit user') && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('JsPress::backend.permission_error')
            ]);
            exit;
        }
        $user->password = Hash::make($request->password);
        $user->save();
        return response()->json(['status'=>1]);
    }

    /**
     * Activate / Deactivate account
     *
     * @param Request $request
     * @param  \App\Models\User  $user
     * @return JsonResponse
     */
    public function updateUserStatus(Request $request, User $user): JsonResponse
    {
        if( !auth()->user()->hasPermissionTo('edit user') && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 2,
                'msg' => __('JsPress::backend.permission_error')
            ]);
            exit;
        }
        $user->status = $request->status;
        $user->save();
        if( $user->status == 1 ){
            $response = [
                'status' => 1,
                'msg' => __('JsPress::backend.pages.users.user_status_active_success')
            ];
        }else{
            $response = [
                'status' => 0,
                'msg' => __('JsPress::backend.pages.users.user_status_deactive_success')
            ];
        }
        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request)
    {
        if( !auth()->user()->hasPermissionTo('delete user') && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('JsPress::backend.permission_error')
            ]);
            exit;
        }
        User::where('id', $request->input('user_id'))->delete();
        $response = [
            'status' => 1,
            'msg' => __('JsPress::backend.pages.users.delete.delete_user_success_message')
        ];
        return response()->json($response);
    }

    /**
     * Check if any user has email that is used
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function checkEmailExists(Request $request): JsonResponse
    {
        $checkUserEmail = User::where('email', $request->input('email'))->first();
        $checkAdminEmail = Admin::where('email', $request->input('email'))->first();

        if( $checkUserEmail ||$checkAdminEmail ){
            return response()->json([
                "valid" => "false"
            ]);
        }else{
            return response()->json([
                "valid" => "true"
            ]);
        }

    }

    /**
     * Verify user
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function verify(Request $request)
    {
        return User::where('id', $request->user_id)->update(['email_verified_at' => date('Y-m-d H:i:s')]);
    }

}
