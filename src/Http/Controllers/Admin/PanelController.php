<?php

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class PanelController extends Controller
{

    public function __construct()
    {
        $roles = self::getRoles();
        $this->middleware(['role:'.$roles]);
    }

    private function getRoles(): string
    {
        $roles = Role::all()->pluck('name')->toArray();
        return implode('|', $roles);
    }
}
