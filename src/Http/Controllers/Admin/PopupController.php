<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PopupController.php
 * Author: Json Yazılım
 * Class: PopupController.php
 * Current Username: Erdinc
 * Last Modified: 16.04.2024 21:35
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use JsPress\JsPress\DataSources\JsPressPanelData;
use JsPress\JsPress\Http\Requests\PopupRequest;
use JsPress\JsPress\Models\Category;
use JsPress\JsPress\Models\Popup;
use JsPress\JsPress\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use JsPressPanel;

class PopupController extends Controller
{
    /**
     * @param JsPressPanelData $jspressPanel
     * @return \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function index(JsPressPanelData $jspressPanel): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        if( !auth()->user()->hasPermissionTo('view popup') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        $jspressPanel->meta['title'] = __('JsPress::backend.popup_title');
        $breadcrumbs = [
            Route('admin.popup.index') => __('JsPress::backend.popup_title')
        ];
        $popups = Popup::orderBy('created_at', 'DESC')->paginate(15);
        return view('JsPress::admin.popup.index', compact('jspressPanel', 'breadcrumbs', 'popups'));
    }

    /**
     * @param JsPressPanelData $jspressPanel
     * @return \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function create(JsPressPanelData $jspressPanel): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        if( !auth()->user()->hasPermissionTo('add popup') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        $jspressPanel->meta['title'] = __('JsPress::backend.add_new_popup');
        $breadcrumbs = [
            Route('admin.popup.index') => __('JsPress::backend.popup_title'),
            Route('admin.popup.create') => __('JsPress::backend.add_new_popup')
        ];
        return view('JsPress::admin.popup.create', compact('jspressPanel', 'breadcrumbs'));
    }

    /**
     * @param Popup $popup
     * @param JsPressPanelData $jspressPanel
     * @return \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
     */
    public function edit(Popup $popup, JsPressPanelData $jspressPanel): \Illuminate\Contracts\View\View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        if( !auth()->user()->hasPermissionTo('edit popup') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        $jspressPanel->meta['title'] = __('JsPress::backend.popup.edit_popup', ['id'=>$popup->id]);
        $breadcrumbs = [
            Route('admin.popup.index') => __('JsPress::backend.popup_title'),
            Route('admin.popup.edit', ['popup'=>$popup->id]) => __('JsPress::backend.popup.edit_popup', ['id'=>$popup->id])
        ];
        return view('JsPress::admin.popup.edit', compact('jspressPanel', 'breadcrumbs', 'popup'));
    }

    /**
     * @param PopupRequest $request
     * @return JsonResponse
     */
    public function store(PopupRequest $request): JsonResponse
    {
        $data = $request->input();

        $popup = $this->save($data);
        if($popup){
            $this->saveDevices($popup, $data['devices']);
            if($data['page_show_type'] == 1){
                $this->saveLocales($popup, $data['languages']);
                $this->savePages($popup, json_decode($data['page_urls'], true));
            }
            if($data['page_show_type'] != 1 && isset($data['id'])){
                $popup->locales()->delete();
                $popup->pages()->delete();
            }
            if($data['page_show_type'] == 3){
                $this->saveUrls($popup, explode(PHP_EOL, $data['custom_url']));
            }
            if($data['page_show_type'] != 3 && isset($data['id'])){
                $popup->urls()->delete();
            }
            return response()->json([
                'status' => 1,
                'msg' => isset($data['id']) ? __('JsPress::backend.popup.popup_edit_success') : __('JsPress::backend.popup.popup_create_success'),
                'route' => route('admin.popup.edit', ['popup'=>$popup->id])
            ]);
        }
        return response()->json([
            'status' => 0
        ]);
    }

    /**
     * @param Popup $popup
     * @return JsonResponse
     */
    public function delete(Popup $popup): JsonResponse
    {
        if( !auth()->user()->hasPermissionTo('delete popup') && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('JsPress::backend.permission_error')
            ]);
        }

        $popup->delete();
        return response()->json([
            'status' => 1,
            'msg' => __('JsPress::backend.popup.delete_popup_message')
        ]);
    }


    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function searchPosts(Request $request): JsonResponse
    {
        $term = $request->input('term');
        $languages = $request->input('languages') ?? [];
        $data = [];
        $posts = Post::where('title', 'like', $term.'%')
            ->when(count($languages) > 0, function ($query) use ($languages) {
                return $query->whereHas('translation', function ($query) use ($languages) {
                    return $query->whereIn('locale', $languages);
                });
            })
            ->orderBy('title', 'asc')
            ->with(['post_type_item'])
            ->paginate(30);
        if($posts->isNotEmpty()){
            foreach($posts as $post){
                $data[] = [
                    'title' => $post->title .' ('. __js('post_type', JsPressPanel::postTypeLangKey($post->post_type_item, 'list'), $post->post_type_item->name).')',
                    'id' => $post->id,
                    'type' => 'post'
                ];
            }
        }
        $categories = Category::where('title', 'like', $term.'%')
            ->when(count($languages) > 0, function ($query) use ($languages) {
                return $query->whereHas('translation', function ($query) use ($languages) {
                    return $query->whereIn('locale', $languages);
                });
            })
            ->orderBy('title', 'asc')
            ->with(['post_type_item'])
            ->paginate(30);
        if($categories->isNotEmpty()){
            foreach($categories as $category){
                if(isset($category->post_type_item->category_detail)){
                    $data[] = [
                        'id' => $category->id,
                        'title' => $category->title. ' ('.__js('post_type', JsPressPanel::postTypeLangKey($category->post_type_item, 'category'), $category->post_type_item->category_detail->plural_name).')',
                        'type' => 'category'
                    ];
                }

            }
        }
        $data = collect($data)->sortBy('title')->take(20)->values()->all();
        return response()->json($data);
    }

    /**
     * @param $data
     * @return Popup
     */
    private function save($data): Popup
    {

        if(isset($data['id'])){
            $popup = Popup::find($data['id']);
        }else{
            $popup = new Popup();
        }
        $popup->title = $data['title'];
        $popup->content = $data['content'];
        $popup->type = $data['type'];
        $popup->place = $data['place'] ?? NULL;
        $popup->status = $data['status'];
        $popup->order = $data['order'];
        $popup->page_show_type = $data['page_show_type'];
        $popup->is_display_name = $data['is_display_name'];
        $popup->is_content_image = $data['type'] == 'tooltip' ? $data['is_content_image'] : 0;
        if(isset($data['button_type'])){
            $popup->button_type = $data['button_type'];
        }
        if(isset($data['button_text'])){
            $popup->button_text = $data['button_text'];
        }

        $popup->impressions = $data['impressions'];
        $popup->impression_count = $data['impression_count'];
        $popup->date_type = $data['date_type'];
        $popup->start_date = $data['date_type'] == 1 ? $data['start_date'] : NULL;
        $popup->end_date = $data['date_type'] == 1 ? $data['end_date'] : NULL;
        $popup->time_type = $data['time_type'];
        $popup->display_time = $data['display_time'];
        $popup->conditions = $data['conditions'];
        $popup->condition_time = $data['condition_time'];
        $popup->condition_scroll = $data['condition_scroll'];
        $data['design']['font_data'] = json_decode($data['design']['font_data'], TRUE);
        $popup->design = json_encode($data['design']);
        $popup->custom_css = $data['custom_css'];
        $popup->save();
        return $popup;
    }

    /**
     * @param $popup
     * @param $devices
     * @return void
     */
    private function saveDevices($popup, $devices): void
    {
        $popup->devices()->delete();
        foreach($devices as $device)
        {
            $popup->devices()->create(['device'=>$device]);
        }
    }

    /**
     * @param $popup
     * @param $locales
     * @return void
     */
    private function saveLocales($popup, $locales): void
    {
        $popup->locales()->delete();
        foreach($locales as $locale)
        {
            $popup->locales()->create(['locale'=>$locale]);
        }
    }

    /**
     * @param $popup
     * @param $pages
     * @return void
     */
    private function savePages($popup, $pages): void
    {
        $popup->pages()->delete();
        foreach($pages as $page){
            $popup->pages()->create(['element_id'=>$page['id'],'type'=>$page['type']]);
        }
    }

    /**
     * @param $popup
     * @param $urls
     * @return void
     */
    private function saveUrls($popup, $urls): void
    {
        $popup->urls()->delete();
        foreach ($urls as $url){
            $popup->urls()->create(['url'=>$url]);
        }
    }
}
