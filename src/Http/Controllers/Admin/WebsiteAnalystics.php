<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: WebsiteAnalystics.php
 * Author: Json Yazılım
 * Class: WebsiteAnalystics.php
 * Current Username: Erdinc
 * Last Modified: 7.01.2024 22:30
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JsPress\JsPress\Interfaces\AnalysticInterface;

class WebsiteAnalystics extends Controller
{

    private AnalysticInterface $analystic;

    public function __construct(
        AnalysticInterface $analystic
    ){
        $this->analystic = $analystic;
    }

    public function statistics()
    {

    }

    public function track(Request $request): array
    {
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $type = $request->input('type') ?? 'device';
        return $this->analystic->setStartDate($start_date)->setEndDate($end_date)->setType($type)->track();
    }

}
