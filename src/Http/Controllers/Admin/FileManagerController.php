<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: FileManagerController.php
 * Author: Json Yazılım
 * Class: FileManagerController.php
 * Current Username: Erdinc
 * Last Modified: 7.08.2023 15:10
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use JsPress\JsPress\Interfaces\MediaInterface;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;

class FileManagerController extends Controller
{
    /**
     * @var MediaInterface
     */
    private MediaInterface $media;

    /**
     * Create a new interface instance.
     * MediaInterface constructor
     *
     * @param MediaInterface $media
     */
    public function __construct(MediaInterface $media)
    {
        return $this->media = $media;
    }

    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        return view('JsPress::admin.filemanager.index');
    }

}
