<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: RedirectController.php
 * Author: Json Yazılım
 * Class: RedirectController.php
 * Current Username: Erdinc
 * Last Modified: 1.03.2024 15:30
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use JsPress\JsPress\DataSources\JsPressPanelData;
use JsPress\JsPress\Exports\RedirectExport;
use JsPress\JsPress\Models\Redirect;
use Maatwebsite\Excel\Facades\Excel;

class RedirectController extends Controller
{
    /**
     * @author Erdinç Taze
     * @param JsPressPanelData $jspressPanel
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
     */
    public function index(JsPressPanelData $jspressPanel, Request $request): \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application
    {
        if( !auth()->user()->hasPermissionTo('view redirects') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        $breadcrumbs = [
            Route('admin.redirect.index') => __('JsPress::backend.redirects')
        ];
        $jspressPanel->meta['title'] = __('JsPress::backend.redirects');
        $redirects = Redirect::when($request->input('q'), function($query)use($request){
                return $query->where('old_url', 'like', '%'.$request->input('q').'%')->orWhere('new_url', 'like', '%'.$request->input('q').'%');
            })
            ->orderBy('created_at', 'DESC')->paginate(12);
        return view('JsPress::admin.redirect.index', compact('breadcrumbs', 'redirects', 'jspressPanel'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        if( !auth()->user()->hasPermissionTo('view redirects') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        $request->validate([
            'old_url' => ['required', 'string'],
            'new_url' => ['required', 'string']
        ]);

        Redirect::updateOrCreate(
            [
                'old_url' => $request->input('old_url')
            ],
            [
                'new_url' => $request->input('new_url'),
                'status_code' => $request->input('status_code')
            ]
        );

        return redirect()->back()->with('success', __('JsPress::backend.redirect_added_successfully'));
    }

    /**
     * @param Redirect $redirect
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Redirect $redirect): \Illuminate\Http\RedirectResponse
    {
        if( !auth()->user()->hasPermissionTo('view redirects') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        $redirect->delete();

        return redirect()->back()->with('success', __('JsPress::backend.redirect_deleted_successfully'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function import(Request $request): \Illuminate\Http\RedirectResponse
    {
        if( !auth()->user()->hasPermissionTo('view redirects') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        Excel::import(new \JsPress\JsPress\Imports\Redirect(), request()->file('redirects'));
        return redirect()->back()->with('success', __('JsPress::backend.success_redirect_import'));
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        if( !auth()->user()->hasPermissionTo('view redirects') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        return Excel::download(new RedirectExport(), 'redirects-'.\Carbon\Carbon::now()->format('Y-m-d H:i:s').'.xlsx');
    }
}
