<?php

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class AdminServicesController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function fileUpload(Request $request)
    {

        if ($request->hasFile('upload')) {
            $originName = $request->file('upload')->getClientOriginalName();
            $fileName = pathinfo($originName, PATHINFO_FILENAME);
            $extension = $request->file('upload')->getClientOriginalExtension();
            $fileName =  \Str::slug($fileName,'-') .'.'. $extension;
            $upload_data = [
                \Carbon\Carbon::now()->format('Y'),
                \Carbon\Carbon::now()->format('m')
            ];
            $uploadPath = implode('/', $upload_data);
            $path = public_path().'/uploads/'.$uploadPath;

            if( !file_exists($path) ){
                \File::makeDirectory($path, $mode = 0777, true, true);
            }
            $request->file('upload')->move($path, $fileName);

            $url = asset('/uploads/'.$uploadPath.'/' . $fileName);
            return response()->json(['fileName' => $fileName, 'uploaded'=> 1, 'url' => $url]);
        }
    }

    /**
     * @param $file
     * @return string
     */
    public function fileUploadClient($file): string
    {

        $originName = $file->getClientOriginalName();
        $fileName = pathinfo($originName, PATHINFO_FILENAME);
        $extension = $file->getClientOriginalExtension();
        $fileName = \Str::slug($fileName,'-') .'.'. $extension;
        $upload_data = [
            \Carbon\Carbon::now()->format('Y'),
            \Carbon\Carbon::now()->format('m')
        ];
        $uploadPath = implode('/', $upload_data);
        $path = public_path().'/uploads/'.$uploadPath;

        if( !file_exists($path) ){
            \File::makeDirectory($path, $mode = 0777, true, true);
        }
        $file->move($path, $fileName);

        return '/uploads/'.$uploadPath.'/' . $fileName;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePostOrder(Request $request): \Illuminate\Http\JsonResponse
    {
        Session::put(generate_post_order_session_key($request->input('post_type')), $request->input('order'));
        return response()->json(['status'=>1]);
    }

}
