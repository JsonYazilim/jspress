<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PostFieldController.php
 * Author: Json Yazılım
 * Class: PostFieldController.php
 * Current Username: Erdinc
 * Last Modified: 30.09.2023 12:55
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Schema;
use JsPress\JsPress\DataSources\JsPressPanelData;
use JsPress\JsPress\Models\PostField;
use JsPressPanel;

class PostFieldController extends Controller
{
    private array $excluded_tables = ['admins', 'category_post', 'failed_jobs', 'language_translations', 'media', 'media_details', 'menu', 'migrations', 'model_has_permissions', 'model_has_roles', 'password_resets', 'password_reset_tokens', 'permissions', 'permission_group', 'personal_access_tokens', 'posts', 'categories', 'post_extras', 'post_fields', 'post_translations', 'post_types', 'post_type_details', 'post_type_permissions', 'post_views', 'roles', 'role_has_permissions', 'seo_meta', 'settings', 'translations', 'translation_strings', 'user_meta'];
    /**
     * Display a listing of the resource.
     */
    public function index(JsPressPanelData $jspressPanel): View
    {
        if( !auth()->user()->hasPermissionTo('view post_field') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }
        $jspressPanel->meta['title'] = __('JsPress::backend.post_field.title');
        $breadcrumbs = [
            Route('admin.post_field.index') => __('JsPress::backend.post_field.title')
        ];

        $post_fields = PostField::orderBy('created_at', 'DESC')->paginate(15);

        return view('JsPress::admin.post_field.index', compact('jspressPanel', 'breadcrumbs', 'post_fields'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(JsPressPanelData $jspressPanel): View
    {
        if( !auth()->user()->hasPermissionTo('add post_field') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        $jspressPanel->meta['title'] = __('JsPress::backend.post_field.add_new');

        $breadcrumbs = [
            Route('admin.post_field.index') => __('JsPress::backend.post_field.title'),
            Route('admin.post_field.create') => __('JsPress::backend.post_field.add_new')
        ];
        $rule_options = $this->getRuleOptions();
        $post_objects = $this->getPostObjects();
        $category_objects = $this->getCategoryObjects();
        $table_options = $this->getDatabaseTableOptions();
        return view('JsPress::admin.post_field.create', compact('jspressPanel', 'breadcrumbs', 'rule_options', 'post_objects', 'category_objects', 'table_options'));
    }

    public function getPostObjects(): array
    {
        $object_data = [];
        $post_types = \DB::table('post_types')
            ->select([
                'post_types.id',
                'post_types.key',
                'post_types.name'
            ])
            ->join('post_type_details', function($join){
                $join->on('post_type_details.post_type_id', '=', 'post_types.id')
                    ->where('post_type_details.model', '=', 'JsPress\JsPress\Models\Post')
                    ->where('locale', JsPressPanel::contentLocale());
            })
            ->get();
        foreach($post_types as $type){
            $object_data[] = [
                'value' => $type->key,
                'label' => __js('post_type', JsPressPanel::postTypeLangKey($type, 'list'), $type->name)
            ];
        }
        return $object_data;
    }

    public function getCategoryObjects(): array
    {
        $object_data = [];
        $post_types = \DB::table('post_types')
            ->select([
                'post_types.id',
                'post_types.key',
                'post_types.name'
            ])
            ->join('post_type_details', function($join){
                $join->on('post_type_details.post_type_id', '=', 'post_types.id')
                    ->where('post_type_details.model', '=', 'JsPress\JsPress\Models\Category')
                    ->where('locale', JsPressPanel::contentLocale());
            })
            ->get();
        foreach($post_types as $type){
            $object_data[] = [
                'value' => $type->key,
                'label' => __js('post_type', JsPressPanel::postTypeLangKey($type, 'list'), $type->name)
            ];
        }
        return $object_data;
    }

    private function getRuleOptions(): array
    {
        $post_type_data = [];
        $category_type_data = [];
        $post_types = \DB::table('post_types')->get();
        $category_types = \DB::table('post_types')->where('is_category', 1)->get();
        foreach( $post_types as $type ){
            $post_type_data[] = [
                'label' => $type->name,
                'value' => $type->key
            ];
        }
        foreach( $category_types as $type ){
            $category_type_data[] = [
                'label' => $type->name,
                'value' => $type->key
            ];
        }
        return [
            'post_type' => $post_type_data,
            'category_type' => $category_type_data,
            'template' => JsPressPanel::getTemplates()
        ];
    }

    private function getDatabaseTableOptions(): array
    {
        $database = Schema::getConnection()->getDatabaseName();
        $tables = Schema::getAllTables();
        $table_data = [];
        foreach($tables as $table){
            if(!in_array($table->{'Tables_in_'.$database}, $this->excluded_tables)){
                $table_data[$table->{'Tables_in_'.$database}]['name'] = $table->{'Tables_in_'.$database};
                $table_columns = Schema::getColumnListing($table->{'Tables_in_'.$database});
                if(count($table_columns) > 0){
                    foreach($table_columns as $column){
                        $table_data[$table->{'Tables_in_'.$database}]['columns'][] = $column;
                    }
                }
            }
        }
        return $table_data;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): \Illuminate\Http\RedirectResponse
    {
        $data = $request->input();

        $post_field_id = $data['post_field_id'] ?? null;
        $post_field_data = $this->convertPostFields($data['post_field']['items']);
        $post_field = PostField::updateOrCreate(
            [
                'id' => $post_field_id
            ],
            [
                'title' => $data['title'],
                'key' => \Str::slug($data['title'], '-'),
                'position' => $data['position'],
                'conditions' => $data['condition'],
                'appearence' => $data['appearence'],
                'is_accordion_open' => $data['is_accordion_open'],
                'order' => $data['order'],
                'data' => $post_field_data
            ]
        );
        $message = isset($data['post_field_id']) ? __('JsPress::backend.post_field.update_success') : __('JsPress::backend.post_field.create_success');
        return redirect()->route('admin.post_field.edit', ['id' => $post_field->id])->with('success', $message);
    }

    private function convertPostFields($fields, &$post_field_data = []): array
    {
        foreach( collect($fields)->sortBy('order')->values()->toArray() as $k => $field ){
            $post_field_data[$k]['label'] = $field['label'];
            $post_field_data[$k]['key'] = $field['key'];
            $post_field_data[$k]['type'] = $field['type'];
            $post_field_data[$k]['order'] = $field['order'];
            $post_field_data[$k]['is_required'] = isset($field['is_required'][0]) ? 1 :0;
            $post_field_data[$k]['width'] = $field['width'];
            $post_field_data[$k]['classes'] = $field['classes'];
            $post_field_data[$k]['ids'] = $field['ids'];
            if( in_array($field['type'], ["text","number","email","textarea"]) ){
                $post_field_data[$k]['placeholder'] = $field['placeholder'] ?? "";
            }

            if( in_array($field['type'], ["checkbox", "radio"]) ){
                $post_field_data[$k]['options'] = $field['options'];
            }
            if( $field['type'] == "selectbox" ){
                $post_field_data[$k]['option_type'] = $field['option_type'];
                if($field['option_type'] == 'manual'){
                    $post_field_data[$k]['options'] = $field['options'];
                }
                if( $field['option_type'] == 'database' ){
                    $post_field_data[$k]['select_options_table'] = $field['select_options_table'];
                    $post_field_data[$k]['select_options_column_value'] = $field['select_options_column_value'] ?? null;
                    $post_field_data[$k]['select_options_column_label'] = $field['select_options_column_label'] ?? null;
                }
                $post_field_data[$k]['is_multiple'] = isset($field['is_multiple']) ? 1 : 0;
            }
            if( $field['type'] == "switch" ){
                $post_field_data[$k]['message'] = $field['message'];
                $post_field_data[$k]['is_open'] = isset($field['is_open'][0]) ? 1 : 0;
            }
            if( $field['type'] == "post_object" ){
                $post_field_data[$k]['post_objects'] = $field['post_objects'];
                $post_field_data[$k]['is_multiple'] = isset($field['is_multiple']) ? 1 : 0;
            }
            if( $field['type'] == "category_object" ){
                $post_field_data[$k]['category_objects'] = $field['category_objects'];
                $post_field_data[$k]['is_multiple'] = isset($field['is_multiple']) ? 1 : 0;
            }
            if( $field['type'] == 'repeater' ){
                $post_field_data[$k]['items'] = [];
                $this->convertPostFields($field['items'], $post_field_data[$k]['items']);
            }
            if( $field['type'] == "gallery" || $field['type'] == "file" ){
                $post_field_data[$k]['max_file'] = $field['max_file'];
            }
        }
        return $post_field_data;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id, JsPressPanelData $jspressPanel): View|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\Foundation\Application
    {
        if( !auth()->user()->hasPermissionTo('edit post_field') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        $jspressPanel->meta['title'] = __('JsPress::backend.post_field.edit');
        $post_field = PostField::find($id);
        $breadcrumbs = [
            Route('admin.post_field.index') => __('JsPress::backend.post_field.title'),
            Route('admin.post_field.edit', ['id'=>$post_field->id]) => __('JsPress::backend.post_field.edit')
        ];

        $rule_options = $this->getRuleOptions();
        $post_objects = $this->getPostObjects();
        $category_objects = $this->getCategoryObjects();
        $table_options = $this->getDatabaseTableOptions();
        return view('JsPress::admin.post_field.create', compact('jspressPanel', 'breadcrumbs', 'rule_options', 'post_field', 'post_objects', 'category_objects', 'table_options'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request)
    {
        if( !auth()->user()->hasPermissionTo('edit post_field') && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('JsPress::backend.permission_error')
            ]);
        }
        $id = $request->input('post_field_id');
        $post_field = PostField::find($id);
        $post_field->delete();
        return response()->json([
            'status' => 1,
            'msg' => __('JsPress::backend.post_field.delete_success')
        ]);
    }
}
