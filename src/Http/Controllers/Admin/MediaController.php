<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: MediaController.php
 * Author: Json Yazılım
 * Class: MediaController.php
 * Current Username: Erdinc
 * Last Modified: 24.06.2023 12:37
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use JsPress\JsPress\Exceptions\FileManagerException;
use JsPress\JsPress\Interfaces\MediaInterface;
use JsImage;
use JsPress\JsPress\DataSources\JsPressPanelData;

class MediaController extends Controller
{

    /**
     * @var MediaInterface
     */
    private MediaInterface $media;

    /**
     * Create a new interface instance.
     * MediaInterface constructor
     *
     * @param MediaInterface $media
     */
    public function __construct(MediaInterface $media)
    {
        return $this->media = $media;
    }

    /**
     * Display a listing of the resource.
     *
     * @param JsPressPanelData $jspressPanel
     * @return View
     */
    public function index(JsPressPanelData $jspressPanel): View
    {
        $breadcrumbs = [
            Route('admin.media.index') => __('JsPress::backend.media.list.title')
        ];
        $jspressPanel->meta['title'] = __('JsPress::backend.media.list.title');
        return view('JsPress::admin.media.index', compact('breadcrumbs', 'jspressPanel'));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getJson(Request $request): JsonResponse
    {
        $data = $request->input();
        return response()->json($this->media->getData($data));
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function getDetailJson(int $id): JsonResponse
    {
        if(!auth()->user()->hasPermissionTo('edit media') && !Auth()->user()->hasRole('Super-Admin')){
            return response()->json([
                'status' => false,
                'msg' => __('JsPress::backend.permission_error')
            ]);
        }
        return response()->json($this->media->getDetail($id));
    }

    /**
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function saveDetails(int $id, Request $request): JsonResponse
    {
        $details = $request->input('detail');
        $this->media->saveDetails($id, $details);
        return response()->json(true);
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return JsonResponse
     * @throws FileManagerException
     */
    public function store(Request $request): JsonResponse
    {
        if(!auth()->user()->hasPermissionTo('add media') && !Auth()->user()->hasRole('Super-Admin')){
            return response()->json([
                'status' => false,
                'msg' => __('JsPress::backend.permission_error')
            ]);
        }
        $validator = Validator::make($request->all(), [
            'file' => 'required|max:'.get_max_upload_size()
        ]);
        if ($validator->fails()) {
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }
        $width = null;
        $height = null;
        $allowed_mime_types = $this->media->allowed_mime_types();
        $file = $request->file('file');
        $action = $request->input('action');
        $fileType = JsImage::getFileType($file);
        $extension = $file->getClientOriginalExtension();
        $saveFile = null;
        if($action === 'media_library'){
            if( $fileType == 'image' && $extension != 'svg' ){
                [$width, $height] = getimagesize($file->path());
            }
            $fileName = JsImage::getFileName($file);
            if( in_array($file->getClientMimeType(), $allowed_mime_types) ){
                $saveFile = $this->media->setFile($file)
                    ->setUploadName($fileName['upload_name'])
                    ->setFileName($fileName['file_name'])
                    ->setExtension($extension)
                    ->setMime($file->getClientMimeType())
                    ->setType($fileType)
                    ->setWidth($width)
                    ->setHeight($height)
                    ->setSize($file->getSize())
                    ->setPath()
                    ->setDirectory()
                    ->setUserId(auth()->user()->id)
                    ->move()
                    ->crop()
                    ->save();
                if(!$saveFile){
                    throw new FileManagerException(__('JsPress::backend.could_not_save_file'));
                }
            }else{
                throw new FileManagerException(__('JsPress::backend.not_allowed_file_type'));
            }
        }
        return response()->json([
            'status' => true,
            'data' => $this->media->getDetail($saveFile->id)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id): JsonResponse
    {
        if(!auth()->user()->hasPermissionTo('delete media') && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => false,
                'msg' => __('JsPress::backend.permission_error')
            ]);
        }
        $this->media->get($id)->unlink()->delete();
        return response()->json([
            'status' => true
        ]);
    }
}
