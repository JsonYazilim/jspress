<?php

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use JsPress\JsPress\DataSources\JsPressPanelData;
use JsPress\JsPress\Models\Language;

class LanguageController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param JsPressPanelData $jspressPanel
     * @return View
     */
    public function index(JsPressPanelData $jspressPanel): View
    {
        if( !auth()->user()->hasPermissionTo('view languages') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }
        $languages = Language::orderBy('is_default', 'DESC')->orderBy('status', 'DESC')->orderBy('id')->get();
        $breadcrumbs = [
            'javascript:void(0);' => __('JsPress::backend.pages.languages.title'),
            Route('admin.language.index') => __('JsPress::backend.pages.languages.list.title')
        ];
        $jspressPanel->meta['title'] = __('JsPress::backend.pages.languages.list.title');
        return view('JsPress::admin.languages.index', compact('languages', 'breadcrumbs', 'jspressPanel'));
    }

    /**
     * Set as default language of selected item
     *
     * @param Request $request
     * @return JsonResponse
     */

    public function setDefaultLanguage(Request $request): JsonResponse
    {
        if( !auth()->user()->hasPermissionTo('edit language') && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('JsPress::backend.permission_error')
            ]);
        }

        $language_id = $request->input('language_id');
        Language::query()->update(['is_default' => 0]);
        $language = Language::find($language_id);
        $language->is_default = 1;
        $language->save();
        Artisan::call('route:clear');
        return response()->json([
            'status' => 1,
            'msg' => __('JsPress::backend.pages.languages.success_default_language')
        ]);
    }

    /**
     * Toggle language active or deactivating
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function toggleLanguage(Request $request): JsonResponse
    {
        if( !auth()->user()->hasPermissionTo('edit language') && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('JsPress::backend.permission_error')
            ]);
        }
        if(website_languages()->count() == 1 && $request->input('status') == 1){
            return response()->json([
                'status' => 0,
                'msg' => __('JsPress::backend.at_least_one_language_error')
            ]);
        }
        $language_id = $request->input('language_id');
        $language = Language::find($language_id);
        $active_languages = Language::where('is_default', 1)->where('id', '!=', $language->id)->get();

        if($request->input('status') == 1 && $language->is_default = 1){
            $language->is_default = 0;
        }
        if($active_languages->count() < 1 && $request->input('status') == 0){
            Language::where('is_default', 1)->update(['is_default' => 0]);
            $language->is_default = 1;
        }
        $language->status = $request->input('status') == 1 ? 0 : 1;
        $language->save();

        $active_languages = Language::where('is_default', 1)->get();
        if($active_languages->count() < 1){
            $language = Language::where('locale', 'tr')->where('status', 1)->first();
            if($language){
                $language->is_default = 1;
                $language->save();
            }else{
                $language = Language::where('status', 1)->first();
                $language->is_default = 1;
                $language->save();
            }
        }
        Artisan::call('route:clear');
        return response()->json([
            'status' => 1,
            'msg' => __('JsPress::backend.pages.languages.success_default_language')
        ]);
    }


}
