<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: VCSController.php
 * Author: Json Yazılım
 * Class: VCSController.php
 * Current Username: Erdinc
 * Last Modified: 26.02.2024 22:28
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Carbon\Carbon;
use CzProject\GitPhp\GitException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use CzProject\GitPhp\GitRepository;


class VCSController extends Controller
{

    public const MESSAGE = 'message';
    public const VCS_INDEX = 'Tools.vcs.index';
    public const ORIGIN = 'origin';
    public const ORIGIN1 = 'origin/';

    /**
     * @throws GitException
     */
    public function index(Request $request)
    {
        if( !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }

        $getOrigin = $request->origin;

        $repo = $this->getRepo();
        $status = $this->getStatus($repo);
        $repo->fetch(self::ORIGIN);

        $remote = $this->getRepoUrl($repo);
        $commitData = $repo->getCommit($repo->getLastCommitId());

        $lastCommit['date'] = Carbon::parse($commitData->getCommitterDate())->setTimezone('Europe/Istanbul');
        $lastCommit['commit'] = $commitData->getId();
        $currentBranch = $repo->getCurrentBranchName();
        $origins = $repo->getRemoteBranches();
        $branches = array_merge($repo->getLocalBranches(), $origins);


        $commits = $this->commitLogs($repo, $getOrigin);

        $logs = collect($this->logs($repo))->slice(0, 10);

        $breadcrumbs = [
            Route('admin.vcs.index') => __('JsPress::backend.version_control_system')
        ];



        return view('JsPress::admin.vcs.index', compact('remote', 'branches', 'currentBranch', 'commits', 'status', 'logs', 'lastCommit', 'origins',
            'getOrigin','breadcrumbs'));
    }

    /**
     * @param string $commitId
     * @param string|null $brach
     * @return RedirectResponse
     * @throws GitException
     */
    public function merge(string $commitId, string $brach = null): RedirectResponse
    {
        $repo = $this->getRepo();
        $repo->fetch(self::ORIGIN);

        if ($brach) {
            $current = $brach;
        } else {
            $current = $repo->getCurrentBranchName();
        }

        $repo->merge($current);

        $repo->execute(['reset', mb_substr($commitId, 0, 8), '--hard']);
        return redirect(route('admin.vcs.index'). '?origin=' . urlencode(self::ORIGIN1 . $current))->with('success', __('JsPress::backend.vcs_merge_success'));
    }

    /**
     * @throws GitException
     */
    private function getRepo()
    {
        return new GitRepository(base_path());
    }

    /**
     * @param GitRepository $repo
     * @return array|array[]
     * @throws GitException
     */
    private function getStatus(GitRepository $repo): array
    {
        $status = ['deleted' => [], 'modified' => [], 'not_tracked' => [], 'new_file' => []];

        if ($repo->hasChanges()) {

            $list = $repo->execute('status');

            $track = false;
            foreach ($list as $item) {
                if (strpos($item, 'deleted:') !== false) {
                    $status['deleted'][] = trim(explode('deleted:', $item)[1]);
                } elseif (strpos($item, 'modified:') !== false) {
                    $status['modified'][] = trim(explode('modified:', $item)[1]);
                } elseif (strpos($item, 'new file:') !== false) {
                    $status['new_file'][] = trim(explode('new file:', $item)[1]);
                } else {
                    if (strpos($item, 'include in what will be committed') !== false) {
                        $track = true;
                    } elseif ($track && trim($item) && strpos($item, DIRECTORY_SEPARATOR)) {
                        $status['not_tracked'][] = trim($item);
                    }
                }
            }

        }
        return $status;
    }

    /**
     * @param GitRepository $repo
     * @return mixed|string
     * @throws GitException
     */
    private function getRepoUrl(GitRepository $repo)
    {
        $remote = collect($repo->execute(['remote', '-v']));

        $remote = trim(str_replace([self::ORIGIN], ['',], $remote->first()));

        $remote = explode(' ', $remote)[0];
        if (strpos($remote, 'git@') !== false) {
            $remote = 'https://' . str_replace(['git@', ':', '.git'], ['', '/', ''], $remote);
        }
        return $remote;
    }

    /**
     * @param GitRepository $repo
     * @param $getOrigin
     * @return array
     * @throws GitException
     */
    private function commitLogs(GitRepository $repo, $getOrigin): array
    {
        if ($getOrigin) {
            $commits = $repo->execute(['log', 'HEAD..' . $getOrigin]);
        } else {
            $current = $repo->getCurrentBranchName();

            $commits = $repo->execute(['log', 'HEAD..origin/' . $current]);
        }

        return $this->commitsToArray($commits);
    }

    /**
     * @param array $commits
     * @return array
     */
    private function commitsToArray(array $commits): array
    {
        $commitLogs = [];
        $i = -1;
        foreach ($commits as $commit) {
            if (strpos($commit, 'commit ') !== false) {
                $commitLogs[++$i]['commit'] = explode(' ', $commit)[1];
                $commitLogs[$i][self::MESSAGE] = '';
            } elseif (strpos($commit, 'Author:') !== false) {
                $regex = '/(Author: )([A-Za-zöğışçöÖÇŞİĞÜ ]+)[<]([a-zA-Z0-9.@]+)[>]/m';
                preg_match_all($regex, $commit, $matches, PREG_SET_ORDER, 0);

                $commitLogs[$i]['author']['name'] = trim(isset($matches[0][2]) ? $matches[0][2] : '');
                $commitLogs[$i]['author']['email'] = trim(isset($matches[0][3]) ? $matches[0][3] : '');
            } elseif (strpos($commit, 'Date:') !== false) {
                $commitLogs[$i]['date'] = Carbon::parse(explode('Date: ', $commit)[1])->setTimezone('Europe/Istanbul');
            } else {
                $commitLogs[$i][self::MESSAGE] .= $commit;
                $commitLogs[$i][self::MESSAGE] = trim($commitLogs[$i][self::MESSAGE]);
            }
        }
        return $commitLogs;
    }

    /**
     * @param GitRepository $repo
     * @return array
     * @throws GitException
     */
    private function logs(GitRepository $repo): array
    {
        $commits = $repo->execute('log');
        return $this->commitsToArray($commits);

    }


}
