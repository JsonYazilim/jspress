<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: SystemController.php
 * Author: Json Yazılım
 * Class: SystemController.php
 * Current Username: Erdinc
 * Last Modified: 28.07.2023 13:11
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Contracts\View\View;
use JsPress\JsPress\DataSources\JsPressPanelData;

class SystemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(JsPressPanelData $jspressPanel): View
    {
        if( !auth()->user()->hasPermissionTo('view system_settings') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }
        $breadcrumbs = [
            Route('admin.system.index') => __('JsPress::backend.system_settings')
        ];
        $jspressPanel->meta['title'] = __('JsPress::backend.system_settings');
        return view('JsPress::admin.system.index', compact('breadcrumbs', 'jspressPanel'));

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function clearSystem(Request $request): \Illuminate\Http\JsonResponse
    {
        if($request->input('type')){
            $type = $request->input('type');
            if( $type == 'cache' ){
                Artisan::call('cache:clear');
            }
            if( $type == 'config' ){
                Artisan::call('config:clear');
            }
            if( $type == 'view' ){
                Artisan::call('view:clear');
            }
            if( $type == 'route' ){
                Artisan::call('route:clear');
            }
            return response()->json([
                'status' => 1,
                'msg' => 'Başarılı şekilde temizlendi'
            ]);
        }
        return response()->json([
            'status' => 0,
            'msg' => 'Bir hata meydana geldi.'
        ]);
    }
}
