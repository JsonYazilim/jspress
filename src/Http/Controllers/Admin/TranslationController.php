<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: TranslationController.php
 * Author: Json Yazılım
 * Class: TranslationController.php
 * Current Username: Erdinc
 * Last Modified: 22.05.2023 22:07
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use JsPress\JsPress\Models\Translation;
use JsPress\JsPress\Models\TranslationString;
use Maatwebsite\Excel\Facades\Excel;
use JsPress\JsPress\Exports\Translation as TranslationExport;
use JsPress\JsPress\Imports\Translation as TranslationImport;

class TranslationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param $locale
     * @param Request $request
     * @return View
     */
    public function index($locale, Request $request): View
    {
        if( !auth()->user()->hasPermissionTo('view translations') && !Auth()->user()->hasRole('Super-Admin') ){
            abort(403);
        }
        $language = display_language($locale);
        if($language->status != 1){
            abort(404);
        }
        $translation_groups = \DB::table('translation_strings')->select('group')->distinct('group')->get();
        $translations = \DB::table('translation_strings')
            ->select([
                'translation_strings.id',
                'translation_strings.group',
                'translation_strings.key',
                'translation_strings.value as default_value',
                'translations.translation_string_id',
                'translations.locale',
                'translations.value'
            ])
            ->leftJoin('translations', function($join)use($locale){
                $join->on('translations.translation_string_id', '=', 'translation_strings.id')
                    ->where('translations.locale', $locale);
            })
            ->when($request->input('group'), function($query)use($request){
                return $query->where('translation_strings.group', $request->input('group'));
            })
            ->when($request->input('q'), function($query)use($request){
                return $query->where('translation_strings.value', 'like', '%'.$request->input('q').'%')->orWhere('translations.value', 'like', '%'.$request->input('q').'%');
            })
            ->when($request->input('group_key'), function($query)use($request){
                return $query->where('translation_strings.key', $request->input('group_key'));
            })
            ->orderBy('translation_strings.group')
            ->orderBy('translation_strings.created_at', 'DESC')
            ->paginate(10);
        $breadcrumbs = [
            'javascript:void(0);' => __('JsPress::backend.pages.translations.title'),
            Route('admin.translation.index', ['locale'=>$locale]) => __('JsPress::backend.pages.translations.list.title', ['language'=>$language->display_language_name])
        ];
        return view('JsPress::admin.translations.index', compact('translations', 'translation_groups', 'language', 'breadcrumbs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        if( !auth()->user()->hasPermissionTo('edit translation') && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('JsPress::backend.permission_error')
            ]);
        }
        $data = $request->input('data');
        Translation::updateOrCreate(
            [
                'translation_string_id' => $data['id'],
                'locale' => $data['locale']
            ],
            [
                'value' => $data['value']
            ]
        );
        $cacheKey = "translation.".$data['locale'].".".$data['group'].".".$data['key'];
        Cache::forget($cacheKey);
        Cache::forever($cacheKey, $data['value']);
        return response()->json([
            'status' => 1,
            'msg' => __('JsPress::backend.pages.translations.success_edit_translation')
        ]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * *
     * @param Request $request
     * @return JsonResponse
     */
    public function destroy(Request $request): JsonResponse
    {
        if( !auth()->user()->hasPermissionTo('delete translation') && !Auth()->user()->hasRole('Super-Admin') ){
            return response()->json([
                'status' => 0,
                'msg' => __('JsPress::backend.permission_error')
            ]);
        }

        $translation_id = $request->input('translation_id');
        $translation_string = TranslationString::find($translation_id);
        foreach( $translation_string->translations as $translation ){
            $cacheKey = "translation.$translation->locale.$translation_string->group.$translation_string->key";
            Cache::forget($cacheKey);
        }
        $translation_string->delete();
        return response()->json([
            'status' => 1,
            'msg' => __('JsPress::backend.pages.translations.success_delete_translation')
        ]);
    }

    /**
     * @param $locale
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function export($locale): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return Excel::download(new TranslationExport($locale), 'translations-'.$locale.'.xlsx');
    }


    /**
     * @param $locale
     * @return \Illuminate\Http\RedirectResponse
     */
    public function import($locale): \Illuminate\Http\RedirectResponse
    {
        Excel::import(new TranslationImport($locale), request()->file('translation'));
        $language = display_language($locale);
        return redirect()->back()->with('success', __('JsPress::backend.pages.translations.success_import', ['language' => $language->display_language_name]));
    }
}
