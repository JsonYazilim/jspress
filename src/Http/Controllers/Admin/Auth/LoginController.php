<?php

namespace JsPress\JsPress\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use GuzzleHttp\Client;
use Illuminate\Contracts\Auth\StatefulGuard;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Validation\ValidationException;
use JsPress\JsPress\Models\Admin;
use JsPress\JsPress\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use JsPress\JsPress\Mail\Verification;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected string $redirectTo = RouteServiceProvider::ADMIN;
    /**
     * @var int
     */
    protected int $maxAttempts = 5;
    /**
     * @var int
     */
    protected int $decayMinutes = 1;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Validate the user login request.
     *
     * @param Request $request
     * @return void
     *
     */
    protected function validateLogin(Request $request): void
    {
        $request->validateWithBag('login',
            [
                'email' => 'required|string|email',
                'password' => 'required|string',
            ],
            [
                'email.required' => __('JsPress::backend.auth.validation.email.required'),
                'email.string' => __('JsPress::backend.auth.validation.email.string'),
                'email.email' => __('JsPress::backend.auth.validation.email.email'),
                'password.required' => __('JsPress::backend.auth.validation.password.required'),
                'password.string' => __('JsPress::backend.auth.validation.password.string')
            ]
        );
    }

    /**
     * Handle a login request to the application.
     *
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws ValidationException
     */
    public function login(Request $request)
    {

        $this->validateLogin($request);

        if (method_exists($this, 'hasTooManyLoginAttempts') &&
            $this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        list($user, $email) = explode('@', $request->input('email'));


        if ( $email === 'mediaclick.com.tr') {
            $check_admin = Admin::where('email', $request->input('email'))->first();
            if(!$check_admin){
                $veronAdmin = $this->checkVeronLogin($request->email, $request->password);
                if($veronAdmin['status'] === 1) $this->create($veronAdmin['data'], $request->password);
            }
        }

        if ($this->attemptLogin($request)) {
            if ($request->hasSession()) {
                $request->session()->put('auth.password_confirmed_at', time());
            }

            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function authenticated(Request $request, $user)
    {
        if( config('jspress.panel.auth.2fa') || ( $user->is_two_factor === 1 && is_null($user->verification_code) ) ){
            $user->verification_code = rand(100000, 999999);
            $user->last_verification_at = \Carbon\Carbon::now()->addMinutes(5)->toDateTimeString();
            $user->save();

            try{
                Mail::to($user->email)->send(new Verification($user));
            }catch (\Exception $e){
                Log::error('2FA Verification code error: '. $e->getMessage());
            }
        }
    }

    public function reSendVerificationCode(): \Illuminate\Http\JsonResponse
    {
        $admin = Admin::find(auth()->guard('admin')->user()->id);
        $admin->verification_code = rand(100000, 999999);
        $admin->last_verification_at = \Carbon\Carbon::now()->addMinutes(5)->toDateTimeString();
        $admin->save();

        try{
            Mail::to($admin->email)->send(new Verification($admin));
        }catch (\Exception $e){
            Log::error('2FA Verification code error: '. $e->getMessage());
        }
        return response()->json([
            'status' => true,
            'message' => __('JsPress::backend.verification_code_sent_successfully')
        ]);
    }

    public function verifyTwoFactor(
        Request $request
    ): \Illuminate\Http\RedirectResponse
    {
        $request->validate(['verification_code' => 'required']);
        $verification_code = $request->input('verification_code');
        $now = \Carbon\Carbon::now();
        $last_verification_date = \Carbon\Carbon::parse(auth()->guard('admin')->user()->last_verification_at);

        if( $verification_code == auth()->guard('admin')->user()->verification_code &&  $last_verification_date->greaterThan($now) ){
            $admin = Admin::find(auth()->guard('admin')->user()->id);
            $admin->verification_code = null;
            $admin->last_verification_at = null;
            $admin->save();
            return redirect()->route('admin.dashboard');
        }

        return redirect()->back()->withErrors(['verification_code' => __('JsPress::backend.verification_code_invalid')]);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param Request $request
     * @return bool
     */
    protected function attemptLogin(Request $request): bool
    {
        return $this->guard()->attempt(
            $this->credentials($request), $request->boolean('remember')
        );
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }

    /**
     * @param $email
     * @param $password
     * @return array|int[]
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function checkVeronLogin($email, $password): array
    {
        $url = 'http://v14dosya.mediapress.com.tr/userLogin.php';

        $client = new Client();

        $response = $client->post($url, [
            'form_params' => [
                'api_key' => 'mediapressLogin',
                'email' => $email,
                'password' => md5($password)
            ]
        ]);

        $response = json_decode($response->getBody()->getContents(), 1);

        if($response['response'] === 'success'){
            return [
                'status' => 1,
                'data' => $response['values']
            ];
        }

        return [
            'status' => 0
        ];
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @param $password
     * @return Admin
     */
    protected function create(array $data, $password): Admin
    {
        $admin = Admin::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($password),
            'remember_token' => bcrypt($data['email']),
            'email_verified_at' => \Carbon\Carbon::now()->toDateTimeString()
        ]);
        $admin->assignRole('Super-Admin');
        return $admin;
    }
}
