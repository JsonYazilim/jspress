<?php

namespace JsPress\JsPress\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\UserMeta;
use Auth;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;
use Socialite;

class SocialiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function redirectToFB()
    {
        session([
            'auth_language' => app()->getLocale()
        ]);
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function handleCallbackFacebook()
    {
        $user = Socialite::driver('facebook')->stateless()->user();
        $socialite_user = User::where('social_id', $user->id)->first();
        app()->setLocale(session('auth_language'));
        request()->session()->forget('auth_language');
        $redirectUrl = \LaravelLocalization::getLocalizedURL(app()->getLocale(), Route('home'));
        if($socialite_user){
            Auth::login($socialite_user);
            return redirect($redirectUrl);
        }
        $newUser = User::create([
            'name' => $user->name,
            'email' => $user->email,
            'social_id'=> $user->id,
            'social_type'=> 'facebook',
            'password' => encrypt('my-facebook')
        ]);
        if($newUser){
            $userName = explode(' ', $user->name);
            UserMeta::create([
                'user_id' => $newUser->id,
                'first_name' => isset($userName[2]) ? $userName[0].' '.$userName[1] : $userName[0],
                'last_name' => isset($userName[2]) ? $userName[2] : $userName[1],
            ]);
        }
        Auth::login($newUser);
        return redirect($redirectUrl);
    }
}
