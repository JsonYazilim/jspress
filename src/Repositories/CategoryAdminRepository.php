<?php

namespace JsPress\JsPress\Repositories;

use JsPress\JsPress\Helpers\JsPressPanel;
use JsPress\JsPress\Interfaces\CategoryAdminInterface;
use JsPress\JsPress\Interfaces\PostExtrasInterface;
use JsPress\JsPress\Interfaces\PostTranslationInterface;
use JsPress\JsPress\Interfaces\SeoInterface;
use JsPress\JsPress\Models\Category;
use JsPress\JsPress\Models\PostField;

class CategoryAdminRepository implements CategoryAdminInterface{

    /**
     * @var null|object
     */
    private ?object $post_type;

    /**
     * @var object
     */
    private object $post_translation;
    private SeoInterface $seo_meta;
    private PostExtrasInterface $post_extra;
    /**
     * @var int|null
     */
    private ?int $id;
    /**
     * @var int
     */
    private int $category_id = 0;
    /**
     * @var string
     */
    private string $title;
    /**
     * @var string|null
     */
    private ?string $slug = null;
    /**
     * @var int|null
     */
    private ?int $author;
    /**
     * @var string|null
     */
    private ?string $content;
    /**
     * @var array|null
     */
    private ?array $seo;
    /**
     * @var int
     */
    private int $status;
    /**
     * @var array|null
     */
    private ?array $extras;
    /**
     * @var string
     */
    private string $publish_date;
    /**
     * @var string
     */
    private string $publish_date_gmt;
    /**
     * @var int
     */
    private int $order = 0;
    /**
     * @var string
     */
    private string $locale;
    /**
     * @var int
     */
    private int $region = 1;
    /**
     * @var int|null
     */
    private ?int $transaction_id = null;
    /**
     * @var string|null
     */
    private ?string $source_locale = null;

    /**
     * @param PostTranslationInterface $postTranslation
     * @param SeoInterface $seo_meta
     * @param PostExtrasInterface $post_extra
     */
    public function __construct(
        PostTranslationInterface $postTranslation,
        SeoInterface $seo_meta,
        PostExtrasInterface $post_extra
    )
    {
        $this->post_translation = $postTranslation;
        $this->seo_meta = $seo_meta;
        $this->post_extra = $post_extra;
    }

    /**
     * @param int|null $id
     * @return CategoryAdminInterface
     */
    public function setId(?int $id): CategoryAdminInterface
    {
        $this->id = $id;
        return $this;
    }


    /**
     * @param int $category_id
     * @return CategoryAdminInterface
     */
    public function setCategoryId(int $category_id): CategoryAdminInterface
    {
        $this->category_id = $category_id;
        return $this;
    }

    /**
     * @param string $title
     * @return CategoryAdminInterface
     */
    public function setTitle(string $title): CategoryAdminInterface
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param string|null $slug
     * @return CategoryAdminInterface
     */
    public function setSlug(?string $slug): CategoryAdminInterface
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @param int|null $author
     * @return CategoryAdminInterface
     */
    public function setAuthor(?int $author): CategoryAdminInterface
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @param string|null $content
     * @return CategoryAdminInterface
     */
    public function setContent(?string $content): CategoryAdminInterface
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @param array|null $seo
     * @return CategoryAdminInterface
     */
    public function setSeo(?array $seo): CategoryAdminInterface
    {
        $this->seo = $seo;
        return $this;
    }

    /**
     * @param int $status
     * @return CategoryAdminInterface
     */
    public function setStatus(int $status): CategoryAdminInterface
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param array|null $extras
     * @return CategoryAdminInterface
     */
    public function setExtras(?array $extras): CategoryAdminInterface
    {
        $this->extras = $extras;
        return $this;
    }

    /**
     * @param string $publish_date
     * @return CategoryAdminInterface
     */
    public function setPublishDate(string $publish_date): CategoryAdminInterface
    {
        $this->publish_date = $publish_date;
        $this->publish_date_gmt = \Carbon\Carbon::parse($publish_date)->setTimezone('UTC');
        return $this;
    }

    /**
     * @param int $order
     * @return CategoryAdminInterface
     */
    public function setOrder(int $order): CategoryAdminInterface
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @param string $locale
     * @return CategoryAdminInterface
     */
    public function setLocale(string $locale): CategoryAdminInterface
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @param int $region
     * @return CategoryAdminInterface
     */
    public function setRegion(int $region): CategoryAdminInterface
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @param int|null $transaction_id
     * @return CategoryAdminInterface
     */
    public function setTransactionId(?int $transaction_id): CategoryAdminInterface
    {
        $this->transaction_id = $transaction_id;
        return $this;
    }

    /**
     * @param string|null $source_locale
     * @return CategoryAdminInterface
     */
    public function setSourceLocale(?string $source_locale): CategoryAdminInterface
    {
        $this->source_locale = $source_locale;
        return $this;
    }


    /**
     * @param string $post_type
     * @return object|null
     */
    public function setPostType(string $post_type):? object
    {
        $this->locale = JsPressPanel::contentLocale();
        $this->post_type = \DB::table('post_types')
            ->select([
                'post_types.id',
                'post_types.key',
                'post_types.name',
                'post_types.post_type_id',
                'post_types.is_category',
                'post_types.is_editor',
                'post_types.is_terms',
                'post_types.is_image',
                'post_types.is_seo',
                'post_types.is_url',
                'post_types.is_category_url',
                'post_types.is_segment_disable',
                'post_types.status',
                'post_type_details.slug',
                'post_type_details.singular_name',
                'post_type_details.plural_name'
            ])
            ->leftJoin('post_type_details', function($join){
                $join->on('post_type_details.post_type_id', '=', 'post_types.id')
                    ->where('model', 'JsPress\JsPress\Models\Category')
                    ->where('post_type_details.status', 1)
                    ->where('locale', $this->locale);
            })
            ->where('post_types.key', $post_type)
            ->first();

        $this->post_type->locale = $this->locale;
        $this->post_type->active_languages = $this->getActiveLanguages($this->post_type->id);
        $this->post_type->other_languages = collect($this->post_type->active_languages)->filter(function($item){
            return $item != $this->locale && in_array($item, website_languages()->pluck('locale')->toArray());
        })->values()->toArray();
        return $this->post_type;
    }

    /**
     * @return void
     */
    public function save(): void
    {
        if(is_null($this->slug)){
            $this->slug = $this->generateSlug(\Str::slug($this->title, '-'));
        }
        $category = Category::updateOrCreate(
            [
                'id' => $this->id
            ],
            [
                'category_id' => $this->category_id,
                'category_type' => $this->post_type->key,
                'title' => $this->title,
                'slug' => $this->slug,
                'content' => $this->content,
                'author' => $this->author,
                'publish_date' => $this->publish_date,
                'publish_date_gmt' => $this->publish_date_gmt,
                'status' => $this->status,
                'order' => $this->order
            ]
        );
        $this->id = $category->id;

        if( $this->post_type->locale == default_language() &&  !is_null($this->source_locale) && !is_null($this->transaction_id) ){
            $categories = $this->getCategoriesTranslation($this->transaction_id);
            foreach($categories as $cat){
                $this->post_translation->setRegion($this->region)
                    ->setLocale($cat->locale)
                    ->setElementId($category->id)
                    ->setType('category')
                    ->setTransactionId($cat->transaction_id)
                    ->setSourceLocale(default_language())
                    ->save();
            }
            $this->source_locale = null;
        }

        $this->post_translation->setRegion($this->region)
            ->setLocale($this->locale)
            ->setElementId($this->id)
            ->setType('category')
            ->setTransactionId($this->transaction_id)
            ->setSourceLocale($this->source_locale)
            ->save();
        $extras = is_null($this->extras) ? [] : $this->extras;
        $this->post_extra->setExtras($extras)
            ->setType('category')
            ->setElementId($category->id)
            ->saveMany();

    }

    /**
     * @param int $id
     * @return array
     */
    public function getCategory(int $id): array
    {

        $category =  Category::select(
                'categories.id',
                'categories.category_id',
                'categories.title',
                'categories.content',
                'categories.slug',
                'categories.author',
                'categories.publish_date',
                'categories.publish_date_gmt',
                'categories.order',
                'categories.status',
                'category_translation.transaction_id',
                'category_translation.locale',
                'category_translation.source_locale',
            )
            ->join('post_translations as category_translation', function($join){
                $join->on('category_translation.element_id', '=', 'categories.id')
                    ->where('category_translation.type', 'category');
            })
            ->where('categories.id', $id)
            ->with(['extras'])
            ->first();
        $category =  json_decode(json_encode($category), TRUE);
        $category['extras'] = $this->prepareExtras($category['extras']);
        return $category;
    }

    /**
     * @param array $category_data
     * @param int $category_id
     * @param int $current_category
     * @return array
     */
    public function getCategories(int $current_category = 0, array &$category_data = [], int $category_id = 0): array
    {
        $categories = $this->queryCategories($category_id, $current_category);
        foreach ($categories as $category) {
            $category_data[$category['id']] = $category;
            //$category_data[$category['id']]['other_languages'] = $this->getLanguageOptions($category);
            $category_data[$category['id']]['children'] = [];
            $this->getCategories($current_category, $category_data[$category['id']]['children'], $category['id']);
        }
        return collect($category_data)->values()->map(function ($category) {
            return $this->reIndexCategories($category);
        })->all();
    }

    /**
     * @param int $post_type_id
     * @return array
     */
    public function getActiveLanguages(int $post_type_id): array
    {
        return \DB::table('post_type_details')
            ->where('post_type_id', $post_type_id)
            ->where('model', 'JsPress\JsPress\Models\Category')
            ->where('status', 1)
            ->get()
            ->pluck('locale')
            ->toArray();
    }

    /**
     * @return array
     */
    public function getOtherLanguagesCategories(): array
    {

        $category_data = [];
        $categories = $this->queryRelatedCategories();
        foreach ($categories as $category) {
            $category_data[$category['id']] = $category;
            $post_translation = \DB::table('post_translations')->where('transaction_id', $category['transaction_id'])->where('locale', $this->post_type->locale)->first();
            $category_data[$category['id']]['translation_exists'] = false;
            if($post_translation){
                $category_data[$category['id']]['translation_exists'] = true;
            }
        }

        return $category_data;
    }

    /**
     * @return void
     */
    public function delete(): void
    {
        if(!is_null($this->id)){
            $this->deleteTranslationRelation($this->id);
            $category = Category::find($this->id);
            $this->seo_meta->setElementId($this->id)->setType('category')->delete();
            $this->post_extra->setElementId($this->id)->setType('category')->deleteItems();
            $this->post_translation->setElementId($this->id)->setType('category')->deleteItem();
            $category->delete();
        }
    }

    /**
     * @param int $category_id
     * @return void
     */
    public function deleteTranslationRelation(int $category_id): void
    {
        $this->post_translation->setRegion($this->region)
            ->setLocale($this->locale )
            ->setElementId($category_id)
            ->setType('category')
            ->setTransactionId($this->transaction_id)
            ->delete();
    }

    /**
     * @param $extras
     * @return array
     */
    private function prepareExtras($extras): array
    {
        $extras_data = [];
        foreach($extras as $extra){
            $extra_value = json_decode($extra['value'], TRUE);
            if(is_array($extra_value)){
                $extras_data[$extra['key']] = $extra_value;
            }else{
                $extras_data[$extra['key']] = $extra['value'];
            }
        }
        return $extras_data;
    }

    /**
     * @param int $transaction_id
     * @return object
     */
    private function getCategoriesTranslation(int $transaction_id): object
    {
        return \DB::table('post_translations')
            ->where('transaction_id', $transaction_id)
            ->where('type', 'category')
            ->whereIn('locale', website_languages()->pluck('locale')->toArray())
            ->get();
    }

    /**
     * @param int $category_id
     * @param $current_category
     * @return mixed
     */
    private function queryCategories(int $category_id, $current_category): mixed
    {
        $categories = \DB::table('categories')
            ->select(
                'categories.id',
                'categories.category_id',
                'categories.title',
                'categories.content',
                'categories.slug',
                'categories.author',
                'categories.publish_date',
                'categories.publish_date_gmt',
                'categories.order',
                'categories.status',
                'category_translation.transaction_id'
            )
            ->join('post_translations as category_translation', function($join){
                $join->on('category_translation.element_id', '=', 'categories.id')
                    ->where('category_translation.type', 'category')
                    ->where('category_translation.region', 1)
                    ->where('category_translation.locale', JsPressPanel::contentLocale());
            })
            ->where('categories.category_type', $this->post_type->key)
            ->where('categories.id', '!=', $current_category)
            ->where('categories.category_id', $category_id)
            ->orderBy('categories.title')
            ->get()
            ->toArray();
        return json_decode(json_encode($categories), TRUE);
    }

    /**
     * @return array
     */
    private function queryRelatedCategories(): array
    {
        $categories = \DB::table('categories')
            ->select(
                'categories.id',
                'categories.title',
                'post_translations.transaction_id',
                'post_translations.locale',
                'current_translations.transaction_id as current_trid_id'
            )
            ->join('post_translations', function($join){
                $join->on('post_translations.element_id', '=', 'categories.id')
                    ->where('post_translations.locale', '!=', $this->post_type->locale)
                    ->whereNull('post_translations.source_locale')
                    ->where('post_translations.type', 'category');
            })
            ->leftJoin('post_translations as current_translations', function($join){
                $join->on('current_translations.transaction_id', '=', 'post_translations.transaction_id')
                    ->where('current_translations.locale', '=', $this->post_type->locale)
                    ->where('current_translations.type', 'category');
            })
            ->where('categories.category_type', $this->post_type->key)
            ->whereNull('current_translations.transaction_id')
            ->whereIn('post_translations.locale', $this->post_type->other_languages)
            ->orderBy('categories.title', 'ASC')
            ->get()
            ->toArray();

        return json_decode(json_encode($categories), TRUE);
    }

    /**
     * @param int $transaction_id
     * @return array
     */
    public function getCategoryTranslation(int $transaction_id): array
    {
        $categories = \DB::table('categories')
            ->select(
                'categories.id',
                'categories.title',
                'post_translations.transaction_id',
                'post_translations.locale'
            )
            ->join('post_translations', function($join){
                $join->on('post_translations.element_id', '=', 'categories.id')
                    ->where('post_translations.locale', '!=', $this->post_type->locale)
                    ->where('post_translations.type', 'category');
            })
            ->where('categories.category_type', $this->post_type->key)
            ->where('post_translations.transaction_id', $transaction_id)
            ->whereIn('post_translations.locale', website_languages()->pluck('locale')->toArray())
            ->orderBy('categories.title', 'ASC')
            ->get()
            ->toArray();

        return json_decode(json_encode($categories), TRUE);
    }

    /**
     * @param array|null $post
     * @return array
     */
    public function getPostField(array|null $post = null): array
    {
        return $this->getPostFields($post);
    }

    /**
     * @param array|null $post
     * @return array
     */
    private function getPostFields(array|null $post): array
    {
        $post_fields = PostField::orderBy('order')->get();
        $post_field_data = [];
        foreach($post_fields as $key => $field){
            $operator = $field->conditions['equal'] == 'is_equal' ? '=' : '!=';
            if(
                $field->conditions['key'] == 'category_type' && dynamic_condition($this->post_type->key, $operator, $field->conditions['value']) ||
                !is_null($post) && ($field->conditions['key'] == 'template' && $this->post_type->key == 'page' && dynamic_condition($post['view'], $operator, $field->conditions['value']))
            ){
                $post_field_data[$key]['title'] = $field->title;
                $post_field_data[$key]['key'] = $field->key;
                $post_field_data[$key]['name'] = $field->key;
                $post_field_data[$key]['description'] = $field->description;
                $post_field_data[$key]['appearence'] = $field->appearence;
                $post_field_data[$key]['is_accordion_open'] = $field->is_accordion_open;
                $post_field_data[$key]['position'] = $field->position;
                $post_field_data[$key]['data'] = $field->data;
                $post_field_data[$key]['order'] = $field->order;
            }
        }
        return collect($post_field_data)->groupBy('position')->toArray();
    }

    /**
     * @param $category
     * @return array
     */
    private function getLanguageOptions($category): array
    {
        $language_data = [];
        foreach($this->post_type->other_languages as $language){
            $translation = \DB::table('post_translations')
                ->where('transaction_id', $category['transaction_id'])
                ->where('type', 'category')
                ->where('region', 1)
                ->where('locale', $language)
                ->first();
            if($translation){
                $language_data[$language] = [
                    'type' => 'edit',
                    'trid' => $translation->transaction_id,
                    'id' => $translation->element_id,
                    'lang' => $translation->locale,
                    'url' => '#'
                ];
            }else{
                $translation = \DB::table('post_translations')
                    ->where('element_id', $category['id'])
                    ->where('type', 'category')
                    ->where('locale', $this->post_type->locale)
                    ->first();
                $language_data[$language] = [
                    'type' => 'add',
                    'trid' => $translation->transaction_id,
                    'url' => route('admin.category.index', ['post-type'=>$this->post_type->key, 'lang'=> $language, 'trid' => $translation->transaction_id, 'source_locale'=>$translation->locale])
                ];

            }
        }
        return $language_data;
    }

    /**
     * @param string $slug
     * @param int $count
     * @param string|null $base_slug
     * @return string
     */
    private function generateSlug(string $slug, int $count = 0, string $base_slug = null): string
    {
        if( $base_slug ){
            $original_slug = $base_slug;
        }else{
            $original_slug = $slug;
        }
        $check_slug = \DB::table('categories')
            ->join('post_translations', function($join){
                $join->on('post_translations.element_id', '=', 'categories.id')
                    ->where('post_translations.type', 'category')
                    ->where('post_translations.locale', '=', JsPressPanel::contentLocale());
            })
            ->where('categories.category_type', '=', $this->post_type->key)
            ->where('categories.slug', '=', $slug)
            ->where('categories.id', '!=', $this->id)
            ->first();
        if($check_slug){
            $count++;
            $slug = $original_slug.'-'.$count;
            return $this->generateSlug($slug, $count, $original_slug);
        }
        return $slug;
    }

    /**
     * @param $category
     * @return mixed
     */
    private function reIndexCategories($category)
    {
        if (isset($category['children'])) {
            $subcategories = collect($category['children']);
            $category['children'] = $subcategories->values()->map(function ($subcategory) {
                return $this->reIndexCategories($subcategory);
            })->all();
        }
        return $category;
    }
}
