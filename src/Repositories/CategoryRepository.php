<?php

namespace JsPress\JsPress\Repositories;

use JsPress\JsPress\DataSources\CategoryDataSource;
use JsPress\JsPress\Interfaces\CategoryInterface;
use JsPress\JsPress\Models\Category;

class CategoryRepository implements CategoryInterface{

    public ?array $category;

    public ?array $extras;

    public ?array $post_type;

    /**
     * @param string $slug
     * @param string $category_type
     * @return object|null
     */
    public function category(string $slug, string $category_type): ?object
    {
        $category = null;
        $category = $this->getCategory($slug, $category_type);
        if(!is_null($category)){
           return new CategoryDataSource($category);
        }
        return null;
    }

    private function getCategory($slug)
    {
        return Category::where('status', 1)
            ->where('slug', $slug)
            ->whereHas('translation', function($query){
                return $query->where('locale', app()->getLocale())
                    ->where('region', 1)->whereIn('locale', website_languages()->pluck('locale')->toArray());
            })
            ->whereHas('post_type_item', function($query){
                return $query->where('status', 1)
                    ->where('is_category_url', 1)
                    ->whereHas('post_type_detail', function($q){
                        return $q->where('region', 1);
                    });
            })
            ->with(['post_type_item', 'post_type_item.post_details'])
            ->first();
    }

}
