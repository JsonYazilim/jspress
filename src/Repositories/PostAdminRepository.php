<?php

namespace JsPress\JsPress\Repositories;

use JsPress\JsPress\Helpers\JsPressPanel;
use JsPress\JsPress\Interfaces\CategoryAdminInterface;
use JsPress\JsPress\Interfaces\PostAdminInterface;
use JsPress\JsPress\Interfaces\PostExtrasInterface;
use JsPress\JsPress\Interfaces\PostTranslationInterface;
use JsPress\JsPress\Interfaces\SeoInterface;
use JsPress\JsPress\Models\Post;
use JsPress\JsPress\Models\PostField;
use JsPress\JsPress\Models\PostView;
use Session;

class PostAdminRepository implements PostAdminInterface{


    /**
     * @var PostTranslationInterface
     */
    private PostTranslationInterface $post_translation;
    /**
     * @var CategoryAdminInterface
     */
    private CategoryAdminInterface $category_admin;
    /**
     * @var PostExtrasInterface
     */
    private PostExtrasInterface $post_extra;
    /**
     * @var SeoInterface
     */
    private SeoInterface $seo_meta;
    /**
     * @var null|object
     */
    private ?object $post_type;
    /**
     * @var int|null
     */
    private ?int $id;
    /**
     * @var int
     */
    private int $post_id = 0;
    /**
     * @var string
     */
    private string $title;
    /**
     * @var string|null
     */
    private ?string $slug = null;
    /**
     * @var int|null
     */
    private ?int $author;
    /**
     * @var string|null
     */
    private ?string $content;
    /**
     * @var array|null
     */
    private ?array $categories = [];
    /**
     * @var array|null
     */
    private ?array $seo;
    /**
     * @var int
     */
    private int $status;
    /**
     * @var array|null
     */
    private ?array $extras;
    /**
     * @var string
     */
    private string $publish_date;
    /**
     * @var string
     */
    private string $publish_date_gmt;
    /**
     * @var int
     */
    private int $order = 0;
    /**
     * @var string
     */
    private string $locale;
    /**
     * @var int
     */
    private int $region = 1;
    /**
     * @var int|null
     */
    private ?int $transaction_id = null;
    /**
     * @var string|null
     */
    private ?string $source_locale = null;
    /**
     * @var int
     */
    private int $is_homepage;

    /**
     * @var string|null
     */
    private ?string $view;

    /**
     * @param PostTranslationInterface $postTranslation
     * @param SeoInterface $seo_meta
     * @param PostExtrasInterface $post_extra
     * @param CategoryAdminInterface $category_admin
     */
    public function __construct(
        PostTranslationInterface $postTranslation,
        SeoInterface $seo_meta,
        PostExtrasInterface $post_extra,
        CategoryAdminInterface $category_admin
    )
    {
        $this->post_translation = $postTranslation;
        $this->seo_meta = $seo_meta;
        $this->post_extra = $post_extra;
        $this->category_admin = $category_admin;
    }
    /**
     * @param int|null $id
     * @return PostAdminInterface
     */
    public function setId(?int $id): PostAdminInterface
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param int $post_id
     * @return PostAdminInterface
     */
    public function setPostId(int $post_id): PostAdminInterface
    {
        $this->post_id = $post_id;
        return $this;
    }

    /**
     * @param string $title
     * @return PostAdminInterface
     */
    public function setTitle(string $title): PostAdminInterface
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param string|null $slug
     * @return PostAdminInterface
     */
    public function setSlug(string|null $slug): PostAdminInterface
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @param int|null $author
     * @return PostAdminInterface
     */
    public function setAuthor(?int $author): PostAdminInterface
    {
        $this->author = $author;
        return $this;
    }


    /**
     * @param string|null $content
     * @return PostAdminInterface
     */
    public function setContent(?string $content): PostAdminInterface
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @param array|null $categories
     * @return PostAdminInterface
     */
    public function setCategories(?array $categories): PostAdminInterface
    {
        $this->categories = $categories;
        return $this;
    }

    /**
     * @param array|null $seo
     * @return PostAdminInterface
     */
    public function setSeo(?array $seo): PostAdminInterface
    {
        $this->seo = $seo;
        return $this;
    }

    /**
     * @param int $status
     * @return PostAdminInterface
     */
    public function setStatus(int $status): PostAdminInterface
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @param array|null $extras
     * @return PostAdminInterface
     */
    public function setExtras(?array $extras): PostAdminInterface
    {
        $this->extras = $extras;
        return $this;
    }

    /**
     * @param string $publish_date
     * @return PostAdminInterface
     */
    public function setPublishDate(string $publish_date): PostAdminInterface
    {
        $this->publish_date = $publish_date;
        $this->publish_date_gmt = \Carbon\Carbon::parse($publish_date)->setTimezone('UTC');
        return $this;
    }

    /**
     * @param int $order
     * @return PostAdminInterface
     */
    public function setOrder(int $order): PostAdminInterface
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @param string $locale
     * @return PostAdminInterface
     */
    public function setLocale(string $locale): PostAdminInterface
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @param int $region
     * @return PostAdminInterface
     */
    public function setRegion(int $region): PostAdminInterface
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @param int|null $transaction_id
     * @return PostAdminInterface
     */
    public function setTransactionId(?int $transaction_id): PostAdminInterface
    {
        $this->transaction_id = $transaction_id;
        return $this;
    }

    /**
     * @param string|null $source_locale
     * @return PostAdminInterface
     */
    public function setSourceLocale(?string $source_locale): PostAdminInterface
    {
        $this->source_locale = $source_locale;
        return $this;
    }

    /**
     * @param int $is_homepage
     * @return PostAdminInterface
     */
    public function setIsHomepage(int $is_homepage): PostAdminInterface
    {
        $this->is_homepage = $is_homepage;
        return $this;
    }

    /**
     * @param string|null $view
     * @return PostAdminInterface
     */
    public function setView(?string $view): PostAdminInterface
    {
        $this->view = $view;
        return $this;
    }



    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }


    /**
     * @param string $post_type
     * @return null|object
     */
    public function setPostType(string $post_type):? object
    {
        $this->locale = JsPressPanel::contentLocale();
        $this->post_type = $this->getPostTypeItem($post_type);
        if(!$this->post_type){
            return null;
        }
        $this->post_type->children = $this->getPostTypeChildren($this->post_type->id);
        $this->post_type->parent = $this->post_type->post_type_id != 0 ? $this->getPostTypeParent($this->post_type->post_type_id) : null;
        $this->post_type->locale = $this->locale;
        $this->post_type->active_languages = $this->getActiveLanguages($this->post_type->id);
        $this->post_type->other_languages = collect($this->post_type->active_languages)->filter(function($item){
            return $item != $this->locale && in_array($item, website_languages()->pluck('locale')->toArray());
        })->values()->toArray();
        $this->post_type->column_list = $this->tableColumns();
        $this->post_type->columns = is_null($this->post_type->columns) ? $this->setDefaultColumns() : json_decode($this->post_type->columns, TRUE);
        $this->post_type->column_options = $this->getTableColumnsOptions();
        return $this->post_type;
    }

    /**
     * @param $post_type
     * @return mixed
     */
    private function getPostTypeItem($post_type): mixed
    {
        return \DB::table('post_types')
            ->select([
                'post_types.id',
                'post_types.key',
                'post_types.name',
                'post_types.post_type_id',
                'post_types.is_category',
                'post_types.is_editor',
                'post_types.is_terms',
                'post_types.is_image',
                'post_types.is_seo',
                'post_types.is_url',
                'post_types.is_segment_disable',
                'post_types.columns',
                'post_types.status',
                'post_type_details.slug',
                'post_type_details.singular_name',
                'post_type_details.plural_name'
            ])
            ->leftJoin('post_type_details', function($join){
                $join->on('post_type_details.post_type_id', '=', 'post_types.id')
                    ->where('post_type_details.status', 1)
                    ->where('post_type_details.model', 'JsPress\JsPress\Models\Post')
                    ->where('locale', JsPressPanel::contentLocale());
            })
            ->where('post_types.key', $post_type)
            ->first();
    }

    /**
     * @param $post_type_id
     * @return mixed
     */
    private function getPostTypeChildren($post_type_id)
    {
        return \DB::table('post_types')
            ->select([
                'post_types.id',
                'post_types.key',
                'post_types.name',
                'post_types.post_type_id',
                'post_types.is_category',
                'post_types.is_terms',
                'post_types.is_image',
                'post_types.is_seo',
                'post_types.is_url',
                'post_types.is_segment_disable',
                'post_types.columns',
                'post_types.status',
                'post_type_details.slug',
                'post_type_details.singular_name',
                'post_type_details.plural_name'
            ])
            ->leftJoin('post_type_details', function($join){
                $join->on('post_type_details.post_type_id', '=', 'post_types.id')
                    ->where('post_type_details.status', 1)
                    ->where('post_type_details.model', 'JsPress\JsPress\Models\Post')
                    ->where('locale', JsPressPanel::contentLocale());
            })
            ->where('post_types.post_type_id', $post_type_id)
            ->get();
    }

    /**
     * @param $post_type_id
     * @return mixed
     */
    private function getPostTypeParent($post_type_id)
    {
        return \DB::table('post_types')
            ->select([
                'post_types.id',
                'post_types.key',
                'post_types.name',
                'post_types.post_type_id',
                'post_types.is_category',
                'post_types.is_terms',
                'post_types.is_image',
                'post_types.is_seo',
                'post_types.is_url',
                'post_types.is_segment_disable',
                'post_types.columns',
                'post_types.status',
                'post_type_details.slug',
                'post_type_details.singular_name',
                'post_type_details.plural_name'
            ])
            ->leftJoin('post_type_details', function($join){
                $join->on('post_type_details.post_type_id', '=', 'post_types.id')
                    ->where('post_type_details.status', 1)
                    ->where('locale', JsPressPanel::contentLocale());
            })
            ->where('post_types.id', $post_type_id)
            ->first();
    }

    /**
     * @param array $data
     * @param bool $trash
     * @return array[]
     */
    public function getTable(array $data, bool $trash = false): array
    {
        $posts = $this->getData($data, $trash);

        return [
            'posts' => $this->convertData($posts),
            'post_paginate' => $posts
        ];
    }

    /**
     * @return array[]
     */
    public function getTableColumns(): array
    {
        if(Session::has('post_columns_'.$this->post_type->key)){
            $columns = Session::get('post_columns_'.$this->post_type->key);
        }else{
            $columns = $this->setDefaultColumns();
        }
        return $columns;
    }

    /**
     * @return array
     */
    public function getPostData(): array
    {
        $post = Post::where('id', $this->id)
            ->with(['seo_meta','categories', 'translation', 'extras'])
            ->first();

        return $this->prepareEdit($post);
    }

    /**
     * @return array
     */
    public function getPost(): array
    {

        $post = \DB::table('posts')
            ->select([
                'posts.id',
                'posts.title',
                'posts.slug',
                'posts.status',
                'posts.order',
                'posts.created_at',
                'posts.updated_at',
                'post_translations.transaction_id'
            ])
            ->join('post_translations', function($join){
                $join->on('post_translations.element_id', '=', 'posts.id')
                    ->where('post_translations.type', 'post')
                    ->where('post_translations.region', 1)
                    ->where('post_translations.locale', $this->post_type->locale);
            })
            ->where('posts.post_type', $this->post_type->key)
            ->where('posts.id', $this->id)
            ->first();

        return json_decode(json_encode($post), TRUE);
    }

    /**
     * @return array|null
     */
    public function getPostByTransactionId():? array
    {

        $post = \DB::table('posts')
            ->select([
                'posts.id',
                'posts.title',
                'posts.slug',
                'posts.status',
                'posts.order',
                'posts.created_at',
                'posts.updated_at',
                'post_translations.transaction_id'
            ])
            ->join('post_translations', function($join){
                $join->on('post_translations.element_id', '=', 'posts.id')
                    ->where('post_translations.type', 'post')
                    ->where('post_translations.transaction_id', $this->transaction_id)
                    ->where('post_translations.region', 1)
                    ->where('post_translations.locale', $this->locale);
            })
            ->where('posts.post_type', $this->post_type->key)
            ->first();

        return json_decode(json_encode($post), TRUE);
    }

    /**
     * @return PostAdminInterface
     */
    public function save(): PostAdminInterface
    {
        if(is_null($this->slug)){
            $this->slug = $this->generateSlug(\Str::slug($this->title, '-'));
        }else{
            $this->slug = \Str::slug($this->slug, '-');
        }
        if($this->is_homepage == 1){
            $this->restoreHomepages();
            $this->slug = '/';
        }
        $post = Post::updateOrCreate(
            [
                'id' => $this->id
            ],
            [
                'post_id' => $this->post_id,
                'post_type' => $this->post_type->key,
                'title' => $this->title,
                'slug' =>  $this->slug,
                'content' => $this->content,
                'author' => $this->author,
                'publish_date' => $this->publish_date,
                'publish_date_gmt' => $this->publish_date_gmt,
                'status' => $this->status,
                'order' => $this->order,
                'is_homepage' => $this->is_homepage,
                'view' => $this->view
            ]
        );

        /* Restore source_locale if default language is not equal to source_locale */
        if( $this->post_type->locale == default_language() && is_null($this->id) && !is_null($this->source_locale) && !is_null($this->transaction_id) ){
            $posts = $this->getPostsTranslation($this->transaction_id);
            foreach($posts as $post_item){

                $this->post_translation->setRegion($this->region)
                    ->setLocale($post_item->locale)
                    ->setElementId($post_item->element_id)
                    ->setType('post')
                    ->setTransactionId($post_item->transaction_id)
                    ->setSourceLocale(default_language())
                    ->save();
            }
            $this->source_locale = null;
        }

        $this->id = $post->id;
        /* Save Translation */
        $this->post_translation->setRegion($this->region)
            ->setLocale($this->locale)
            ->setElementId($this->id)
            ->setType('post')
            ->setTransactionId($this->transaction_id)
            ->setSourceLocale($this->source_locale)
            ->save();

        /* Save Seo Metas */
        if(!is_null($this->seo)){
            $this->saveSeoMeta();
        }

        /* Sync Categories */
        $categories = is_null($this->categories) ? [] : $this->categories;
        $post->categories()->sync($categories);

        /* Save Extras */
        $extras = is_null($this->extras) ? [] : $this->extras;
        $this->post_extra->setExtras($extras)
            ->setType('post')
            ->setElementId($post->id)
            ->saveMany();


        return $this;
    }

    /**
     * @return void
     */
    private function restoreHomepages(): void
    {
        $posts = \DB::table('posts')
            ->select([
                'posts.id',
                'posts.title'
            ])
            ->join('post_translations', function($join){
                $join->on('post_translations.element_id', '=', 'posts.id')
                    ->where('post_translations.type', 'post')
                    ->where('post_translations.region', 1)
                    ->where('post_translations.locale', $this->locale);
            })
            ->where('posts.is_homepage', 1)
            ->where('posts.post_type', $this->post_type->key)
            ->where('posts.id', '!=', $this->id)
            ->get();

        foreach($posts as $post){
            $post_item = Post::find($post->id);
            $post_item->slug = $this->generateSlug(\Str::slug($post->title, '-'));
            $post_item->is_homepage = 0;
            $post_item->save();
        }
    }

    /**
     * @return void
     */
    public function destroy(): void
    {
        $post = Post::find($this->id);
        $post->status = 2;
        $post->save();
        $post->delete();
    }

    /**
     * @return void
     */
    public function restore(): void
    {
        $post = Post::withTrashed()->find($this->id);
        $post->status = 1;
        $post->save();
        $post->restore();
    }

    /**
     * @return void
     */
    public function delete(): void
    {
        if(!is_null($this->id)){
            $this->deleteTranslationRelation();
            $post = Post::onlyTrashed()->find($this->id);
            $this->seo_meta->setElementId($this->id)->setType('post')->delete();
            $this->post_extra->setElementId($this->id)->setType('post')->deleteItems();
            $this->post_translation->setElementId($this->id)->setType('post')->deleteItem();
            $post->forceDelete();
        }
    }

    /**
     * @return void
     */
    public function deleteTranslationRelation(): void
    {
        $this->post_translation->setRegion($this->region)
            ->setLocale($this->post_type->locale)
            ->setElementId($this->id)
            ->setType('post')
            ->setTransactionId($this->transaction_id)
            ->delete();
    }


    /**
     * @param $post
     * @return array
     */
    private function prepareEdit($post): array
    {
        $post_data = collect($post)->filter(function($item, $key){
            return !in_array($key, ['seo_meta', 'categories', 'translation', 'extras']);
        })->toArray();
        $post_data['seo'] = $this->prepareSeoMeta($post->seo_meta);
        app()->setLocale($post->translation->locale);
        $post_data['url'] = get_the_view_url($post, $this->post_type);
        app()->setLocale(JsPressPanel::contentLocale());
        $post_data['extras'] = $this->prepareExtras($post->extras);
        $post_data['translation'] = $post->translation->toArray();
        $post_data['categories'] = $post->categories->pluck('id')->toArray();

        return $post_data;
    }

    /**
     * @return void
     */
    private function saveSeoMeta(): void
    {
        foreach($this->seo as $key => $value){
            $seo_meta = $this->seo_meta->setElementType('post')->setElementId($this->id);
            if($key == 'title'){
                $seo_meta->setType('name')
                    ->setKey('title')
                    ->setContent($value)
                    ->save();
            }
            if($key == 'description'){
                $seo_meta->setType('name')
                    ->setKey('description')
                    ->setContent($value)
                    ->save();
            }
            if( $key == 'facebook' ){
                foreach($value as $k => $v){
                    $seo_meta->setType('property')
                        ->setPrefix('og')
                        ->setKey($k)
                        ->setContent($v)
                        ->save();
                }
            }
            if( $key == 'twitter' ){
                $seo_meta->setType('name')
                    ->setPrefix('twitter')
                    ->setKey('card')
                    ->setContent('summary_large_image')
                    ->save();
                foreach($value as $k => $v){
                    $seo_meta->setType('name')
                        ->setPrefix('twitter')
                        ->setKey($k)
                        ->setContent($v)
                        ->save();
                }
            }
        }
    }

    /**
     * @param $seo_metas
     * @return array
     */
    private function prepareSeoMeta($seo_metas): array
    {
        $seo_data = [];
        foreach($seo_metas as $meta){
            if( is_null($meta['prefix'])){
                if( $meta['key'] == 'title' ){
                    $seo_data['title'] = $meta['content'];
                }
                if( $meta['key'] == 'description' ){
                    $seo_data['description'] = $meta['content'];
                }
            }elseif( $meta['prefix'] == 'og' ){
                if( $meta['key'] == 'title' ){
                    $seo_data['facebook']['title'] = $meta['content'];
                }
                if( $meta['key'] == 'description' ){
                    $seo_data['facebook']['description'] = $meta['content'];
                }
                if( $meta['key'] == 'image' ){
                    $seo_data['facebook']['image'] = $meta['content'];
                }
            }elseif( $meta['prefix'] == 'twitter' ){
                if( $meta['key'] == 'title' ){
                    $seo_data['twitter']['title'] = $meta['content'];
                }
                if( $meta['key'] == 'description' ){
                    $seo_data['twitter']['description'] = $meta['content'];
                }
                if( $meta['key'] == 'image' ){
                    $seo_data['twitter']['image'] = $meta['content'];
                }
            }
        }
        return $seo_data;
    }

    /**
     * @param $extras
     * @return array
     */
    private function prepareExtras($extras): array
    {
        $extras_data = [];
        $extra_array = $extras->groupBy('key');
        foreach($extra_array as $key => $extra){
            if($extra->count() > 1){
                $extras_data[$key] = $extra->pluck('value')->toArray();
            }else{
                $extra_item = $extra->first()->value;
                if(is_json($extra_item)){
                    $extras_data[$key] = json_decode($extra_item, TRUE);
                }else{
                    $extras_data[$key] = $extra_item;
                }

            }
        }

        return $extras_data;
    }

    /**
     * @param object $post_type
     * @return array
     */
    public function prepare(object $post_type): array
    {

        if($post_type->is_segment_disable == 1){
            if($this->post_type->locale != default_language()){
                $url = url('').'/'.$this->post_type->locale.'/';
            }else{
                $url = url('').'/';
            }
        }else{
            if($this->post_type->locale != default_language()){
                $url =  url($this->post_type->locale).'/'.$post_type->slug.'/';
            }else{
                $url =  url($post_type->slug).'/';
            }
        }
        return [
            'key' => $post_type->key,
            'is_url' => $post_type->is_url,
            'root_url' => $url,
            'is_category' => $post_type->is_category,
            'is_editor' => $post_type->is_editor,
            'is_seo' => $post_type->is_seo,
            'is_image' => $post_type->is_image,
            'is_terms' => $post_type->is_terms
        ];
    }

    /**
     * @param array|null $post
     * @return array
     */
    public function getPostField(array|null $post = null): array
    {
        return $this->getPostFields($post);
    }

    /**
     * @param array|null $post
     * @return array
     */
    private function getPostFields(array|null $post): array
    {
        $post_fields = PostField::orderBy('order')->get();
        $post_field_data = [];
        foreach($post_fields as $key => $field){
            $operator = $field->conditions['equal'] == 'is_equal' ? '=' : '!=';
            if(
                $field->conditions['key'] == 'post_type' && dynamic_condition($this->post_type->key, $operator, $field->conditions['value']) ||
                !is_null($post) && ($field->conditions['key'] == 'template' && $this->post_type->key == 'page' && dynamic_condition($post['view'], $operator, $field->conditions['value']))
            ){
                $post_field_data[$key]['title'] = $field->title;
                $post_field_data[$key]['key'] = $field->key;
                $post_field_data[$key]['name'] = $field->key;
                $post_field_data[$key]['description'] = $field->description;
                $post_field_data[$key]['appearence'] = $field->appearence;
                $post_field_data[$key]['is_accordion_open'] = $field->is_accordion_open;
                $post_field_data[$key]['position'] = $field->position;
                $post_field_data[$key]['data'] = $field->data;
                $post_field_data[$key]['order'] = $field->order;
            }
        }
        return collect($post_field_data)->groupBy('position')->toArray();
    }

    /**
     * @param int $transaction_id
     * @return object
     */
    private function getPostsTranslation(int $transaction_id): object
    {
        return \DB::table('post_translations')
            ->where('transaction_id', $transaction_id)
            ->where('type', 'post')
            ->whereIn('locale', website_languages()->pluck('locale')->toArray())
            ->get();
    }

    /**
     * @param object $posts
     * @return array
     */
    private function convertData(object $posts): array
    {
        $post_data = [];

        foreach($posts as $key => $post){
            foreach($post as $k => $p){
                if(in_array($k, $this->post_type->column_list)){
                    if($k == 'status'){
                        $post_data[$key][$k] = status_badge($p);
                    }else{
                        $post_data[$key][$k] = $p;
                    }
                }
            }
            if(count($this->post_type->other_languages) > 0){
                $post_data[$key]['languages'] = $this->getLanguageOptions($post);
            }
            if($this->post_type->is_category == 1){
                $categories = get_the_post_category($post->id, JsPressPanel::contentLocale());
                if(count($categories) > 0){
                    $category_data = [];
                    foreach($categories as $category){
                        $category_data[] = $category['title'];
                    }
                    $post_data[$key]['category'] = implode(',',$category_data);
                }else{
                    $post_data[$key]['category'] = '-';
                }
            }
            $post_data[$key]['url'] = $this->getTheViewUrl($post, $this->post_type);
            $post_data[$key]['post_id'] = $post->post_id;
            $count = PostView::where('element_id', $post->id)->where('type', 'post')->where('locale', JsPressPanel::contentLocale())->count();
            $post_data[$key]['post_view_count'] = $count;

        }

        return $post_data;
    }

    /**
     * @param $post_item
     * @param $post_type
     * @return string
     */
    private function getTheViewUrl($post_item, $post_type): string
    {
        if(JsPressPanel::contentLocale() == default_language() && config('jspress.language.hide_url_default_language')){
            $base_url = url('/');
        }else{
            $base_url = url(JsPressPanel::contentLocale());
        }

        if($post_type->is_url == 0){
            return 'javascript:void(0);';
        }
        if($post_type->is_segment_disable == 1 && $post_item->post_id == 0){
            return $base_url.'/'.$post_item->slug;
        }
        if( $post_type->is_segment_disable == 0 && $post_item->post_id == 0 ){
            return $base_url.'/'.$post_type->slug.'/'.$post_item->slug;
        }
        if( $post_item->post_id != 0 ){
            $post = get_the_post_without_status($post_item->post_id);
            return $base_url.'/'.$post->slug.'/'.$post_item->slug;
        }

        return 'javascript:void(0);';
    }

    /**
     * @param array $data
     * @param $trash
     * @return object
     */
    private function getData(array $data, $trash): object
    {
        $permission_all = !auth()->user()->hasPostPermission('all', $this->post_type->id) && !Auth()->user()->hasRole('Super-Admin');
        $order_session_key = $this->post_type->key.'_post_order';
        return \DB::table('posts')
            ->select([
                'posts.id',
                'posts.post_id',
                'posts.title',
                'posts.slug',
                'posts.status',
                'posts.order',
                'posts.created_at',
                'posts.updated_at',
                'admins.name as author',
                'post_translations.transaction_id',
            ])
            ->join('admins', function($join){
                $join->on('admins.id', '=', 'posts.author');
            })
            ->join('post_translations', function($join){
                $join->on('post_translations.element_id', '=', 'posts.id')
                    ->where('post_translations.type', 'post')
                    ->where('post_translations.region', 1)
                    ->where('post_translations.locale', JsPressPanel::contentLocale());;
            })
            ->when($permission_all, function($query){
                return $query->where('author', auth()->user()->id);
            })
            ->where('posts.post_type', $this->post_type->key)
            ->when(isset($data['post_id']), function($query)use($data){
                return $query->where('posts.post_id', $data['post_id']);
            })
            ->when($trash, function($query){
                return $query->whereNotNull('posts.deleted_at');
            })
            ->when(!$trash, function($query){
                return $query->whereNull('posts.deleted_at');
            })
            ->when(isset($data['q']), function($query)use($data){
                return $query->where('title', 'like', '%'.$data['q'].'%');
            })
            ->when(isset($data['category']), function($query)use($data){
                return $query->join('category_post', function($join)use($data){
                    return $join->on('category_post.post_id', '=', 'posts.id')->where('category_id', $data['category']);
                });
            })
            ->when(session($order_session_key), function($query)use($order_session_key){
                $sort_explode = explode('-', session($order_session_key));
                return $query->orderBy($sort_explode[0], $sort_explode[1]);
            })
            ->when(!session($order_session_key), function($query){
                return $query->orderBy('created_at', 'DESC');
            })
            ->paginate(20);
    }

    /**
     * @return array[]
     */
    private function setDefaultColumns(): array
    {
        $columns = [
            [
                'name' => 'ID',
                'key' => 'id',
                'width' => 60,
                'order' => 1
            ],
            [
                'name' => __('JsPress::backend.title'),
                'key' => 'title',
                'width' => 250,
                'order' => 2
            ],
            [
                'name' => __('JsPress::backend.author'),
                'key' => 'author',
                'width' => 100,
                'order' => 5
            ],
            [
                'name' => __('JsPress::backend.status'),
                'key' => 'status',
                'width' => 100,
                'order' => 6
            ],
            [
                'name' => __('JsPress::backend.post_views'),
                'key' => 'post_view_count',
                'width' => 50,
                'order' => 6
            ],
            [
                'name' => __('JsPress::backend.order'),
                'key' => 'order',
                'width' => 50,
                'order' => 7
            ],
            [
                'name' => __('JsPress::backend.created_at'),
                'key' => 'created_at',
                'width' => 100,
                'order' => 8
            ]
        ];
        if(count($this->post_type->other_languages) > 0){
            $langauge_html = '<div class="d-flex w-100">';
            foreach($this->post_type->other_languages as $language){
                $langauge_html .= '<img src="'.asset("/jspress/assets/media/flags/svg/".$language.".svg").'" width="20" height="20" class="me-2"/>';
            }
            $langauge_html .= '</div>';

            $columns[] = [
                'name' => $langauge_html,
                'key' => 'languages',
                'width' => 150,
                'order' => 3
            ];
        }
        if($this->post_type->is_category == 1){
            $columns[] = [
                'name' => __('JsPress::backend.categories'),
                'key' => 'category',
                'width' => 150,
                'order' => 4
            ];
        }

        return collect($columns)->sortBy('order')->values()->all();
    }

    /**
     * @return array
     */
    private function tableColumns(): array
    {
        $column_data = [];
        $default_columns = $this->setDefaultColumns();
        foreach($default_columns as $column){
            $column_data[] = $column['key'];
        }
        return $column_data;
    }

    /**
     * @return array
     */
    private function getTableColumnsOptions(): array
    {
        $column_data = [];
        $default_columns = $this->setDefaultColumns();
        foreach($default_columns as $column){
            $column_data[] = [
                'key' => $column['key'],
                'name' => $column['name']
            ];
        }
        return $column_data;
    }

    /**
     * @param int $post_type_id
     * @return array
     */
    public function getActiveLanguages(int $post_type_id): array
    {
        if($this->post_type->key == 'page'){
            $active_langauges = website_languages()->pluck('locale')->toArray();
        }else{
            $active_langauges = \DB::table('post_type_details')
                ->where('post_type_id', $post_type_id)
                ->where('model', 'JsPress\JsPress\Models\Post')
                ->where('status', 1)
                ->get()
                ->pluck('locale')
                ->toArray();
        }

        return $active_langauges;
    }

    /**
     * @param $id
     * @return mixed
     */
    private function getPostDataById($id)
    {
        return \DB::table('posts')
            ->select([
                'posts.id',
                'posts.post_id',
                'posts.title',
                'post_translations.transaction_id'
            ])
            ->join('post_translations', function($join){
                $join->on('post_translations.element_id', '=', 'posts.id')
                    ->where('post_translations.type', 'post');
            })
            ->where('posts.id', $id)
            ->first();
    }


    /**
     * @param $post
     * @return array
     */
    private function getLanguageOptions($post): array
    {

        $language_data = [];
        foreach($this->post_type->other_languages as $language){
            $translation = \DB::table('post_translations')
                ->where('transaction_id', $post->transaction_id)
                ->where('type', 'post')
                ->where('region', 1)
                ->where('locale', $language)
                ->first();

            if($translation){
                $url_data['id'] = $translation->element_id;
                $url_data['post-type'] = $this->post_type->key;
                $url_data['lang'] = $language;
                if($post->post_id != 0){
                    $post = $this->getPostDataById($translation->element_id);
                    $url_data['post_id'] = $post->post_id;
                }
                $language_data[$language] = [
                    'type' => 'edit',
                    'trid' => $translation->transaction_id,
                    'id' => $translation->element_id,
                    'lang' => $translation->locale,
                    'url' => route('admin.post.edit', $url_data)
                ];
            }else{
                if($post->post_id != 0 && $this->post_type->key != 'page'){
                    $parent_post = $this->getPostDataById($post->post_id);
                    $other_translation = \DB::table('post_translations')
                        ->where('transaction_id', $parent_post->transaction_id)
                        ->where('type', 'post')
                        ->where('region', 1)
                        ->where('locale', $language)
                        ->first();

                    if(is_null($other_translation)){
                        $language_data[$language] = [
                            'type' => 'need_parent'
                        ];
                    }else{
                        $translation = \DB::table('post_translations')
                            ->where('element_id', $post->id)
                            ->where('type', 'post')
                            ->where('locale', $this->post_type->locale)
                            ->first();

                        $url_data['post-type'] = $this->post_type->key;
                        $url_data['trid'] = $translation->transaction_id;
                        $url_data['post_id'] = $other_translation->element_id;
                        $url_data['lang'] = $language;
                        if(is_null($translation->source_locale)){
                            $url_data['source_locale'] = $this->post_type->locale;
                        }else{
                            $url_data['source_locale'] = $translation->source_locale;
                        }
                        $language_data[$language] = [
                            'type' => 'add',
                            'trid' => $translation->transaction_id,
                            'url' => route('admin.post.add', $url_data)
                        ];
                    }
                }else{
                    $translation = \DB::table('post_translations')
                        ->where('element_id', $post->id)
                        ->where('type', 'post')
                        ->where('locale', $this->post_type->locale)
                        ->first();
                    $url_data['post-type'] = $this->post_type->key;
                    $url_data['trid'] = $translation->transaction_id;
                    $url_data['lang'] = $language;
                    if(is_null($translation->source_locale)){
                        $url_data['source_locale'] = $this->post_type->locale;
                    }else{
                        $url_data['source_locale'] = $translation->source_locale;
                    }
                    $language_data[$language] = [
                        'type' => 'add',
                        'trid' => $translation->transaction_id,
                        'url' => route('admin.post.add', $url_data)
                    ];
                }
            }
        }
        return $language_data;
    }

    /**
     * @param string $slug
     * @param int $count
     * @param string|null $base_slug
     * @return string
     */
    public function generateSlug(string $slug, int $count = 0, string $base_slug = null): string
    {
        if( $base_slug ){
            $original_slug = $base_slug;
        }else{
            $original_slug = $slug;
        }
        $parent_post_slug = false;
        $check_slug = $this->checkSlug($slug, $this->post_type->key);
        if($this->post_id !== 0){
            $post = Post::find($this->post_id);
            $check_slug = $this->checkDeepSlug($slug, $post->slug);
        }

        if($check_slug){
            $count++;
            $slug = $original_slug.'-'.$count;
            return $this->generateSlug($slug, $count, $original_slug);
        }
        return $slug;
    }

    /**
     * @param $slug
     * @param $post_type
     * @return mixed
     */
    private function checkSlug($slug, $post_type): mixed
    {
        return \DB::table('posts')
            ->join('post_translations', function($join){
                $join->on('post_translations.element_id', '=', 'posts.id')
                    ->where('post_translations.type', 'post')
                    ->where('post_translations.locale', '=', $this->locale);
            })
            ->where('posts.post_type', '=', $post_type)
            ->where('posts.slug', '=', $slug)
            ->first();
    }

    /**
     * @param $slug
     * @param $parent_slug
     * @return mixed
     */
    private function checkDeepSlug($slug, $parent_slug): mixed
    {
        return \DB::table('posts')
            ->select('posts.*', 'post_parent.slug as parent_slug')
            ->join('post_translations', function($join){
                $join->on('post_translations.element_id', '=', 'posts.id')
                    ->where('post_translations.type', 'post')
                    ->where('post_translations.locale', '=', JsPressPanel::contentLocale());
            })
            ->join('posts as post_parent', function($join){
                $join->on('post_parent.id', '=', 'posts.post_id');
            })
            ->where('post_parent.slug', $parent_slug)
            ->where('posts.slug', '=', $slug)
            ->first();

    }
}
