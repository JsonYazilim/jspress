<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PostRepository.php
 * Author: Json Yazılım
 * Class: PostRepository.php
 * Current Username: Erdinc
 * Last Modified: 31.07.2023 21:23
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Repositories;

use Jenssegers\Agent\Agent;
use JsPress\JsPress\DataSources\PostDataSource;
use JsPress\JsPress\Interfaces\PostInterface;
use JsPress\JsPress\Interfaces\PostViewInterface;
use JsPress\JsPress\Models\Post;

class PostRepository implements PostInterface{

    private PostViewInterface $post_view;
    public ?array $seo_meta;

    public ?array $post;

    public ?array $extras;

    public ?array $post_type;
    public ?array $post_type_detail;


    public function __construct(PostViewInterface $post_view){
        $this->post_view = $post_view;
    }

    /**
     * @param string $slug
     * @param int $parent_id
     * @return object|null
     */
    public function post(string $slug, int $parent_id = 0, $post_type = null): ?object
    {
        $post = null;
        $post = $this->getPost($slug, $parent_id, $post_type);
        if(!is_null($post)){
            $this->createTracks($post);
            return new PostDataSource($post);
        }
        return null;
    }

    private function getPost($slug, $parent_id, $post_type)
    {
        return Post::where('status', 1)
            ->where('slug', $slug)
            ->where('post_id', $parent_id)
            ->when(!is_null($post_type), function($query) use ($post_type){
                return $query->where('post_type', $post_type);
            })
            ->whereHas('translation', function($query){
                return $query->where('locale', app()->getLocale())->where('region', 1)->whereIn('locale', website_languages()->pluck('locale')->toArray());
            })
            ->whereHas('post_type_item', function($query){
                return $query->where('status', 1)
                    ->where('is_url', 1)
                    ->whereHas('post_type_detail', function($q){
                        return $q->where('region', 1);
                    });
            })
            ->with(['parent', 'children','seo_meta','post_type_item', 'post_type_item.post_details', 'categories', 'extras'])
            ->first();
    }

    public function homepage():? object
    {
        $post = Post::where('status', 1)
            ->whereHas('translation', function($query){
                return $query->where('locale', app()->getLocale())->where('region', 1)->whereIn('locale', website_languages()->pluck('locale')->toArray());
            })
            ->where('is_homepage', 1)
            ->where('post_type', 'page')
            ->with(['seo_meta','post_type_item', 'post_type_item.post_details', 'translation', 'extras'])
            ->first();
        $this->createTracks($post);
        $post_data = json_decode(json_encode($post), TRUE);

        if(!is_null($post)){
            return new PostDataSource($post);
        }
        abort(404);
    }

    private function createTracks($post): void
    {
        $path = request()->path();
        if($path !== '/'){
            $path = '/'.$path;
        }

        $agent = new Agent();
        if(!$agent->isRobot()){
            $this->post_view->setLocale(app()->getLocale())
                ->setElementId($post->id)
                ->setUrl($path)
                ->setType('post')
                ->setIp(request()->ip())
                ->setRegion(1)
                ->setBrowser($agent->browser())
                ->setDevice($agent->deviceType())
                ->setDeviceName($agent->device())
                ->setPlatform($agent->platform())
                ->setUserAgent(request()->userAgent())
                ->save();
        }
    }

}
