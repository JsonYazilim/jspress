<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: MediaRepository.php
 * Author: Json Yazılım
 * Class: MediaRepository.php
 * Current Username: Erdinc
 * Last Modified: 3.07.2023 10:31
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Repositories;

use Illuminate\Support\Facades\Cache;
use JsPress\JsPress\Interfaces\MediaInterface;
use JsPress\JsPress\Models\Admin;
use JsPress\JsPress\Models\Media;
use JsPress\JsPress\Models\MediaDetail;
use Illuminate\Support\Facades\File;

class MediaRepository implements MediaInterface{


    /**
     * @var array|object
     */
    protected array|object $website_languages;
    /**
     * @var array
     */
    protected array $display_language = [];

    /**
     * @var array|string[]
     */
    public array $mimeTypes = [
        'image/bmp',
        'image/gif',
        'image/jpeg',
        'image/png',
        'image/svg+xml',
        'image/svg',
        'image/tiff',
        'image/x-icon',
        'audio/mpeg',
        'audio/mp3',
        'audio/x-wav',
        'audio/mid',
        'audio/basic',
        'video/mpeg',
        'video/mp4',
        'video/x-msvideo',
        'application/pdf',
        'application/json',
        'text/plain',
        'text/csv',
        'application/docx',
        'application/msword',
        'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'application/excel',
        'application/vnd.ms-excel',
        'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'application/zip',
        'application/octet-stream',
        'application/x-zip-compressed',
        'multipart/x-zip',
        '.zip'
    ];

    /**
     * @var object
     */
    private object $file;

    /**
     * @var int|null
     */
    private ?int $id = null;

    /**
     * @var string
     */
    private string $upload_name;

    /**
     * @var string
     */
    private string $file_name;

    /**
     * @var string
     */
    private string $extension;

    /**
     * @var string
     */
    private string $mime;

    /**
     * @var string
     */
    private string $type;

    /**
     * @var int|null
     */
    private ?int $width = null;

    /**
     * @var int|null
     */
    private ?int $height = null;

    /**
     * @var int|null
     */
    private ?int $size = null;

    /**
     * @var string
     */
    private string $directory;

    /**
     * @var string
     */
    private string $path;
    /**
     * @var int
     */
    private int $user_id;


    public function __construct()
    {
        $this->website_languages = website_languages();
        foreach( $this->website_languages as $language ){
            $this->display_language[$language->locale] = display_language($language->locale);
        }
    }

    /**
     * @param array|null $data
     * @return array
     */
    public function getData(array|null $data): array
    {
        $filter = $data['data']['filter'] ?? false;
        $files = Media::when($data['term'], function($query)use($data){
                return $query->where('upload_name', 'like', '%'.$data['term'].'%');
            })
            ->when(!Auth()->user()->hasRole('Super-Admin') && !auth()->user()->hasPermissionTo('all media'), function($query){
                return $query->where('user_id', auth()->user()->id);
            })
            ->when($filter && is_array($filter), function($query)use($filter){
                return $query->when($filter['key'], function($q)use($filter){
                    return $q->where('file_name', 'like', $filter['key'].'%');
                })->when($filter['file_type'], function($q)use($filter){
                    return $q->where('type', $filter['file_type']);
                })->when($filter['start_date'], function($q)use($filter){
                    return $q->whereDate('created_at', '>=', $filter['start_date']);
                })->when($filter['end_date'], function($q)use($filter){
                    return $q->whereDate('created_at', '<=', $filter['end_date']);
                })->when($filter['order'], function($q)use($filter){
                    $order_field = explode('-', $filter['order']);
                    return $q->orderBy($order_field[0], $order_field[1]);
                });
            })
            ->orderBy('id', 'DESC')
            ->paginate($data['pagination'] ?? 18);
        return $this->transformFiles($files);
    }

    /**
     * @param int $id
     * @param array $details
     * @return void
     */
    public function saveDetails(int $id, array $details): void
    {
        foreach($details as $key => $detail){
            MediaDetail::updateOrCreate(
                [
                    'media_id' => $id,
                    'lang' => $key
                ],
                [
                    'title' => $detail['title'],
                    'tag' => $detail['tag'],
                    'caption' => $detail['caption']
                ]
            );
        }
        Cache::forget('media_'.$id);
    }

    /**
     * @return Media
     */
    public function save(): Media
    {
        return Media::create([
            'user_id' => $this->user_id,
            'upload_name' => $this->upload_name,
            'file_name' => $this->file_name,
            'extension' => $this->extension,
            'mime' => $this->mime,
            'type' => $this->type,
            'width' => $this->width,
            'height' => $this->height,
            'size' => $this->size,
            'path' => $this->path,
            'original_url' => $this->directory
        ]);
    }

    /**
     * @return self
     */
    public function move(): self
    {
        if(!File::exists(public_path($this->path))){
            try{
                File::makeDirectory(public_path($this->path), 0777, true, true);
            }catch (\Exception $e){

            }
        }
        $file = $this->file;
        $file->move(public_path($this->path), $this->upload_name);
        if(config('jspress.image.webp')){
            $dir = $this->path.'/'.$this->upload_name;
            $path = $this->path.'/'.$this->file_name;
            $this->convertWebp($dir, $path);
        }
        return $this;
    }


    /**
     * @return $this
     */
    public function crop(): self
    {
        $image_type = strtolower($this->extension);
        $image_format  = get_image_mime_type($this->directory);

        if( $image_format == 'image/jpg' || $image_format === 'image/jpeg' || $image_format === 'image/png' ){
            if( $image_format === 'image/png' ){
                $img = @imagecreatefrompng(public_path($this->directory));
            }elseif($image_format === 'image/webp' ){
                $img = @imagecreatefromwebp(public_path($this->directory));
            }elseif($image_format === 'image/bmp' ){
                $img = @imagecreatefrombmp(public_path($this->directory));
            }elseif($image_format === 'image/xbm' ){
                $img = @imagecreatefromxbm(public_path($this->directory));
            }else{
                $img = @imagecreatefromjpeg(public_path($this->directory));
            }
            $width = imagesx($img);
            $height = imagesy($img);
            $original_aspect = $width / $height;
            foreach( config('jspress.files.images.sizes') as $size ){
                $w = $size['width'];
                $h = $size['height'];
                $size_aspect = $w / $h;
                if ( $original_aspect >= $size_aspect ){
                    $new_height = $h;
                    $new_width = $width / ($height / $h);
                }else{
                    $new_width = $w;
                    $new_height = $height / ($width / $w);
                }
                $cropped_image = imagecreatetruecolor( $w, $h );
                $file_name = $this->path.'/'.$this->file_name.'-'.$w.'-'.$h.'.'.$this->extension;
                imagecopyresampled($cropped_image,
                    $img,
                    0 - ($new_width - $w) / 2, // Center the image horizontally
                    0 - ($new_height - $h) / 2, // Center the image vertically
                    0, 0,
                    $new_width, $new_height,
                    $width, $height);
                if( $image_format === 'image/png' ){
                    imagepng($cropped_image, public_path($file_name));
                }else{
                    imagejpeg($cropped_image, public_path($file_name), 80);
                }
                if(config('jspress.image.webp')){
                    $dir = $file_name;
                    $path = $this->path.'/'.$this->file_name.'-'.$w.'-'.$h;
                    $this->convertWebp($dir, $path);
                }
            }
        }
        return $this;
    }

    /**
     * @param int $id
     * @return array|null
     */
    public function getDetail(int $id): array|null
    {
        $file = \DB::table('media')->where('id', $id)->first();
        if(is_null($file)) return null;
        return $this->convertFileData($file);
    }

    /**
     * @param object $files
     * @return array
     */
    private function transformFiles(object $files): array
    {
        $data['current_page'] = $files->currentPage();
        $data['last_page'] = $files->lastPage();
        $data['links'] = $files->links('JsPress::vendor.paginator-js')->render();
        $data['total_files'] = $files->count();
        foreach($files as $file){
            $data['files'][] = $this->convertFileData($file);
        }
        return $data;
    }

    /**
     * @param $dir
     * @param $path
     * @return void
     */
    private function convertWebp($dir, $path): void
    {
        $image_format  = get_image_mime_type($dir);
        if( $image_format == 'image/jpg' || $image_format === 'image/jpeg' || $image_format === 'image/png' || $image_format === 'image/bmp' || $image_format === 'image/xbm' ){
            if( $image_format === 'image/png' ){
                $img = @imagecreatefrompng(public_path($dir));
                imagepalettetotruecolor($img);
            }elseif($image_format === 'image/webp' ){
                return ;
            }elseif($image_format === 'image/bmp' ){
                $img = @imagecreatefrombmp(public_path($dir));
            }elseif($image_format === 'image/xbm' ){
                $img = @imagecreatefromxbm(public_path($dir));
            }else{
                $img = @imagecreatefromjpeg(public_path($dir));
            }

            $result = imagewebp($img, public_path($path).'.webp', config('jspress.image.quality'));

            imagedestroy($img);
        }
    }

    /**
     * @param object $file
     * @return array
     */
    private function convertFileData(object $file): array
    {
        $optimized_url = null;
        if( config('jspress.image.webp') ){
            $optimized_image_path = $file->path.'/'.$file->file_name;
            if(\File::exists(public_path($optimized_image_path.'.webp'))){
                $optimized_url = $optimized_image_path.'.webp';
            }else{
                $this->convertWebp($file->original_url, $optimized_image_path);
                if(\File::exists(public_path($optimized_image_path.'.webp'))){
                    $optimized_url = $optimized_image_path.'.webp';
                }
            }
        }
        $admin = Admin::find($file->user_id);
        return [
            'original_url' => asset($file->original_url),
            'optimized_url' => !is_null($optimized_url) ? asset($optimized_url) : null,
            'file_name' => $file->file_name,
            'upload_name' => $file->upload_name,
            'path' => $file->path,
            'mime' => $file->mime,
            'extension' => $file->extension,
            'type' => $file->type,
            'id' => $file->id,
            'width' => !is_null($file->width) ? $file->width : null,
            'height' => !is_null($file->height) ? $file->height : null,
            'size' => readable_file_size($file->size),
            'details' => $this->getMediaDetailLanguages($file),
            'has_dimension' => $this->hasSize($file->extension),
            'dimensions' => $this->hasSize($file->extension) ? $this->getSizes($file): [],
            'uploaded_by' => !is_null($admin) ? $admin->name : 'Deleted'
        ];
    }

    /**
     * @param object $file
     * @return array
     */
    public function getSizes(object $file): array
    {
        $path = $file->path;
        $file_name = $file->file_name;
        $defined_sizes = collect(config('jspress.files.images.sizes'));
        $size_data = [];
        foreach($defined_sizes as $size){
            $optimized_url = null;
            $original_url = $this->convertCropImageSlug($path, $file_name, $size, $file->extension, false);
            $url = $this->convertCropImageSlug($path, $file_name, $size, $file->extension, true);
            if(!\File::exists(public_path($original_url))){
                $url = url($file->original_url);
            }else{
                if( config('jspress.image.webp') ){
                    $optimized_image_path = $this->convertCropImageSlugForWebP($path, $file_name, $size);
                    if(\File::exists(public_path($optimized_image_path.'.webp'))){
                        $optimized_url = asset($optimized_image_path.'.webp');
                    }else{
                        $this->convertWebp($original_url, $optimized_image_path);
                        if(\File::exists(public_path($optimized_image_path.'.webp'))){
                            $optimized_url = asset($optimized_image_path.'.webp');
                        }
                    }
                }
            }
            $size_data[$size['name']] = [
                'width' => $size['width'],
                'height' => $size['height'],
                'url' => $url,
                'original_url' => $original_url,
                'optimized_url' => $optimized_url
            ];
        }
        return $size_data;
    }

    /**
     * @param string $extension
     * @return bool
     */
    public function hasSize(string $extension): bool
    {
        $str_extension = strtolower($extension);
        return $str_extension === 'jpeg' || $str_extension === 'png' || $str_extension === 'jpg';
    }

    /**
     * @param string $path
     * @param string $file_name
     * @param array $size
     * @param string $extension
     * @param bool $is_full
     * @return string
     */
    private function convertCropImageSlug(string $path, string $file_name, array $size, string $extension, bool $is_full = true): string
    {
        if($is_full){
            $url = asset($path.'/'.$file_name.'-'.$size['width'].'-'.$size['height'].'.'.$extension);
        }else{
            $url = $path.'/'.$file_name.'-'.$size['width'].'-'.$size['height'].'.'.$extension;
        }
        return $url;
    }

    /**
     * @param string $path
     * @param string $file_name
     * @param array $size
     * @return string
     */
    private function convertCropImageSlugForWebP(string $path, string $file_name, array $size): string
    {
        return $path.'/'.$file_name.'-'.$size['width'].'-'.$size['height'];
    }

    /**
     * @param $media
     * @return array
     */
    private function getMediaDetailLanguages($media): array
    {
        $details = [];
        foreach($this->website_languages as $language){
            $locale = $language->locale;
            $language_item = $this->display_language[$locale];
            $detail = \DB::table('media_details')->where('media_id', $media->id)->where('lang', $locale)->first();
            if(is_null($detail)){
                $details[$locale]['title'] = "";
                $details[$locale]['tag'] = "";
                $details[$locale]['caption'] = "";
            }else{
                $details[$locale]['title'] = $detail->title;
                $details[$locale]['tag'] = $detail->tag;
                $details[$locale]['caption'] = $detail->caption;
            }
            $details[$locale]['language'] = $language_item->display_language_name;
        }
        return $details;
    }

    /**
     * @param $file
     * @return self
     */
    public function setFile($file): self
    {
        $this->file = $file;
        return $this;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param string $upload_name
     * @return self
     */
    public function setUploadName(string $upload_name): self
    {
        $this->upload_name = $upload_name;
        return $this;
    }

    /**
     * @param string $file_name
     * @return self
     */
    public function setFileName(string $file_name): self
    {
        $this->file_name = $file_name;
        return $this;
    }

    /**
     * @param string $extension
     * @return self
     */
    public function setExtension(string $extension): self
    {
        $this->extension = $extension;
        return $this;
    }

    /**
     * @param string $mime
     * @return self
     */
    public function setMime(string $mime): self
    {
        $this->mime = $mime;
        return $this;
    }

    /**
     * @param string $type
     * @return self
     */
    public function setType(string $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param int|null $width
     * @return self
     */
    public function setWidth(int|null $width): self
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @param int|null $height
     * @return self
     */
    public function setHeight(int|null $height): self
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @param int $size
     * @return self
     */
    public function setSize(int $size): self
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @param string|null $path
     * @return self
     */
    public function setPath(string|null $path = null): self
    {
        $now = \Carbon\Carbon::now();
        $year = $now->format('Y');
        $month = $now->format('m');
        $day = $now->format('d');
        if(is_null($path)){
            $this->path = 'uploads/'.$year.'/'.$month.'/'.$day;
        }else{
            $this->path = $path;
        }
        return $this;
    }

    /**
     * @param string|null $directory
     * @return self
     */
    public function setDirectory(string|null $directory = null): self
    {
        if(is_null($directory)){
            $this->directory = $this->path.'/'.$this->upload_name;
        }else{
            $this->directory = $directory;
        }
        return $this;
    }

    /**
     * @param int $user_id
     * @return $this
     */
    public function setUserId(int $user_id): self
    {
        $this->user_id = $user_id;
        return $this;
    }



    /**
     * @return string
     */
    public function getMimeType(): string
    {
        return $this->mime;
    }

    /**
     * @return array|string[]
     */
    public function allowed_mime_types(): array
    {
        return $this->mimeTypes;
    }

    /**
     * @return $this
     */
    public function unlink(): self
    {
        $extension = strtolower($this->extension);
        $files = [public_path($this->directory)];
        $file_dir = $this->path.'/'.$this->file_name;
        if(\File::exists(public_path($file_dir.'.webp'))){
            $files[] = public_path($file_dir.'.webp');
        }
        if( $extension === 'jpg' || $extension === 'jpeg' || $extension === 'png'  ){
            foreach(config('jspress.files.images.sizes') as $size ){
                $file_directory = $this->path.'/'.$this->file_name.'-'.$size["width"].'-'.$size["height"].'.'.$this->extension;
                $files[] = public_path($file_directory);
                if(\File::exists(public_path($file_dir.'-'.$size["width"].'-'.$size["height"].'.webp'))){
                    $files[] = public_path($file_dir.'-'.$size["width"].'-'.$size["height"].'.webp');
                }
            }
        }

        File::delete($files);
        return $this;
    }

    /**
     * @return bool
     */
    public function delete(): bool
    {
        Media::find($this->id)->delete();
        return true;
    }

    /**
     * @param int $id
     * @return self
     */
    public function get(int $id): self
    {
        $media = Media::find($id);
        return self::setId($media->id)
            ->setUploadName($media->upload_name)
            ->setFileName($media->file_name)
            ->setExtension($media->extension)
            ->setMime($media->mime)
            ->setType($media->type)
            ->setWidth($media->width)
            ->setHeight($media->height)
            ->setSize($media->size)
            ->setPath()
            ->setDirectory();
    }
}
