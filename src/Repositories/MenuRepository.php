<?php

namespace JsPress\JsPress\Repositories;

use Illuminate\Support\Facades\Cache;
use JsPress\JsPress\Interfaces\MenuInterface;
use JsPress\JsPress\Interfaces\PostTranslationInterface;
use JsPress\JsPress\Models\Category;
use JsPress\JsPress\Models\Menu;
use JsPress\JsPress\Models\Post;
use JsPress\JsPress\Models\PostTranslation;
use JsPressPanel;

class MenuRepository implements MenuInterface{

    private ?int $id;
    private ?string $title;
    private ?string $slug;
    private int $region = 1;
    private string $locale;
    private ?array $data;
    private PostTranslationInterface $post_translations;
    private ?int $transaction_id = null;
    private ?string $source_locale = null;

    public function setId(?int $id): MenuInterface
    {
        $this->id = $id;
        return $this;
    }

    public function setTitle(string $title):? MenuInterface
    {
        $this->title = $title;
        return $this;
    }

    public function setSlug(string $slug):? MenuInterface
    {
        $this->slug = $slug;
        return $this;
    }

    public function setRegion(int $region): MenuInterface
    {
        $this->region = $region;
        return $this;
    }

    public function setLocale(string $locale): MenuInterface
    {
        $this->locale = $locale;
        return $this;
    }

    public function setData(?array $data): MenuInterface
    {
        $this->data = $data;
        return $this;
    }

    public function setTransactionId(?int $transaction_id): MenuInterface
    {
        $this->transaction_id = $transaction_id;
        return $this;
    }

    public function setSourceLocale(?string $source_locale): MenuInterface
    {
        $this->source_locale = $source_locale;
        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }



    public function __construct(PostTranslationInterface $post_translations){
        $this->post_translations = $post_translations;
    }

    public function getMenuOptions(): array
    {
        $post_data = [];
        $category_data = [];
        $posts = Post::select([
                'posts.id',
                'posts.title',
                'posts.post_id',
                'posts.created_at',
                'post_types.key as post_type',
                'post_types.name as post_type_name'
            ])
            ->join('post_types', function($join){
                $join->on('post_types.key', '=', 'posts.post_type')->where('post_types.is_url', 1);
            })
            ->join('post_translations', function($join){
                $join->on('post_translations.element_id', '=', 'posts.id')
                    ->where('post_translations.type', 'post')
                    ->where('locale', JsPressPanel::contentLocale())
                    ->where('region', 1);
            })
            ->orderBy('post_types.created_at', 'ASC')
            ->get()
            ->groupBy('post_type')
            ->toArray();
        foreach($posts as $key => $post_items){
            $post_data[$key]['name'] = __js('post_type', JsPressPanel::postTypeLangKeyFromKey($post_items[0]['post_type'], 'list'), $post_items[0]['post_type_name']);
            foreach($post_items as $post){
                $post_data[$key]['menu_items'][] = $post;
            }
        }
        $result_data = [];
        foreach($post_data as $key => $post_items){
            $result_data['post'][$key]['name'] = $post_items['name'];
            $result_data['post'][$key]['menu_items']['recently_added'] = collect($post_items['menu_items'])->sortByDesc('created_at')->toArray();
            $result_data['post'][$key]['menu_items']['menu_items'] = collect($post_items['menu_items'])->sortBy('title')->toArray();
        }

        $categories = Category::select([
            'categories.id',
            'categories.title',
            'categories.category_id',
            'categories.created_at',
            'post_types.key as post_type',
            'post_types.name as post_type_name'
        ])
            ->join('post_types', function($join){
                $join->on('post_types.key', '=', 'categories.category_type')->where('post_types.is_category_url', 1);
            })
            ->join('post_translations', function($join){
                $join->on('post_translations.element_id', '=', 'categories.id')
                    ->where('post_translations.type', 'category')
                    ->where('locale', JsPressPanel::contentLocale())
                    ->where('region', 1);
            })
            ->orderBy('post_types.created_at', 'ASC')
            ->get()
            ->groupBy('post_type')
            ->toArray();
        foreach($categories as $key => $category_items){
            $category_data[$key]['name'] = __js('admin', 'admin_menu_category_'.$category_items[0]['post_type'], $category_items[0]['post_type_name'].' Kategori');
            foreach($category_items as $category){
                $category_data[$key]['menu_items'][] = $category;
            }
        }

        foreach($category_data as $key => $category_items){
            $result_data['category'][$key]['name'] = $category_items['name'];
            $result_data['category'][$key]['menu_items']['recently_added'] = collect($category_items['menu_items'])->sortByDesc('created_at')->toArray();
            $result_data['category'][$key]['menu_items']['menu_items'] = collect($category_items['menu_items'])->sortBy('title')->toArray();
        }

        return $result_data;
    }

    public function save(): MenuInterface
    {
        $slug = \Str::slug($this->slug, '-');
        $menu = Menu::updateOrCreate(
            [
                'id' => $this->id
            ],
            [
                'title' => $this->title,
                'slug' => $slug,
                'region' => $this->region,
                'data' => $this->data
            ]
        );

        if( $this->locale == default_language() && is_null($this->id) && !is_null($this->source_locale) && !is_null($this->transaction_id) ){
            $menus = $this->getPostsTranslation($this->transaction_id);
            foreach($menus as $menu_item){
                $this->post_translations->setRegion(1)
                    ->setLocale($menu_item->locale)
                    ->setElementId($menu_item->element_id)
                    ->setType('menu')
                    ->setTransactionId($menu_item->transaction_id)
                    ->setSourceLocale(default_language())
                    ->save();
            }
            $this->source_locale = null;
        }

        $this->id = $menu->id;

        /* Save Translation */
        $this->post_translations->setRegion($this->region)
            ->setLocale($this->locale)
            ->setElementId($this->id)
            ->setType('menu')
            ->setTransactionId($this->transaction_id)
            ->setSourceLocale($this->source_locale)
            ->save();
        Cache::forget($key = 'menu_'.$slug.'_'.$this->locale);
        return $this;
    }

    public function get():? array
    {
        $menu = Menu::where('id', $this->id)
            ->whereHas('translation', function($query){
                return $query->where('locale', JsPressPanel::contentLocale());
            })
            ->with('translation')
            ->first();
        if(is_null($menu)){
            return null;
        }
        return json_decode(json_encode($menu), TRUE);
    }

    public function getMenuData():? array
    {
        $menus = Menu::whereHas('translation', function($query){
                return $query->where('locale', JsPressPanel::contentLocale());
            })
            ->with('translation')
            ->get();
        $menu_data = [];
        foreach($menus as $key => $menu){
            $this->id = $menu->id;
            $this->transaction_id = $menu->translation->transaction_id;
            $menu_data[$key] = json_decode(json_encode($menu), TRUE);
            $menu_data[$key]['translations'] = $this->getLanguageOptions();
        }
        return $menu_data;
    }

    /**
     * @return array|null
     */
    public function getPostByTransactionId():? array
    {

        $menu = \DB::table('menu')
            ->select([
                'menu.title'
            ])
            ->join('post_translations', function($join){
                $join->on('post_translations.element_id', '=', 'menu.id')
                    ->where('post_translations.type', 'menu')
                    ->where('post_translations.transaction_id', $this->transaction_id)
                    ->where('post_translations.region', 1)
                    ->where('post_translations.locale', $this->locale);
            })
            ->first();

        return json_decode(json_encode($menu), TRUE);
    }

    /**
     * @return void
     */
    public function delete(): void
    {
        if(!is_null($this->id)){
            $menu = Menu::where('id',$this->id)->with('translation')->first();
            $this->transaction_id = $menu->translation->transaction_id;
            $this->deleteTranslationRelation();
            $this->post_translations->setElementId($this->id)->setType('menu')->deleteItem();
            $menu->delete();
        }
    }

    /**
     * @return void
     */
    public function deleteTranslationRelation(): void
    {
        $this->post_translations->setRegion(1)
            ->setLocale($this->locale)
            ->setElementId($this->id)
            ->setType('menu')
            ->setTransactionId($this->transaction_id)
            ->delete();
    }

    /**
     * @param int $transaction_id
     * @return object
     */
    private function getPostsTranslation(int $transaction_id): object
    {
        return \DB::table('post_translations')
            ->where('transaction_id', $transaction_id)
            ->where('type', 'menu')
            ->whereIn('locale', website_languages()->pluck('locale')->toArray())
            ->get();
    }

    /**
     * @param $post
     * @return array
     */
    private function getLanguageOptions(): array
    {

        $language_data = [];
        $other_languages = collect(website_languages())->where('locale', '!=', $this->locale)->values()->all();
        foreach($other_languages as $language){
            $translation = \DB::table('post_translations')
                ->where('transaction_id', $this->transaction_id)
                ->where('type', 'menu')
                ->where('region', 1)
                ->where('locale', $language->locale)
                ->first();

            if($translation){
                $url_data['id'] = $translation->element_id;
                $url_data['lang'] = $language->locale;
                $url_data['trid'] = $this->transaction_id;
                $language_data[$language->locale] = [
                    'type' => 'edit',
                    'trid' => $translation->transaction_id,
                    'id' => $translation->element_id,
                    'lang' => $translation->locale,
                    'url' => route('admin.menu.edit', $url_data)
                ];
            }else{
                $translation = \DB::table('post_translations')
                    ->where('element_id', $this->id)
                    ->where('type', 'menu')
                    ->where('locale', $this->locale)
                    ->first();
                $url_data['trid'] = $translation->transaction_id;
                $url_data['lang'] = $language->locale;
                if(is_null($translation->source_locale)){
                    $url_data['source_locale'] = $this->locale;
                }else{
                    $url_data['source_locale'] = $translation->source_locale;
                }
                $language_data[$language->locale] = [
                    'type' => 'add',
                    'trid' => $translation->transaction_id,
                    'url' => route('admin.menu.create', $url_data)
                ];
            }
        }
        return $language_data;
    }
}
