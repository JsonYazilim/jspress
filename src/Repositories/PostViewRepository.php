<?php

namespace JsPress\JsPress\Repositories;

use JsPress\JsPress\Interfaces\PostViewInterface;
use JsPress\JsPress\Models\PostView;

class PostViewRepository implements PostViewInterface{

    /**
     * @var string
     */
    private string $type;
    /**
     * @var int
     */
    private int $element_id;
    /**
     * @var string
     */
    private string $locale;
    /**
     * @var int
     */
    private int $region = 1;
    /**
     * @var string|null
     */
    private ?string $ip = null;
    /**
     * @var string|null
     */
    private ?string $user_agent = null;
    /**
     * @var string|null
     */
    private ?string $url = null;
    /**
     * @var string|null
     */
    private ?string $device = null;
    /**
     * @var string|null
     */
    private ?string $device_name = null;
    /**
     * @var string|null
     */
    private ?string $browser = null;
    /**
     * @var string|null
     */
    private ?string $platform = null;

    /**
     * @param int $id
     * @return PostViewInterface
     */
    public function setId(int $id): PostViewInterface
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @param int $element_id
     * @return PostViewInterface
     */
    public function setElementId(int $element_id): PostViewInterface
    {
        $this->element_id = $element_id;
        return $this;
    }


    /**
     * @param string $type
     * @return PostViewInterface
     */
    public function setType(string $type): PostViewInterface
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param string $locale
     * @return PostViewInterface
     */
    public function setLocale(string $locale): PostViewInterface
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @param int $region
     * @return PostViewInterface
     */
    public function setRegion(int $region): PostViewInterface
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @param string|null $ip
     * @return PostViewInterface
     */
    public function setIp(?string $ip): PostViewInterface
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @param string|null $user_agent
     * @return PostViewInterface
     */
    public function setUserAgent(?string $user_agent): PostViewInterface
    {
        $this->user_agent = $user_agent;
        return $this;
    }

    /**
     * @param string|null $url
     * @return PostViewInterface
     */
    public function setUrl(?string $url): PostViewInterface
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @param string|null $device
     * @return PostViewInterface
     */
    public function setDevice(?string $device): PostViewInterface
    {
        $this->device = $device;
        return $this;
    }

    /**
     * @param string|null $device_name
     * @return PostViewInterface
     */
    public function setDeviceName(?string $device_name): PostViewInterface
    {
        $this->device_name = $device_name;
        return $this;
    }

    /**
     * @param string|null $browser
     * @return PostViewInterface
     */
    public function setBrowser(?string $browser): PostViewInterface
    {
        $this->browser = $browser;
        return $this;
    }

    /**
     * @param string|null $platform
     * @return PostViewInterface
     */
    public function setPlatform(?string $platform): PostViewInterface
    {
        $this->platform = $platform;
        return $this;
    }

    /**
     * @return void
     */
    public function save(): void
    {

        PostView::updateOrCreate(
            [
                'type' => $this->type,
                'element_id' => $this->element_id,
                'ip' => $this->ip,
                'device' => $this->device,
                'updated_at' => PostView::where('updated_at', '>=', \Carbon\Carbon::now()->subMinute(2))->first()->updated_at ?? date('Y-m-d H:i:s')
            ],
            [
                'locale' => $this->locale,
                'region' => $this->region,
                'url' => $this->url,
                'user_agent' => $this->user_agent,
                'device_name' => $this->device_name,
                'browser' => $this->browser,
                'platform' => $this->platform
            ]
        );
    }


}
