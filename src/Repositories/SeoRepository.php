<?php

namespace JsPress\JsPress\Repositories;

use JsPress\JsPress\Interfaces\SeoInterface;
use JsPress\JsPress\Models\SeoMeta;

class SeoRepository implements SeoInterface{

    /**
     * @var string
     */
    private string $element_type;
    /**
     * @var int
     */
    private int $element_id;
    /**
     * @var string
     */
    private string $type;
    /**
     * @var string|null
     */
    private ?string $prefix = null;
    /**
     * @var string
     */
    private string $key;
    /**
     * @var string|null
     */
    private ?string $content;

    /**
     * @return void
     */
    public function save(): void
    {
        SeoMeta::updateOrCreate(
            [
                'element_id' => $this->element_id,
                'element_type' => $this->element_type,
                'type' => $this->type,
                'key' => $this->key,
                'prefix' => $this->prefix
            ],
            [
                'content' => $this->content
            ]
        );
    }

    /**
     * @return void
     */
    public function delete():void
    {
        SeoMeta::where('element_id', $this->element_id)->where('element_type', $this->type)->delete();
    }

    /**
     * @param string $element_type
     * @return SeoInterface
     */
    public function setElementType(string $element_type): SeoInterface
    {
        $this->element_type = $element_type;
        return $this;
    }

    /**
     * @param int $element_id
     * @return SeoInterface
     */
    public function setElementId(int $element_id): SeoInterface
    {
        $this->element_id = $element_id;
        return $this;
    }

    /**
     * @param string $type
     * @return SeoInterface
     */
    public function setType(string $type): SeoInterface
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param string|null $prefix
     * @return SeoInterface
     */
    public function setPrefix(?string $prefix): SeoInterface
    {
        $this->prefix = $prefix;
        return $this;
    }

    /**
     * @param string $key
     * @return SeoInterface
     */
    public function setKey(string $key): SeoInterface
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @param string|null $content
     * @return SeoInterface
     */
    public function setContent(string|null $content): SeoInterface
    {
        $this->content = $content;
        return $this;
    }


}
