<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PostTypeRepository.php
 * Author: Json Yazılım
 * Class: PostTypeRepository.php
 * Current Username: Erdinc
 * Last Modified: 24.07.2023 15:33
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Repositories;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use JsPress\JsPress\Interfaces\PostTypeInterface;
use JsPress\JsPress\Models\PostType;
use JsPress\JsPress\Models\PostTypeDetail;
use JsPress\JsPress\Models\PostTypePermission;
use JsPressPanel;

class PostTypeRepository implements PostTypeInterface{

    /**
     * @var int
     */
    private int $post_type_id;

    /**
     * @param string $key
     * @param int|string|null $model_id
     * @return bool
     */
    public function validateKey(string $key, null|int|string $model_id): bool
    {
        if(!is_null($model_id)){
            return \DB::table('post_types')->where('key', $key)->where('id', '!=', $model_id)->exists();
        }
        return \DB::table('post_types')->where('key', $key)->exists();
    }

    /**
     * @param PostType $postType
     * @param array $data
     * @return PostTypeInterface
     */
    public function save(PostType $postType, array $data): PostTypeInterface
    {

        $postType->post_type_id = $data['parent_id'];
        $postType->name = $data['name'];
        $postType->key = $data['key'];
        $postType->icon = $data['icon'];
        $postType->is_category = $data['is_category'];
        $postType->is_category_url = $data['is_category_url'];
        $postType->is_editor = $data['is_editor'];
        $postType->is_terms = $data['is_terms'];
        $postType->is_image = $data['is_image'];
        $postType->is_seo = $data['is_seo'];
        $postType->is_segment_disable = $data['is_segment_disable'];
        $postType->is_url = $data['is_url'];
        $postType->is_menu = $data['is_menu'];
        $postType->order = $data['order'];
        $postType->save();
        $this->post_type_id = $postType->id;
        if($postType){
            $this->saveDetails($data['details'], $data['is_category']);
        }
        JsPressPanel::clearRoute();
        return $this;
    }

    /**
     * @param Request|null $request
     * @param bool $is_menu
     * @param bool $is_paginate
     * @return LengthAwarePaginator|Collection
     */
    public function list(Request|null $request, bool $is_menu = false, bool $is_paginate = true): LengthAwarePaginator|Collection
    {
        $post_types = \DB::table('post_types')
            ->select([
                'post_types.*'
            ])
            ->where('post_types.key', '!=', 'page')
            ->when($request->q, function($query)use($request){
                return $query->where('name', 'like', $request->q.'%');
            })
            ->orderBy('post_types.order');
            if( $is_paginate ){
                return $post_types->paginate(20);
            }
            return $post_types->get();
    }

    /**
     * @param array $permissions
     * @return void
     */
    public function setPermissions(array $permissions): void
    {
        $postType = PostType::find($this->post_type_id);
        $postType->permissions()->delete();
        $permission_data = [];
        if(count($permissions) > 0 ){
            foreach($permissions as $permission){
                if((!is_null($permission['role']) && count($permission['permissions']) > 0 )){
                    foreach($permission['permissions'] as $p){
                        $permissionModel = new PostTypePermission();
                        $permissionModel->role_id = $permission['role'];
                        $permissionModel->permission = $p;
                        $permission_data[] = $permissionModel;
                    }
                }
            }
        }

        $postType->permissions()->saveMany($permission_data);
    }

    /**
     * @return void
     */
    public function createFiles(): void
    {
        // TODO: Implement createFiles() method.
    }

    /**
     * @return array[]
     */
    public function permissionElements(): array
    {
        return [
            'select' => [
                'width' => '50',
                'label' => __('JsPress::backend.choose_role'),
                'placeholder' => __('JsPress::backend.select_a_role'),
                'name' => 'role',
                'required' => true,
                'is_repeater' => true,
                'options' => JsPressPanel::getRoles()
            ],
            'checkbox' => [
                'width' => '50',
                'type' => 'inline',
                'label' => __('JsPress::backend.choose_permissions'),
                'name' => 'permissions',
                'options' => [
                    [
                        'id' => 'permission_view',
                        'label' => __('JsPress::backend.permission_view'),
                        'value' => 'view',
                        'checked' => false
                    ],
                    [
                        'id' => 'permission_add',
                        'label' => __('JsPress::backend.permission_add'),
                        'value' => 'add',
                        'checked' => false
                    ],
                    [
                        'id' => 'permission_edit',
                        'label' => __('JsPress::backend.permission_edit'),
                        'value' => 'edit',
                        'checked' => false
                    ],
                    [
                        'id' => 'permission_category',
                        'label' => __('JsPress::backend.permission_category'),
                        'value' => 'category',
                        'checked' => false
                    ],
                    [
                        'id' => 'permission_delete',
                        'label' => __('JsPress::backend.permission_delete'),
                        'value' => 'delete',
                        'checked' => false
                    ],
                    [
                        'id' => 'permission_trash',
                        'label' => __('JsPress::backend.permission_trash'),
                        'value' => 'trash',
                        'checked' => false
                    ],
                    [
                        'id' => 'permission_all',
                        'label' => __('JsPress::backend.permission_view_all'),
                        'value' => 'all',
                        'checked' => false
                    ]
                ]
            ]
        ];
    }

    /**
     * @param PostType $postType
     * @return array
     */
    public function getPermissionsValues(PostType $postType): array
    {
        $permissions = $postType->permissions()->get()->groupBy('role_id')->values();
        $data = [];
        foreach($permissions as $key => $permission){
            $permissions_data = $permission->pluck('permission')->toArray();
            $data[$key] = [
                'permissions' => $permissions_data,
                'role' => $permission[0]['role_id']
            ];
        }
        return $data;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function is_exist(string $key): bool
    {
        return PostType::where('key', $key)->exists();
    }

    /**
     * @param $details
     * @param $is_category
     * @return void
     */
    private function saveDetails($details, $is_category): void
    {
        foreach($details as $locale => $detail){
            $category_data = $detail['category'] ?? null;
            if( isset($detail['disabled']) ){
                $post_type_detail = PostTypeDetail::where('post_type_id', $this->post_type_id)->where('locale', $locale)->first();
                if($post_type_detail) $post_type_detail->delete();
            }else{
                $this->savePostTypeDetail($detail['post'], $locale);
                if(!is_null($category_data)){
                    $status = $is_category == 1 ? 1 : 0;
                    $this->saveCategories($category_data, $locale, $status);
                }else{
                    $post_type_category = PostTypeDetail::where('post_type_id', $this->post_type_id)->where('locale', $locale)->where('model', 'JsPress\JsPress\Models\Category')->first();
                    if($post_type_category) $post_type_category->delete();
                }
            }
        }
    }

    /**
     * @param array $detail
     * @param string $locale
     * @param int $status
     * @return void
     */
    private function savePostTypeDetail(array $detail, string $locale, int $status = 1): void
    {
        $slug = $detail['slug'] ?? $detail['single_name'];
        PostTypeDetail::updateOrCreate(
            [
                'post_type_id' => $this->post_type_id,
                'locale' => $locale,
                'model' => 'JsPress\JsPress\Models\Post'
            ],
            [
                'region' => 1,
                'singular_name' => $detail['single_name'],
                'plural_name' => $detail['plural_name'],
                'slug' => \Str::slug($slug, '-'),
                'status' => $status
            ]
        );
    }

    /**
     * @param array $category
     * @param string $locale
     * @param int $status
     * @return void
     */
    private function saveCategories(array $category, string $locale, int $status = 1): void
    {
        $slug = $category['slug'] ?? $category['single_name'];
        PostTypeDetail::updateOrCreate(
            [
                'post_type_id' => $this->post_type_id,
                'locale' => $locale,
                'model' => 'JsPress\JsPress\Models\Category'
            ],
            [
                'region' => 1,
                'singular_name' => $category['single_name'],
                'plural_name' => $category['plural_name'],
                'slug' => \Str::slug($slug, '-'),
                'status' => $status
            ]
        );
    }
}
