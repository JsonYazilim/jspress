<?php

namespace JsPress\JsPress\Repositories;

use JsPress\JsPress\Interfaces\PostExtrasInterface;
use JsPress\JsPress\Models\PostExtra;

class PostExtrasRepository implements PostExtrasInterface{

    /**
     * @var string
     */
    private string $type;
    /**
     * @var int
     */
    private int $element_id;
    /**
     * @var string
     */
    private string $key;
    /**
     * @var string|null
     */
    private ?string $value;
    /**
     * @var array|null
     */
    private ?array $extras;

    /**
     * @param string $type
     * @return PostExtrasInterface
     */
    public function setType(string $type): PostExtrasInterface
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @param int $element_id
     * @return PostExtrasInterface
     */
    public function setElementId(int $element_id): PostExtrasInterface
    {
        $this->element_id = $element_id;
        return $this;
    }

    /**
     * @param string $key
     * @return PostExtrasInterface
     */
    public function setKey(string $key): PostExtrasInterface
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @param string|null $value
     * @return PostExtrasInterface
     */
    public function setValue(?string $value): PostExtrasInterface
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @param array|null $extras
     * @return PostExtrasInterface
     */
    public function setExtras(?array $extras): PostExtrasInterface
    {
        $this->extras = $extras;
        return $this;
    }


    /**
     * @return void
     */
    public function save(): void
    {
        PostExtra::updateOrCreate(
            [
                'element_id' => $this->element_id,
                'type' => $this->type,
                'key' => $this->key
            ],
            [
                'value' => $this->value
            ]
        );
    }

    /**
     * @return void
     */
    public function deleteItems(): void
    {
        PostExtra::where('element_id', $this->element_id)->where('type', $this->type)->delete();
    }

    /**
     * @return void
     */
    public function saveMany(): void
    {
        $non_exists_extras = $this->collectNotExistKeys();
        if(count($non_exists_extras) > 0){
            foreach($non_exists_extras as $extra){
                $this->key = $extra;
                $this->delete();
            }
        }
        if(count($this->extras) > 0){
            foreach($this->extras as $key => $extra){
                if(is_array($extra)){
                    $depth = $this->array_depth($extra);
                    if($depth === 0){
                        $this->deleteNotExists($extra, $key);
                        foreach($extra as $e){
                            $this->key = $key;
                            if(!$this->checkIfExists($e)){
                                $this->value = $e;
                                PostExtra::create([
                                    'element_id' => $this->element_id,
                                    'type' => $this->type,
                                    'key' => $this->key,
                                    'value' => $this->value,
                                    'is_multiple' => 1
                                ]);
                            }
                        }
                    }else{
                        $this->key = $key;
                        $this->value = json_encode($extra);
                        $this->save();
                    }
                }else{
                    $this->key = $key;
                    $this->value = $extra;
                    $this->save();
                }
            }
        }
    }

    /**
     * @return void
     */
    public function delete(): void
    {
        PostExtra::where('type', $this->type)->where('key', $this->key)->where('element_id', $this->element_id)->delete();
    }

    /**
     * @return array
     */
    private function collectNotExistKeys(): array
    {
        $key_data = [];
        foreach($this->extras as $key => $extra){
            $key_data[] = $key;
        }

        return PostExtra::where('type', $this->type)->where('element_id', $this->element_id)->whereNotIn('key', $key_data)->get()->pluck('key')->toArray();
    }

    /**
     * @param array $extras
     * @param $key
     * @return void
     */
    private function deleteNotExists(array $extras, $key):void
    {
        $not_exits_values = PostExtra::where('type', $this->type)
            ->where('element_id', $this->element_id)
            ->where('key', $key)
            ->whereNotIn('value', $extras)
            ->get();
        foreach($not_exits_values as $not_exits_value){
            $not_exits_value->delete();
        }
    }

    /**
     * @param $value
     * @return bool
     */
    private function checkIfExists($value): bool
    {
        return PostExtra::where('type', $this->type)->where('element_id', $this->element_id)->where('key', $this->key)->where('value', $value)->exists();
    }

    /**
     * Determines the depth of a multidimensional array.
     *
     * @param array $array The array to determine the depth of.
     * @param int $level The current depth level during recursion.
     * @return int The maximum depth of the array.
     */
    private function array_depth(array $array, $level = 0): int {
        $depth = $level;

        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $depth = max($depth, $this->array_depth($value, $level + 1)); // Use $this->
            }
        }

        return $depth;
    }
}
