<?php
/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: SitemapRepository.php
 * Author: Json Yazılım
 * Class: SitemapRepository.php
 * Current Username: Erdinc
 * Last Modified: 18.04.2024 01:27
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Repositories;

use JsPress\JsPress\Interfaces\SitemapInterface;
use JsPress\JsPress\Models\Post;
use Illuminate\Support\Facades\Cache;
class SitemapRepository implements SitemapInterface{

    /**
     * @var string|null
     */
    private ?string $locale = null;
    /**
     * @var string|null
     */
    private ?string $post_type = null;

    /**
     * @param string|null $locale
     * @return SitemapInterface
     */
    public function setLocale(?string $locale): SitemapInterface
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @param string|null $post_type
     * @return SitemapInterface
     */
    public function setPostType(?string $post_type): SitemapInterface
    {
        $this->post_type = $post_type;
        return $this;
    }

    /**
     * @return array
     */
    public function getPosts(): array
    {
        return Cache::remember('sitemaps_'.$this->post_type.'_'.$this->locale, 24*60*60, function () {
            return $this->getPostItems($this->post_type, $this->locale);
        });
    }

    /**
     * @param $post_type
     * @param $locale
     * @return array
     */
    private function getPostItems($post_type, $locale): array
    {
        $post_data = [];
        $posts = Post::where('status', 1)
            ->where('post_type', $post_type)
            ->whereHas('translation', function($query)use($locale){
                return $query->where('locale', $locale);
            })
            ->orderBy('is_homepage', 'desc')
            ->orderBy('publish_date', 'asc')
            ->with(['extras', 'post_type_item'])
            ->get();
        foreach($posts as $post){
            $publish_date = date("c", strtotime($post->updated_at));
            $post_data[] = [
                'url' => get_the_url($post, $post->post_type_item),
                'last_updated' => $publish_date
            ];
        }

        return $post_data;
    }

}
