<?php

namespace JsPress\JsPress\Repositories;

use JsPress\JsPress\Interfaces\PostTranslationInterface;
use JsPress\JsPress\Models\PostTranslation;

class PostTranslationRepository implements PostTranslationInterface{

    /**
     * @var int
     */
    private int $element_id;
    /**
     * @var int|null
     */
    private ?int $transaction_id;
    /**
     * @var int
     */
    private int $region = 1;
    /**
     * @var string
     */
    private string $locale;
    /**
     * @var string|null
     */
    private ?string $source_locale;
    /**
     * @var string
     */
    private string $type;

    /**
     * @param int $element_id
     * @return PostTranslationInterface
     */
    public function setElementId(int $element_id): PostTranslationInterface
    {
        $this->element_id = $element_id;
        return $this;
    }

    /**
     * @param int|null $transaction_id
     * @return PostTranslationInterface
     */
    public function setTransactionId(int|null $transaction_id): PostTranslationInterface
    {
        $this->transaction_id = $transaction_id;
        return $this;
    }

    /**
     * @param int $region
     * @return PostTranslationInterface
     */
    public function setRegion(int $region): PostTranslationInterface
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @param string $locale
     * @return PostTranslationInterface
     */
    public function setLocale(string $locale): PostTranslationInterface
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @param string|null $source_locale
     * @return PostTranslationInterface
     */
    public function setSourceLocale(?string $source_locale): PostTranslationInterface
    {
        $this->source_locale = $source_locale;
        return $this;
    }

    /**
     * @param string $type
     * @return PostTranslationInterface
     */
    public function setType(string $type): PostTranslationInterface
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return PostTranslationInterface
     */
    public function generate(): PostTranslationInterface
    {
        if(is_null($this->transaction_id)){
            $this->transaction_id = $this->createTransactionId();
        }
        PostTranslation::updateOrCreate(
            [
                'type' => $this->type,
                'element_id' => $this->element_id
            ],
            [
                'transaction_id' => $this->transaction_id,
                'region' => $this->region,
                'locale' => $this->locale,
                'source_locale' => $this->source_locale
            ]
        );
        return $this;
    }

    /**
     * @return void
     */
    public function save(): void
    {
        if(is_null($this->transaction_id)){
            $this->transaction_id = $this->createTransactionId();
        }
        PostTranslation::updateOrCreate(
            [
                'type' => $this->type,
                'element_id' => $this->element_id
            ],
            [
                'transaction_id' => $this->transaction_id,
                'region' => $this->region,
                'locale' => $this->locale,
                'source_locale' => $this->source_locale
            ]
        );
    }

    /**
     * @return void
     */
    public function delete(): void
    {
        $translations = PostTranslation::where('type', $this->type)
            ->where('locale', '!=', $this->locale)
            ->where('transaction_id', $this->transaction_id)
            ->orderBy('id')
            ->get();
        PostTranslation::where('type', $this->type)
            ->where('locale', $this->locale)
            ->where('element_id', $this->element_id)
            ->update([
                'transaction_id' => $this->createTransactionId(),
                'source_locale' => null
            ]);
        $current_translation = $translations->first();
        foreach($translations as $translation){
            if($translation->locale != $current_translation->locale){
                $translation->source_locale = $current_translation->locale;
            }else{
                $translation->source_locale = null;
            }
            $translation->save();
        }
    }

    /**
     * @return void
     */
    public function deleteItem(): void
    {
        PostTranslation::where('element_id', $this->element_id)->where('type', $this->type)->delete();
    }

    /**
     * @return int
     */
    private function createTransactionId(): int
    {
        $last_record = \DB::table('post_translations')
            ->where('type', $this->type)
            ->orderBy('transaction_id', 'DESC')
            ->first();
        if($last_record){
            return intval($last_record->transaction_id) + 1;
        }
        return 1;
    }

}
