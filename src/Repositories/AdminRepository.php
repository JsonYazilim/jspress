<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: AdminRepository.php
 * Author: Json Yazılım
 * Class: AdminRepository.php
 * Current Username: Erdinc
 * Last Modified: 14.07.2023 12:37
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Repositories;

use Illuminate\Http\Request;
use JsPress\JsPress\Models\Admin;
use JsPress\JsPress\Interfaces\AdminInterface;
use JsPress\JsPress\Interfaces\MediaInterface;
use Illuminate\Support\Facades\Hash;
use JsImage;


class AdminRepository implements AdminInterface{

    /**
     * @var string|null
     */
    private ?string $avatar = null;

    /**
     * @var MediaInterface
     */
    private MediaInterface $media;

    /**
     * @param MediaInterface $media
     */
    public function __construct(MediaInterface $media)
    {
        $this->media = $media;
    }

    /**
     * @param Admin $admin
     * @param array $data
     * @return Admin
     */
    public function save(Admin $admin, array $data): Admin
    {
        foreach($data as $key => $value){
            if( $key == 'password' ){
                $admin->{$key} = Hash::make($value);
            }else{
                $admin->{$key} = $value;
            }
        }
        if(!is_null($this->avatar)) $admin->avatar = $this->avatar;
        $admin->save();
        return $admin;
    }

    public function get(int $id): Admin
    {
        return Admin::find($id);

    }


    /**
     * @param object|null $file
     * @return AdminInterface
     */
    public function setAvatar(object|null $file): AdminInterface
    {
        if(!is_null($file)){
            $fileType = JsImage::getFileType($file);
            $fileName = JsImage::getFileName($file);
            $extension = $file->getClientOriginalExtension();
            [$width, $height] = getimagesize($file->path());
            $media = $this->media->setFile($file)
                ->setUserId(auth()->user()->id)
                ->setUploadName($fileName['upload_name'])
                ->setFileName($fileName['file_name'])
                ->setExtension($extension)
                ->setMime($file->getClientMimeType())
                ->setType($fileType)
                ->setWidth($width)
                ->setHeight($height)
                ->setSize($file->getSize())
                ->setPath()
                ->setDirectory()
                ->move()
                ->crop()
                ->save();
            $media_detail = $this->media->getDetail($media->id);
            $this->avatar = $media_detail['dimensions']['thumbnail']['original_url'];
        }
        return $this;
    }

    /**
     * @param Admin $admin
     * @param string $role
     * @return void
     */
    public function assign_role(Admin $admin, string $role): void
    {
        $admin->assignRole($role);
    }

    /**
     * @param Admin $admin
     * @param object $role
     * @return void
     */
    public function sync_role(Admin $admin, object $role): void
    {
        $admin->syncRoles([$role]);
    }

    /**
     * @param Admin $admins
     * @param Request $request
     * @return \Illuminate\Pagination\LengthAwarePaginator
     */
    public function list(Admin $admins, Request $request): \Illuminate\Pagination\LengthAwarePaginator
    {
        if( $request->input('q') ){
            $admins = $admins->where(function($query)use($request){
                return $query->where('name', 'like', '%'.$request->q.'%')
                    ->orWhere('email', 'like', $request->q.'%');
            });
        }
        if( $request->input('role') ){
            $role = $request->input('role');
            $admins = $admins->whereHas('rolePivot', function($query) use ($role){
                return $query->where('id', $role);
            });
        }
        $admins = $admins->orderBy('created_at', 'DESC');
        return $admins->paginate(10);
    }
}
