<?php

namespace JsPress\JsPress\Repositories;

use JsPress\JsPress\Interfaces\AnalysticInterface;
use JsPress\JsPress\Models\PostView;

class AnalysticRepository implements AnalysticInterface{

    /**
     * @var string|null
     */
    private ?string $start_date;

    /**
     * @var string|null
     */
    private ?string $end_date;

    /**
     * @var string
     */
    private string $type;

    private array $devices = ["desktop", "phone", "tablet"];

    /**
     * @param string|null $start_date
     * @return AnalysticInterface
     */
    public function setStartDate(?string $start_date): AnalysticInterface
    {
        $this->start_date = $start_date;
        return $this;
    }

    /**
     * @param string|null $end_date
     * @return AnalysticInterface
     */
    public function setEndDate(?string $end_date): AnalysticInterface
    {
        $this->end_date = $end_date;
        return $this;
    }

    /**
     * @param string $type
     * @return AnalysticInterface
     */
    public function setType(string $type): AnalysticInterface
    {
        $this->type = $type;
        return $this;
    }


    public function track(): array
    {
        return match ($this->type) {
            'device' => $this->trackByDevice(),
            'browser' => $this->trackByBrowser(),
            'page_view' => $this->trackByPageView(),
            default => [],
        };
    }

    private function trackByDevice(): array
    {
        $post_views = PostView::select('post_views.*',\DB::raw('count(*) as count'))
            ->whereBetween('created_at', [\Carbon\Carbon::parse($this->start_date)->startOfDay(), \Carbon\Carbon::parse($this->end_date)->endOfDay()])
            ->groupBy('device')
            ->get();

        $data['name'] = __('JsPress::backend.view_track_name');
        $data['labels'] = [];
        $data['values'] = [];
        $data['colors'] = [];
        $data['percentages'] = [];
        $total_row = $post_views->sum('count');
        foreach($this->devices as $device){
            $view_item = $post_views->where('device', $device)->first();
            if($view_item){
                $data['labels'][] = $this->setDeviceName($device);
                $data['values'][] = $view_item->count;
                $data['colors'][] = $this->setDeviceColor($device);
                $data['percentages'][] = round(($view_item->count / $total_row) * 100);
            }else{
                $data['labels'][] = $this->setDeviceName($device);
                $data['values'][] = 0;
                $data['colors'][] = $this->setDeviceColor($device);
                $data['percentages'][] = 0;
            }
        }
        return $data;

    }

    private function trackByPageView(): array
    {
        $post_view_query = PostView::select('post_views.*',\DB::raw('DATE(created_at) as x'))
            ->whereBetween('created_at', [\Carbon\Carbon::parse($this->start_date)->startOfDay(), \Carbon\Carbon::parse($this->end_date)->endOfDay()])
            ->orderBy('x')
            ->get();
        $post_views = $post_view_query->groupBy('x');
        $data['labels'] = [];
        $periods = \Carbon\CarbonPeriod::create(\Carbon\Carbon::parse($this->start_date)->format('Y-m-d'), \Carbon\Carbon::parse($this->end_date)->format('Y-m-d'))->toArray();
        $page_view_types = ["all","desktop","phone","tablet"];
        foreach($periods as $p => $period){
            $date = $period->format('Y-m-d');
            foreach($page_view_types as $key => $type){
                $data['series'][$key]['name'] = $this->setDeviceName($type);
                if(isset($post_views[$date])){
                    $view_item = $post_views[$date]->when($type !== 'all', function($query)use($type){
                            return $query->where('device', $type);
                        });
                    if($view_item->isNotEmpty()){
                        $data['series'][$key]['data'][] = [$period, $view_item->count()];
                    }else{
                        $data['series'][$key]['data'][] = [$period, 0];
                    }
                }else{
                    $data['series'][$key]['data'][] = [$period, 0];
                }
            }
            $data['labels'][] = $period->translatedFormat('j M');
        }

        foreach($page_view_types as $key => $type){
            $data['colors'][$key]  = $this->setDeviceColor($type);
        }
        $total_row = $post_view_query->count();
        $data['total'] = $total_row;
        $data['total_row'] = count($periods);
        $data['min_date'] = $periods[0];
        return $data;
    }

    private function trackByBrowser(): array
    {
        $post_views = PostView::select('post_views.*',\DB::raw('count(*) as count'))
            ->whereBetween('created_at',[$this->start_date,$this->end_date])
            ->groupBy('browser')
            ->get();

        $data['name'] = __('JsPress::backend.view_track_name');
        $data['labels'] = [];
        $data['values'] = [];
        $data['colors'] = [];
        if($post_views->isNotEmpty()){
            foreach($post_views as $view){
                $data['labels'][] = $this->setDeviceName($view->device);
                $data['values'][] = $view->count;
                $data['colors'][] = $this->setDeviceColor($view->device);
            }
        }

        return $data;

    }

    private function setDeviceName($device): string
    {
        return match ($device){
            'desktop' => __('JsPress::backend.desktop'),
            'phone' =>  __('JsPress::backend.mobile'),
            'tablet' =>  __('JsPress::backend.tablet'),
            'all' =>  __('JsPress::backend.all')
        };
    }

    private function setDeviceColor($device): string
    {
        return match ($device){
            'desktop' => '#3E97FF',
            'phone' => '#F1416C',
            'tablet' => '#50CD89',
            'all' => '#ffa800'
        };
    }

}
