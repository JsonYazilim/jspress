/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: site.js
 * Author: Json Yazılım
 * Class: site.js
 * Current Username: Erdinc
 * Last Modified: 14.05.2024 14:46
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */


const SiteFunc = {
    init: function () {

    },
    callGoogleFonts:function(f)
    {
        var apiUrl = [];
        apiUrl.push('https://fonts.googleapis.com/css?family=');
        apiUrl.push(f.family.replace(/ /g, '+'));
        if (
            f.variants.includes('regular') ||
            f.variants.includes('italic') ||
            f.variants.includes('300') ||
            f.variants.includes('400') ||
            f.variants.includes('500') ||
            f.variants.includes('600') ||
            f.variants.includes('700') ||
            f.variants.includes('800') ||
            f.variants.includes('900') ||
            f.variants.includes('300italic') ||
            f.variants.includes('500italic') ||
            f.variants.includes('600italic') ||
            f.variants.includes('700italic') ||
            f.variants.includes('800italic')
        ) {
            apiUrl.push(':');
            apiUrl.push(',');
            if(f.variants.includes('regular')){
                apiUrl.push(',');
                apiUrl.push('regular');
            }
            if(f.variants.includes('italic')){
                apiUrl.push(',');
                apiUrl.push('italic');
            }
            if(f.variants.includes('400')){
                apiUrl.push(',');
                apiUrl.push('400');
            }
            if(f.variants.includes('500')){
                apiUrl.push(',');
                apiUrl.push('500');
            }
            if(f.variants.includes('600')){
                apiUrl.push(',');
                apiUrl.push('600');
            }
            if(f.variants.includes('700')){
                apiUrl.push(',');
                apiUrl.push('700');
            }
            if(f.variants.includes('800')){
                apiUrl.push(',');
                apiUrl.push('800');
            }
            if(f.variants.includes('900')){
                apiUrl.push(',');
                apiUrl.push('900');
            }
            if(f.variants.includes('300italic')){
                apiUrl.push(',');
                apiUrl.push('300italic');
            }
            if(f.variants.includes('500italic')){
                apiUrl.push(',');
                apiUrl.push('500italic');
            }
            if(f.variants.includes('600italic')){
                apiUrl.push(',');
                apiUrl.push('600italic');
            }
            if(f.variants.includes('800italic')){
                apiUrl.push(',');
                apiUrl.push('800italic');
            }

        }
        if (f.variants.includes('italic')) {
            apiUrl.push(':');
            apiUrl.push('italic');
        }
        if (f.variants.includes('300')) {
            apiUrl.push(':');
            apiUrl.push('300');
        }
        if (f.variants.includes('400')) {
            apiUrl.push(':');
            apiUrl.push('400');
        }
        if (f.subsets.includes('latin-ext')) {
            apiUrl.push('&subset=');
            apiUrl.push('latin-ext');
        }
        var fp = document.querySelector('link[rel="shortcut icon"]'),
            h = '<link rel="stylesheet" href="'+apiUrl.join('')+'">';
        fp.insertAdjacentHTML('afterend', h);

    }
}
$(document).ready(function () {
    SiteFunc.init();
});
