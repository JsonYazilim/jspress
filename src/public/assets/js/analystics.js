/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: analystics.js
 * Author: Json Yazılım
 * Class: analystics.js
 * Current Username: Erdinc
 * Last Modified: 7.01.2024 22:42
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

var device_chart = null;
var device_chart_donut = null;
var page_view_chart = null;

const AnalysticsFunc = {
    init: function () {
        if(document.getElementById("device_analystics")){
            this.getDeviceAnalystics('device', window.start_date, window.end_date);
        }
        if(document.getElementById("page_views_report")){
            this.getPageViewAnalystics('page_view', window.start_date, window.end_date);
        }
    },
    fetchAnalystics:function(type, start_date, end_date){
        let data = {
            type:type,
            start_date:start_date,
            end_date:end_date
        };
        let options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                "X-CSRF-Token": document.head.querySelector("[name~=csrf-token][content]").content
            },
            body: JSON.stringify(data)
        }
        return fetch(location.origin + "/js-admin/analystic/track", options)
            .then((response) => response.json())
            .then(function(response){
                return response;
            });
    },
    getDeviceAnalystics:function(type, start_date, end_date, chart = false){
        this.fetchAnalystics(type, start_date, end_date).then(function(res) {
            device_chart = AnalysticsFunc.renderDeviceAnalystics(res.name, res.values, res.labels, res.colors, chart);
            device_chart_donut = AnalysticsFunc.renderDeviceAnalysticsDonut(res.name, res.values, res.labels, res.colors, chart);
        });
    },
    getPageViewAnalystics:function(type, start_date, end_date, chart = false){
        this.fetchAnalystics(type, start_date, end_date).then(function(res) {
            page_view_chart = AnalysticsFunc.renderPageViewAnalystics(res.series, res.labels, res.colors, res.min_date, res.total_row, chart);
            document.querySelector('.totalPageView').textContent = res.total;
        });
    },
    renderDeviceAnalystics:function(name, values, labels, colors, chart){
        if(!chart){
            return this.initDeviceBarChart(name, values, labels, colors);
        }else{
            device_chart.updateOptions({
                xaxis: {
                    categories: labels
                },
                series: [{
                    name:name,
                    data: values
                }],
                colors: colors
            });
            return device_chart;
        }
    },
    renderDeviceAnalysticsDonut:function(name, values, labels, colors, chart){
        if(!chart){
            var options = {
                series: values,
                chart: {
                    width: document.getElementById('device_analystics').offsetWidth - 100,
                    type: 'pie',
                    foreColor: window.theme === 'dark' ? '#fff':'#000',
                    fontFamily:'Poppins,Helvetica,sans-serif',
                },
                colors:colors,
                labels: labels,
                legend: {
                    position:'top',
                    color:'#fff',
                    fontFamily:'Poppins,Helvetica,sans-serif',
                    fontSize:'14px'
                },
                responsive: [{
                    breakpoint: 480,
                    options: {
                        chart: {
                            width: 200
                        },
                        legend: {
                            show:false
                        }
                    }
                }]
            };
            device_chart_donut = new ApexCharts(document.querySelector("#donut_chart"), options);
            device_chart_donut.render();
        }else{
            device_chart_donut.updateOptions({
                series:values,
                labels: labels,
                colors: colors
            });
        }
        return device_chart_donut;
    },
    renderPageViewAnalystics:function(series, labels, colors, min_date, rows, chart){
        if(!chart){
            return this.initPageViewChart(series, labels, colors, min_date, rows);
        }else{
            page_view_chart.updateOptions({
                xaxis: {
                    categories: labels
                },
                series: series,
                colors: colors
            });
            return page_view_chart;
        }
    },
    updateDeviceAnalystics:function(type, start_date, end_date){
        this.getDeviceAnalystics(type, start_date, end_date, true);
    },
    updatePageViewAnalystics:function(type, start_date, end_date){
        this.getPageViewAnalystics(type, start_date, end_date, true);
    },
    initDeviceBarChart:function(name, values, labels, colors){
        var e = document.getElementById("device_analystics");
        var a = KTUtil.getCssVariableValue("--bs-gray-800"),
            t = KTUtil.getCssVariableValue("--bs-border-dashed-color"),
            l = new ApexCharts(e, {
                series: [{
                    name: name,
                    data: values
                }],
                chart: {
                    fontFamily: "inherit",
                    type: "bar",
                    height: 350,
                    toolbar: {
                        show: !1
                    }
                },
                plotOptions: {
                    bar: {
                        borderRadius: 8,
                        horizontal: !0,
                        distributed: !0,
                        barHeight: 50,
                        dataLabels: {
                            position: "bottom"
                        }
                    }
                },
                dataLabels: {
                    enabled: !0,
                    textAnchor: "start",
                    offsetX: 0,
                    style: {
                        fontSize: "14px",
                        fontWeight: "600",
                        align: "left"
                    }
                },
                legend: {
                    show: !1
                },
                colors: colors,
                xaxis: {
                    categories: labels,
                    labels: {
                        style: {
                            colors: a,
                            fontSize: "14px",
                            fontWeight: "600",
                            align: "left"
                        }
                    },
                    axisBorder: {
                        show: !1
                    }
                },
                yaxis: {
                    labels: {
                        formatter: function(e, a) {
                            return Number.isInteger(e) ? e + " - " + parseInt(100 * e / 18).toString() + "%" : e
                        },
                        style: {
                            colors: a,
                            fontSize: "14px",
                            fontWeight: "600"
                        },
                        offsetY: 2,
                        align: "left"
                    }
                },
                grid: {
                    borderColor: t,
                    xaxis: {
                        lines: {
                            show: !0
                        }
                    },
                    yaxis: {
                        lines: {
                            show: !1
                        }
                    },
                    strokeDashArray: 4
                },
                tooltip: {
                    style: {
                        fontSize: "12px"
                    },
                    y: {
                        formatter: function(e) {
                            return e
                        }
                    }
                }
            });
        setTimeout((function() {
            l.render();
        }), 200);
        return l;
    },
    initPageViewChart:function(series, labels, colors, min_date, rows){
        var e = document.getElementById("page_views_report"),
            t = (parseInt(KTUtil.css(e, "height")), KTUtil.getCssVariableValue("--bs-gray-500")),
            a = KTUtil.getCssVariableValue("--bs-gray-200"),
            o = KTUtil.getCssVariableValue("--bs-info"),
            s = KTUtil.getCssVariableValue("--bs-light-info"),
            l = new ApexCharts(e, {
                series: series,
                chart: {
                    fontFamily: "inherit",
                    type: "area",
                    height: 350,
                    toolbar: {
                        show: !1
                    }
                },
                plotOptions: {},
                legend: {
                    show: !1
                },
                dataLabels: {
                    enabled: !1
                },
                fill: {
                    type: "solid",
                    opacity:.1
                },
                stroke: {
                    curve: "smooth",
                    show: !0,
                    width: 3,
                    colors: colors
                },
                xaxis: {
                    type:'datetime',
                    min:min_date,
                    axisBorder: {
                        show: !1
                    },
                    axisTicks: {
                        show: !1
                    },
                    labels: {
                        style: {
                            colors: t,
                            fontSize: "12px"
                        },
                        formatter: function (value) {
                            return moment(value).lang('tr').format('DD MMM YYYY');
                        }
                    },
                    crosshairs: {
                        position: "front",
                        stroke: {
                            color: o,
                            width: 1,
                            dashArray: 3
                        }
                    },
                    tooltip: {
                        enabled: !0,
                        formatter: void 0,
                        offsetY: 0,
                        style: {
                            fontSize: "12px"
                        }
                    },
                },
                yaxis: {
                    labels: {
                        style: {
                            colors: t,
                            fontSize: "12px"
                        }
                    }
                },
                states: {
                    normal: {
                        filter: {
                            type: "none",
                            value: 0
                        }
                    },
                    hover: {
                        filter: {
                            type: "none",
                            value: 0
                        }
                    },
                    active: {
                        allowMultipleDataPointsSelection: !1,
                        filter: {
                            type: "none",
                            value: 0
                        }
                    }
                },
                tooltip: {
                    style: {
                        fontSize: "12px"
                    },
                    y: {

                    }
                },
                colors: colors,
                grid: {
                    borderColor: a,
                    strokeDashArray: 4,
                    yaxis: {
                        lines: {
                            show: !0
                        }
                    }
                },
                markers: {
                    strokeColor: o,
                    strokeWidth: 3
                }
            });
        setTimeout((function() {
            l.render();
        }), 200);
        return l;
    }
}
$(document).ready(function () {
    AnalysticsFunc.init();
});
