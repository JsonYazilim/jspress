const xhr = new XMLHttpRequest();
const token = document.head.querySelector('meta[name="csrf-token"]').content;
const loadingImage = '../jspress/assets/media/svg/files/file-loading.svg';
const api_url = location.origin+'/js-admin';
const mediaUrl = api_url+'/loadFiles';
const mediaDetailUrl = api_url+'/getMediaDetail';
const MediaWrapper = document.getElementById('imageList');
var selectedFiles = {
    files : [],
    onChange(){
        let fileManagerFooter = document.getElementById('fileManagerFooter');
        let countEl = document.getElementById('fileCount');
        let imagesEl = document.getElementById('selectedFiles');
        countEl.textContent = "";
        imagesEl.innerHTML = "";
        if(this.files.length >= 1){
            countEl.textContent = this.files.length.toString();
            this.files.forEach(image => {
                let imageWrapper = document.createElement('div');
                imageWrapper.classList.add('fileImageWrapper');
                let imageEl = JsPressFunc.createImageElement(image, false);
                imageWrapper.appendChild(imageEl);
                imagesEl.appendChild(imageWrapper);
            });
            fileManagerFooter.classList.remove('d-none');
        }else{
            fileManagerFooter.classList.add('d-none');
        }
    },
    get selected_files(){
        return this.files;
    },
    set selected_files(files){
        this.files = files;
        this.onChange();
    }
}
var JsPressFunc = {
    init: function () {
        if(document.querySelectorAll('.jspress-datepicker').length > 0){
            this.setDatepickers();
        }
        if(document.getElementById('js_popup')){
            this.initPopupSettings();
        }
    },
    changeTheme: function (theme) {
        return new Promise((resolve, reject) => {
            xhr.onreadystatechange = function() {
                if (xhr.readyState !== 4) {
                    return;
                }
                if (xhr.status === 200) {
                    location.reload();
                }else{
                    console.log('error');
                }
            };
            xhr.open("POST", api_url+'/change-theme', true);
            xhr.setRequestHeader('X-CSRF-TOKEN', token);
            const data = new FormData();
            data.append("theme", theme);
            xhr.send(data);
        });
    },
    uploadFile:function(file, action, async = false, page = 0){
        return new Promise((resolve, reject) => {
            xhr.onreadystatechange = function() {
                if (xhr.readyState !== 4) {
                    return;
                }
                if (xhr.status === 200) {
                    let response_data = {
                        'status' :  xhr.status,
                        'data' : JSON.parse(xhr.responseText)
                    }
                    if( async ){
                        response_data.count  = page + 1;
                    }
                    resolve(response_data);
                }else{
                    let response_data = {
                        'status' :  xhr.status
                    }
                    if( async ){
                        response_data.count  = page + 1;
                    }
                    resolve(response_data);
                }
            };
            xhr.open("POST", api_url+"/upload", true);
            xhr.setRequestHeader('X-CSRF-TOKEN', token);
            const formData = new FormData();
            formData.append("file", file);
            formData.append("action", action);
            xhr.send(formData);
        });

    },
    MediaFileUpload:function(file_list, action, page = 0){
        let files = file_list;
        let total_files = file_list.length;
        let uniq = Math.floor(Math.random() * 100000);
        if( page < total_files){
            let loadingImage = this.createLoading(uniq);
            MediaWrapper.prepend(loadingImage);
            this.uploadFile(files[page], action, true , page).then(function(res){
                page = res.count;
                let el = loadingImage.getElementsByTagName('img')[0];
                el.classList.remove('loading-image');
                if(res.status === 200){
                    let status = res.data.status;
                    let file = res.data.data;
                    if(status){
                        loadingImage.setAttribute('data-media', file.id);
                        el = JsPressFunc.setImageSrc(el, file);
                        el.setAttribute('title', file.upload_name);
                        el.addEventListener('click', () => {
                            JsPressFunc.getMediaDetail(file.id);
                        });
                    }else{
                        document.querySelector('.loading-image[data-id="'+uniq+'"]').remove();
                        Swal.fire({
                            text: res.data.msg,
                            icon: 'error',
                            buttonsStyling: !1,
                            confirmButtonText: trans['confirm'],
                            customClass: { confirmButton: "btn btn-primary" }
                        });
                    }
                }else{
                    if(res.status === 413){
                        el.classList.add('w-50px');
                        el.src = '../jspress/assets/media/icons/duotune/arrows/arr015-danger.svg';
                        el.title = 'Dosya boyutunu izin verilen dosya boyutundan büyüktür!';
                        loadingImage.insertAdjacentHTML('beforeend', '<span class="text-center fs-8">'+trans.max_upload_size_error+'</span>');
                    }
                    if(res.status === 422){
                        el.classList.add('w-50px');
                        el.src = '../jspress/assets/media/icons/duotune/arrows/arr015-danger.svg';
                        el.title = 'Desteklenmeyen dosya tipi!';
                        loadingImage.insertAdjacentHTML('beforeend', '<span class="text-center fs-8">'+trans.file_size_error+'</span>');
                    }
                }
                return JsPressFunc.MediaFileUpload(files, action, page);
            });
        }else{
            document.getElementById('file').value = "";
            var fileTab = document.getElementById('filesTabLink');
            if(fileTab){
                fileTab.click();
                JsPressFunc.__reCallFileManager(1);
            }
        }
    },
    createLoading:function(uniq = null, fullwidth = false){
        const divArr = ['d-flex', 'flex-column', 'align-items-center', 'justify-content-center', 'image_wrap', 'loading-image', 'me-2', 'mb-2'];
        const divElement = document.createElement('div');
        const imgElement = document.createElement('img');
        divElement.classList.add(...divArr);
        if( fullwidth ){
            divElement.classList.add(...['w-100', 'min-h-450px']);
        }
        if(uniq !== null){
            divElement.setAttribute("data-id", uniq);
        }
        imgElement.src = loadingImage;
        divElement.appendChild(imgElement);
        return divElement;
    },
    loadMediaViaFetch:async function(page = 1, term = null, data = null, pagination = 18){
        const response = await fetch(mediaUrl, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body:JSON.stringify({
                _token:token,
                page:page,
                term:term,
                data:data,
                pagination:pagination
            })
        });
        return response.json();
    },
    loadMediaDetailViaFetch:async function(file_id){
        const response = await fetch(mediaDetailUrl+'/'+file_id, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body:JSON.stringify({
                _token:token
            })
        });
        return response.json();
    },
    getMediaFiles:function(page, pagination){
        JsPressFunc.loadMediaViaFetch(page, null, null, pagination).then((data) => {
            document.getElementById('imageList').innerHTML = "";
            JsPressFunc.MediaFilesTheming(data.files);
            if(data.last_page > 1){
                let paginator = document.getElementById('pagination');
                paginator.innerHTML = data.links;
                paginator.setAttribute('data-current', data.current_page);
                JsPressFunc.setPagination();
            }

        });
    },
    setPagination:function(){
        let paginator = document.getElementById('paginator').querySelectorAll('li');
        paginator.forEach((item)=>{
            item.addEventListener('click', function(){
                let page_id = this.querySelector('a').getAttribute('data-page');
                JsPressFunc.getMediaFiles(parseInt(page_id), 40);
            });
        });
    },
    MediaFilesTheming:function(files){
        const imageWrapperClassList = ['d-flex', 'flex-column', 'align-items-center', 'justify-content-center', 'image_wrap', 'me-2', 'mb-2'];
        for (let i in files) {
            let img = this.createImageElement(files[i]);
            let imageWrapper = document.createElement('div');
            imageWrapper.classList.add(...imageWrapperClassList);
            imageWrapper.setAttribute('data-media', files[i].id);
            imageWrapper.appendChild(img);
            MediaWrapper.appendChild(imageWrapper);
        }
    },
    createImageElement:function(file, addEvent = true){
        let e = document.createElement('img');
        e.dataset.id = file.id;
        e.title = file.upload_name;
        e.alt = file.file_name;
        e.width = 75;
        e = this.setImageSrc(e, file);
        if(addEvent){
            e.addEventListener('click', () => {
                this.getMediaDetail(file.id);
            });
        }
        return e;
    },
    setImageSrc:function(e, file){
        if(file.type === 'image'){
            e.src = file.has_dimension ? file.dimensions.thumbnail.url : file.original_url;
        }
        if(file.type === 'application'){
            if(file.mime === 'application/pdf'){
                e.src =  location.origin+"/jspress/assets/media/svg/files/pdf.svg";
            }else if(file.mime === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'){
                e.src =  location.origin+"/jspress/assets/media/svg/files/doc.svg";
            }else if( file.mime === 'application/vnd.ms-excel' ){
                e.src =  location.origin+"/jspress/assets/media/svg/files/excel.svg";
            }else if( file.mime === 'application/application/zip' || file.mime === 'application/x-zip-compressed' || file.mime === 'multipart/x-zip'){
                e.src =  location.origin+"/jspress/assets/media/svg/files/zip.svg";
            }else{
                e.src =  location.origin+"/jspress/assets/media/icons/duotune/files/fil004.svg";
            }
        }
        if(file.type === 'video'){
            e.src =  location.origin+"/jspress/assets/media/svg/files/video-play.svg";
        }
        if(file.type === 'audio'){
            e.src =  location.origin+"/jspress/assets/media/svg/files/audio.svg";
        }
        if(file.type === 'text'){
            if(file.mime === 'text/csv'){
                e.src =  location.origin+"/jspress/assets/media/svg/files/csv.svg";
            }else{
                e.src =  location.origin+"/jspress/assets/media/icons/duotune/files/txt.svg";
            }
        }
        return e;
    },
    getImageSrc:function(file){
        var src = "";
        if(file.type === 'image'){
            src = file.has_dimension ? file.dimensions.thumbnail.url : file.original_url;
        }
        if(file.type === 'application'){
            if(file.mime === 'application/pdf'){
                src = location.origin+"/jspress/assets/media/svg/files/pdf.svg";
            }else if(file.mime === 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'){
                src =  location.origin+"/jspress/assets/media/svg/files/doc.svg";
            }else if( file.mime === 'application/vnd.ms-excel' ){
                src =  location.origin+"/jspress/assets/media/svg/files/excel.svg";
            }else if( file.mime === 'application/application/zip' || file.mime === 'application/x-zip-compressed' || file.mime === 'multipart/x-zip'){
                src =  location.origin+"/jspress/assets/media/svg/files/zip.svg";
            }else{
                src =  location.origin+"/jspress/assets/media/icons/duotune/files/fil004.svg";
            }
        }
        if(file.type === 'video'){
            src =  location.origin+"/jspress/assets/media/svg/files/video-play.svg";
        }
        if(file.type === 'audio'){
            src =  location.origin+"/jspress/assets/media/svg/files/audio.svg";
        }
        if(file.type === 'text'){
            if(file.mime === 'text/csv'){
                src =  location.origin+"/jspress/assets/media/svg/files/csv.svg";
            }else{
                src =  location.origin+"/jspress/assets/media/icons/duotune/files/txt.svg";
            }
        }
        return src;
    },
    createModalCloseBtn:function(Modal){
        const closeButtonClassList = ['btn', 'btn-sm', 'btn-icon', 'btn-active-color-primary'];
        const closeButton = document.createElement('div');
        closeButton.classList.add(...closeButtonClassList);
        closeButton.innerHTML = '<span class="svg-icon svg-icon-1"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"> <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" /> <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" /> </svg> </span>';
        closeButton.addEventListener('click', () => {
            Modal.hide();
        });
        return closeButton;
    },
    createImageObjectModal:function(file){
        const image = document.createElement('img');
        image.src = file.original_url;
        return image;
    },
    createEditImageElement:function(file){
        const btnWrapper = document.createElement('div');
        const btn = document.createElement('button');
        btnWrapper.classList.add('d-block', 'w-100', 'text-center');
        btn.classList.add('mt-5', 'btn', 'btn-light-primary');
        btn.textContent = 'Resmi Düzenle';
        btnWrapper.appendChild(btn);
        return btnWrapper;
    },
    getMediaDetail:function(file_id){
        const MediaDetailModal = document.getElementById('mediaDetailModal');
        const MediaDetailModalObject = new bootstrap.Modal(MediaDetailModal, {
            keyboard: false
        });
        JsPressFunc.loadMediaDetailViaFetch(file_id).then((data) => {
            if(data.hasOwnProperty('status') && !data.status){
                Swal.fire({
                    text: data.msg,
                    icon: "error",
                    buttonsStyling: !1,
                    confirmButtonText: trans.ok,
                    customClass: { confirmButton: "btn btn-primary" }
                });
            }
            MediaDetailModal.querySelector('.modal-header').innerHTML = "";
            MediaDetailModal.querySelector('.modal-body .imageCropArea').innerHTML = "";
            MediaDetailModal.querySelector('.modal-body ul.nav').innerHTML = "";
            MediaDetailModal.querySelector('.modal-body .tab-content').innerHTML = "";
            MediaDetailModal.querySelector('.modal-footer').innerHTML = "";
            const modalHeader = MediaDetailModal.querySelector('.modal-header');
            const modalFooter = MediaDetailModal.querySelector('.modal-footer');
            const modalImageArea = document.getElementById('imageDetailBox');
            const modalTitle = document.createElement('h2');
            const modalCloseButton = this.createModalCloseBtn(MediaDetailModalObject);
            const modalFooterApplyBtn = this.createMediaSaveButton(data.id);
            const modalFooterDeleteBtn = this.createMediaDeleteButton(data.id, modalCloseButton);
            modalTitle.textContent = data.upload_name;
            modalHeader.appendChild(modalTitle);
            modalHeader.appendChild(modalCloseButton);
            if(data.type === 'image' && data.mime !== 'image/svg+xml'){
                const imageWrapper = this.createImageObjectModal(data);
                const editImageBtn = this.createEditImageElement(data);
                modalImageArea.querySelector('.imageCropArea').appendChild(imageWrapper);
                modalImageArea.querySelector('.imageCropArea').appendChild(editImageBtn);
            }else{
                let imageWrapper = document.createElement('img');
                imageWrapper.classList.add('h-200px');
                let image = this.setImageSrc(imageWrapper, data);
                modalImageArea.querySelector('.imageCropArea').appendChild(image);
            }
            this.setMediaDetailTable(data);
            this.setMediaDetails(data.details);
            modalFooter.appendChild(modalFooterDeleteBtn);
            modalFooter.appendChild(modalFooterApplyBtn);
            MediaDetailModalObject.show();
        });
    },
    setMediaDetailTable:function(details){
        const mediaDetailList = document.getElementById('MediaDetailList');
        const flexClassList = ['text-gray-700', 'fw-bold', 'fs-6', 'me-2'];
        const spanClassList = ['text-gray-900', 'fs-6'];
        mediaDetailList.innerHTML = "";
        for( const detail in details ){
            let detailFlex = document.createElement('div');
            let detailInnerDiv = document.createElement('div');
            let detailSpan = document.createElement('span');
            detailFlex.classList.add('d-flex');
            detailInnerDiv.classList.add(...flexClassList);
            detailSpan.classList.add(...spanClassList);
            if(details[detail] !== null && detail !== 'id' && detail !== 'details' && detail !== 'dimensions' && detail !== 'has_dimension'){
                detailInnerDiv.textContent = trans[detail]+': ';
                if( detail === 'width' || detail === 'height' ){
                    detailSpan.textContent = details[detail]+' px';
                }else{
                    detailSpan.textContent = details[detail];
                }
            }
            detailFlex.appendChild(detailInnerDiv);
            detailFlex.appendChild(detailSpan);
            mediaDetailList.appendChild(detailFlex);
        }
    },
    setMediaDetails:function(details){
        const Tab = document.getElementById('MediaDetailTab');
        let ul = Tab.querySelector('ul');
        let Content = document.getElementById('MediaDetailTabContent');
        ul.innerHTML = "";
        for( let locale in details ){
            let li = this.createMediaLiElement(locale, details[locale]);
            let tabContent = this.createMediaTabContentElement(locale, details[locale]);
            ul.appendChild(li);
            Content.appendChild(tabContent);
        }
        let firstLi = ul.getElementsByTagName('li')[0];
        let firstTab = Content.getElementsByClassName('tab-pane')[0];
        firstLi.querySelector('a').classList.add('active');
        firstTab.classList.add('show');
        firstTab.classList.add('active');
    },
    createMediaLiElement(locale, detail){
        const li = document.createElement('li');
        const a = document.createElement('a');
        const c = ['nav-link', 'text-active-primary', 'pb-4'];
        li.classList.add('nav-item');
        a.classList.add(...c);
        a.setAttribute('data-bs-toggle', 'tab');
        a.href = '#'+locale;
        a.innerHTML = '<span class="symbol symbol-20px me-1"> <img class="rounded-1" src="../jspress/assets/media/flags/svg/'+locale+'.svg" alt=""> </span>' + detail.language;
        li.appendChild(a);
        return li;
    },
    createMediaTabContentElement:function(locale, detail){
        const tab = document.createElement('div');
        const inputClassList = ['form-control', 'form-control-lg', 'mb-3', 'mb-lg-0']
        const labelClassList = ['col-sm-4', 'col-form-label']
        const tabClassList = ['tab-pane', 'fade'];
        const formClassList = ['form-group', 'row', 'mt-5', 'mb-5'];
        tab.classList.add(...tabClassList);
        tab.id = locale;
        tab.setAttribute('role', 'tabpanel');
        for( let i in detail ){
            if( i !== 'language' ){
                let div = document.createElement('div');
                let inputDiv = document.createElement('div');
                let input = document.createElement('input');
                let label = document.createElement('label');

                div.classList.add(...formClassList);
                input.classList.add(...inputClassList);
                inputDiv.classList.add('col-sm-8');
                label.classList.add(...labelClassList);

                input.type = 'text';
                input.name = 'detail['+locale+']['+i+']';
                input.id = 'detail_'+locale+'_'+i;
                input.value = detail[i]

                label.htmlFor = 'detail_'+locale+'_'+i;
                label.textContent = trans[i];
                label.style.cursor = 'pointer';
                inputDiv.appendChild(input);
                div.appendChild(label);
                div.appendChild(inputDiv);
                tab.appendChild(div);
            }
        }
        return tab;
    },
    createMediaSaveButton:function(id){
        const button = document.createElement('button');
        button.textContent = trans['save'];
        button.type = 'button';
        button.classList.add(...['btn', 'btn-primary']);
        this.saveMediaDetail(button, id);
        return button;
    },
    createMediaDeleteButton:function(id, modalCloseButton){
        const removeButton = document.createElement('button');
        removeButton.textContent = trans['delete_image'];
        removeButton.type = 'button';
        removeButton.classList.add(...['btn', 'btn-danger']);
        this.deleteMedia(removeButton, id , modalCloseButton);
        return removeButton;
    },
    saveMediaDetail:function(button, id){
        button.addEventListener('click', () => {
            if(document.querySelector('#mediaDetailModal .modal-footer span')){
                document.querySelector('#mediaDetailModal .modal-footer span').remove();
            }
            const form = document.getElementById('mediaDetailForm');
            const data = new FormData(form);
            xhr.onreadystatechange = function() {
                if (xhr.readyState !== 4) {
                    return;
                }
                if (xhr.status === 200) {
                    document.querySelector('#mediaDetailModal .modal-footer').insertAdjacentHTML('afterbegin', '<span class="flex-grow-1 fs-4 text-success">Dosya başarılı şekilde güncellendi</span>');
                }else{
                    console.log('error');
                }
            };
            xhr.open("POST", api_url+'/saveMediaDetail/'+id, true);
            xhr.setRequestHeader('X-CSRF-TOKEN', token);
            xhr.send(data);
        });
    },
    deleteMedia:function(button, id, modalCloseButton){
        button.addEventListener('click', () => {
            Swal.fire({
                text: trans['delete_image_warning'],
                icon: "warning",
                showCancelButton: true,
                buttonsStyling: true,
                confirmButtonText: trans['confirm'],
                cancelButtonText: trans["no_return"],
                customClass: {
                    confirmButton: "btn btn-primary",
                    cancelButton: "btn btn-active-light"
                },
            }).then(
                function (t) {
                    if( t.isConfirmed ){
                        const data = new FormData();
                        data.append('id', id);
                        xhr.onreadystatechange = function() {
                            if (xhr.readyState !== 4) {
                                return;
                            }
                            if (xhr.status === 200) {
                                var fileTab = document.getElementById('filesTabLink');
                                if(fileTab){
                                    JsPressFunc.__reCallFileManager(1);
                                }else{
                                    document.getElementById('imageList').querySelector('div[data-media="'+id+'"]').remove();
                                }
                                if(modalCloseButton != null){
                                    modalCloseButton.click();
                                }
                            }else{
                                console.log('error');
                            }
                        };
                        xhr.open("POST", api_url+'/delete-file/'+id, true);
                        xhr.setRequestHeader('X-CSRF-TOKEN', token);
                        xhr.send(data);
                    }
                }
            );
        });
    },
    searchMediaFile:function(){
        let input = document.getElementById('mediaSearchInput');
        input.addEventListener('change', (e) => {
            e.preventDefault();
            document.getElementById('imageList').innerHTML = "";
            document.getElementById('paginator').innerHTML = "";
            let term = input.value;
            JsPressFunc.loadMediaViaFetch(1, term, null, 40).then((data) => {
                JsPressFunc.MediaFilesTheming(data.files);
            });
        });
    },
    clearMediaFilter:function(){
        let input = document.getElementById('mediaSearchInput');
        input.value = "";
        document.getElementById('imageList').innerHTML = "";
        JsPressFunc.getMediaFiles(1, 40);
    },
    updatePanelForm:function(){
        const panelForm = document.getElementById('panelForm');
        panelForm.addEventListener('submit', (e) => {
            e.preventDefault();
            const data = new FormData(panelForm);
            xhr.onreadystatechange = function() {
                if (xhr.readyState !== 4) {
                    return;
                }
                if (xhr.status === 200) {
                    Swal.fire({
                        text: trans['update_message'],
                        icon: "success",
                        buttonsStyling: false,
                        confirmButtonText: trans['confirm_button'],
                        customClass: { confirmButton: "btn btn-primary" }
                    }).then(
                        function (t) {
                            location.reload();
                        }
                    );
                }else{
                    console.log('error');
                }
            };
            xhr.open("POST", api_url+'/admin/update-panel', true);
            xhr.setRequestHeader('X-CSRF-TOKEN', token);
            xhr.send(data);
        });
    },
    updateLanguage:function(locale){
        return new Promise((resolve, reject) => {
            xhr.onreadystatechange = function() {
                if (xhr.readyState !== 4) {
                    return;
                }
                if (xhr.status === 200) {
                    location.reload();
                }else{
                    console.log('error');
                }
            };
            xhr.open("POST", api_url+'/change-language', true);
            xhr.setRequestHeader('X-CSRF-TOKEN', token);
            const data = new FormData();
            data.append("locale", locale);
            xhr.send(data);
        });
    },
    updateContentLanguage:function(locale){
        return new Promise((resolve, reject) => {
            xhr.onreadystatechange = function() {
                if (xhr.readyState !== 4) {
                    return;
                }
                if (xhr.status === 200) {
                    location.reload();
                }else{
                    console.log('error');
                }
            };
            xhr.open("POST", api_url+'/change-content-language', true);
            xhr.setRequestHeader('X-CSRF-TOKEN', token);
            const data = new FormData();
            data.append("locale", locale);
            xhr.send(data);
        });
    },
    generateFormFields:function(){
        let categoryEl = document.getElementById('is_category');
        let categoryContainers = document.getElementsByClassName('dependOnCategory');
        categoryEl.addEventListener('change', function(e){
            JsPressFunc.resetValidation();
            categoryContainers.forEach((currentEl) => {
                let inputs = currentEl.querySelectorAll('input');
                if(categoryEl.checked){
                    inputs.forEach((inputEl, i) => {
                        let disableEl = currentEl.closest('.post_type_container').querySelector('.disablePostType');
                        if(!disableEl.checked){
                            inputEl.removeAttribute('disabled', '');
                            if( i !== inputs.length - 1 ){
                                inputEl.setAttribute('required', '');
                            }
                        }else{
                            inputEl.removeAttribute('required', '');
                        }
                    })
                    currentEl.classList.remove('d-none');
                }else{
                    inputs.forEach((inputEl, e) => {
                        inputEl.setAttribute('disabled', '');
                        inputEl.removeAttribute('required', '');
                    })
                    currentEl.classList.add('d-none');
                }
                inputs[0].addEventListener('change', function(e) {
                    let value = e.currentTarget.value;
                    let slugifyEl =  this.closest('.row').querySelector('.slugify');
                    if( slugifyEl.value === "" ){
                        slugifyEl.value = JsPressFunc.slugify(value);
                    }
                });
            });
        });
        this.disablePostTypeDetails();
        this.toggleSegmentDisableAction();
        this.submitPostTypeForm();
    },
    toggleSegmentDisableAction:function() {
        const el = document.getElementById('is_url');
        const disEl = document.querySelector('.segmentDisable');
        el.addEventListener('change', (e) => {
            if(el.checked){
                disEl.classList.remove('d-none');
            }else{
                disEl.classList.add('d-none');
                document.getElementById('is_segment_disable').checked = false;
            }
        });
        this.checkIfIsSegmentChecked();
    },
    checkIfIsSegmentChecked:function(){
        const segment_input = document.getElementById('is_segment_disable');
        segment_input.addEventListener('change', function() {
            if(this.checked){
                JsPressFunc.togglePostTypeSlugFields(true);
            }else{
                JsPressFunc.togglePostTypeSlugFields(false);
            }
        });

    },
    togglePostTypeSlugFields:function(s){
        const elements = document.querySelectorAll('input[data-type="slug"]');
        if(s){
            elements.forEach((el) => {
                el.setAttribute('disabled', '');
            });
        }else{
            elements.forEach((el) => {
                el.removeAttribute('disabled');
            });
        }
    },
    submitPostTypeForm:function(){
        const form = document.getElementById('post_type_create_form');
        form.addEventListener('submit', (e) => {
            e.preventDefault();
            JsPressFunc.resetValidation();
            let elements = form.querySelectorAll('[required]');
            var errors = 0;
            elements.forEach((el) => {
                if(el.value === "" || el.value == null){
                    el.insertAdjacentHTML('afterend', '<span class="pt-1 pb-1 text-danger fs-7 not-valid-input">'+trans.is_required_message+'</span>');
                    errors++;
                }
            });
            if( errors > 0 ){
                return false;
            }
            JsPressFunc.storePostType(form);
        });
    },
    storePostType:function(form){
        xhr.onreadystatechange = function() {
            if (xhr.readyState !== 4) {
                return;
            }
            if (xhr.status === 200) {
                let res = JSON.parse(xhr.responseText);
                if( res.status === 0 ){
                    Swal.fire({
                        text: res.msg,
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: trans.ok,
                        customClass: { confirmButton: "btn btn-primary" }
                    });
                }else{
                    Swal.fire(
                        {
                            text: res.msg,
                            icon: "success",
                            buttonsStyling: !1,
                            confirmButtonText: trans.ok,
                            customClass: { confirmButton: "btn btn-primary" }
                        }).then(
                        function (t) {
                            if(res.redirect == null){
                                location.reload();
                            }else{
                                window.location.href = res.redirect;
                            }
                        }
                    );
                }
            }
        };
        xhr.open("POST", api_url+'/post-type/store', true);
        xhr.setRequestHeader('X-CSRF-TOKEN', token);
        const data = new FormData(form);
        xhr.send(data);
    },
    resetValidation:function(){
        const form = document.getElementById('post_type_create_form');
        form.querySelectorAll('.not-valid-input').forEach((el) => {
            el.remove();
        });
    },
    autoCompleteSlugPostType:function(){
        let postTypeContainer = document.getElementsByClassName('postTypeContainer');
        postTypeContainer.forEach((el, index) => {
            let inputs = el.querySelectorAll('input');
            inputs[0].addEventListener('change', function(e){
                if(inputs[2].value === "" && !inputs[2].disabled){
                    inputs[2].value = JsPressFunc.slugify(this.value);
                }
            });
        });
    },
    slugify:function(text){
        let trMap = {
            'çÇ':'c',
            'ğĞ':'g',
            'şŞ':'s',
            'üÜ':'u',
            'ıİ':'i',
            'öÖ':'o'
        };
        for(let key in trMap) {
            text = text.replace(new RegExp('['+key+']','g'), trMap[key]);
        }
        return  text.replace(/[^-a-zA-Z0-9\s]+/ig, '') // remove non-alphanumeric chars
            .replace(/\s/gi, "-") // convert spaces to dashes
            .replace(/[-]+/gi, "-") // trim repeated dashes
            .toLowerCase();
    },
    toastrError:function(msg){
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toastr-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        toastr.error(msg);
    },
    __callSwalFireReLoad:function(confirm_msg, status, msg){
        Swal.fire(
            {
                text: msg,
                icon: status,
                buttonsStyling: !1,
                confirmButtonText: confirm_msg,
                customClass: { confirmButton: "btn btn-primary" }
            }).then(
            function (t) {
                location.reload();
            }
        );
    },
    disablePostTypeDetails:function(){
        let buttons = document.getElementsByClassName('disablePostType');
        buttons.forEach((button) => {
            button.addEventListener('click', (e) => {
                JsPressFunc.resetValidation();
                let inputs = e.currentTarget.closest('.post_type_container').getElementsByTagName('input');
                inputs.forEach((input) => {
                    if(e.currentTarget.checked && input !== e.currentTarget){
                        input.setAttribute('disabled', '');
                        input.removeAttribute('required', '');
                    }else{
                        input.removeAttribute('disabled', '');
                        if(input !== e.currentTarget){
                            input.setAttribute('required', '');
                        }
                    }
                });
            });
        });

    },
    renderPostTable:function(){
        const columns = this.setColumns();
        const key = document.getElementById('post_table').getAttribute('data-type');
        const table = new DataTable('#post_table', {
            ajax: {
                url: api_url + '/posts/table',
                type: 'POST',
                data: {
                    'post-type': key,
                    '_token': token
                }
            },
            processing: true,
            serverSide: true,
            columns: columns,
            select: {
                style: 'multi',
                selector: 'td:first-child input[type="checkbox"]',
                className: 'row-selected'
            },
            language:trans.datatable
        });
        this.toggleTableColumns(table);
        setTimeout(function(){
            KTMenu.createInstances();
        },500);
    },
    setColumns:function(){
        const inputs = document.getElementById('table_columns').getElementsByTagName('input');
        var columns_data = [];
        inputs.forEach((input) => {
            columns_data.push({
                data:input.getAttribute('data-key'),
                name:input.getAttribute('data-title'),
                title:input.getAttribute('data-title'),
                type:input.getAttribute('data-type'),
                width:input.getAttribute('data-width'),
                className:input.getAttribute('data-class'),
                visible:input.checked
            });
        });
        return columns_data;
    },
    toggleTableColumns:function(table){
        const inputs = document.getElementById('table_columns').getElementsByTagName('input');
        inputs.forEach((input, index) => {
            input.addEventListener('change', function(e){
                e.preventDefault();
                var status = !!this.checked;
                table.column(index).visible(status);
            });
        });
    },
    __callJsEditor:function(){
        const editors = document.querySelectorAll('.js_editor');
        editors.forEach(editor => {
            CKEDITOR.replace(editor, {
                height:450,
                versionCheck: false,
                skin: window.theme === 'dark' ? 'monodark':'moono-lisa',
                language: window.lang,
                filebrowserBrowseUrl: '/js-admin/file-manager',
                filebrowserImageBrowseUrl: '/js-admin/file-manager?type=image&max_file=1&action=ckfinder',
                filebrowserUploadUrl: '/js-admin/file-manager/upload',
                entities: false,
                entities_latin: false,
                pasteFromWordPromptCleanup: true,
                pasteFromWordRemoveFontStyles: true,
                forcePasteAsPlainText: true,
                ignoreEmptyParagraph: true,
                toolbar: [
                    {
                        name: 'clipboard',
                        groups: ['clipboard', 'undo'],
                        items: ['PasteFromWord', '-', 'Undo', 'Redo']
                    },
                    {
                        name: 'editing',
                        groups: ['find', 'selection', 'spellchecker'],
                        items: ['Find', 'Replace']
                    },
                    //  { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
                    {
                        name: 'paragraph',
                        groups: ['list', 'indent', 'blocks', 'align', 'bidi'],
                        items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'textindent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock']
                    },
                    {name: 'insert', items: ['Image', 'Table', 'bol', 'SpecialChar', 'Iframe']},
                    {name: 'links', items: ['Link', 'Unlink']},
                    '/',
                    {
                        name: 'basicstyles',
                        groups: ['basicstyles', 'cleanup'],
                        items: ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript', '-', 'RemoveFormat']
                    },
                    {name: 'styles', items: ['Styles', 'Format', 'FontSize']},
                    {name: 'colors', items: ['TextColor', 'BGColor']},
                    {name: 'tools', items: ['Maximize', 'ShowBlocks']},
                    {name: 'document', groups: ['mode', 'document', 'doctools'], items: ['Source', 'kopyala']}

                ]
            });
        });
    },
    __callFileManager:function(page = 1, term = null, data = []){
        JsPressFunc.loadMediaViaFetch(page, term, data).then((data) => {
            if(data.files){
                JsPressFunc.createFileManagerItem(data.files);
            }else{
                document.getElementById('fileManagerContainer').innerHTML = "<div class='d-flex justify-content-center align-items-center'><span class='fs-2 fw-bold'>"+trans.not_found_result+".</span></div>"
            }
            if(data.links !== ""){
                JsPressFunc.renderPaginationforFileManager(data.links);
            }
        });
    },
    renderPaginationforFileManager:function(data){
        const pagination = document.getElementById('pagination');
        pagination.insertAdjacentHTML('afterbegin', data);
        this.setPaginationFileManager();
    },
    setPaginationFileManager:function(){
        const paginator = document.getElementById('paginator');
        const paginations = paginator.querySelectorAll('a');
        paginations.forEach(pagination => {
            pagination.addEventListener('click', function(e){
                e.preventDefault();
                let page = this.getAttribute('data-page');
                JsPressFunc.__reCallFileManager(page);
            });
        });
    },
    createFileManagerItem:function(files){

        const itemContainer = document.getElementById('fileManagerContainer');
        const containerClasses = ['col-lg-2', 'col-md-3', 'col-4', 'img_item'];
        const labelClasses = ['position-relative', 'float-left', 'w-100', 'border', 'rounded', 'p-2', 'cursor-pointer'];
        const labelDivClasses = ['images', 'position-relative'];
        const labelDivContainerClasses = ['img', 'w-100'];
        const imgClasses = ['w-100', 'rounded'];
        const checkboxDivClasses = ['form', 'position-absolute'];
        const checkboxSubDivClasses = ['form-check', 'form-check-custom', 'form-check-solid'];
        var selected_files = [];
        for(var i in files){
            let container = document.createElement('div');
            let label = document.createElement('label');
            let labelDiv = document.createElement('div');
            let labelDivContainer = document.createElement('div');
            let img = this.createImageElement(files[i], false);
            let checkboxDiv = document.createElement('div');
            let checkboxSubDiv = document.createElement('div');
            let input = document.createElement('input');
            let footerDiv = document.createElement('div');
            let footerIcon = document.createElement('div');
            let editButton = document.createElement('button');
            let deleteButton = document.createElement('button');
            let u = document.createElement('u');
            let file = files[i];
            let file_id = file.id;

            container.classList.add(...containerClasses);
            label.classList.add(...labelClasses);
            label.for = 'file_id_'+files[i].id;
            labelDiv.classList.add(...labelDivClasses);
            labelDivContainer.classList.add(...labelDivContainerClasses);
            img.classList.add(...imgClasses);
            checkboxDiv.classList.add(...checkboxDivClasses);
            checkboxSubDiv.classList.add(...checkboxSubDivClasses);
            input.classList.add('form-check-input');
            input.type = 'checkbox';
            input.value = files[i].id;
            input.id = 'file_id_'+files[i].id;
            input.addEventListener('change', function(e){
                let checkedCount = JsPressFunc.getFileManagerCheckedCount();
                if(!this.checked){
                    this.checked = false;
                    selected_files = selected_files.filter(item => {
                        return item.id !== file_id;
                    });
                }else{
                    if( checkedCount > window._max_file ){
                        Swal.fire(
                            {
                                text: window._max_file_error,
                                icon: 'error',
                                buttonsStyling: !1,
                                confirmButtonText: window._confirm_button,
                                customClass: { confirmButton: "btn btn-primary" }
                            });
                        this.checked = false;
                    }else{
                        this.checked = true;
                        selected_files.push(file);
                    }
                }
                selectedFiles.selected_files = selected_files;
            });
            footerDiv.classList.add(...['d-flex']);
            u.classList.add('w-75');
            u.textContent = files[i].upload_name;
            footerIcon.classList.add(...['w-25', 'd-flex', 'justify-content-end']);
            editButton.classList.add(...['btn', 'btn-icon', 'btn-sm', 'me-2', 'mt-1']);
            deleteButton.classList.add(...['btn', 'btn-icon', 'btn-sm', 'mt-1']);
            editButton.insertAdjacentHTML('afterbegin', '<i class="fas fa-info-circle fs-6 text-warning"></i>');
            deleteButton.insertAdjacentHTML('afterbegin', '<i class="far fa-trash-alt fs-6 text-danger"></i>');

            editButton.addEventListener('click', function(e){
                e.preventDefault();
                JsPressFunc.getMediaDetail(file_id);
            });

            JsPressFunc.deleteMedia(deleteButton, file_id, null);

            footerIcon.appendChild(editButton);
            footerIcon.appendChild(deleteButton);
            footerDiv.appendChild(u);
            footerDiv.appendChild(footerIcon);
            labelDivContainer.appendChild(img);
            labelDiv.appendChild(labelDivContainer);
            if(window._type !== 'all' && window._type === files[i].type){
                checkboxSubDiv.appendChild(input);
                checkboxDiv.appendChild(checkboxSubDiv);
                checkboxDiv.style.top = '5px';
                checkboxDiv.style.right = '5px';
                labelDiv.appendChild(checkboxDiv);
            }
            label.append(labelDiv);
            container.appendChild(label);
            container.appendChild(footerDiv);
            itemContainer.appendChild(container);

        }
    },
    getFileManagerCheckedCount:function(){
        var count = 0;
        let allInputs = document.querySelectorAll('.form-check-input');
        allInputs.forEach(el => {
            if(el.checked){
                count++;
            }
        });
        return count;
    },
    __reCallFileManager:function(page, filter = null){
        var fileManagerContainer = document.getElementById('fileManagerContainer');
        document.getElementById('pagination').innerHTML = "";
        let loading = this.createLoading(null, true);
        fileManagerContainer.innerHTML = "";
        fileManagerContainer.appendChild(loading);
        var data = window._data;
        if(filter !== null){
            data.filter = filter;
        }
        setTimeout(function(){
            JsPressFunc.__callFileManager(page, null, data);
            loading.remove();
        },500);
    },
    searchFileManager:function(){
        let key = document.querySelector('[name="key"]').value;
        let file_type = document.querySelector('[name="file_type"]').value;
        let order = document.querySelector('[name="order"]').value;
        let start_date = document.querySelector('[name="start_date"]').value;
        let end_date = document.querySelector('[name="end_date"]').value;
        var filter = {
            key:key,
            file_type: file_type,
            order: order,
            start_date: start_date,
            end_date: end_date,
        };
        this.__reCallFileManager(1, filter);
    },
    resetFileManagerFilter:function(){
        let key = document.querySelector('[name="key"]').value = "";
        let file_type = document.querySelector('[name="file_type"]').value = "";
        let order = document.querySelector('[name="order"]').value = "created_at-desc";
        let start_date = document.querySelector('[name="start_date"]').value = "";
        let end_date = document.querySelector('[name="end_date"]').value = "";
        var filter = {
            key:"",
            file_type: "",
            order: 'created_at-desc',
            start_date: "",
            end_date: "",
        };
        this.__reCallFileManager(1, filter);
    },
    applySelectedFile:function(){
        if(window._action === 'gallery_file_upload'){
            window.opener["uploadGalleryFiles"](window._source, selectedFiles.selected_files);
        }else if(window._action === 'file_upload'){
            window.opener["uploadFiles"](window._source, selectedFiles.selected_files);
        }else{
            selectedFiles.selected_files.forEach(file => {
                if(window._action === 'ckfinder'){
                    window.opener.CKEDITOR.tools.callFunction( window._ckeditorFunc, file.original_url );
                }
                if(window._action === 'single_file_upload' || window._action === 'repeater_file_upload' ){
                    window.opener["handleFileManagerSelector"](window._source, file);
                }
                if(window._action === 'dropzone_image'){
                    window.opener["dropzoneImageUploadCallback"](window._source, file);
                }
            });
        }
        window.close();
    },
    videFileUpload:function($this){
        $this.parentNode.querySelector('a').classList.add('d-none');
        $this.parentNode.querySelector('.loadingVideoAnimation').classList.remove('d-none');

        JsPressFunc.uploadFile($this.files[0], 'media_library').then(function(res){
            $this.parentNode.querySelector('.loadingVideoAnimation').classList.add('d-none');
            if(res.status === 200){
                if(res.data.status){
                    $this.parentNode.querySelector('input[type="hidden"]').value = res.data.data.id;
                    const divClassList = ['w-100', 'mh-200px', 'uploadedVideoField'];
                    const divElement = document.createElement('div');
                    const videoElement = document.createElement('video');
                    const sourceElement = document.createElement('source');
                    const btnDivElement = document.createElement('div');
                    const btnElement = document.createElement('button');
                    btnDivElement.classList.add(...['w-100', 'd-flex', 'flex-center', 'mt-1']);
                    btnElement.classList.add(...['btn', 'btn-light-danger', 'me-2', 'mt-1']);
                    btnElement.innerText = 'Videoyu Kaldır';
                    btnElement.type = 'button';
                    btnDivElement.appendChild(btnElement);
                    divElement.classList.add(...divClassList);
                    videoElement.setAttribute("controls","controls");
                    sourceElement.src = res.data.data.original_url;
                    sourceElement.type = res.data.data.mime;
                    videoElement.appendChild(sourceElement);
                    divElement.appendChild(videoElement);
                    divElement.appendChild(btnDivElement);
                    btnElement.addEventListener('click', function(){
                        $this.parentNode.querySelector('input[type="hidden"]').value = null;
                        $this.value = null;
                        $this.parentNode.querySelector('a').classList.remove('d-none');
                        divElement.remove();
                    });
                    $this.parentNode.appendChild(divElement);

                }else{
                    $this.parentNode.querySelector('a').classList.remove('d-none');
                    Swal.fire({
                        text: res.data.msg,
                        icon: 'error',
                        buttonsStyling: !1,
                        confirmButtonText: trans['confirm'],
                        customClass: { confirmButton: "btn btn-primary" }
                    });
                }

            }else{
                $this.parentNode.querySelector('a').classList.remove('d-none');
                $this.parentNode.querySelector('.loadingVideoAnimation').classList.add('d-none');
                if(res.status === 413){
                    Swal.fire({
                        text: 'Dosya boyutunu izin verilen dosya boyutundan büyüktür!',
                        icon: 'error',
                        buttonsStyling: !1,
                        confirmButtonText: trans['confirm'],
                        customClass: { confirmButton: "btn btn-primary" }
                    });
                }
                if(res.status === 422){
                    Swal.fire({
                        text: 'Desteklenmeyen dosya tipi!',
                        icon: 'error',
                        buttonsStyling: !1,
                        confirmButtonText: trans['confirm'],
                        customClass: { confirmButton: "btn btn-primary" }
                    });
                }
            }
        });
    },
    deleteVideoFile:function(el){
        let $this = document.getElementById(el);
        $this.value = null;
        $this.parentNode.querySelector('input[type="hidden"]').value = null;
        $this.parentNode.querySelector('a').classList.remove('d-none');
        $this.parentNode.querySelector('.uploadedVideoField').remove();

    },
    storePost:function(){
        let form = document.getElementById('create_post_form');
        form.addEventListener('submit', function(e){
            e.preventDefault();
            var formData = new FormData(form);
            return false;
        });
    },
    fileUploadButtonAction:function(name, type, max_file, action, lang, element = null){
        const url = new URL(api_url+'/file-manager');
        var source = name;
        url.searchParams.append("type", type);
        url.searchParams.append("max_file", max_file);
        url.searchParams.append("action", action);
        url.searchParams.append("langCode", lang);
        if(action === 'repeater_file_upload'){
            source = element.closest('.file_upload_wrapper').querySelector('input').getAttribute('name');
        }
        url.searchParams.append("source", source);
        var left = (screen.width - 1210) / 2;
        var top = (screen.height - 900) / 4;

        let filemanagerwindow = window.open(url.href, 'filemanager', 'width=1210,height=755,top='+top+',left='+left);
        if (window.focus) {filemanagerwindow.focus()}
        return false;
    },
    completePostSeo:function(post_type){
        let title = document.getElementsByName('title')[0];
        let googleTitle = document.querySelector('h2.google');
        let metaTitle = document.getElementById('meta_title');
        let googleMetaDescription = document.getElementById('meta_description');
        let googleMetaDescriptionText = document.querySelector('.google-description');
        if(metaTitle){
            title.addEventListener('change', function(e){
                var value = title.value;
                let googleUrl = document.querySelector('.google-seo-sub-url');
                let pageRootUrl = googleUrl.getAttribute('data-root');
                if(value.length > 0){
                    JsPressFunc.getSlugForPost(value, post_type).then(function(res){
                        googleUrl.textContent = pageRootUrl+''+res.slug;
                        if(metaTitle.value.length < 1){
                            googleTitle.textContent = value.slice(0, 60);
                        }else{
                            googleTitle.textContent = metaTitle.value.slice(0, 60);
                        }
                    });
                }else{
                    googleUrl.textContent = pageRootUrl;
                    if(metaTitle.textContent === ''){
                        googleTitle.textContent = trans.title_is_here;
                    }
                }
            });
        }
        if( CKEDITOR.instances.content ){
            CKEDITOR.instances.content.on('change', function() {
                var p = JsPressFunc.removeHtmlFromString(this.getData());
                if(p.length > 0 && googleMetaDescription.value.length < 1){
                    googleMetaDescriptionText.textContent = p.slice(0, 160).replace(/\xA0/g,' ');
                }else if(googleMetaDescription.value.length > 0){
                    googleMetaDescriptionText.textContent = googleMetaDescription.value.slice(0,160);
                }else{
                    googleMetaDescriptionText.textContent = trans.seo_content_is_here;
                }

            });
        }
        if(metaTitle){
            metaTitle.addEventListener('keyup', function(e){
                var value = e.currentTarget.value;
                if(value.length > 0){
                    googleTitle.textContent = value.slice(0, 60);
                }else if(title.value.length > 0){
                    googleTitle.textContent = title.value.slice(0, 60);
                }else{
                    googleTitle.textContent = trans.title_is_here;
                }
            });
            googleMetaDescription.addEventListener('keyup', function(e){
                var value = e.currentTarget.value;
                var p = JsPressFunc.removeHtmlFromString(CKEDITOR.instances.content.getData());
                if(value.length > 0){
                    googleMetaDescriptionText.textContent = value.slice(0, 160);
                }else if(p.length > 0){
                    googleMetaDescriptionText.textContent = p.slice(0, 160).replace(/\xA0/g,' ');
                }else{
                    googleMetaDescriptionText.textContent = trans.seo_content_is_here;
                }
            })
        }
    },
    getSlugForPost:function(value, post_type){
        return new Promise((resolve, reject) => {
            xhr.onreadystatechange = function() {
                if (xhr.readyState !== 4) {
                    return;
                }
                if (xhr.status === 200) {
                    let response = JSON.parse(xhr.responseText);
                    let res = {
                        status:1,
                        slug:response.slug
                    };
                    resolve(res);
                }else{
                    let res = {
                        status:0
                    };
                    resolve(res);
                }
            };
            xhr.open("POST", api_url+'/posts/check-slug', true);
            xhr.setRequestHeader('X-CSRF-TOKEN', token);
            const data = new FormData();
            data.append("title", value);
            data.append("post-type", post_type);
            xhr.send(data);
        });

    },
    removeHtmlFromString:function(text){
        let div = document.createElement('div');
        div.innerHTML = text;
        let paragraphs = div.getElementsByTagName('p');
        var p_array = [];
        paragraphs.forEach(item => {
            p_array.push(item.innerHTML.replace(/(<p[^>]+?>|<p>|<\/p>)/img, ""));
        });
        return p_array.join(' ');
    },
    removeDropzoneImage:function(el, name){
        console.log(name);
        let dropzone = el.closest('.dropzone-image');
        document.getElementsByName(name)[0].value = "";
        dropzone.querySelector('img').src = "";
        dropzone.querySelector('.dropzone-image-wrapper').classList.add('d-none');
    },
    removeCategoryTranslationRelation:function(element_id, transaction_id, post_type){
        Swal.fire({
            text: trans['delete_translation_relation'],
            icon: "warning",
            showCancelButton: true,
            buttonsStyling: true,
            confirmButtonText: trans['confirm'],
            cancelButtonText: trans["no_return"],
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-active-light"
            },
        }).then(
            function (t) {
                if( t.isConfirmed ){
                    const data = new FormData();
                    data.append('element_id', element_id);
                    data.append('post-type', post_type);
                    data.append('transaction_id', transaction_id);
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState !== 4) {
                            return;
                        }
                        if (xhr.status === 200) {
                            location.reload();
                        }else{
                            console.log('error');
                        }
                    };
                    xhr.open("POST", api_url+'/categories/delete-translation', true);
                    xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    xhr.send(data);
                }
            }
        );
    },
    destroyPost:function(id, post_type){
        Swal.fire({
            text: trans['delete_post_warning'],
            icon: "warning",
            showCancelButton: true,
            buttonsStyling: true,
            confirmButtonText: trans['confirm'],
            cancelButtonText: trans["no_return"],
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-active-light"
            },
        }).then(
            function (t) {
                if( t.isConfirmed ){
                    const data = new FormData();
                    data.append('post-type', post_type);
                    data.append('id', id);
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState !== 4) {
                            return;
                        }
                        if (xhr.status === 200) {
                            let response = JSON.parse(xhr.responseText);
                            if(response.status === 1){
                                Swal.fire({
                                    text: response.msg,
                                    icon: "success",
                                    showCancelButton: false,
                                    buttonsStyling: true,
                                    confirmButtonText: trans['ok'],
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    },
                                }).then(function(t){
                                    location.reload();
                                });
                            }else{
                                Swal.fire({
                                    text: response.msg,
                                    icon: "error",
                                    showCancelButton: false,
                                    buttonsStyling: true,
                                    confirmButtonText: trans['ok'],
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    },
                                });
                            }
                        }else{
                            console.log('error');
                        }
                    };
                    xhr.open("POST", api_url+'/posts/destroy', true);
                    xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    xhr.send(data);
                }
            }
        );
    },
    deletePost:function(id, post_type, lang){
        Swal.fire({
            text: trans['force_delete_post_warning'],
            icon: "warning",
            showCancelButton: true,
            buttonsStyling: true,
            confirmButtonText: trans['confirm'],
            cancelButtonText: trans["no_return"],
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-active-light"
            },
        }).then(
            function (t) {
                if( t.isConfirmed ){
                    const data = new FormData();
                    data.append('post-type', post_type);
                    data.append('id', id);
                    data.append('lang', lang);
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState !== 4) {
                            return;
                        }
                        if (xhr.status === 200) {
                            let response = JSON.parse(xhr.responseText);
                            if(response.status === 1){
                                Swal.fire({
                                    text: response.msg,
                                    icon: "success",
                                    showCancelButton: false,
                                    buttonsStyling: true,
                                    confirmButtonText: trans['ok'],
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    },
                                }).then(function(t){
                                    location.reload();
                                });
                            }else{
                                Swal.fire({
                                    text: response.msg,
                                    icon: "error",
                                    showCancelButton: false,
                                    buttonsStyling: true,
                                    confirmButtonText: trans['ok'],
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    },
                                });
                            }
                        }else{
                            console.log('error');
                        }
                    };
                    xhr.open("POST", api_url+'/posts/delete', true);
                    xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    xhr.send(data);
                }
            }
        );
    },
    deleteCategory:function(id, post_type, lang){
        Swal.fire({
            text: trans['force_delete_category_warning'],
            icon: "warning",
            showCancelButton: true,
            buttonsStyling: true,
            confirmButtonText: trans['confirm'],
            cancelButtonText: trans["no_return"],
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-active-light"
            },
        }).then(
            function (t) {
                if( t.isConfirmed ){
                    const data = new FormData();
                    data.append('post-type', post_type);
                    data.append('id', id);
                    data.append('lang', lang);
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState !== 4) {
                            return;
                        }
                        if (xhr.status === 200) {
                            let response = JSON.parse(xhr.responseText);
                            if(response.status === 1){
                                Swal.fire({
                                    text: response.msg,
                                    icon: "success",
                                    showCancelButton: false,
                                    buttonsStyling: true,
                                    confirmButtonText: trans['ok'],
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    },
                                }).then(function(t){
                                    location.reload();
                                });
                            }else{
                                Swal.fire({
                                    text: response.msg,
                                    icon: "error",
                                    showCancelButton: false,
                                    buttonsStyling: true,
                                    confirmButtonText: trans['ok'],
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    },
                                });
                            }
                        }else{
                            console.log('error');
                        }
                    };
                    xhr.open("POST", api_url+'/categories/destroy', true);
                    xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    xhr.send(data);
                }
            }
        );
    },
    restorePost:function(id, post_type){
        Swal.fire({
            text: trans['restore_post_warning'],
            icon: "warning",
            showCancelButton: true,
            buttonsStyling: true,
            confirmButtonText: trans['confirm'],
            cancelButtonText: trans["no_return"],
            customClass: {
                confirmButton: "btn btn-primary",
                cancelButton: "btn btn-active-light"
            },
        }).then(
            function (t) {
                if( t.isConfirmed ){
                    const data = new FormData();
                    data.append('post-type', post_type);
                    data.append('id', id);
                    xhr.onreadystatechange = function() {
                        if (xhr.readyState !== 4) {
                            return;
                        }
                        if (xhr.status === 200) {
                            let response = JSON.parse(xhr.responseText);
                            if(response.status === 1){
                                Swal.fire({
                                    text: response.msg,
                                    icon: "success",
                                    showCancelButton: false,
                                    buttonsStyling: true,
                                    confirmButtonText: trans['ok'],
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    },
                                }).then(function(t){
                                    location.reload();
                                });
                            }else{
                                Swal.fire({
                                    text: response.msg,
                                    icon: "error",
                                    showCancelButton: false,
                                    buttonsStyling: true,
                                    confirmButtonText: trans['ok'],
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    },
                                });
                            }
                        }else{
                            console.log('error');
                        }
                    };
                    xhr.open("POST", api_url+'/posts/restore', true);
                    xhr.setRequestHeader('X-CSRF-TOKEN', token);
                    xhr.send(data);
                }
            }
        );
    },
    changePostOrder:function(post_type){
        let value = document.querySelector('input[name="post_order"]:checked').value;
        const data = new FormData();
        data.append('order', value);
        data.append('post_type', post_type);
        xhr.onreadystatechange = function() {
            if (xhr.readyState !== 4) {
                return;
            }
            if (xhr.status === 200) {
                location.reload();
            }else{
                console.log('error');
            }
        };
        xhr.open("POST", api_url+'/change-post-order', true);
        xhr.setRequestHeader('X-CSRF-TOKEN', token);
        xhr.send(data);
    },
    setDatepickers:function(){
        $('.jspress-datepicker').each(function(item){
            let format =  $(this).attr('data-format') ?? 'YYYY-MM-DD';
            let options = {
                singleDatePicker:true,
                autoUpdateInput:false,
                showDropdowns:$(this).attr('data-dropdown') ?? true,
                minDate:$(this).attr('data-mindate') ?? 1,
                maxDate:$(this).attr('data-maxdate') ?? 1,
                autoApply:$(this).attr('data-autoapply') ?? true,
                timePicker:($(this).attr('data-timepicker') === 'true') ?? false,
                timePicker24Hour:true,
                timePickerSeconds:true,
                timePickerIncrement:true,
                locale:{
                    format: format,
                    daysOfWeek:trans.days,
                    monthNames:trans.months,
                    applyLabel: trans.apply,
                    cancelLabel: trans.cancel,
                    firstDay:0
                }
            };
            $(this).daterangepicker(options);
            $(this).on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format(format));
            });
        });
    },
    initPopupSettings:function(){
        document.querySelectorAll('.typePopup').forEach(function(el){
            el.addEventListener('change', function(e){
                JsPressFunc.__resetPopupTypePlaces();
                let type = e.target.value;
                document.querySelectorAll('[data-type="'+type+'"]').forEach((el) => {
                    el.classList.remove('d-none');
                });
                document.querySelectorAll('.popupDesign').forEach((el) => {
                    var types = JSON.parse(el.getAttribute('data-types'));
                    if( types.includes( type ) ){
                        el.classList.remove('d-none');
                    }
                });
                let button_type = document.querySelector('input[name="button_type"]:checked').value;
                if(parseInt(button_type) === 0){
                    document.querySelector('.dependsPopupTypeBar').classList.remove('d-none');
                }
            });
        });

        document.querySelectorAll('.pageShowType').forEach(function(el){
            el.addEventListener('change', function(e){
                JsPressFunc.__resetPopupPostShow();
                let value = parseInt(e.target.value);
                if(value === 1){
                    document.querySelectorAll('[data-type="show_posts"]').forEach((t)=>{
                        t.classList.remove('d-none')
                    });
                }
                if(value === 3){
                    document.querySelector('[data-type="custom_url"]').classList.remove('d-none');
                }
            });
        });

        document.querySelectorAll('.impressionInput').forEach(function(el){
            el.addEventListener('change', function(e){
                var value = parseInt(e.target.value);
                document.querySelector('.dependsPopupImression').classList.add('d-none');
                if(value === 1 || value === 2){
                    document.querySelector('.dependsPopupImression').classList.remove('d-none');
                }
            });
        });

        document.querySelectorAll('.dateTypeInput').forEach(function(el){
            el.addEventListener('change', function(e){
                var value = parseInt(e.target.value);
                document.querySelector('.dependsPopupDateType').classList.add('d-none');
                if(value === 1){
                    document.querySelector('.dependsPopupDateType').classList.remove('d-none');
                }
            });
        });

        document.querySelectorAll('.timeTypeInput').forEach(function(el){
            el.addEventListener('change', function(e){
                var value = parseInt(e.target.value);
                document.querySelector('.dependsPopupTimeType').classList.add('d-none');
                if(value === 1){
                    document.querySelector('.dependsPopupTimeType').classList.remove('d-none');
                }
            });
        });

        document.querySelectorAll('.conditionInput').forEach(function(el){
            el.addEventListener('change', function(e){
                var value = parseInt(e.target.value);
                document.querySelectorAll('.dependsPopupConditions').forEach((t)=>{
                    t.classList.add('d-none');
                });
                if(value === 0){
                    document.querySelector('.dependsPopupConditions[data-type="condition_time"]').classList.remove('d-none');
                }
                if(value === 1){
                    document.querySelector('.dependsPopupConditions[data-type="condition_scroll"]').classList.remove('d-none');
                }
            });
        });

        document.querySelectorAll('input[name="button_type"]').forEach(function(el){
            el.addEventListener('change', function(e){
                if(parseInt(e.target.value) === 0){
                    document.querySelector('.dependsPopupTypeBar').classList.remove('d-none');
                }else{
                    document.querySelector('.dependsPopupTypeBar').classList.add('d-none');
                }
            });
        });
    },
    __resetPopupTypePlaces:function(){
        document.querySelectorAll('.dependsPopupType').forEach(function(el){
            el.classList.add('d-none');
        });
        document.querySelectorAll('.popupDesign').forEach((el) => {
            el.classList.add('d-none');
        });
        document.querySelectorAll('input[name="place"]').forEach((el) => {
            el.checked = false;
        });
    },
    __resetPopupPostShow:function(){
        document.querySelectorAll('.dependsPopupPageShow').forEach((t)=>{
            t.classList.add('d-none');
        });
    },
    __resetPopupErrors:function(){
        document.querySelectorAll('div.error').forEach((t)=>{
            t.remove();
        });
    },
    __setupPopupErrors:function(errors){
        for(const [key, value] of Object.entries(errors)){
            var el = document.querySelector('[name="' + key + '"]');
            if(el === null){
                el = document.querySelector('[name="' + key + '[]"]');
            }
            let errorEl = document.createElement('div');
            errorEl.classList.add('error');
            errorEl.classList.add('mt-2');
            errorEl.textContent = value[0];
            if(el.parentElement.classList.contains('form-check')){
                el.parentNode.parentNode.parentNode.appendChild(errorEl);
            }else{
                el.parentNode.appendChild(errorEl);
            }
        }
    },
    storePopupForm:function() {
        document.getElementById('create_popup_form').addEventListener('submit', function (e) {
            e.preventDefault();
            JsPressFunc.__resetPopupErrors();
            const data = new FormData(e.target);
            if(CKEDITOR.instances.content){
                data.append('content', CKEDITOR.instances.content.getData());
            }
            JsPressFunc.sendPopupForm(data).then(r => {
                if(r.data.status === 0){
                    JsPressFunc.__setupPopupErrors(r.data.errors);
                    Swal.fire({
                        text: r.data.msg,
                        icon: 'error',
                        buttonsStyling: !1,
                        confirmButtonText: trans.confirm_button,
                        customClass: { confirmButton: "btn btn-primary" }
                    });
                }else{
                    Swal.fire({
                        text: r.data.msg,
                        icon: 'success',
                        buttonsStyling: !1,
                        confirmButtonText: trans.confirm_button,
                        customClass: { confirmButton: "btn btn-primary" }
                    }).then(function(){
                        window.location.href = r.data.route;
                    });

                }
            });
            return false;
        });
    },
    sendPopupForm:function(data) {
        return new Promise((resolve, reject) => {
            xhr.onreadystatechange = function() {
                if (xhr.readyState !== 4) {
                    return;
                }
                if (xhr.status === 200) {
                    let response_data = {
                        'status' :  xhr.status,
                        'data' : JSON.parse(xhr.responseText)
                    }
                    resolve(response_data);
                }else{
                    let response_data = {
                        'status' :  xhr.status
                    }
                    resolve(response_data);
                }
            };
            xhr.open("POST", api_url+"/popup/store", true);
            xhr.setRequestHeader('X-CSRF-TOKEN', token);
            xhr.send(data);
        });
    }
}
$(document).ready(function () {
    JsPressFunc.init();
});
var handleFileManagerSelector = function(source, file){
    let elements = document.getElementsByName(source);
    let el = elements[0];
    el.value = file.id;
    const img = document.createElement('img');
    const imgWrapper = el.closest('.file_upload_wrapper').querySelector('.single_file_image');
    imgWrapper.innerHTML = "";
    img.src = file.original_url;
    img.tagName = file.details.hasOwnProperty(window.lang) ? file.details[window.lang].title : file.file_name;
    el.closest('.file_upload_wrapper').querySelector('.single_file_image').appendChild(img);
}
var dropzoneImageUploadCallback = function(source, file){
    let elements = document.getElementsByName(source);
    let el = elements[0];
    el.value = file.id;
    let dropzone = el.closest('.dropzone-image');
    let image = dropzone.querySelector('img');
    image.src = file.original_url;
    dropzone.querySelector('.dropzone-image-wrapper').classList.remove('d-none');
}

var uploadGalleryFiles = function(source, files){
    var gallery_file_ids = [];
    var gallery_images = [];
    for(var i = 0; i < files.length; i++){
        gallery_file_ids.push(files[i].id);
        gallery_images.push({
            id:files[i].id,
            file:files[i].dimensions.thumbnail.url
        });
    }
    let elements = document.getElementsByName(source);
    let el = elements[0];
    if( el.value !== "" && el.value != null ){
        var current_element_ids = JSON.parse(el.value);
        gallery_file_ids = current_element_ids.concat(gallery_file_ids);
    }
    el.value = JSON.stringify(gallery_file_ids);
    let galleryContainer = el.closest('.file_upload_wrapper').querySelector('.gallery_file_images');
    for(var f = 0; f < gallery_images.length; f++){
        let div = document.createElement('div');
        let deleteIconDiv = document.createElement('button');
        let deleteIcon = document.createElement('i');
        let image = document.createElement('img');
        let dragDiv = document.createElement('div');
        dragDiv.classList.add('draggable');
        div.classList.add(...['rounded-1', 'border', 'me-2', 'position-relative', 'galleryImageContainer']);
        deleteIconDiv.classList.add(...['btn', 'btn-icon', 'btn-sm', 'btn-danger', 'deleteGalleryImage']);
        deleteIconDiv.type = 'button';
        deleteIcon.classList.add(...['fas','fa-trash']);
        deleteIconDiv.appendChild(deleteIcon);
        image.src = gallery_images[f].file;
        image.classList.add( 'draggable-handle');
        image.setAttribute('data-id', gallery_images[f].id);
        div.setAttribute('data-id', gallery_images[f].id);
        div.appendChild(image);
        div.appendChild(deleteIconDiv);
        dragDiv.appendChild(div);
        dragDiv.setAttribute('data-id', gallery_images[f].id);
        galleryContainer.appendChild(dragDiv);
        deleteIconDiv.addEventListener('click', function(e){
            e.preventDefault();
            var divContainer = e.target.closest('div');
            var id = parseInt(divContainer.querySelector('img').getAttribute('data-id'));
            var input = e.target.closest('.file_upload_wrapper').querySelector('input');
            var values = JSON.parse(input.value);
            values = values.filter((item)=>{
                return item !== id;
            });
            if(values.length === 0){
                input.value = "";
            }else{
                input.value = JSON.stringify(values);
            }
            dragDiv.remove();
        });
    }
}
var uploadFiles = function(source, files){
    var gallery_file_ids = [];
    var gallery_images = [];
    for(var i = 0; i < files.length; i++){
        gallery_file_ids.push(files[i].id);
        gallery_images.push({
            id:files[i].id,
            file:files[i].original_url,
            type:files[i].type,
            mime:files[i].mime,
            name:files[i].upload_name
        });
    }
    let elements = document.getElementsByName(source);
    let el = elements[0];
    if( el.value !== "" && el.value != null ){
        var current_element_ids = JSON.parse(el.value);
        gallery_file_ids = current_element_ids.concat(gallery_file_ids);
    }
    el.value = JSON.stringify(gallery_file_ids);
    let galleryContainer = el.closest('.file_upload_wrapper').querySelector('.gallery_file_images');
    for(var f = 0; f < gallery_images.length; f++){
        let div = document.createElement('div');
        let deleteIconDiv = document.createElement('button');
        let deleteIcon = document.createElement('i');
        let image = document.createElement('img');
        let dragDiv = document.createElement('div');
        let span = document.createElement('span');
        dragDiv.classList.add('draggable');
        div.classList.add(...['rounded-1', 'border', 'me-2', 'position-relative', 'galleryImageContainer']);
        deleteIconDiv.classList.add(...['btn', 'btn-icon', 'btn-sm', 'btn-danger', 'deleteGalleryImage']);
        deleteIconDiv.type = 'button';
        deleteIcon.classList.add(...['fas','fa-trash']);
        deleteIconDiv.appendChild(deleteIcon);
        image.classList.add( 'draggable-handle');
        image.setAttribute('data-id', gallery_images[f].id);
        image.src = JsPressFunc.getImageSrc(gallery_images[f]);
        div.setAttribute('data-id', gallery_images[f].id);
        div.appendChild(image);
        div.appendChild(deleteIconDiv);
        span.classList.add(...['d-block', 'fileNameSpan']);
        span.textContent = gallery_images[f].name;
        dragDiv.appendChild(div);
        dragDiv.appendChild(span);
        dragDiv.setAttribute('data-id', gallery_images[f].id);
        galleryContainer.appendChild(dragDiv);
        deleteIconDiv.addEventListener('click', function(e){
            e.preventDefault();
            var divContainer = e.target.closest('div');
            var id = parseInt(divContainer.querySelector('img').getAttribute('data-id'));
            var input = e.target.closest('.file_upload_wrapper').querySelector('input');
            var values = JSON.parse(input.value);
            values = values.filter((item)=>{
                return item !== id;
            });
            if(values.length === 0){
                input.value = "";
            }else{
                input.value = JSON.stringify(values);
            }
            dragDiv.remove();
        });
    }
}
