/*
 * Copyright 2024 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: popup.js
 * Author: Json Yazılım
 * Class: popup.js
 * Current Username: Erdinc
 * Last Modified: 16.04.2024 15:09
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

(function (root, factory) {
    if (root === undefined && window !== undefined) root = window;
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return (root['JsPopup'] = factory());
        });
    } else if (typeof module === 'object' && module.exports) {
        module.exports = factory();
    } else {
        root['JsPopup'] = factory();
    }
}(this, function () {

    "use strict";

    {

        var is_active = true;

        var options = {
            unique_id:1,
            transition: 'fade',
            type:'lightbox',
            place:'left',
            design:{
                border_color: "#000000",
                border_radius: 0,
                border_size:5,
                button_background_color:"#0b1f8f",
                button_color:"#ffffff",
                button_font_size: "12",
                button_position :"right",
                color : "#000000",
                content_animation:"fadeIn",
                content_font_size : "17",
                counter_font_size:"12",
                dialog_background_color:"#ffffff",
                padding:15,
                link_color:'#0b1f8f',
                is_backdrop:1,
                title_color:'#000',
                title_animation:'fadeIn',
                title_font_size:20,
                title_space:10,
            },
            display_title:0,
            is_content_image:0,
            content:'',
            time_type:0,
            display_time:60,
            impressions:0,
            impression_count:3,
            button_type:1,
            button_text:'Kabul Ediyorum',
            screen_timer_text:'saniye sonra sona erecek'
        }

        var JsPopup = function JsPopup() {
            var params = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
            this.options = Object.assign(options, params);
            this.is_active = is_active;
            this._init();
        };

        var $html;
        JsPopup.prototype = {
            _init: function _init() {
                if(this.options.impressions !== 0){
                    let impression_count = parseInt(this.getImpressionCount(this.options.unique_id));
                    if( impression_count > this.options.impression_count ){
                        console.info('Popup impressions count is not enough');
                        this.is_active = false;
                        return false;
                    }
                    this.setImpressionCount(this.options.unique_id);
                }

                let _this = this, timeout = _this.options.timeout * 1000;
                _this._render(_this.options.content)._setupEvents();

            },
            _render: function _render() {
                $html = document.querySelector('html');
                this._renderTemplate();
                document.body.appendChild(this.$el);
                return this;
            },
            _setupEvents: function _setupEvents() {
                var _this = this;

                this.$closeBtn.addEventListener('click', function () {
                    return _this.close();
                });
                window.addEventListener('keydown', function (e) {
                    return _this._onEscPress(e);
                });
            },
            _renderTemplate:function _renderTemplate() {
                var _this = this;
                _this.$el = document.createElement('div');
                _this.$closeBtn = document.createElement('button');
                var pc = document.createElement('div'),
                    pb = document.createElement('div'),
                    pcc = document.createElement('div');
                _this.$el.classList.add('JsPopup');
                pc.classList.add(...['JsPopup--dialog',`JsPopup--${this.options.transition}`, `JsPopup--${this.options.type}`]);
                if(_this.options.type === 'tooltip' || _this.options.type === 'bar'){
                    pc.classList.add('JsPopup--place--'+_this.options.place);
                }
                _this.$closeBtn.classList.add(...['JsPopup-closeBtn']);
                pb.classList.add('JsPopup--body');
                pcc.classList.add(...['JsPopup--content']);

                pb.innerHTML = _this.options.content;
                if(_this.options.is_content_image === 1){
                    pb.innerHTML = pb.querySelector('p').innerHTML;
                }
                if( this.options.display_title === 1 ){
                    var ph = document.createElement('div');
                    var pt = document.createElement('h2');
                    ph.classList.add('JsPopup--header');
                    pt.textContent = _this.options.title;
                    ph.appendChild(pt);
                    ph.appendChild(pb);
                    if(_this.options.type !== 'screen'){
                        ph.appendChild(_this.$closeBtn);
                    }else{
                        pt.style.color = _this.options.design.title_color;
                        ph.classList.add(_this.options.design.title_animation);
                        pt.style.marginBottom = _this.options.design.title_space;
                        pt.style.fontSize = `${_this.options.design.title_font_size}px`;

                    }
                    pcc.appendChild(ph);
                    if(_this.options.type === 'bar' || _this.options.type === 'tooltip'){
                        ph.style.border = 'none';
                    }
                }else{
                    if(_this.options.type !== 'screen'){
                        pb.appendChild(_this.$closeBtn);
                    }
                }
                pcc.appendChild(pb);
                pc.appendChild(pcc);
                _this.$el.appendChild(pc);
                if(_this.options.design.font_data !== null){
                    _this.$el.style.fontFamily = _this.options.design.font_data.family;
                }
                if(_this.options.type === 'lightbox'){
                    pcc.style.border = `${_this.options.design.border_size}px solid ${_this.options.design.border_color}`;
                    pcc.style.borderRadius = `${_this.options.design.border_radius}px`;
                    pb.style.padding = `${_this.options.design.padding}px`;
                    _this.$closeBtn.style.backgroundColor = _this.options.design.button_background_color;
                    _this.$closeBtn.style.color = _this.options.design.button_color;

                }
                if(_this.options.type === 'tooltip'){
                    pcc.style.borderRadius = `${_this.options.design.border_size}px solid ${_this.options.design.border_color}`;
                    _this.$closeBtn.style.backgroundColor = _this.options.design.button_background_color;
                    _this.$closeBtn.style.color = _this.options.design.button_color;
                    _this.$closeBtn.style.border = 'none';
                    pcc.style.backgroundColor = _this.options.design.dialog_background_color;
                    pcc.style.color = _this.options.design.color;
                    if(_this.options.is_content_image === 0){
                        pb.style.paddingLeft = '1rem';
                        pb.style.paddingRight = '1rem';
                    }

                    if(_this.options.design.is_backdrop == 2){
                        _this.$el.style.background = 'none';
                    }
                }
                if(_this.options.type === 'bar'){
                    _this.$closeBtn.style.backgroundColor = _this.options.design.button_background_color;
                    _this.$closeBtn.style.color = _this.options.design.button_color;
                    _this.$closeBtn.style.border = 'none';
                    pcc.style.backgroundColor = _this.options.design.dialog_background_color;
                    pcc.style.color = _this.options.design.color;
                    pb.style.paddingLeft = '1rem';
                    pb.style.paddingRight = '1rem';
                    if(_this.options.button_type === 0){
                        _this.$closeBtn.classList.add('JsPopup-approvedBtn');
                        _this.$closeBtn.textContent = _this.options.button_text;
                        _this.$closeBtn.classList.add('JsPopup--is_approved_btn');
                    }
                }
                if(_this.options.type === 'lightbox' || _this.options.type === 'tooltip' || _this.options.type === 'bar'){
                    pcc.querySelectorAll('a').forEach(function(a){
                        a.style.color = _this.options.design.link_color;
                    })
                }
                if(_this.options.type === 'screen'){
                    pcc.style.position = 'inherit';
                    pb.style.fontSize = `${_this.options.design.content_font_size}px`;
                    pcc.style.color = _this.options.design.color;
                    _this.$closeBtn.style.color = _this.options.design.button_color;
                    pb.classList.add(_this.options.design.content_animation);

                    if( _this.options.time_type === 1 ){
                        _this.$closeBtn.textContent = _this.options.button_text;
                        _this.$closeBtn.style.background = 'none';
                        _this.$closeBtn.style.border = 'none';
                        _this.$closeBtn.style.padding = '0';
                        _this.$closeBtn.style.textAlign = _this.options.design.button_position;
                        _this.$closeBtn.style.fontSize = `${_this.options.design.button_font_size}px`;
                        pcc.appendChild(_this.$closeBtn);
                    }else{
                        _this.$closeBtn.style.background = 'none';
                        _this.$closeBtn.style.border = 'none';
                        pc.appendChild(_this.$closeBtn);
                    }
                    if(_this.options.time_type === 1){
                        let pcbtn = document.createElement('div'),
                            pcsc = document.createElement('span'),
                            pcs = document.createElement('span');
                        pcbtn.classList.add('JsPopup--timer');
                        pcsc.classList.add('JsPopup--timer--span');
                        pcs.setAttribute('data-timer', _this.options.display_time);
                        pcsc.textContent = _this.options.screen_timer_text;
                        pcs.textContent = _this.options.display_time;
                        let counter = _this.options.display_time;
                        setInterval(function(){
                            counter--;
                            pcs.textContent = counter;
                            if(counter === 0){
                                _this.close();
                            }
                        }, 1000);
                        pcsc.insertAdjacentElement('afterbegin', pcs);
                        pcbtn.appendChild(pcsc);
                        pc.appendChild(pcbtn);
                    }
                }
            },
            /**
             * @param event
             */
            _onEscPress: function _onEscPress(event) {
                if (event.keyCode === 27) {
                    this.close();
                }
            },
            close: function close() {
                $html.classList.remove('JsPopup--isOpen');
                $html.classList.remove('JsPopup--overflowScroll');
                this.$el.classList.remove('show');
                if (this.options.onClose && typeof this.options.onClose == 'function') {
                    this.options.onClose(this.$el);
                }
            },
            open: function open() {
                let _this = this;
                if (this.options.onOpen && typeof this.options.onOpen == 'function') {
                    this.options.onOpen(this.$el);
                }

                $html.classList.add('JsPopup--isOpen');
                if(_this.options.type !== 'lightbox' && _this.options.type !== 'screen'){
                    $html.classList.add('JsPopup--overflowScroll');
                }
                setTimeout(function(){
                    _this.$el.classList.add('show');
                    if(_this.options.time_type === 1){
                        let close_duration = _this.options.display_time * 1000;
                        setTimeout(function(){
                            _this.close();
                        },close_duration);
                    }
                }, 500);
            },

            /**
             * @param content
             */
            setContent: function setContent(content) {
                this.options.content = content;
                this.$el.querySelector('.JsPopup-content').innerHTML = this.options.content;
            },

            /**
             * @param active
             */
            setActive: function setContent(active) {
                this.options.is_active = active;
            },

            /**
             * @return {String}
             */
            getContent: function getContent() {
                return this.options.content;
            },
            destroy: function destroy() {
                window.removeEventListener('keydown', this._onEscPress);
                this.$el.parentNode.removeChild(this.$el);
            },
            setImpressionCount:function setImpressionCount(popup_id){
                var impressions_count = this.getImpressionCount(popup_id);
                impressions_count = parseInt(impressions_count) + 1;
                let cname = 'popup_'+popup_id;
                if( this.options.impressions === 1 ){
                    document.cookie = cname + "=" + impressions_count+"; expires=Fri, 31 Dec 9999 23:59:59 GMT;";
                }else{
                    sessionStorage.setItem(cname, impressions_count);
                }

            },
            getImpressionCount: function getImpressionCount(popup_id) {
                let name = 'popup_'+popup_id + "=";
                if( this.options.impressions === 1 ){
                    let decodedCookie = decodeURIComponent(document.cookie);
                    let ca = decodedCookie.split(';');
                    for(let i = 0; i <ca.length; i++) {
                        let c = ca[i];
                        while (c.charAt(0) === ' ') {
                            c = c.substring(1);
                        }
                        if (c.indexOf(name) === 0) {
                            return c.substring(name.length, c.length);
                        }
                    }
                }
                if( this.options.impressions === 2 ){
                    let count = sessionStorage.getItem('popup_'+popup_id);
                    if(count){
                        return count;
                    }
                }
                return 0;
            }
        };
    }

    return JsPopup;

}));
