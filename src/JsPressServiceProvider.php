<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: JsPressServiceProvider.php
 * Author: Json Yazılım
 * Class: JsPressServiceProvider.php
 * Current Username: Erdinc
 * Last Modified: 16.05.2023 17:10
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress;

use Illuminate\Console\Events\CommandFinished;
use Illuminate\Foundation\AliasLoader;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use JsPress\JsPress\Interfaces\AnalysticInterface;
use JsPress\JsPress\Interfaces\CategoryAdminInterface;
use JsPress\JsPress\Interfaces\CategoryInterface;
use JsPress\JsPress\Interfaces\MenuInterface;
use JsPress\JsPress\Interfaces\PostAdminInterface;
use JsPress\JsPress\Interfaces\PostExtrasInterface;
use JsPress\JsPress\Interfaces\PostInterface;
use JsPress\JsPress\Interfaces\PostTranslationInterface;
use JsPress\JsPress\Interfaces\PostViewInterface;
use JsPress\JsPress\Interfaces\SeoInterface;
use JsPress\JsPress\Interfaces\SitemapInterface;
use JsPress\JsPress\Repositories\AnalysticRepository;
use JsPress\JsPress\Repositories\CategoryAdminRepository;
use JsPress\JsPress\Repositories\CategoryRepository;
use JsPress\JsPress\Repositories\MenuRepository;
use JsPress\JsPress\Repositories\PostAdminRepository;
use JsPress\JsPress\Repositories\PostExtrasRepository;
use JsPress\JsPress\Repositories\PostRepository;
use JsPress\JsPress\Repositories\PostTranslationRepository;
use JsPress\JsPress\Repositories\PostViewRepository;
use JsPress\JsPress\Repositories\SeoRepository;
use JsPress\JsPress\Repositories\SitemapRepository;
use JsPress\JsPress\Services\JsPressLocalization;
use JsPress\JsPress\Helpers\General;
use JsPress\JsPress\Helpers\Image;
use JsPress\JsPress\Helpers\JsPressPanel;
use JsPress\JsPress\Http\Kernel;
use Illuminate\Contracts\Http\Kernel as HttpKernel;
use Illuminate\Support\Facades\Artisan;
use JsPress\JsPress\Interfaces\AdminInterface;
use JsPress\JsPress\Interfaces\MediaInterface;
use JsPress\JsPress\Interfaces\PostTypeInterface;
use JsPress\JsPress\Models\User;
use JsPress\JsPress\Repositories\AdminRepository;
use JsPress\JsPress\Repositories\MediaRepository;
use JsPress\JsPress\Repositories\PostTypeRepository;
use JsPress\JsPress\View\Components\Alert;
use JsPress\JsPress\View\Components\BigAlert;
use JsPress\JsPress\View\Components\Checkbox;
use JsPress\JsPress\View\Components\CheckBoxField;
use JsPress\JsPress\View\Components\Datepicker;
use JsPress\JsPress\View\Components\Dot;
use JsPress\JsPress\View\Components\Editor;
use JsPress\JsPress\View\Components\FalseIcon;
use JsPress\JsPress\View\Components\File;
use JsPress\JsPress\View\Components\Gallery;
use JsPress\JsPress\View\Components\ImageUpload;
use JsPress\JsPress\View\Components\PostObjects;
use JsPress\JsPress\View\Components\PostRepeater;
use JsPress\JsPress\View\Components\Radio;
use JsPress\JsPress\View\Components\Repeater;
use JsPress\JsPress\View\Components\Select;
use JsPress\JsPress\View\Components\FileUploadButton;
use JsPress\JsPress\View\Components\SelectBox;
use JsPress\JsPress\View\Components\SwitchButton;
use JsPress\JsPress\View\Components\Text;
use JsPress\JsPress\View\Components\Textarea;
use JsPress\JsPress\View\Components\TrueIcon;
use JsPress\JsPress\View\Components\Video;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Support\Facades\View;
use App\Providers\RouteServiceProvider as LaravelRouteServiceProvider;

class JsPressServiceProvider extends ServiceProvider{

    /**
     *
     */
    public const CONFIG = 'config';
    /**
     *
     */
    public const LOG_VIEWER_URL = 'js-admin/log-viewer';

    /**
     * @var string
     */
    protected string $seeds_path = '/Database/Seeders';
    /**
     * @var array|string[]
     */
    protected array $commands = [
        'JsPress\JsPress\Http\Commands\CreateAdmin'
    ];

    /**
     * @return void
     */
    public function boot(): void
    {
        Schema::defaultStringLength(125);
        $this->loadViewsFrom(__DIR__ . '/Resources/views', 'JsPress');
        $this->loadMigrationsFrom(__DIR__.'/Database/Migrations');
        $this->loadTranslationsFrom(__DIR__ . '/Resources/lang', 'JsPress');
        $this->loadViewComponentsAs('jspress', [
            TrueIcon::class,
            FalseIcon::class,
            Repeater::class,
            Select::class,
            Checkbox::class,
            Dot::class,
            FileUploadButton::class,
            ImageUpload::class,
            Alert::class,
            BigAlert::class,
            Text::class,
            PostRepeater::class,
            Textarea::class,
            SelectBox::class,
            CheckBoxField::class,
            Radio::class,
            SwitchButton::class,
            PostObjects::class,
            Editor::class,
            Datepicker::class,
            Gallery::class,
            File::class,
            Video::class
        ]);
        $this->publishes([
            __DIR__.'/public/' => public_path('jspress/'),
        ], 'jspress');

        if ($this->app->runningInConsole()) {
            if ($this->isConsoleCommandContains([ 'db:seed', '--seed' ], [ '--class', 'help', '-h' ])) {
                $this->addSeedsAfterConsoleCommandFinished();
            }
        }

        $this->loadViewShares();

    }

    /**
     * @return void
     */
    public function register(): void
    {

        $kernel = app(\Illuminate\Contracts\Http\Kernel::class);
        $loader = AliasLoader::getInstance();
        $this->app->singleton(HttpKernel::class, Kernel::class);
        $this->app->register(LaravelRouteServiceProvider::class);
        $this->app->register(RouteServiceProvider::class);
        $this->app->register(AuthServiceProvider::class);
        $this->commands($this->commands);
        $this->app[self::CONFIG]->set("database.connections.mysql.strict", false);
        $this->app[self::CONFIG]->set("app.local", 'tr');
        $this->app[self::CONFIG]->set("app.fallback_locale", 'tr');
        $this->app[self::CONFIG]->set("app.timezone", 'Europe/Istanbul');
        $this->app[self::CONFIG]->set("auth.providers.users.model", User::class);
        $this->app[self::CONFIG]->set("log-viewer.route_path", self::LOG_VIEWER_URL);
        $this->registerBindings();
        $this->registerHelpers();

        $this->publishes([
            __DIR__ . '/Config/jspress.php' => config_path('jspress.php'),
            __DIR__ . '/Config/jspress_settings.php' => config_path('jspress_settings.php'),
        ], 'jspress');

        $files = $this->app['files']->files(__DIR__ . '/Config');
        foreach ($files as $file) {
            $filename = $this->getConfigBasename($file);
            if($filename != 'jspress' && $filename != 'jspress_settings'){
                $this->mergeConfig($file, $filename);
            }
        }

        app()->bind(MediaInterface::class, MediaRepository::class);
        app()->bind(AdminInterface::class, AdminRepository::class);
        app()->bind(PostTypeInterface::class, PostTypeRepository::class);
        app()->bind(PostInterface::class, PostRepository::class);
        app()->bind(CategoryInterface::class, CategoryRepository::class);
        app()->bind(PostAdminInterface::class, PostAdminRepository::class);
        app()->bind(CategoryAdminInterface::class, CategoryAdminRepository::class);
        app()->bind(PostTranslationInterface::class, PostTranslationRepository::class);
        app()->bind(SeoInterface::class, SeoRepository::class);
        app()->bind(PostExtrasInterface::class, PostExtrasRepository::class);
        app()->bind(MenuInterface::class, MenuRepository::class);
        app()->bind(PostViewInterface::class, PostViewRepository::class);
        app()->bind(AnalysticInterface::class, AnalysticRepository::class);
        app()->bind(SitemapInterface::class, SitemapRepository::class);

        $loader->alias('JsImage', Image::class);
        $loader->alias('General', General::class);
        $loader->alias('JsPressPanel', JsPressPanel::class);

        Paginator::defaultView('vendor.pagination.bootstrap-4');

        $this->setBladeDirectives();
    }

    /**
     * Registers app bindings and aliases.
     */
    protected function registerBindings(): void
    {
        $this->app->singleton(JsPressLocalization::class, function () {
            return new JsPressLocalization();
        });

        $this->app->alias(JsPressLocalization::class, 'jspresslocalization');
    }


    /**
     * Get a value that indicates whether the current command in console
     * contains a string in the specified $fields.
     *
     * @param array|string $contain_options
     * @param array|string|null $exclude_options
     *
     * @return bool
     */
    protected function isConsoleCommandContains(array|string $contain_options, array|string $exclude_options = null) : bool
    {
        $args = Request::server('argv', null);
        if (is_array($args)) {
            $command = implode(' ', $args);
            if (\Str::contains($command, $contain_options) && ($exclude_options == null || !\Str::contains($command, $exclude_options))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Add seeds from the $seed_path after the current command in console finished.
     *
     * @return void
     */
    protected function addSeedsAfterConsoleCommandFinished(): void
    {
        Event::listen(CommandFinished::class, function(CommandFinished $event) {
            // Accept command in console only,
            // exclude all commands from Artisan::call() method.
            if ($event->output instanceof ConsoleOutput) {
                $this->addSeedsFrom(__DIR__ . $this->seeds_path);
            }
        });
    }

    /**
     * Register seeds.
     *
     * @param string $seeds_path
     * @return void
     */
    protected function addSeedsFrom(string $seeds_path): void
    {
        $file_names = glob( $seeds_path . '/*.php');
        foreach ($file_names as $filename)
        {
            $classes = $this->getClassesFromFile($filename);
            foreach ($classes as $class) {
                Artisan::call('db:seed', [ '--class' => $class, '--force' => '' ]);
            }
        }
    }

    /**
     * Get full class names declared in the specified file.
     *
     * @param string $filename
     * @return array an array of class names.
     */
    private function getClassesFromFile(string $filename) : array
    {
        // Get namespace of class (if vary)
        $namespace = "";
        $lines = file($filename);
        $namespaceLines = preg_grep('/^namespace /', $lines);
        if (is_array($namespaceLines)) {
            $namespaceLine = array_shift($namespaceLines);
            $match = array();
            preg_match('/^namespace (.*);$/', $namespaceLine, $match);
            $namespace = array_pop($match);
        }

        // Get name of all class has in the file.
        $classes = array();
        $php_code = file_get_contents($filename);
        $tokens = token_get_all($php_code);
        $count = count($tokens);
        for ($i = 2; $i < $count; $i++) {
            if ($tokens[$i - 2][0] == T_CLASS && $tokens[$i - 1][0] == T_WHITESPACE && $tokens[$i][0] == T_STRING) {
                $class_name = $tokens[$i][1];
                if ($namespace !== "") {
                    $classes[] = $namespace . "\\$class_name";
                } else {
                    $classes[] = $class_name;
                }
            }
        }

        return $classes;
    }

    /**
     * Register helpers file
     */
    public function registerHelpers(): void
    {
        // Load the helpers in src/Http/helpers.php

        if (file_exists($file = __DIR__.'/Http/helpers.php'))
        {
            require $file;
        }
    }

    /**
     * @param $file
     * @return array|string|null
     */
    private function getConfigBasename($file): array|string|null
    {
        return preg_replace('/\\.[^.\\s]{3,4}$/', '', basename($file));
    }

    /**
     * @param $path
     * @param $key
     * @return void
     */
    protected function mergeConfig($path, $key): void
    {
        $config = $this->app[self::CONFIG]->get($key, []);
        foreach (require $path as $k => $v) {
            if (is_array($v)) {
                if (isset($config[$k])) {
                    $config[$k] = array_merge($config[$k], $v);
                } else {
                    $config[$k] = $v;
                }

            } else {
                $config[$k] = $v;
            }
        }
        $this->app[self::CONFIG]->set($key, $config);
    }

    /**
     * @return void
     */
    public function loadViewShares(): void
    {
        View::composer(['JsPress::admin.layouts.app', 'JsPress::admin.filemanager.index'], function($view)
        {
            $view->with('jspress', JsPressPanel::appViewShare());
        });
    }

    protected function setBladeDirectives(): void
    {

        Blade::directive('categories', function ($expiressions = []) {
            return "<?php foreach(get_the_categories($expiressions) as \$category): ?>";
        });
        Blade::directive('endcategories', function () {
            return '<?php endforeach; ?>';
        });
        Blade::directive('the_title', function ($expression) {
            return "<?php echo {$expression['title']} ?>";
        });
        Blade::directive('posts', function ($expiressions = []) {
            return "<?php foreach(get_the_posts_array($expiressions) as \$post): ?>";
        });
        Blade::directive('endposts', function () {
            return '<?php endforeach; ?>';
        });
    }
}
