<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: FileManagerException.php
 * Author: Json Yazılım
 * Class: FileManagerException.php
 * Current Username: Erdinc
 * Last Modified: 27.06.2023 16:25
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Exceptions;

use Exception;

class FileManagerException extends Exception
{
    /**
     * @param $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request): \Illuminate\Http\JsonResponse
    {
        return response()->json(["error" => true, "message" => $this->getMessage()], 422);
    }
}
