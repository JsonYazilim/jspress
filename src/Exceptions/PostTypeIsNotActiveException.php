<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: PostTypeIsNotActiveException.php
 * Author: Json Yazılım
 * Class: PostTypeIsNotActiveException.php
 * Current Username: Erdinc
 * Last Modified: 18.09.2023 13:16
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Exceptions;

use Exception;

class PostTypeIsNotActiveException extends Exception
{
    public function render($request)
    {
        abort(403, $this->getMessage());
    }
}
