<?php
/*
 * Copyright 2023 Erdinç Taze/Json Yazılım. All rights reserved.
 *
 * Package:JsPress CMS
 * Filename: UnsupportedLocaleException.php
 * Author: Json Yazılım
 * Class: UnsupportedLocaleException.php
 * Current Username: Erdinc
 * Last Modified: 31.07.2023 19:07
 *
 * Licensed under the JSON_YAZILIM_CMS license, with the following license key: c1244860af21fc4bd511c999f833a3ad7b088909.
 */

namespace JsPress\JsPress\Exceptions;

use Exception;

class UnsupportedLocaleException extends Exception
{
}
