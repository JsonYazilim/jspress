# JsPress

Admin Panel which is developed by JSON Yazilim for Laravel Framework

## Installition

```
composer require jspress/jspress

```



For Laravel 5.4 and below:
For older versions of the framework, follow the steps below:

Register the service provider in config/app.php


```php
'providers' => [
// [...]
        \JsPress\JsPress\JsPressServiceProvider::class,
],
```

### Assets Files

In order to publish template assets:

```
php artisan vendor:publish 

```
**Press 0 to load all tags:**


### Migrations and Seeders

In order create tables and records:

```
php artisan migrate
php artisan db:seed

```


## Creting Admin User

Create default admin for logged in to panel

```
php artisan create:admin

```


